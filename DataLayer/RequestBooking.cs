﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.BL;
using BWI.DataLayer;
using System.Configuration;

namespace BWI.DataLayer
{
    public class RequestBooking
    {
        public static DBHelper.DBReturnCode GetAllDriver(out List<BWI.Trans_Tbl_ServiceType> objST)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            objST = null;
            try
            {
                var query = from Obj in DB.Trans_Tbl_ServiceTypes select Obj;
                objST = query.ToList();
                if (objST.Count > 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

        public static DBHelper.DBReturnCode Get(string name, out List<BWI.Trans_Tbl_Location> LocTable)
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();
            //Trans_Tbl_Location LocTable = new Trans_Tbl_Location();
            DBHelper.DBReturnCode retcode;

            LocTable = null;
            try
            {
                var query = from objLocation in DB.Trans_Tbl_Locations where objLocation.LocationName.StartsWith(name) select objLocation;
                LocTable = query.ToList();
                if (LocTable.Count > 0)
                {
                    retcode = DBHelper.DBReturnCode.SUCCESS;
                }
                else
                {
                    retcode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                }
            }
            catch
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retcode;
        }
    }
}