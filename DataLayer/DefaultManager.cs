﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.BL;
using BWI.DataLayer;
using System.Configuration;
using System.Data;
using System.Text;
using System.Net.Mail;
using System.Net;
using BWI.BL;

namespace BWI.DataLayer
{
    public class DefaultManager
    {
        public static DBHelper.DBReturnCode UserLogin(string Username, string Password, string UserType)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;
            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retcode = new DBHelper.DBReturnCode();
            retcode = DBHelper.DBReturnCode.SUCCESS;
            try
            {
                if (UserType == "Customer")
                {
                    Trans_Tbl_CustomerMaster Customer = new Trans_Tbl_CustomerMaster();
                    var List = (from Obj in DB.Trans_Tbl_CustomerMasters where Obj.Email == Username && Obj.Password == Password select Obj).ToList();
                    if (List.Count == 1)
                    {
                        GlobalDefaultTransfers GlobalDefault = new GlobalDefaultTransfers();
                        GlobalDefault.Sid = List[0].sid;
                        GlobalDefault.sFirstName = List[0].FirstName;
                        GlobalDefault.sLastName = List[0].LastName;
                        GlobalDefault.sEmail = Username;
                        GlobalDefault.sPassword = List[0].Password;
                        GlobalDefault.sRoleId = "2";
                        HttpContext.Current.Session["UserLogin"] = GlobalDefault;
                        retcode = DBHelper.DBReturnCode.SUCCESS;
                    }
                    else if (List.Count == 0)
                    {
                        retcode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                    }
                }
                else
                {
                    var LoginResult = DB.Proc_Trans_TblLogin(Username, Password);
                    //var LoginResult = from ObjLogin in DB.Trans_Tbl_Logins where ObjLogin.sEmail == Username && ObjLogin.sPassword == Password select ObjLogin;
                    var Query = LoginResult.ToList();

                    if (Query.Count == 1)
                    {
                        GlobalDefaultTransfers GlobalDefault = new GlobalDefaultTransfers();
                        GlobalDefault.sFirstName = Query[0].sFirstName;
                        GlobalDefault.Sid = Query[0].Sid;
                        GlobalDefault.sFirstName = Query[0].sFirstName;
                        GlobalDefault.sLastName = Query[0].sLastName;
                        GlobalDefault.sGender = Query[0].sGender;
                        GlobalDefault.sEducation = Query[0].sEducation;
                        GlobalDefault.sMarried = Query[0].sMarried;
                        GlobalDefault.sSpousName = Query[0].sSpousName;
                        GlobalDefault.sPAddress = Query[0].sPAddress;
                        GlobalDefault.sPLandmark = Query[0].sPLandmark;
                        GlobalDefault.sPPincode = Query[0].sPPincode;
                        GlobalDefault.sPCountry = Query[0].sPCountry;
                        GlobalDefault.sCAddress = Query[0].sCAddress;
                        GlobalDefault.sPCity = Query[0].sPCity;
                        GlobalDefault.sCLandmark = Query[0].sCLandmark;
                        GlobalDefault.sCCity = Query[0].sCCity;
                        GlobalDefault.sCPincode = Query[0].sCPincode;
                        GlobalDefault.sCCountry = Query[0].sCCountry;
                        GlobalDefault.sMobile = Query[0].sMobile;
                        GlobalDefault.sPhone = Query[0].sPhone;
                        GlobalDefault.sEmail = Query[0].sEmail;
                        GlobalDefault.sPassword = Query[0].sPassword;
                        GlobalDefault.sIntroducerName = Query[0].sIntroducerName;
                        GlobalDefault.dRegistrationDate = Query[0].dRegistrationDate;
                        GlobalDefault.sUserType = Query[0].sUserType;
                        GlobalDefault.sRoleId = Query[0].sRoleId;
                        GlobalDefault.sUniqueCode = Query[0].sUniqueCode;
                        GlobalDefault.sCategory = Query[0].sCategory;
                        GlobalDefault.sRefMail = Query[0].sRefMail;
                        GlobalDefault.sIsActive = Convert.ToBoolean(Query[0].sIsActive);
                        GlobalDefault.sUserLimit = Query[0].sUserLimit;
                        GlobalDefault.DOB = Query[0].DOB;
                        HttpContext.Current.Session["UserLogin"] = GlobalDefault;
                        retcode = DBHelper.DBReturnCode.SUCCESS;
                    }
                    else if (Query.Count == 0)
                    {
                        retcode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                    }
                }
            }
            catch
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retcode;
        }

        public static bool SendPassword(string sTo, string sSubject, string sMessageTitle, string Password)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                #region Send Password Template
                sb.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
                sb.Append("<title>limoallaround.com - Customer Receipt</title>  ");
                sb.Append("<style>                                            ");
                sb.Append(".Norm {font-family: Verdana;                       ");
                sb.Append("font-size: 12px;                                 ");
                sb.Append("font-color: red;                               ");
                sb.Append(" }                                               ");
                sb.Append(".heading {font-family: Verdana;                   ");
                sb.Append("  font-size: 14px;                            ");
                sb.Append("  font-weight: 800;                           ");
                sb.Append("	 }                                                  ");
                sb.Append("   td {font-family: Verdana;                         ");
                sb.Append("	  font-size: 12px;                                  ");
                sb.Append("	 }                                                  ");
                sb.Append("  </style>                                           ");
                sb.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
                sb.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
                sb.Append("");
                sb.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
                sb.Append("  <tbody><tr>");
                sb.Append("    <td width='660' colspan='13' valign='top'>");
                sb.Append("      <table width='100%'>");
                sb.Append("        <tbody><tr>");
                sb.Append("        </tr>");
                sb.Append("        <tr>");
                sb.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com'>http://www.bwishuttleservice.com</a></td>");
                sb.Append("        </tr>");
                sb.Append("        <tr>");
                sb.Append("          <td colspan='2' width='100%'><a href='mailto:reservation@bwishuttleservice.com'>reservation@bwishuttleservice.com</a></td>");
                sb.Append("        </tr>");
                sb.Append("        <tr>");
                sb.Append("          <td colspan='2' width='25%'>Call Us: 1-844-904-5151,  410-904-5151</td>");
                //sb.Append("          <td width='75%' align='right'><font size='3'>");
                //sb.Append("		Receipt");
                //sb.Append("	     </font></td>");
                sb.Append("        </tr>");
                sb.Append("      </tbody></table>");
                sb.Append("     </td>");
                sb.Append("  </tr><tr>");
                sb.Append("  </tr><tr>");
                sb.Append("    <td colspan='13' width='660'>");
                sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                sb.Append("  	<tbody><tr>");
                sb.Append("  	</tr>");
                sb.Append("  	<tr>");
                sb.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br><font size='3'>");
                sb.Append("	     </font>Your Password is " + Password + " <b></b><br>&nbsp;</td>");
                sb.Append("	</tr>");
                sb.Append("      </tbody></table>");
                sb.Append("</body></html>            ");
                #endregion

                string msg = "";
                MailMessage message = new MailMessage();
                //SmtpClient smtpClient = new SmtpClient();
                try
                {
                    MailAddress fromAddress = new MailAddress("reservation@bwishuttleservice.com");
                    message.From = fromAddress;
                    message.To.Add(sTo);
                    //message.CC.Add("limoallaround@gmail.com");
                    message.Subject = sSubject;
                    message.IsBodyHtml = true;
                    message.Body = sb.ToString();
                    System.Net.Mail.SmtpClient mailObj = new System.Net.Mail.SmtpClient("relay-hosting.secureserver.net", 25);
                    mailObj.Credentials = new System.Net.NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                    mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    mailObj.EnableSsl = false;
                    message.Sender = new System.Net.Mail.MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                    mailObj.Send(message);
                    message.Dispose();
                    return true;
                }
                catch (SmtpException ex)
                {
                    msg = ex.Message;
                    return false;
                }
                /*System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                mailMsg.From = new MailAddress("support@memorEbook.in", "Transfers");
                mailMsg.To.Add(sTo);
                //mailMsg.To.Add("shahidanwar888@gmail.com");
                mailMsg.Subject = sSubject;
                mailMsg.IsBodyHtml = true;
                mailMsg.Body = sb.ToString();
                SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                mailObj.EnableSsl = false;
                mailMsg.Sender = new MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                mailObj.Send(mailMsg);
                mailMsg.Dispose();
                return true;*/
            }
            catch
            {

                return false;
            }

        }

        #region Email Template
        /*const string SERVER = "EvidentNet";
        const string URL = "http://www.evidentnet.com";
        const string EmailTemplate = "<html><body>" +
                                    "<div align=\"center\">" +
                                    "<table style=\"width:602px;border:silver 1px solid\" cellspacing=\"0\" cellpadding=\"0\">" +
                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;color:White; FONT-SIZE: 14px;background-color:Black\"><span>#title#</span></td>" +
                                    "</tr>" +

                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Name: #name#</td>" +
                                    "</tr>" +


                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Email: #email#</td>" +
                                    "</tr>" +

                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Subject: #Subject#</td>" +
                                    "</tr>" +

                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Message: #text#</td>" +
                                    "</tr>" +

                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:middle;text-align:left;padding:20px 15px 20px 15px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\"><b>Thank You</b><br/>Adminstrator</td>" +
                                    "</tr>" +

                                    "<tr>" +
                                    "<td width=\"100%\" style=\"padding:8px 5px 8px 5px;text-align:center;FONT-SIZE: 8pt;color:#007CC2;FONT-FAMILY: Tahoma;background-color:Silver\">  © 2016 - 2017 Nass Technologies</td>" +
                                    "</tr>" +

                                    "</table>" +
                                    "</div>" +
                                    "</html></body>";*/
        #endregion

        public static bool SendEmail(string sTo, string sFrom, string sSubject, string sMessageTitle, string sName, string sEmail, string Subject, string Website, string sMessage)
        {
            try
            {
                string sMailMessage = "";//EmailTemplate.Replace("#title#", sMessageTitle);
                sMailMessage = sMailMessage.Replace("#name#", sName);
                sMailMessage = sMailMessage.Replace("#email#", sEmail);
                sMailMessage = sMailMessage.Replace("#Subject#", Subject);
                sMailMessage = sMailMessage.Replace("#Website#", Website);
                sMailMessage = sMailMessage.Replace("#text#", sMessage);
                MailMessage message = new MailMessage();
                //SmtpClient smtpClient = new SmtpClient();
                string msg = string.Empty;
                try
                {
                    MailAddress fromAddress = new MailAddress("reservation@bwishuttleservice.com");
                    message.From = fromAddress;
                    message.To.Add("bwiairportshuttleservice@gmail.com");
                    message.CC.Add("SHAHIDANWAR888@GMAIL.COM");
                    message.Subject = sSubject;
                    message.IsBodyHtml = true;
                    message.Body = sMailMessage.ToString();
                    System.Net.Mail.SmtpClient mailObj = new System.Net.Mail.SmtpClient("relay-hosting.secureserver.net", 25);
                    mailObj.Credentials = new System.Net.NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                    mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    mailObj.EnableSsl = false;
                    message.Sender = new System.Net.Mail.MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                    mailObj.Send(message);
                    message.Dispose();
                    return true;
                }
                catch (Exception ex)
                {
                    msg = ex.Message;
                    return false;
                }
                /*System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                mailMsg.From = new MailAddress("support@limoallaround.com", "BWI Shuttle Service");
                mailMsg.To.Add("reservation@bwishuttleservice.com");
                mailMsg.CC.Add("limoallaround@gmail.com");
                //mailMsg.To.Add("kashifkhan4847@gmail.com");
                //mailMsg.CC.Add("kashifkhan4847@yahoo.com");
                mailMsg.Subject = "Enquiry";
                mailMsg.IsBodyHtml = true;
                mailMsg.Body = sMailMessage;
                //SmtpClient mailObj = new SmtpClient("smtp.Universalimpex.asia", 25);
                SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                //mailObj.Credentials = new NetworkCredential("support@Universalimpex.asia", "universal.123");
                mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                mailObj.EnableSsl = false;
                mailObj.Send(mailMsg);
                mailMsg.Dispose();*/
                return true;
            }
            catch
            {
                //SmtpClient mailObj = new SmtpClient("smtp.Universalimpex.asia", 587);
                //mailObj.Credentials = new NetworkCredential("support@Universalimpex.asia", "universal.123");
                return false;
            }

        }

        public static bool MailShoot(string Password, string To, string sName, string sEmail, string Subject, string Phone, string sMessage, string Type)
        {
            try
            {
                #region Email Template
                const string EmailTemplate = "<html><body>" +
                                            "<div align=\"center\">" +
                                            "<table style=\"width:602px;border:silver 1px solid\" cellspacing=\"0\" cellpadding=\"0\">" +
                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;color:White; FONT-SIZE: 14px;background-color:Black\"><span>#title#</span></td>" +
                                            "</tr>" +

                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Name: #name#</td>" +
                                            "</tr>" +


                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Email: #email#</td>" +
                                            "</tr>" +

                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Subject: #Subject#</td>" +
                                            "</tr>" +

                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Phone: #Phone#</td>" +
                                            "</tr>" +

                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Message: #text#</td>" +
                                            "</tr>" +

                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:middle;text-align:left;padding:20px 15px 20px 15px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\"><b>Thank You</b><br/>Adminstrator</td>" +
                                            "</tr>" +

                                            "<tr>" +
                                            "<td width=\"100%\" style=\"padding:8px 5px 8px 5px;text-align:center;FONT-SIZE: 8pt;color:#007CC2;FONT-FAMILY: Tahoma;background-color:Silver\">  © 2016 - 2017 Nass Technologies</td>" +
                                            "</tr>" +

                                            "</table>" +
                                            "</div>" +
                                            "</html></body>";
                #endregion
                string sSubject = "";
                string sMailMessage = "";
                StringBuilder sb = new StringBuilder();
                if (Type == "Password Mail")
                {
                    sSubject = "Your Password Detail";
                    #region Send Password Template
                    sb.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
                    sb.Append("<title>limoallaround.com - Customer Receipt</title>  ");
                    sb.Append("<style>                                            ");
                    sb.Append(".Norm {font-family: Verdana;                       ");
                    sb.Append("font-size: 12px;                                 ");
                    sb.Append("font-color: red;                               ");
                    sb.Append(" }                                               ");
                    sb.Append(".heading {font-family: Verdana;                   ");
                    sb.Append("  font-size: 14px;                            ");
                    sb.Append("  font-weight: 800;                           ");
                    sb.Append("	 }                                                  ");
                    sb.Append("   td {font-family: Verdana;                         ");
                    sb.Append("	  font-size: 12px;                                  ");
                    sb.Append("	 }                                                  ");
                    sb.Append("  </style>                                           ");
                    sb.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
                    sb.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
                    sb.Append("");
                    sb.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
                    sb.Append("  <tbody><tr>");
                    sb.Append("    <td width='660' colspan='13' valign='top'>");
                    sb.Append("      <table width='100%'>");
                    sb.Append("        <tbody><tr>");
                    sb.Append("        </tr>");
                    sb.Append("        <tr>");
                    sb.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com'>http://www.bwishuttleservice.com</a></td>");
                    sb.Append("        </tr>");
                    sb.Append("        <tr>");
                    sb.Append("          <td colspan='2' width='100%'><a href='mailto:reservation@bwishuttleservice.com'>reservation@bwishuttleservice.com</a></td>");
                    sb.Append("        </tr>");
                    sb.Append("        <tr>");
                    sb.Append("          <td colspan='2' width='25%'>Call Us: 1-844-904-5151,  410-904-5151</td>");
                    //sb.Append("          <td width='75%' align='right'><font size='3'>");
                    //sb.Append("		Receipt");
                    //sb.Append("	     </font></td>");
                    sb.Append("        </tr>");
                    sb.Append("      </tbody></table>");
                    sb.Append("     </td>");
                    sb.Append("  </tr><tr>");
                    sb.Append("  </tr><tr>");
                    sb.Append("    <td colspan='13' width='660'>");
                    sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                    sb.Append("  	<tbody><tr>");
                    sb.Append("  	</tr>");
                    sb.Append("  	<tr>");
                    sb.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br><font size='3'>");
                    sb.Append("	     </font>Your Password is " + Password + " <b></b><br>&nbsp;</td>");
                    sb.Append("	</tr>");
                    sb.Append("      </tbody></table>");
                    sb.Append("</body></html>            ");
                    #endregion
                }
                else if (Type == "Enquiry Mail")
                {
                    sMailMessage = EmailTemplate.Replace("#title#", Type);
                    sMailMessage = sMailMessage.Replace("#name#", sName);
                    sMailMessage = sMailMessage.Replace("#email#", sEmail);
                    sMailMessage = sMailMessage.Replace("#Subject#", Subject);
                    sMailMessage = sMailMessage.Replace("#Phone#", Phone);
                    sMailMessage = sMailMessage.Replace("#text#", sMessage);
                    sSubject = "Enquiry";
                }
                string msg = "";
                MailMessage message = new MailMessage();
                
                try
                {
                    MailAddress fromAddress = new MailAddress("reservation@bwishuttleservice.com");
                    message.From = fromAddress;
                    if (Type == "Password Mail")
                        message.To.Add(To);
                    else if (Type == "Enquiry Mail")
                    {
                        message.To.Add("Bwiairportshuttleservice@gmail.com");
                        message.CC.Add("shahidanwar888@gmail.com");
                    }
                   
                    message.Subject = sSubject;
                    message.IsBodyHtml = true;
                    if (Type == "Password Mail")
                        message.Body = sb.ToString();
                    else
                        message.Body = sMailMessage.ToString();
                    System.Net.Mail.SmtpClient mailObj = new System.Net.Mail.SmtpClient("relay-hosting.secureserver.net", 25);
                    mailObj.Credentials = new System.Net.NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                    mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    mailObj.EnableSsl = false;
                    message.Sender = new System.Net.Mail.MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                    mailObj.Send(message);
                    message.Dispose();
                    return true;
                }
                catch (SmtpException ex)
                {
                    msg = ex.Message;
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool TestMail(string sTo, string sSubject, string sMessageTitle, string Password)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                #region Email Template
                const string EmailTemplate = "<html><body>" +
                                            "<div align=\"center\">" +
                                            "<table style=\"width:602px;border:silver 1px solid\" cellspacing=\"0\" cellpadding=\"0\">" +
                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;color:White; FONT-SIZE: 14px;background-color:Black\"><span>#title#</span></td>" +
                                            "</tr>" +

                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Name: #name#</td>" +
                                            "</tr>" +


                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Email: #email#</td>" +
                                            "</tr>" +

                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Subject: #Subject#</td>" +
                                            "</tr>" +

                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Phone: #Phone#</td>" +
                                            "</tr>" +

                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Message: #text#</td>" +
                                            "</tr>" +

                                            "<tr>" +
                                            "<td width=\"100%\" style=\"vertical-align:middle;text-align:left;padding:20px 15px 20px 15px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\"><b>Thank You</b><br/>Adminstrator</td>" +
                                            "</tr>" +

                                            "<tr>" +
                                            "<td width=\"100%\" style=\"padding:8px 5px 8px 5px;text-align:center;FONT-SIZE: 8pt;color:#007CC2;FONT-FAMILY: Tahoma;background-color:Silver\">  © 2016 - 2017 Nass Technologies</td>" +
                                            "</tr>" +

                                            "</table>" +
                                            "</div>" +
                                            "</html></body>";
                #endregion
                string text = (string)HttpContext.Current.Session["Mail"];
                string[] splitter = text.Split('^');
                string msg = "";
                MailMessage message = new MailMessage();
                string sMailMessage = EmailTemplate.Replace("#title#", sMessageTitle);
                sMailMessage = sMailMessage.Replace("#name#", splitter[0]);
                sMailMessage = sMailMessage.Replace("#email#", splitter[1]);
                sMailMessage = sMailMessage.Replace("#Subject#", splitter[2]);
                sMailMessage = sMailMessage.Replace("#Phone#", splitter[3]);
                sMailMessage = sMailMessage.Replace("#text#", splitter[4]);
                try
                {
                    MailAddress fromAddress = new MailAddress("reservation@bwishuttleservice.com");
                    message.From = fromAddress;
                    //message.To.Add(sTo);
                    message.To.Add("Bwiairportshuttleservice@gmail.com");
                    message.CC.Add("shahidanwar888@gmail.com");
                    //message.CC.Add("limoallaround@gmail.com");
                    message.Subject = sSubject;
                    message.IsBodyHtml = true;
                    message.Body = sMailMessage;
                    System.Net.Mail.SmtpClient mailObj = new System.Net.Mail.SmtpClient("relay-hosting.secureserver.net", 25);
                    mailObj.Credentials = new System.Net.NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                    mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    mailObj.EnableSsl = false;
                    message.Sender = new System.Net.Mail.MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                    mailObj.Send(message);
                    message.Dispose();
                    return true;
                }
                catch (SmtpException ex)
                {
                    msg = ex.Message;
                    return false;
                }
                /*System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                mailMsg.From = new MailAddress("support@memorEbook.in", "Transfers");
                mailMsg.To.Add(sTo);
                //mailMsg.To.Add("shahidanwar888@gmail.com");
                mailMsg.Subject = sSubject;
                mailMsg.IsBodyHtml = true;
                mailMsg.Body = sb.ToString();
                SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                mailObj.EnableSsl = false;
                mailMsg.Sender = new MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                mailObj.Send(mailMsg);
                mailMsg.Dispose();
                return true;*/
            }
            catch
            {

                return false;
            }

        }
    }
}