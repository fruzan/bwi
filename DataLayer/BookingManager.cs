﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.Admin;
using BWI.BL;
using BWI.DataLayer;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace BWI.DataLayer
{
    public class BookingManager
    {

        public static DBHelper.DBReturnCode BookingSuccess(string ResDate, string Pickup_Location, string Drop_Location, string PickupTime, string txt_Person, string AirLines, string FlightNumber, string FlightTime, string FName, string LName, string PhoneNumber, string AltPhoneNumber, string Email, string TotalFare, string From, string To, string ApproxDistance, string ApproxTime, string Service, string Stop1, string Stop2, string Stop3, string Stop4, string Stop5, string TotalAmount, string MeetGreet, string GratuityPercent, string GratuityAmount, string CcType, string CvNumber, string OfferCode, decimal DiscountPrice, decimal OfferPercent, string Remark, string Adult, string Child, string Acc4Last)
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();
            Trans_Reservation Add = new Trans_Reservation();
            
            //Trans_BookingReservation Booking = new Trans_BookingReservation();
             float fare =0;
              string Tab = (HttpContext.Current.Session["Tab"]).ToString();
            if(Tab !="3")
            {
                Trans_Tbl_DistanceRate objVehicle = new Trans_Tbl_DistanceRate();
                objVehicle = DB.Trans_Tbl_DistanceRates.Single(D => D.CarType_Sid == Convert.ToInt64(HttpContext.Current.Session["CarType"]));
                fare = Convert.ToSingle(ApproxDistance.Replace("km", "")) * Convert.ToSingle(objVehicle.MilesPerDistance)+Convert.ToSingle(objVehicle.BaseCharge);
                if (OfferPercent != 0)
                {
                    float Per = (fare / 100) * (float)OfferPercent;
                    fare = fare - Per;
                }
            }
            else
            {
                Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                var List = (from ObjVehicle in DB.Trans_tbl_Vehicle_Infos
                            join ObjRate in DB.Trans_Tbl_HourlyRates
                            on Convert.ToInt16(ObjVehicle.sid) equals ObjRate.CarType_Sid
                            where ObjRate.sid == Convert.ToInt64(HttpContext.Current.Session["CarType"])
                            orderby Convert.ToDecimal(ObjRate.HourlyRate) ascending
                            select new
                            {
                                ObjVehicle.sid,
                                ObjVehicle.UniqueId,
                                ObjVehicle.Vehicle_Make,
                                ObjVehicle.Model,
                                //ObjVehicle.Vechile_Type,
                                //ObjVehicle.Max_Capcity,
                                //ObjVehicle.Max_Baggage,
                                //ObjVehicle.Img_Url,
                                //ObjVehicle.Remark,

                                //ObjRate.sid,
                                //ObjRate.Name,
                                //ObjRate.RateGroup,
                                ObjRate.CarType_Sid,
                                //ObjRate.HourlyMinimum,
                               ObjRate.HourlyRate,
                                //ObjRate.ServiceUpto,
                                //ObjRate.Service
                            }).ToList();
                if (List.Count > 0)
                {
                    fare = Convert.ToSingle(Pickup_Location) * Convert.ToSingle(List[0].HourlyRate);
                    if (OfferPercent != 0)
                    {
                        float Per = (fare / 100) * (float)OfferPercent;
                        fare = fare - Per;
                    }
                    HttpContext.Current.Session["CarType"] = List[0].sid;
                }
            }
            DBHelper.DBReturnCode retcode;
            BWI.DataLayer.GlobalDefaultTransfers Global = new BWI.DataLayer.GlobalDefaultTransfers();
            Global = (BWI.DataLayer.GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
            try
            {
                //bool EmailBit = true;
                
                string Phone = PhoneNumber;
                //string CountryCode = Countrycode;
                //var sid = DB.Proc_AddSelectCustomer(FName, LName, Email, Phone, CountryCode);
                var sid = DB.Proc_AddSelectCustomer(FName, LName, Email, Phone, "");

                Random generator = new Random();
                int Reservation = generator.Next(1000000, 10000000);

              
                if (Tab == "1")
                {
                    Add.DropAddress = Drop_Location;      
                    
                }
                if (Tab == "2")
                {
                    Add.Stop1 = Stop1;
                    Add.Stop2 = Stop2;
                    Add.Stop3 = Stop3;
                    Add.Stop4 = Stop4;
                    Add.Stop5 = Stop5;
                    Add.DropAddress = Drop_Location;
                }
                if (Tab == "3")
                {
                    Add.Hours = Pickup_Location;
                }
                Add.uid = sid;
                //Add.Date = Date;
                Add.Service = Service;
                Add.ReservationDate = ResDate;
                Add.PickUpAddress = Pickup_Location;
                Add.Pickup_Time = PickupTime;
                

                //Add.FlightDate = Date;
                Add.Date = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                Add.ModifiedDate = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
               
                Add.FlightTime = FlightTime;
                Add.Persons = txt_Person;
                //Add.Adult = txt_Person;
                Add.FlightNumber = FlightNumber;
                //Add.FlightFrom = FlightArriving;
                //Add.Destination = DestinationAdd;
                Add.FirstName = FName;
                Add.LastName = LName;
                Add.PhoneNumber = PhoneNumber;
                Add.ContactNumber = AltPhoneNumber;
                Add.CardIDNumber = "";
                Add.CardType = "";
                Add.ExpirationDate = "";
                Add.CardIDNumber = "";
                Add.ZIPCode = "";
                Add.CardholderName = "";
                Add.Email = Email;
                Add.ReservationID = "RES-" + Reservation;
                string SessionReservation = "RES-" + Reservation;
                HttpContext.Current.Session["ReservationNo"] = SessionReservation;
                Add.InvoiceNum = "INV-" + Reservation;
                Add.VoucherNum = "VCH-" + Reservation;
                Add.ModifiedDate = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                Add.UpdatedBy = "Customer";
                if (Tab == "4")
                {
                    Add.Adult = Adult;
                    Add.Child = Child;
                }
                
                //Add.TravelType = TravelType;
                Add.Airlines = AirLines;
                //string[] FareTotal = TotalFare.Split(' ');
                //if (MeetGreet == "True")
                //    TotalFare = (Convert.ToDecimal(TotalFare) + 10).ToString();
                Add.TotalFare = Convert.ToDecimal(TotalFare);
                Add.Source = From;
                Add.PickUpAddress = From;
                Add.Destination = To;
                Add.DropAddress = To;
                Add.ApproxDistance = ApproxDistance;
                Add.ApproxTime = ApproxTime;
                Add.Status = "Requested Online";
                //Add.Fare = Fare;
                Add.Gratuity = Convert.ToDecimal(GratuityPercent);
                Add.GratuityAmount = Convert.ToDecimal(GratuityAmount);
                Add.CCType = CcType;
                Add.CCLast4 = Acc4Last;
                Add.Paid = true;
                Add.Completed = false;
                Add.ChildCarSheet = false;
                Add.MeetGreat =Convert.ToBoolean( MeetGreet);
                Add.ReservationType = "Online";
                Add.VehicalType = HttpContext.Current.Session["CarType"].ToString();
                Add.Parking = 0;
                Add.Toll = 0;
                Add.PetInCage = false;
                Add.CurbSidePickUp = false;
                Add.ChildCarSheet = false;
                Add.Fare = Convert.ToDecimal(fare);
                Add.OfferCode = OfferCode;
                Add.DiscountPrice = DiscountPrice;
                Add.OfferPercent = OfferPercent.ToString();
                Add.Remark = Remark;
                DB.Trans_Reservations.InsertOnSubmit(Add);
                DB.SubmitChanges();
                StringBuilder sb = new StringBuilder();
                StringBuilder sbDriver = new StringBuilder();
               // string json = "";
                retcode = DBHelper.DBReturnCode.SUCCESS;

                //Trans_Tbl_Login Driver = DB.Trans_Tbl_Logins.Single(x => x.Sid == Convert.ToInt64(DropDowns[5]));
                //Trans_Reservation Booking = DB.Trans_Reservations.Single(x => x.sid == ResTable.sid);
                    //string pass = (generator.Next(1000000, 10000000)).ToString();
                ReservationHandler r = new ReservationHandler();
                r.AssignBooking(Convert.ToInt64(Add.sid), Convert.ToInt16(0), "Select Driver");
                retcode = DBHelper.DBReturnCode.SUCCESS;
            }
            catch
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retcode;
        }


        public static DBHelper.DBReturnCode BookingSuccessReturn(string ResDate, string Pickup_Location, string Drop_Location, string PickupTime, string txt_Person, string AirLines, string FlightNumber, string FlightTime, string FName, string LName, string PhoneNumber, string AltPhoneNumber, string Email, string TotalFare, string From, string To, string ApproxDistance, string ApproxTime, string Service, string Stop1, string Stop2, string Stop3, string Stop4, string Stop5, string Ret_Date, string Ret_Time, string Ret_FlightNumber, string Ret_Airlines, string Ret_FlightTime, string MeetGreet, string GratuityPercent, string GratuityAmount, string CcType, string CvNumber, string OfferCode, decimal DiscountPrice, decimal OfferPercent, string Remark, string Adult, string Child, string Acc4Last)
        {

            DBHandlerDataContext DB = new DBHandlerDataContext();
            Trans_Reservation Add = new Trans_Reservation();
            
            Trans_BookingReservation Booking = new Trans_BookingReservation();
            DBHelper.DBReturnCode retcode;
            BWI.DataLayer.GlobalDefaultTransfers Global = new BWI.DataLayer.GlobalDefaultTransfers();
            Global = (BWI.DataLayer.GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
            
            try
            {
                bool EmailBit = true;
                var List = (from Obj in DB.Trans_Tbl_CustomerMasters where (Obj.Email == Email) select Obj).ToList();
                if (List.Count > 0)
                {
                    EmailBit = false;
                }
                string Phone = PhoneNumber;
                //string CountryCode = Countrycode;
                var sid = DB.Proc_AddSelectCustomer(FName, LName, Email, Phone, "");


                Random generator = new Random();
                int Reservation = generator.Next(1000000, 10000000);

                string Tab = (HttpContext.Current.Session["Tab"]).ToString();
                if (Tab == "1")
                {
                    //Add.Service = "AirPort Reservation";             
                    Add.Service = Service;
                }
                if (Tab == "2")
                {
                    Add.Stop1 = Stop1;
                    Add.Stop2 = Stop2;
                    Add.Stop3 = Stop3;
                    Add.Stop4 = Stop4;
                    Add.Stop5 = Stop5;
                    Add.Service = "Point To Point Reservation";
                }
                if (Tab == "3")
                {
                    Add.Service = "Hourly Reservation";
                }
                Add.uid = sid;
                //Add.Date = Date;

                Add.ReservationDate = ResDate;
                Add.PickUpAddress = Drop_Location;
                Add.Pickup_Time = PickupTime;
                Add.DropAddress = Pickup_Location;
                //Add.FlightDate = Date;
                Add.Date = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                Add.ModifiedDate = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);

                Add.FlightTime = FlightTime;
                Add.Persons = txt_Person;
                //Add.Adult = txt_Person;
                Add.FlightNumber = FlightNumber;
                //Add.FlightFrom = FlightArriving;
                //Add.Destination = DestinationAdd;
                Add.FirstName = FName;
                Add.LastName = LName;
                Add.PhoneNumber = PhoneNumber;
                Add.ContactNumber = AltPhoneNumber;
                Add.CardIDNumber = "";
                Add.CardType = "";
                Add.ExpirationDate = "";
                Add.CardIDNumber = "";
                Add.ZIPCode = "";
                Add.CardholderName = "";
                Add.Email = Email;
                Add.ReservationID = "RES-" + Reservation;
                string SessionReservation = "RES-" + Reservation;
                HttpContext.Current.Session["ReservationNo"] = SessionReservation;
                Add.InvoiceNum = "INV-" + Reservation;
                Add.VoucherNum = "VCH-" + Reservation;
                Add.ModifiedDate = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                Add.UpdatedBy = "Customer";
                if (Tab == "4")
                {
                    Add.Adult = Adult;
                    Add.Child = Child;
                }
                Add.Airlines = AirLines;
                //Add.TotalFare = Convert.ToDecimal(TotalFare);
                //string[] FareTotal = TotalFare.Split(' ');
                Add.TotalFare = Convert.ToDecimal(TotalFare);
                Add.Source = To;
                Add.Destination = From;
                Add.ApproxDistance = ApproxDistance;
                Add.ApproxTime = ApproxTime;
                Add.Status = "Requested Online";
                //Add.Fare = Fare;
                Add.Gratuity = Convert.ToDecimal(GratuityPercent);
                Add.GratuityAmount = Convert.ToDecimal(GratuityAmount);
                Add.CCType = CcType;
                Add.CCLast4 = Acc4Last;
                Add.Paid = true;
                Add.Completed = false;
                Add.ChildCarSheet = false;
                Add.Return_Date = Ret_Date;
                Add.Ret_Time = Ret_Time;
                Add.Ret_Airlines = Ret_Airlines;
                Add.Ret_FlightNumber = Ret_FlightNumber;
                Add.Ret_FlightTime = Ret_FlightTime;
                Add.MeetGreat = Convert.ToBoolean(false);
                Add.ReservationType = "Online";
                Add.VehicalType = HttpContext.Current.Session["CarType"].ToString();
                Add.OfferCode = OfferCode;
                Add.DiscountPrice = DiscountPrice;
                Add.OfferPercent = OfferPercent.ToString();
                Add.Remark = Remark;
                // Add .tr= TravelDate;
                // Booking.Date = Input[0];
                // Booking.Flighttime = Select[0] + ":" + Select[1];
                // Booking.Persons = Input[1];
                // Booking.FlightNumber = Input[2];
                // Booking.FlightFrom = Input[3];
                // Booking.DestinationAddress = Areas[0];
                // Booking.ContactName = Input[4];
                // Booking.PhoneNumber = Input[5];
                // Booking.CardIDNumber = Input[5];
                // Booking.CardType = Input[5];
                // Booking.ExpirationDate = Input[5];
                // Booking.CardIDNumber = Input[5];
                // Booking.ZIPCode = Input[5];
                // Booking.CardholderName = Input[5];
                // Booking.Email = Input[5];
                // Booking.ReservationID = "RES-" + Reservation;
                // Booking.InvoiceNo = "INV-" + Reservation;
                // Booking.VoucherNo = "VCH-" + Reservation;

                DB.Trans_Reservations.InsertOnSubmit(Add);
                DB.SubmitChanges();
                retcode = DBHelper.DBReturnCode.SUCCESS;

                StringBuilder sb = new StringBuilder();
                StringBuilder sbDriver = new StringBuilder();
                
                string json = "";
                ReservationHandler r = new ReservationHandler();
                r.AssignBooking(Convert.ToInt64(Add.sid), Convert.ToInt16(0), "Select Driver");
                retcode = DBHelper.DBReturnCode.SUCCESS;
                //if (EmailBit == true)
               // {
                    string pass = (generator.Next(1000000, 10000000)).ToString();

                    #region Commented on 11-01-2018
                    
                    
                    #region Customer Mail
                    sb.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
                    sb.Append("<title>limoallaround.com - Customer Receipt</title>  ");
                    sb.Append("<style>                                            ");
                    sb.Append(".Norm {font-family: Verdana;                       ");
                    sb.Append("font-size: 12px;                                 ");
                    sb.Append("font-color: red;                               ");
                    sb.Append(" }                                               ");
                    sb.Append(".heading {font-family: Verdana;                   ");
                    sb.Append("  font-size: 14px;                            ");
                    sb.Append("  font-weight: 800;                           ");
                    sb.Append("	 }                                                  ");
                    sb.Append("   td {font-family: Verdana;                         ");
                    sb.Append("	  font-size: 12px;                                  ");
                    sb.Append("	 }                                                  ");
                    sb.Append("  </style>                                           ");
                    sb.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
                    sb.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
                    sb.Append("");
                    sb.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
                    sb.Append("  <tbody><tr>");
                    sb.Append("    <td width='660' colspan='13' valign='top'>");
                    sb.Append("      <table width='100%'>");
                    sb.Append("        <tbody><tr>");
                    //sb.Append("          <td colspan='2' width='100%' class='heading'><b>Bwi Shuttle Service</b></td>");
                    sb.Append("        </tr>");
                    sb.Append("        <tr>");
                    sb.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com'>http://www.bwishuttleservice.com</a></td>");
                    sb.Append("        </tr>");
                    sb.Append("        <tr>");
                    sb.Append("          <td colspan='2' width='100%'><a href='mailto:reservation@bwishuttleservice.com'>reservation@bwishuttleservice.com</a></td>");
                    sb.Append("        </tr>");
                    sb.Append("        <tr>");
                    sb.Append("          <td colspan='2' width='25%'>Call Us: 1-844-904-5151, 410-904-5151</td>");
                    sb.Append("          <td width='75%' align='right'><font size='3'>");
                    sb.Append("		Receipt");
                    sb.Append("	     </font></td>");
                    sb.Append("        </tr>");
                    sb.Append("      </tbody></table>");
                    sb.Append("     </td>");
                    sb.Append("  </tr><tr>");
                    sb.Append("  </tr><tr>");
                    sb.Append("    <td colspan='13' width='660'>");
                    sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                    sb.Append("  	<tbody><tr>");
                    sb.Append("  	  <td width='70%' style='border-bottom:medium solid blue; font-size:16px'><b>Reservation # " + Add.ReservationID + "</b></td>");
                    sb.Append("  	  <td width='30%' align='right' style='border-bottom:medium solid blue;'>Service Date: " + Add.Date + "</td>");
                    sb.Append("  	</tr>");
                    sb.Append("  	<tr>");
                    sb.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br>Your Booking with following Details!  Below please find your confirmation. If any of the information appears to be incorrect, please contact our office immediately to correct it.<br>&nbsp;</td>");
                    sb.Append("	</tr>");
                    sb.Append("      </tbody></table>");
                    sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                    sb.Append("	<tbody><tr>");
                    sb.Append("	  <td class='heading'>Pick-up / Drop Date:</td>");
                    sb.Append("	  <td width='75%'>" + Add.ReservationDate + "</td>                 ");
                    sb.Append("	</tr>                                             ");


                    //sb.Append("	<tr>                                              ");
                    //sb.Append("	  <td class='heading'>Driver Name:</td>          ");
                    //sb.Append("	  <td width='75%'>" + Driver.sFirstName + " " + Driver.sLastName + "</td>                      ");
                    //sb.Append("	</tr>                                             ");


                    //sb.Append("	<tr>                                              ");
                    //sb.Append("	  <td class='heading'>Driver Mobile:</td>          ");
                    //sb.Append("	  <td width='75%'>" + Driver.sMobile + "</td>                      ");
                    //sb.Append("	</tr>                                             ");


                    //sb.Append("	<tr>                                              ");
                    //sb.Append("	  <td class='heading'>Driver Email:</td>          ");
                    //sb.Append("	  <td width='75%'>" + Driver.sEmail + "</td>                      ");
                    //sb.Append("	</tr>                                             ");



                    //sb.Append("	<tr>                                              ");
                    //sb.Append("	  <td class='heading'>Pick-up Time:</td>          ");
                    //sb.Append("	  <td width='75%'>" + Add.FlightTime + "</td>                      ");
                    //sb.Append("	</tr>                                             ");

                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Full Name:</td>       ");
                    sb.Append("	  <td width='75%'>" + Add.FirstName + " " + Add.LastName + "</td>           ");
                    sb.Append("	</tr>                                             ");
                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Contact Number:</td>        ");
                    sb.Append("	  <td width='75%'>" + Add.PhoneNumber + "</td>               ");
                    sb.Append("	</tr>                                             ");
                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>No. of Passenger:</td>         ");
                    sb.Append("	  <td width='75%'>" + Add.Persons + "</td>                          ");
                    sb.Append("	</tr>                                             ");
                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Service:</td>       ");
                    sb.Append("	  <td width='75%'>" + Service + "</td>                          ");
                    sb.Append("	</tr>	                                          ");
                    //sb.Append("	<tr>                                              ");
                    //sb.Append("	  <td class='heading' valign='Top' style='border-bottom:thin solid green;'>Vehicle Type:</td>           ");
                    //sb.Append("	  <td width='75%' style='border-bottom:thin solid green;'>                                              ");
                    //sb.Append("	  " + Add.VehicalType + "<br>&nbsp;</td>                                                                                    ");
                    //sb.Append("	</tr>                                                                                                   ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading'>Trip Routing Information:</td>                                                    ");
                    sb.Append("	                                                                                                        ");
                    sb.Append("	  <td><b>Pick-up Location: &nbsp;</b>" + Add.PickUpAddress + "</td>                         ");
                    sb.Append("	                                                                                                        ");
                    sb.Append("	</tr>                                                                                                   ");
                    //sb.Append("	<tr>                                                                                                    ");
                    //sb.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                    //sb.Append("	  <td><b>Pick-up Instructions: &nbsp;</b>your reservation is canceled by xx/xx/2016<br>&nbsp;</td>      ");
                    //sb.Append("	</tr>	                                                                                                ");
                    //sb.Append("                                                                                                         ");
                    if (Service != "Hourly Reservation")
                    {
                        sb.Append("	<tr>                                                                                                    ");
                        sb.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                        sb.Append("	                                                                                                        ");
                        sb.Append("	  <td><b>Drop-off Location: &nbsp;</b>" + Add.DropAddress + "</td>                                                  ");
                        sb.Append("	                                                                                                        ");
                        sb.Append("	</tr>	                                                                                                ");
                    }
                    sb.Append("        <tr>                                                                                             ");
                    sb.Append("          <td style='border-bottom:thin solid green;' colspan='2'>&nbsp;</td>                            ");
                    sb.Append("        </tr>                                                                                            ");
                    sb.Append("                                                                                                         ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading' valign='top'>Payment Method:</td>                                                 ");
                    sb.Append("	  <td width='75%'>" + Add.CCType.Replace("Card", "") + " Card<br>&nbsp;</td>                                                            ");
                    sb.Append("	</tr>                                                                                                   ");
                    sb.Append("                                                                                                         ");
                    sb.Append("                                                                                                         ");
                    //sb.Append(" 	<tr>                                                                                                ");
                    //sb.Append("	  <td class='heading'>Quoted Rate:</td>                                                                 ");
                    //sb.Append("	  <td>Rs. " + Add.Fare + "                                                                                         ");
                    //sb.Append("	</td></tr>                                                                                              ");
                    //sb.Append("                                                                                                         ");
                    //sb.Append("	<tr>                                                                                                    ");
                    //sb.Append("	  <td class='heading'>Gratuity:</td>                                                                    ");
                    //sb.Append("	  <td>Rs. " + Add.Gratuity + "                                                                                          ");
                    //sb.Append("	</td></tr>                                                                                              ");
                    //sb.Append("                                                                                                         ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading'>Reservation Total:</td>                                                           ");
                    sb.Append("	  <td>Rs. " + Add.TotalFare + "                                                                                         ");
                    sb.Append("	</td></tr>                                                                                              ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td style='border-bottom:thin solid green;' class='heading' valign='top'><font color='red'>Total Due:</font></td>");
                    sb.Append("	  <td style='border-bottom:thin solid green;'><font color='red'>Rs. 0.00<br>&nbsp;</font></td>     ");
                    sb.Append("	</tr>                                                                                              ");
                    sb.Append("	<tr>                                                                                               ");
                    sb.Append("	  <td class='heading' valign='top'>Terms &amp; Conditions / Reservation Agreement:</td>            ");
                    sb.Append("	  <td>Guest agrees that there will be no smoking in our vehicles.<br>                              ");
                    sb.Append("		Guest assures that no illegal drugs are brought into our vehicles.<br>                         ");
                    sb.Append("		Guest agrees that no alcoholic beverages shall be brought into our vehicles.                   ");
                    sb.Append("		Guest agrees that the passengers capacity of vehicle provided shall not be exceeded.<br>       ");
                    sb.Append("		In case of misconduct by your party, chauffeur has the right to terminate this agreement without refunds.<br>");
                    sb.Append("		Guest holds Limo All Around Service harmless and not liable for any personal or material damages arising from the conduct of his/her party.<br>");
                    sb.Append("		Guest is responsible for the full payment of any overtime charges, beyond the original agreement.<br>");
                    sb.Append("		Guest agrees to 100% payment charged to his/her credit card at the time reservation has been made.<br>");
                    sb.Append("		Luxury Sedan guests can cancel 3 hours before pickup time with $10.00 processing fee; if not you will be charged the full amount of reservation. Stretch limousine and Van Guests can cancel 48 hours before pickup time with a $20.00 processing fee; if you cancel after 48 hours, you will be billed for a minimum 3 hours of service charges.<br>");
                    sb.Append("		Guest acknowledges that he/she understands that Limo All Around Service imposes a service fee for late night service (11:00PM TO 5:00AM).<br>");
                    sb.Append("		Guest ackowledges that he/she understands that Limo All Around Service imposes an additional service fee for Incoming International flights.<br>		");
                    sb.Append("		Limo All Around Service cannot be held responsible for mechanical problems, inclement weather, or other uncontrollable circumstances resulting in the inability to start a job as it is schedule time or complete a job. In the event that the requested vehicle can not be provided; Limo All Around Service may provide a vehicle of equal quality.<br>");
                    sb.Append("		Limo All Around Service reserves the right to change rates without notice.  If a rate change is necessary due to a destination being outside of the immediate service area, technical error or increase in fuel charges; Limo All Around Service will make every effort to notify the Guest of the change.<br>");
                    sb.Append("	</td>                    ");
                    sb.Append("	</tr>                    ");
                    sb.Append("      </tbody></table>    ");
                    sb.Append("  </td></tr>              ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("</tbody></table>          ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("</body></html>            ");
                    string mail = sb.ToString();
                    #endregion
                    
                    #region Mailing Section
                    try
                    {

                        System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                        mailMsg.From = new MailAddress("support@memorEbook.in", "Transfers ");
                        mailMsg.To.Add(Add.Email);
                        mailMsg.Subject = "Confirm Booking-" + Add.ReservationID;
                        mailMsg.IsBodyHtml = true;
                        mailMsg.Body = sb.ToString();
                        SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                        mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                        mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        mailObj.EnableSsl = false;
                        mailMsg.Sender = new MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                        mailObj.Send(mailMsg);
                        mailMsg.Dispose();
                    }
                    catch
                    {
                        json = "{\"Session\":\"1\",\"Retcode\":\"3\"}";
                        //return json;
                    }

                    try
                    {
                        System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                        mailMsg.From = new MailAddress("support@memorEbook.in", "Transfers ");
                        //mailMsg.To.Add(Driver.sEmail);
                        mailMsg.Subject = "You have Booking -" + Add.ReservationID;
                        mailMsg.IsBodyHtml = true;
                        mailMsg.Body = sbDriver.ToString();
                        SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                        mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                        mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        mailObj.EnableSsl = false;
                        mailMsg.Sender = new MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                        mailObj.Send(mailMsg);
                        mailMsg.Dispose();
                    }
                    catch
                    {
                        json = "{\"Session\":\"1\",\"Retcode\":\"3\"}";
                        //return json;
                    }

                    #endregion
                    #endregion
                //}

                
            }
            catch
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retcode;
        }



        //public static DBHelper.DBReturnCode BookingSuccess(string[] Input, string[] Select, string[] Areas, string Childs, string TravelType, string TotalFare, string From, string To, string ApproxDistance, string ApproxTime, string TravelDate, string Email)
        //{

        //    DBHandlerDataContext DB = new DBHandlerDataContext();
        //    Trans_Reservation Add = new Trans_Reservation();
        //    Trans_BookingReservation Booking = new Trans_BookingReservation();
        //    DBHelper.DBReturnCode retcode;
        //    BWI.DataLayer.GlobalDefaultTransfers Global = new BWI.DataLayer.GlobalDefaultTransfers();
        //    Global = (BWI.DataLayer.GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];

        //    try
        //    {
        //        string[] Arr = (Input[4]).Split(' ');
        //        string FirstName = Arr[0];
        //        string LastName = Arr[1];

        //        string Phone = Input[6];
        //        string CountryCode = Input[5];
        //        var sid = DB.Proc_AddSelectCustomer(FirstName, LastName, Email, Phone, CountryCode);


        //        Random generator = new Random();
        //        int Reservation = generator.Next(1000000, 10000000);

        //        string Tab = (HttpContext.Current.Session["Tab"]).ToString();
        //        if (Tab == "1")
        //        {
        //            Add.Service = "AirPort Reservation";
        //        }
        //        if (Tab == "2")
        //        {
        //            Add.Service = "Point To Point Reservation";
        //        }
        //        if (Tab == "3")
        //        {
        //            Add.Service = "Hourly Reservation";
        //        }
        //        Add.uid = sid;
        //        Add.Date = Input[0];
        //        Add.FlightTime = Select[0] + ":" + Select[1];
        //        Add.Persons = Input[1];
        //        Add.FlightNumber = Input[2];
        //        Add.FlightFrom = Input[3];
        //        Add.Destination = Areas[0];
        //        Add.FirstName = Arr[0];
        //        Add.LastName = Arr[1];
        //        Add.PhoneNumber = Input[5] + "-" + Input[6];
        //        Add.ContactNumber =
        //        Add.CardIDNumber = Input[8];
        //        Add.CardType = Select[2];
        //        Add.ExpirationDate = Select[3] + "-" + Select[4]; ;
        //        Add.CardIDNumber = Input[8];
        //        Add.ZIPCode = Input[9];
        //        Add.CardholderName = Input[10];
        //        Add.Email = Input[5];
        //        Add.ReservationID = "RES-" + Reservation;
        //        string SessionReservation = "RES-" + Reservation;
        //        HttpContext.Current.Session["ReservationNo"] = SessionReservation;
        //        Add.InvoiceNum = "INV-" + Reservation;
        //        Add.VoucherNum = "VCH-" + Reservation;
        //        Add.ModifiedDate = DateTime.Now.ToString("dd/MM/yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
        //        Add.UpdatedBy = "Customer";
        //        Add.ReservationDate = DateTime.Now.ToString("dd/MM/yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
        //        Add.Child = Childs;
        //        Add.TravelType = TravelType;
        //        Add.TotalFare = Convert.ToDecimal(TotalFare);
        //        Add.Source = From;
        //        Add.Destination = To;
        //        Add.ApproxDistance = ApproxDistance;
        //        Add.ApproxTime = ApproxTime;
        //        Add.Status = "Requested";
        //        // Add .tr= TravelDate;



        //        // Booking.Date = Input[0];
        //        // Booking.Flighttime = Select[0] + ":" + Select[1];
        //        // Booking.Persons = Input[1];
        //        // Booking.FlightNumber = Input[2];
        //        // Booking.FlightFrom = Input[3];
        //        // Booking.DestinationAddress = Areas[0];
        //        // Booking.ContactName = Input[4];
        //        // Booking.PhoneNumber = Input[5];
        //        // Booking.CardIDNumber = Input[5];
        //        // Booking.CardType = Input[5];
        //        // Booking.ExpirationDate = Input[5];
        //        // Booking.CardIDNumber = Input[5];
        //        // Booking.ZIPCode = Input[5];
        //        // Booking.CardholderName = Input[5];
        //        // Booking.Email = Input[5];
        //        // Booking.ReservationID = "RES-" + Reservation;
        //        // Booking.InvoiceNo = "INV-" + Reservation;
        //        // Booking.VoucherNo = "VCH-" + Reservation;

        //        DB.Trans_Reservations.InsertOnSubmit(Add);
        //        DB.SubmitChanges();
        //        retcode = DBHelper.DBReturnCode.SUCCESS;
        //    }
        //    catch
        //    {
        //        retcode = DBHelper.DBReturnCode.EXCEPTION;
        //    }



        //    return retcode;
        //}
    }
}