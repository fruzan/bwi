﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.BL;
using System.Configuration;

namespace BWI.DataLayer
{
    public class ServiceTypeManager
    {

        public static DBHelper.DBReturnCode GetServiceType(out List<BWI.Proc_Trans_GetServiceTypeResult> ServiceType)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();


            ServiceType = null;
            try
            {
                var Service = DB.Proc_Trans_GetServiceType();
                ServiceType = Service.ToList();
                if (ServiceType.Count > 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retCode;
        }


    }
}