﻿using BWI.DataLayer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace BWI.Customer
{
    /// <summary>
    /// Summary description for DefaultHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class DefaultHandler : System.Web.Services.WebService
    {

        string json = "";
        DBHandlerDataContext DB = new DBHandlerDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        [WebMethod(EnableSession = true)]
        public string LoadAll()
        {
            GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            var List = (from Obj in DB.Trans_Reservations select Obj).ToList();
            Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
            //var FirstName = Global.sFirstName;
            //var LastName = Global.sLastName;
            var Email = Global.sEmail;
            // var NewList = List.Where(x => ConvertDateTime(x.Date) < DateTime.Now).ToList();
            //var NewList = List.Where(x => ConvertDateTime(x.ReservationDate) < DateTime.Now).ToList();
            var NewList = (from Obj in DB.Trans_Reservations where Obj.uid == Global.Sid select Obj).ToList();
            //var NewList = List.Where(x => x.DriverSid == Sid).ToList();

            if (NewList.Count > 0)
            {
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                json = jsSerializer.Serialize(NewList);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string CustomerActivity(string From, string To)
        {
            DateTime dFrom = GetAppDate(From);
            DateTime dTo = GetAppDate(To);
            GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
            //var FirstName = Global.sFirstName;
            //var LastName = Global.sLastName;
            var email = Global.sEmail;
            //var Name = FirstName+" " + LastName;
            //var Sid = Global.Sid;
            //var List = (from Obj in DB.Trans_Reservations where Obj.FirstName == FirstName && Obj.LastName == LastName select Obj).ToList();
            var List = (from Obj in DB.Trans_Reservations where Obj.uid == Global.Sid select Obj).ToList();

            //var NewList = List.Where(s => ConvertDateTime(s.ReservationDate) >= dFrom && ConvertDateTime(s.ReservationDate) <= dTo && ConvertDateTime(s.ReservationDate) < DateTime.Now).ToList();
            var NewList = List.Where(s => GetAppDate(s.ReservationDate) >= dFrom && GetAppDate(s.ReservationDate) <= dTo).ToList();

            if (NewList.Count > 0)
            {
                json = jsSerializer.Serialize(NewList);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        public static DateTime GetAppDate(string Date)
        {
            // string Date = Row["AppliedDate"].ToString();
            DateTime date = new DateTime();
            Date = (Date.Split(' ')[0]).Replace("/", "-");
            try
            {
                string[] formats = {"M/d/yyyy", "MM/dd/yyyy",
                                "d/M/yyyy", "dd/MM/yyyy",
                                "yyyy/M/d", "yyyy/MM/dd",
                                "M-d-yyyy", "MM-dd-yyyy",
                                "d-M-yyyy", "dd-MM-yyyy",
                                "yyyy-M-d", "yyyy-MM-dd",
                                "M.d.yyyy", "MM.dd.yyyy",
                                "d.M.yyyy", "dd.MM.yyyy",
                                "yyyy.M.d", "yyyy.MM.dd",
                                "M,d,yyyy", "MM,dd,yyyy",
                                "d,M,yyyy", "dd,MM,yyyy",
                                "yyyy,M,d", "yyyy,MM,dd",
                                "M d yyyy", "MM dd yyyy",
                                "d M yyyy", "dd MM yyyy",
                                "yyyy M d", "yyyy MM dd",
                                "m/dd/yyyy hh:mm:ss tt",
                               };
                //DateTime dateValue;
                foreach (string dateStringFormat in formats)
                {
                    if (DateTime.TryParseExact(Date, dateStringFormat, CultureInfo.CurrentCulture, DateTimeStyles.None,
                                               out date))
                    {
                        date.ToShortDateString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
            return date;
        }

        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                //string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string CurrentPattern = "m-d-y";
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;
        }
    }
}
