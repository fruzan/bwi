﻿function ChangePassWord() {
    debugger;
    var Old = $('#txtOldPassword').val()
    var New = $('#txtNewPassword').val()
    var Confirm = $('#txtConfirmNewPassword').val()


    if (Old == "") {
        $('#SpnMessege').text("Please Enter old password")
        $('#ModelMessege').modal('show')
        return false
    }

    if (New == "") {
        $('#SpnMessege').text("Please Enter new password")
        $('#ModelMessege').modal('show')
        return false
    }
    if (Confirm == "") {
        $('#SpnMessege').text("Please Enter confirm password")
        $('#ModelMessege').modal('show')
        return false
    }

    if (New != Confirm) {
        $('#SpnMessege').text("Password Does not match!")
        $('#ModelMessege').modal('show')
        return false
    }

    var data = { Old: Old, NewPassword: New }

    $.ajax({
        type: "POST",
        url: "../DefaultHandler.asmx/ChangePassword",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.Retcode == 1) {
                $('#txtOldPassword').val('')
                $('#txtNewPassword').val('')
                $('#txtConfirmNewPassword').val('')
                $('#SpnMessege').text("Password change Successfully.")
                $('#ModelMessege').modal('show')
            }

            if (obj.Retcode == 2) {
                $('#SpnMessege').text("Old Password is wrong!")
                $('#ModelMessege').modal('show')
            }

            if (obj.Retcode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}