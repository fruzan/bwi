﻿//$(document).ready(function()
//{
//    GetAll();
//});

function Submit() {
    var From = $('#datepicker1').val();
    var To = $('#datepicker2').val();
    //var DriverName = $("#sel_driver option:selected").text();
    var data = { From: From, To: To}
   
    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/CustomerActivity",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                Arr = obj.Arr
                var ul = '';
                $("#MyDetails").empty();
                for (var i = 0; i < Arr.length; i++) {
                    if (Arr[i].AssignedTo == null)
                        Arr[i].AssignedTo = "not assign"
                    ul += '<tr>';
                    ul += '<td align="center" >'+(i+1)+'</td>';
                    ul += '<td align="center">'+Arr[i].AssignedTo+'</td>';
                    ul += '<td align="center">' + Arr[i].FirstName + ' ' + Arr[i].LastName + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationDate+ '</td>';
                    ul += '<td align="center">' + Arr[i].Service+ '</td>';
                    ul += '<td align="center">' + Arr[i].Source+ '</td>';
                    ul += '<td align="center">' + Arr[i].Destination+ '</td>';
                    ul += '<td align="center">' + Arr[i].TotalFare + '</td>';
                    ul += '<td align="center"><a style="cursor:pointer" onclick="PrintDetails(\'' + i + '\')" ><i class="glyphicon glyphicon-print"></i></a></td>';
                    ul += '</tr>';

                }
                //$("#TopSpan").html('<span>Total Fare : $' + parseFloat( obj.TotalFare).toFixed(2) + '</span>, <span>Percentage Fare :  $' + parseFloat(obj.PercentageOfFare).toFixed(2) + '</span>, <span>Drivers Payout :  $' + parseFloat(obj.DriversPayOut).toFixed(2) + '</span>');
                $("#MyDetails").append(ul);
            }
            else {
                $("#TopSpan").empty();
                $("#MyDetails").empty();
                $('#SpnMessege').text("No Record Found.")
                $('#ModelMessege').modal('show')

            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });

}
var Arr
function GetAll() {
    $("#Details").dataTable().fnClearTable();
    $("#Details").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/LoadAll",
        data: '',
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
               Arr = obj.Arr
                var ul = '';
                $("#").empty();
                for (var i = 0; i < Arr.length; i++) {
                    if (Arr[i].AssignedTo == null)
                        Arr[i].AssignedTo = "not assign"
                    ul += '<tr>';
                    ul += '<td align="center" >' + (i + 1) + '</td>';
                    ul += '<td align="center">' + Arr[i].AssignedTo + '</td>';
                    ul += '<td align="center">' + Arr[i].FirstName + ' ' + Arr[i].LastName + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationDate + '</td>';
                    ul += '<td align="center">' + Arr[i].Service + '</td>';
                    ul += '<td align="center">' + Arr[i].Source + '</td>';
                    ul += '<td align="center">' + Arr[i].Destination + '</td>';
                    ul += '<td align="center">' + Arr[i].TotalFare + '</td>';
                    ul += '<td align="center"><a style="cursor:pointer" onclick="PrintDetails(\''+i+'\')" ><i class="glyphicon glyphicon-print"></i></a></td>';
                    ul += '</tr>';

                }

                $("#Details tbody").append(ul);
                $("#Details").dataTable({
                    "bSort": false
                });
                //$("#TopSpan").html('<span>Total Fare:' + obj.TotalFare + '</span>,<span>Percentage Fare: ' + obj.PercentageOfFare + '</span>,<span>Drivers Payout: ' + obj.DriversPayOut + '</span>');
            }
            else {
                $('#SpnMessege').text("No Record Found.")
                $('#ModelMessege').modal('show')

            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });

}
function PrintDetails(ObjNo) {
    $("#ModelDetails").empty();
    $("#BookingDetails").modal('show')
    var Name = Arr[ObjNo].FirstName + " " + Arr[ObjNo].LastName;
    var ResService = Arr[ObjNo].Service;
    var ResTime ="";
    if (ResService == "From Airport") {
        ResTime=Arr[ObjNo].FlightTime;
    }
    else if (ResService == "To Airport") {
        ResTime=Arr[ObjNo].Pickup_Time;
    }
    else {
        ResTime = Arr[ObjNo].P2PTime;
    }
    if (Arr[ObjNo].AssignedTo == null)
        Arr[ObjNo].AssignedTo = 'Not Assigned'
    var url = "../InvoicePrint.aspx?BookingNo=" + Arr[ObjNo].ReservationID + "&ResDate=" + Arr[ObjNo].ReservationDate + "&Source=" + Arr[ObjNo].Source +
                           "&Destination=" + Arr[ObjNo].Destination + "&AssignedTo=" + Arr[ObjNo].AssignedTo + "&Name=" + Name + "&Service=" + Arr[ObjNo].Service +
                           "&TotalFare=" + "$" + Arr[ObjNo].TotalFare + "&Passenger=" + Arr[ObjNo].Persons + "&PhoneNo=" + Arr[ObjNo].PhoneNumber + "&ResTime=" + ResTime
    $("#ModelDetails").append('<iframe src="' + url + '" style="width:100%;height:500px" />')
    
    //window.location.href = "../InvoicePrint.aspx?BookingNo=" + Arr[ObjNo].ReservationID + "&ResDate=" + Arr[ObjNo].ReservationDate + "&Source=" + Arr[ObjNo].Source +
    //                       "&Destination=" + Arr[ObjNo].Destination + "&AssignedTo=" +Arr[ObjNo].AssignedTo + "&Name=" + Name + "&Service="+Arr[ObjNo].Service +
    //                       "&TotalFare=" + "$" + Arr[ObjNo].TotalFare + "&Passenger=" + Arr[ObjNo].Persons + "&PhoneNo=" + Arr[ObjNo].ContactNumber + "&ResTime=" + ResTime;
}