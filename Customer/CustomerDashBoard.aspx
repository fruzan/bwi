﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Customer/CustomerMaster.Master" AutoEventWireup="true" CodeBehind="CustomerDashBoard.aspx.cs" Inherits="CUT.Customer.CustomerDashBoard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Scrool.css" rel="stylesheet" />
     <script type="text/javascript">
         $(function () {
             $("#datepicker1").datepicker({
                 changeMonth: true,
                 changeYear: true,
                 dateFormat: "m-d-yy"
             });

             $("#datepicker2").datepicker({
                 changeMonth: true,
                 changeYear: true,
                 dateFormat: "m-d-yy"
             });
             GetAll();
             //GetAll()
         });

    </script>
    <script src="Scripts/Default.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <br />
    <%--<span id="TopSpan"> <span>Total Fare:180</span>,<span>Percentage Fare: 80</span>,<span>Drivers Payout: 80</span> </span>--%>
    <span style="margin-left: 1.8%" id="TopSpan"></span>
    <br/>
    <table align="left" style="margin-left: 1.8%">
        <tbody>
            <tr>

                <td style="padding-top: 3.2%">
                    <label>From</label>
                    <input type="text" class="form-control datepicker" id="datepicker1" style="cursor: pointer; background: url('../../images/calendar.png') #fff no-repeat right;" placeholder="mm-dd-yyyy"/>
                </td>
                <td style="padding-top: 3.2%; padding-left: 3.2%">
                    <label>To</label>
                    <input type="text" class="form-control datepicker" id="datepicker2" style="cursor: pointer; background: url('../../images/calendar.png') #fff no-repeat right; padding-left: 25px;" placeholder="mm-dd-yyyy"/>
                </td>
                <td style="padding-top: 4.4%">

                    <input type="button" class="btn-search" value="Search" id="btn_Search" onclick="Submit();"/>
                </td>
                <td colspan="2"></td>
            </tr>
        </tbody>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <div class="offset-2">
        <div class="fblueline">
           
           <%-- <span class="farrow"></span>--%>
            <a style="color: white" href="StaffDetails.aspx" title="Staff Details"><b>Booking Details</b></a>

        </div>
        <div class="frow1">

           <br />
    <br />

            <div class="table-responsive">
                <div style="width: auto; height: 500px; overflow: scroll;">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered dataTable" id="Details" style="width: 100%;">
                        <thead>
                            <tr>
                                <td align="center"><b>S.N</b>
                                </td>
                                <td align="center"><b>Assigned To</b>
                                </td>
                                <td align="center"><b>Guest Name</b>
                                </td>
                                <td align="center"><b>Reservation Date</b>
                                </td>
                                <td align="center"><b>Service</b>
                                </td>
                                <td align="center"><b>Source</b>
                                </td>
                                <td align="center"><b>Destination</b>
                                </td>
                                <td align="center"><b>Total Fare</b>
                                </td>
                                 <td align="center"><b>Print</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody id="MyDetails">
                        </tbody>
                    </table>

                </div>
            </div>
            <br>
        </div>
    </div>


    <div class="modal fade bs-example-modal-lg" id="BookingDetails" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:90%">
            <div class="modal-content" style="background-color: aliceblue;">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel2" style="text-align: center">Booking Details</h4>
                </div>
                <div class="modal-body Iframe" id="ModelDetails">
                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>

