﻿var Text;

$(function () {
    LoadAllComment();
    $("#Return").hide();
    $("#Airlines").hide();
    $('#FlightNo').hide(); 
    $('#FromRet').hide();
    $('#SearchTo').show();
    $('#FlightNoFrom').hide();
    $("#ArrivalTimeFrom").hide();
    //***********for Shuttle*************
    $("#ShuttleFlightNoFrom").hide();
    $("#ShuttleArrivalTimeFrom").hide();
    $("#ShuttleReturn").hide();
    $("#ShuttleFlightNo").hide();
    $("#ShuttleAirlines").hide();
    $("#ShuttleFromRet").hide();
    $('#ShuttleSearchTo').show();
})


function ChangeLocationText() {
    //alert("jghb")
    debugger;
    Text = $("#Service option:selected").val();
    var Div = ''; 
   // alert(Text);
    if (Text == "To Airport") {

        $("#lblLocation").empty();
        Div += 'Pick Up Location'
        $("#lblLocation").append(Div);
        Div = '';
        $("#lblDate").empty();
        Div += 'Pick Up Date'
        $("#lblDate").append(Div);

        $('#FromRet').hide();
        $("#RetReservation").show();
        $('#ChkRetReservation').attr('checked', false)
        $('#FlightNoFrom').hide();
        $("#ArrivalTimeFrom").hide();
        $("#SearchFrom").hide();
        $('#SearchTo').show();
        $("#Pickup_time").show();
    }
    else if (Text == "From Airport") {
        $("#lblLocation").empty();
        Div += 'Drop Location'
        $("#lblLocation").append(Div);
        Div = '';
        $("#lblDate").empty();
        Div += 'Drop Date'
        $("#lblDate").append(Div);
        //$("#RetReservation").hide();
        $("#Return").hide();
        $("#Airlines").hide();
        $('#FlightNo').hide();
        $('#ChkRetReservation').attr('checked', false)
        $('#FlightNoFrom').show();
        $("#ArrivalTimeFrom").show();
        $("#SearchFrom").show();
        $('#SearchTo').hide();
        $("#Pickup_time").hide();
    }
}

function ChangeLocationText1() {
    //alert("jghb")
    debugger;
    Text = $("#ShuttleService option:selected").val();
    var Div = '';
    // alert(Text);
    if (Text == "To Airport") {

        $("#ShuttlelblLocation").empty();
        Div += 'Pick Up Location'
        $("#ShuttlelblLocation").append(Div);
        Div = '';
        $("#ShuttlelblDate").empty();
        Div += 'Pick Up Date'
        $("#ShuttlelblDate").append(Div);

        $('#ShuttleFromRet').hide();
        $("#ShuttleRetReservation").show();
        $('#ShuttleChkRetReservation').attr('checked', false)
        $('#ShuttleFlightNoFrom').hide();
        $("#ShuttleArrivalTimeFrom").hide();
        $("#ShuttleSearchFrom").hide();
        $('#ShuttleSearchTo').show();
        $("#ShuttlePickup_time").show();
    }
    else if (Text == "From Airport") {
        $("#ShuttlelblLocation").empty();
        Div += 'Drop Location'
        $("#ShuttlelblLocation").append(Div);
        Div = '';
        $("#ShuttlelblDate").empty();
        Div += 'Drop Date'
        $("#ShuttlelblDate").append(Div);
        //$("#RetReservation").hide();
        $("#ShuttleReturn").hide();
        $("#ShuttleAirlines").hide();
        $('#ShuttleFlightNo').hide();
        $('#ShuttleChkRetReservation').attr('checked', false)
        $('#ShuttleFlightNoFrom').show();
        $("#ShuttleArrivalTimeFrom").show();
        $("#ShuttleSearchFrom").show();
        $('#ShuttleSearchTo').hide();
        $("#ShuttlePickup_time").hide();
    }
}

var Service;
function RetReservationChange() {
    
    Service = $("#Service option:selected").val();
    if ($("#ChkRetReservation").is(':not(:checked)') && Service == "To Airport") {
        $("#Return").hide();
        $("#Airlines").hide();
        $('#FlightNo').hide();
        $('#SearchToRet').hide();
        $('#SearchTo').show();
    }
    else if ($('#ChkRetReservation').is(':checked') && Service == "To Airport")
    {
        $("#Return").show();
        $("#Airlines").show();
        $('#FlightNo').show();
        $('#SearchToRet').show();
        $('#SearchTo').hide();
        //$('#FlightNo').show();
        //$('#ArrivalTime').show();
        //$('#ArrivingFrom').show();
        //$("#FlightDate").show();
    }
    else if ($("#ChkRetReservation").is(':not(:checked)') && Service == "From Airport") {
       
        $('#FromRet').hide();
        $('#SearchFromRet').hide();
        $('#SearchTo').hide();
        $("#SearchFrom").show();
        $("#PickupTime").hide();
        
    }
    else if ($('#ChkRetReservation').is(':checked') && Service == "From Airport") {

        $('#FromRet').show();
        $('#SearchFromRet').show();
        $('#SearchTo').hide();
        $("#SearchFrom").hide();
        //$("Return_Pickup").val($("#PickUp_Location").text);
    }
}

function RetReservationChange1() {

    Service = $("#ShuttleService option:selected").val();
    if ($("#ShuttleChkRetReservation").is(':not(:checked)') && Service == "To Airport") {
        $("#ShuttleReturn").hide();
        $("#ShuttleAirlines").hide();
        $('#ShuttleFlightNo').hide();
        $('#ShuttleSearchToRet').hide();
        $('#ShuttleSearchTo').show();
    }
    else if ($('#ShuttleChkRetReservation').is(':checked') && Service == "To Airport") {
        $("#ShuttleReturn").show();
        $("#ShuttleAirlines").show();
        $('#ShuttleFlightNo').show();
        $('#ShuttleSearchToRet').show();
        $('#ShuttleSearchTo').hide();
        //$('#FlightNo').show();
        //$('#ArrivalTime').show();
        //$('#ArrivingFrom').show();
        //$("#FlightDate").show();
    }
    else if ($("#ShuttleChkRetReservation").is(':not(:checked)') && Service == "From Airport") {

        $('#ShuttleFromRet').hide();
        $('#ShuttleSearchFromRet').hide();
        $('#ShuttleSearchTo').hide();
        $("#ShuttleSearchFrom").show();
        $("#ShuttlePickupTime").hide();

    }
    else if ($('#ShuttleChkRetReservation').is(':checked') && Service == "From Airport") {

        $('#ShuttleFromRet').show();
        $('#ShuttleSearchFromRet').show();
        $('#ShuttleSearchTo').hide();
        $("#ShuttleSearchFrom").hide();
        //$("Return_Pickup").val($("#PickUp_Location").text);
    }
}

function ChangeLocation()
{
    if(Service == "From Airport")
    {
        var Drop = $("#PickUp_Location").val();
        $("#Return_Pickup").val(Drop)
    }
}

function ChangeLocation1() {
    if (Service == "From Airport") {
        var Drop = $("#ShuttlePickUp_Location").val();
        $("#ShuttleReturn_Pickup").val(Drop)
    }
}

function ChangeAirport()
{
    if (Service == "From Airport") {
        var AirPort = $("#AirPort option:selected").text();
        $("#Return_Drop").val(AirPort)
    }
}

function ChangeAirport1() {
    if (Service == "From Airport") {
        var AirPort = $("#ShuttleAirPort option:selected").text();
        $("#ShuttleReturn_Drop").val(AirPort)
    }
}

function ScrollBookNow() {
    $("html, body").animate({ scrollTop: 690 }, 3500);
}

function SaveComment() {
    var Name = $("#Name").val();
    var Message = $("#Message").val();
    if (Name == "") {
        alert("Please Enter Name");
        return false;
    }
    if (Message == "")
    {
        alert("Please Enter Comment");
        return false;
    }
    var Data = { Name: Name, Message: Message };
    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/SaveComment",
        data: JSON.stringify(Data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            if (obj.retCode == 1){
                alert("Your Comment Send Successfully")
                $("#Name").val('');
                $("#Message").val('');
                LoadAllComment();
            }
            else {
                alert('Something going wrong while sending comment');
            }
        },
    });
}

function LoadAllComment()
{
    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/LoadAllComment",
        data: '{}',
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            $("#Comments").empty();
            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                var Div = '';
                for (var i = 0; i < Arr.length; i++) {
                    if (i == 0)
                        Div += '<div class="item active">'
                    else
                        Div += '<div class="item">'
                    Div += '<p><i class="fa fa-quote-left"></i>' + Arr[i].Text + '<i class="fa fa-quote-right"></i></p>'
                    Div += '<span>' + Arr[i].Name + '</span>'
                    Div +='</div>'
                }
                $("#Comments").append(Div);
            }
        },
    });
}



