﻿var markers
var From
var To
var Dis
var Ti
var Dt
var Person
var Tab
var Source
var AddLatLong
var Temp
var SourceLat
var SourceLong
var DestinationLat
var DestinationLong
var Hours = '';

var SourceLatitude
var SourceLongitude
var DestinationLatitude
var DestinationLongitude
var Stop1="";
var Stop2="";
var Stop3="";
var Stop4="";
var Stop5 = "";

var Pickuptime;

var ChkRet;
var Return_Date;
var Ret_FlightNumber;
var Ret_Airlines;
var Ret_FlightTime;

var From_FlightNumber;
var From_ArrTime;
var From_Airlines;
var From_ReturnDate;
var From_RetTime;

var PickTimeHourly;
var PickTime;
var Sid = 0;
var Adult = 0;
var Child = 0;
var ChildL5 = 0;
//Written onto aspx page
//$(function () {
//    debugger;
//    Tab = GetQueryStringParams('Tab');
//    if (Tab == "1") {
//        var Ser = GetQueryStringParams('Service');
//        Ser = Ser.replace(/%20/g, " ");


//        From = GetQueryStringParams('From');
//        From = From.replace(/%20/g, " ");
//        To = GetQueryStringParams('To');
//        To = To.replace(/%20/g, " ");
//        Temp = GetQueryStringParams('LatLongFirstTab');
//        Person = GetQueryStringParams('Passenger');
//        AddLatLong = Temp.split(',');
//        SourceLat = AddLatLong[0];
//        SourceLong = AddLatLong[1];
//        DestinationLat = AddLatLong[2];
//        DestinationLong = AddLatLong[3];
//        Dis = AddLatLong[5];
//        Dis = Dis.replace(/%20/g, " ");
//        Ti = AddLatLong[4];
//        Ti = Ti.replace(/%20/g, " ");
//    }


//    if (Tab == "2") {
//        From = GetQueryStringParams('SourceP2P');
//        From = From.replace(/%20/g, " ");
//        To = GetQueryStringParams('DestinationP2P');
//        To = To.replace(/%20/g, " ");
//        Temp = GetQueryStringParams('LatLongP2P');
//        Person = GetQueryStringParams('PassengerP2P');
//        AddLatLong = Temp.split(',');
//        SourceLat = AddLatLong[0];
//        SourceLong = AddLatLong[1];
//        DestinationLat = AddLatLong[2];
//        DestinationLong = AddLatLong[3];
//        Dis = AddLatLong[5];
//        Dis = Dis.replace(/%20/g, " ");
//        Ti = AddLatLong[4];
//        Ti = Ti.replace(/%20/g, " ");
//    }
//    if (Tab == "3") {
//        From = GetQueryStringParams('Source');
//        From = From.replace(/%20/g, " ");
//        To = GetQueryStringParams('Destination');
//        To = To.replace(/%20/g, " ");
//        Temp = GetQueryStringParams('LatLong');
//        Person = GetQueryStringParams('Passenger');

//        AddLatLong = Temp.split(',');
//        SourceLat = AddLatLong[0];
//        SourceLong = AddLatLong[1];
//        DestinationLat = AddLatLong[2];
//        DestinationLong = AddLatLong[3];
//        Dis = AddLatLong[5];
//        Dis = Dis.replace(/%20/g, " ");
//        Ti = AddLatLong[4];
//        Ti = Ti.replace(/%20/g, " ");

//        Hours = GetQueryStringParams('Halt');
//    }

//    CarLoad();

//})

var SortArrPrice
function CarLoad() {
    var data = { Tab: Tab, Capacity: Person }
    $.ajax({
        type: "POST",
        url: "../BookingHandler.asmx/LoadCars",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.Retcode == 1) {
                debugger
                var arr = obj.ArrCar;
                SortArrPrice = arr;
                
                $('#MinimumPrice').text(arr[0].BaseCharge)
                $('#TotalCars').text(arr.length)

                var Content = '';
                if (Tab == "1" || Tab == "2")
                {

                    for (var i = 0; i < arr.length; i++)
                    {
                        Content += '<div class="container-fluid no-padding page-content">'
                        Content += '<div class="container">	'
                        Content += '<div class="col-md-6 blog-area">'
                        Content += '<article class="blog-post-list">'                       
                        Content += '<div class="blog-content" style="width:700px;">'
                        Content += '<div class="entry-cover" style="float:left;padding-left:10px">'
                        Content += '    <img src="' + arr[i].Img_Url + ' " alt="" class="imghover" >'
                        Content += '<div style="float:right;padding-right:30px">'
                        Content += '<h3 style="color:#24a7de">' + arr[i].Model + '</h3>'
                        Content += '<ul class="cp-meta-listed">'
                        Content += '<li>Maximum Capacity: <span>' + arr[i].Max_Capcity + '</span></li>'
                        Content += '<li>Max Baggage: <span>' + arr[i].Max_Baggage + '</span></li>'
                        if (Tab == "1" || Tab == "2")
                        {
                            var Price = parseFloat(arr[i].BaseCharge) + parseFloat(Dis) * parseFloat(arr[i].MilesPerDistance);
                            Price = Price.toFixed(2);                           
                            Content += '<li>Price: $ <strong>' + Price + '</strong></li>'
                        }

                        //Content += '<li>Price: $ <strong>' + arr[i].BaseCharge + '</strong></li>'
                        Content += '</ul>'
                        Content += '<button type="button" style="cursor:pointer;background-color:#de302f;color:white;font-weight:600" onclick=RedirectToBooking(' + arr[i].CarType_Sid + ') class="btn">Book Now</button> '
                        Content += '</div>'
                        Content += '</div>'                    

                        Content += '</div>'
                        Content += '</article>'
                        Content += '</div>'
                        Content += '</div>'
                        Content += '</div>'
                    }
                    $('#Div_Cars').append(Content);
                }


                if(Tab=="4")
                {
                    for (var i = 0; i < arr.length; i++) {

                       
                        //   Content += '<li>Maximum Capacity: <span>' + arr[i].Max_Capcity + '</span></li>'
                        //    Content += '<li>Max Baggage: <span>' + arr[i].Max_Baggage + '</span></li>'
                        if (Tab == "4") {
                            Content += CalRate(i);
                        }
                    }
                    $('#Div_Cars').append(Content);
                }

                else if (Tab == "3"){

                    for (var i = 0; i < arr.length; i++) {
                       
                        Content += '<div class="container-fluid no-padding page-content">'
                        
                        Content += '<div class="container">	'
                        Content += '<div class="col-md-6 blog-area">'
                        Content += '<article class="blog-post-list">'                       
                        
                        Content += '<div class="blog-content" style="width:700px;">'
                        Content += '<div class="entry-cover" style="float:left;padding-left:10px">'
                        Content += '    <img src="' + arr[i].Img_Url + '" alt="" style="width:292px;height:146px">'
                        Content += '<div style="float:right;padding-right:30px">'
                        Content += '<h3 style="color:#24a7de">' + arr[i].Model + '</h3>'
                        Content += '<ul class="cp-meta-listed">'
                        Content += '<li>Minimum Hours: <span>' + arr[i].HourlyMinimum + '</span></li>'
                        Content += '<li>Maximum Hours : <span>' + arr[i].ServiceUpto + '</span></li>'
                    
                        //var Price = parseFloat(arr[i].BaseCharge) + parseFloat(Hours) * parseFloat(arr[i].HourlyRate);
                        var Price = parseFloat(Hours) * parseFloat(arr[i].HourlyRate);
                        Price = Price.toFixed(2);
                        Content += '<li>Price: $ <strong>' + Price + '</strong></li>'
                        
                        //Content += '<li>Price: $ <strong>' + arr[i].HourlyRate + '</strong></li>'
                        Content += '</ul>'
                        Content += '<button type="button" style="cursor:pointer;background-color:#de302f;color:white;font-weight:600" onclick=RedirectToBooking(' + arr[i].sid + ') class="btn">Book Now</button> '
                        Content += '</div>'
                        Content += '</div>'
                        Content += '</div>'
                        Content += '</article>'
                        Content += '</div>'
                        Content += '</div>'
                        Content += '</div>'
                    }

                    $('#Div_Cars').append(Content);
                }
            }

            else if (obj.Retcode == 2) {
                $('#SpnMessege').text("No Record Found")
                $('#ModelMessege').modal('show')
            }

            else if (obj.Retcode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function SortByPrice() {
    debugger;
    var arr = SortArrPrice
    arr = arr.reverse();
    var Content = '';
    $('#Div_Cars').empty();
    if (Tab == "1" || Tab == "2") {

        for (var i = 0; i < arr.length; i++) {

            Content += '<div class="offset-2">';
            Content += '<div class="col-md-4 offset-0">';
            Content += '<div class="listitem2">';
            Content += '<a href="' + arr[i].Img_Url + '" data-footer="A custom footer text" data-title="A random title" data-gallery="multiimages" data-toggle="lightbox">';
            Content += '<img src="' + arr[i].Img_Url + '" alt="" /></a>';
            Content += '<div class="liover"></div>';
            Content += '</div>';
            Content += '</div>';
            Content += '<div class="col-md-8 offset-0">';
            Content += '<div class="itemlabel3">';
            Content += '<div class="labelright">';
            Content += '<img src="images/filter-rating-5.png" width="60" alt="" /><br />';
            Content += '<br />';
            Content += '<br />';
            Content += '<img src="images/user-rating-5.png" width="60" alt="" /><br />';
            Content += '<span class="size11 grey">18 Reviews</span><br />';
            Content += '<br />';
            Content += '<span class="green size18"><b>$' + arr[i].BaseCharge + '</b></span><br />';
            Content += '<span class="size11 grey">avg/night</span><br />';
            Content += '<br />';
            Content += '<br />';
            Content += '<button class="bookbtn mt1" type="submit" onclick=RedirectToBooking(' + arr[i].sid + ')>Book</button>';
            Content += '</div>';
            Content += '<div class="labelleft2">';
            Content += '<b><span style="cursor:pointer" onclick=RedirectToBooking(' + arr[i].sid + ')>' + arr[i].Model + '</span></b><br />';
            Content += '<br />';
            Content += '<br />';
            Content += '<p class="grey">';
            Content += '<span class="grey">Maximum Capacity ' + arr[i].Max_Capcity + '</span><br><br>';
            Content += '<span class="grey">Max Baggage ' + arr[i].Max_Baggage + '</span><br><br>';
            Content += '<span class="grey">' + arr[i].Remark + '</span><br>';
            Content += '</p>';
            Content += '<br />';
            Content += '<ul class="hotelpreferences">';
            Content += '<li class="icohp-hairdryer"></li>';
            Content += '<li class="icohp-garden"></li>';
            Content += '<li class="icohp-grill"></li>';
            Content += '<li class="icohp-kitchen"></li>';
            Content += '<li class="icohp-bar"></li>';
            Content += '<li class="icohp-living"></li>';
            Content += '<li class="icohp-tv"></li>';
            Content += '</ul>';
            Content += '</div>';
            Content += '</div>';
            Content += '</div>';
            Content += '</div>';
            Content += '<div class="clearfix"></div>';
            Content += '<div class="offset-2">';
            Content += '<hr class="featurette-divider3">';
            Content += '</div>';
        }
        $('#Div_Cars').append(Content);
    }
    else {

        for (var i = 0; i < arr.length; i++) {
            Content += '<div class="offset-2">';
            Content += '<div class="col-md-4 offset-0">';
            Content += '<div class="listitem2">';
            Content += '<a href="' + arr[i].Img_Url + '" data-footer="A custom footer text" data-title="A random title" data-gallery="multiimages" data-toggle="lightbox">';
            Content += '<img src="' + arr[i].Img_Url + '" alt="" /></a>';
            Content += '<div class="liover"></div>';


            Content += '</div>';
            Content += '</div>';
            Content += '<div class="col-md-8 offset-0">';
            Content += '<div class="itemlabel3">';
            Content += '<div class="labelright">';
            Content += '<img src="images/filter-rating-5.png" width="60" alt="" /><br />';
            Content += '<br />';
            Content += '<br />';
            Content += '<img src="images/user-rating-5.png" width="60" alt="" /><br />';
            Content += '<span class="size11 grey">18 Reviews</span><br />';
            Content += '<br />';
            Content += '<span class="green size18"><b>$' + arr[i].HourlyRate + '</b></span><br />';
            Content += '<span class="size11 grey">Per Hour</span><br />';
            Content += '<br />';
            Content += '<br />';
            Content += '<button class="bookbtn mt1" type="submit" onclick=RedirectToBooking(' + arr[i].sid + ')>Book</button>';
            Content += '</div>';
            Content += '<div class="labelleft2">';
            Content += '<b><span style="cursor:pointer" onclick=RedirectToBooking(' + arr[i].sid + ')>' + arr[i].Model + '</span></b><br />';
            Content += '<br />';
            Content += '<br />';
            Content += '<p class="grey">';
            Content += '<span class="grey">Minimum Hours ' + arr[i].HourlyMinimum + '</span><br><br>';
            Content += '<span class="grey">Maximum Hours ' + arr[i].ServiceUpto + '</span><br><br>';
            Content += '<span class="grey">' + arr[i].Remark + '</span><br>';
            Content += '</p>';
            Content += '<br />';
            Content += '<ul class="hotelpreferences">';
            Content += '<li class="icohp-hairdryer"></li>';
            Content += '<li class="icohp-garden"></li>';
            Content += '<li class="icohp-grill"></li>';
            Content += '<li class="icohp-kitchen"></li>';
            Content += '<li class="icohp-bar"></li>';
            Content += '<li class="icohp-living"></li>';
            Content += '<li class="icohp-tv"></li>';
            Content += '</ul>';
            Content += '</div>';
            Content += '</div>';
            Content += '</div>';
            Content += '</div>';
            Content += '<div class="clearfix"></div>';
            Content += '<div class="offset-2">';
            Content += '<hr class="featurette-divider3">';
            Content += '</div>';
        }

        $('#Div_Cars').append(Content);
    }
}

var Valid=false // whatever result it will sort by Assending first Time
function SortByModel() {
    debugger;
    if (Valid) {
        Valid = false
        Descending()
    }
    else {
        Valid = true
        Assending();
    }
}

function RedirectToBooking(sid) {
    if (Person != 0)
    {
        if (Tab == "1") {
            window.location.href = "OnlineBooking.html?Sid=" + sid + "&From=" + From + "&To=" + To + "&Distance=" + Dis + "&Time=" + Ti + "&Dt=" + Dt + "&Person=" + Person + "&IsP2P=" + "false" + "&Service=" + $('#hdn_service').val() + "&Pickuptime=" + Pickuptime + "&ChkRet=" + ChkRet + "&Return_Date=" + Return_Date + "&Ret_FlightNumber=" + Ret_FlightNumber + "&Ret_Airlines=" + Ret_Airlines + "&Ret_FlightTime=" + Ret_FlightTime + "&From_FlightNumber=" + From_FlightNumber + "&From_ArrTime=" + From_ArrTime + "&From_Airlines=" + From_Airlines + "&From_ReturnDate=" + From_ReturnDate + "&From_RetTime=" + From_RetTime;
        }
        if (Tab == "2") {
            window.location.href = "OnlineBooking.html?Sid=" + sid + "&From=" + From + "&To=" + To + "&Distance=" + Dis + "&Time=" + Ti + "&Dt=" + Dt + "&PickTime=" + PickTime + "&Person=" + Person + "&IsP2P=" + "true" + "&Stop1=" + Stop1 + "&Stop2=" + Stop2 + "&Stop3=" + Stop3 + "&Stop4=" + Stop4 + "&Stop5=" + Stop5;
        }
        if (Tab == "3") {
            window.location.href = "OnlineBooking.html?Sid=" + sid + "&From=" + From + "&To=" + To + "&Distance=" + Dis + "&Time=" + Ti + "&Dt=" + Dt + "&Person=" + Person + "&IsP2P=" + "false" + "&Hours=" + Hours + "&PickTimeHourly=" + PickTimeHourly;
        }
        if (Tab == "4") {
            window.location.href = "OnlineBooking.html?Sid=4052&From=" + From + "&To=" + To + "&Distance=" + Dis + "&Time=" + Ti + "&Dt=" + Dt + "&Person=" + Person + "&IsP2P=" + "false" + "&Service=" + $('#hdn_service').val() + "&Pickuptime=" + Pickuptime + "&ChkRet=" + ChkRet + "&Return_Date=" + Return_Date + "&Ret_FlightNumber=" + Ret_FlightNumber + "&Ret_Airlines=" + Ret_Airlines + "&Ret_FlightTime=" + Ret_FlightTime + "&From_FlightNumber=" + From_FlightNumber + "&From_ArrTime=" + From_ArrTime + "&From_Airlines=" + From_Airlines + "&From_ReturnDate=" + From_ReturnDate + "&From_RetTime=" + From_RetTime + "&Adult=" + Adult + "&Child=" + Child + "&ChildL5=" + ChildL5;
        }
    }
    else {
        alert("Please select passenger")
    }
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}


function Assending() {
    $.ajax({
        type: "POST",
        url: "../BookingHandler.asmx/Assending",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.Retcode == 1) {
                debugger
                var arr = obj.ArrCar;

                $('#Div_Cars').empty();
                var Content = '';
                if (Tab == "1" || Tab == "2") {

                    for (var i = 0; i < arr.length; i++) {

                        Content += '<div class="offset-2">';
                        Content += '<div class="col-md-4 offset-0">';
                        Content += '<div class="listitem2">';
                        Content += '<a href="' + arr[i].Img_Url + '" data-footer="A custom footer text" data-title="A random title" data-gallery="multiimages" data-toggle="lightbox">';
                        Content += '<img src="' + arr[i].Img_Url + '" alt="" /></a>';
                        Content += '<div class="liover"></div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '<div class="col-md-8 offset-0">';
                        Content += '<div class="itemlabel3">';
                        Content += '<div class="labelright">';
                        Content += '<img src="images/filter-rating-5.png" width="60" alt="" /><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<img src="images/user-rating-5.png" width="60" alt="" /><br />';
                        Content += '<span class="size11 grey">18 Reviews</span><br />';
                        Content += '<br />';
                        Content += '<span class="green size18"><b>$' + arr[i].BaseCharge + '</b></span><br />';
                        Content += '<span class="size11 grey">avg/night</span><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<button class="bookbtn mt1" type="submit" onclick=RedirectToBooking(' + arr[i].sid + ')>Book</button>';
                        Content += '</div>';
                        Content += '<div class="labelleft2">';
                        Content += '<b><span style="cursor:pointer" onclick=RedirectToBooking(' + arr[i].sid + ')>' + arr[i].Model + '</span></b><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<p class="grey">';
                        Content += '<span class="grey">Maximum Capacity ' + arr[i].Max_Capcity + '</span><br><br>';
                        Content += '<span class="grey">Max Baggage ' + arr[i].Max_Baggage + '</span><br><br>';
                        Content += '<span class="grey">' + arr[i].Remark + '</span><br>';
                        Content += '</p>';
                        Content += '<br />';
                        Content += '<ul class="hotelpreferences">';
                        Content += '<li class="icohp-hairdryer"></li>';
                        Content += '<li class="icohp-garden"></li>';
                        Content += '<li class="icohp-grill"></li>';
                        Content += '<li class="icohp-kitchen"></li>';
                        Content += '<li class="icohp-bar"></li>';
                        Content += '<li class="icohp-living"></li>';
                        Content += '<li class="icohp-tv"></li>';
                        Content += '</ul>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '<div class="clearfix"></div>';
                        Content += '<div class="offset-2">';
                        Content += '<hr class="featurette-divider3">';
                        Content += '</div>';
                    }
                    $('#Div_Cars').append(Content);
                }
                else {

                    for (var i = 0; i < arr.length; i++) {
                        Content += '<div class="offset-2">';
                        Content += '<div class="col-md-4 offset-0">';
                        Content += '<div class="listitem2">';
                        Content += '<a href="' + arr[i].Img_Url + '" data-footer="A custom footer text" data-title="A random title" data-gallery="multiimages" data-toggle="lightbox">';
                        Content += '<img src="' + arr[i].Img_Url + '" alt="" /></a>';
                        Content += '<div class="liover"></div>';


                        Content += '</div>';
                        Content += '</div>';
                        Content += '<div class="col-md-8 offset-0">';
                        Content += '<div class="itemlabel3">';
                        Content += '<div class="labelright">';
                        Content += '<img src="images/filter-rating-5.png" width="60" alt="" /><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<img src="images/user-rating-5.png" width="60" alt="" /><br />';
                        Content += '<span class="size11 grey">18 Reviews</span><br />';
                        Content += '<br />';
                        Content += '<span class="green size18"><b>$' + arr[i].HourlyRate + '</b></span><br />';
                        Content += '<span class="size11 grey">Per Hour</span><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<button class="bookbtn mt1" type="submit" onclick=RedirectToBooking(' + arr[i].sid + ')>Book</button>';
                        Content += '</div>';
                        Content += '<div class="labelleft2">';
                        Content += '<b><span style="cursor:pointer" onclick=RedirectToBooking(' + arr[i].sid + ')>' + arr[i].Model + '</span></b><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<p class="grey">';
                        Content += '<span class="grey">Minimum Hours ' + arr[i].HourlyMinimum + '</span><br><br>';
                        Content += '<span class="grey">Maximum Hours ' + arr[i].ServiceUpto + '</span><br><br>';
                        Content += '<span class="grey">' + arr[i].Remark + '</span><br>';
                        Content += '</p>';
                        Content += '<br />';
                        Content += '<ul class="hotelpreferences">';
                        Content += '<li class="icohp-hairdryer"></li>';
                        Content += '<li class="icohp-garden"></li>';
                        Content += '<li class="icohp-grill"></li>';
                        Content += '<li class="icohp-kitchen"></li>';
                        Content += '<li class="icohp-bar"></li>';
                        Content += '<li class="icohp-living"></li>';
                        Content += '<li class="icohp-tv"></li>';
                        Content += '</ul>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '<div class="clearfix"></div>';
                        Content += '<div class="offset-2">';
                        Content += '<hr class="featurette-divider3">';
                        Content += '</div>';
                    }

                    $('#Div_Cars').append(Content);
                }
            }

            else if (obj.Retcode == 2) {
                $('#SpnMessege').text("No Record Found")
                $('#ModelMessege').modal('show')
            }

            else if (obj.Retcode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function Descending() {
    $.ajax({
        type: "POST",
        url: "../BookingHandler.asmx/Descending",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.Retcode == 1) {
                debugger
                var arr = obj.ArrCar;

                $('#Div_Cars').empty();
                var Content = '';
                if (Tab == "1" || Tab == "2") {

                    for (var i = 0; i < arr.length; i++) {

                        Content += '<div class="offset-2">';
                        Content += '<div class="col-md-4 offset-0">';
                        Content += '<div class="listitem2">';
                        Content += '<a href="' + arr[i].Img_Url + '" data-footer="A custom footer text" data-title="A random title" data-gallery="multiimages" data-toggle="lightbox">';
                        Content += '<img src="' + arr[i].Img_Url + '" alt="" /></a>';
                        Content += '<div class="liover"></div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '<div class="col-md-8 offset-0">';
                        Content += '<div class="itemlabel3">';
                        Content += '<div class="labelright">';
                        Content += '<img src="images/filter-rating-5.png" width="60" alt="" /><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<img src="images/user-rating-5.png" width="60" alt="" /><br />';
                        Content += '<span class="size11 grey">18 Reviews</span><br />';
                        Content += '<br />';
                        Content += '<span class="green size18"><b>$' + arr[i].BaseCharge + '</b></span><br />';
                        Content += '<span class="size11 grey">avg/night</span><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<button class="bookbtn mt1" type="submit" onclick=RedirectToBooking(' + arr[i].sid + ')>Book</button>';
                        Content += '</div>';
                        Content += '<div class="labelleft2">';
                        Content += '<b><span style="cursor:pointer" onclick=RedirectToBooking(' + arr[i].sid + ')>' + arr[i].Model + '</span></b><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<p class="grey">';
                        Content += '<span class="grey">Maximum Capacity ' + arr[i].Max_Capcity + '</span><br><br>';
                        Content += '<span class="grey">Max Baggage ' + arr[i].Max_Baggage + '</span><br><br>';
                        Content += '<span class="grey">' + arr[i].Remark + '</span><br>';
                        Content += '</p>';
                        Content += '<br />';
                        Content += '<ul class="hotelpreferences">';
                        Content += '<li class="icohp-hairdryer"></li>';
                        Content += '<li class="icohp-garden"></li>';
                        Content += '<li class="icohp-grill"></li>';
                        Content += '<li class="icohp-kitchen"></li>';
                        Content += '<li class="icohp-bar"></li>';
                        Content += '<li class="icohp-living"></li>';
                        Content += '<li class="icohp-tv"></li>';
                        Content += '</ul>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '<div class="clearfix"></div>';
                        Content += '<div class="offset-2">';
                        Content += '<hr class="featurette-divider3">';
                        Content += '</div>';
                    }
                    $('#Div_Cars').append(Content);
                }
                else {

                    for (var i = 0; i < arr.length; i++) {
                        Content += '<div class="offset-2">';
                        Content += '<div class="col-md-4 offset-0">';
                        Content += '<div class="listitem2">';
                        Content += '<a href="' + arr[i].Img_Url + '" data-footer="A custom footer text" data-title="A random title" data-gallery="multiimages" data-toggle="lightbox">';
                        Content += '<img src="' + arr[i].Img_Url + '" alt="" /></a>';
                        Content += '<div class="liover"></div>';


                        Content += '</div>';
                        Content += '</div>';
                        Content += '<div class="col-md-8 offset-0">';
                        Content += '<div class="itemlabel3">';
                        Content += '<div class="labelright">';
                        Content += '<img src="images/filter-rating-5.png" width="60" alt="" /><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<img src="images/user-rating-5.png" width="60" alt="" /><br />';
                        Content += '<span class="size11 grey">18 Reviews</span><br />';
                        Content += '<br />';
                        Content += '<span class="green size18"><b>$' + arr[i].HourlyRate + '</b></span><br />';
                        Content += '<span class="size11 grey">Per Hour</span><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<button class="bookbtn mt1" type="submit" onclick=RedirectToBooking(' + arr[i].sid + ')>Book</button>';
                        Content += '</div>';
                        Content += '<div class="labelleft2">';
                        Content += '<b><span style="cursor:pointer" onclick=RedirectToBooking(' + arr[i].sid + ')>' + arr[i].Model + '</span></b><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<p class="grey">';
                        Content += '<span class="grey">Minimum Hours ' + arr[i].HourlyMinimum + '</span><br><br>';
                        Content += '<span class="grey">Maximum Hours ' + arr[i].ServiceUpto + '</span><br><br>';
                        Content += '<span class="grey">' + arr[i].Remark + '</span><br>';
                        Content += '</p>';
                        Content += '<br />';
                        Content += '<ul class="hotelpreferences">';
                        Content += '<li class="icohp-hairdryer"></li>';
                        Content += '<li class="icohp-garden"></li>';
                        Content += '<li class="icohp-grill"></li>';
                        Content += '<li class="icohp-kitchen"></li>';
                        Content += '<li class="icohp-bar"></li>';
                        Content += '<li class="icohp-living"></li>';
                        Content += '<li class="icohp-tv"></li>';
                        Content += '</ul>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '<div class="clearfix"></div>';
                        Content += '<div class="offset-2">';
                        Content += '<hr class="featurette-divider3">';
                        Content += '</div>';
                    }

                    $('#Div_Cars').append(Content);
                }
            }

            else if (obj.Retcode == 2) {
                $('#SpnMessege').text("No Record Found")
                $('#ModelMessege').modal('show')
            }

            else if (obj.Retcode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}


function PriceFilter() {
    debugger;

    var Min = $('#Slider1').val();
    var Max = $('#Slider2').val();

    var data = { Min: Min, Max: Max }
    $.ajax({
        type: "POST",
        url: "../BookingHandler.asmx/PriceFilter",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.Retcode == 1) {
                debugger
                var arr = obj.ArrCar;
                $('#Div_Cars').empty();
                var Content = '';
                if (Tab == "1" || Tab == "2") {

                    for (var i = 0; i < arr.length; i++) {

                        Content += '<div class="offset-2">';
                        Content += '<div class="col-md-4 offset-0">';
                        Content += '<div class="listitem2">';
                        Content += '<a href="' + arr[i].Img_Url + '" data-footer="A custom footer text" data-title="A random title" data-gallery="multiimages" data-toggle="lightbox">';
                        Content += '<img src="' + arr[i].Img_Url + '" alt="" /></a>';
                        Content += '<div class="liover"></div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '<div class="col-md-8 offset-0">';
                        Content += '<div class="itemlabel3">';
                        Content += '<div class="labelright">';
                        Content += '<img src="images/filter-rating-5.png" width="60" alt="" /><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<img src="images/user-rating-5.png" width="60" alt="" /><br />';
                        Content += '<span class="size11 grey">18 Reviews</span><br />';
                        Content += '<br />';
                        Content += '<span class="green size18"><b>$' + arr[i].BaseCharge + '</b></span><br />';
                        Content += '<span class="size11 grey">avg/night</span><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<button class="bookbtn mt1" type="submit" onclick=RedirectToBooking(' + arr[i].sid + ')>Book</button>';
                        Content += '</div>';
                        Content += '<div class="labelleft2">';
                        Content += '<b><span style="cursor:pointer" onclick=RedirectToBooking(' + arr[i].sid + ')>' + arr[i].Model + '</span></b><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<p class="grey">';
                        Content += '<span class="grey">Maximum Capacity ' + arr[i].Max_Capcity + '</span><br><br>';
                        Content += '<span class="grey">Max Baggage ' + arr[i].Max_Baggage + '</span><br><br>';
                        Content += '<span class="grey">' + arr[i].Remark + '</span><br>';
                        Content += '</p>';
                        Content += '<br />';
                        Content += '<ul class="hotelpreferences">';
                        Content += '<li class="icohp-hairdryer"></li>';
                        Content += '<li class="icohp-garden"></li>';
                        Content += '<li class="icohp-grill"></li>';
                        Content += '<li class="icohp-kitchen"></li>';
                        Content += '<li class="icohp-bar"></li>';
                        Content += '<li class="icohp-living"></li>';
                        Content += '<li class="icohp-tv"></li>';
                        Content += '</ul>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '<div class="clearfix"></div>';
                        Content += '<div class="offset-2">';
                        Content += '<hr class="featurette-divider3">';
                        Content += '</div>';
                    }
                    $('#Div_Cars').append(Content);
                }
                else {

                    for (var i = 0; i < arr.length; i++) {
                        Content += '<div class="offset-2">';
                        Content += '<div class="col-md-4 offset-0">';
                        Content += '<div class="listitem2">';
                        Content += '<a href="' + arr[i].Img_Url + '" data-footer="A custom footer text" data-title="A random title" data-gallery="multiimages" data-toggle="lightbox">';
                        Content += '<img src="' + arr[i].Img_Url + '" alt="" /></a>';
                        Content += '<div class="liover"></div>';


                        Content += '</div>';
                        Content += '</div>';
                        Content += '<div class="col-md-8 offset-0">';
                        Content += '<div class="itemlabel3">';
                        Content += '<div class="labelright">';
                        Content += '<img src="images/filter-rating-5.png" width="60" alt="" /><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<img src="images/user-rating-5.png" width="60" alt="" /><br />';
                        Content += '<span class="size11 grey">18 Reviews</span><br />';
                        Content += '<br />';
                        Content += '<span class="green size18"><b>$' + arr[i].HourlyRate + '</b></span><br />';
                        Content += '<span class="size11 grey">Per Hour</span><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<button class="bookbtn mt1" type="submit" onclick=RedirectToBooking(' + arr[i].sid + ')>Book</button>';
                        Content += '</div>';
                        Content += '<div class="labelleft2">';
                        Content += '<b><span style="cursor:pointer" onclick=RedirectToBooking(' + arr[i].sid + ')>' + arr[i].Model + '</span></b><br />';
                        Content += '<br />';
                        Content += '<br />';
                        Content += '<p class="grey">';
                        Content += '<span class="grey">Minimum Hours ' + arr[i].HourlyMinimum + '</span><br><br>';
                        Content += '<span class="grey">Maximum Hours ' + arr[i].ServiceUpto + '</span><br><br>';
                        Content += '<span class="grey">' + arr[i].Remark + '</span><br>';
                        Content += '</p>';
                        Content += '<br />';
                        Content += '<ul class="hotelpreferences">';
                        Content += '<li class="icohp-hairdryer"></li>';
                        Content += '<li class="icohp-garden"></li>';
                        Content += '<li class="icohp-grill"></li>';
                        Content += '<li class="icohp-kitchen"></li>';
                        Content += '<li class="icohp-bar"></li>';
                        Content += '<li class="icohp-living"></li>';
                        Content += '<li class="icohp-tv"></li>';
                        Content += '</ul>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '</div>';
                        Content += '<div class="clearfix"></div>';
                        Content += '<div class="offset-2">';
                        Content += '<hr class="featurette-divider3">';
                        Content += '</div>';
                    }

                    $('#Div_Cars').append(Content);
                }
            }

            else if (obj.Retcode == 2) {
                $('#SpnMessege').text("No Record Found")
                $('#ModelMessege').modal('show')
            }

            else if (obj.Retcode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

var Adults = 0;
var Childs = 0;
var ChildLessthan5 = 0;
function CalcPassenger(Para) {
    debugger;
    
    Adults = $("#ShuttleNoOfAdults_Passengers").val();
    Childs = $("#ShuttleNoOfChildren_Passengers").val();
    ChildLessthan5 = $("#Shuttlelessthan5_Passengers").val();
    if (Adults == "") {
        Adults = 0;
    }
    if (Childs == "") {
        Childs = 0;
    }
    if (ChildLessthan5 == "") {
        ChildLessthan5 = 0;
    }
    if (parseFloat(Childs) < parseFloat(ChildLessthan5)) {
        //$("#Shuttlelessthan5_Passengers").val(Child);
        ChildLessthan5 = Childs;
    }
    var TotalPassenger = parseFloat(Adults) + (parseFloat(Childs) - parseFloat(ChildLessthan5));
    if (Para == 1)
        Person = parseInt(parseInt(Person) + parseInt(TotalPassenger));
    else if (Para == 0) {
        if (TotalPassenger <= Person)
            Person = parseInt(parseInt(Person) - parseInt(TotalPassenger));
        else
            Person = 0;
    }
    $('#Div_Cars').empty();
    var Content = CalRate();
    $('#Div_Cars').append(Content);
}

function CalRate()
{
    var Content = '';
    
    Content += '<div class="container-fluid no-padding page-content">'

    Content += '<div class="container">	'
    Content += '<div class="col-md-6 blog-area">'
    Content += '<article class="blog-post-list">'
    Content += '<div class="blog-content" style="width:740px;">'
    Content += '<div class="entry-cover" style="float:left;">'
    Content += '<img src="' + SortArrPrice[0].Img_Url + ' " alt="" style="width:292px;height:146px">'
    Content += '<div style="float:right;padding-right:30px">'
    Content += '<h3 style="color:#24a7de">' + SortArrPrice[0].Model + '</h3>'
    Content += '<ul class="cp-meta-listed">'

    var Price = 0;
    var BaseRate = 0;
    if (Person != 0){
        Price = parseFloat(SortArrPrice[0].BaseCharge) + parseFloat(Dis) * parseFloat(SortArrPrice[0].MilesPerDistance);
        Price = Price.toFixed(2);
        BaseRate = parseFloat(20) + parseFloat(Price);
    }
    var personPrice=0;

    for (i = 0; i < Person; i++)
    {
        if(i==0)
        {
            //personPrice = parseFloat(20) + parseFloat(Price);          
            personPrice =  parseFloat(Price);
        }
        else if(i==1)
        {
            personPrice +=parseFloat(15);
        }
        else
        {
            personPrice += parseFloat(7);
            //var Addprice = personPrice;
        }
    }
    Content += '<li>Price: <strong>$ ' + personPrice.toFixed(2) + '</strong></li>'
    
                       
    var AddPrice = parseFloat(personPrice) - parseFloat(BaseRate);

    //Content += '<li>Price: $ <strong>' + arr[i].BaseCharge + '</strong></li>'
    Content += '</ul>'                       

    Content += '<button type="button" style="cursor:pointer;background-color:#de302f;color:white;font-weight:600" onclick=RedirectToBooking(4052) class="btn">Book Now</button> '
    Content += '<br/>'
    Content += '<br/>'

    Content += '<ul class="cp-meta-listed">'
    if (Person == 0)
        Content += '<li>Base Rate for 0 passenger  <strong>$  ' + BaseRate.toFixed(2) + '</strong></li>'
    else
        Content += '<li>Base Rate for 1 passenger  <strong style="margin-left:35px">$ ' + BaseRate.toFixed(2) + '</strong></li>'
    Content += '</ul>'
    Content += '<ul>'
    if (Person == 0)
        Content += 'Additional Rate for 0 passenger <strong>$  ' + AddPrice.toFixed(2) + '</strong>'
    else
        Content += 'Additional Rate for ' + parseFloat(Person - 1) + ' passenger  <strong>$ ' + AddPrice.toFixed(2) + '</strong>'
    Content += '</ul>'
    Content += '<ul>'
    Content += 'Total  <strong style="margin-left:183px">$ ' + personPrice.toFixed(2) + ' </strong>'
    Content += '</ul>'

    Content += '</div>'
    Content += '<div><br>'
    Content += '<table>'
    Content += '<tr>'
    Content += '<td>'
    Content += '<input type="number" id="ShuttleNoOfAdults_Passengers" class="form-control" placeholder="Adults Passenger" min="0">'
    Content += '</td>'
    Content += '<td Colspan="2">'
    Content += '<input type="number" id="ShuttleNoOfChildren_Passengers" class="form-control" placeholder="No Of Childrens" min="0">'//Adults Passenger
    Content += '</td>'
    Content += '</tr>'
    Content += '<tr>'
    Content += '<td>'
    Content += '<input type="number" id="Shuttlelessthan5_Passengers" class="form-control" placeholder="Child Less Than 5 Yrs" min="0">'
    Content += '</td>'
    Content += '<td>'
    Content += '<img src="../images/icon/Plus.png" alt="Add" style="margin-left: 21px;margin-top: 3px;cursor:pointer" onclick="CalcPassenger(1)" title="Add Passenger">'
    Content += '</td>'
    Content += '<td>'
    Content += '<img src="../images/icon/Minus.png" alt="Sub" style="margin-top: 3px;cursor:pointer" onclick="CalcPassenger(0)" title="Sub Passenger">'
    //Content += '<input type="button" style="cursor:pointer;background-color:#de302f;color:white;font-weight:600" class="form-control" value="-" Onclick="calPassenger()">'
    Content += '</td>'
    Content += '</tr>'
    Content += '</table>'
    Content += '</div>'
    Content += '</div>'
    Content += '</div>'
    Content += '</article>'
    Content += '</div>'
    Content += '</div>'
    Content += '</div>'
    return Content;
}