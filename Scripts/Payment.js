﻿var Sid
var Chk_Terms
var GlobalFrom
var GlobalTo
var GlobalDistance
var GlobalTime
var GlobalDate
var GlobalHours
var Stop1
var Stop2
var Stop3
var Stop4
var Stop5

var Pickup_Time
var Pickup_Hour
var Pickup_Min
var Pickup_AM

var From_ArrTime
var From_ArrHour
var From_ArrMin
var From_ArrAM

var From_FlightNo;
var From_Airlines;

var PickupDate;
var serv;
var Hour;
var ChkRet;

var From_ReturnDate;
var From_RetTime;

var Return_Date;
var Ret_FlightNumber;
var Ret_Airlines;
var Ret_FlightTime;
var Meet_Greet = false;
var Div = '';
var Total;
var GlobalAmount;
var GlobalOnLoadTotal;
var GlobalTotal=0;
var GlobalTotalRet = 0;
var Adult = 0;
var Child = 0;

$(function (){
    //debugger;
   
    //$('#Pickup_Date').datetimepicker({

    //    yearOffset: 0,
    //    lang: 'ch',
    //    timepicker: false,
    //    format: 'm-d-Y',
    //    formatDate: 'm-d-Y',
    //});

    Sid = GetQueryStringParams('Sid');
    From = GetQueryStringParams('From');
    From = From.replace(/%20/g," ");
    To = GetQueryStringParams('To');
    To = To.replace(/%20/g," ");
    Distance = GetQueryStringParams('Distance');
    Distance = Distance.replace(/%20/g, " ");
    Time = GetQueryStringParams('Time');
    Time = Time.replace(/%20/g, " ");
    
    PickupDate = GetQueryStringParams('Dt');
    PickupDate = PickupDate.replace(/%20/g, " ");

    if (GetQueryStringParams('IsP2P') == "true") {
        Stop1 = GetQueryStringParams('Stop1').replace(/%20/g, " ");
        Stop2 = GetQueryStringParams('Stop2').replace(/%20/g, " ");
        Stop3 = GetQueryStringParams('Stop3').replace(/%20/g, " ");
        Stop4 = GetQueryStringParams('Stop4').replace(/%20/g, " ");
        Stop5 = GetQueryStringParams('Stop5').replace(/%20/g, " ");
        var PickTime = GetQueryStringParams('PickTime').replace(/%20/g, " ");
        PickTime = PickTime.split(':');
        var PickHour = PickTime[0];
        var PickMin= PickTime[1];
        var PickAm= PickTime[2];
        $("#RetDivider").hide();
        $("#Ret_Date").hide();
        $("#Ret_Timings").hide();
        $("#Ret_FlightNumber").hide();
        $("#Ret_Airlines").hide();
        $("#FlightNum_From").hide();
        $("#Airline_From").hide();
        $("#Timing").append("Abc");
        Div = '';
        $("#Timing").empty();
        Div += "Pickup time";
        $("#Timing").append(Div);
        $("#Pickup_Hour").val(PickHour);
        $("#Pickup_Minutes").val(PickMin);
        $("#Pickup_AM_PM").val(PickAm);
    }
    Person = GetQueryStringParams('Person');
    Hours = GetQueryStringParams('Hours');
    Hour = GetQueryStringParams('Hours');
    $('#RetReservation').hide();
    if (typeof Hour != 'undefined') {
        var PickTime = GetQueryStringParams('PickTimeHourly').replace(/%20/g, " ");
        
        PickTime = PickTime.split(':');
        var PickHour = PickTime[0];
        var PickMin = PickTime[1];
        var PickAm = PickTime[2];
        $('#Airlines').hide();
        $('#FlightNo').hide();
        $('#ArrivalTime').hide();
        $('#ArrivingFrom').hide();
        $("#FlightDate").hide();
        $("#RetDivider").hide();
        $("#Ret_Date").hide();
        $("#Ret_Timings").hide();
        $("#Ret_FlightNumber").hide();
        $("#Ret_Airlines").hide();
        $("#FlightNum_From").hide();
        $("#Airline_From").hide();
        $("#Pickup_Hour").val(PickHour);
        $("#Pickup_Minutes").val(PickMin);
        $("#Pickup_AM_PM").val(PickAm);
        Div = '';
        $("#Hour").empty();
        Div += "Hours";
        $("#Hour").append(Div);
        $('#Drop_Location').empty();
        $('#Drop_Location').val(Hour);
    }
    ChkRet = GetQueryStringParams('ChkRet');
    serv = GetQueryStringParams('Service');
    if (serv != undefined) {
        serv = serv.replace(/%20/g, " ");
        $('#hdn_service').val(serv);
    }
    $('#Airlines').hide();
    $('#FlightNo').hide();
    $('#ArrivalTime').hide();
    $('#From').text(From)
    $('#FromTop').text(From)
    $('#To').text(To)
    $('#ToTop').text(To)
    $('#Persons').text(Person)

    GlobalFrom = From
    GlobalTo = To
    GlobalDistance = Distance
    GlobalTime = Time
    //GlobalDate = Dt
    GlobalHours = Hours
    Load();
    $("#Pickup_Location").val(From);
    if (typeof Hour == 'undefined')
    {
        $("#Drop_Location").val(To);
    }
    $("#Passenger").val(Person);
    if (serv == "To Airport") {
        Div = '';
        $("#Timing").empty();
        Div += "Pickup time";
        $("#Timing").append(Div);
        Pickup_Time = GetQueryStringParams('Pickuptime');
        Pickup_Time = Pickup_Time.replace(/%20/g, " ");
        Pickup_Time = Pickup_Time.split(':');
        Pickup_Hour = Pickup_Time[0];
        Pickup_Min = Pickup_Time[1];
        Pickup_AM = Pickup_Time[2];
        $("#Pickup_Hour option:selected").text(Pickup_Hour)
        $("#Pickup_Minutes option:selected").text(Pickup_Min)
        $("#Pickup_AM_PM option:selected").text(Pickup_AM)
        $("#FlightNum_From").hide();
        $("#Airline_From").hide();
        if (ChkRet == "true") {
            Div = '';
            $("#Ret_Timing").empty();
            Div += "Flight arrival time";
            $("#Ret_Timing").append(Div);

            Return_Date = GetQueryStringParams('Return_Date');
            $("#Return_Date").val(Return_Date);

            Ret_FlightNumber = GetQueryStringParams('Ret_FlightNumber');
            $("#Ret_FlightNo").val(Ret_FlightNumber);

            Ret_Airlines = GetQueryStringParams('Ret_Airlines');
            Ret_Airlines = Ret_Airlines.replace(/%20/g, " ");
            $("#Ret_Airline").val(Ret_Airlines);

            Ret_FlightTime = GetQueryStringParams('Ret_FlightTime');
            Ret_FlightTime = Ret_FlightTime.split(':');
            var RetHour = Ret_FlightTime[0];
            var RetMin = Ret_FlightTime[1];
            var RetAM = Ret_FlightTime[2];
            $("#Ret_Hour option:selected").text(RetHour)
            $("#Ret_Minutes option:selected").text(RetMin)
            $("#Ret_AM_PM option:selected").text(RetAM)

            $("#RetDivider").show();
            $("#Ret_Date").show();
            $("#Ret_Timings").show();
            $("#Ret_FlightNumber").show();
            $("#Ret_Airlines").show();
        }
        else {
            $("#RetDivider").hide();
            $("#Ret_Date").hide();
            $("#Ret_Timings").hide();
            $("#Ret_FlightNumber").hide();
            $("#Ret_Airlines").hide();
        }
    }
    else if(serv=="From Airport")
    {
        $("#Ret_FlightNumber").hide();
        $("#Ret_Airlines").hide();

        Div = '';
        $("#Timing").empty();
        Div += "Flight arrival time";
        $("#Timing").append(Div);
        From_ArrTime = GetQueryStringParams('From_ArrTime');
        From_ArrTime = From_ArrTime.replace(/%20/g, " ");
        From_ArrTime = From_ArrTime.split(':');
        From_ArrHour = From_ArrTime[0];
        From_ArrMin = From_ArrTime[1];
        From_ArrAM = From_ArrTime[2];
        $("#Pickup_Hour option:selected").text(From_ArrHour)
        $("#Pickup_Minutes option:selected").text(From_ArrMin)
        $("#Pickup_AM_PM option:selected").text(From_ArrAM)

        From_FlightNo = GetQueryStringParams('From_FlightNumber');
        From_FlightNo = From_FlightNo.replace(/%20/g, " ");
        $("#FlightNo_From").val(From_FlightNo);

        From_Airlines = GetQueryStringParams('From_Airlines');
        From_Airlines = From_Airlines.replace(/%20/g, " ");
        $("#Flight_From").val(From_Airlines);

        $("#FlightNum_From").show();
        $("#Airline_From").show();
        if (ChkRet == "true") {
            Div = '';
            $("#Ret_Timing").empty();
            Div += "Return time";
            $("#Ret_Timing").append(Div);

            From_ReturnDate = GetQueryStringParams('From_ReturnDate');
            $("#Return_Date").val(From_ReturnDate);

            From_RetTime = GetQueryStringParams('From_RetTime');
            From_RetTime = From_RetTime.split(':');
            var From_RetHour = From_RetTime[0];
            var From_RetMin = From_RetTime[1];
            var From_RetAM = From_RetTime[2];
            $("#Ret_Hour option:selected").text(From_RetHour)
            $("#Ret_Minutes option:selected").text(From_RetMin)
            $("#Ret_AM_PM option:selected").text(From_RetAM)

            $("#RetDivider").show();
            $("#Ret_Date").show();
            $("#Ret_Timings").show();
        }
        else {
            $("#Ret_Date").hide();
            $("#Ret_Timings").hide();
            $("#RetDivider").hide();
        }
    }
    $("#datepicker").val(PickupDate);
    $("#Chk_Terms").change(function () {
        debugger;
        Chk_Terms = ($(this).is(":checked"));
    });
})

function ChildCar() {
    debugger;
    var Select_ChildCarSheet = $('#Select_ChildCarSheet').val()
    if (Select_ChildCarSheet == "Y") {
        var Total = $('#Total').text();
        Total = (parseFloat(Total) + 10).toFixed(2);
        $('#Total').text(Total);
    }
    else {
        var Total = $('#Total').text();
        Total = (parseFloat(Total) - 10).toFixed(2);
        $('#Total').text(Total);
    }
}

var IncBit = false;
var arrPrice, SubPrice = 0;
function HourChangeEvent() {
    debugger;

    var sel_Hour = $('#sel_Hour').val()
    if (sel_Hour == "23" || sel_Hour == "00" || sel_Hour == "01" || sel_Hour == "02" || sel_Hour == "03" || sel_Hour == "04" || sel_Hour == "05") {
        IncBit = true;
        var Total = $('#Total').text();
        Total = (parseFloat(Total) + 10).toFixed(2);
        $('#Total').text(Total);
    }
    else {
        if (IncBit) {
            IncBit = false
            var Total = $('#Total').text();
            Total = (parseFloat(Total) - 10).toFixed(2);
            $('#Total').text(Total);
        }
    }
}

function HourChangeEventOnFinal() {
    debugger;

    var sel_Hour = $('#Pickup_Hour').val();
    var am_pm = $('#Pickup_AM_PM').val();
    var Total = $('#SubTotal').text().split(' ')[1];
    var LateNight = $('#Late_NightCharge').text().split(' ')[1];
    if (sel_Hour == "12" && am_pm == "AM")
    {
        sel_Hour = "00";
    }
    if (am_pm == 'PM')
    {
        sel_Hour = (parseInt(sel_Hour) + 12);
    }

    if (sel_Hour == "23" || sel_Hour == "00" || sel_Hour == "01" || sel_Hour == "02" || sel_Hour == "03" || sel_Hour == "04" || sel_Hour == "05") {
        $('#Late_NightCharge').text('$ 10')
        IncBit = true;
       //var Total = $('#Total').text();
        Total = (parseFloat(Total) + 10).toFixed(2);
        $('#Total').text('$ '+Total);
    }
    else {
        if (IncBit) {
           $('#Late_NightCharge').text('$ 0')
            IncBit = false
            //var Total = $('#Total').text();
            Total = (parseFloat(Total)).toFixed(2);
            $('#Total').text(Total);
        }
    }
}
var Tab;
function Load() {
    var data = { sid: Sid }

    $.ajax({
        type: "POST",
        url: "../BookingHandler.asmx/GetRate",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.Retcode == 1) {
                debugger;
                Tab = obj.Tab;
                var Img = obj.Img;
                $('#Div_Img').html('<img src=' + Img + ' width="285" height="140" alt="" /><br />')
                
                if (Tab == "1") {
                    
                    $('#RatesTable').empty();;
                    var Content = '';

                    Content += '<li style="font-weight:600">Base Fare<label id="Baserate" style="float:right"></label></li>'
                    Content += '<li style="font-weight:600">Approx Distance <label id="Distance" style="float:right"></label></li>'
                    Content += '<li style="font-weight:600">Cost Per Miles <label id="CPD" style="float:right"></label></li>'
                    Content += ' <li style="font-weight:600">Sub Total <label id="SubTotal" style="float:right"></label></li>'

                    Content += ' <li style="font-weight:600">Return Subtotal <label id="Return_Amount" style="float:right"></label></li>'
                    Content += ' <li style="font-weight:600">Meet & Greet <input type="checkbox" onclick="MeetGreet()" id="Meet_Greet" style="float:right"></li>'
                    Content += ' <li style="font-weight:600">Meet and Greet  <label id="Meet" style="float:right"></label></li>'
                    Content += '<li style="font-weight:600">Late Night Charges <label id="Late_NightCharge" style="float:right"></label></li>'

                    Content += ' <li style="font-weight:600"><input type="text" style="width: 150px; height: 30px; line-height: 30px; padding: 2px 5px; border: 1px solid #ced6dc;" placeholder="Offer Code" id="OfferCode" autocomplete="off"/><input type="button" class="btn-submit" id="btnOffer" onclick="CheckOffer()" value="Apply" style="font-size: 12px;font-weight: 300;padding-bottom: 2px;padding-left: 5px;padding-top: 2px;padding-right: 1px;background-color: #00aaf1;color: white;"><label id="PercentAmount" style="float:right">Not Applied</label></li>'
                    Content += ' <li style="font-weight:600">Apply Promo Code before Gratuity<br />'
                       Content+=' <br />'
                       Content += '<select id="Sel_Stops" onchange="LoadForCalculation();" style="width:67%"><option selected="selected" value="0">Select&nbsp;Gratuity</option><option value="0">0%</option><option value="10">10%</option><option value="15">15%</option><option value="18">18%</option><option value="18">18%</option><option value="20">20%</option><option value="25">25%</option><option value="30">30%</option></select>'
                       Content += ' <label id="Stops" style="float:right"></label>'
                       Content+='<br /><br />'
                       Content+='<label style="font-size:12px">Recommended Gratuity 15%</label>'
                       Content+='<div class="section-header" style="margin-bottom:10px">'
                       Content+='<h3></h3>'
                       Content+='</div>'
                       Content+='</li>'
                       Content += '<li style="font-weight:600">Grand Total <label id="Total" style="float:right"></label></li>'
                      

                    $('#RatesTable').append(Content);
                    
                    //Start Shahid Calculation 
                    var Array = obj.Arr;
                    var Splitter = Distance.split('km');
                    var TravelDistance = Splitter[0];
                    var Baserate = (parseFloat(Array[0].BaseCharge)).toFixed(2);
                    var CostPerMiles = (parseFloat(Array[0].MilesPerDistance)).toFixed(2);
                    var SubTotal = parseFloat(CostPerMiles) * parseFloat(TravelDistance) + parseFloat(Baserate);
                    SubTotal = SubTotal.toFixed(2)
                    SubT = SubTotal;
                    var Total = (parseFloat(SubTotal)).toFixed(2);
                    //End Shahid Calculation 
                    // New Mster Calculation
                    arrPrice = obj.Arr;
                    // End Master Calculation
                    $('#Type').text(Array[0].Name)
                    $('#Distance').text( Splitter[0] + " Miles")
                    $('#Baserate').text("$ " + Baserate)
                    $('#Stops').text("$ 0")
                    $('#CPD').text("$ " + CostPerMiles)
                    $('#SubTotal').text("$ " + SubTotal)
                    if (ChkRet == "true") {
                        $('#Return_Amount').text("$ " + SubTotal)
                    }
                    $('#Total').text("$ " + Total)
                    $('#Sel_Paypal').append('<option value="Option 1">Option 1 $' + Total + ' USD</option>')
                    
                    LateNightCalc(Total)
                    if(Ten==10)
                    {
                        Total = parseFloat(Total) + parseFloat(10);
                        Total = Total.toFixed(2);
                    }
                    if (Tan == 10)
                    {
                        Total = parseFloat(Total) + parseFloat(10);
                        Total = Total.toFixed(2);
                    }
                    GlobalOnLoadTotal = Total;
                }
                else if(Tab == "2")
                {
                    debugger;
                    $('#RatesTable').empty();;
                    var Content = '';

                    Content += '<li style="font-weight:600">Base Fare<label id="Baserate" style="float:right"></label></li>'
                    Content += '<li style="font-weight:600">Approx Distance <label id="Distance" style="float:right"></label></li>'
                    Content += '<li style="font-weight:600">Cost Per Miles <label id="CPD" style="float:right"></label></li>'
                    Content += ' <li style="font-weight:600">Sub Total <label id="SubTotal" style="float:right"></label></li>'
                    Content += '<li style="font-weight:600">Late Night Charges <label id="Late_NightCharge" style="float:right"></label></li>'

                    Content += ' <li style="font-weight:600"><input type="email"  id="email" placeholder="Offer Code"/><button type="submit" class="btn" id="btnOffer" onclick="CheckOffer()">Apply</button><label id="PercentAmount" style="float:right">Not Applied</label></li>'
                    Content += ' <li style="font-weight:600">Apply Promo Code before Gratuity<br />'
                    Content += ' <br />'

                    Content += '<select id="Sel_Stops" onchange="LoadForCalculation();" style="width:67%"><option selected="selected" value="0">Select&nbsp;Gratuity</option><option value="0">0 %</option><option value="10">10% </option><option value="15">15%</option><option value="18">18%</option><option value="20">20%</option><option value="25">25%</option><option value="30">30%</option></select>'
                    Content += ' <label id="Stops" style="float:right">$ 0</label>'
                    Content += '<br /><br />'
                    Content += '<label style="font-size:12px">Recommended Gratuity 15%</label>'
                    Content += '<div class="section-header" style="margin-bottom:10px">'
                    Content += '<h3></h3>'
                    Content += '</div>'
                    Content += '</li>'
                    Content += '<li style="font-weight:600">Grand Total <label id="Total" style="float:right"></label></li>'


                  //  Content += '<table style="width: 100%">';

                  //  Content += '<tr>';
                  //  Content += '<td>Base Rate</td>';
                  //  Content += '<td><label id="Baserate"></label></td>';
                  //  Content += '</tr>';

                    //Content += '<tr>';
                    //Content += '<td>Approx Distance </td>';
                    //Content += '<td><label id="Distance"></label></td>';
                    //Content += '</tr>';

                    //Content += '<tr>';
                    //Content += '<td>Cost Per Miles</td>';
                    //Content += '<td><label id="CPD"></label></td>';
                    //Content += '</tr>';

                    //Content += '<tr>';
                    //Content += '<td>Sub Total</td>';
                    //Content += '<td><label id="SubTotal"></label></td>';
                    //Content += '</tr>';

                    //Content += '<tr>';
                    //Content += '<td>Late Night Charges</td>';
                    //Content += '<td><label id="Late_NightCharge">$ 0</label></td>';
                    //Content += '</tr>';
                   
                    //Content += '<tr>';
                    //Content += '<td><input type="text" style="width: 150px; height: 30px; line-height: 30px; padding: 2px 5px; border: 1px solid #ced6dc;" placeholder="Offer Code" id="OfferCode" autocomplete="off"/><input type="button" class="btn-submit" id="btnOffer" onclick="CheckOffer()" value="Apply" style="font-size: 12px;font-weight: 300;padding-bottom: 2px;padding-left: 5px;padding-top: 2px;padding-right: 1px;background-color: #00aaf1;color: white;"></td>';
                    //Content += '<td><label id="PercentAmount">Not Applied</label></td>';
                    //Content += '</tr>';

                    //Content += '<tr style="background-color: white;">';
                    //Content += '<td>Apply Promo Code before Gratuity</td>';
                    //Content += '</tr>';

                    //Content += '<td><select id="Sel_Stops" onchange="LoadForCalculation();" style="width:80%"  class="form-control"><option selected="selected" value="0">Select Gratuity</option><option  value="0">0 %</option><option  value="10">10% </option><option  value="15">15%</option><option value="20">20%</option><option value="25">25%</option><option value="30">30%</option></select></td>';
                    //Content += '<td><label id="Stops">$ 0</label></td>';
                    //Content += '</tr>';

                    //Content += '<tr style="background-color: white;">';
                    //Content += '<td style="font-size:13px">Recommended Gratuity 15%</td>';
                    //Content += '</tr>';

                    //Content += '</table>';
                    $('#RatesTable').append(Content);

                    //Start Shahid Calculation 
                    var Array = obj.Arr;
                    var Splitter = Distance.split('km');
                    var TravelDistance = Splitter[0];
                    var Baserate = (parseFloat(Array[0].BaseCharge)).toFixed(2);
                    var CostPerMiles = (parseFloat(Array[0].MilesPerDistance)).toFixed(2);
                    var SubTotal = parseFloat(CostPerMiles) * parseFloat(TravelDistance) + parseFloat(Baserate);
                    SubTotal = SubTotal.toFixed(2)
                    SubT = SubTotal;
                    var Total = (parseFloat(SubTotal)).toFixed(2);
                    //End Shahid Calculation 
                    // New Mster Calculation
                    arrPrice = obj.Arr;
                    // End Master Calculation
                    $('#Type').text(Array[0].Name)
                    $('#Distance').text(Splitter[0] + " Miles")
                    $('#Baserate').text("$ " + Baserate)
                    $('#CPD').text("$ " + CostPerMiles)
                    $('#SubTotal').text("$ " + SubTotal)
                    $('#Total').text("$ " + Total)
                    $('#Sel_Paypal').append('<option value="Option 1">Option 1 $' + Total + ' USD</option>')
                    GlobalAmount = Total;
                    LateNightCalc(Total)
                    if (Tan == 10) {
                        Total = parseFloat(Total) + parseFloat(10);
                        Total = Total.toFixed(2);
                    }
                    GlobalOnLoadTotal = Total;
                }
                else if (Tab == "3")
                {
                    $('#RatesTable').empty();;
                    var Content = '';


                    Content += '<li style="font-wight:600">Hourly Rate<label id="Rate" style="float:right"></label></li>'
                    Content += '<li style="font-weight:600">Hours <label  style="float:right">' + " " + Hours + '</label></li>'
                    Content += ' <li style="font-weight:600">Sub Total <label id="SubTotal" style="float:right"></label></li>'

                    Content += ' <li style="font-weight:600"><input type="email"  id="OfferCode" placeholder="Offer Code" autocomplete="off"/><button type="submit" class="btn" id="btnOffer" onclick="CheckOffer()">Apply</button><label id="PercentAmount" style="float:right">Not Applied</label></li>'
                    Content += ' <li style="font-weight:600">Apply Promo Code before Gratuity<br />'
                    Content += ' <br />'

                    Content += '<select id="Sel_Stops" onchange="LoadForCalculation();" style="width:67%"><option selected="selected" value="0">Select&nbsp;Gratuity</option><option value="0">0 %</option><option value="10">10% </option><option value="15">15%</option><option value="18">18%</option><option value="20">20%</option><option value="25">25%</option><option value="30">30%</option></select>'
                    Content += ' <label id="Stops" style="float:right">$ 0</label>'
                    Content += '<br /><br />'
                    Content += '<label style="font-size:12px">Recommended Gratuity 15%</label>'
                    Content += '<div class="section-header" style="margin-bottom:10px">'
                    Content += '<h3></h3>'
                    Content += '</div>'
                    Content += '</li>'
                    Content += '<li style="font-weight:600">Grand Total <label id="Total" style="float:right"></label></li>'





                    //Content += '<table style="width: 100%">';

                    //Content += '<td>Hourly Rate </td>';
                    //Content += '<td><label id="Rate"></label></td>';
                    //Content += '</tr>';

                    //Content += '<tr>';
                    //Content += '<td>Hours</td>';
                    //Content += '<td><label>' + " "+Hours + '</label></td>';
                    //Content += '</tr>';

                    //Content += '<tr>';
                    //Content += '<td>Sub Total</td>';
                    //Content += '<td><label id="SubTotal"></label></td>';
                    //Content += '</tr>';

                    //Content += '<tr>';
                    //Content += '<td><input type="text" style="width: 150px; height: 30px; line-height: 30px; padding: 2px 5px; border: 1px solid #ced6dc;" placeholder="Offer Code" id="OfferCode" autocomplete="off"/><input type="button" class="btn-submit" id="btnOffer" onclick="CheckOffer()" value="Apply" style="font-size: 12px;font-weight: 300;padding-bottom: 2px;padding-left: 5px;padding-top: 2px;padding-right: 1px;background-color: #00aaf1;color: white;"></td>';
                    //Content += '<td><label id="PercentAmount">Not Applied</label></td>';
                    //Content += '</tr>';

                    //Content += '<tr style="background-color: white;">';
                    //Content += '<td>Apply Promo Code before Gratuity</td>';
                    //Content += '</tr>';

                    //Content += '<td><select id="Sel_Stops" onchange="LoadForCalculation();" style="width:80%"  class="form-control"><option selected="selected" value="0">Select Gratuity</option><option  value="0">0 %</option><option  value="10">10% </option><option  value="15">15%</option><option value="20">20%</option><option value="25">25%</option><option value="30">30%</option></select></td>';
                    //Content += '<td><label id="Stops">$ 0</label></td>';
                    //Content += '</tr>';

                    //Content += '<tr style="background-color: white;">';
                    //Content += '<td style="font-size:13px">Recommended Gratuity 15%</td>';
                    //Content += '</tr>';

                    //Content += '</table>';
                    $('#RatesTable').append(Content);

                    var CPDTotal
                    var Array = obj.Arr;
                    var Splitter = Distance.split('km');
                    var TravelDistance = Splitter[0]

                    var Baserate = (parseFloat(Array[0].HourlyRate)).toFixed(2);

                    var TotalRate = parseFloat(Array[0].HourlyRate) * parseFloat(Hours);

                    var BaseDistance = (parseFloat(Array[0].BaseDistance)).toFixed(2);
                    // New Mster Calculation
                    arrPrice = obj.Arr;
                    // End Master Calculation
                    var Stops = $('#Sel_Stops').val();
                    var Gratuality = (parseFloat(Stops) * parseFloat(TotalRate)) / 100
                    $('#Stops').text("$ " + Gratuality.toFixed(2))
                    var SubTotal = parseFloat(TotalRate)
                    Total = parseFloat(SubTotal) + parseFloat(Gratuality);
                    Total = parseFloat(SubTotal).toFixed(2);
                    GlobalTotal = Total;
                    $('#Type').text(Array[0].Name)
                    $('#Rate').text("$ " + Baserate)
                    $('#TotalRate').text("$ " + TotalRate)
                    $('#SubTotal').text("$ " + SubTotal.toFixed(2))
                    $('#Total').text("$ " + Total)
                    $('#txt_paypal').val("$ " + Total)
                    $('#Sel_Paypal').append('<option value="Option 1">Option 1 $' + Total + ' USD</option>')
                }

                if (Tab == "4") {
                    debugger
                    Adult = GetQueryStringParams('Adult');
                    Child = GetQueryStringParams('Child');
                    var ChL5 = GetQueryStringParams('ChildL5');
                    //Child = Child + "^" + ChL5;
                    $('#RatesTable').empty();;
                    var Content = '';

                    Content += '<li style="font-weight:600">Base Fare<label id="Baserate" style="float:right"></label></li>'
                    Content += '<li style="font-weight:600">Approx Distance <label id="Distance" style="float:right"></label></li>'
                    Content += '<li style="font-weight:600">Cost Per Miles <label id="CPD" style="float:right"></label></li>'
                    Content += ' <li style="font-weight:600">Sub Total <label id="SubTotal" style="float:right"></label></li>'

                    Content += ' <li style="font-weight:600">Return Subtotal <label id="Return_Amount" style="float:right"></label></li>'
                    Content += ' <li style="font-weight:600">Meet & Greet <input type="checkbox" onclick="MeetGreet()" id="Meet_Greet" style="float:right"></li>'
                    Content += ' <li style="font-weight:600">Meet and Greet  <label id="Meet" style="float:right"></label></li>'
                    Content += '<li style="font-weight:600">Late Night Charges <label id="Late_NightCharge" style="float:right"></label></li>'

                    Content += ' <li style="font-weight:600"><input type="email"  id="email" placeholder="Offer Code"/><button type="submit" class="btn">Apply</button><label id="PercentAmount" style="float:right">Not Applied</label></li>'
                    Content += ' <li style="font-weight:600">Apply Promo Code before Gratuity<br />'
                    Content += ' <br />'
                    Content += '<select id="Sel_Stops" onchange="LoadForCalculation();" style="width:67%"><option selected="selected" value="0">Select&nbsp;Gratuity</option><option value="0">0 %</option><option value="10">10% </option><option value="15">15%</option><option value="18">18%</option><option value="20">20%</option><option value="25">25%</option><option value="30">30%</option></select>'
                    Content += ' <label id="Stops" style="float:right"></label>'
                    Content += '<br /><br />'
                    Content += '<label style="font-size:12px">Recommended Gratuity 15%</label>'
                    Content += '<div class="section-header" style="margin-bottom:10px">'
                    Content += '<h3></h3>'
                    Content += '</div>'
                    Content += '</li>'
                    Content += '<li style="font-weight:600">Grand Total <label id="Total" style="float:right"></label></li>'
                    Content += '<div class="section-header" style="margin-bottom:10px"><h3></h3></div>'
                    Content += '<label style="font-size:14px">Passenger Calculation</label>'
                    debugger
                    var SortArrPrice = obj.Arr;
                    var Splitter = Distance.split('km');
                    var TravelDistance = Splitter[0];
                    var Price = 0;
                    var BaseRate = 0;
                    if (Person != 0) {
                        Price = parseFloat(SortArrPrice[0].BaseCharge) + parseFloat(TravelDistance) * parseFloat(SortArrPrice[0].MilesPerDistance);
                        Price = Price.toFixed(2);
                        //BaseRate = parseFloat(20) + parseFloat(Price);
                        BaseRate = parseFloat(Price);
                    }
                    var personPrice = 0;

                    for (i = 0; i < Adult; i++) {
                        if (i == 0) {
                            //personPrice = parseFloat(20) + parseFloat(Price);          
                            personPrice = parseFloat(Price);
                        }
                        else {
                            personPrice += parseFloat(15);
                        }
                    }
                    for (i = 0; i < Child; i++) {
                        personPrice += parseFloat(7);
                    }


                    var AddPrice = parseFloat(personPrice) - parseFloat(BaseRate);
                    Content += '<ul class="cp-meta-listed">'
                    if (Person == 0)
                        Content += '<li>Rate for 0 passenger  <strong>$  ' + BaseRate.toFixed(2) + '</strong></li>'
                   
                    else
                        Content += '<small>Rate for 1 passenger </small> <strong style="margin-left:35px">$ ' + BaseRate.toFixed(2) + '</strong>'
                    Content += '</ul>'
                    Content += '<ul>'
                    if (Person == 0)
                        Content += 'Additional Rate for 0 passenger <strong>$  ' + AddPrice.toFixed(2) + '</strong>'
                    else
                        Content += '<small>Additional ' + parseFloat(Adult - 1) + ' Adult & ' + parseFloat(Child) + ' Child </small><strong style="margin-left:28px">$ ' + AddPrice.toFixed(2) + '</strong><br>'
                    if (Person == 0)
                        Content += 'Additional Rate for 0 passenger <strong>$  ' + AddPrice.toFixed(2) + '</strong>'
                    else
                        Content += '<small>Additional ' + parseFloat(ChL5) + ' Child Below 5 Yrs </small><strong style="margin-left:15px">$ ' + 0 + '</strong>'

                    Content += '</ul>'
                  

                    $('#RatesTable').append(Content);
                    //Start Azhar Calculation 

                    var Array = obj.Arr;
                    var Baserate = (parseFloat(Array[0].BaseCharge)).toFixed(2);
                    var CostPerMiles = (parseFloat(Array[0].MilesPerDistance)).toFixed(2);
                    var SubTotal = parseFloat(Baserate);
                    SubTotal = SubTotal.toFixed(2);

                    var personPrice = 0;

                    for (i = 0; i < Adult; i++) {
                        if (i == 0) {
                            //personPrice = parseFloat(20) + parseFloat(Price);          
                            personPrice = parseFloat(Price);
                        }
                        else {
                            personPrice += parseFloat(15);
                        }
                    }
                    for (i = 0; i < Child; i++) {
                        personPrice += parseFloat(7);
                    }

                    personPrice = personPrice.toFixed(2);

                    SubT = personPrice;
                    var Total = (parseFloat(personPrice)).toFixed(2);
                    //End Azhar Calculation 

                    //Start Shahid Calculation 
                    //var Array = obj.Arr;
                    
                    //var Baserate = (parseFloat(Array[0].BaseCharge)).toFixed(2);
                    //var CostPerMiles = (parseFloat(Array[0].MilesPerDistance)).toFixed(2);
                    //var SubTotal = parseFloat(Baserate);
                    //SubTotal = SubTotal.toFixed(2);

                    //var personPrice = 0;

                    //for (i = 0; i < Person; i++) {
                    //    if (i == 0) {
                    //        personPrice = parseFloat(20) + parseFloat(SubTotal);

                    //    }
                    //    else if (i == 1) {
                    //        personPrice += parseFloat(15);
                    //    }
                    //    else {
                    //        personPrice += parseFloat(7);
                    //    }
                    //}
                    //personPrice = personPrice.toFixed(2);

                    //SubT = personPrice;
                    //var Total = (parseFloat(personPrice)).toFixed(2);
                    //End Shahid Calculation 
                    // New Mster Calculation
                    arrPrice = obj.Arr;
                    // End Master Calculation
                    $('#Type').text(Array[0].Name)
                    $('#Distance').text(Splitter[0] + " Miles")
                    $('#Baserate').text("$ " + Baserate)
                    $('#Stops').text("$ 0")
                    $('#CPD').text("$ " + CostPerMiles)
                    $('#SubTotal').text("$ " + personPrice)
                    if (ChkRet == "true") {
                        $('#Return_Amount').text("$ " + personPrice)
                    }
                    $('#Total').text("$ " + Total)
                    $('#Sel_Paypal').append('<option value="Option 1">Option 1 $' + Total + ' USD</option>')

                    LateNightCalc(Total)
                    if (Ten == 10) {
                        Total = parseFloat(Total) + parseFloat(10);
                        Total = Total.toFixed(2);
                    }
                    if (Tan == 10) {
                        Total = parseFloat(Total) + parseFloat(10);
                        Total = Total.toFixed(2);
                    }
                    GlobalOnLoadTotal = Total;
                }
            }

            else if (obj.Retcode == 2) {
                $('#SpnMessege').text("No Record Found")
                $('#ModelMessege').modal('show')
            }

            else if (obj.Retcode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
    
}
var SubT;
var GratuityPercent=0;
var GratuityAmount = 0;

function LoadForCalculation() {
   
    debugger;
    var Stops = $('#Sel_Stops').val();
    GratuityPercent = Stops;
    var Tot = $("#SubTotal").text();
    Tot = Tot.split(' ');
    if (Tab == "1") {
        //Start Shahid Calculation 
        $('#Sel_Stops').val(Stops);
        var SubTotal = Tot[1];
        var Gratuality = (parseFloat(Stops) * parseFloat(Tot[1])) / 100;
        if (ChkRet == "true") {
            Gratuality = Gratuality * 2;
            SubTotal = SubTotal * 2;
        }
        GratuityAmount = Gratuality
        SubTotal = parseFloat(SubTotal) + parseFloat(Gratuality);
        SubTotal = SubTotal.toFixed(2)
        if (Meet_Greet == true) {
            $('#Meet_Greet').attr('checked', true);
            $("#Meet").text("$ 10");
            SubTotal = (parseFloat(SubTotal) + parseFloat(10)).toFixed(2);
        }
        Late_Night = 0;
        Late_Night = parseFloat(Tan) + parseFloat(Ten);
        SubTotal = parseFloat(SubTotal) + Late_Night;
        $("#Total").text("$ " + SubTotal.toFixed(2));
        $('#Stops').text("$ " + Gratuality.toFixed(2))
        $('#Sel_Paypal').append('<option value="Option 1">Option 1 $' + Total + ' USD</option>')
    }
    else if (Tab == "2"){

        $('#Sel_Stops').val(Stops);
        var SubTotal = Tot[1];
        GratuityAmount = (parseFloat(Stops) * parseFloat(Tot[1])) / 100;
        GratuityAmount = GratuityAmount
        SubTotal = parseFloat(SubTotal) + parseFloat(GratuityAmount);
        SubTotal = parseFloat(SubTotal) + parseFloat(Tan);
        $("#Total").text("$ " + SubTotal.toFixed(2));
        $('#Stops').text("$ " + GratuityAmount.toFixed(2))
        $('#Sel_Paypal').append('<option value="Option 1">Option 1 $' + Total + ' USD</option>')
    }

    else if (Tab == "3")
    {
        $('#Sel_Stops').val(Stops);
        var SubTotal = Tot[1];
        GratuityAmount = (parseFloat(Stops) * parseFloat(Tot[1])) / 100;
        GratuityAmount = GratuityAmount
        SubTotal = parseFloat(SubTotal) + parseFloat(GratuityAmount);
        $("#Total").text("$ " + SubTotal.toFixed(2));
        $('#Stops').text("$ " + GratuityAmount.toFixed(2))
        $('#Sel_Paypal').append('<option value="Option 1">Option 1 $' + Total + ' USD</option>')
    }
    if (Tab == "4") {
        //Start Shahid Calculation 
        $('#Sel_Stops').val(Stops);
        var SubTotal = Tot[1];
        var Gratuality = (parseFloat(Stops) * parseFloat(Tot[1])) / 100;
        if (ChkRet == "true") {
            Gratuality = Gratuality * 2;
            SubTotal = SubTotal * 2;
        }
        GratuityAmount = Gratuality
        SubTotal = parseFloat(SubTotal) + parseFloat(Gratuality);
        SubTotal = SubTotal.toFixed(2)
        if (Meet_Greet == true) {
            $('#Meet_Greet').attr('checked', true);
            $("#Meet").text("$ 10");
            SubTotal = (parseFloat(SubTotal) + parseFloat(10)).toFixed(2);
        }
        Late_Night = 0;
        Late_Night = parseFloat(Tan) + parseFloat(Ten);
        SubTotal = parseFloat(SubTotal) + Late_Night;
        $("#Total").text("$ " + SubTotal.toFixed(2));
        $('#Stops').text("$ " + Gratuality.toFixed(2))
        $('#Sel_Paypal').append('<option value="Option 1">Option 1 $' + Total + ' USD</option>')
    }
}

var Pickup_Location;
var Drop_Location;
var Pickup_Date;
var PickupTime;
var TravelType;
var AirLines;
var FlightNumber;
var FlightTime;
var PhoneNumber;
var AltPhoneNumber;
var txt_Person;
var Ret_Date;
var Ret_Time;
var TotalFare;
var Ret_Amount = 0;
var Late_Night = 0;
var Late_NightRet = 0;

var Ret_FlightNo;
var Ret_Airline;
var Ret_FlightTimes;

var CardNumber;
var Month;
var Year;
var CVV;
var Security_Code;
var CcType, CvNumber;
var OfferCode = "";
var DiscountPrice = 0;
var Remark = "";
function Submit() {
    debugger;

    TotalFare = $('#Total').text();
    TotalFare = TotalFare.split(' ');
    TotalFare = TotalFare[1];
    Pickup_Location = $("#Pickup_Location").val();
    Drop_Location = $("#Drop_Location").val();
    Pickup_Date = $('#datepicker').val();

    txt_Person = $("#Passenger").val();
    //var Pickup_Hour = $('#Pickup_Hour').val();
    //var Pickup_Minutes = $("#Pickup_Minutes").val();
    //var Pickup_AM = $("#Pickup_AM_PM").val();

    var Pickup_Date = $('#datepicker').val();
    
    CardNumber = $("#CardsNumber").val();
    Month = $("#Month option:selected").text();
    Year = $("#Year option:selected").text();
    //CVV = $("#CardsNumber").val();
    Security_Code = $("#Security_Code").val();

    if (CardNumber == "") {
        //$('#SpnMessege').text("Please Insert Credit Card Number")
        //$('#ModelMessege').modal('show')
        alert("Please Insert Credit Card Number");
        return false;
    }
    if (Security_Code == "") {
        //$('#SpnMessege').text("Please Insert Security Code")
        //$('#ModelMessege').modal('show')
        alert("Please Insert Security Code");
        return false;
    }
    //if (serv == "From Airport" || serv == "To Airport")
    //{
    //    TotalFare = $("#SubTotal").val()
    //    //alert(TotalFare)
    //}

    if (serv == "From Airport")
    {
        FlightTime = $('#Pickup_Hour option:selected').text() + ":" + $("#Pickup_Minutes option:selected").text() + ":" + $("#Pickup_AM_PM option:selected").text();

        PickupTime = "";

        AirLines = $('#Flight_From').val();
        FlightNumber = $('#FlightNo_From').val();
        if(ChkRet=="true")
        {
            Ret_Date = $("#Return_Date").val();
            Ret_Time = $("#Ret_Hour option:selected").text() + ":" + $("#Ret_Minutes option:selected").text() + ":" + $("#Ret_AM_PM option:selected").text();
            Ret_FlightNo = "";
            Ret_Airline = "";
            Ret_FlightTimes = "";
        }
    }
    else
    {
        PickupTime = $('#Pickup_Hour option:selected').text() + ":" + $("#Pickup_Minutes option:selected").text() + ":" + $("#Pickup_AM_PM option:selected").text();
        FlightTime = "";
        AirLines = "";
        FlightNumber = "";
        if (ChkRet == "true")
        {
            Ret_Date = $("#Return_Date").val();
            Ret_FlightTimes = $("#Ret_Hour option:selected").text() + ":" + $("#Ret_Minutes option:selected").text() + ":" + $("#Ret_AM_PM option:selected").text();
            Ret_FlightNo = $("#Ret_FlightNo").val();
            Ret_Airline = $("#Ret_Airline").val();
            Ret_Time = "";
        }
    }
    if (GlobalDate == undefined) {
        GlobalDate = "";
    }

    if(Tab != "2")
    {
        Stop1 = "";
        Stop2 = "";
        Stop3 = "";
        Stop4 = "";
        Stop5 = "";
    }
    debugger;
   
    var Splitter = $("#Total").text();
    var Sub = $("#SubTotal").text();
    Sub = Sub.split(' ');
    Splitter = Splitter.split(' ');
    GlobalTotal = Splitter[1];
    if (GratuityPercent != 0 && ChkRet == "true") {
        if (GratuityPercent != 0) {
            GratuityAmount = parseFloat(GratuityAmount) / parseFloat(2);
        }
        GlobalTotalRet = parseFloat(Sub[1]) + parseFloat(GratuityAmount) + parseFloat(Ten);
        GlobalTotal = parseFloat(Splitter[1]) - parseFloat(GlobalTotalRet);
    }
        
    //var Amount1 = 0;
    //Amount1 = $("#Total").text();
    //Amount1 = Amount1.split(' ');
    //Amount1 = Amount1[1];
    if (ChkRet == "true") {
        DiscountPrice = DiscountPrice / 2;
        GlobalTotalRet = parseFloat(Sub[1]) + parseFloat(Ten);
        GlobalTotal = parseFloat(Splitter[1]) - parseFloat(GlobalTotalRet);
    }
    var Cost = Splitter[1];
    if (FName == "TEST" && LName == "TESTED" && PhoneNumber == "1234567890" && AltPhoneNumber == "1234567890" && Email == "shahidanwar888@gmail.com")
        Cost = 0.1;
    var Payment = {
        CardNumber: CardNumber,
        Month: Month,
        Year: Year,
        Security_Code: Security_Code,
        Amount: Cost
    }
    if (Tab == "2")
    {
        serv = "Point To Point Reservation";
    }
    if (Tab == "3")
    {
        GlobalDistance = "";
        GlobalTime = "";
        serv = "Hourly Reservation"
        GlobalTo = "";
    }
    if (Tab == "4")
    {       
        if (serv == "From Airport")
        {
            serv = "From Airport Shuttle";
        }
        else if (serv == "To Airport")
        {
            serv = "To Airport Shuttle";
        }
    }
    $("#CircleImage").show();
    Remark = $("#txtRemark").val();
    $.ajax({
        type: "POST",
        url: "../PayPalHandler.asmx/Paypal",
        data: JSON.stringify(Payment),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            //var S = "Transaction Successful.";
             if (response.d == "Transaction Successful.")
            //if (S == "Transaction Successful.")
            {
                setTimeout(function () {
                    var data = {

                        ResDate: Pickup_Date,
                        Pickup_Location: Drop_Location,
                        Drop_Location: Pickup_Location,
                        PickupTime: PickupTime,
                        txt_Person: txt_Person,
                        AirLines: AirLines,
                        FlightNumber: FlightNumber,
                        FlightTime: FlightTime,
                        //FlightArriving: FlightArriving,
                        //DestinationAdd: DestinationAdd,
                        FName: FName,
                        LName: LName,
                        //Countrycode: Countrycode,
                        PhoneNumber: PhoneNumber,
                        AltPhoneNumber: AltPhoneNumber,
                        Email: Email,
                        //TotalFare: TotalFare,
                        TotalFare: GlobalTotal,
                        From: GlobalFrom,
                        To: GlobalTo,
                        ApproxDistance: GlobalDistance,
                        ApproxTime: GlobalTime,
                        //TravelDate: GlobalDate,
                        //RDate: RDate,
                        //Grad: Grad,
                        Service: serv,
                        //Fare: fare,
                        Stop1: Stop1,
                        Stop2: Stop2,
                        Stop3: Stop3,
                        Stop4: Stop4,
                        Stop5: Stop5,
                        TotalAmount: Splitter[1],
                        MeetGreet: Meet_Greet,
                        GratuityPercent: GratuityPercent,
                        GratuityAmount: GratuityAmount.toFixed(2),
                        CcType: $("#sel_CcType").val(),
                        CvNumber: $("#Security_Code").val(),
                        OfferCode: OfferCode,
                        DiscountPrice: DiscountPrice,
                        OfferPercent: OfferPercent,
                        Remark: Remark,
                        Adult: Adult,
                        Child: Child,
                        AccNo: CardNumber
                    }
                    $.ajax({
                        type: "POST",
                        url: "../BookingHandler.asmx/BookingSuccess",
                        data: JSON.stringify(data),
                        contentType: "application/json",
                        datatype: "json",
                        success: function (response) {

                            var obj = JSON.parse(response.d)
                            if (obj.Retcode == 1) {

                                var ID = obj.ID;
                                if (serv == "From Airport") {
                                    serv = "To Airport"
                                }

                                else if (serv == "To Airport") {
                                    serv = "From Airport"
                                    FlightTime = $("#Ret_Hour").val() + ":" + $("#Ret_Minutes").val() + $("#Ret_AM_PM").val()
                                }

                                if (ChkRet == "true") {

                                    TotalFare = $("#Return_Amount").text()
                                    TotalFare = TotalFare.split(' ')[1]
                                    var data2 = {

                                        ResDate: $("#Return_Date").val(),
                                        Pickup_Location: Pickup_Location,
                                        Drop_Location: Drop_Location,
                                        PickupTime: PickupTime,
                                        txt_Person: txt_Person,
                                        AirLines: AirLines,
                                        FlightNumber: FlightNumber,
                                        FlightTime: FlightTime,
                                        //FlightArriving: FlightArriving,
                                        //DestinationAdd: DestinationAdd,
                                        FName: FName,
                                        LName: LName,
                                        //Countrycode: Countrycode,
                                        PhoneNumber: PhoneNumber,
                                        AltPhoneNumber: AltPhoneNumber,
                                        Email: Email,
                                        //TotalFare: TotalFare,
                                        TotalFare: GlobalTotalRet,
                                        From: GlobalFrom,
                                        To: GlobalTo,
                                        ApproxDistance: GlobalDistance,
                                        ApproxTime: GlobalTime,
                                        TravelDate: GlobalDate,
                                        //RDate: RDate,
                                        //Grad: Grad,
                                        Service: serv,
                                        //Fare: fare,
                                        Stop1: Stop1,
                                        Stop2: Stop2,
                                        Stop3: Stop3,
                                        Stop4: Stop4,
                                        Stop5: Stop5,
                                        Ret_Date: Ret_Date,
                                        Ret_Time: Ret_Time,
                                        Ret_FlightNumber: Ret_FlightNo,
                                        Ret_Airlines: Ret_Airline,
                                        Ret_FlightTime: Ret_FlightTimes,
                                        MeetGreet: Meet_Greet,
                                        GratuityPercent: GratuityPercent,
                                        GratuityAmount: GratuityAmount.toFixed(2),
                                        CcType: $("#sel_CcType").val(),
                                        CvNumber: $("#Security_Code").val(),
                                        OfferCode: OfferCode,
                                        DiscountPrice: DiscountPrice,
                                        OfferPercent: OfferPercent,
                                        Remark: Remark,
                                        Adult: Adult,
                                        Child: Child,
                                        AccNo: CardNumber
                                    }
                                    $.ajax({
                                        type: "POST",
                                        url: "../BookingHandler.asmx/BookingSuccessReturn",
                                        data: JSON.stringify(data2),
                                        contentType: "application/json",
                                        datatype: "json",
                                        success: function (response) {

                                            var obj = JSON.parse(response.d)
                                            if (obj.Retcode == 1) {
                                                //  var ID = obj.ID;
                                                $("#CircleImage").hide();
                                                alert("Reservation Done Successfully.")
                                                window.location.href = "ConfirmBooking.html?ID=" + ID
                                            }
                                            else {
                                                $('#SpnMessege').text("Reservation unsuccessfull.")
                                                $('#ModelMessege').modal('show')
                                            }
                                        },
                                        error: function () {

                                            $('#SpnMessege').text("Something Went Wrong.")
                                            $('#ModelMessege').modal('show')
                                        },
                                    });
                                }
                                else {
                                    $("#CircleImage").hide();
                                    alert("Reservation Done Successfully.")
                                    window.location.href = "ConfirmBooking.html?ID=" + ID
                                }
                            }
                            else if (obj.Retcode == 3) {
                                $('#SpnMessege').text("Entered Email Address is not Registered")
                                $('#ModelMessege').modal('show')
                            }
                            else if (obj.Retcode == 2) {
                                $("#CircleImage").hide();
                                $('#SpnMessege').text("Reservation unsuccessfull.")
                                $('#ModelMessege').modal('show')
                            }
                        },
                        error: function () {
                            $("#CircleImage").hide();
                            $('#SpnMessege').text("Something Went Wrong.")
                            $('#ModelMessege').modal('show')
                        },
                    });
                },1200);
            }
            else {
                $("#CircleImage").hide();
                $('#SpnMessege').text(response.d)
                $('#ModelMessege').modal('show')
            }
        }

    })
}

var OfferPercent = 0;

function CheckOffer()
{
    OfferCode = $("#OfferCode").val();
    debugger;
    if (OfferCode == "") {
        $('#SpnMessege').text("Please Enter Promo Code")
        $('#ModelMessege').modal('show')
        return false;
    }
    var MyData = { OfferCode: OfferCode };
    $.ajax({
        type: "POST",
        url: "../BookingHandler.asmx/CheckOffer",
        data: JSON.stringify(MyData),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var Div = "";
            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                $('#btnOffer').css({
                    display: 'none'
                });
                //$("#").empty();

                var Offerstbl = obj.Offerstbl;
                OfferPercent = Offerstbl[0].Percents;
                OfferCode = Offerstbl[0].Sid;
                var SubPrice = $("#SubTotal").text()
                SubPrice = SubPrice.split(' ');
                var PercentAmount = (parseFloat(SubPrice[1]) / 100) * parseFloat(Offerstbl[0].Percents);
                DiscountPrice = parseFloat(SubPrice[1]) - parseFloat(PercentAmount);
                $("#SubTotal").text("$ " + DiscountPrice.toFixed(2));
                $("#PercentAmount").text("Applied");
                if (ChkRet == "true") {
                    $("#Return_Amount").text("$ " + DiscountPrice.toFixed(2));
                    DiscountPrice = DiscountPrice + DiscountPrice;
                }
                GlobalTotal = (parseFloat(DiscountPrice) + parseFloat(Late_Night));
                if (Meet_Greet == true) {
                    $('#Meet_Greet').attr('checked', true);
                    GlobalTotal = parseFloat(GlobalTotal) + parseFloat(10);
                }
                $("#Total").text("$ " + GlobalTotal.toFixed(2));
                $("#txt_BaseAmt").val(GlobalTotal.toFixed(2))
            }
            else {
                alert("This Offer Code is not available");
            }
        },
    });
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function GetAirLines() {

    $.ajax({
        url: "../Admin/ReservationRateHandler.asmx/GetAirLines",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);

            if (obj.Retcode == 1) {
                var Driver = obj.Arr;
                debugger;
                var OptionMac_Name = null;

                if (obj.Retcode == 1) {
                    var Driver = obj.Arr;
                    for (var i = 0; i < Driver.length; i++) {

                        OptionMac_Name += '<option value="' + Driver[i].Callsign + '" >' + Driver[i].Callsign + '</option>'
                    }

                    $("#Select_Airlines").append(OptionMac_Name);
                }
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}

function Login() {
    var txt_Username = $('#txt_Username').val();
    var txt_Password = $('#txt_Password').val();

    data = { txt_Username: txt_Username, txt_Password: txt_Password }
    $.ajax({
        type: "POST",
        url: "../BookingHandler.asmx/CustomerLogin",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {

                $('#txt_First').val(obj.FirstName)
                $('#txt_Last').val(obj.LastName)
                $('#PhoneNumber').val(obj.Mobile)
                $('#Email').val(obj.Email)
            }
            else {
                $('#SpnMessege').text("Please Create Account")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });
}

function RetReservationChange() {
    if ($("#ChkRetReservation").is(':not(:checked)')) {
        $("#Return").hide();
    }
    else {
        $("#Return").show();
        //$('#TravelType').show();
        $('#Airlines').show();
        $('#FlightNo').show();
        $('#ArrivalTime').show();
        $('#ArrivingFrom').show();
        $("#FlightDate").show();
    }
}

function Package() {
    if (serv == "From Airport") {
        var FlightHour = $('#Pickup_Hour option:selected').text();
        var FlightAM = $("#Pickup_AM_PM option:selected").text();
        var FlightMin = $("#Pickup_Minutes option:selected").text();
        if (FlightHour == "11" && FlightAM == "PM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (FlightHour == "12" && FlightAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (FlightHour == "00" && FlightAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (FlightHour == "01" && FlightAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (FlightHour == "02" && FlightAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (FlightHour == "03" && FlightAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (FlightHour == "04" && FlightAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (FlightHour == "05" && FlightMin == "00" && FlightAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        FlightTime = $('#Pickup_Hour option:selected').text() + ":" + $("#Pickup_Minutes option:selected").text() + ":" + $("#Pickup_AM_PM option:selected").text();

        PickupTime = "";

        AirLines = $('#Airlines_From').val();
        FlightNumber = $('#FlightNo_From').val();

    }
    else if (serv == "To Airport") {
        var PickupHour = $('#Pickup_Hour option:selected').text();
        var PickupAM = $("#Pickup_AM_PM option:selected").text();
        var PickupMin = $("#Pickup_Minutes option:selected").text();
        if (PickupHour == "11" && PickupAM == "PM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (PickupHour == "12" && PickupAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (PickupHour == "00" && PickupAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (PickupHour == "01" && PickupAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (PickupHour == "02" && PickupAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (PickupHour == "03" && PickupAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (PickupHour == "04" && PickupAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        else if (PickupHour == "05" && PickupMin == "00" && PickupAM == "AM") {
            TotalFare = parseFloat(TotalFare) + parseFloat(10);
            Late_Night = 10;
        }
        PickupTime = $('#Pickup_Hour option:selected').text() + ":" + $("#Pickup_Minutes option:selected").text() + ":" + $("#Pickup_AM_PM option:selected").text();
        FlightTime = "";
        AirLines = "";
        FlightNumber = "";
    }
    //if(Meet_Greet == true)
    //{
    //    TotalFare = parseFloat(TotalFare) + parseFloat(10);
    //}
}

function ReturnPackage() {
    if (serv == "To Airport") {
        if (Late_Night == 10) {
            Ret_Amount = parseFloat(TotalFare) - parseFloat(10);
            Late_NightRet = 10;
        }
        else {
            Ret_Amount = parseFloat(TotalFare);
        }
        var Ret_FlightHour = $('#Ret_Hour option:selected').text();
        var Ret_FlightAM = $("#Ret_AM_PM option:selected").text();
        var Ret_FlightMin = $("#Ret_Minutes option:selected").text();

        if (Ret_FlightHour == "11" && Ret_FlightAM == "PM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_FlightHour == "12" && Ret_FlightAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_FlightHour == "00" && Ret_FlightAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_FlightHour == "01" && Ret_FlightAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_FlightHour == "02" && Ret_FlightAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_FlightHour == "03" && Ret_FlightAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_FlightHour == "04" && Ret_FlightAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_FlightHour == "05" && Ret_FlightMin == "00" && Ret_FlightAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        Ret_Date = $("#Return_Date").val();
        Ret_FlightTimes = $("#Ret_Hour option:selected").text() + ":" + $("#Ret_Minutes option:selected").text() + ":" + $("#Ret_AM_PM option:selected").text();

        Ret_FlightNo = $("#Ret_FlightNo").val();
        Ret_Airline = $("#Ret_Airline").val();
        Ret_Time = "";
    }
    else if (serv == "From Airport") {

        Ret_Time = $("#Ret_Hour option:selected").text() + ":" + $("#Ret_Minutes option:selected").text() + ":" + $("#Ret_AM_PM option:selected").text();
        if (Late_Night == 10) {
            Ret_Amount = parseFloat(TotalFare) - parseFloat(10);
        }
        else {
            Ret_Amount = parseFloat(TotalFare);
        }
        var Ret_TimeHour = $('#Ret_Hour option:selected').text();
        var Ret_TimeAM = $("#Ret_AM_PM option:selected").text();
        var Ret_TimeMin = $("#Ret_Minutes option:selected").text();
        if (Ret_TimeHour == "11" && Ret_TimeAM == "PM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_TimeHour == "00" && Ret_TimeAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_TimeHour == "12" && Ret_TimeAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_TimeHour == "01" && Ret_TimeAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_TimeHour == "02" && Ret_TimeAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_TimeHour == "03" && Ret_TimeAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_TimeHour == "04" && Ret_TimeAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        else if (Ret_TimeHour == "05" && Ret_TimeMin == "00" && Ret_TimeAM == "AM") {
            Ret_Amount = parseFloat(Ret_Amount) + parseFloat(10);
            Late_NightRet = 10;
        }
        Ret_Date = $("#Return_Date").val();
        Ret_FlightNo = "";
        Ret_Airline = "";
        Ret_FlightTimes = "";
    }
}

function MeetGreet() {
    if ($("#Meet_Greet").is(':not(:checked)')) {
        if (Meet_Greet == true) {
            Meet_Greet = false;
            // var TotalPrice = $("#txt_BaseAmt").val();
            var TotalPrice = $("#Total").text();
            TotalPrice = TotalPrice.split(' ');
            var Tot = TotalPrice[1];
            //var Tot = 0
            Tot = parseFloat(Tot) - parseFloat(10);
            Tot = Tot.toFixed(2);
            //GlobalTotal = Tot;
            //GlobalOnLoadTotal = Tot;
            $("#Meet").text("$ 0");
            $("#Total").text("$ " + Tot);
        }
    }
    else {
        Meet_Greet = true;
        var TotalPrice = $("#Total").text();
        //var TotalPrice = $("#txt_BaseAmt").val();
        TotalPrice = TotalPrice.split(' ');
        var Tot = TotalPrice[1];
        //var Tot =0
        Tot = parseFloat(Tot) + parseFloat(10);
        Tot = Tot.toFixed(2);
        //GlobalOnLoadTotal = Tot;
        //GlobalTotal = Tot;
        $("#Meet").text("$ 10");
        $("#Total").text("$ " + Tot);
    }
}

function ConfirmBooking()
{
    debugger;
    var bValid = ValidateGeneralInfo();

    if (bValid == true)
    {
        //CheckOffer();
        $("#BookingDetails").modal("show");
        $("#Source").text($("#Pickup_Location").val());
        $("#Destination").text($("#Drop_Location").val());
        $("#Passengr").text($("#Passenger").val());
        $("#Pic_Drop_Date").text($("#datepicker").val());
        $("#Pic_Drop_Date").text($("#datepicker").val());
        var Time = $('#Pickup_Hour option:selected').text() + ":" + $("#Pickup_Minutes option:selected").text() + ":" + $("#Pickup_AM_PM option:selected").text();
        $("#Time").text(Time);

        $("#TotalAmount").text("Total Amount: " + $("#Total").text());
        
        $("#Fname").text($('#First_Name').val());
        $("#Lname").text($('#Last_Name').val());
        $("#MobNo").text($('#PhoneNo').val());
        $("#AltMobNo").text($('#AltPhoneNo').val());
        $("#Mail").text($('#Email').val());

        $("#txt_FlightNoFrom").hide();

        $("#FlightNoFrom").hide();
        $("#trAirlinesFrom").hide();
        $("#Ret_DateAndTime").hide();
        $("#Ret_details").hide();

        $("#Ret_FlightDetais").hide();
        $("#Ret_DateAndTime").hide();
        $("#Ret_FlightNumber").hide();
        $("#Ret_Airlines").hide();

        if (serv == "To Airport") {
            Div = '';
            $("#txt_Date").empty();
            Div += "Pickup Date:";
            $("#txt_Date").append(Div);

            Div = '';
            $("#txt_Time").empty();
            Div += "Pickup Time:";
            $("#txt_Time").append(Div);

            if (ChkRet == "true") {
                $("#Ret_DateAndTime").show();
                $("#Ret_details").show();

                Div = '';
                $("#txt_RetTime").empty();
                Div += "Flight Arrival Time:";
                $("#txt_RetTime").append(Div);

                $("#Return_Dates").text($("#Return_Date").val());

                var Ret_FlightTimes = $("#Ret_Hour option:selected").text() + ":" + $("#Ret_Minute option:selected").text() + ":" + $("#Ret_AM_PM option:selected").text();
                $("#RetTime").text(Ret_FlightTimes);

                $("#Return_Flightno").text($("#Ret_FlightNo").val());

                $("#Return_Airlines").text($("#Ret_Airline").val());

                $("#Ret_FlightDetais").show();
                $("#Ret_Date").show();
                $("#Ret_Timings").show();
                $("#Ret_FlightNumber").show();
                $("#Ret_Airlines").show();
            }
            else {

                $("#Ret_Date").hide();
                $("#Ret_Timings").hide();
            }
        }
        else if (serv == "From Airport")
        {
            Div = '';
            $("#txt_Date").empty();
            Div += "Drop Date:";
            $("#txt_Date").append(Div);

            Div = '';
            $("#txt_Time").empty();
            Div += "Flight Arrival Time:";
            $("#txt_Time").append(Div);

            $("#txt_FlightNoFrom").show();
            $("#FlightNoFrom").show();
            $("#FlightNoFrom").text($("#FlightNo_From").val());

            $("#trAirlinesFrom").show();
            $("#AirlinesFrom").text($("#Flight_From").val());


            $("#FlightNum_From").show();
            $("#Airline_From").show();

            if (ChkRet == "true") {
                $("#Ret_DateAndTime").show();

                Div = '';
                $("#txt_RetTime").empty();
                Div += "Return Time:";
                $("#txt_RetTime").append(Div);

                $("#Return_Dates").text($("#Return_Date").val());

                var Ret_FlightTimes = $("#Ret_Hour option:selected").text() + ":" + $("#Ret_Minutes option:selected").text() + ":" + $("#Ret_AM_PM option:selected").text();
                $("#RetTime").text(Ret_FlightTimes);
                $("#Ret_details").show();
                $("#Ret_Date").show();
                $("#Ret_Timings").show();
            }
            else {
                $("#Ret_Date").hide();
                $("#Ret_Timings").hide();
                $("#RetDivider").hide();
            }
        }
        else if(Tab == "2")
        {
            Div = '';
            $("#txt_Time").empty();
            Div += "Pickup Time:";
            $("#txt_Time").append(Div);

            Div = '';
            $("#txt_Date").empty();
            Div += "Pickup Date:";
            $("#txt_Date").append(Div);
        }
        else if (Tab == "3") {
            //$("#notHours").hide();

            Div = '';
            $("#txt_Date").empty();
            Div += "Pickup Date:";
            $("#txt_Date").append(Div);

            Div = '';
            $("#txt_Time").empty();
            Div += "Pickup Time:";
            $("#txt_Time").append(Div);

            Div = '';
            $("#txt_Destination").empty();
            Div += "Hours:";
            $("#txt_Destination").append(Div);
        }
    }
}
var FName;
var LName;
var Email;
function ValidateGeneralInfo()
{
    PhoneNumber = $('#PhoneNo').val();
    AltPhoneNumber = $('#AltPhoneNo').val();
    Email = $('#Email').val();
    FName = $('#First_Name').val();
    LName = $('#Last_Name').val();

    if (FName == '') {
        $('#SpnMessege').text("Please Provide First Name")
        $('#ModelMessege').modal('show')
        return false;
    }
    if (LName == '') {
        $('#SpnMessege').text("Please Provide Last Name")
        $('#ModelMessege').modal('show')
        return false;
    }

    if (PhoneNumber == '') {
        $('#SpnMessege').text("Please Provide Phone Number")
        $('#ModelMessege').modal('show')
        return false;
    }
    //if (AltPhoneNumber == '') {
    //    $('#SpnMessege').text("Please Provide Phone Number")
    //    $('#ModelMessege').modal('show')
    //    return false;
    //}
    //if (Email == '') {
    //    $('#SpnMessege').text("Please Provide Email")
    //    $('#ModelMessege').modal('show')
    //    return false;
    //}
    if (Chk_Terms != true) {
        $('#SpnMessege').text("Please Accept Terms & Condition")
        $('#ModelMessege').modal('show')
        //alert("Please Accept Terms & Condition");
        return false;
    }
    
    //if(Email!='')
    //{
    //    Emailchechking();
    //    if(EmailBool==false)
    //    {
    //        return false;
    //    }
    //}
     
    return true;
}
var EmailBool;
function Emailchechking()
{
    var Email = $('#Email').val();
    var Data = { Email: Email };
    

    if (Email != "")
    {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test(Email))
        {
            $('#SpnMessege').text("Please enter valid Email ID");
            $('#ModelMessege').modal('show');
            return false;
        }
        else
        {
            $.ajax({
                url: "BookingHandler.asmx/Emailchechking",
                type: "POST",
                data: JSON.stringify(Data),
                contentType: "application/json",
                datatype: "json",
                success: function (response) {
                    var obj = JSON.parse(response.d);

                    if (obj.Retcode == 1) {
                        EmailBool = true;
                        ConfirmBooking();
                    }
                    else if (obj.Retcode == 0) {
                        EmailBool = false;
                        $('#SpnMessege').text("Entered Email Address is not registered")
                        $("#ModelMessege").modal("show")
                    }
                },
                error: function () {
                    $('#SpnMessege').text("Somthing went wrong. Please try again.")
                    $("#ModelMessege").modal("show")
                }
            });
        }
    }
    else 
    {
        $('#SpnMessege').text(" Please enter Email Address")
        $("#ModelMessege").modal("show")
    }
}
var Ten = 0;
var Tan = 0;
function LateNightCalc(Price)
{
    debugger;
   
    var FlightHour = $('#Pickup_Hour option:selected').text();
    var FlightAM = $("#Pickup_AM_PM option:selected").text();
    var FlightMin = $("#Pickup_Minutes option:selected").text();

    var Ret_TimeHour = $('#Ret_Hour option:selected').text();
    var Ret_TimeAM = $("#Ret_AM_PM option:selected").text();
    var Ret_TimeMin = $("#Ret_Minutes option:selected").text();

    if (FlightHour == "11" && FlightAM == "PM") {
        Tan = 10;
    }
    else if (FlightHour == "12" && FlightAM == "AM") {
        Tan = 10;
    }
    else if (FlightHour == "00" && FlightAM == "AM") {
        Tan = 10;
    }
    else if (FlightHour == "01" && FlightAM == "AM") {
        Tan = 10;
    }
    else if (FlightHour == "02" && FlightAM == "AM") {
        Tan = 10;
    }
    else if (FlightHour == "03" && FlightAM == "AM") {
        Tan = 10;
    }
    else if (FlightHour == "04" && FlightAM == "AM") {
        Tan = 10;
    }
    else if (FlightHour == "05" && FlightMin == "00" && FlightAM == "AM"){
        Tan = 10;
    }
    if (ChkRet=="true")
    {
        if (Ret_TimeHour == "11" && Ret_TimeAM == "PM") {
            Ten = 10;
        }
        else if (Ret_TimeHour == "00" && Ret_TimeAM == "AM") {
            Ten = 10;
        }
        else if (Ret_TimeHour == "12" && Ret_TimeAM == "AM") {
            Ten = 10;
        }
        else if (Ret_TimeHour == "01" && Ret_TimeAM == "AM") {
            Ten = 10;
        }
        else if (Ret_TimeHour == "02" && Ret_TimeAM == "AM") {
            Ten = 10;
        }
        else if (Ret_TimeHour == "03" && Ret_TimeAM == "AM") {
            Ten = 10;
        }
        else if (Ret_TimeHour == "04" && Ret_TimeAM == "AM") {
            Ten = 10;
        }
        else if (Ret_TimeHour == "05" && Ret_TimeMin == "00" && Ret_TimeAM == "AM") {
            Ten = 10;
        }
        var LateNite
        debugger;
    }
    Late_Night = 0;
    Late_Night = parseFloat(Tan) + parseFloat(Ten);
    $("#Late_NightCharge").text("$ " + Late_Night);
    var sTot = $("#SubTotal").text();
    sTot = sTot.split(' ');
    var SubTotal = sTot[1];
    if (ChkRet == "true") {
        SubTotal = SubTotal * 2;
    }
    var tOT1 = parseFloat(SubTotal) + Late_Night;
    tOT1 = tOT1.toFixed(2);
    $("#Total").text("$ " + tOT1);
}

function TipPrice(Price) {
    debugger;
    //var  Price = $("#Total").text();
    //Price = Price.split(' ');
    //Price = Price[1];
    //TotalFare = $("#Total").text();
    var NightyRate = 0;
    var FlightHour = $('#Pickup_Hour option:selected').text();
    var FlightAM = $("#Pickup_AM_PM option:selected").text();
    var FlightMin = $("#Pickup_Minutes option:selected").text();

    var Ret_TimeHour = $('#Ret_Hour option:selected').text();
    var Ret_TimeAM = $("#Ret_AM_PM option:selected").text();
    var Ret_TimeMin = $("#Ret_Minutes option:selected").text();

    if (FlightHour == "11" && FlightAM == "PM") {
        Price = parseFloat(Price) + parseFloat(10);
        Tan = 10;
        NightyRate += 10;
    }
    else if (FlightHour == "12" && FlightAM == "AM") {
        Price = parseFloat(Price) + parseFloat(10);
        Price = Price.toFixed(2);
        $("#Late_NightCharge").text("$ 10");
        Tan = 10;
        NightyRate += 10;
    }
    else if (FlightHour == "00" && FlightAM == "AM") {
        Price = parseFloat(Price) + parseFloat(10);
        Price = Price.toFixed(2);
        $("#Late_NightCharge").text("$ 10");
        Tan = 10; NightyRate += 10;
    }
    else if (FlightHour == "01" && FlightAM == "AM") {
        Price = parseFloat(Price) + parseFloat(10);
        Price = Price.toFixed(2);
        $("#Late_NightCharge").text("$ 10");
        Tan = 10; NightyRate += 10;
    }
    else if (FlightHour == "02" && FlightAM == "AM") {
        Price = parseFloat(Price) + parseFloat(10);
        $("#Late_NightCharge").text("$ 10");
        Tan = 10; NightyRate += 10;
    }
    else if (FlightHour == "03" && FlightAM == "AM") {
        Price = parseFloat(Price) + parseFloat(10);
        Price = Price.toFixed(2);
        $("#Late_NightCharge").text("$ 10");
        Tan = 10; NightyRate += 10;
    }
    else if (FlightHour == "04" && FlightAM == "AM") {
        Price = parseFloat(Price) + parseFloat(10);
        Price = Price.toFixed(2);
        $("#Late_NightCharge").text("$ 10");
        Tan = 10; NightyRate += 10;
    }
    else if (FlightHour == "05" && FlightMin == "00" && FlightAM == "AM") {
        Price = parseFloat(Price) + parseFloat(10);
        Price = Price.toFixed(2);
        $("#Late_NightCharge").text("$ 10");
        Tan = 10; NightyRate += 10;
    }
    //alert("P " + Price);
    //Price = Price.toFixed(2);
    //$("#SubTotal").text("$ " + Price);
    GlobalTotal = Price;
    //alert(Price);
    //GlobalOnLoadTotal = Price;
    $("#Total").text("$ " + Price);
    if ($("#txt_BaseAmt").val() == "" && ChkRet != "true")
        $("#txt_BaseAmt").val(Price);
    if (ChkRet == "true") {
        var charges = $("#Late_NightCharge").text();
        charges = charges.split(' ');
        charges = charges[1];
        //alert(charges);
        if (charges == 10) {
            charges = 20;
        }
        else {
            charges = 10;
        }

        if (Ret_TimeHour == "11" && Ret_TimeAM == "PM") {
            Price = parseFloat(Price) + parseFloat(10);
            Price.toFixed(2);
            $("#Late_NightCharge").text("$ " + charges);
            Ten = 10;
        }
        else if (Ret_TimeHour == "00" && Ret_TimeAM == "AM") {
            Price = parseFloat(Price) + parseFloat(10);
            Price.toFixed(2);
            $("#Late_NightCharge").text("$ " + charges);
            Ten = 10;
        }
        else if (Ret_TimeHour == "12" && Ret_TimeAM == "AM") {
            Price = parseFloat(Price) + parseFloat(10);
            Price.toFixed(2);
            $("#Late_NightCharge").text("$ " + charges);
            Ten = 10;
        }
        else if (Ret_TimeHour == "01" && Ret_TimeAM == "AM") {
            Price = parseFloat(Price) + parseFloat(10);
            Price.toFixed(2);
            $("#Late_NightCharge").text("$ " + charges);
            Ten = 10;
        }
        else if (Ret_TimeHour == "02" && Ret_TimeAM == "AM") {
            Price = parseFloat(Price) + parseFloat(10);
            Price.toFixed(2);
            $("#Late_NightCharge").text("$ " + charges);
            Ten = 10;
        }
        else if (Ret_TimeHour == "03" && Ret_TimeAM == "AM") {
            Price = parseFloat(Price) + parseFloat(10);
            Price.toFixed(2);
            $("#Late_NightCharge").text("$ " + charges);
            Ten = 10;
        }
        else if (Ret_TimeHour == "04" && Ret_TimeAM == "AM") {
            Price = parseFloat(Price) + parseFloat(10);
            Price.toFixed(2);
            $("#Late_NightCharge").text("$ " + charges);
            Ten = 10;
        }
        else if (Ret_TimeHour == "05" && Ret_TimeMin == "00" && Ret_TimeAM == "AM") {
            Price = parseFloat(Price) + parseFloat(10);
            Price.toFixed(2);
            $("#Late_NightCharge").text("$ " + charges);
            Ten = 10;
        }
        if (Ten == 10) {
            NightyRate += 10;
            GlobalTotalRet = parseFloat(GlobalAmount) + parseFloat(10);
            GlobalTotalRet = GlobalTotalRet.toFixed(2);
        }
        else {
            GlobalTotalRet = GlobalAmount;
        }
        //Price = Price.toFixed(2);
        //alert(GlobalAmount);
        //alert(GlobalTotal);
        $("#Return_Amount").text("$ " + GlobalTotalRet);
        //alert(GlobalTotalRet)
        var t = parseFloat(GlobalTotalRet) + parseFloat(GlobalTotal);
        t = t.toFixed(2);
        //GlobalOnLoadTotal = t;
        $("#Total").text("$ " + t);
        if ($("#txt_BaseAmt").val() == "")
            $("#txt_BaseAmt").val(t);
    }
    //alert($("#txt_BaseAmt").val())
}