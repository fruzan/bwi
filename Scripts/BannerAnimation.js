﻿$(document).ready(function () {

    $.fn.preload = function () {
        this.each(function () {
            $('<img/>')[0].src = this;
        });
    }
    images = Array("index_files/Banner2.jpg",
                       "index_files/Banner1.jpg", "index_files/banner3.jpg");

    lists = Array("Guranteed low Prices",
                       "Comfortable & Reliable",
                       "Not a Traditional Shuttle",
                       "Use SUVs, Mini Vans and Sedans",
                       "No more than <strong>“TWO STOPS”</strong>",
                       "Guranteed low Prices");

    $([images[0], images[1]]).preload();

    $([lists[0], lists[1], lists[2], lists[3], lists[4]]).preload();

    // Usage:



    $('.toggle').each(function () {

        $(this).click(function () {
            //alert('test');
            $(this).parent().parent().find('.navigation').slideToggle('200');

            var media_query_breakpoint = uouMediaQueryBreakpoint();
            if (media_query_breakpoint < 991) {
                $(this).parent().parent().find('.header-language').slideToggle('200');
                $(this).parent().parent().find('.header-cta-buttons').slideToggle('200');
            }
        });
    });


    $('a').click(function (e) {
        //return false;
        //do other stuff when a click happens
    });

    setTimeout(loadimg, 5000);

    $('#non-stop-button').click(function () {
        $("#non-stop-button").addClass("active");
        $("#two-stop-button").removeClass("active");
        $("#sprinter-button").removeClass("active");

        $("#non-stop").addClass("active");
        $("#two-stop").removeClass("active");
        $("#sprinter").removeClass("active");
    });

    $('#two-stop-button').click(function () {
        $("#non-stop-button").removeClass("active");
        $("#two-stop-button").addClass("active");
        $("#sprinter-button").removeClass("active");

        $("#non-stop").removeClass("active");
        $("#two-stop").addClass("active");
        $("#sprinter").removeClass("active");
    });

    $('#sprinter-button').click(function () {
        $("#non-stop-button").removeClass("active");
        $("#two-stop-button").removeClass("active");
        $("#sprinter-button").addClass("active");

        $("#non-stop").removeClass("active");
        $("#two-stop").removeClass("active");
        $("#sprinter").addClass("active");
    });

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-85175328-1', 'auto');
    ga('send', 'pageview');

    setInterval(function () { tick2() }, 3000);

});
var currimg = 0;
var currlists = 0;
var images = "";
var lists = "";
function loadimg() {

    $('.intro').animate({ opacity: 1 }, 500, function () {

        //finished animating, minifade out and fade new back in
        $('.intro').animate({ opacity: 1 }, 100, function () {

            currimg++;

            currlists++;

            if (currimg > images.length - 1) {

                currimg = 0;

            }

            if (currlists > lists.length - 1) {

                currlists = 0;

            }

            var newimage = images[currimg];

            var newlists = lists[currlists];

            $('.main-heading').fadeOut();
            $('.main-heading').fadeIn(3000);

            //swap out bg src
            $('.intro').css("background-image", "url(" + newimage + ")");


            $('.list-1 span').html(newlists);
            newlists = lists[++currlists];
            $('.list-2 span').html(newlists);

            //animate fully back in
            $('.intro').animate({ opacity: 1 }, 400, function () {

                //set timer for next
                setTimeout(loadimg, 5000);

            });

        });

    });

}

function tick2() {
    //alert('hello');
    $('#ticker_02 li:first').slideUp(function () { $(this).appendTo($('#ticker_02')).slideDown(); });
}