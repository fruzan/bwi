﻿var sLat;
var sLong;
var dLat;
var dLong;
var PicLat;
var PicLong;

var TimeTaken;
var TotalDistance;

var First = false;
var Second = false;
var Third = false;

var ChkRet;

$(document).ready(function () {

    $('#txt_FirstHalt').attr('readonly', true);
    $('#txt_FirstHalt').addClass('input-disabled');
    $('#txt_SecondHalt').attr('readonly', true);
    $('#txt_SecondHalt').addClass('input-disabled');
    $('#txt_ThirdHalt').attr('readonly', true);
    $('#txt_ThirdHalt').addClass('input-disabled');
    $('#txt_FourthHalt').attr('readonly', true);
    $('#txt_FourthHalt').addClass('input-disabled');
    $('#txt_FifthHalt').attr('readonly', true);
    $('#txt_FifthHalt').addClass('input-disabled');
    $('#txtSourceP2P').keyup(function () {
        if ($("#txtSourceP2P").val() != '') {
            $("#txt_FirstHalt").attr('readonly', false);
        }
        else {
            $("#txt_FirstHalt").attr('readonly', true);
            $("#txt_SecondHalt").attr('readonly', true);
            $("#txt_ThirdHalt").attr('readonly', true);
            $("#txt_FourthHalt").attr('readonly', true);
            $("#txt_FifthHalt").attr('readonly', true);

            $("#txt_FirstHalt").val('');
            $("#txt_SecondHalt").val('');
            $("#txt_ThirdHalt").val('');
            $("#txt_FourthHalt").val('');
            $("#txt_FifthHalt").val('');
        }
    });
    

    $('#txt_SecondHalt').keyup(function () {
        if ($("#txt_SecondHalt").val() != '') {
            $("#txt_ThirdHalt").attr('readonly', false);
        }
        else {
            $("#txt_ThirdHalt").attr('readonly', true);
            $("#txt_FourthHalt").attr('readonly', true);
            $("#txt_FifthHalt").attr('readonly', true);

            $("#txt_ThirdHalt").val('');
            $("#txt_FourthHalt").val('');
            $("#txt_FifthHalt").val('');
        }
    });

    $('#txt_ThirdHalt').keyup(function () {
        if ($("#txt_ThirdHalt").val() != '') {
            $("#txt_FourthHalt").attr('readonly', false);
        }
        else {
            $("#txt_FourthHalt").attr('readonly', true);
            $("#txt_FifthHalt").attr('readonly', true);

            $("#txt_FourthHalt").val('');
            $("#txt_FifthHalt").val('');
        }
    });

    $('#txt_FourthHalt').keyup(function () {
        if ($("#txt_FourthHalt").val() != '') {
            $("#txt_FifthHalt").attr('readonly', false);
        }
        else {
            $("#txt_FifthHalt").attr('readonly', true);

            $("#txt_FifthHalt").val('');
        }
    });
    //var dateTime = convertToDateTime("23.11.2009 12:34:56", "dd.MM.yyyy");
    //alert(dateTime);
    GetServices();
    GetAirLines();
});

var arrService = new Array();
function GetServices() {
    $.ajax({
        url: "../Admin/ReservationHandler.asmx/GetAllServices",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var ServiceType = obj.ServiceType;
            var arrLocation = obj.Location;
            if (obj.retCode == 1) {
                arrService = obj.ServiceType;

                if (arrLocation.length > 0) {
                    ddlRequest = '';
                    $("#AirPort").empty();
                    $("#ShuttleAirPort").empty();
                    var ddlRequest = '<option value="-" selected="selected">Select</option>';
                    for (i = 0; i < arrLocation.length; i++) {
                        var asd = arrLocation[i].Status;
                        if (arrLocation[i].LocationName == "IAD")
                        {
                            var Name = "";
                            ddlRequest += '<option value="' + arrLocation[i].sid + ',' + arrLocation[i].Latitude + ',' + arrLocation[i].Longitude + ',' + arrLocation[i].LocationName + '">' + arrLocation[i].LocationName + '</option>';
                        }
                        else if (arrLocation[i].Status!="Deactive")
                        {
                            ddlRequest += '<option value="' + arrLocation[i].sid + ',' + arrLocation[i].Latitude + ',' + arrLocation[i].Longitude + ',' + arrLocation[i].LocationName + '">' + arrLocation[i].LocationName + '</option>';
                        }
                    }
                    $("#AirPort").append(ddlRequest);
                    $("#ShuttleAirPort").append(ddlRequest);
                }
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}

function Submit() {
    //debugger

    var Service;
    var Passenger;
    var Pdate;
    var PTime;
    var PickUpAt;
    var DropOff;
    var From;
    var To;
    var Date;
    var bValid = true;

   // debugger;

    if (First == true) {
      //  debugger;
        //$('#SpnMessege').text("Something Went Wrong.")
        //$('#ModelMessege').modal('show')
        if (TotalDistance == '' || TotalDistance == undefined) {
            alert("Please Enter Proper Location Unable to calculate Distance")
            //$('#P2P_Date').focus();
            return false;
        }


        var AirPort = $('#AirPort').val();
        var AirPortSplit = AirPort.split(',')
        var Service = $('#Service').val();
        Date = $('#txt_Date').val();
        var Passenger = $('#PickUp_Passengers').val();

        var Return_Date = $("#Return_Date").val();
        if (Service == "From Airport")
            Return_Date = $("#Return_DateFrom").val();
        var Ret_FlightNumber = $("#FlightNumber").val();
        var Ret_Airlines = $("#Select_Airline").val();
        var Ret_FlightTime = $("#Flight_Hour").val() + ":" + $("#Flight_Minutes").val() + ":" + $("#Flight_AM_PM").val();
        var Pickuptime = $("#Pickup_Hour").val() + ":" + $("#Pickup_Minutes").val() + ":" + $("#Pickup_AM_PM").val();
        var From_FlightNumber = $("#FlightNumberFrom").val();
        var From_ArrTime = $("#Flight_HourFrom").val() + ":" + $("#Flight_MinutesFrom").val() + ":" + $("#Flight_AM_PMFrom").val();
        var From_Airlines = $("#Select_AirlineFrom").val();
        //alert(From_Airlines);
        var From_ReturnDate = $("#Return_DateFrom").val();
        var From_RetTime = $("#ReturnFlight_Hour").val() + ":" + $("#ReturnFlight_Minutes").val() + ":" + $("#ReturnFlight_AM_PM").val();

        if (AirPort == '-')
        {
            alert("Please select Airport");
            $('#AirPort').focus();
            return false;
        }
        if (Date == '') {
            alert("Please select Date");
            $('#txt_Date').focus();
            return false;
        }
        if (Passenger == 0 || Passenger > 50)
        {
            alert("Please select Passenger from 1 to 50");
            $('#PickUp_Passengers').focus();
            return false;
        }

        if ($("#ChkRetReservation").is(':not(:checked)'))
        {
            ChkRet = false;
        }
        else
        {
            ChkRet = true;
        }
        var Data = { Pickuptime: Pickuptime, Date: Date };
        $.ajax({
            url: "DefaultHandler.asmx/CheckResTime",
            type: "POST",
            data: JSON.stringify(Data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.Retcode == 1) {
                    if (Service == 'To Airport') {
                        From = $('#PickUp_Location').val();
                        To = AirPortSplit[3]

                        if (ChkRet == true) {
                            if (Return_Date == '') {
                                alert("Please select Return Date");
                                $('#Return_Date').focus();
                                return false;
                            }
                            if (Ret_FlightNumber == '') {
                                alert("Please Enter Flight Number");
                                $('#FlightNumber').focus();
                                return false;
                            }
                            if (Ret_Airlines == '') {
                                alert("Please select Airlines");
                                $('#Select_Airline').focus();
                                return false;
                            }
                        }
                    }
                    else if (Service == 'From Airport') {
                        From = AirPortSplit[3]
                        To = $('#PickUp_Location').val();

                        if (From_FlightNumber == '') {
                            alert("Please Enter Flight Number");
                            $('#FlightNumberFrom').focus();
                            return false;
                        }
                        if (From_Airlines == '') {
                            alert("Please select Airlines");
                            $('#Select_AirlineFrom').focus();
                            return false;
                        }
                        if (ChkRet == true) {
                            if (Return_Date == '') {
                                alert("Please select Return Date");
                                $('#Return_Date').focus();
                                return false;
                            }
                        }
                        //DropOff = $('#PickUp_Location').val();
                        //PickUpAt = ''
                    }
                    var Passengers = Passenger
                    //var Date = $('#datepicker3').val()
                    window.location.href = "../Search.html?Tab=1&Service=" + Service + "&Passenger=" + Passenger + "&From=" + From + "&To=" + To + "&LatLong=" + AirPortSplit[1] + "," + AirPortSplit[2] + "," + dLat + "," + dLong + "," + TimeTaken + "," + TotalDistance + "&Date=" + Date + "&Pickuptime=" + Pickuptime + "&ChkRet=" + ChkRet + "&Return_Date=" + Return_Date + "&Ret_FlightNumber=" + Ret_FlightNumber + "&Ret_Airlines=" + Ret_Airlines + "&Ret_FlightTime=" + Ret_FlightTime + "&From_FlightNumber=" + From_FlightNumber + "&From_ArrTime=" + From_ArrTime + "&From_Airlines=" + From_Airlines + "&From_ReturnDate=" + From_ReturnDate + "&From_RetTime=" + From_RetTime;

                    $("#Service option:selected").val("To Airport");
                    $("#AirPort option:selected").val('-');
                    $("#PickUp_Location option:selected").val('-');
                    $("#PickUp_Passengers").val('');
                    $("#txt_Date").val('');
                }
                else {
                    alert("The time you have selected is within 24 hours, you must call our Reservation Department to make this reservation or select a new pickup time.");
                }
            },
        });
    }

    if (Second == true) {
        //debugger;
        var StopMisc = $('input[name="LocationM"]').map(function () {
            return this.value;
        }).get();
        var Pick_location = $("#txtSourceP2P").val();
        $("#txt_FirstHalt").val(Pick_location);
        Date = $('#P2P_Date').val()
        var PassengerP2P = $('#Passengers').val();
        //var StopHours = $('select[name="LocationH1"]').map(function () {
        //    return this.value;
        //}).get();

        //var StopMins = $('select[name="LocationH2"]').map(function () {
        //    return this.value;
        //}).get();
        //var Location = $('input[name="Location"]').map(function () {
        //    return this.value;
        //}).get();
        if (Pick_location == '') {
            alert("Please Enter Location")
            $('#txtSourceP2P').focus();
            return false;
        }
        if (Date == '')
        {
            alert("Please Enter Date")
            $('#P2P_Date').focus();
            return false;
        }
        if (PassengerP2P == 0 || PassengerP2P > 50)
        {
            alert("Please Select Passenger from 1 to 50")
            $('#Passengers').focus();
            return false;
        }
        if ($('#txt_SecondHalt').val() == '')
        {
            alert("Please Enter Second Location")
            $('#txt_SecondHalt').focus();
            return false;
        } 
        if (TotalDistance == '' || TotalDistance == undefined)
        {
            alert("Please Enter Proper Location Unable to calculate Distance")
            //$('#P2P_Date').focus();
            return false;
        }
        var PickTime = $("#P2P_Hour").val() + ":" + $("#P2P_Minutes").val() + ":" + $("#P2P_AM_PM").val();
        var Data = { Pickuptime: PickTime, Date: Date };
        $.ajax({
            url: "DefaultHandler.asmx/CheckResTime",
            type: "POST",
            data: JSON.stringify(Data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.Retcode == 1) {
                    var Stop1, Stop2, Stop3, Stop4, Stop5;
                    var Locations = document.getElementsByName("Location");
                    var StopHours = document.getElementsByName("LocationH1");
                    var StopMins = document.getElementsByName("LocationH2");
                    var MISC = document.getElementsByName("LocationM");
                    Stop1 = Locations[0].value + "^" + StopHours[0].value + ":" + StopMins[0].value + "^" + MISC[0].value
                    Stop2 = Locations[1].value + "^" + StopHours[1].value + ":" + StopMins[1].value + "^" + MISC[1].value
                    Stop3 = Locations[2].value + "^" + StopHours[2].value + ":" + StopMins[2].value + "^" + MISC[2].value
                    Stop4 = Locations[3].value + "^" + StopHours[3].value + ":" + StopMins[3].value + "^" + MISC[3].value
                    Stop5 = Locations[4].value + "^" + StopHours[4].value + ":" + StopMins[4].value + "^" + MISC[4].value
                    var SourceP2P = $('#txtSourceP2P').val()
                    //var DestinationP2P = $('#txtDestinationP2P').val()
                    var DestinationP2P = $('#txt_FirstHalt').val() + '^' + $('#txt_SecondHalt').val() + '^' + $('#txt_ThirdHalt').val() + '^' + $('#txt_FourthHalt').val() + '^' + $('#txt_FifthHalt').val()
                    //var PassengerP2P = $('#P2P_Passengers').val()
                    From = SourceP2P;
                    for (var i = 0; i < 5; i++)
                    {
                        if(Locations[i].value !="")
                        {
                            To = Locations[i].value;
                        }
                    }
                    //var Passengers = PassengerP2P
                    window.location.href = "../Search.html?Tab=2&SourceP2P=" + SourceP2P + "&DestinationP2P=" + DestinationP2P + "&PassengerP2P=" + PassengerP2P + "&LatLong=" + sLat + "," + sLong + "," + dLat + "," + dLong + "," + TimeTaken + "," + TotalDistance + "&From=" + From + "&To=" + To + "&Date=" + Date + "&PickTime=" + PickTime + "&Stop1=" + Stop1 + "&Stop2=" + Stop2 + "&Stop3=" + Stop3 + "&Stop4=" + Stop4 + "&Stop5=" + Stop5;

                    $("#txtSourceP2P").val('');
                    $("#P2P_Date").val('');
                    $("#Passengers option:selected").val('-');
                    $("#txt_FirstHalt").val('');
                    $("#1LocationH1 option:selected").val('-');
                    $("#1LocationH2 option:selected").val('-'); 
                    $("#txt_FirstMisc").val('');
                }
                else {
                    alert("The time you have selected is within 24 hours, you must call our Reservation Department to make this reservation or select a new pickup time.");
                }
            },
        });
    }

            if (Third == true) {

                var Source = $('#txtSource').val()
                //var Destination = $('#txtDestination').val()
                var Passenger = $('#HourlyPassengers').val()
                //alert(Passenger);
                var Halt = $('#Hours').val();
                if (Passenger == 0 || Passenger > 50) {
                    alert("Please Select Passenger from 1 to 50")
                    $('#HourlyPassengers').focus();
                    return false;
                }
                var PickTime = $("#Hourly_Hour").val() + ":" + $("#Hourly_Minutes").val() + ":" + $("#Hourly_AM_PM").val();
                Date = $('#Hourly_Date').val()
                var Data = { Pickuptime: PickTime, Date: Date };
                $.ajax({
                    url: "DefaultHandler.asmx/CheckResTime",
                    type: "POST",
                    data: JSON.stringify(Data),
                    contentType: "application/json",
                    datatype: "json",
                    success: function (response) {
                        var obj = JSON.parse(response.d);
                        if (obj.Retcode == 1) {
                            From = Source;
                            //To = Destination
                            var Passengers = Passenger
                            window.location.href = "../Search.html?Tab=3&Source=" + Source + "&Passenger=" + Passenger + "&Date=" + Date + "&Halt=" + Halt + "&LatLong=" + sLat + "," + sLong + "," + dLat + "," + dLong + "&From=" + From + "&PickTime=" + PickTime;
                        }
                        else {
                            alert("The time you have selected is within 24 hours, you must call our Reservation Department to make this reservation or select a new pickup time.");
                        }
                    },
                });
            }

            if (First == false && Second==false && Third==false)
            {
                alert("Please Enter Location");
            }
}

function Submit1() {
    //debugger
    
    var Service;
    var Passenger;
    var Pdate;
    var PTime;
    var PickUpAt;
    var DropOff;
    var From;
    var To;
    var Date;
    var bValid = true;
       
        //debugger;
        //$('#SpnMessege').text("Something Went Wrong.")
        //$('#ModelMessege').modal('show')
        if (TotalDistance == '' || TotalDistance == undefined) {
            alert("Please Enter Proper Location Unable to calculate Distance")
            //$('#P2P_Date').focus();
            return false;
        }
        var AirPort = $('#ShuttleAirPort').val();
        var AirPortSplit = AirPort.split(',')
        var Service = $('#ShuttleService').val();
        Date = $('#Shuttletxt_Date').val();
        var Pickuptime = $("#ShuttlePickup_Hour").val() + ":" + $("#ShuttlePickup_Minutes").val() + ":" + $("#ShuttlePickup_AM_PM").val();
        var Passenger = $('#ShuttlePickUp_Passengers').val();
        var Data = { Pickuptime: Pickuptime, Date: Date };
        var Adult = $('#ShuttleNoOfAdults_Passengers').val();
        var Child = $('#ShuttleNoOfChildren_Passengers').val();
        var ChildL5 = $('#Shuttlelessthan5_Passengers').val();
        $.ajax({
            url: "DefaultHandler.asmx/CheckResTime",
            type: "POST",
            data: JSON.stringify(Data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.Retcode == 1) {
                    var Return_Date = $("#ShuttleReturn_Date").val();
                    if (Service == "From Airport")
                        Return_Date = $("#ShuttleReturn_DateFrom").val();
                    var Ret_FlightNumber = $("#ShuttleFlightNumber").val();
                    var Ret_Airlines = $("#ShuttleSelect_Airline").val();
                    var Ret_FlightTime = $("#ShuttleFlight_Hour").val() + ":" + $("#ShuttleFlight_Minutes").val() + ":" + $("#ShuttleFlight_AM_PM").val();
                   
                    var From_FlightNumber = $("#ShuttleFlightNumberFrom").val();
                    var From_ArrTime = $("#ShuttleFlight_HourFrom").val() + ":" + $("#ShuttleFlight_MinutesFrom").val() + ":" + $("#ShuttleFlight_AM_PMFrom").val();
                    var From_Airlines = $("#ShuttleSelect_AirlineFrom").val();
                    //alert(From_Airlines);
                    var From_ReturnDate = $("#ShuttleReturn_DateFrom").val();
                    var From_RetTime = $("#ShuttleReturnFlight_Hour").val() + ":" + $("#ShuttleReturnFlight_Minutes").val() + ":" + $("#ShuttleReturnFlight_AM_PM").val();
        
                    if (AirPort == '-') {
                        alert("Please select Airport");
                        $('#ShuttleAirPort').focus();
                        return false;
                    }
                    if (Date == '') {
                        alert("Please select Date");
                        $('#Shuttletxt_Date').focus();
                        return false;
                    }
                    if (Passenger == 0 || Passenger > 50) {
                        alert("Please select Passenger from 1 to 50");
                        $('#ShuttlePickUp_Passengers').focus();
                        return false;
                    }

                    if ($("#ShuttleChkRetReservation").is(':not(:checked)')) {
                        ChkRet = false;
                    }
                    else {
                        ChkRet = true;
                    }

                    if (Service == 'To Airport') {
                        From = $('#ShuttlePickUp_Location').val();
                        To = AirPortSplit[3]

                        if (ChkRet == true) {
                            if (Return_Date == '') {
                                alert("Please select Return Date");
                                $('#ShuttleReturn_Date').focus();
                                return false;
                            }
                            if (Ret_FlightNumber == '') {
                                alert("Please Enter Flight Number");
                                $('#ShuttleFlightNumber').focus();
                                return false;
                            }
                            if (Ret_Airlines == '') {
                                alert("Please select Airlines");
                                $('#ShuttleSelect_Airline').focus();
                                return false;
                            }
                        }
                    }
                    else if (Service == 'From Airport') {
                        From = AirPortSplit[3]
                        To = $('#ShuttlePickUp_Location').val();

                        if (From_FlightNumber == '') {
                            alert("Please Enter Flight Number");
                            $('#ShuttleFlightNumberFrom').focus();
                            return false;
                        }
                        if (From_Airlines == '') {
                            alert("Please select Airlines");
                            $('#ShuttleSelect_AirlineFrom').focus();
                            return false;
                        }

                        if (ChkRet == true) {
                            if (Return_Date == '') {
                                alert("Please select Return Date");
                                $('#ShuttleReturn_Date').focus();
                                return false;
                            }
                        }
                        //DropOff = $('#PickUp_Location').val();
                        //PickUpAt = ''
                    }
                    var Passengers = Passenger
                    //var Date = $('#datepicker3').val()

                    setTimeout(function(){
                        window.location.href = "../Search1.html?Tab=4&Service=" + Service + "&Passenger=" + Passenger + "&From=" + From + "&To=" + To + "&LatLong=" + AirPortSplit[1] + "," + AirPortSplit[2] + "," + dLat + "," + dLong + "," + TimeTaken + "," + TotalDistance + "&Date=" + Date + "&Pickuptime=" + Pickuptime + "&ChkRet=" + ChkRet + "&Return_Date=" + Return_Date + "&Ret_FlightNumber=" + Ret_FlightNumber + "&Ret_Airlines=" + Ret_Airlines + "&Ret_FlightTime=" + Ret_FlightTime + "&From_FlightNumber=" + From_FlightNumber + "&From_ArrTime=" + From_ArrTime + "&From_Airlines=" + From_Airlines + "&From_ReturnDate=" + From_ReturnDate + "&From_RetTime=" + From_RetTime + "&Adult=" + Adult + "&Child=" + Child + "&ChildL5=" + ChildL5;
                        $("#ShuttleService option:selected").val("To Airport");
                        $("#ShuttleAirPort option:selected").val('-');
                        $("#ShuttlePickUp_Location option:selected").val('-');
                        $("#ShuttlePickUp_Passengers").val('');
                        $("#Shuttletxt_Date").val('');
                    }, 200)
                }
                else {
                    alert("The time you have selected is within 24 hours, you must call our Reservation Department to make this reservation or select a new pickup time.");
                }
            },
        });
}

function GetAirLines() {

    $.ajax({
        url: "../Admin/ReservationRateHandler.asmx/GetAirLines",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);


            if (obj.Retcode == 1) {
                var Driver = obj.Arr;
                //debugger;
                var OptionMac_Name = null;

                if (obj.Retcode == 1) {
                    var Driver = obj.Arr;
                    for (var i = 0; i < Driver.length; i++) {

                        OptionMac_Name += '<option value="' + Driver[i].Callsign + '" >' + Driver[i].Callsign + '</option>'
                    }

                    $("#Select_Airlines").append(OptionMac_Name);
                    $("#Select_AirlinesFrom").append(OptionMac_Name);
                    $("#ShuttleSelect_Airlines").append(OptionMac_Name);
                    $("#ShuttleSelect_AirlinesFrom").append(OptionMac_Name);
                }
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}

function stringToDate(_date, _format, _delimiter) {
    var formatLowerCase = _format.toLowerCase();
    var formatItems = formatLowerCase.split(_delimiter);
    var dateItems = _date.split(_delimiter);
    var monthIndex = formatItems.indexOf("mm");
    var dayIndex = formatItems.indexOf("dd");
    var yearIndex = formatItems.indexOf("yyyy");
    var month = parseInt(dateItems[monthIndex]);
    month -= 1;
    var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
    //alert(formatedDate);
    return formatedDate;
}

stringToDate("17/9/2014", "dd/MM/yyyy", "/");
stringToDate("9/17/2014", "mm/dd/yyyy", "/")
stringToDate("9-17-2014", "mm-dd-yyyy", "-")

var Adults = 0;
var Childs = 0;
var ChildLessthan5 = 0;
function calPassenger()
{
    debugger;
    Adults = $("#ShuttleNoOfAdults_Passengers").val();
    Childs = $("#ShuttleNoOfChildren_Passengers").val();
    ChildLessthan5 = $("#Shuttlelessthan5_Passengers").val();
    if (Childs == "")
    {
        Childs = 0;
    }
    if (parseFloat(Childs) < parseFloat(ChildLessthan5))
    {
        $("#Shuttlelessthan5_Passengers").val(Childs);
        ChildLessthan5 = Childs;
    }
    //var TotalPassenger =parseFloat( Adults) + (parseFloat(Childs) -parseFloat(ChildLessthan5));
    var TotalPassenger = parseFloat(Adults) + (parseFloat(Childs) + parseFloat(ChildLessthan5));

    $("#ShuttlePickUp_Passengers").val(TotalPassenger);
}