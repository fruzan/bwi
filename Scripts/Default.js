﻿var Username;
var Password;
var UserType;
function Login()
{
    var bValid = Validation();
    if (bValid == true)
    {
        UserName = $("#Username").val();
        Password = $("#Password").val();
        UserType = $("#UserType").val();
        var data = { Username: UserName, Password: Password, UserType: UserType }

        $.ajax({
            url: "DefaultHandler.asmx/UserLogin",
            type: "POST",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.Session == 1) {
                    if (obj.Retcode == "1") {
                        window.location.href = obj.Redirect;
                    }
                    else if (obj.Retcode == "-1") {
                        $('#SpnMessege').text("Invalid Username or Password. Please try again.");
                        $("#ModelMessege").modal("show")


                    }
                    else if (obj.Retcode == "0") {
                        $('#SpnMessege').text("Somthing went wrong. Please try again.")
                        $("#ModelMessege").modal("show")
                    }
                }
                else {
                    $('#SpnMessege').text("Session Expired!");
                    $("#ModelMessege").modal("show")
                }

            },
            error: function () {

                $('#SpnMessege').text("Somthing went wrong. Please try again.")
                $("#ModelMessege").modal("show")
            }


        });




        //if (UserName == "admin" && Password == "admin")
        //{
        //    window.location.href = "Admin/AdminDashboard.aspx";
        //}
        //else if(UserName == "customer" && Password == "customer")
        //{
        //    window.location.href = "Customer/CustomerDashBoard.aspx";
        //}
        //else if (UserName == "driver" && Password == "driver")
        //{
        //    window.location.href = "Driver/DriverDashBoard.aspx";
        //}
    }
}

function LogOut() {
        $.ajax({
            url: "../DefaultHandler.asmx/LogOut",
            type: "POST",
            data: {},
            contentType: "application/json",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                        window.location.href = "../Index.html";
            },
            error: function () {
                $('#SpnMessege').text("Somthing went wrong. Please try again.")
                $("#ModelMessege").modal("show")
            }
        });
}

function Validation()
{
    UserName = $("#Username").val();
    Password = $("#Password").val();
    if ($("#UserName").val() == "")
    {
        alert("Please enter name");
        document.getElementById("UserName").focus();
        return false;
    }
    else if ($("#Password").val() == "")
    {
        alert("Please Enter Password")
        document.getElementById("Password").focus();
        return false;
    }
   
    return true;
}

