﻿function RegisterEmail(Type) {
    var Email = $("#NewEmail").val();
    var Password = $("#NewPassword").val();
    var FirstName = $("#txt_FirstName").val();
    var LastName = $("#txt_LastName").val();
    var Gender = $("#Gender option:selected").val();
    var Mobile = $("#txt_Mob").val();
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (Email == '')
    {
        alert("Please Enter Email");
        return false;
    }
    if (!emailReg.test(Email)) {
        alert("Please enter valid Email ID");
        return false;
        }
    if (Password == '') {
        alert("Please Enter Password");
        return false;
    }
    if (FirstName == '') {
        alert("Please Enter First Name");
        return false;
    }
    if (LastName == '') {
        alert("Please Enter Last Name");
        return false;
    }
    if (Mobile == '') {
        alert("Please Enter Mobile");
        return false;
    }
    var data = { Email: Email, Password: Password, FirstName: FirstName, LastName: LastName, Gender: Gender, Mobile: Mobile };

    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/Register",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {
                if (Type == 'Booking')
                {
                    $("#Register").modal('hide');
                    $('#SpnMessege').text("Email Address is Registered");
                    $('#ModelMessege').modal('show');
                    $("#First_Name").val(FirstName);
                    $("#Last_Name").val(LastName);
                    $("#PhoneNo").val(Mobile);
                    $("#Email").val(Email);
                    $("#lblRegister").text("Registered")
                }
                else {
                    alert("Email Address is Registered");
                    window.location.href = "Login.html";
                }
                //alert("Email Address is Registered");
                //window.location.href = "Login.aspx";
            }
            else if (obj.Retcode == 2) {
                //$('#SpnMessege').text("This Email address is already registered")
                //$('#ModelMessege').modal('show')
                alert("This Email address is already registered");
            }
        },
    });
}


function SendEmail() {
    
    var Name = $("#input_name").val();
    var Email = $("#input_email").val();
    var Subject = $("#input_phone").val();
    var Website = $("#input_subject").val();
    var Message = $("#textarea_message").val();

    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (Email == '') {
        alert("Please Enter Email");
        return false;
    }
    if (!emailReg.test(Email)) {
        alert("Please enter valid Email ID");
        return false;
    }
   
    if (Name == '') {
        alert("Please Enter Name");
        return false;
    }
    if (Message == '') {
        alert("Please Enter Message");
        return false;
    }
    var data = { Name: Name, Email: Email, Subject: Subject, Website: Website, Message: Message};

    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/EmailSending",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                alert("Enquiry Sent");
                window.location.reload();
            }
            else {               
                alert("Something Went Wrong");
            }
        },
    });
}
