﻿function Register()
{
    var Email = $("#NewEmail").val();
    var Password = $("#NewPassword").val();
    var data = { Email: Email, Password: Password }

    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/Register",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1)
            {
                $("#Register").modal('hide');
                $('#SpnMessege').text("Email Address is Registered");
                $('#ModelMessege').modal('show');
                //alert("Email Address is Registered");
                //window.location.href = "Login.aspx";
            }
            else if (obj.Retcode == 2)
            {
                $('#SpnMessege').text("This Email address is already registered")
                $('#ModelMessege').modal('show')
                //alert("This Email address is already registered");
            }
        },
    });
}