﻿function sentmail()
{
    var validate = true;
    if ($("#name").val() == "") {
        $("#name").focus();
        alert("Please enter your Name");
        validate = false;
        return false;
    }
    else if ($("#email").val() == "") {
        $("#email").focus();
        alert("Please enter your Email");
        validate = false;
        return false;
    }
    else if ($("#email").val() != "") {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test($("#email").val())) {
            $("#email").focus();
            alert("Please enter valid Email ID");
            return false;
        }
    else if ($("#Subject").val() == "") {
        $("#Subject").focus();
        alert("Please enter Subject");
        validate = false;
        return false;
    }
    else if ($("#txt").val() == "") {
        $("#txt").focus();
        alert("Please enter Message");
        validate = false;
        return false;
    }
    }
    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/EmailSending",
        beforeSend: function () {
            $('#wait').css('display', 'inline');
        },
        complete: function () {
            $('#wait').css('display', 'none');
        },
        data: '{"Name":"' + $("#name").val() + '","Email":"' + $("#email").val() + '","Subject":"' + $("#Subject").val() + '","Website":"' + $("#Website").val() + '","Message":"' + $("#txt").val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            try {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (result.retCode == 1) {
                    $("#name").val('');
                    $("#email").val('');
                    $("#Subject").val('');
                    $("#Website").val('');
                    $("#txt").val('');
                    alert("Your request sent.We will get in touch with you soon.");
                }
                else {
                    alert("Your request sent.We will get in touch with you soon");
                }

            } catch (e) { }
        },
        error: function () {
            alert("Something going wrong");
        }
    });

}