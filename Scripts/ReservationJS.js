﻿function Submit() {
    debugger;

    var Input = $('input[name="text"]').map(function () {
        return this.value
    }).get()
    var Select = $('select[name="Drop"]').map(function () {
        return this.value
    }).get()
    var Areas = $('textarea[name="textarea"]').map(function () {
        return this.value
    }).get()
    var data = { Input: Input, Select: Select, Areas: Areas}

    $.ajax({
        type: "POST",
        url: "../BookingHandler.asmx/BookingSuccess",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {
                $('#SpnMessege').text("Reservation Done Successfully.")
                $('#ModelMessege').modal('show')
            }
            else {
                $('#SpnMessege').text("Reservation unsuccessfull.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },


    });

}