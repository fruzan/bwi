﻿$(document).ready(function () {
    
    var RefreshTimer = self.setInterval(function () {
        $("#image").css("display", "none");
        CheckSession();
    }, 1000);
    //setInterval(CheckSession(), 500);
});
function CheckSession() {
    
   
    $.ajax({
        url: "../DefaultHandler.asmx/CheckSession",
        type: "post",
        data: '',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var result = (typeof data.d) == 'string' ? eval('(' + data.d + ')') : data.d;
            if (result.retCode != 1 || result.retCode == 0) {
                window.location.href = "../Login.html";
            }
        },
        error: function () {
            //alert('Error occured while authenticating user!');
            window.location.href = "../Login.html";
        }
    });
    $("#image").css("display", "none");
}
