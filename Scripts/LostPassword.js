﻿function MailPassword()
{
    var Email = $("#Username").val();
    if (Email=="")
    {
        alert('Please Enter Registered Email Id');
        return false;
    }
    var data = { sEmail: Email }

    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/sendPassword",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.retCode == 1)
            {
                alert("Password sent to the entered Email Adress");
                window.location.href = "Login.html";
            }
            else if (obj.retCode == 0) {
                alert("Email address is not available");
            }
            else if (obj.retCode == -1) {
                alert("You are not Registered");
            }
        },
    });
}