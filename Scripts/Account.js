﻿var bvalid


function Login() {
    var Username = $('#Username').val();
    var Password = $('#Password').val();

    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (Username != "") {
        if (emailReg.test(Username)) {

        }
        else {
            $('#SpnMessege').text("Please Enter proper Email Format!")
            $('#ModelMessege').modal('show')
            return false;
        }
    }
    else {
        $('#SpnMessege').text("Please Enter Email Id")
        $('#ModelMessege').modal('show')
        return false;
    }

    if (Password == "") {
        $('#SpnMessege').text("Please Enter Your Password")
        $('#ModelMessege').modal('show')
        return false;
    }
    else {
        var Count = Password.length

        if (Count > 12 || Count < 6) {
            $('#SpnMessege').text("Password Length Should be 6-12")
            $('#ModelMessege').modal('show')
            return false;
        }

    }

    var data = { Username: Username, Password: Password }

    $.ajax({
        type: "POST",
        url: "../GeneralHandler.asmx/UserLogin",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.Retcode == 1) {
                debugger;
                window.location.href = "../Request.aspx"
            }

            else if (obj.Retcode == 2) {
                $('#SpnMessege').text("Invalid Username or Password!")
                $('#ModelMessege').modal('show')
            }

            else if (obj.Retcode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });



}

function Submit() {
    debugger;
    var Input = $('input[name="Text[]"]').map(function () {
        return this.value
    }).get()

    var AccountType = $('#Sel_AccountType').val();
    var Prefix = $('#Sel_Prefix').val();

    var data = { Input: Input, AccountType: AccountType, Prefix: Prefix }

    bvalid = Validations();

    if (bvalid) {
        $.ajax({
            type: "POST",
            url: "../GeneralHandler.asmx/CreateAccount",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.Retcode == 1) {
                    debugger;
                    alert("Acoount Created. Please Sign In For Booking.")
                    window.location.href = "../Account.aspx"
                }

                if (obj.Retcode == 3) {
                    debugger;
                    alert("Acoount Created. Problem While Sending Mail.")
                    window.location.href = "../Account.aspx"
                }

                if (obj.Retcode == 2) {
                    $('#SpnMessege').text("Invalid Username or Password!")
                    $('#ModelMessege').modal('show')
                }

                if (obj.Retcode == 0) {
                    $('#SpnMessege').text("Something Went Wrong.")
                    $('#ModelMessege').modal('show')
                }
            }
        });
    }
}

function Reset() {
    debugger;
    var Username = $('#Email').val();

    var data = { Username: Username }


    $.ajax({
        type: "POST",
        url: "../GeneralHandler.asmx/ResetPassowrd",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.Retcode == 1) {
                debugger;

                $('#SpnMessege').text("An Email Containg password is sent to your account.")
                $('#ModelMessege').modal('show')
            }

            if (obj.Retcode == 2) {
                $('#SpnMessege').text("No Account Found With this Email-ID")
                $('#ModelMessege').modal('show')
            }

            if (obj.Retcode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}


function Validations() {

    var AccountType = $('#Sel_AccountType').val()
    var Prefix = $('#Sel_Prefix').val()
    var FirstName = $('#txt_FirstName').val()
    var LastName = $('#txt_LastName').val()
    var Address = $('#txt_Address').val()
    var City = $('#txt_City').val()
    var State = $('#txt_State').val()
    var ZipCode = $('#txt_ZipCode').val()
    var Email = $('#txt_Email').val()
    var Password = $('#txt_Password').val()


    if (AccountType == "-") {
        $('#SpnMessege').text("Please Select Account Type")
        $('#ModelMessege').modal('show')
        return false;
    }
    if (Prefix == "-") {
        $('#SpnMessege').text("Please Select Prefix")
        $('#ModelMessege').modal('show')
        return false;
    }

    var regex = new RegExp("^[a-zA-Z]+$"); // for alphabets to allow

    if (FirstName != "") {
        if (regex.test(FirstName)) {

        }
        else {
            $('#SpnMessege').text("Only alphbets Are Allowed")
            $('#ModelMessege').modal('show')

            return false;
        }
    }
    else {
        $('#SpnMessege').text("Please Enter First Name")
        $('#ModelMessege').modal('show')

        return false;
    }

    if (LastName != "") {
        if (regex.test(LastName)) {

        }
        else {
            $('#SpnMessege').text("Only alphbets Are Allowed")
            $('#ModelMessege').modal('show')

            return false;
        }
    }
    else {
        $('#SpnMessege').text("Please Enter Last Name")
        $('#ModelMessege').modal('show')

        return false;
    }

    if (Address == "") {
        $('#SpnMessege').text("Please Enter Your Address")
        $('#ModelMessege').modal('show')
        return false;
    }

    if (City == "") {
        $('#SpnMessege').text("Please Enter Your City")
        $('#ModelMessege').modal('show')
        return false;
    }
    if (State == "") {
        $('#SpnMessege').text("Please Enter Your State")
        $('#ModelMessege').modal('show')
        return false;

    }
    if (ZipCode == "") {
        $('#SpnMessege').text("Please Enter Your Zipcode")
        $('#ModelMessege').modal('show')
        return false;
    }


    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (Email != "") {
        if (emailReg.test(Email)) {

        }
        else {
            $('#SpnMessege').text("Please Enter proper Email Format!")
            $('#ModelMessege').modal('show')
            return false;
        }
    }
    else {
        $('#SpnMessege').text("Please Enter Email Id")
        $('#ModelMessege').modal('show')
        return false;
    }

    if (Password == "") {
        $('#SpnMessege').text("Please Enter Your Password")
        $('#ModelMessege').modal('show')
        return false;
    }
    else {
        var Count = Password.length
        if (Count > 12 || Count < 6) {
            $('#SpnMessege').text("Password Length Should be 6-12")
            $('#ModelMessege').modal('show')
            return false;
        }
    }

    return true;
}




function Redirect() {
    window.location.href = "../Account.aspx"
}