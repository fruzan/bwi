﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="ServiceType.aspx.cs" Inherits="BWI.Admin.ServiceType" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        tbody.scrollContent {
            display: block;
            height: 262px;
            overflow: auto;
            width: 100%;
        }

        div.scrollingDiv {
            width: auto;
            height: auto;
            overflow: scroll;
        }

        div.scrollingDiv2 {
            width: 100%;
            height: 600px;
            overflow-x: scroll;
            padding-right: 20px;
        }

        div.scrollingIndividual {
            width: auto;
            height: 330px;
            overflow-x: scroll;
            padding-right: 20px;
        }

        @media screen and (max-width: 992px) {

            span {
                font-size: 100%;
            }
        }

        @media screen and (max-width: 768px) {
            span {
                font-size: 12px;
            }
        }
    </style>
    <style type="text/css">
        input[type="text"] {
            width: 80%;
            margin-bottom: 5px;
        }
    </style>
    <script type="text/javascript" src="Scripts/ServiceType.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    

 <br />

    <table align="right" style="margin-right: 1.8%">
        <tbody>
            <tr>
                <td colspan="6"></td>

                <td style="padding-top: 3.2%"><a href="#">
                    <input type="button" class="popularbtn right" onclick="Open()" style="margin-right: 3.8%" value="+ Add New" title="Add New Car">
                </a></td>
                <td style="padding-top: 3.2%">
                    <input type="button" class="grid2btn active" style="cursor: pointer" title="Export To Excel" id="btnExport" onclick="ExportAgentDetailsToExcel()">
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <br>
    <br>
    <div class="offset-2">
        <div class="fblueline">
            Manage
           
            <span class="farrow"></span>
            <a style="color: white" href="ServiceType.aspx" title="AirPort Details"><b>AirPort</b></a>

        </div>
        <div class="frow1">

            <br />
            <br />

            <div class="table-responsive">
                <div id="tbl_StaffDetails_wrapper" class="dataTables_wrapper">

                    <table class="table table-striped table-bordered" id="tbl_StaffDetails" style="width: 100%;">
                        <thead>
                            <tr>
                                <td align="center" style="width: 10%"><b>S.N</b>
                                </td>
                                <td align="center"><b>Service Type</b>
                                </td>
                                <td align="center"><b>Edit</b>
                                </td>
                                <td align="center"><b>Delete</b>
                                </td>
                                <td align="center"><b>Active</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody id="Details">
<%--                            <tr class="odd">
                                <td align="center" style="width: 10%">1</td>
                                <td align="center">Pick And Drop</td>
                                <td align="center"><a style="cursor: pointer" onclick="UpdateDilogBox()" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a></td>
                                <td align="center"><a style="cursor: pointer" onclick="UpdateDilogBox()" href="#"><span class="glyphicon glyphicon-trash" title="Edit"></span></a></td>
                                <td align="center"><a style="cursor: pointer" onclick="UpdateDilogBox()" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a></td>
                            </tr>

                            <tr class="odd">
                                <td align="center" style="width: 10%">2</td>
                                <td align="center">Cab</td>
                                <td align="center"><a style="cursor: pointer" onclick="UpdateDilogBox()" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a></td>
                                <td align="center"><a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Edit"></span></a></td>
                                <td align="center"><a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a></td>
                            </tr>--%>
                        </tbody>
                    </table>

                </div>
            </div>
            <br>
        </div>
    </div>

    <div class="modal fade" id="Add" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Add Service</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>

                                    <tr>
                                        <td>
                                            <label>Name *</label>

                                            <input id="Fname" type="text" class="form-control" />

                                        </td>
                                        <%--    <td>
                                          
                                         <label>Service Type :</label>
                                            <select  class="form-control">
                                                <option value="0">Select Service Type</option>
                                                <option>Pick and Drop</option>
                                                <option>Cab</option>
                                         
                                            </select>
                                           
                                            </td>--%>
                                    </tr>


                                    <tr>
                                        <td colspan="2">


                                            <label>Description *</label>
                                            <br />
                                            <textarea class=" form-control" rows="4" cols="60" name="comment" id="Area"></textarea>
                                            <br />
                                            <div align="right">
                                                <input type="button" class="btn-search" value="Save" onclick="Insert()" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#Add').modal('hide')" />
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="Update" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Add Service</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>

                                    <tr>
                                        <td>
                                            <label>Name *</label>

                                            <input id="AFname" type="text" class="form-control" />

                                        </td>
                                        <%--  <td>
                                          
                                         <label>Service Type :</label>
                                            <select  class="form-control">
                                                <option value="0">Select Service Type</option>
                                                <option>Pick and Drop</option>
                                                <option>Cab</option>
                                         
                                            </select>
                                           
                                            </td>--%>
                                    </tr>


                                    <tr>
                                        <td colspan="2">


                                            <label>Description *</label>
                                            <br />
                                            <textarea class=" form-control" rows="4" cols="60" name="comment" id="AArea"></textarea>
                                            <br />
                                            <div align="right">
                                                <input type="button" class="btn-search" value="Update" onclick="Update();" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#Update').modal('hide')" />
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <input type="hidden" id="hdn" />
</asp:Content>
