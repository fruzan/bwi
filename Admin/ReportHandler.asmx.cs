﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;
using System.ComponentModel;
using BWI.DataLayer;
using System.Globalization;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for ReportHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ReportHandler : System.Web.Services.WebService
    {
        string json = "";
        DBHandlerDataContext DB = new DBHandlerDataContext();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
        //List<Trans_Reservation> ReservationList=new List<Trans_Reservation>();

        [WebMethod(EnableSession = true)]
        public string DriverActivityLoadAll()
        {
            var List = (from Obj in DB.Trans_Reservations select Obj).ToList();

            var NewList = List.Where(x => GetAppDate(x.ReservationDate) < DateTime.Now).ToList();

            if (NewList.Count > 0)
            {
                DataTable dtCustomerList = ConvertToDatatable(NewList);
                HttpContext.Current.Session["CustomerList"] = dtCustomerList;
                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                json = jsSerializer.Serialize(NewList);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string DriverActivity(string From, string To, string DriverId)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            List<Trans_Reservation> NewList = new List<Trans_Reservation>();
            var List = (from Obj in DB.Trans_Reservations where Obj.DriverSid == Convert.ToInt64(DriverId) && Obj.Status == "Completed" select Obj).ToList();
            if (From != "")
            {
                DateTime dFrom = GetAppDate(From);
                DateTime dTo = GetAppDate(To);
                NewList = List.Where(s => GetAppDate(s.ReservationDate) >= dFrom && GetAppDate(s.ReservationDate) <= dTo).ToList();
            }
            else
            {
                NewList = List;
            }
            if (NewList.Count > 0)
            {
                string DriverSid = (NewList[0].DriverSid).ToString();
                Trans_Tbl_Login Login = DB.Trans_Tbl_Logins.Single(x => x.Sid == Convert.ToInt64(DriverSid));
                Decimal Percentage = Convert.ToDecimal(Login.Percentage);

                Decimal TotalFare = 0;
                Decimal PercentageOfFare = 0;
                Decimal DriversPayOut = 0;
                Decimal Tip = 0;
                Decimal Fare = 0;
                foreach (var item in NewList)
                {
                    TotalFare = TotalFare + Convert.ToDecimal(item.TotalFare);
                    Fare = Fare + Convert.ToDecimal(item.Fare);
                    Tip = Tip + Convert.ToDecimal(item.GratuityAmount);
                }
                Percentage = Percentage / 100;
                #region Old
                //PercentageOfFare = (Percentage * Fare);
                #endregion
                TotalFare = TotalFare - Tip;
                PercentageOfFare = (Percentage * TotalFare);
                DriversPayOut = TotalFare - PercentageOfFare + Tip;
                //DriversPayOut = DriversPayOut + Tip;

                decimal Totalf = Math.Round(TotalFare, 2);

                DataTable dtDriverList = ConvertToDatatable(NewList);
                HttpContext.Current.Session["DriverList"] = dtDriverList;
               
                Session["DriverAmount"] = Math.Round(TotalFare, 2) + " " + Math.Round(PercentageOfFare, 2) + " " + Math.Round(DriversPayOut, 2) + " " + Math.Round(Tip, 2) + " " + Math.Round(Fare, 2);
                json = jsSerializer.Serialize(NewList);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"TotalFare\":\"" + TotalFare + "\",\"PercentageOfFare\":\"" + PercentageOfFare + "\",\"DriversPayOut\":\"" + DriversPayOut + "\",\"Tip\":\"" + Tip + "\",\"Fare\":\"" + Fare + "\",\"Arr\":[" + json + "]}";
                //json = js.Serialize(new { Session = 1, retCode = 1, TotalFare = TotalFare, PercentageOfFare = PercentageOfFare, DriversPayOut = DriversPayOut, Tip = Tip, Arr = json, Fare = Fare });
            }

            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAlCustomerName()
        {
            var List = ((from Customer in DB.Trans_Tbl_CustomerMasters
                         join Res in DB.Trans_Reservations on Customer.sid equals Res.uid
                         select Customer).Distinct()).ToList();

            if (List.Count > 0)
            {
                json = jsSerializer.Serialize(List);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"tblCustomer\":[" + json + "]}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string CustomerActivity(string From, string To, Int64 uid, string ResNo, string PhoneNo)
        {
            var List = (from Obj in DB.Trans_Reservations select Obj).ToList();

            if (uid != 0)
            {
                List = List.Where(s => s.uid == uid).ToList();
            }
            if (From != "" && To != "")
            {
                DateTime dFrom = GetAppDate(From);
                DateTime dTo = GetAppDate(To);
                List = List.Where(s => GetAppDate(s.ReservationDate) >= dFrom && GetAppDate(s.ReservationDate) <= dTo).ToList();
            }
            if (ResNo != "")
            {
                List = List.Where(s => s.ReservationID == ResNo).ToList();
            }
            if (PhoneNo != "")
            {
                List = List.Where(s => s.ContactNumber == PhoneNo).ToList();
            }
            if (List.Count > 0)
            {
                DataTable dtCustomerList = ConvertToDatatable(List);
                Decimal TotalFare = 0;
                Decimal PercentageOfFare = 0;
                Decimal DriversPayOut = 0;
                Decimal Tip = 0;
                Decimal Fare = 0;
                foreach (var item in List)
                {
                    TotalFare = TotalFare + Convert.ToDecimal(item.TotalFare);
                    Fare = Fare + Convert.ToDecimal(item.Fare);
                    Tip = Tip + Convert.ToDecimal(item.GratuityAmount);
                }
                Decimal Percentage = Convert.ToDecimal(0);
                Percentage = Percentage / 100;
                PercentageOfFare = (Percentage * Fare);
                DriversPayOut = TotalFare - PercentageOfFare;
                //DriversPayOut = DriversPayOut + Tip;

                decimal Totalf = Math.Round(TotalFare, 2);

                Session["CustomerAmount"] = Math.Round(TotalFare, 2) + " " + Math.Round(PercentageOfFare, 2) + " " + Math.Round(DriversPayOut, 2) + " " + Math.Round(Tip, 2) + " " + Math.Round(Fare, 2);

                HttpContext.Current.Session["CustomerList"] = dtCustomerList;
                json = jsSerializer.Serialize(List);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetByType(string From, string To, string Type)
        {
            List<Trans_Reservation> NewList = new List<Trans_Reservation>();
            var List = (from Obj in DB.Trans_Reservations select Obj).ToList();
            if (Type != "All")
                List = (from Obj in DB.Trans_Reservations where Obj.Status == Type select Obj).ToList();
            if (From != "")
            {
                DateTime dFrom = GetAppDate(From);
                DateTime dTo = GetAppDate(To);
                NewList = List.Where(s => GetAppDate(s.ReservationDate) >= dFrom && GetAppDate(s.ReservationDate) <= dTo).ToList();
            }
            else
            {
                NewList = List;
            }
            if (NewList.Count > 0)
            {
                string DriverSid = (NewList[0].DriverSid).ToString();

                Decimal Percentage = 0;

                Decimal TotalFare = 0;
                Decimal PercentageOfFare = 0;
                Decimal DriversPayOut = 0;
                Decimal Tip = 0;
                Decimal Fare = 0;
                foreach (var item in NewList)
                {
                    TotalFare = TotalFare + Convert.ToDecimal(item.TotalFare);
                    Fare = Fare + Convert.ToDecimal(item.Fare);
                    Tip = Tip + Convert.ToDecimal(item.GratuityAmount);
                    if (item.AssignedTo != "not assign" && item.AssignedTo != null)
                    {
                        Trans_Tbl_Login Login = DB.Trans_Tbl_Logins.Single(x => x.Sid == Convert.ToInt64(item.DriverSid));
                        Percentage = (Convert.ToDecimal(item.Fare) * Convert.ToDecimal(Login.Percentage)) / 100;
                        PercentageOfFare = PercentageOfFare + Percentage;
                    }
                }
                //Percentage = Percentage / 100;
                //PercentageOfFare = (Percentage * TotalFare);
                DriversPayOut = TotalFare - PercentageOfFare;
                //DriversPayOut = DriversPayOut + Tip;

                decimal Totalf = Math.Round(TotalFare, 2);
                DataTable dtReservationList = ConvertToDatatable(NewList);
                HttpContext.Current.Session["CustomerList"] = dtReservationList;
                Session["CustomerAmount"] = Math.Round(TotalFare, 2) + " " + Math.Round(PercentageOfFare, 2) + " " + Math.Round(DriversPayOut, 2) + " " + Math.Round(Tip, 2) + " " + Math.Round(Fare, 2);
                json = jsSerializer.Serialize(NewList);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"TotalFare\":\"" + TotalFare + "\",\"PercentageOfFare\":\"" + PercentageOfFare + "\",\"DriversPayOut\":\"" + DriversPayOut + "\",\"Tip\":\"" + Tip + "\",\"Arr\":[" + json + "]}";
            }

            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
            //if (Type == "All")
            //{
            //    var List = (from Obj in DB.Trans_Reservations select Obj).ToList();

            //    var NewList = List.Where(s => ConvertDateTime(s.ReservationDate) >= dFrom && ConvertDateTime(s.ReservationDate) <= dTo).ToList();
            //    DataTable dtReservationList = ConvertToDatatable(NewList);
            //    HttpContext.Current.Session["CustomerList"] = dtReservationList;
            //    if (NewList.Count > 0)
            //    {

            //        json = jsSerializer.Serialize(NewList);
            //        json = json.TrimEnd(']');
            //        json = json.TrimStart('[');
            //        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            //    }

            //    else
            //    {
            //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            //    }
            //}

            //else
            //{

            //    var List = (from Obj in DB.Trans_Reservations where Obj.Status == Type select Obj).ToList();
            //    DataTable dtReservationList = ConvertToDatatable(List);
            //    HttpContext.Current.Session["CustomerList"] = dtReservationList;
            //   // var NewList = List.Where(s => ConvertDateTime(s.ReservationDate) >= dFrom && ConvertDateTime(s.ReservationDate) <= dTo).ToList();

            //    if (List.Count > 0)
            //    {

            //        json = jsSerializer.Serialize(List);
            //        json = json.TrimEnd(']');
            //        json = json.TrimStart('[');
            //        json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            //    }

            //    else
            //    {
            //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            //    }
            //}


            // return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAllOldReservation()
        {
            List<OldReservation> NewList = new List<OldReservation>();
            var List = (from Obj in DB.tbl_OldReservations select new {
                Username = Obj.fname + " " + Obj.lname,
                Obj.ReservationNumber,
                Obj.AssignedTo,
                Obj.hour,
                Obj.service,
                Obj.airport,
                Obj.Completed,
                Obj.flightdate
            }).ToList();
            //var data3 = List.Select(c => new { c.fname+""+c.lname});
            if (List.Count > 0)
            {
                DataTable tblResult = ConvertToDatatable(List);
                //json = jsSerializer.Serialize(List);
                //json = json.TrimEnd(']');
                //json = json.TrimStart('[');
                //json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
                List<Dictionary<string, object>> tbl = new List<Dictionary<string, object>>();
                tbl = ConvertDataTable(tblResult);
                Int64 re = jsSerializer.MaxJsonLength;
                jsSerializer.MaxJsonLength = Int32.MaxValue;
                List<string> DateList = new List<string>();
                for (int i = 0; i < tblResult.Rows.Count; i++)
                {
                    DateList.Add(tblResult.Rows[i]["flightdate"].ToString().Split(' ')[0]);
                }
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, Arr = tbl, DateList = DateList });
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        public static DateTime GetAppDate(string Date)
        {
            // string Date = Row["AppliedDate"].ToString();
            DateTime date = new DateTime();
            Date = (Date.Split(' ')[0]).Replace("/", "-");
            try
            {
                string[] formats = {"M/d/yyyy", "MM/dd/yyyy",
                                "d/M/yyyy", "dd/MM/yyyy",
                                "yyyy/M/d", "yyyy/MM/dd",
                                "M-d-yyyy", "MM-dd-yyyy",
                                "d-M-yyyy", "dd-MM-yyyy",
                                "yyyy-M-d", "yyyy-MM-dd",
                                "M.d.yyyy", "MM.dd.yyyy",
                                "d.M.yyyy", "dd.MM.yyyy",
                                "yyyy.M.d", "yyyy.MM.dd",
                                "M,d,yyyy", "MM,dd,yyyy",
                                "d,M,yyyy", "dd,MM,yyyy",
                                "yyyy,M,d", "yyyy,MM,dd",
                                "M d yyyy", "MM dd yyyy",
                                "d M yyyy", "dd MM yyyy",
                                "yyyy M d", "yyyy MM dd",
                                "m/dd/yyyy hh:mm:ss tt",
                               };
                //DateTime dateValue;
                foreach (string dateStringFormat in formats)
                {
                    if (DateTime.TryParseExact(Date, dateStringFormat, CultureInfo.CurrentCulture, DateTimeStyles.None,
                                               out date))
                    {
                        date.ToShortDateString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
            return date;
        }

        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                // string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;

                string CurrentPattern = "mm-dd-yyyy";
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;
        }

        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static List<Dictionary<string, object>> ConvertDataTable(DataTable dtResult)
        {
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dtResult.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dtResult.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return parentRow;
        }

        //public static DataTable GetReservationDataTable()
        //{

        //    var Listed = System.Web.HttpContext.Current.Session["ReservationList"];

        //    DataTable dtReservation = ConvertToDatatable(Listed);

        //    return dtReservation;
        //}

        //private static DataTable ConvertToDatatable<T>(List<T> data)
        //{
        //    PropertyDescriptorCollection props =
        //        TypeDescriptor.GetProperties(typeof(T));
        //    DataTable table = new DataTable();
        //    for (int i = 0; i < props.Count; i++)
        //    {
        //        PropertyDescriptor prop = props[i];
        //        if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
        //            table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
        //        else
        //            table.Columns.Add(prop.Name, prop.PropertyType);
        //    }
        //    object[] values = new object[props.Count];
        //    foreach (T item in data)
        //    {
        //        for (int i = 0; i < values.Length; i++)
        //        {
        //            values[i] = props[i].GetValue(item);
        //        }
        //        table.Rows.Add(values);
        //    }
        //    return table;
        //}
    }
}
