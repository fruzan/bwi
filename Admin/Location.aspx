﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="Location.aspx.cs" Inherits="BWI.Admin.Location" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/Location.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <br>
    <div class="offset-2">
        <div id="alSuccess" class="alert alert-success alert-dismissible" role="alert" style="display: none">
            <span id="spnMsg">Staff status has been changed successfully!</span>
            <button type="button" class="close" onclick="hidealert()"><span aria-hidden="true">×</span></button>
        </div>
        <div id="alError" class="alert alert-danger alert-dismissible" role="alert" style="display: none">
            Something went wrong while processing your request! Please try again.
                   
            <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="hidealert()"><span aria-hidden="true">×</span></button>
        </div>
    </div>
    <table align="right" style="margin-right: 1.8%">
        <tbody>
            <tr>
                <td></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td style="padding-top: 3.2%"><a href="#">
                    <input type="button" class="popularbtn right" onclick="AddNewLocation()" style="margin-right: 3.8%" value="+ Add New" title="Add New Airport" />
                </a></td>
                <%--<td style="padding-top: 3.2%">
                    <input type="button" class="grid2btn active" style="cursor: pointer" title="Export To Excel" id="btnExport" onclick="ExportAgentDetailsToExcel()" />
                </td>--%>
            </tr>
        </tbody>
    </table>
    <br />
    <br />
    <br />
    <div class="offset-2">
        <div class="fblueline">
            Add Master
           
            <span class="farrow"></span>
            <a style="color: white" href="Location.aspx" title="Airport"><b>Airport</b></a>

        </div>
        <div class="frow1">

            <br />
            <br />

            <div class="table-responsive">
                <div id="tbl_StaffDetails_wrapper" class="dataTables_wrapper" role="grid">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered dataTable" id="tbl_VehicleManufacturer" aria-describedby="tbl_VehicleManufacturer_info" style="width: 100%;">
                        <thead>
                            <tr role="row">
                                <td class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="tbl_StaffDetails" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Staff Detail
                            : activate to sort column descending"
                                    style="width: 152px;"><b>Airport Name</b>
                                </td>
                                <td class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="tbl_StaffDetails" rowspan="1" colspan="1" aria-label="Email | Password Manage
                            : activate to sort column ascending"
                                    style="width: 174px;"><b>Latitude</b>
                                </td>
                                <td class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="tbl_StaffDetails" rowspan="1" colspan="1" aria-label="Email | Password Manage
                            : activate to sort column ascending"
                                    style="width: 174px;"><b>Longitude</b>
                                </td>
                                <td class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="tbl_StaffDetails" rowspan="1" colspan="1" aria-label="Email | Password Manage
                            : activate to sort column ascending"
                                    style="width: 174px;"><b>Change Status</b>
                                </td>
                                <td align="center" class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="tbl_StaffDetails" rowspan="1" colspan="1" aria-label="Edit nbsp;| nbsp;Delete
                            : activate to sort column ascending"
                                    style="width: 153px;"><b>Edit &nbsp;</b>
                                </td>
                            </tr>
                        </thead>
                        <tbody  id="LocationDetails">

                        </tbody>
                    </table>
                </div>
            </div>
            <br />
        </div>
    </div>


    <div class="modal fade" id="AddNewLocation" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 75%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="dlg_Manufacturer">Add New Airport</b></div>
                        <div class="frow2">
                            <br />
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Airport :  </label>
                                    <input id="txt_Location" type="text" class="form-control" />
                                   <center> <label>Ex: Airport Name</label></center> <br/>
                                   <center>  <input type="button" style="margin-top: -18px;" onclick="GetLocation()" class="btn-search" value="Search" title="Add Location" id="btn_AddLocation" /></center> 
                                </div>
                                <div class="col-md-3">
                                    <label>Latitude :  </label>
                                    <input id="txt_Latitude" type="text" class="form-control" />
                                </div>
                                <div class="col-md-3">
                                    <label>Longitude : </label>
                                    <input id="txt_Longitude" type="text" class="form-control" />
                                </div>
                                <div class="col-md-1">
                                    <input type="button" style="margin-top: 24px;" onclick="AddLocation()" class="btn-search" value="ADD" title="Add Location" id="btn_AddLocation1" />
                                </div>
                            </div>
                            <br />
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <input type="hidden" id="hdnsid"/>
</asp:Content>
