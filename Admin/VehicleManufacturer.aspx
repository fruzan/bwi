﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="VehicleManufacturer.aspx.cs" Inherits="BWI.Admin.VehicleManufacturer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        tbody.scrollContent {
            display: block;
            height: 262px;
            overflow: auto;
            width: 100%;
        }

        div.scrollingDiv {
            width: auto;
            height: auto;
            overflow: scroll;
        }

        div.scrollingDiv2 {
            width: 100%;
            height: 600px;
            overflow-x: scroll;
            padding-right: 20px;
        }

        div.scrollingIndividual {
            width: auto;
            height: 330px;
            overflow-x: scroll;
            padding-right: 20px;
        }

        @media screen and (max-width: 992px) {

            span {
                font-size: 100%;
            }
        }

        @media screen and (max-width: 768px) {
            span {
                font-size: 12px;
            }
        }
    </style>
    <style type="text/css">
        input[type="text"] {
            width: 80%;
            margin-bottom: 5px;
        }
    </style>

    <script type="text/javascript" src="Scripts/VehicleManufacturer.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    

 <br>

    <table align="right" style="margin-right: 1.8%">
        <tbody>
            <tr>
                <td colspan="6"></td>
       
                <td style="padding-top: 3.2%"><a href="#">
                    <input type="button" class="popularbtn right" onclick="AddNewManufacturer()" style="margin-right: 3.8%" value="+ Add New" title="Add New Manufacturer" />
                </a></td>
                <%--<td style="padding-top: 3.2%">
                    <input type="button" class="grid2btn active" style="cursor: pointer" title="Export To Excel" id="btnExport" onclick="ExportAgentDetailsToExcel()" />
                </td>--%>
            </tr>
        </tbody>
    </table>
    <br />
    <br />
    <br />
    <div class="offset-2">
        <div class="fblueline">
            Add Master
           
            <span class="farrow"></span>
            <a style="color: white" href="VehicleManufacturer.aspx" title="Vehicle Manufacturer"><b>Vehicle Manufacturer</b></a>

        </div>
        <div class="frow1">

            <br />
            <br />

            <div class="table-responsive">
                <div id="tbl_StaffDetails_wrapper" class="dataTables_wrapper" role="grid">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered dataTable" id="tbl_VehicleManufacturer" aria-describedby="tbl_VehicleManufacturer_info" style="width: 100%;">
                        <thead>
                            <tr role="row">
                                <td align="center" class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="tbl_StaffDetails" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Staff Detail
                            : activate to sort column descending"
                                    style="width: 152px;"><b>Sr. No.</b>
                                </td>
                                <td align="center" class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="tbl_StaffDetails" rowspan="1" colspan="1" aria-label="Email | Password Manage
                            : activate to sort column ascending"
                                    style="width: 174px;"><b>Name</b>
                                </td>
                                <td align="center" class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="tbl_StaffDetails" rowspan="1" colspan="1" aria-label="Email | Password Manage
                            : activate to sort column ascending"
                                    style="width: 174px;"><b>Change Status</b>
                                </td>
                                <td align="center" class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="tbl_StaffDetails" rowspan="1" colspan="1" aria-label="Edit nbsp;| nbsp;Delete
                            : activate to sort column ascending"
                                    style="width: 153px;"><b>Edit</b>
                                </td>
                            </tr>
                        </thead>
                        <tbody  id="ManufacturerDetails">

                        </tbody>
                    </table>
                </div>
            </div>
            <br />
        </div>
    </div>
    
    <input type="hidden" id="hdn"/>
    <div class="modal fade" id="AddNewManufacturer" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="dlg_Manufacturer">Add Manufacturer</b></div>
                        <div class="frow2">
                            <br />
                            <br />
                            <div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-2">
                                    Name :
                                </div>
                                <div class="col-md-6">
                                    <input id="txt_Mname" type="text" class="form-control" />
                                </div>
                                <div class="col-md-1">
                                    <input type="button" style="margin-top: 0px" onclick="AddManufacturer()" class="btn-search" value="ADD" title="Add Manufacturer" id="btn_AddManufacturer" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

     <div class="modal fade" id="Update" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="dlg_Manufacturer">Add Manufacturer</b></div>
                        <div class="frow2">
                            <br />
                            <br />
                            <div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-2">
                                    Name :
                                </div>
                                <div class="col-md-6">
                                    <input id="Atxt_Mname" type="text" class="form-control" />
                                </div>
                                <div class="col-md-1">
                                    <input type="button" style="margin-top: 0px" onclick="Update()" class="btn-search" value="Update" title="Add Manufacturer"  />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</asp:Content>
