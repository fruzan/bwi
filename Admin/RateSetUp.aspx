﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="RateSetUp.aspx.cs" Inherits="BWI.Admin.RateSetUp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    

<br />

    <table align="right" style="margin-right: 1.8%">
        <tbody>
            <tr>
                <td colspan="6"></td>

                <td style="padding-top: 3.2%"><a href="#">
                    <input type="button" class="popularbtn right" onclick="$('#AddDilogBox').modal('show')" style="margin-right: 3.8%" value="+ Add New" title="Add New Car">
                </a></td>
                <td style="padding-top: 3.2%">
                    <input type="button" class="grid2btn active" style="cursor: pointer" title="Export To Excel" id="btnExport" onclick="ExportAgentDetailsToExcel()">
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <br>
    <br>
    <div class="offset-2">
        <div class="fblueline">
            Rate Master
           
            <span class="farrow"></span>
            <a style="color: white" href="DriverDetails.aspx" title="Staff Details"><b>Rate Set Up</b></a>

        </div>
        <div class="frow1">

            <br />
            <br />

            <div class="table-responsive">
                <div id="tbl_StaffDetails_wrapper" class="dataTables_wrapper">

                    <table class="table table-striped table-bordered" id="tbl_StaffDetails" style="width: 100%;">
                        <thead>
                            <tr>
                                <td align="center" style="width: 10%"><b>S.N</b>
                                </td>
                                <td align="center"><b>Name</b>
                                </td>
                                <td align="center"><b>Upto (Miles)</b>
                                </td>
                                <td align="center"><b>Base Charge</b>
                                </td>

                                 <td align="center"><b>Cost Per Distance</b>
                                </td>
                                  <td align="center"><b>Miles Per Distance</b>
                                </td>
                                <td align="center"><b>Edit</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody id="DriverDetails">
                            <tr class="odd">
                                <td align="center" style="width: 10%">1</td>
                                <td align="center">Sedan</td>
                                <td align="center">50.0 Miles</td>
                                <td align="center">50</td>
                                 <td align="center">3</td>
                                <td align="center">2</td>
                                <td align="center"><a style="cursor: pointer" onclick='$('#AddDilogBox').modal('hide')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a></td>
                            </tr>
                            <tr class="odd">
                                <td align="center" style="width: 10%">1</td>
                                <td align="center">Shuttle</td>
                                <td align="center">100.0 Miles</td>
                                <td align="center">40</td>
                                 <td align="center">2</td>
                                <td align="center">1</td>
                                <td align="center"><a style="cursor: pointer" onclick='$('#AddDilogBox').modal('hide')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a></td>
                            </tr>

                        </tbody>
                    </table>

                </div>
            </div>
            <br>
        </div>
    </div>

    <div class="modal fade" id="AddDilogBox" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Rate Set Up</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>Name *</label>
                                            <input id="txtName" type="text" class="form-control" />
                                        </td>
                                        <td>
                                            <label>Up to(Mile) :</label>
                                            <input id="txt_BaseFare" type="text" class="form-control" />
                                        </td>
                                        <td>
                                            <label>Base Charge :</label>
                                            <input id="txt_PerMile" type="text" class="form-control" />
                                        </td>
                                    </tr>


                                       <tr>
                                        <td>
                                            <label>Cost Per Distance:</label>
                                            <input id="txtName" type="text" class="form-control" />
                                        </td>
                                        <td>
                                            <label>Miles Per Distance :</label>
                                            <input id="txt_BaseFare" type="text" class="form-control" />
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">

                                            <div align="right">
                                                <input type="button" class="btn-search" value="Save" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#AddDilogBox').modal('hide')" />
                                            </div>
                                        </td>
                                    </tr>



                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="UpdateDilogBox" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Rate Set Up</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                                                  <tr>
                                        <td>
                                            <label>Name *</label>
                                            <input id="txtName" type="text" class="form-control" />
                                        </td>
                                        <td>
                                            <label>Up to(Mile) :</label>
                                            <input id="txt_BaseFare" type="text" class="form-control" />
                                        </td>
                                        <td>
                                            <label>Base Charge :</label>
                                            <input id="txt_PerMile" type="text" class="form-control" />
                                        </td>
                                    </tr>


                                       <tr>
                                        <td>
                                            <label>Cost Per Distance:</label>
                                            <input id="txtName" type="text" class="form-control" />
                                        </td>
                                        <td>
                                            <label>Miles Per Distance :</label>
                                            <input id="txt_BaseFare" type="text" class="form-control" />
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">

                                            <div align="right">
                                                <input type="button" class="btn-search" value="Save" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#UpdateDilogBox').modal('hide')" />
                                            </div>
                                        </td>
                                    </tr>



                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</asp:Content>
