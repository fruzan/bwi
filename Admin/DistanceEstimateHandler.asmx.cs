﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using BWI.BL;
using BWI.DataLayer;
using BWI.Admin.DataLayer;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for DistanceEstimateHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DistanceEstimateHandler : System.Web.Services.WebService
    {
        string json = "";
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        DBHandlerDataContext DB = new DBHandlerDataContext();

        [WebMethod(EnableSession = true)]
        public string VehicleNameDropDown()
        {
            List<BWI.Trans_tbl_Vehicle_Info> List = null;
            retCode = DistanceEstimateManager.ModelNameDropDown(out List);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(List);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"VehicleName\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string AddDistanceRate(string Group, string VehName, string BaseDistance, string BaseCharge, string CostDistance, string MilesDistance, string UptoMiles)
        {
            //var List = (from obj in DB.Trans_Tbl_CarTypes where obj.Name == VehName select obj).ToList();
            var List = (from obj in DB.Trans_Tbl_DistanceRates where obj.Name == VehName select obj).ToList();

            if (List.Count > 0)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"2\"}";
            }
            else
            {
                retCode = DistanceEstimateManager.AddDistanceRate(Group, VehName, BaseDistance, BaseCharge, CostDistance, MilesDistance, UptoMiles);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAllRates()
        {
            List<BWI.Trans_Tbl_DistanceRate> List = null;
            retCode = DistanceEstimateManager.GetAllRates(out List);


            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(List);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"RateDetails\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetRate(Int64 sid)
        {
            List<BWI.Trans_Tbl_DistanceRate> List = null;
            retCode = DistanceEstimateManager.GetRate(sid, out List);


            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(List);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"RateDetails\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateRate(Int64 sid, string Group, string VehName, string BaseDistance, string BaseCharge, string CostDistance, string MilesDistance, string UptoMiles)
        {
            retCode = DistanceEstimateManager.UpdateRate(sid, Group, VehName, BaseDistance, BaseCharge, CostDistance, MilesDistance, UptoMiles);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteRate(Int64 sid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_DistanceRate HourlyRate = DB.Trans_Tbl_DistanceRates.Single(x => x.sid == sid);
                DB.Trans_Tbl_DistanceRates.DeleteOnSubmit(HourlyRate);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }
    }
}

