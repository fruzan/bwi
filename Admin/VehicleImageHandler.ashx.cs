﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for VehicleImageHandler
    /// </summary>
    public class VehicleImageHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            try
            {
                string dirFullPath = HttpContext.Current.Server.MapPath("~/Admin/VehicleImage/");
                string[] files;
                int numFiles;
                files = System.IO.Directory.GetFiles(dirFullPath);
                numFiles = files.Length;
                numFiles = numFiles + 1;
                Guid DocumentName = Guid.NewGuid();
                string str_image = "";

                foreach (string s in context.Request.Files)
                {
                    HttpPostedFile file = context.Request.Files[s];
                    string fileName = file.FileName;
                    string fileExtension = file.ContentType;

                    if (!string.IsNullOrEmpty(fileName))
                    {
                        fileExtension = Path.GetExtension(fileName);
                        str_image = "Vehicle_" + DocumentName.ToString() + fileExtension;
                        string pathToSave_100 = HttpContext.Current.Server.MapPath("~/Admin/VehicleImage/") + str_image;
                        file.SaveAs(pathToSave_100);
                    }
                }
                //  database record update logic here  ()

                context.Response.Write(str_image);
            }
            catch (Exception ac)
            {

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}