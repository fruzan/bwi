﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="CustomerActivity.aspx.cs" Inherits="BWI.Admin.CustomerActivity" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#datepicker1").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "mm-dd-yy"
            });

            $("#datepicker2").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "mm-dd-yy"
            });
            GetAllCustomer()
            GetAll()
        });

    </script>
    <script type="text/javascript" src="Scripts/CustomerReport.js?v=1.2"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="container">
        <div class="row">
              <div class="col-md-4">
                  <label>From</label>
                    <input type="text" class="form-control datepicker" id="datepicker1" style="cursor: pointer; background: url('../../images/calendar.png') #fff no-repeat right;" placeholder="mm-dd-yyyy">
    
            </div>
              <div class="col-md-4">
                   <label>To</label>
                    <input type="text" class="form-control datepicker" id="datepicker2" style="cursor: pointer; background: url('../../images/calendar.png') #fff no-repeat right; padding-left: 25px;" placeholder="mm-dd-yyyy">
    
              </div>
              <div class="col-md-4">
                  <label>Customer Name</label>
                    <%--<select id="sel_driver" class="form-control"></select>--%>
                   <input id="sel_Customer" class="form-control"  list="sel_Customers"/ value="" />
                                    <datalist id="sel_Customers"></datalist>   


              </div>
              
        </div>
         <div class="row">
             <div class="col-md-4" style="padding-top: 10px;">
                       <label>Resevation No.</label>
                    <input type="text" id="ResevationNo" class="form-control"/>
  
                  </div>
             
              <div class="col-md-4" style="padding-top: 10px;">
                     <label>Phone No.</label>
                    <input type="text" id="PhoneNo" class="form-control" />
             </div>
             <div class="col-md-4" style="padding-top: 20px;">
                    <input type="button" class="btn-search" value="Search" id="btn_Search" onclick="Submit();">
                <img src="../images/ExportToExcel.png" title="Export To Excel" onclick="ExportToExcel()" alt="Export To Excel" style="height:40px;width:40px;padding-left: 5px;cursor:pointer;padding-top: 10px;">

             </div>
             
         </div>
        <br />
        
    </div>
    <div class="offset-2">
        <div class="fblueline">
            Reports
           
            <span class="farrow"></span>
            <a style="color: white" href="CustomerActivity.aspx" title="Customer Details"><b>Customer Details</b></a>

        </div>
        <div class="frow1">

            <br>
            <br>

            <div class="table-responsive">
                <div style="width: auto; height: 740px; overflow: scroll;">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered dataTable" id="Details" style="width: 100%;">
                        <thead>
                            <tr>
                                <td align="center"><b>S.N</b>
                                </td>
                                <td align="center"><b>Reservation No</b>
                                </td>
                                <td align="center"><b>Assigned To</b>
                                </td>
                                <td align="center"><b>Guest Name</b>
                                </td>
                                 <td align="center"><b>Phone No</b>
                                </td>
                                <td align="center"><b>Reservation Date</b>
                                </td>
                                <td align="center"><b>Service</b>
                                </td>
                                <td align="center"><b>Source</b>
                                </td>
                                 <td align="center"><b>Destination</b>
                                </td>
                               <%-- <td align="center"><b>Tip</b>
                                </td>--%>
                                <td align="center"><b>Total Fare</b>
                                </td>
                                 <td align="center"><b>Make Reservation</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>

                </div>
            </div>
            <br>
        </div>
    </div>
    <div class="modal fade" id="AddReserevation" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Select Reservation Type</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>Reservation Type :</label>
                                            <select id="ResType" class="form-control">
                                                <option value="Airport Reservation">Airport Reservation</option>
                                                <option value="PointToPoint Reservation">PointToPoint Reservation</option>
                                                 <option value="Hourly Reservation">Hourly Reservation</option>
                                                 <option value="Two Stop Shuttle Reservation">Two Stop Shuttle Reservation</option>
                                            </select>
                                        </td>
                                         <td>

                                            <div align="right">
                                                <input type="button" class="btn-search" value="Submit" onclick="Redirect()" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#AddReserevation').modal('hide')" />
                                            </div>
                                        </td>
                                    </tr>
                                    <input type="text" id="txt_Account" name="Text[]" placeholder=" Account Number " class="form-control" style="display:none" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_AccountNumber">
                                        <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Code" placeholder="Organisation Code" class="form-control" style="display:none" />

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</asp:Content>
