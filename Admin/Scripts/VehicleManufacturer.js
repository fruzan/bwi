﻿$(document).ready(function () {
    GetAllManufacturer();
});

var Mac_Name;
var MacDetails;
function AddManufacturer() {
    if ($("#txt_Mname").val() == "") {
        alert("Please enter Name")
    }
    else {
        Mac_Name = $("#txt_Mname").val();
        var data = {
            Mac_Name: Mac_Name
        }
        $.ajax({
            type: "POST",
            url: "VehicleManufacturerHandler.asmx/AddManufacturer",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (obj.retCode == 1) {
                    $('#AddNewManufacturer').modal('hide')
                    GetAllManufacturer();
                    $('#SpnMessege').text("Vehicle Manufacturer Inserted Successfully.")
                    $('#ModelMessege').modal('show')
                }

                else {
                    $('#SpnMessege').text("Something Went Wrong")
                    $('#ModelMessege').modal('show')
                }
            }
        });
    }
}

function GetAllManufacturer() {
    $.ajax({
        type: "POST",
        url: "VehicleManufacturerHandler.asmx/GetAllManufacturer",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Manufacturer = obj.ManufacturerDetails;
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var ul;
            $("#ManufacturerDetails").empty();
            if (obj.retCode == 1) {
                for (var i = 0; i < Manufacturer.length; i++) {
                    ul += '<tr class="odd">'
                    ul += '<td class=" ">' + (i+1) + '</td>'
                    ul += '<td class=" ">' + Manufacturer[i].Name + '</td>'
                    var status = Manufacturer[i].IsActive;
                    if (status == true) {
                        //ul += '<td align="center" ><a style="cursor: pointer" onclick="Activate(\'' + CarDetails[i].sid + '\')" href="#"><span class="glyphicon glyphicon-eye-open" title="Edit"></span></a> | <a style="cursor: pointer" onclick="OpenUpdatePopUp(\'' + CarDetails[i].sid + '\',\'' + CarDetails[i].Name + '\',\'' + CarDetails[i].Description + '\')"><span class="glyphicon glyphicon-edit" title="Edit"></span></a>| <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="Delete(' + CarDetails[i].sid + ')"></span></a></td>';
                        ul += '<td align="center" style="width:10%"><span class="glyphicon glyphicon-eye-open" onclick="ActivateVehicleMac(' + Manufacturer[i].sid + ')" style="cursor:pointer" title="Activate/Deactivate"></span></td>'
                    }
                    else {
                        //ul += '<td align="center" ><a style="cursor: pointer" onclick="Activate(\'' + CarDetails[i].sid + '\')" href="#"><span class="fa fa-eye-slash" title="Edit"></span></a> | <a style="cursor: pointer" onclick="OpenUpdatePopUp(\'' + CarDetails[i].sid + '\',\'' + CarDetails[i].Name + '\',\'' + CarDetails[i].Description + '\')"><span class="glyphicon glyphicon-edit" title="Edit"></span></a>| <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="Delete(' + CarDetails[i].sid + ')"></span></a></td>';
                        ul += '<td align="center" style="width:10%"><span class="glyphicon glyphicon-eye-close" onclick="ActivateVehicleMac(' + Manufacturer[i].sid + ')" style="cursor:pointer" title="Activate/Deactivate"></span></td>'
                    }
                    //ul += '<td align="center" class=" "><a style="cursor: pointer" onclick="UpdateDilogBox(' + Manufacturer[i].sid + ')" href="#"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a>&nbsp;&nbsp;| <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="Delete(' + Manufacturer[i].sid + ')"></span></a></td>'
                    ul += '<td align="center" class=" "><a style="cursor: pointer" onclick="UpdateDilogBox(' + Manufacturer[i].sid + ')" href="#"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a>&nbsp;&nbsp;</td>'
                    ul += '</tr>'
                }
                var s = ul.split('undefined')
                MacDetails = s[1];
                $("#ManufacturerDetails").append(MacDetails);
            }

            else {

            }
        }
    });
}

function Delete(sid) {

    var data = { Sid: sid }
    debugger;
    $.ajax({
        type: "POST",
        url: "VehicleManufacturerHandler.asmx/Delete",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;

                GetAllManufacturer();
                $('#SpnMessege').text("Record Deleted")
                $('#ModelMessege').modal('show')

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not Deleted")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function UpdateDilogBox(sid) {
    debugger;

    $('#hdn').val(sid);
    var data = { sid: sid }
    debugger;
    $.ajax({
        type: "POST",
        url: "VehicleManufacturerHandler.asmx/GetSingle",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;

                $('#Atxt_Mname').val(Arr[0].Name);
                $('#Update').modal('show')


            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated Successfully")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function Update() {
    debugger;
    var sid = $('#hdn').val()
    var Name = $('#Atxt_Mname').val()
    if (Name == '')
    {

        $('#SpnMessege').text("Please Enter Vehicle Manufacturer Name")
        $('#ModelMessege').modal('show')
        $("#txt_Mname").focus();
        return false;
    }
    var data = { Sid: sid, Name: Name }
    debugger;
    $.ajax({
        type: "POST",
        url: "VehicleManufacturerHandler.asmx/Update",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                $('#Update').modal('hide')
                GetAllManufacturer();
                $('#SpnMessege').text("Vehicle Manufacturer updated Successfully")
                $('#ModelMessege').modal('show')


            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function ActivateVehicleMac(sid) {
    debugger
    var data = {
        Sid: sid,
    }
    $.ajax({
        type: "POST",
        url: "VehicleManufacturerHandler.asmx/ActivateVehicleMac",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                GetAllManufacturer();
                //$('#SpnMessege').text("Vehical Deactivated.")
                //$('#ModelMessege').modal('show')
            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record not Deactivated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });

}

function AddNewManufacturer() {
    $("#txt_Mname").val('')
    $("#AddNewManufacturer").modal("show")
}