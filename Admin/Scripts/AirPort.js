﻿$(function () {
    Load();
})

function Load() {
    debugger;
    $.ajax({
        type: "Post",
        url: "AirPortHandler.asmx/Load",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);

            //var jQueryXml = $(data);
            //var test = data.lastChild.innerHTML;
            //var obj = JSON.parse(test);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                var Content = '';

                for (var i = 0; i < Arr.length; i++) {

                    Content = '';
                    Content += '<tr class="odd">';
                    Content += '<td align="center" style="width: 10%">' + (i + 1) + '</td>';
                    Content += '<td align="center">' + Arr[i].AirPort + '</td>';
                    Content += '<td align="center">' + Arr[i].City + '</td>';
                    Content += '<td align="center">' + Arr[i].Code + '</td>';
                    Content += '<td align="center">' + Arr[i].Latitude + '</td>';
                    Content += '<td align="center">' + Arr[i].Longitude + '</td>';
                   
                    Content += '<td align="center" ><a style="cursor: pointer" onclick="UpdateDetails(' + Arr[i].sid + ')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a>| <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer"onclick="Delete(' + Arr[i].sid + ')"></span></a></td>';
                    Content += '</tr>';

                    $('#Details').append(Content);
                }

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("No Record")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {
            alert('Error Function')
        }

    });
}
function A() {
    debugger;
    $.ajax({
        type: "Post",
        url: "AirPortHandler.asmx/Load",
        data: {},
        contentType: "text/plain",
        datatype: 'xml',
        success: function (data) {
            //var obj = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            // var obj = JSON.parse(response.d);
            //alert(data[0]);
            var jQueryXml = $(data);
            var test = data.lastChild.innerHTML;
            var obj = JSON.parse(test);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                var Content = '';

                for (var i = 0; i < Arr.length; i++) {
             
                    Content = '';
                    Content += '<tr class="odd">';
                    Content += '<td align="center" style="width: 10%">' + (i + 1) + '</td>';
                    Content += '<td align="center">' + Arr[i].AirPort + '</td>';
                    Content += '<td align="center">' + Arr[i].City + '</td>';
                    Content += '<td align="center">' + Arr[i].Code + '</td>';
                    Content += '<td align="center">' + Arr[i].Latitude + '</td>';
                    Content += '<td align="center">' + Arr[i].Longitude + '</td>';
                    Content += '<td align="center" ><a style="cursor: pointer" onclick="UpdateDetails(' + Arr[i].sid + ')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a>| <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer"onclick="Delete(' + Arr[i].sid + ')"></span></a></td>';
                    Content += '</tr>';

                    $('#Details').append(Content);
                }

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("No Record")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {
            alert('Error Function')
        }

    });
}

function Delete(sid) {
    var data = { sid: sid }
    debugger;
    $.ajax({
        type: "POST",
        url: "AirPortHandler.asmx/Delete",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                $('#Details').empty();
                Load()

                $('#SpnMessege').text("Record Delted")
                $('#ModelMessege').modal('show')

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not Delted")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });

}

function UpdateDetails(sid) {
    debugger;

    var data = { sid: sid }
    debugger;
    $.ajax({
        type: "POST",
        url: "AirPortHandler.asmx/GetSingle",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;

                $('#AFname').val(Arr[0].Name);

                $('#hdn').val(sid);

                $('#Update').modal('show')

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });






}

function Update() {
    debugger;

    var Sid = $('#hdn').val();
    var Name = $('#AFname').val();
    var Description = $('#AArea').val();

    if (Name == '') {
        $('#SpnMessege').text("Please Insert Name")
        $('#ModelMessege').modal('show')
        return false
    }


    var data = { sid: Sid, Name: Name, Description: Description }
    debugger;
    $.ajax({
        type: "POST",
        url: "AirPortHandler.asmx/Update",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);


            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                $('#Update').modal('hide')
                $('#Details').empty();
                Load()
                $('#SpnMessege').text("Record updated")
                $('#ModelMessege').modal('show')

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });



}

function Insert() {
    debugger;
    var Name = $('#Fname').val();
    var Description = $('#Area').val();

    var data = { Name: Name, Description: Description }

    if (Name == '') {
        $('#SpnMessege').text("Please Insert Name")
        $('#ModelMessege').modal('show')
        return false
    }

    debugger;
    $.ajax({
        type: "POST",
        url: "AirPortHandler.asmx/Insert",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);


            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                $('#Add').modal('hide')
                $('#Details').empty();
                Load()
                $('#SpnMessege').text("Record Inserted")
                $('#ModelMessege').modal('show')

            }



            if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not Inserted")
                $('#ModelMessege').modal('show')
            }

            if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function Open() {
    debugger
    //$('#Fname').val('');
    $('#Add').modal('show')
}

function ActiveAirport(Sid) {
    var data = {
        Sid: Sid
    }
    $.ajax({
        type: "POST",
        url: "AirPortHandler.asmx/ActiveAirport",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (obj.retCode == 1) {
                GetAllStaff();
            }
            else {
                alert("Something Went Wrog");
                window.location.reload();
            }
        }
    });
}

function Active(sid) {
    var data = { sid: sid }
    debugger;
    $.ajax({
        type: "POST",
        url: "AirPortHandler.asmx/Active",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                $('#Details').empty();
                Load();
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });

}