﻿function AddNewLocation() {
    Sid = 0;
    $('#btn_AddLocation1').val("Save");
    $("#AddNewLocation").modal("show");
    $("#txt_Location").val('');
    $("#txt_Latitude").val('');
    $("#txt_Longitude").val('');
}

$(function () {
    LoadAll();
})

function cleartextboxes() {
    $("#ddl_VehicleMake").val(0);
    $("#txt_ModelName").val("");
    $("#txt_RegistrationYear").val("");
    $("#ddl_VehicleType").val(0);
    $("#txt_MaxCapacity").val("");
    $("#txt_MaxBaggage").val("");
    $("#f_img").val("");
    $("#txt_Remarks").val("");
}

function GetLocation() {
    debugger;
    var address = $("#txt_Location").val();
    var data = {
        address: address,
    }

    $.ajax({
        type: "POST",
        url: "../LocationHandler.asmx/Post",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Location = obj.OutPut;
                var Location_Split = Location.split(',');

                $("#txt_Latitude").val(Location_Split[0]);
                $("#txt_Longitude").val(Location_Split[1]);

            }
           else if (obj.retCode == -1) {
               alert("No Airport Found");
            }

            else {
                alert("Something Went Wrog");
                window.location.reload();
            }
        }
    });
}

var Sid;

function AddLocation() {
    debugger;

    var address = $("#txt_Location").val();
    var Latitude = $("#txt_Latitude").val();
    var Longitude = $("#txt_Longitude").val();
    if (address == '') {
        $('#SpnMessege').text("Please insert Airport Name")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Latitude == '') {
        $('#SpnMessege').text("Please insert Latitude")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Longitude == '') {
        $('#SpnMessege').text("Please insert Longitude")
        $('#ModelMessege').modal('show')
        return false;
    }
   //var sid= $('#hdnsid').val()
    if (Sid == 0) {
       
       var data = {
           address: address,
           Latitude: Latitude,
           Longitude: Longitude

       }
        $.ajax({
            type: "POST",
            url: "LocationHandler.asmx/AddLocation",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    $('#AddNewLocation').modal('hide')
                    LoadAll();
                    $('#SpnMessege').text("Airport Inserted Successfully")
                    $('#ModelMessege').modal('show')

                }
                else if (obj.retCode == -1) {
                    alert("No Airport Found");
                }

                else {
                    alert("Something Went Wrog");
                    window.location.reload();
                }
            }
        });
    }

   else {
       //var address = $("#txt_Location").val();
       //var Latitude = $("#txt_Latitude").val();
       //var Longitude = $("#txt_Longitude").val();
       var data = {
           sid: Sid,
           address: address,
           Latitude: Latitude,
           Longitude: Longitude
       }
        $.ajax({
            type: "POST",
            url: "LocationHandler.asmx/UpdateLocation",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    $('#AddNewLocation').modal('hide')
                    LoadAll();
                    $('#SpnMessege').text("Airport Updated Successfully")
                    $('#ModelMessege').modal('show')

                }
                else if (obj.retCode == -1) {
                    alert("No Location Found");
                }

                else {
                    alert("Something Went Wrog");
                    window.location.reload();
                }
            }
        });

    }
}

function LoadAll() {
    debugger;
    $('#btn_AddLocation1').val("Save");

    $.ajax({
        type: "POST",
        url: "LocationHandler.asmx/LocationLoadAll",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
               

                var CarDetails = obj.CarDetails;

                var ul = '';
                $("#LocationDetails").empty();
           
                if (CarDetails) {
                    $("#LocationDetails").empty();
                    for (var i = 0; i <CarDetails.length; i++) {
                        
                        ul += '<tr class="odd">';
                        ul += '<td class=" ">' + CarDetails[i].LocationName + '</td>';
                        ul += '<td class=" ">' + CarDetails[i].Latitude + '</td>';
                        ul += '<td class=" ">' + CarDetails[i].Longitude + '</td>';
                        var Act = CarDetails[i].Status;
                        if (Act == "Activate") {
                            ul += '<td align="center" style="width:10%"><span class="glyphicon glyphicon-eye-open" onclick="ActiveLocation(' + CarDetails[i].sid + ')" style="cursor:pointer" title="Activate/Deactivate"></span></td>'
                        }
                        else {
                            ul += '<td align="center" style="width:10%"><span class="glyphicon glyphicon-eye-close" onclick="ActiveLocation(' + CarDetails[i].sid + ')" style="cursor:pointer" title="Activate/Deactivate"></span></td>'
                        }
                        //ul += '<td align="center" class=" "><a style="cursor: pointer" onclick="LoadByKey(' + CarDetails[i].sid + ')" href="#"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a>&nbsp;&nbsp;| <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="DeleteLocation(' + CarDetails[i].sid + ')"></span></a></td>';
                        ul += '<td align="center" class=" "><a style="cursor: pointer" onclick="LoadByKey(' + CarDetails[i].sid + ')" href="#"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a>&nbsp;&nbsp;</td>';
                        ul += '</tr>';

                    }
                    $('#LocationDetails').append(ul);
                    
                }
                else {
                    $("#LocationDetails").empty();
                    ul += '<tr>';
                    ul+='<td colspan="5">No Record Found</td>'
                    ul += '</tr>';
                    $('#LocationDetails').append(ul);
                }

            }
            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function LoadByKey(sid) {
    debugger;
    $('#btn_AddLocation1').val("Update");
    //$('#hdnsid').val(sid)
    Sid = sid;

    var data = {
        sid: Sid,
    }

    $.ajax({
        type: "POST",
        url: "LocationHandler.asmx/LoadByKey",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var CarDetails = obj.CarDetails;
                var ul = '';
          

                if (CarDetails) {
            
                    $("#AddNewLocation").modal("show");

                    var LocationName = CarDetails[0].LocationName;

                    var Latitude = CarDetails[0].Latitude;
                    var Longitude = CarDetails[0].Longitude;

                    $('#txt_Location').val(LocationName )
                    $('#txt_Latitude').val(Latitude)
                    $('#txt_Longitude').val(Longitude)
                      

                }
                else {
           
           
                }

            }
            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function DeleteLocation(sid) {
    debugger;

    var data = {
        sid: sid,
    }

    $.ajax({
        type: "POST",
        url: "LocationHandler.asmx/DeleteLocation",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                $('#AddNewLocation').modal('hide')
                LoadAll();
                $('#SpnMessege').text("Location Deleted.")
                $('#ModelMessege').modal('show')

            }
            else if (obj.retCode == -1) {
                alert("No Location Found");
            }

            else {
                alert("Something Went Wrog");
                window.location.reload();
            }
        }
    });
}

function ActiveLocation(Sid) {
    var data = {
        Sid: Sid
    }
    $.ajax({
        type: "POST",
        url: "LocationHandler.asmx/ActiveLocation",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (obj.retCode == 1) {
                $('#SpnMessege').text("Status Change Successfully")
                $('#ModelMessege').modal('show')
                //alert("Status Change Successfully");
                LoadAll();
            }
            else {
                alert("Something Went Wrog");
                window.location.reload();
            }
        }
    });
}