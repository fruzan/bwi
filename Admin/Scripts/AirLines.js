﻿$(function () {

    LoadAirLines();
})

function LoadAirLines() {
    
    debugger;
    $.ajax({
        type: "POST",
        url: "AirLinesHandler.asmx/LoadAirLines",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                $('#Details').empty();
                var Arr = obj.Arr;
                var Content = '';

                for (var i = 0; i < Arr.length; i++) {
                    Content = '';
                    Content += '<tr class="odd">';
                    Content += ' <td align="center" style="width: 10%">' + (i + 1) + '</td>';
                    Content += '<td align="center">' + Arr[i].Callsign + '</td>';
                    Content += '<td align="center">' + Arr[i].ICAO + '</td>';
                    Content += '<td align="center">' + Arr[i].IATA + '</td>';
                    var Act = Arr[i].Status;
                    if (Act == "Activate") {
                        Content += '<td align="center" style="width:10%"><span class="glyphicon glyphicon-eye-open" onclick="ActiveAirlines(' + Arr[i].Sid + ')" style="cursor:pointer" title="Activate/Deactivate"></span></td>'
                    }
                    else {
                        Content += '<td align="center" style="width:10%"><span class="glyphicon glyphicon-eye-close" onclick="ActiveAirlines(' + Arr[i].Sid + ')" style="cursor:pointer" title="Activate/Deactivate"></span></td>'
                    }
                    //Content += '<td align="center" ><a style="cursor: pointer" onclick="UpdateDetails(' + Arr[i].Sid + ')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a> | <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="DeleteAirLines(' + Arr[i].Sid + ')"></span></a></td>';
                    Content += '<td align="center" ><a style="cursor: pointer" onclick="UpdateDetails(' + Arr[i].Sid + ')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a></td>';
                    Content += '</tr>';

                    $('#Details').append(Content);
                }

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function DeleteAirLines(sid)
{
    if (confirm("Are you sure you want to delete this?"))
    {
        var data = { Sid: sid }
        debugger;
        $.ajax({
            type: "POST",
            url: "AirLinesHandler.asmx/DeleteAirLines",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    var Arr = obj.Arr;
                    $('#Details').empty();
                    LoadAirLines()

                    $('#SpnMessege').text("AirLines is Deleted")
                    $('#ModelMessege').modal('show')
                }

                else if (obj.retCode == -1) {
                    $('#SpnMessege').text("Record is not Delted")
                    $('#ModelMessege').modal('show')
                }

                else if (obj.retCode == 0) {
                    $('#SpnMessege').text("Something Went Wrong.")
                    $('#ModelMessege').modal('show')
                }
            }
        });
        $("#delete-button").attr("href", "query.php?ACTION=delete&ID='1'");
    }
    else
    {
        return false;
    }
}

function UpdateDetails(sid) {
    debugger;

    var data = { sid: sid }
    debugger;
    $.ajax({
        type: "POST",
        url: "AirLinesHandler.asmx/GetSingle",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;

                $('#Name').val(Arr[0].Callsign);
                $('#IATA').val(Arr[0].IATA);
                $('#ICAO').val(Arr[0].ICAO);
                $('#hdn').val(sid);
                $('#Submit').val('Update')
                $('#Update').modal('show')

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function UpdateAirLines() {
    debugger;

    var Sid = $('#hdn').val();
    var Name = $('#Name').val();
    var IATA = $('#IATA').val();
    var ICAO = $('#ICAO').val();

    if (Name == '') {
        $('#SpnMessege').text("Please Insert Name")
        $('#ModelMessege').modal('show')
        return false
    }
    if (IATA == '') {
        $('#SpnMessege').text("Please Insert IATA")
        $('#ModelMessege').modal('show')
        return false
    }
    if (ICAO == '') {
        $('#SpnMessege').text("Please Insert ICAO")
        $('#ModelMessege').modal('show')
        return false
    }

    var data = { Sid: Sid, Name: Name, IATA: IATA, ICAO: ICAO }
    debugger;
    $.ajax({
        type: "POST",
        url: "AirLinesHandler.asmx/UpdateAirLines",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);


            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                $('#Update').modal('hide')
                $('#Details').empty();
                LoadAirLines()
                $('#SpnMessege').text("AirLines updated Successfully")
                $('#ModelMessege').modal('show')

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });



}

function InsertAirLines() {
    debugger;
    var Name = $('#AName').val();
    var IATA = $('#AIATA').val();
    var ICAO = $('#AICAO').val();

    var data = { Name: Name, IATA: IATA, ICAO: ICAO }

    if (Name == '') {
        $('#SpnMessege').text("Please Insert Name")
        $('#ModelMessege').modal('show')
        return false
    }
    if (IATA == '') {
        $('#SpnMessege').text("Please Insert IATA")
        $('#ModelMessege').modal('show')
        return false
    }
    if (ICAO == '') {
        $('#SpnMessege').text("Please Insert ICAO")
        $('#ModelMessege').modal('show')
        return false
    }
    debugger;
    $.ajax({
        type: "POST",
        url: "AirLinesHandler.asmx/InsertAirLines",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);


            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                $('#Add').modal('hide')
                $('#Details').empty();
                LoadAirLines()
                $('#SpnMessege').text("AirLines Inserted Successfully")
                $('#ModelMessege').modal('show')

            }

            if (obj.retCode == 3) {
                var Arr = obj.Arr;
                $('#Add').modal('hide')
                $('#SpnMessege').text("Name Already Exist")
                $('#ModelMessege').modal('show')

            }
            if (obj.retCode == 4) {
                var Arr = obj.Arr;
                $('#Add').modal('hide')
                $('#SpnMessege').text("ICAO Already Exist")
                $('#ModelMessege').modal('show')

            }
            if (obj.retCode == 5) {
                var Arr = obj.Arr;
                $('#Add').modal('hide')
                $('#SpnMessege').text("IATA Already Exist")
                $('#ModelMessege').modal('show')

            }
         

            if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not Inserted")
                $('#ModelMessege').modal('show')
            }

            if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function Open() {
    $('#AName').val('');
    $('#AIATA').val('');
    $('#AICAO').val('');
    $('#Add').modal('show')
}

function ActiveAirlines(Sid) {
    var data = {
        Sid: Sid
    }
    $.ajax({
        type: "POST",
        url: "AirLinesHandler.asmx/ActiveAirLines",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (obj.retCode == 1) {
                LoadAirLines();
                $('#SpnMessege').text("Status Change Successfully")
                $('#ModelMessege').modal('show')
                //alert("Status Change Successfully");
            }
            else {
                alert("Something Went Wrog");
                window.location.reload();
            }
        }
    });
}