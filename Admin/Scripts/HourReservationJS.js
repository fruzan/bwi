﻿var Paid = false;
var Completed = false;
var Collect = false;
var CCPayment = false;
var bValid = true;

var sum = 0;
var sid;
var Arr

var MeetGreat = false;
var SpecialAssistant = false;
var CurbSidePickUp = false;
var BaggageClaimPickup = false;
var PetInCage = false;
var ChildCarSheet = false;

var HourlyMinimum
var ServiceUpto
var HourlyRate
var Hour
var Chk_PetInCage = false;

var Chk_rub = false;

var Chk_Baggageclaim = false;

var Gratuity = 0;
var Parking = 0;
var Toll = 0;
var Remark = '';
var Email;
var ContactNo;
var GratuityAmount = 0;
var ReservationType;
var CustId;
var OfferPercent = 0;
var CCType = "-";

$(function () {

    GetAllPhoneNo();
    GetAllOfferCode();

    if (location.href.indexOf('?') != -1) {
        var dt = new Date();
        ReservationType = GetQueryStringParams('ReservationType');
        if (ReservationType == "Customer") {
            CustId = GetQueryStringParams('CustId');
            var data = { CustId: CustId }
            $.ajax({
                type: "POST",
                url: "ReservationHandler.asmx/GetCustById",
                data: JSON.stringify(data),
                contentType: "application/json",
                datatype: "json",
                success: function (response) {

                    var obj = JSON.parse(response.d)
                    if (obj.retCode == 1) {
                        var Arr = obj.Arr
                        $('#txt_Email').val(Arr[0].Email)
                        $('#txt_FirstName').val(Arr[0].FirstName)
                        $('#txt_LastName').val(Arr[0].LastName)
                        $('#txt_ContactNumber').val(Arr[0].Mobile)
                    }
                },
            });
        }
        else {
            sid = GetQueryStringParams('sid');
            $('#btn_Submit').val('Update')
            var data = { BookingSid: sid }
            $.ajax({
                type: "POST",
                url: "ReservationHandler.asmx/UpdateData",
                data: JSON.stringify(data),
                contentType: "application/json",
                datatype: "json",
                success: function (response) {
                    var obj = JSON.parse(response.d)
                    if (obj.retCode == 1) {
                        Arr = obj.Arr;

                        if (Arr[0].Paid) {
                            $("#Chk_Paid").prop("checked", true);
                            Paid = true;
                            Collect = false;
                        }
                        else {
                            $("#Chk_Collect").prop("checked", true);
                            Collect = true;
                            Paid = false;
                        }
                        if (Arr[0].Completed) {
                            $("#Chk_Completed").prop("checked", true);
                            Completed = true;
                        }

                        if (Arr[0].CCPayment) {
                            $("#Chk_CCPayment").prop("checked", true);
                            CCPayment = true;
                        }
                        if (Arr[0].ChildCarSheet) {
                            $("#Chk_ChildCarSheet").prop("checked", true);
                            ChildCarSheet = true;
                        }

                        if (Arr[0].MeetGreat) {
                            $("#Chk_Meet").prop("checked", true);
                            MeetGreat = true;
                        }

                        if (Arr[0].SpecialAssistant) {
                            $("#Chk_Special").prop("checked", true);
                            SpecialAssistant = true;
                        }
                        if (Arr[0].CurbSidePickUp) {
                            $("#Chk_rub").prop("checked", true);
                            CurbSidePickUp = true;
                            Chk_rub = true
                        }
                        if (Arr[0].BaggageClaimPickup) {
                            $("#Chk_Baggageclaim").prop("checked", true);
                            BaggageClaimPickup = true;
                            Chk_Baggageclaim = true
                        }
                        if (Arr[0].PetInCage) {
                            $("#Chk_PetInCage").prop("checked", true);
                            PetInCage = true;
                            Chk_PetInCage = true;
                        }
                        OfferId = Arr[0].OfferCode;
                        if (OfferId == 0) {
                            OfferPercent = 0;
                        }
                        else {
                            for (var i = 0; i < OfferList.length; i++) {
                                if (OfferList[i].Sid == OfferId) {
                                    OfferPercent = OfferList[i].Percents;
                                }
                            }
                        }
                        if (Arr[0].OfferCode != null) {
                            debugger;
                            setTimeout(function () {
                                debugger;
                                $("#ddlOfferCode option").filter(function () {
                                    return $(this).val() == Arr[0].OfferCode;
                                }).prop("selected", true);
                            }, 1800);
                        }
                        //$('#txt_Account').val(Arr[0].AccountNumber)
                        // $('#txt_Account').attr('readonly', true);
                        //$('#txt_Account').addClass('input-disabled');
                        $('#txt_Date').val(Arr[0].ReservationDate);
                        //$('#txt_Code').val(Arr[0].OrganisationCode);
                        // $('#ddl_VehicleType').val(Arr[0].VehicalType);
                        //setTimeout(function () {
                        //    $("#Sel_CC option").filter(function () {
                        //        return $(this).val() == Arr[0].CCType;
                        //    }).prop("selected", true);
                        //}, 1000);
                        setTimeout(function () {
                            $("#txt_Assigned option").filter(function () {
                                return $(this).val() == Arr[0].DriverSid;
                            }).prop("selected", true);
                        }, 1200);

                        setTimeout(function () {
                            $("#ddl_VehicleType option").filter(function () {
                                return $(this).val() == Arr[0].VehicalType;
                            }).prop("selected", true);
                        }, 2500);

                        $("#Hours option").filter(function () {
                            return $(this).val() == Arr[0].Hours;
                        }).prop("selected", true);

                        Hour = Arr[0].Hours;
                        if (Arr[0].GratuityAmount != null) {
                            GratuityAmount = Arr[0].GratuityAmount;
                        }

                        if (Arr[0].GratuityAmount != null) {
                            GratuityAmount = Arr[0].GratuityAmount;
                        }
                        OfferId = Arr[0].OfferCode;
                        if (OfferId == 0 || OfferId == null) {
                            OfferPercent = 0;
                        }
                        else {
                            for (var i = 0; i < OfferList.length; i++) {
                                if (OfferList[i].Sid == OfferId) {
                                    OfferPercent = OfferList[i].Percents;
                                }
                            }
                        }

                        $('#txt_AreaRemarks').val(Arr[0].Remark);
                        Remark = Arr[0].Remark;
                        $('#txt_Email').val(Arr[0].Email)
                        $('#txt_FirstName').val(Arr[0].FirstName)
                        $('#txt_LastName').val(Arr[0].LastName)
                        if (Arr[0].ContactNumber != "")
                            $('#txt_ContactNumber').val(Arr[0].ContactNumber)
                        else
                            $('#txt_ContactNumber').val(Arr[0].PhoneNumber)
                        //$('#txt_Child').val(Arr[0].Child)
                        //$('#txt_Child').val(Arr[0].Child)
                        $('#txt_Adult').val(Arr[0].Persons)
                        $('#txt_FlightNumber').val(Arr[0].FlightNumber)
                        $('#txt_Last4').val(Arr[0].CCLast4)
                        $('#txt_Fare').val(Arr[0].Fare)
                        $('#txt_Gratuity').val(Arr[0].Gratuity)
                        Gratuity = Arr[0].Gratuity;
                        $('#txt_Parking').val(Arr[0].Parking)
                        Parking = Arr[0].Parking;
                        $('#txt_Total').val(Arr[0].TotalFare)
                        sum = Arr[0].TotalFare;
                        $('#txt_Toll').val(Arr[0].Toll)
                        Toll = Arr[0].Toll;
                        $('#hdn_Status').val(Arr[0].Status)
                        // $('#txt_PickUpTime').val(Arr[0].P2PTime)
                        if (Arr[0].P2PTime != null)
                            $('#txt_PickUpTime').val(SetTime(Arr[0].P2PTime));
                        else
                            $('#txt_PickUpTime').val(SetTime(Arr[0].Pickup_Time));
                        $('#txt_Address').val(Arr[0].Source);
                        // $('#txt_Address').val(Arr[0].AccountNumber)
                        if (Arr[0].CCType != null) {
                            $("#ddlCCType option").filter(function () {
                                return $(this).val() == Arr[0].CCType;
                            }).prop("selected", true);
                        }
                        TimeTaken = Arr[0].ApproxTime
                        TotalDistance = Arr[0].ApproxDistance
                        source = Arr[0].Source
                        destination = Arr[0].Destination
                        if (Arr[0].Status == "Completed") {
                            $("#btn_Submit").hide();
                        }
                    }
                    else {
                        $('#SpnMessege').text("Reservation unsuccessfull.")
                        $('#ModelMessege').modal('show')

                    }
                },
                error: function () {
                    $('#SpnMessege').text("Something Went Wrong.")
                    $('#ModelMessege').modal('show')
                },
            });
        }
    }
    else {
        $("#Chk_Paid").prop("checked", true);
        random = Math.floor((Math.random() * (99999999 - 10000000 + 1)) + 10000000);
        $('#txt_Account').val("ARC-" + random);
        $('#txt_Account').attr('readonly', true);
        $('#txt_Account').addClass('input-disabled');
    }
    $("#Chk_Paid").change(function () {
        debugger;

        Paid = ($(this).is(":checked"));
        if (Paid) {
            Paid = true;
            Collect = false;
            CheckRemark("Collect Fare")
        }
        else {
            Paid = false;
            Collect = true;
        }
    });
    $("#Chk_Collect").change(function () {
        debugger;
        Collect = ($(this).is(":checked"));

        if (Collect) {
            //Remark=
            SetRemark("Collect Fare");
            Paid = false;
            Collect = true;
        }
        else {
            CheckRemark("Collect Fare");
            Paid = true;
            Collect = false;
        }

    });

    $("#Chk_CCPayment").change(function () {
        //debugger;
        CCPayment = ($(this).is(":checked"));
        if (CCPayment) {
            CCPayment = true;
            //$('#txt_AreaRemarks').text('CC Payment');
        }
        else {
            CCPayment = false;
            //$('#txt_AreaRemarks').text('');
        }
    });

    $("#Chk_Meet").change(function () {
        //debugger;
        MeetGreat = ($(this).is(":checked"));

        if (MeetGreat) {
            SetRemark("Meet & Greet");
            sum = parseFloat(sum) + 10
            $("#txt_Total").val(sum.toFixed(2));
        }
        else {
            CheckRemark("Meet & Greet");
            sum = parseFloat(sum) - 10;
            $("#txt_Total").val(sum.toFixed(2));
        }
    });

    $("#Chk_Special").change(function () {
        //debugger;
        SpecialAssistant = ($(this).is(":checked"));
        if (SpecialAssistant) {
            SetRemark("Special Assistant");
        }
        else {
            CheckRemark("Special Assistant");
        }
    });

    $("#ServicePopUp").change(function () {
        //debugger;
        var a;
        var Type = $("#ServicePopUp").val();
        if (Type == "FromAirPort") {
            $('#New_Pop_Up_Location').html('Drop Location')
            a = "From Airport";
            $("#Select_Service option").each(function () {

                if ($(this).html() == a) {
                    $(this).attr("selected", "selected");
                    return;
                }
            });
            //alert($('#Select_Service option:selected').val())

        }
        else {
            a = "To Airport";
            $("#Select_Service").focus();
            $('#New_Pop_Up_Location').html('PickUp Location');
            $("#Select_Service option").each(function () {
                //alert($(this).html())
                if ($(this).html() == a) {
                    $(this).attr("selected", "selected");
                    return;
                }
            });

        }
    });

    $("#Select_Service").change(function () {
        //debugger;
        $("#Time").text('');
        var Type = $("#Select_Service").val();
        if (Type == "From Airport") {
            $('#Spn_Location').html('<b>Drop Location</b>')
            $("#Time").text("Flight Arrival Time :");
        }
        else {
            $('#Spn_Location').html('<b>PickUp Location</b>')
            $("#Time").text("Pickup Time :");
        }
    });

    $("#txt_Fare").blur(function () {
        //debugger
        sum = 0
        var Num = /^[0-9]+$/;
        var FareVal = $("#txt_Fare").val();
        Gratuity = $("#txt_Gratuity").val();
        Parking = $("#txt_Parking").val();
        Toll = $("#txt_Toll").val();

        if (FareVal != "") {
            sum = parseFloat(sum) + parseFloat(FareVal)
        }
        if (Gratuity != "-") {
            var Gra = parseFloat(sum) * parseFloat(Gratuity) / 100;
            GratuityAmount = Gra;
            sum = parseFloat(sum) + parseFloat(Gra);
        }
        if (Parking != "") {
            sum = parseFloat(sum) + parseFloat(Parking)
        }
        if (Toll != "") {
            sum = parseFloat(sum) + parseFloat(Toll)
        }
        if (ChildCarSheet == true) {
            sum = parseFloat(sum) + 10
        }

        if (Chk_rub == true) {
            sum = parseFloat(sum) + 10
        }
        //if (Chk_Baggageclaim == true) {
        //    sum = parseFloat(sum) + 10
        //}
        if (Chk_PetInCage == true) {
            sum = parseFloat(sum) + 10
        }


        $("#txt_Total").val(sum.toFixed(2));
    });

    $("#txt_Gratuity").change(function () {
        debugger
        if ($("#txt_Total").val() != "") {
            sum = 0
            var Num = /^[0-9]+$/;
            var FareVal = $("#txt_Fare").val();
            Gratuity = $("#txt_Gratuity option:selected").val();
            Parking = $("#txt_Parking").val();
            Toll = $("#txt_Toll").val();

            if (FareVal != "") {
                sum = parseFloat(sum) + parseFloat(FareVal)
            }
            if (Gratuity != "-") {
                var Gra = parseFloat(sum) * parseFloat(Gratuity) / 100;
                GratuityAmount = Gra;
                sum = parseFloat(sum) + parseFloat(Gra);
                //sum = parseFloat(sum) * parseFloat(Gratuity) / 100;
            }
            if (Parking != "") {
                sum = parseFloat(sum) + parseFloat(Parking)
            }
            if (Toll != "") {
                sum = parseFloat(sum) + parseFloat(Toll)
            }
            if (ChildCarSheet == true) {
                sum = parseFloat(sum) + 10
            }
            if (Chk_rub == true) {
                sum = parseFloat(sum) + 10
            }
            if (MeetGreat) {
                sum = parseFloat(sum) + 10
            }
            //if (Chk_Baggageclaim == true) {
            //    sum = parseFloat(sum) + 10
            //}
            if (Chk_PetInCage == true) {
                sum = parseFloat(sum) + 10
            }
            $("#txt_Total").val(sum.toFixed(2));
        }
        else {
            alert("Gratuity will be set on Total Amount");
        }

    });

    $("#txt_Parking").blur(function () {
        debugger
        sum = 0
        var Num = /^[0-9]+$/;
        var FareVal = $("#txt_Fare").val();
        Gratuity = $("#txt_Gratuity").val();
        Parking = $("#txt_Parking").val();
        Toll = $("#txt_Toll").val();

        if (FareVal != "") {
            sum = parseFloat(sum) + parseFloat(FareVal)
        }
        if (Gratuity != "-") {
            var GraAmount = parseFloat(sum) * parseFloat(Gratuity) / 100
            GratuityAmount = GraAmount;
            sum = parseFloat(sum) + parseFloat(GraAmount)
        }
        if (Parking != "") {
            sum = parseFloat(sum) + parseFloat(Parking)
        }
        if (Toll != "") {
            sum = parseFloat(sum) + parseFloat(Toll)
        }
        if (ChildCarSheet == true) {
            sum = parseFloat(sum) + 10
        }

        if (Chk_rub == true) {
            sum = parseFloat(sum) + 10
        }
        if (MeetGreat) {
            sum = parseFloat(sum) + 10
        }
        //if (Chk_Baggageclaim == true) {
        //    sum = parseFloat(sum) + 10
        //}
        if (Chk_PetInCage == true) {
            sum = parseFloat(sum) + 10
        }
        $("#txt_Total").val(sum.toFixed(2));
    });

    $("#txt_Toll").blur(function () {
        debugger
        sum = 0
        var Num = /^[0-9]+$/;
        var FareVal = $("#txt_Fare").val();
        Gratuity = $("#txt_Gratuity").val();
        Parking = $("#txt_Parking").val();
        Toll = $("#txt_Toll").val();

        if (FareVal != "") {
            sum = parseFloat(sum) + parseFloat(FareVal)
        }
        if (Gratuity != "-") {
            var Gra = parseFloat(sum) * parseFloat(Gratuity) / 100;
            GratuityAmount = Gra;
            sum = parseFloat(sum) + parseFloat(Gra);
        }
        if (Parking != "") {
            sum = parseFloat(sum) + parseFloat(Parking)
        }
        if (Toll != "") {
            sum = parseFloat(sum) + parseFloat(Toll)
        }

        if (ChildCarSheet == true) {
            sum = parseFloat(sum) + 10
        }
        if (Chk_rub == true) {
            sum = parseFloat(sum) + 10
        }
        if (MeetGreat) {
            sum = parseFloat(sum) + 10
        }
        //if (Chk_Baggageclaim == true) {
        //    sum = parseFloat(sum) + 10
        //}
        if (Chk_PetInCage == true) {
            sum = parseFloat(sum) + 10
        }
        $("#txt_Total").val(sum.toFixed(2));
    });

    $("#Chk_ChildCarSheet").change(function () {
        debugger;

        ChildCarSheet = ($(this).is(":checked"));

        if (ChildCarSheet) {
            SetRemark("Child Car Sheet");
            //SpecialService = parseFloat(SpecialService) + 10;
            //$("#txt_SpecialService").val(SpecialService);
            sum = parseFloat(sum) + 10
            $("#txt_Total").val(sum.toFixed(2));
        }
        else {
            CheckRemark("Child Car Sheet");
            //SpecialService = parseFloat(SpecialService) - 10;
            //$("#txt_SpecialService").val(SpecialService);
            sum = parseFloat(sum) - 10;
            $("#txt_Total").val(sum.toFixed(2));
        }

    });

    $("#Chk_rub").change(function () {
        debugger;
        CurbSidePickUp = ($(this).is(":checked"));
        Chk_rub = ($(this).is(":checked"));

        if (Chk_rub) {
            SetRemark("Curb Side PickUp");
            sum = parseFloat(sum) + 10
            $("#txt_Total").val(sum.toFixed(2));
        }
        else {
            CheckRemark("Curb Side PickUp");
            sum = parseFloat(sum) - 10;
            $("#txt_Total").val(sum.toFixed(2));
        }

    });

    $("#Chk_PetInCage").change(function () {
        debugger;
        PetInCage = ($(this).is(":checked"));
        Chk_PetInCage = ($(this).is(":checked"));

        if (Chk_PetInCage) {
            SetRemark("Pet In Cage");
            sum = parseFloat(sum) + 10
            $("#txt_Total").val(sum.toFixed(2));
        }
        else {
            CheckRemark("Pet In Cage");
            sum = parseFloat(sum) - 10;
            $("#txt_Total").val(sum.toFixed(2));
        }
    });

    $("#ddl_VehicleType").change(function () {
        debugger;

        var Tab = "3";
        var VehicleSid = $('#ddl_VehicleType').val();
        var data = { Tab: Tab, sid: VehicleSid }
        $.ajax({
            type: "POST",
            url: "ReservationRateHandler.asmx/GetRate",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {

                var obj = JSON.parse(response.d)
                if (obj.Retcode == 1) {
                    var Arr = obj.Arr

                    Hour = $('#Hours').val();
                    //var Minutes = $('#Minutes').val();
                    //HourlyMinimum = Arr[0].HourlyMinimum
                    //ServiceUpto = Arr[0].ServiceUpto

                    HourlyRate = Arr[0].HourlyRate

                    //var MinToHour = parseFloat(Minutes) / 60;
                    //var TotalHours = (parseFloat(Hour) + parseFloat(MinToHour));

                    var BaseRate = parseFloat(Hour) * parseFloat(HourlyRate)
                    if (OfferPercent != 0 && OfferPercent != null) {
                        var sub = (parseFloat(BaseRate) / 100) * parseFloat(OfferPercent);
                        BaseRate = parseFloat(BaseRate) - parseFloat(sub);;
                    }
                    sum = BaseRate
                    $('#txt_Fare').val(sum.toFixed(2))
                    if (Gratuity > 0) {
                        var GraAmount = (parseFloat(Gratuity) * parseFloat(sum)) / 100;
                        GratuityAmount = GraAmount;
                        sum = parseFloat(sum) + parseFloat(GraAmount)
                    }
                    if (Chk_PetInCage) {
                        sum = parseFloat(sum) + 10
                    }
                    if (ChildCarSheet) {
                        sum = parseFloat(sum) + 10
                    }
                    //if (Chk_Baggageclaim) {
                    //    sum = parseFloat(sum) + 10
                    //}
                    if (MeetGreat) {
                        sum = parseFloat(sum) + 10
                    }

                    if (Parking != 0) {
                        sum = parseFloat(sum) + parseFloat(Parking)
                    }
                    if (Toll != 0) {
                        sum = parseFloat(sum) + parseFloat(Toll)
                    }
                    if (Chk_rub) {
                        sum = parseFloat(sum) + 10
                    }
                    $('#txt_Total').val(sum.toFixed(2))
                }
                else {
                    $('#SpnMessege').text("No rate Found.")
                    $('#ModelMessege').modal('show')
                }
            },
            error: function () {

                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            },
        });
    });

    $("#txt_Email").blur(function () {
        debugger;

        Email = $("#txt_Email").val();

        for (var i = 0; i < CustomerList.length; i++) {
            var as = CustomerList[i].Email;
            if (CustomerList[i].Email == Email) {
                if (CustomerList[i].FirstName != null) {
                    $("#txt_FirstName").val(CustomerList[i].FirstName);
                }
                if (CustomerList[i].LastName != null) {
                    $("#txt_LastName").val(CustomerList[i].LastName);
                }
                if (CustomerList[i].Mobile != null) {
                    $("#txt_ContactNumber").val(CustomerList[i].Mobile);
                }
                //$("#txt_LastName").val(CustomerList[i].LastName);
                $("#txt_ContactNumber").val(CustomerList[i].Mobile);
                //$("#txt_FirstName").val(CustomerList[i].FirstName);
            }
        }
        //$("#txt_Total").val(sum);
    });

    $("#Hours").change(function () {
        debugger;
        OfferId = $("#ddlOfferCode option:selected").val();
        if (OfferId == 0 || OfferId == null) {
            OfferPercent = 0;
        }
        else {
            for (var i = 0; i < OfferList.length; i++) {
                if (OfferList[i].Sid == OfferId) {
                    OfferPercent = OfferList[i].Percents;
                }
            }
        }
        var Num = /^[0-9]+$/;
        var FareVal = $("#txt_Fare").val();
        var Gratuity = $("#txt_Gratuity").val();
        var Parking = $("#txt_Parking").val();
        var Toll = $("#txt_Toll").val();

        var VehhicleType = $('#ddl_VehicleType').val()
        if (VehhicleType == "0") {
            $('#SpnMessege').text("Please Select Vehicle Type.")
            $('#ModelMessege').modal('show')
            return false;
        }
        var Tab = "3";
        var VehicleSid = $('#ddl_VehicleType').val();
        var data = { Tab: Tab, sid: VehicleSid }
        $.ajax({
            type: "POST",
            url: "ReservationRateHandler.asmx/GetRate",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d)
                if (obj.Retcode == 1) {
                    var Arr = obj.Arr
                    Hour = $('#Hours').val();
                    HourlyRate = Arr[0].HourlyRate
                    var BaseRate = (parseFloat(Hour) * parseFloat(HourlyRate));
                    sum = BaseRate
                    if (OfferPercent != 0) {
                        var sub = (parseFloat(sum) / 100) * parseFloat(OfferPercent);
                        sum = parseFloat(sum) - parseFloat(sub);
                    }
                    $('#txt_Fare').val(sum.toFixed(2))
                    if (Gratuity > 0) {
                        var GraAmount = (parseFloat(Gratuity) * parseFloat(sum)) / 100;
                        GratuityAmount = GraAmount;
                        sum = parseFloat(sum) + parseFloat(GraAmount)
                    }
                    if (Chk_PetInCage) {
                        sum = parseFloat(sum) + 10
                    }
                    if (ChildCarSheet) {
                        sum = parseFloat(sum) + 10
                    }
                    if (MeetGreat) {
                        sum = parseFloat(sum) + 10
                    }
                    if (Parking != 0) {
                        sum = parseFloat(sum) + parseFloat(Parking)
                    }
                    if (Toll != 0) {
                        sum = parseFloat(sum) + parseFloat(Toll)
                    }
                    if (Chk_rub) {
                        sum = parseFloat(sum) + 10
                    }
                    $('#txt_Total').val(sum.toFixed(2))
                }
            },
        });
    })

    $("#Minutes").change(function () {
        debugger;
        var Num = /^[0-9]+$/;
        var FareVal = $("#txt_Fare").val();
        var Gratuity = $("#txt_Gratuity").val();
        var Parking = $("#txt_Parking").val();
        var Toll = $("#txt_Toll").val();


        var VehhicleType = $('#ddl_VehicleType').val()
        if (VehhicleType == "0") {
            $('#SpnMessege').text("Please Select Vehicle Type.")
            $('#ModelMessege').modal('show')
            return false;
        }

        var Hour = $('#Hours').val();
        var Minutes = $('#Minutes').val();
        var HourToMin = parseFloat(Hour) * 60;
        var MinToHour = parseFloat(Minutes) / 60;

        var BaseRate = ((parseFloat(Hour) + parseFloat(MinToHour)) * parseFloat(HourlyRate)).toFixed(2)

        sum = BaseRate
        if (Gratuity != "-") {
            sum = parseFloat(sum) + parseFloat(Gratuity)
        }
        if (Chk_PetInCage) {
            sum = parseFloat(sum) + 10
        }
        if (Chk_rub) {
            sum = parseFloat(sum) + 10
        }
        if (Chk_Baggageclaim) {
            sum = parseFloat(sum) + 10
        }
        if (Parking != "") {
            sum = parseFloat(sum) + parseFloat(Parking)
        }
        if (Toll != "") {
            sum = parseFloat(sum) + parseFloat(Toll)
        }
        $('#txt_Fare').val(BaseRate)
        $('#txt_Total').val(sum)
    })

    $("#txt_ContactNumber").blur(function () {
        debugger;

        ContactNo = $("#txt_ContactNumber").val();

        for (var i = 0; i < CustomerList.length; i++) {
            var as = CustomerList[i].Email;
            if (CustomerList[i].Mobile == ContactNo) {
                if (CustomerList[i].FirstName != null) {
                    $("#txt_FirstName").val(CustomerList[i].FirstName);
                }
                if (CustomerList[i].LastName != null) {
                    $("#txt_LastName").val(CustomerList[i].LastName);
                }
                if (CustomerList[i].Mobile != null) {
                    $("#txt_ContactNumber").val(CustomerList[i].Mobile);
                }
                //$("#txt_LastName").val(CustomerList[i].LastName);
                $("#txt_Email").val(CustomerList[i].Email);
                //$("#txt_FirstName").val(CustomerList[i].FirstName);
            }
        }
        //$("#txt_Total").val(sum);
    });

    $("#ddlOfferCode").change(function () {
        debugger;
        CalcOffer();
    });
});

function VehicleTypeDropDown() {
    debugger;
    $.ajax({
        type: "POST",
        url: "VehicleInfoHandler.asmx/VehicleTypeDropDown",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Manufacturer = obj.VehicleType;
            $("#ddl_VehicleType").empty();
            var OptionVehicleType;
            OptionVehicleType += '<option value="0">--Select Vehicle Type--</option>'
            if (obj.retCode == 1) {
                for (var i = 0; i < Manufacturer.length; i++) {
                    if (Manufacturer[i].sid != "4052")
                        OptionVehicleType += '<option value="' + Manufacturer[i].sid + '" >' + Manufacturer[i].Name + '</option>'
                }
                var s = OptionVehicleType.split('undefined')
                VehicleType = s[1];
                $("#ddl_VehicleType").append(VehicleType);
            }

            else {

            }
        }
    });
}

function GetAllDriver() {
    $.ajax({
        url: "ReservationHandler.asmx/GetAllDriver",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Driver = obj.tblDriver;
            //var arrLocation = obj.Location;
            if (obj.retCode == 1) {
                arrService = obj.tblDriver;
                debugger;
                var OptionMac_Name = null;

                if (obj.retCode == 1) {
                    for (var i = 0; i < Driver.length; i++) {
                        var fullName = Driver[i].sFirstName + " " + Driver[i].sLastName;
                        OptionMac_Name += '<option value="' + Driver[i].Sid + '" >' + fullName + '</option>'
                    }
                    //var s = OptionMac_Name.split('undefined')
                    //Mac_Name = s[1];
                    $("#txt_Assigned").append(OptionMac_Name);
                }
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}

function Submit() {
    debugger;
    var text = $('#btn_Submit').val()
    bValid = Validation();
    if (text == "Add") {

        if (bValid) {
            var Input = $('input[name="Text[]"]').map(function () {
                return this.value
            }).get()

            var DropDowns = $('select[name="Drop[]"]').map(function () {
                return this.value;
            }).get();

            if ($('#Chk_Paid').is(':checked')) {
                Paid = true;
            }

            var CheckBoxes = [Paid, Completed, Collect, CCPayment, ChildCarSheet]
            var Remarks = "";
            Remarks = $('#txt_AreaRemarks').val();
            var Status = $('#hdn_Status').val();
            var DriverName = $("#txt_Assigned option:selected").text();
            var DriverID = $("#txt_Assigned option:selected").val();
            var data = { Input: Input, DropDowns: DropDowns, CheckBoxes: CheckBoxes, Remarks: Remarks, MeetGreat: MeetGreat, SpecialAssistant: SpecialAssistant, CurbSidePickUp: CurbSidePickUp, BaggageClaimPickup: BaggageClaimPickup, PetInCage: PetInCage, Status: Status, DriverName: DriverName, Email: Email, ContactNo: ContactNo, GratuityAmount: GratuityAmount, DriverID: DriverID }
            debugger;

            $.ajax({
                type: "POST",
                url: "ReservationHandler.asmx/HourReservation",
                data: JSON.stringify(data),
                contentType: "application/json",
                datatype: "json",
                success: function (response) {
                    var obj = JSON.parse(response.d)
                    debugger;
                    if (obj.Retcode == 1) {
                        $('#SpnMessege').text("Per Hour Reservation Done Successfully.")
                        $('#ModelMessege').modal('show')
                        AllClear();
                        //location.reload();
                    }
                    else {
                        $('#SpnMessege').text("Reservation unsuccessfull.")
                        $('#ModelMessege').modal('show')
                    }
                },
                error: function () {
                    $('#SpnMessege').text("Something Went Wrong.")
                    $('#ModelMessege').modal('show')
                },
            });
        }
    }


    else {
        if (bValid) {

            var Input = $('input[name="Text[]"]').map(function () {
                return this.value
            }).get()

            var DropDowns = $('select[name="Drop[]"]').map(function () {
                return this.value;
            }).get();

            if ($('#Chk_Paid').is(':checked')) {
                Paid = true;
            }

            var CheckBoxes = [Paid, Completed, Collect, CCPayment, ChildCarSheet]
            var Remarks = "";
            Remarks = $('#txt_AreaRemarks').val();
            var Status = $('#hdn_Status').val();
            var DriverName = $("#txt_Assigned option:selected").text();
            var DriverID = $("#txt_Assigned option:selected").val();
            var data = { sid: sid, Input: Input, DropDowns: DropDowns, CheckBoxes: CheckBoxes, Remarks: Remarks, MeetGreat: MeetGreat, SpecialAssistant: SpecialAssistant, CurbSidePickUp: CurbSidePickUp, BaggageClaimPickup: BaggageClaimPickup, PetInCage: PetInCage, Status: Status, DriverName: DriverName, Email: Email, ContactNo: ContactNo, GratuityAmount: GratuityAmount, DriverID: DriverID }


            $.ajax({
                type: "POST",
                url: "ReservationHandler.asmx/UpdateHourReservation",
                data: JSON.stringify(data),
                contentType: "application/json",
                datatype: "json",
                success: function (response) {
                    var obj = JSON.parse(response.d)
                    if (obj.Retcode == 1) {
                        $('#SpnMessege').text("Per Hour Reservation Updated Successfully.")
                        $('#ModelMessege').modal('show')
                        AllClear();
                    }
                    else if (obj.Retcode == 3) {
                        $('#SpnMessege').text("Reservation Done Successfully.Problem while sending mail.")
                        $('#ModelMessege').modal('show')
                        location.reload();
                    }
                    else {
                        $('#SpnMessege').text("Reservation unsuccessfull.")
                        $('#ModelMessege').modal('show')
                    }
                },
                error: function () {
                    $('#SpnMessege').text("Something Went Wrong.")
                    $('#ModelMessege').modal('show')
                },
            });
        }
    }
}
var Email;
function Validation() {

    debugger;
    Email = document.getElementById("txt_Email").value;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (Email != "") {
        if (emailReg.test(Email)) {
            $('#lbl_Email').css("display", "none");
        }
        else {
            $('#lbl_Email').text("Please Enter proper Email Format!");
            $('#lbl_Email').css("display", "");
            return false;
        }
    }
    else {
        $('#lbl_Email').text("* This field is required");
        $('#lbl_Email').css("display", "");
        return false;
    }

    ContactNo = document.getElementById("txt_ContactNumber").value;
    if (ContactNo == "") {
        $('#lbl_ContactNumber').text("* This field is required");
        $('#lbl_ContactNumber').css("display", "");
        return false;
    }
    else {
        $('#lbl_ContactNumber').css("display", "none");
    }

    var regex = new RegExp("^[a-zA-Z]+$"); // for alphabets to allow
    var FN = $('#txt_FirstName').val();
    if (FN == "") {
        $('#lbl_FirstName').text("* This field is required");
        $('#lbl_FirstName').css("display", "");
        return false;
    }
    else {
        $('#lbl_FirstName').css("display", "none");
    }

    var LN = $('#txt_LastName').val();
    if (LN == "") {
        $('#lbl_LastName').text("* This field is required");
        $('#lbl_LastName').css("display", "");
        return false;
    }
    else {
        $('#lbl_LastName').css("display", "none");
    }

    // Mobile Validation

    ContactNo = document.getElementById("txt_ContactNumber").value;
    var pattern = /^\d{10}$/;
    //if (LN != "") {
    //    if (pattern.test(MN)) {
    //        $('#lbl_ContactNumber').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_ContactNumber').text("It is not valid mobile number.input 10 digits number!");
    //        $('#lbl_ContactNumber').css("display", "");
    //        return false;
    //    }
    //}


    var ReservationDate = document.getElementById("txt_Date").value;
    if (ReservationDate == "") {
        $('#lbl_Date').text("* This field is required");
        $('#lbl_Date').css("display", "");
        return false;
    }
    else {
        $('#lbl_Date').css("display", "none");
    }


    //var Code = document.getElementById("txt_Code").value;
    //if (Code == "") {
    //    Code = 0;
    //    //$('#lbl_Code').text("* This field is required");
    //    //$('#lbl_Code').css("display", "");
    //    //return false;
    //}
    //else {
    //    $('#lbl_Code').css("display", "none");
    //}

    var Adult = document.getElementById("txt_Adult").value;
    var Num = /^[0-9]+$/;
    if (Adult != "") {
        if (Num.test(Adult)) {
            $('#lbl_Adult').css("display", "none");
        }
        else {
            $('#lbl_Adult').text("Only Numbers Allowed!!");
            $('#lbl_Adult').css("display", "");
            return false;
        }
    }
    else {

        $('#lbl_Adult').css("display", "");
        return false;
    }

    var VehicleType = $("#ddl_VehicleType option:selected").val();
    if (VehicleType == "0") {
        $('#lbl_Vehical').text("* This field is required");
        $('#lbl_Vehical').css("display", "");
        return false;
    }
    else {
        $('#lbl_Vehical').css("display", "none");
    }

    var CCFour = document.getElementById("txt_Last4").value;
    var patternFour = /^\d{4}$/;
    if (CCFour != "") {
        if (patternFour.test(CCFour)) {
            $('#lbl_Last4').css("display", "none");
        }
        else {
            $('#lbl_Last4').text("Only 4 Digits are allowed!");
            $('#lbl_Last4').css("display", "");
            return false;
        }
    }
    else {
        $('#lbl_Last4').text("* This field is required");
        $('#lbl_Last4').css("display", "");
        return false;
    }

    var Address = document.getElementById("txt_Address").value;
    if (Address == "") {
        $('#lbl_Address').text("* This field is required");
        $('#lbl_Address').css("display", "");
        return false;
    }
    else {
        $('#lbl_Address').css("display", "none");
    }

    var Time = document.getElementById("txt_PickUpTime").value;
    if (Time == "") {
        $('#lbl_PickUpTime').text("* This field is required");
        $('#lbl_PickUpTime').css("display", "");
        return false;
    }
    else {
        $('#lbl_PickUpTime').css("display", "none");
    }

    var AS = $('#txt_Assigned').val();
    if (AS != "") {
        //if (regex.test(AS)) {
        //    $('#lbl_Assigned').css("display", "none");
        //}
        //else {
        //    $('#lbl_Assigned').text("Only alphbets Are Allowed");
        //    $('#lbl_Assigned').css("display", "");
        //    return false;
        //}
    }
    else {
        $('#lbl_Assigned').text("* This field is required");
        $('#lbl_Assigned').css("display", "");
        return false;
    }

    var payment;

    if (Paid) {
        payment = true
    }
    else if (Completed) {
        payment = true
    }
    else if (Collect) {
        payment = true
    }
    else if (CCPayment) {
        payment = true
    }

    //if (payment != true) {

    //    alert("Select Any payment option");
    //    return false;
    //}



    //var Fare = document.getElementById("txt_Fare").value;
    //Fare = parseFloat(Fare)
    //Num = /^[0-9]+$/;
    //if (Fare != "") {
    //    if (Num.test(Fare)) {
    //        $('#lbl_Fare').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_Fare').text("Only Numbers Allowed!!");
    //        $('#lbl_Fare').css("display", "");
    //        return false;
    //    }
    //}
    //else {

    //    $('#lbl_Fare').css("display", "");
    //    return false;
    //}

    //var Gratuity = document.getElementById("txt_Gratuity").value;
    //Num = /^[0-9]+$/;
    //if (Gratuity != "") {
    //    if (Num.test(Gratuity)) {
    //        $('#lbl_Gratuity').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_Gratuity').text("Only Numbers Allowed!!");
    //        $('#lbl_Gratuity').css("display", "");
    //        return false;
    //    }
    //}
    //else {

    //    $('#lbl_Gratuity').css("display", "");
    //    return false;
    //}


    //var Parking = document.getElementById("txt_Parking").value;
    //Num = /^[0-9]+$/;
    //if (Parking != "") {
    //    if (Num.test(Parking)) {
    //        $('#lbl_Parking').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_Parking').text("Only Numbers Allowed!!");
    //        $('#lbl_Parking').css("display", "");
    //        return false;
    //    }
    //}
    //else {

    //    $('#lbl_Parking').css("display", "");
    //    return false;
    //}


    //var TotalFare = document.getElementById("txt_Total").value;
    //Num = /^[0-9]+$/;
    //if (TotalFare != "") {
    //    if (Num.test(TotalFare)) {
    //        $('#lbl_Total').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_Total').text("Only Numbers Allowed!!");
    //        $('#lbl_Total').css("display", "");
    //        return false;
    //    }
    //}
    //else {

    //    $('#lbl_Total').css("display", "");
    //    return false;
    //}

    //var Toll = document.getElementById("txt_Toll").value;
    //Num = /^[0-9]+$/;
    //if (Toll != "") {
    //    if (Num.test(Toll)) {
    //        $('#lbl_Toll').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_Toll').text("Only Numbers Allowed!!");
    //        $('#lbl_Toll').css("display", "");
    //        return false;
    //    }
    //}
    //else {

    //    $('#lbl_Toll').css("display", "");
    //    return false;
    //}

    //var City = document.getElementById("txt_City").value;
    //if (City == "") {
    //    $('#lbl_City').text("* This field is required");
    //    $('#lbl_City').css("display", "");
    //    return false;
    //}
    //else {
    //    $('#lbl_City').css("display", "none");
    //}

    //var State = document.getElementById("txt_State").value;
    //if (State == "") {
    //    $('#lbl_State').text("* This field is required");
    //    $('#lbl_State').css("display", "");
    //    return false;
    //}
    //else {
    //    $('#lbl_State').css("display", "none");
    //}

    //var ZipCode = document.getElementById("txt_ZipCode").value;
    //if (ZipCode == "") {
    //    $('#lbl_ZipCode').text("* This field is required");
    //    $('#lbl_ZipCode').css("display", "");
    //    return false;
    //}
    //else {
    //    $('#lbl_ZipCode').css("display", "none");
    //}

    return true;

}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function AllClear() {
    $("#Chk_Paid").prop("checked", true);
    $('input[name="Text[]"]').val('');
    $('select[name="Drop[]"]').val('');
    $('input:checkbox').removeAttr('checked');
    $('#txt_AreaRemarks').val('');
    $('#txt_Email').val('');
    $("#txt_ContactNumber").val('');
    $("#txt_Gratuity").val("-");
    $("#Sel_TravelType").val('Domestic');
    $("#Hours").val('1');
    $("#txt_Assigned").val(0);
    $("#ddl_VehicleType").val(0);
}

function GetAllEmail() {

    $.ajax({
        url: "ReservationHandler.asmx/GetAllEmail",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);

            if (obj.retCode == 1) {
                CustomerList = obj.CustomerList;
                CustomerList = CustomerList[0];
                //debugger;
                var Div = '';

                for (var i = 0; i < CustomerList.length; i++) {
                    Div += '<option value="' + CustomerList[i].Email + '" >' + CustomerList[i].Email + '</option>'
                }
                $("#Select_Email").append(Div);
            }
            else if (obj.retCode == -1) {
                //$('#SpnMessege').text("Somthing went wrong. Please try again.")
                //$("#ModelMessege").modal("show")
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}

function VehicleNameDropDown() {
    debugger;
    $.ajax({
        type: "POST",
        url: "ReservationHandler.asmx/VehicleNameDropDown",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Manufacturer = obj.VehicleName;
            $("#ddl_VehicleType").empty();
            //$("#Addl_VehicleType").empty();
            var OptionVehicleType = '';
            OptionVehicleType += '<option value="0">Select Vehicle Type</option>'

            if (obj.retCode == 1) {
                for (var i = 0; i < Manufacturer.length; i++) {

                    if (Manufacturer[i].sid != "4052")
                        OptionVehicleType += '<option value="' + Manufacturer[i].sid + '" >' + Manufacturer[i].Model + '</option>'
                }

                $("#ddl_VehicleType").append(OptionVehicleType);
                //$("#Addl_VehicleType").append(VehicleType);
            }
            else {

            }
        }
    });
}

function CheckRemark(Text) {
    debugger;
    var FinalRemark = '';

    //alert(asdf);
    Remark = $('#txt_AreaRemarks').val();
    var Splitter = Remark.split(',');
    for (var i = 0; i < Splitter.length; i++) {
        if (Text == Splitter[i] && i == 0) {
            Splitter[i] = '';
            for (var j = 0; j < Splitter.length; j++) {
                if (j == 0 || j == 1) {
                    FinalRemark = Splitter[j];
                }

                else {
                    if (Splitter[j] != '')
                        FinalRemark += "," + Splitter[j];
                }
            }
        }
        else if (Text == Splitter[i]) {
            Splitter[i] = '';
            for (var j = 0; j < Splitter.length; j++) {
                if (j == 0) {
                    FinalRemark = Splitter[j];
                }
                else {
                    if (Splitter[j] != '')
                        FinalRemark += "," + Splitter[j];
                }
            }
        }
    }
    $('#txt_AreaRemarks').val(FinalRemark);
}

function SetRemark(text) {
    debugger;
    Remark = $('#txt_AreaRemarks').val();
    if (Remark != '') {
        Remark += "," + text;
        $('#txt_AreaRemarks').val(Remark);
    }
    else {
        Remark = text;
        $('#txt_AreaRemarks').val(Remark);
    }
}

function GetAllPhoneNo() {

    $.ajax({
        url: "ReservationHandler.asmx/GetAllEmail",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);

            if (obj.retCode == 1) {
                CustomerList = obj.CustomerList;
                CustomerList = CustomerList[0];
                //debugger;
                var Div = '';

                for (var i = 0; i < CustomerList.length; i++) {
                    Div += '<option value="' + CustomerList[i].Mobile + '" >' + CustomerList[i].Mobile + '</option>'
                }
                $("#Select_ContactNumber").append(Div);
            }
            else if (obj.retCode == -1) {
                //$('#SpnMessege').text("Somthing went wrong. Please try again.")
                //$("#ModelMessege").modal("show")
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}

function SetTime(Time) {
    debugger;
    try {
        var time = Time.split(":");
        if (time.length == 3) {
            var hours = Number(time[0]);
            var minutes = Number(time[1]);
            var AMPM = time[2];
            if (AMPM == "PM" && hours < 12) hours = hours + 12;
            if (AMPM == "AM" && hours == 12) hours = hours - 12;
            var sHours = hours.toString();
            var sMinutes = minutes.toString();
            if (hours < 10) sHours = "0" + sHours;
            if (minutes < 10) sMinutes = "0" + sMinutes;
            //alert(sHours + ":" + sMinutes);
            return (sHours + ":" + sMinutes);
        }
        else {
            return Time;
            /* Kashif Khanna
            Time = Time.split(':');
            if ((Time.length == 3 && Time[0] != 12) || Time[0] == "00")
                return Time[0] + ":" + Time[1];
            else {
                //alert((12 + parseFloat(Time[0])) + ":" + Time[1])
                if (Time[0] == 12)

                    return "00:" + Time[1].replace("AM", "").replace("PM", "");
                else
                    return (12 + parseFloat(Time[0])) + ":" + Time[1];
            }*/
        }
    }
    catch (ex) {

    }

}

var OfferList = "";
function GetAllOfferCode() {
    $.ajax({
        url: "ReservationHandler.asmx/GetAllOfferCode",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            $("#ddlOfferCode").empty();
            if (obj.retCode == 1) {
                OfferList = obj.OfferList;
                var Div = '';

                for (var i = 0; i < OfferList.length; i++) {
                    Div += '<option value="0" >Select Offer Code</option>'
                    Div += '<option value="' + OfferList[i].Sid + '" >' + OfferList[i].Code + '</option>'
                }
                $("#ddlOfferCode").append(Div);
            }

        },
    });
}

var OfferPer = 0;
var OfferId = 0;
function CalcOffer() {
    OfferId = $("#ddlOfferCode option:selected").val();
    if (OfferId == 0 || OfferId == null) {
        OfferPercent = 0;
    }
    else {
        for (var i = 0; i < OfferList.length; i++) {
            if (OfferList[i].Sid == OfferId) {
                OfferPercent = OfferList[i].Percents;
            }
        }
    }

    var Tab = "3";
    var VehicleSid = $('#ddl_VehicleType').val();
    var data = { Tab: Tab, sid: VehicleSid }
    $.ajax({
        type: "POST",
        url: "ReservationRateHandler.asmx/GetRate",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            debugger
            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {
                var Arr = obj.Arr
                Hour = $('#Hours').val();
                HourlyRate = Arr[0].HourlyRate
                var SubTotal = (parseFloat(Hour) * parseFloat(HourlyRate));
                //SubTotal = SubTotal;
                if (OfferPercent != 0 && OfferPercent != null) {
                    var sub = (parseFloat(SubTotal) / 100) * parseFloat(OfferPercent);
                    SubTotal = parseFloat(SubTotal) - parseFloat(sub);
                }
                sum = SubTotal;
                $('#txt_Fare').val(sum.toFixed(2))
                if (Gratuity > 0) {
                    var GraAmount = (parseFloat(Gratuity) * parseFloat(sum)) / 100;
                    GratuityAmount = GraAmount;
                    sum = parseFloat(sum) + parseFloat(GraAmount)
                }
                if (Chk_PetInCage) {
                    sum = parseFloat(sum) + 10
                }
                if (ChildCarSheet) {
                    sum = parseFloat(sum) + 10
                }
                if (MeetGreat) {
                    sum = parseFloat(sum) + 10
                }

                if (Parking != 0) {

                    sum = parseFloat(sum) + parseFloat(Parking)
                }
                if (Toll != 0) {

                    sum = parseFloat(sum) + parseFloat(Toll)
                }
                if (Chk_rub) {
                    sum = parseFloat(sum) + 10
                }
                sum = sum.toFixed(2);
                $('#txt_Total').val(sum)
            }
        },
    });
}