﻿$(document).ready(function () {
    GetAllRates();
    VehicleNameDropDown();
    VehicleNameDropDownUpdate();
});
function AddDialog() {
    $('#AddDilogBox').modal('show');
    //VehicleNameDropDown();
}
function UpdateDialog(sid) {

    $('#UpdateDilogBox').modal('show');
    //VehicleNameDropDownUpdate();
    GetRate(sid);
}
var Id;
var vName;
var Group;
var VehName;
var BaseDistance;
var BaseCharge;
var CostDistance;
var MilesDistance;
var UptoMiles;
var RateDetails;
function VehicleNameDropDown() {
    debugger;
    $.ajax({
        type: "POST",
        url: "DistanceEstimateHandler.asmx/VehicleNameDropDown",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Manufacturer = obj.VehicleName;
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;

            $("#ddl_SelectName").empty();

            var OptionMac_Name;
            OptionMac_Name += '<option value="0">--Select Vehicle--</option>'

            if (obj.retCode == 1) {
                for (var i = 0; i < Manufacturer.length; i++) {

                    OptionMac_Name += '<option value="' + Manufacturer[i].sid + '" >' + Manufacturer[i].Model + '</option>'
                }
                var s = OptionMac_Name.split('undefined')
                vName = s[1];
                $("#ddl_SelectName").append(vName);
            }

            else {

            }
        }
    });
}

function AddDistanceRate()
{
    debugger;
    var Bvalid = Validation();
    if (Bvalid == true) {

        Group = $("#Select_Gruop option:selected").text();
        VehName = $("#ddl_SelectName option:selected").text();
        BaseDistance = "0";
        BaseCharge = $("#txt_BaseCharge").val();
        CostDistance = "0";
        MilesDistance = $("#txt_MilesDistance").val();
        UptoMiles = "0";
        var data = {
            Group: Group,
            VehName: VehName,
            BaseDistance: BaseDistance,
            BaseCharge: BaseCharge,
            CostDistance: CostDistance,
            MilesDistance: MilesDistance,
            UptoMiles: UptoMiles
        }
        $.ajax({
            type: "POST",
            url: "DistanceEstimateHandler.asmx/AddDistanceRate",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (obj.retCode == 1) {
                    $('#SpnMessege').text("Vehicle Rate Inserted Successfully.")
                    $('#ModelMessege').modal('show')
                    $('#AddDilogBox').modal('hide');
                    GetAllRates();
                }
                else if (obj.retCode == 2) {
                    $('#SpnMessege').text("This Vehicle Rate is already Exist")
                    $('#ModelMessege').modal('show')
                    //alert("This Vehicle is already Exist");
                    //window.location.reload();
                }

                else {
                    alert("Something Went Wrog");
                    window.location.reload();
                }
            }
        });
    }
}

function GetAllRates() {
    $.ajax({
        type: "POST",
        url: "DistanceEstimateHandler.asmx/GetAllRates",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Rates = obj.RateDetails;
            var ul;
            $("#RateDetails").empty();
            if (obj.retCode == 1) {
                for (var i = 0; i < Rates.length; i++) {
                    ul += '<tr class="odd">'
                    ul += '<td align="center" style="width: 10%">' + (i+1) + '</td>'
                    ul += '<td align="center" style="width:10%">' + Rates[i].Name + '</td>'
                    //ul += '<td align="center" style="width:10%">' + Rates[i].RateGroup + '</td>'
                    //ul += '<td align="center" style="width:10%">' + Rates[i].BaseDistance + '</td>'
                    ul += '<td align="center" style="width:10%">' + Rates[i].BaseCharge + '</td>'
                    //ul += '<td align="center" style="width:10%">' + Rates[i].UptoMiles + '</td>'
                    //ul += ' <td align="center" style="width:10%">' + Rates[i].CostPerDistance + '</td>'
                    ul += '<td align="center"style="width:10%">' + Rates[i].MilesPerDistance + '</td>'
                    ul += '<td align="center" style="width:20%"><a style="cursor: pointer" onclick="UpdateDialog(' + Rates[i].sid + ')" ><span class="glyphicon glyphicon-edit" title="Edit"></span></a> | <a style="cursor: pointer" onclick="DeleteRate(' + Rates[i].sid + ')" href="#"><span class="glyphicon glyphicon-trash" title="Delete"></span></a></td>'
                    ul += '</tr>'
                }
                var s = ul.split('undefined')
                RateDetails = s[1];
                $("#RateDetails").append(RateDetails);
            }

            else {

            }
        }
    });
}

function VehicleNameDropDownUpdate() {
    debugger;
    $.ajax({
        type: "POST",
        url: "DistanceEstimateHandler.asmx/VehicleNameDropDown",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Manufacturer = obj.VehicleName;
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;

            $("#ddl_SelectVehName").empty();

            var OptionMac_Name='';
            OptionMac_Name += '<option value="0">--Select Vehicle--</option>'

            if (obj.retCode == 1) {
                for (var i = 0; i < Manufacturer.length; i++) {

                    OptionMac_Name += '<option value="' + Manufacturer[i].sid + '" >' + Manufacturer[i].Model + '</option>'
                }
                //var s = OptionMac_Name.split('undefined')
                //vName = s[1];
                $("#ddl_SelectVehName").append(OptionMac_Name);
            }

            else {

            }
        }
    });
}

function GetRate(sid) {
    Id = sid;
    debugger;
    var data = { sid: sid }
    $.ajax({
        type: "POST",
        url: "DistanceEstimateHandler.asmx/GetRate",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Rates = obj.RateDetails;
            var ul = null;
            //$("#RateDetails").empty();
            if (obj.retCode == 1) {
                //$("#txtBaseDistance").val(Rates[0].BaseDistance);
                $("#txtBaseCharge").val(Rates[0].BaseCharge);
                //$("#txtCostDistance").val(Rates[0].CostPerDistance);
                $("#txtMilesDistance").val(Rates[0].MilesPerDistance);
                //$("#txtUptoMiles").val(Rates[0].UptoMiles);
                $("#ddl_SelectVehName option").each(function ()
                {
                    if ($(this).html() == Rates[0].Name)
                    {
                        $(this).attr("selected", "selected");
                        return;
                    }
                });

                //$("#ddl_SelectVehName option:selected").text(Rates[0].Name);
                //$("#Select_Group").val(Rates[0].RateGroup);
            }
            else {
            }
        }
    });
}

function UpdateRate() {
    debugger;
    var Bvalid = ValidationForUpdate();
    if (Bvalid == true) {
        Group = $("#Select_Group option:selected").text();
        VehName = $("#ddl_SelectVehName option:selected").text();
        BaseDistance = "0";
        BaseCharge = $("#txtBaseCharge").val();
        CostDistance = "0";
        MilesDistance = $("#txtMilesDistance").val();
        UptoMiles = "0";

        var data = {
            sid: Id,
            Group: Group,
            VehName: VehName,
            BaseDistance: BaseDistance,
            BaseCharge: BaseCharge,
            CostDistance: CostDistance,
            MilesDistance: MilesDistance,
            UptoMiles: UptoMiles
        }
        debugger;
        $.ajax({
            type: "POST",
            url: "DistanceEstimateHandler.asmx/UpdateRate",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    $('#UpdateDilogBox').modal('hide');
                    $('#SpnMessege').text("Vehicle Rate Updated Successfully.")
                    $('#ModelMessege').modal('show')
                    GetAllRates();
                    ClearUpdateField();
                }
                else if (obj.retCode == -1) {
                    alert("Record is not updated")
                }
                else if (obj.retCode == 0) {
                    alert("Something Went Wrong.")
                }
            }
        });
    }
}

function Validation() {
    if ($("#Select_Gruop").val() == 0) {
        alert("Please select Group");
        $("#Select_Gruop").focus();
        return false;
    }
    else if ($("#ddl_SelectName").val() == 0) {
        alert("Please select Vehicle Name");
        $("#ddl_SelectName").focus();
        return false;
    }
    else if ($("#txt_BaseDistance").val() == "") {
        alert("Please enter Base Distance");
        $("#txt_BaseDistance").focus();
        return false;
    }
    else if ($("#txt_BaseCharge").val() == "") {
        alert("Please enter Base Charge");
        $("#txt_BaseCharge").focus();
        return false;
    }
    else if ($("#txt_UptoMiles").val() == "") {
        alert("Please enter Upto (Miles)");
        $("#txt_UptoMiles").focus();
        return false;
    }
    else if ($("#txt_CostDistance").val() == "")
    {
    alert("Please enter Cost Per Distance");
    $("#txt_CostDistance").focus();
        return false;
    }
    else if ($("#txt_MilesDistance").val() == "") {
        alert("Please enter Miles Per Distance");
        $("#txt_MilesDistance").focus();
        return false;
    }
    return true;
}

function ValidationForUpdate() {
    //if ($("#Select_Group").val() == 0) {
    //    alert("Please select Group");
    //    $("#Select_Group").focus();
    //    return false;
    //}
    //else if ($("#ddl_SelectVehName").val() == 0) {
    //    alert("Please select Vehicle Name");
    //    $("#ddl_SelectVehName").focus();
    //    return false;
    //}
    if ($("#txtBaseDistance").val() == "") {
        alert("Please enter Hourly Minimum");
        $("#txtBaseDistance").focus();
        return false;
    }
    else if ($("#txtBaseCharge").val() == "") {
        alert("Please enter Service");
        $("#txtService").focus();
        return false;
    }
    else if ($("#txtCostDistance").val() == "") {
        alert("Please enter Upto (Hours)");
        $("#txtUptoHours").focus();
        return false;
    }
    else if ($("#txtMilesDistance").val() == "") {
        alert("Please enter Hourly Rate");
        $("#txtHourlyRate").focus();
        return false;
    }
    return true;
}

function DeleteRate(sid) {
    if (confirm("Are you sure, you want to delete this record ?") == true)
    {
        var data = { sid: sid }
        debugger;
        $.ajax({
            type: "POST",
            url: "DistanceEstimateHandler.asmx/DeleteRate",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    $('#SpnMessege').text("Vehicle Rate Deleted Successfully.")
                    $('#ModelMessege').modal('show')
                    GetAllRates();
                }
                else if (obj.retCode == -1) {
                    alert("Record is not Deleted")
                }
                else if (obj.retCode == 0) {
                    alert("Something Went Wrong.")
                }
            },
        });
    }
}

function ClearUpdateField() {
    $("#txtBaseDistance").val("");
    $("#txtBaseCharge").val("");
    $("#txtCostDistance").val("");
    $("#txtMilesDistance").val("");
}