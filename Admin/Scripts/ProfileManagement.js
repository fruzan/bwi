﻿var hiddensid


$(function () {

    $.ajax({
        type: "POST",
        url: "../DefaultHandler.asmx/GetAdminProfile",
        data: '',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)

            if (result.Retcode == 1) {
                var Array = result.Arr;
                if (Array.length > 0) {
                    debugger;


                    hiddensid = Array[0].Sid;
                    $('#txtFirstName').val(Array[0].sFirstName);
                    $('#txtLastName').val(Array[0].sLastName);

                    $('#txtMobileNumber').val(Array[0].sMobile);

                    $('#txtEmail').val(Array[0].sEmail);
                    $('#txtPhone').val(Array[0].sPhone);
                    $('#txtAddress').val(Array[0].sPAddress);
                    //$('#selCountry').val(Array[0].Country);
                    $('#txtPinCode').val(Array[0].sPPincode);
                    //$('#txtPanNumber').val(Array[0].PANNo);
                    //$('#txtFax').val(Array[0].Fax);
                    //$('#txtUsername').val(Array[0].uid);

                    //$('#divLogo').append('<img src="../images/dash/logo.png" alt="Logo not found" style="width:50%" />');
                }

            }
            if (result.Retcode == 0) {
                alert("Something went wrong!")
            }
        },
        error: function () {
            alert("An error occured while loading agent details")
        }
    });

});

function Update_Click() {
    debugger;

    sFirstName = $('#txtFirstName').val();
    sLastName = $('#txtLastName').val();
    sMobile = $('#txtMobileNumber').val();
    sEmail = $('#txtEmail').val();
    sPhone = $('#txtPhone').val();
    sAddress = $('#txtAddress').val();
    sCountry = $('#selCountry').val();
    sCode = $('#selCity').val();
    sPinCode = $('#txtPinCode').val();
    sPanNumber = $('#txtPanNumber').val();

    var data = {
        sFirstName: sFirstName,
        sLastName: sLastName,
        sMobile: sMobile,
        sEmail: sEmail,
        sPhone: sPhone,
        sAddress: sAddress,
        sCountry: sCountry,
        sCode: sCode,
        sPinCode: sPinCode,
        sPanNumber: sPanNumber
    }

    $.ajax({
        type: "POST",
        url: "../DefaultHandler.asmx/UpdateProfile",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = JSON.parse(response.d)
            if (result.Retcode == 1) {

                $('#SpnMessege').text("Profile Updated")
                $('#ModelMessege').modal('show')
            }
            if (result.Retcode == 0) {
                $('#SpnMessege').text("Profile not Updated")
                $('#ModelMessege').modal('show')

            }
        },
        error: function () {
            $('#SpnMessege').text("An error occured while updating")
            $('#ModelMessege').modal('show')
        }
    });
}






