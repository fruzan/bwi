﻿var Arr;

function GetAllDriver() {
    $.ajax({
        url: "ReservationHandler.asmx/GetAllDriver",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Driver = obj.tblDriver;
           
            if (obj.retCode == 1) {
                arrService = obj.tblDriver;
                debugger;
                var OptionMac_Name = null;

                if (obj.retCode == 1) {
                    for (var i = 0; i < Driver.length; i++) {
                        var fullName = Driver[i].sFirstName + " " + Driver[i].sLastName;
                        OptionMac_Name += '<option value="' + Driver[i].Sid + '" >' + fullName + '</option>'
                    }
           
                    $("#sel_driver").append(OptionMac_Name);
                }
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}

function Submit() {
    var From = $('#datepicker1').val();
    var To = $('#datepicker2').val();
    var DriverName = $("#sel_driver").val();
    var data = { From: From, To: To, DriverId: DriverName }
    $("#Details").dataTable().fnClearTable();
    $("#Details").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "ReportHandler.asmx/DriverActivity",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                Arr = obj.Arr
                var ul = '';
                //$("#Details").empty();
                for (var i = 0; i < Arr.length; i++) {

                    ul += '<tr>';
                    ul += '<td align="center" >' + (i + 1) + '</td>';
                    //ul += '<td align="center" style="cursor: pointer;"><a>' + Arr[i].ReservationID + '</a></td>';
                    ul += '<td align="center" style="cursor: pointer;" onclick="BookingDetails('+i+')"><a>' + Arr[i].ReservationID + '</a></td>';
                    ul += '<td align="center">'+Arr[i].AssignedTo+'</td>';
                    ul += '<td align="center">' + Arr[i].FirstName + ' ' + Arr[i].LastName + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationDate+ '</td>';
                    ul += '<td align="center">' + Arr[i].Service+ '</td>';
                    ul += '<td align="center">' + Arr[i].Source+ '</td>';
                    ul += '<td align="center">' + Arr[i].Destination+ '</td>';
                    ul += '<td align="center">' + Arr[i].TotalFare + '</td>';
                    ul += '</tr>';

                }
                $("#TopSpan").html('<span>Fare : $' + parseFloat(obj.Fare).toFixed(2) + '</span>, <span>Percentage Fare :  $' + parseFloat(obj.PercentageOfFare).toFixed(2) + '</span>, <span>Tip :  $' + parseFloat(obj.Tip).toFixed(2) + '</span>,<span>Total Fare : $' + parseFloat(obj.TotalFare).toFixed(2) + '</span>, <span>Drivers Payout :  $' + parseFloat(obj.DriversPayOut).toFixed(2) + '</span>');
                $("#Details tbody").append(ul);
                $("#Details").dataTable({
                    "bSort": false
                });
            }
            else {
                $('#SpnMessege').text("No Record Found")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });

}

function GetAll() {
    $("#Details").dataTable().fnClearTable();
    $("#Details").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "ReportHandler.asmx/DriverActivityLoadAll",
        data: '',
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                Arr = obj.Arr
                var ul = '';
                //$("#Details").empty();
                for (var i = 0; i < Arr.length; i++)
                {
                    ul += '<tr>';
                    ul += '<td align="center" >' + (i + 1) + '</td>';
                    ul += '<td align="center">' + Arr[i].AssignedTo + '</td>';
                    ul += '<td align="center">' + Arr[i].FirstName + ' ' + Arr[i].LastName + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationDate + '</td>';
                    ul += '<td align="center">' + Arr[i].Service + '</td>';
                    ul += '<td align="center">' + Arr[i].Source + '</td>';
                    ul += '<td align="center">' + Arr[i].Destination + '</td>';
                    ul += '<td align="center">' + Arr[i].TotalFare + '</td>';
                    ul += '</tr>';
                }

                $("#Details tbody").append(ul);
                $("#Details").dataTable({
                    "bSort": false
                });
                //$("#TopSpan").html('<span>Total Fare:' + obj.TotalFare + '</span>,<span>Percentage Fare: ' + obj.PercentageOfFare + '</span>,<span>Drivers Payout: ' + obj.DriversPayOut + '</span>');
            }
            else {
                $('#SpnMessege').text("No Record Found.")
                $('#ModelMessege').modal('show')

            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });

}

function BookingDetails(ObjNo)
{
    var Name=Arr[ObjNo].FirstName+" "+Arr[ObjNo].LastName;
    $("#BookingDetails").modal("show");
    $("#BookingNo").text(Arr[ObjNo].ReservationID);
    $("#ResDate").text(Arr[ObjNo].ReservationDate);
    $("#Source").text(Arr[ObjNo].Source);
    $("#Destination").text(Arr[ObjNo].Destination);
    $("#AssignedTo").text(Arr[ObjNo].AssignedTo);
    $("#Name").text(Name);
    $("#Service").text(Arr[ObjNo].Service);
    $("#TotalFare").text("$ " + Arr[ObjNo].TotalFare); Passenger
    $("#Passenger").text(Arr[ObjNo].Persons);
    $("#PhoneNo").text(Arr[ObjNo].ContactNumber);
}

function ExportToExcel() {
    debugger;
    var From = $('#datepicker1').val();
    var To = $('#datepicker2').val();
    var DriverName = $("#sel_driver").val();

    window.location.href = "ExportToExcelHandler.ashx?datatable=" + 'dtDriver' + "&From=" + From + "&To=" + To + "&DriverName=" + DriverName
}

