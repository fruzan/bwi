﻿$(document).ready(function () {
    GetAllRates();
    VehicleNameDropDownUpdate();
    VehicleNameDropDown();
});
function AddDialog() {
    $('#AddDilogBox').modal('show');
    //VehicleNameDropDown();
}
function UpdateDialog(sid) {
    
    $('#UpdateDilogBox').modal('show');
    //VehicleNameDropDownUpdate();
    GetRate(sid);
}
var Id;
var vName;
var Group;
var VehName;
var HourlyMinimum;
var Service;
var UptoHours;
var HourlyRate;
var RateDetails;
function VehicleNameDropDown() {
    debugger;
    $.ajax({
        type: "POST",
        url: "HourlyRateHandler.asmx/VehicleNameDropDown",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Manufacturer = obj.VehicleName;
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;

            $("#ddl_SelectName").empty();

            var OptionMac_Name;
            OptionMac_Name += '<option value="0">--Select Vehicle--</option>'

            if (obj.retCode == 1) {
                for (var i = 0; i < Manufacturer.length; i++) {

                    OptionMac_Name += '<option value="' + Manufacturer[i].sid + '" >' + Manufacturer[i].Model + '</option>'
                }
                var s = OptionMac_Name.split('undefined')
                vName = s[1];
                $("#ddl_SelectName").append(vName);
            }

            else {

            }
        }
    });
}

function AddHourlyRate() {
    debugger;
    var Bvalid = Validation();
    if (Bvalid == true) {

        //Group = $("#ddl_SelectGroup option:selected").text();
        Group = '';
        VehName = $("#ddl_SelectName option:selected").text();
        HourlyMinimum = $("#txt_HourlyMinimum").val();
        //Service = $("#txt_Service").val();
        //UptoHours = $("#txt_UptoHours").val();
        HourlyRate = $("#txt_HourlyRate").val();
        Service = "0";
        UptoHours = "0";
        
        var data = {
            Group: Group,
            VehName: VehName,
            HourlyMinimum: HourlyMinimum,
            Service: Service,
            UptoHours: UptoHours,
            HourlyRate: HourlyRate
        }
        $.ajax({
            type: "POST",
            url: "HourlyRateHandler.asmx/AddHourlyRate",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                if (obj.retCode == 1) {
                    $('#SpnMessege').text("Hourly Rate Added Successfully")
                    $('#ModelMessege').modal('show')
                    $('#AddDilogBox').modal('hide');
                    //window.location.reload();
                    GetAllRates();
                }
                else if (obj.retCode == 2) {
                    $('#SpnMessege').text("This Vehicle Rate is already Exist")
                    $('#ModelMessege').modal('show');
                    $('#AddDilogBox').modal('hide');
                }

                else {
                    alert("Something Went Wrog");
                    window.location.reload();
                }
            }
        });
    }
}

function GetAllRates() {
    $.ajax({
        type: "POST",
        url: "HourlyRateHandler.asmx/GetAllRates",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Rates = obj.RateDetails;
            var ul;
            $("#RateDetails").empty();
            if (obj.retCode == 1) {
                for (var i = 0; i < Rates.length; i++) {
                    ul += '<tr class="odd">'
                    //ul += '<td align="center" style="width: 10%">' + Rates[i].sid + '</td>'
                    ul += '<td align="center" style="width: 10%">' + parseInt(i+1) + '</td>'
                    ul += '<td align="center" style="width:10%">' + Rates[i].Name + '</td>'
                    //ul += '<td align="center" style="width:10%">' + Rates[i].RateGroup + '</td>'
                    ul += '<td align="center" style="width:10%">' + Rates[i].HourlyMinimum + '</td>'
                    //ul += '<td align="center" style="width:10%">' + Rates[i].Service + '</td>'
                    //ul += '<td align="center" style="width:10%">' + Rates[i].ServiceUpto + '</td>'
                    ul += ' <td align="center" style="width:10%">' + Rates[i].HourlyRate + '</td>'
                    ul += '<td align="center" style="width:20%"><a style="cursor: pointer" onclick="UpdateDialog(' + Rates[i].sid + ')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a> | <a style="cursor: pointer" onclick="DeleteRate(' + Rates[i].sid + ')" href="#"><span class="glyphicon glyphicon-trash" title="Delete"></span></a></td>'
                    ul += '</tr>'
                }
                var s = ul.split('undefined')
                RateDetails = s[1];
                $("#RateDetails").append(RateDetails);
            }
            else if(obj.retCode == 2)
            {
                $('#SpnMessege').text("No Record found")
                $('#ModelMessege').modal('show')
            }

            else {

            }
        }
    });
}

function VehicleNameDropDownUpdate() {
    debugger;
    $.ajax({
        type: "POST",
        url: "HourlyRateHandler.asmx/VehicleNameDropDown",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Manufacturer = obj.VehicleName;
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;

            $("#ddl_SelectVehName").empty();

            var OptionMac_Name;
            //OptionMac_Name += '<option value="0">--Select Vehicle--</option>'

            if (obj.retCode == 1) {
                for (var i = 0; i < Manufacturer.length; i++) {

                    OptionMac_Name += '<option value="' + Manufacturer[i].sid + '" >' + Manufacturer[i].Model + '</option>'
                }
                var s = OptionMac_Name.split('undefined')
                vName = s[1];
                $("#ddl_SelectVehName").append(vName);
            }

            else {

            }
        }
    });
}

function GetRate(sid) {
    Id = sid;
    debugger;
    var data = { sid: sid }
    $.ajax({
        type: "POST",
        url: "HourlyRateHandler.asmx/GetRate",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Rates = obj.RateDetails;
            var ul=null;
            //$("#RateDetails").empty();
            if (obj.retCode == 1)
            {
                $("#txtHourlyMinimum").val(Rates[0].HourlyMinimum);
                //$("#txtService").val(Rates[0].Service);
                //$("#txtUptoHours").val(Rates[0].ServiceUpto);
                $("#txtHourlyRate").val(Rates[0].HourlyRate);
                $("#ddl_SelectVehName option").each(function () {
                    if ($(this).html() == Rates[0].Name) {
                        $(this).attr("selected", "selected");
                        return;
                    }
                });
                //$("#ddl_SelectVehName").val(Rates[0].Name);
                //$("#Select_Group").val(Rates[0].RateGroup);
            }
           
        }
    });
}

function UpdateRate() {
    debugger;
    var Bvalid = ValidationForUpdate();
    if (Bvalid == true)
    {
        Group = $("#Select_Group option:selected").text();
        VehName = $("#ddl_SelectVehName option:selected").text();
        HourlyMinimum = $("#txtHourlyMinimum").val();
        //Service = $("#txtService").val();
        //UptoHours = $("#txtUptoHours").val();
        Service = "0";
        UptoHours = "0";
        HourlyRate = $("#txtHourlyRate").val();
        
        var data = {
            sid: Id,
            Group: Group,
            VehName: VehName,
            HourlyMinimum: HourlyMinimum,
            Service: Service,
            UptoHours: UptoHours,
            HourlyRate: HourlyRate
        }
        debugger;
        $.ajax({
            type: "POST",
            url: "HourlyRateHandler.asmx/UpdateRate",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1)
                {
                    $('#UpdateDilogBox').modal('hide');
                    $('#SpnMessege').text("Hourly Rate Updated Successfully")
                    $('#ModelMessege').modal('show')
                    GetAllRates();
                    ClearUpdateField();
                }
                else if (obj.retCode == -1) {
                    alert("Record is not updated")
                }
                else if (obj.retCode == 0) {
                    alert("Something Went Wrong.")
                }
            }
        });
    }
}

function Validation() {
    if ($("#ddl_SelectGroup").val() == 0) {
        alert("Please select Group");
        $("#ddl_SelectGroup").focus();
        return false;
    }
    else if ($("#ddl_SelectName").val() == 0) {
        alert("Please select Vehicle Name");
        $("#ddl_SelectName").focus();
        return false;
    }
    else if ($("#txt_HourlyMinimum").val() == "") {
        alert("Please enter Hourly Minimum");
        $("#txt_HourlyMinimum").focus();
        return false;
    }
    else if ($("#txt_Service").val() == "") {
        alert("Please enter Service");
        $("#txt_Service").focus();
        return false;
    }
    else if ($("#txt_UptoHours").val() == "") {
        alert("Please enter Upto (Hours)");
        $("#txt_UptoHours").focus();
        return false;
    }
    else if ($("#txt_HourlyRate").val() == "") {
        alert("Please enter Hourly Rate");
        $("#txt_HourlyRate").focus();
        return false;
    }
    return true;
}

function ValidationForUpdate() {
    if ($("#Select_Group").val() == 0) {
        alert("Please select Group");
        $("#Select_Group").focus();
        return false;
    }
    //else if ($("#ddl_SelectVehName").val() == 0) {
    //    alert("Please select Vehicle Name");
    //    $("#ddl_SelectVehName").focus();
    //    return false;
    //}
    else if ($("#txtHourlyMinimum").val() == "") {
        alert("Please enter Hourly Minimum");
        $("#txtHourlyMinimum").focus();
        return false;
    }
    else if ($("#txtService").val() == "") {
        alert("Please enter Service");
        $("#txtService").focus();
        return false;
    }
    else if ($("#txtUptoHours").val() == "") {
        alert("Please enter Upto (Hours)");
        $("#txtUptoHours").focus();
        return false;
    }
    else if ($("#txtHourlyRate").val() == "") {
        alert("Please enter Hourly Rate");
        $("#txtHourlyRate").focus();
        return false;
    }
    return true;
}

function ClearUpdateField()
{
    $("#txtHourlyMinimum").val("");
    $("#txtService").val("");
    $("#txtUptoHours").val("");
    $("#txtHourlyRate").val("");
}

function DeleteRate(sid)
{
    if (confirm("Are you sure, you want to delete this record ?") == true)
    {
        var data = { sid: sid }
        debugger;
        $.ajax({
            type: "POST",
            url: "HourlyRateHandler.asmx/DeleteRate",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    $('#SpnMessege').text("Hourly Rate Deleted Successfully")
                    $('#ModelMessege').modal('show')
                    GetAllRates();
                }
                else if (obj.retCode == -1) {
                    alert("Record is not Deleted")
                }
                else if (obj.retCode == 0) {
                    alert("Something Went Wrong.")
                }
            },
        });
    }
}