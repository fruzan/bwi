﻿$(function () {
    LoadAllComment()
})

function LoadAllComment() {
    $.ajax({
        type: "POST",
        url: "../DefaultHandler.asmx/LoadAllComment",
        data: '{}',
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            $("#Comments").empty();
            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                var Div = '';
                for (var i = 0; i < Arr.length; i++) {
                    Div += '<tr class="odd">';
                    Div += '<td align="center" style="width: 10%">' + (i + 1) + '</td>';
                    Div += '<td align="center">' + Arr[i].Name + '</td>';
                    Div += '<td align="center">' + Arr[i].Text + '</td>';
                    Div += '<td align="center">' + Arr[i].Date + '</td>';
                    Div += '<td align="center"> <a style="cursor: pointer" onclick="DeleteComment(' + Arr[i].Sid + ')"><span class="glyphicon glyphicon-trash" ></span></a></td>';
                    Div += '</tr>';
                }
                $("#Comments").append(Div);
            }
            else {
                alert('Something going wrong while deleting comment');
            }
        },
    });
}

function DeleteComment(Sid)
{
    var Data = { Sid: Sid };
    $.ajax({
        type: "POST",
        url: "../DefaultHandler.asmx/DeleteComment",
        data: JSON.stringify(Data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d)
            $("#Comments").empty();
            if (obj.retCode == 1) {
                LoadAllComment();
                alert("Comment deleted Successfully")
            }
        },
    });
}