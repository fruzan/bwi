﻿$(function () {

    LoadAll()


})
var FN;
var Lanme;
var Mobile;
var Email;
var Password;
var Address;
var Country;
var City;
var PinCode;
var bValid;
function LoadAll() {

    $.ajax({
        type: "POST",
        url: "UserHandler.asmx/GetAll",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                $('#Details').empty();
                var Arr = obj.Details;
                var Content = '';
                for (var i = 0; i < Arr.length; i++) {
                    Content = '';
                    Content += '<tr class="odd">';
                    Content += '<td align="center" style="width: 10%">'+(i+1)+'</td>';
                    Content += '<td align="center">' + Arr[i].sFirstName + '  ' + Arr[i].sLastName + '</td>';
                    Content += '<td align="center">' + Arr[i].sUserType + '</td>';
                    Content += '<td align="center">' + Arr[i].sMobile + '</td>';
                    Content += '<td align="center">' + Arr[i].sEmail + '</td>';
                    Content += '<td align="center"><a style="cursor: pointer" onclick="UpdateDetails(' + Arr[i].Sid + ')" href="#"><span class="glyphicon glyphicon-edit" ></span></a> | <a style="cursor: pointer" onclick="Delete(' + Arr[i].Sid + ')" href="#"><span class="glyphicon glyphicon-trash" ></span></a></td>';
                    Content += '</tr>';

                    $('#Details').append(Content)
                }

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("No record found")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function ValidateAddUser()
{
    FN = $('#Fname').val();
    Lanme = $('#Lanme').val()
    Mobile = $('#Mobile').val()
    //var TextMessage = $('#TextMessage').val()
    Email = $('#Email').val()
    Password = $('#Password').val()

    Address = $('#Address').val()
    Country = $('#Country').val()
    City = $('#City').val()
    PinCode = $('#PinCode').val()

    if (FN == '') {
        $('#SpnMessege').text("Please Insert First Name")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Lanme == '') {
        $('#SpnMessege').text("Please Insert Last Name")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Mobile == '') {
        $('#SpnMessege').text("Please Insert Mobile No")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Email == '') {
        $('#SpnMessege').text("Please Insert Email")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Password == '') {
        $('#SpnMessege').text("Please Insert Password")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Address == '') {
        $('#SpnMessege').text("Please Insert Address")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (City == '') {
        $('#SpnMessege').text("Please Insert City")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Country == '') {
        $('#SpnMessege').text("Please Insert Country")
        $('#ModelMessege').modal('show')
        return false;
    }

    return true;
}

function ValidateUpdateUser() {
    FN = $('#AFname').val();
    Lanme = $('#ALanme').val()
    Mobile = $('#AMobile').val()
    //var TextMessage = $('#TextMessage').val()
    Email = $('#AEmail').val()
    Password = $('#APassword').val()

    Address = $('#AAddress').val()
    Country = $('#ACountry').val()
    City = $('#ACity').val()
    PinCode = $('#APinCode').val()

    if (FN == '') {
        $('#SpnMessege').text("Please Insert First Name")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Lanme == '') {
        $('#SpnMessege').text("Please Insert Last Name")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Mobile == '') {
        $('#SpnMessege').text("Please Insert Mobile No")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Email == '') {
        $('#SpnMessege').text("Please Insert Email")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Password == '') {
        $('#SpnMessege').text("Please Insert Password")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Address == '') {
        $('#SpnMessege').text("Please Insert Address")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (City == '') {
        $('#SpnMessege').text("Please Insert City")
        $('#ModelMessege').modal('show')
        return false;
    }
    else if (Country == '') {
        $('#SpnMessege').text("Please Insert Country")
        $('#ModelMessege').modal('show')
        return false;
    }

    return true;
}

function Insert() {
    debugger;
    bValid = ValidateAddUser();
    if (bValid == true)
    {
        var UserType = $('#UserType').val()
        //var Active = $('#Active').val()
        var Active = "True";

        var data = {

            FN: FN,
            Lanme: Lanme,
            Mobile: Mobile,
            //TextMessage: TextMessage,
            Email: Email,
            Password: Password,
            UserType: UserType,
            Active: Active,
            Address: Address,
            Country: Country,
            City: City,
            PinCode: PinCode
        }
        $.ajax({
            type: "POST",
            url: "UserHandler.asmx/Add",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    //$('#UpdateDilogBox').modal('hide')
                    LoadAll();
                    $('#SpnMessege').text("System User Added Successfully.")
                    $('#ModelMessege').modal('show')
                    $('#AddUser').modal('hide');

                    ClearValues();
                    //LoadAll();
                }

                else if (obj.retCode == -1) {
                    $('#SpnMessege').text("User is not Added")
                    $('#ModelMessege').modal('show')
                }

                else if (obj.retCode == 0) {
                    $('#SpnMessege').text("Something Went Wrong.")
                    $('#ModelMessege').modal('show')
                }
            }
        });
    }
}

function UpdateUser() {

    bValid = ValidateUpdateUser();
    if (bValid == true)
    {
        var sid = $('#hdn').val();
        //var UserType = "Admin"
        //var Active = $('#Active').val()
        var Active = "True";
        FN = $('#AFname').val();
        Lanme = $('#ALanme').val()
        Mobile = $('#AMobile').val()
        //var TextMessage = $('#ATextMessage').val()
        Email = $('#AEmail').val()
        Password = $('#APassword').val()
        Address = $('#AAddress').val()
        Country = $('#ACountry').val()
        City = $('#ACity').val()
        PinCode = $('#APinCode').val()
        var UserType = $('#AUserType').val()
        //var Active = $('#AActive').val()

        var data = {
            sid: sid,
            FN: FN,
            Lanme: Lanme,
            Mobile: Mobile,
            //TextMessage: TextMessage,
            Email: Email,
            Password: Password,
            UserType: UserType,
            Active: Active,
            Address: Address,
            Country: Country,
            City: City,
            PinCode: PinCode
        }
        $.ajax({
            type: "POST",
            url: "UserHandler.asmx/Update",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    $('#Details').empty();
                    $('#UpdateUser').modal('hide')
                    LoadAll();
                    $('#SpnMessege').text("System User Updated Successfully.")
                    $('#ModelMessege').modal('show')

                }

                else if (obj.retCode == -1) {
                    $('#SpnMessege').text("User is not Updated")
                    $('#ModelMessege').modal('show')
                }

                else if (obj.retCode == 0) {
                    $('#SpnMessege').text("Something Went Wrong.")
                    $('#ModelMessege').modal('show')
                }
            }
        });
    }
}

function UpdateDetails(sid) {
    $('#hdn').val(sid);

    var data = {
        sid: sid,
    }
    $.ajax({
        type: "POST",
        url: "UserHandler.asmx/UpdateDetails",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Details;

                $('#hdn').val(sid);
                $('#AFname').val(Arr[0].sFirstName);
                $('#ALanme').val(Arr[0].sLastName)
                $('#AMobile').val(Arr[0].sMobile)
                $('#AEmail').val(Arr[0].sEmail)
                $('#APassword').val(Arr[0].sPassword)
                $('#AAddress').val(Arr[0].sPAddress)
                $('#ACountry').val(Arr[0].sPCountry)
                $('#ACity').val(Arr[0].sPCity)
                $('#APinCode').val(Arr[0].sCPincode)
                $('#AUserType').val(Arr[0].sUserType)
                $('#UpdateUser').modal('show')
            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("User is not found")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function Delete(sid) {
    if (confirm("Are you sure, you want to delete this record ?") == true)
    {
        var data = {
            sid: sid,
        }
        $.ajax({
            type: "POST",
            url: "UserHandler.asmx/Delete",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    $('#Details').empty();
                    LoadAll();
                    $('#SpnMessege').text("System User Deleted Successfully")
                    $('#ModelMessege').modal('show')
                }

                else if (obj.retCode == -1) {
                    $('#SpnMessege').text("User is not Deleted")
                    $('#ModelMessege').modal('show')
                }

                else if (obj.retCode == 0) {
                    $('#SpnMessege').text("Something Went Wrong.")
                    $('#ModelMessege').modal('show')
                }
            }
        });
    }
}

function ClearValues() {
$('#Fname').val('');
$('#Lanme').val('');
$('#Mobile').val('');
$('#TextMessage').val('');
$('#Email').val('');
$('#Password').val('');
$('#Address').val('');
$('#Country').val('');
$('#City').val('');
$('#PinCode').val('');
$('#UserType').val('Admin');
$('#Active').prop('checked', false);
}
