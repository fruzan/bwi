﻿$(function () {
    Load();
})

function Load() {
    debugger;
    $.ajax({
        type: "POST",
        url: "ServiceHandler.asmx/Load",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                var Content = '';

                for (var i = 0; i < Arr.length; i++) {
                    var status = Arr[i].IsActive
                    Content = '';
                    Content += '<tr class="odd">';
                    Content += ' <td align="center" style="width: 10%">' + (i + 1) + '</td>';
                    Content += '<td align="center">' + Arr[i].Name + '</td>';
                    Content += '<td align="center"><a style="cursor: pointer" onclick="UpdateDetails(' + Arr[i].sid + ')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a></td>';
                    Content += '<td align="center"><a style="cursor: pointer" onclick="Delete(' + Arr[i].sid + ')" href="#" ><span class="glyphicon glyphicon-trash" title="Edit"></span></a></td>';

                    if (status == true) {
                        Content += '<td align="center"><a style="cursor: pointer" onclick="Active(' + Arr[i].sid + ')" href="#"><span class="glyphicon glyphicon-eye-open" title="Active"></span></a> </td>';
                    }
                    else {
                        Content += '<td align="center"><a style="cursor: pointer" onclick="Active(' + Arr[i].sid + ')" href="#"><span class="fa fa-eye-slash" title="Deactive"></span></a> </td>';
                    }
                    Content += '</tr>';

                    $('#Details').append(Content);
                }

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("No Record")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function Delete(sid) {
    var data = { sid: sid }
    debugger;
    $.ajax({
        type: "POST",
        url: "ServiceHandler.asmx/Delete",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                $('#Details').empty();
                Load()

                $('#SpnMessege').text("Record Delted")
                $('#ModelMessege').modal('show')

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not Delted")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });

}

function UpdateDetails(sid) {
    debugger;

    var data = { sid: sid }
    debugger;
    $.ajax({
        type: "POST",
        url: "ServiceHandler.asmx/GetSingle",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Arr; 

                //alert(Arr[0].Description);
                $('#AFname').val(Arr[0].Name);
           
                $('#AArea').val(Arr[0].Description);
                $('#hdn').val(sid);
            
                $('#Update').modal('show')

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function Update() {
    debugger;

    var Sid = $('#hdn').val();
    var Name = $('#AFname').val();
    var Description = $('#AArea').val();

    if (Name == '') {
        $('#SpnMessege').text("Please Insert Name")
        $('#ModelMessege').modal('show')
        return false
    }
 

    var data = { sid: Sid, Name: Name, Description: Description }
    debugger;
    $.ajax({
        type: "POST",
        url: "ServiceHandler.asmx/Update",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);


            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                $('#Update').modal('hide')
                $('#Details').empty();
                Load()
                $('#SpnMessege').text("Record updated")
                $('#ModelMessege').modal('show')

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });



}

function Insert() {
    debugger;
    var Name = $('#Fname').val();
    var Description = $('#Area').val();

    var data = { Name: Name, Description: Description }

    if (Name == '') {
        $('#SpnMessege').text("Please Insert Name")
        $('#ModelMessege').modal('show')
        return false
    }

    debugger;
    $.ajax({
        type: "POST",
        url: "ServiceHandler.asmx/Insert",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);


            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                $('#Add').modal('hide')
                $('#Details').empty();
                Load()
                $('#SpnMessege').text("Record Inserted")
                $('#ModelMessege').modal('show')

                $("#Fname").val('');
                $("#Area").val('');
            }

            if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not Inserted")
                $('#ModelMessege').modal('show')
            }

            if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function Open() {
    debugger
    //$('#Fname').val('');
    $('#Add').modal('show')
}

function Active(sid) {
    var data = { sid: sid }
    debugger;
    $.ajax({
        type: "POST",
        url: "ServiceHandler.asmx/Active",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                $('#Details').empty();
                Load();
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });

}