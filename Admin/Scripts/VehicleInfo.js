﻿var GlobalArray;
var ManufacturerName;
var ModelName;
var RegistrationYear;
var VehicleType;
var Capacity;
var Baggage;
var Remarks;
var Mac_Name;
var VehicleType;
var Image_Path;
$(function () {
    debugger;
    loadAll();

    VehicleMakeDropDown()

    VehicleTypeDropDown()
})

function AddVehicleInfo() {
    $("#AddVehicleInfo").modal("show");
}

function cleartextboxes() {
    $("#ddl_VehicleMake").val(0);
    $("#txt_ModelName").val("");
    $("#txt_RegistrationYear").val("");
    $("#ddl_VehicleType").val(0);
    $("#txt_MaxCapacity").val("");
    $("#txt_MaxBaggage").val("");
    $("#f_img").val("");
    $("#txt_Remarks").val("");
}

function AddVehicle() {
    debugger;
    var Bvalid = Validation();
    if (Bvalid == true) {

        ManufacturerName = $("#ddl_VehicleMake option:selected").val();
        ModelName = $("#txt_ModelName").val();
        RegistrationYear = $("#txt_RegistrationYear").val();
        VehicleType = $("#ddl_VehicleType option:selected").val();
        Capacity = $("#txt_MaxCapacity").val();
        if (Capacity == "") {
            Capacity = 0;
        }
        Baggage = $("#txt_MaxBaggage").val();
        if (Baggage == "") {
            Baggage = 0;
        }
        Remarks = $("#txt_Remarks").val();

        var data = {
            ManufacturerName: ManufacturerName,
            ModelName: ModelName,
            RegistrationYear: RegistrationYear,
            VehicleType: VehicleType,
            Capacity: Capacity,
            Baggage: Baggage,
            Remarks: Remarks,
            Image_Path:Image_Path
        }
        $.ajax({
            type: "POST",
            url: "VehicleInfoHandler.asmx/AddVehicleInfo",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    $('#SpnMessege').text("Vehicle Added Successfully");
                    $('#ModelMessege').modal('show');
                    $("#AddVehicleInfo").modal("hide");
                    ClearAll();
                    loadAll();
                }

                else {
                    alert("Something Went Wrog");
                }
            }
        });
    }
}

function Validation() {
    if ($("#ddl_VehicleMake").val() == 0) {
        $('#SpnMessege').text("Please select Vehicle Manufacturer");
        $('#ModelMessege').modal('show');
        $("#ddl_VehicleMake").focus();
        return false;
    }
    else if ($("#ddl_VehicleType").val() == 0) {
        $('#SpnMessege').text("Please select Vehicle Type");
        $('#ModelMessege').modal('show');
        $("#ddl_VehicleType").focus();
        return false;
    }
    else if ($("#txt_ModelName").val() == "") {
        $('#SpnMessege').text("Please enter Model Name");
        $('#ModelMessege').modal('show');
        $("#txt_ModelName").focus();
        return false;
    }
    else if ($("#txt_RegistrationYear").val() == "") {
        $('#SpnMessege').text("Please enter Registration Year");
        $('#ModelMessege').modal('show');
        $("#txt_RegistrationYear").focus();
        return false;
    }
    else if ($("#txt_MaxCapacity").val() == "") {
        $('#SpnMessege').text("Please Provide Max Capacity");
        $('#ModelMessege').modal('show');
        $("#txt_MaxCapacity").focus();
        return false;
    }
    else if ($("#txt_MaxBaggage").val() == 0) {
        $('#SpnMessege').text("Please Provide Max Baggage");
        $('#ModelMessege').modal('show');
        $("#txt_MaxBaggage").focus();
        return false;
    }
    return true;
}

function ValidationForUpdate() {
    if ($("#Addl_VehicleMake").val() == 0) {
        alert("Please select Vehicle Manufacturer No.");
        $("#ddl_VehicleMake").focus();
        return false;
    }
    else if ($("#Atxt_ModelName").val() == "") {
        alert("Please enter Model Name");
        $("#txt_ModelName").focus();
        return false;
    }
    else if ($("#Atxt_RegistrationYear").val() == "") {
        alert("Please enter Registration Year");
        $("#txt_RegistrationYear").focus();
        return false;
    }
    else if ($("#Addl_VehicleType").val() == 0) {
        alert("Please select Vehicle Type");
        $("#ddl_VehicleType").focus();
        return false;
    }

    return true;
}

function VehicleMakeDropDown() {
    debugger;
    $.ajax({
        type: "POST",
        url: "VehicleInfoHandler.asmx/VehicleMakeDropDown",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Manufacturer = obj.VehicleMake;
            $("#ddl_VehicleMake").empty();
            $("#Addl_VehicleMake").empty();
            var OptionMac_Name='';
            OptionMac_Name += '<option value="0" selected="selected">--Select Vehicle Manufacturer--</option>'

            if (obj.retCode == 1) {
                for (var i = 0; i < Manufacturer.length; i++) {

                    OptionMac_Name += '<option value="' + Manufacturer[i].sid + '" >' + Manufacturer[i].Name + '</option>'
                }
                //var s = OptionMac_Name.split('undefined')
                //Mac_Name = s[1];
                $("#ddl_VehicleMake").append(OptionMac_Name);
                $("#Addl_VehicleMake").append(OptionMac_Name);
            }

            else {

            }
        }
    });
}

function VehicleTypeDropDown() {
    debugger;
    $.ajax({
        type: "POST",
        url: "VehicleInfoHandler.asmx/VehicleTypeDropDown",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Manufacturer = obj.VehicleType;
            $("#ddl_VehicleType").empty();
            $("#Addl_VehicleType").empty();
            var OptionVehicleType='';
            OptionVehicleType += '<option value="0">--Select Vehicle Type--</option>'

            if (obj.retCode == 1) {
                for (var i = 0; i < Manufacturer.length; i++) {

                    OptionVehicleType += '<option value="' + Manufacturer[i].sid + '" >' + Manufacturer[i].Name + '</option>'
                }
                //var s = OptionVehicleType.split('undefined')
                //VehicleType = s[1];
                $("#ddl_VehicleType").append(OptionVehicleType);
                $("#Addl_VehicleType").append(OptionVehicleType);
            }

            else {

            }
        }
    });
}

function loadAll() {
    $("#Details").dataTable().fnClearTable();
    $("#Details").dataTable().fnDestroy();
    debugger;
    $.ajax({
        type: "POST",
        url: "VehicleInfoHandler.asmx/LoadAll",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.VehicleMake;
                GlobalArray = obj.VehicleMake;
                var Content = '';
                //$('#Details').empty();
                for (var i = 0; i < Arr.length; i++) {
                    Content = '';
                    Content += '<tr>';
                    Content += '<td style="width: 152px;">' + (i + 1) + '';
                    Content += '</td>';
                    Content += '<td style="width: 174px;">' + Arr[i].Company + '';
                    Content += '</td>';
                    Content += '<td style="width: 153px;">' + Arr[i].Model + '';
                    Content += '</td>';
                    Content += '<td style="width: 153px;">' + Arr[i].Year_Registration + '';
                    Content += '</td>';
                    Content += '<td style="width: 153px;">' + Arr[i].CarType + '';
                    Content += '</td>';
                    Content += '<td style="width: 153px;">' + Arr[i].Max_Capcity + '';
                    Content += '</td>';
                    Content += '<td style="width: 153px;">' + Arr[i].Max_Baggage + '';
                    Content += '</td>';
                    //Content += '<td style="width: 153px;"><a style="cursor: pointer" onclick="OpenImage(' + Arr[i].Img_Url + ')" href="#"><span class="glyphicon glyphicon-picture"></span></a>';
                    //Content += '</td>';
                    Content += '<td align="center" class=" "><a style="cursor: pointer" onclick="UpdateDetails(\'' + Arr[i].sid + '\',\'' + Arr[i].Img_Url + '\')" href="#"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a></td>';
                    Content += ' </tr>';
                    $('#Details tbody').append(Content);
                }
                $("#Details").dataTable({
                    "bSort": false
                });
            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("No Record Found")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });

}

function UpdateDetails(sid,url) {
    debugger;

    $('#hdn').val(sid);
    Image_Path = url;
    ImgVehicleUpdate.src = url;
    var ArryUpdate = GlobalArray;

    for (var i = 0; i < ArryUpdate.length; i++) {
        if (ArryUpdate[i].sid == sid) {
            $('#Update').modal('show');
            $('#Atxt_ModelName').val(ArryUpdate[i].Model)
            var VehMake = ArryUpdate[i].Company;
            var VehType = ArryUpdate[i].CarType;
            debugger;
            //setTimeout(function () {
            //    $("#Addl_VehicleMake option").filter(function () {
            //        return $(this).val() == VehMake;
            //    }).prop("selected", true);
            //}, 1000);

            setTimeout(function () {
                debugger;
            $("#Addl_VehicleMake option").each(function () {
                if ($(this).html() == VehMake) {
                    $(this).attr("selected", "selected");
                    return;
                }
            });
            }, 400);
            $("#Addl_VehicleType option").each(function () {
                if ($(this).html() == VehType) {
                    $(this).attr("selected", "selected");
                    return;
                }
            });
            //$('#Addl_VehicleMake option:selected').text(ArryUpdate[i].Company)
            $('#Atxt_RegistrationYear').val(ArryUpdate[i].Year_Registration)
            //$('#Addl_VehicleType option:selected').text(ArryUpdate[i].CarType)
            $('#Atxt_MaxCapacity').val(ArryUpdate[i].Max_Capcity)
            $('#Atxt_MaxBaggage').val(ArryUpdate[i].Max_Baggage)
            $("#Atxt_Remarks").val(ArryUpdate[i].Remark);
            break;
        }
    }
}

function UpdateVehicle() {
    debugger;
    var Bvalid = ValidationForUpdate();
    if (Bvalid == true) {

        ManufacturerName = $("#Addl_VehicleMake option:selected").val();
        ModelName = $("#Atxt_ModelName").val();
        RegistrationYear = $("#Atxt_RegistrationYear").val();
        VehicleType = $("#Addl_VehicleType option:selected").val();
        Capacity = $("#Atxt_MaxCapacity").val();
        if (Capacity == "") {
            Capacity = 0;
        }
        Baggage = $("#Atxt_MaxBaggage").val();
        if (Baggage == "") {
            Baggage = 0;
        }
        Remarks = $("#Atxt_Remarks").val();
        var sid = $('#hdn').val();
        var data = {
            sid: sid,
            ManufacturerName: ManufacturerName,
            ModelName: ModelName,
            RegistrationYear: RegistrationYear,
            VehicleType: VehicleType,
            Capacity: Capacity,
            Baggage: Baggage,
            Remarks: Remarks,
            Image_Path: Image_Path
        }
        $.ajax({
            type: "POST",
            url: "VehicleInfoHandler.asmx/UpdateVehicleInfo",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    $('#Details').empty();
                    loadAll()
                    $('#SpnMessege').text("Vehicle Updated Successfully");
                    $('#ModelMessege').modal('show');
                    //$('#Update').modal('hide');
                    window.location.reload();
                }

                else {
                    loadAll()
                    $('#SpnMessege').text("Something Went Wrog");
                    $('#ModelMessege').modal('show');
                    $('#Update').modal('hide');
                }
            }
        });
    }
}

function Delete(sid) {
    debugger;
    if (confirm("Are you sure, you want to delete this record ?") == true)
    {
        var data = {
            sid: sid,
        }
        $.ajax({
            type: "POST",
            url: "VehicleInfoHandler.asmx/Delete",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    $('#Details').empty();
                    loadAll()
                    $('#SpnMessege').text("Vehicle Deleted Successfully");
                    $('#ModelMessege').modal('show')

                }
                else {
                    $('#SpnMessege').text("Something Went Wrog");
                    $('#ModelMessege').modal('show')
                }
            }
        });
    }
}

function VehicleImage() {
    debugger;
    var obj = $("#Vehicle_Image");
    var Files = obj[0].files

    var formData = new FormData();
    formData.append('file', Files[0]);
    $.ajax({
        type: 'post',
        url: 'VehicleImageHandler.ashx',
        data: formData,
        success: function (status) {
            if (status != 'error') {
                Image_Path = "../Admin/VehicleImage/" + status;
                $("#ImgVehicle").attr("src", Image_Path);
                document.getElementById("ImgVehicle").setAttribute("style", "width:300px;height:200px");
                document.getElementById("hrefImgVehicle").setAttribute("href", Image_Path);
            }
        },
        processData: false,
        contentType: false,
        error: function () {
            alert("Whoops something went wrong!");
        }
    });
}

function VehicleImageUpdate() {
    debugger;
    var obj = $("#Af_img");
    var Files = obj[0].files

    var formData = new FormData();
    formData.append('file', Files[0]);
    $.ajax({
        type: 'post',
        url: 'VehicleImageHandler.ashx',
        data: formData,
        success: function (status) {
            if (status != 'error') {
                Image_Path = "../Admin/VehicleImage/" + status;
                $("#ImgVehicleUpdate").attr("src", Image_Path);
                document.getElementById("ImgVehicleUpdate").setAttribute("style", "width:300px;height:200px");
                document.getElementById("hrefImgVehicleUpdate").setAttribute("href", Image_Path);
            }
        },
        processData: false,
        contentType: false,
        error: function () {
            alert("Whoops something went wrong!");
        }
    });
}

function ClearAll()
{
    $('#txt_ModelName').val('');
    $('#txt_RegistrationYear').val('');
    $('#txt_MaxCapacity').val('');
    $('#txt_MaxBaggage').val('');
    $("#txt_Remarks").val('');
    $('#hrefImgVehicle').empty(); 
    $('#Vehicle_Image').empty();
    var Text = "--Select Vehicle Manufacturer--";

    $("#Addl_VehicleMake option").each(function () {
        if ($(this).html() == Text) {
            $(this).attr("selected", "selected");
            return;
        }
    });
    $("#Addl_VehicleType option").each(function () {
        if ($(this).html() == Text) {
            $(this).attr("selected", "selected");
            return;
        }
    });
    //$('#ddl_VehicleType option:selected').val(0);
    //$('#ddl_VehicleMake option:selected').val(0);
}