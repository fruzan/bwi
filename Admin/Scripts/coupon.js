﻿$(function () {

    LoadCoupons();
})

function LoadCoupons() {

    debugger;
    $.ajax({
        type: "POST",
        url: "Coupons.asmx/LoadCoupons",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                $('#Details').empty();
                var Arr = obj.Arr;
                var Content = '';

                for (var i = 0; i < Arr.length; i++) {
                    Content = '';
                    Content += '<tr class="odd">';
                    Content += ' <td align="center" style="width: 10%">' + (i + 1) + '</td>';
                    Content += '<td align="center">' + Arr[i].Name + '</td>';
                    Content += '<td align="center">' + Arr[i].Code + '</td>';
                    Content += '<td align="center">' + Arr[i].Percents + '</td>';
                    //var Act = Arr[i].Status;
                    //if (Act == "Activate") {
                    //    Content += '<td align="center" style="width:10%"><span class="glyphicon glyphicon-eye-open" onclick="ActiveAirlines(' + Arr[i].Sid + ')" style="cursor:pointer" title="Activate/Deactivate"></span></td>'
                    //}
                    //else {
                    //    Content += '<td align="center" style="width:10%"><span class="glyphicon glyphicon-eye-close" onclick="ActiveAirlines(' + Arr[i].Sid + ')" style="cursor:pointer" title="Activate/Deactivate"></span></td>'
                    //}
                    //Content += '<td align="center" ><a style="cursor: pointer" onclick="UpdateDetails(' + Arr[i].Sid + ')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a> | <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="DeleteAirLines(' + Arr[i].Sid + ')"></span></a></td>';
                    Content += '<td align="center" ><a style="cursor: pointer" onclick="UpdateDetails(' + Arr[i].Sid + ')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a></td>';
                    Content += '<td align="center" ><a style="cursor: pointer" onclick="DeleteCoupons(' + Arr[i].Sid + ')" href="#"><span class="glyphicon glyphicon-remove" title="Delete"></span></a></td>';
                    Content += '</tr>';

                    $('#Details').append(Content);
                }

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function DeleteCoupons(sid) {
    if (confirm("Are you sure you want to delete this?")) {
        var data = { Sid: sid }
        debugger;
        $.ajax({
            type: "POST",
            url: "Coupons.asmx/DeleteCoupons",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    var Arr = obj.Arr;
                    //$('#Details').empty();
                    LoadCoupons();
                    $('#SpnMessege').text("Coupan is Deleted")
                    $('#ModelMessege').modal('show')
                }
                else if (obj.retCode == -1) {
                    $('#SpnMessege').text("Coupan is not Delted")
                    $('#ModelMessege').modal('show')
                }
                else if (obj.retCode == 0) {
                    $('#SpnMessege').text("Something Went Wrong.")
                    $('#ModelMessege').modal('show')
                }
            }
        });
        //$("#delete-button").attr("href", "query.php?ACTION=delete&ID='1'");
    }
    else {
        return false;
    }
}

function UpdateDetails(sid) {
    debugger;

    var data = { sid: sid }
    debugger;
    $.ajax({
        type: "POST",
        url: "Coupons.asmx/GetSingle",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;

                $('#offername').val(Arr[0].Name);
                $('#offercode').val(Arr[0].Code);
                $('#offerPercent').val(Arr[0].Percents);
                $('#hdn').val(sid);
                $('#Submit').val('Update')
                $('#Update').modal('show')

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function UpdateCoupons() {
    debugger;

    var Sid = $('#hdn').val();
    var Name = $('#offername').val();
    var Code = $('#offercode').val();
    var Percents = $('#offerPercent').val();

    if (Name == '') {
        $('#SpnMessege').text("Please Insert Name")
        $('#ModelMessege').modal('show')
        return false
    }
    if (Code == '') {
        $('#SpnMessege').text("Please Insert Code")
        $('#ModelMessege').modal('show')
        return false
    }
    if (Percents == '') {
        $('#SpnMessege').text("Please Insert Percents")
        $('#ModelMessege').modal('show')
        return false
    }

    var data = { Sid: Sid, Name: Name, Code: Code, Percents: Percents }
    debugger;
    $.ajax({
        type: "POST",
        url: "Coupons.asmx/UpdateCoupons",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);


            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                $('#Update').modal('hide')
                $('#Details').empty();
                LoadCoupons();
                $('#SpnMessege').text("Coupon updated Successfully")
                $('#ModelMessege').modal('show')

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });



}

function InsertCoupon() {
    debugger;
    var Name = $('#offrname').val();
    var Code = $('#offrcode').val();
    var Percents = $('#ofrpercent').val();

    var data = { Name: Name, Code: Code, Percents: Percents }

    if (Name == '') {
        $('#SpnMessege').text("Please Insert Name")
        $('#ModelMessege').modal('show')
        return false
    }
    if (Code == '') {
        $('#SpnMessege').text("Please Insert Code")
        $('#ModelMessege').modal('show')
        return false
    }
    if (Percents == '') {
        $('#SpnMessege').text("Please Insert Percent")
        $('#ModelMessege').modal('show')
        return false
    }
    debugger;
    $.ajax({
        type: "POST",
        url: "Coupons.asmx/InsertCoupon",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);


            if (obj.retCode == 1) {
                var Arr = obj.Arr;
                $('#Add').modal('hide')
                $('#Details').empty();
                LoadCoupons();
                $('#SpnMessege').text("Coupon Inserted Successfully")
                $('#ModelMessege').modal('show')

            }

            if (obj.retCode == 3) {
                var Arr = obj.Arr;
                $('#Add').modal('hide')
                $('#SpnMessege').text("Name Already Exist")
                $('#ModelMessege').modal('show')

            }
            if (obj.retCode == 4) {
                var Arr = obj.Arr;
                $('#Add').modal('hide')
                $('#SpnMessege').text("Code Already Exist")
                $('#ModelMessege').modal('show')

            }
            if (obj.retCode == 5) {
                var Arr = obj.Arr;
                $('#Add').modal('hide')
                $('#SpnMessege').text("Percent Already Exist")
                $('#ModelMessege').modal('show')

            }


            if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not Inserted")
                $('#ModelMessege').modal('show')
            }

            if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function Open() {
    $('#offrname').val('');
    $('#offrcode').val('');
    $('#ofrpercent').val('');
    $('#Add').modal('show')
}

function ActiveAirlines(Sid) {
    var data = {
        Sid: Sid
    }
    $.ajax({
        type: "POST",
        url: "AirLinesHandler.asmx/ActiveAirLines",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (obj.retCode == 1) {
                LoadAirLines();
                $('#SpnMessege').text("Status Change Successfully")
                $('#ModelMessege').modal('show')
                //alert("Status Change Successfully");
            }
            else {
                alert("Something Went Wrog");
                window.location.reload();
            }
        }
    });
}