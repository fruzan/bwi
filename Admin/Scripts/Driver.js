﻿$(document).ready(function () {
    GetAllDriver();
    RandomNo();
});
var Fname;
var Lname;
var Gender;
var Mobile;
var Address;
var Country;
var City;
var Pincode;
var Email;
var DriverD;
var sid;
var Profile;
var Lic1
var Lic2
var Percentage;
var formData12;
var Random;
var Password;
function AddDialogBox() {
    ClearValues();
    //var fu1 = document.getElementById('Profile');
    //if (fu1 != null) {
    //    document.getElementById('Profile').outerHTML = fu1.outerHTML;
    //}
    //var fu2 = document.getElementById('LicenceUpload1');
    //if (fu2 != null) {
    //    document.getElementById('LicenceUpload1').outerHTML = fu2.outerHTML;
    //}
    //var fu3 = document.getElementById('LicenceUpload2');
    //if (fu3 != null) {
    //    document.getElementById('LicenceUpload2').outerHTML = fu3.outerHTML;
    //}
    $("#btn_RegisterDriver").val('Add');
    $("#btn_RegisterDriver").text('Add');
    $("#AddUpdateDialogBox").modal("show")
    
}

function ClearValues() {
    $("#Fname").val('');
    $("#Lname").val('');
    $("#Address").val('');
    $("#City").val('');
    $("#Mobile").val('');
    $("#Country").val('');
    $("#Email").val('');
    $("#Pin").val('');
    $("#Male").prop("checked", true);
    $("#hdn_id").val('0');
    $("#Profile").val(null);
    $("#LicenceUpload1").val(null);
    $("#LicenceUpload2").val(null);
    $("#dvPreview").empty();
    $("#dvPreview2").empty();
    $("#dvPreview3").empty();
    $("#Select_Percentage").val('0');

}

function UpdateDilogBox() {

    $("#UpdateDilogBox").modal("show")
}
function DeleteDriver(sid) {
    if (confirm("Are you sure, you want to delete this record ?") == true) {
        var data = { sid: sid }
        debugger;
        $.ajax({
            type: "POST",
            url: "DriverHandler.asmx/DeleteDriver",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    $('#SpnMessege').text("Driver Deleted Successfully")
                    $('#ModelMessege').modal('show')
                    GetAllDriver();
                }
                else if (obj.retCode == -1) {
                    $('#SpnMessege').text("Record is not Deleted")
                    $('#ModelMessege').modal('show')
                }
                else if (obj.retCode == 0) {
                    alert("Something Went Wrong.")
                }
            },
        });
    }
}

function AddUpdateDriver() {
    debugger

    var Bvalid = Validation();

    if (Bvalid == true) {

        sid = $("#hdn_id").val();
        Fname = $("#Fname").val();
        Lname = $("#Lname").val();

        var Male = document.getElementById('Male');
        var Female = document.getElementById('Female');
        if (Male.checked == true) {
            Gender = "Male";
        }
        else if (Female.checked == true) {
            Gender = "Female";
        }

        Mobile = $("#Mobile").val();
        Address = $("#Address").val();
        Country = ""
        City = ""
        Pincode = ""
        Email = $("#Email").val();
        Percentage = $("#Select_Percentage").val();
        //alert(Profile);

        if ($("#btn_RegisterDriver").val() == "Add") {
            var ext1 = ($("#Profile").val()).split('.');
            Profile = Random + "1." + ext1[ext1.length - 1]
            var ext2 = ($("#LicenceUpload1").val()).split('.');
            Lic1 = Random + "2." + ext2[ext2.length - 1]
            var ext3 = ($("#LicenceUpload2").val()).split('.');
            Lic2 = Random + "3." + ext3[ext3.length - 1]
            var Bool = UploadDocuments();
            if (Bool == true) {
                var data = {
                    sid: sid,
                    Fname: Fname,
                    Lname: Lname,
                    Gender: Gender,
                    Mobile: Mobile,
                    Address: Address,
                    Percentage: Percentage,
                    Country: Country,
                    City: City,
                    Pincode: Pincode,
                    Email: Email,
                    Password: $("#Password").val(),
                    Profile: Profile,
                    Lic1: Lic1,
                    Lic2: Lic2
                }
                $.ajax({
                    type: "POST",
                    url: "DriverHandler.asmx/AddDriver",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (response) {
                        var obj = JSON.parse(response.d);
                        //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                        if (obj.retCode == 1) {
                            $('#SpnMessege').text("Driver Added Successfully")
                            $('#ModelMessege').modal('show')
                            $("#UpdateDilogBox").modal("hide");
                            $("#AddUpdateDialogBox").modal("hide");
                            GetAllDriver();
                            $("#dvPreview").empty();
                            $("#dvPreview2").empty();
                            $("#dvPreview3").empty();
                            $("#Password").val('');
                        }
                        else {
                            alert("Something Went Wrong");
                            window.location.reload();
                        }
                    }
                });
            }
        }
        else if ($("#btn_RegisterDriver").val() == "Update") {
            //RandomNo();
            if ($("#Profile").val() != "") {
                var ext1 = ($("#Profile").val()).split('.');
                Profile = Random + "1." + ext1[ext1.length - 1]
            }
            else {
                Profile = "";
            }
            if ($("#LicenceUpload1").val() != "") {
                var ext2 = ($("#LicenceUpload1").val()).split('.');
                Lic1 = Random + "2." + ext2[ext2.length - 1]
            }
            else {
                Lic1 = ""
            }
            if ($("#LicenceUpload2").val() != "") {
                var ext3 = ($("#LicenceUpload2").val()).split('.');
                Lic2 = Random + "3." + ext3[ext3.length - 1]
            }
            else {
                Lic2 = "";
            }

            if (Profile != "" || Lic1 != "" || Lic2 != "") {

                var Bool = UpdateDocuments();
            }
            var data = {
                sid: sid,
                Fname: Fname,
                Lname: Lname,
                Gender: Gender,
                Mobile: Mobile,
                Address: Address,
                Percentage: Percentage,
                Country: Country,
                City: City,
                Pincode: Pincode,
                Email: Email,
                Password: $("#Password").val(),
                Profile: Profile,
                Lic1: Lic1,
                Lic2: Lic2
            }
            $.ajax({
                type: "POST",
                url: "DriverHandler.asmx/UpdateDriver",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    var obj = JSON.parse(response.d);

                    if (obj.retCode == 1) {

                        $('#SpnMessege').text("Driver Updated Successfully")
                        $('#ModelMessege').modal('show')
                        $("#AddUpdateDialogBox").modal("hide");
                        $("#UpdateDilogBox").modal("hide")
                        GetAllDriver();
                        $("#dvPreview").empty();
                        $("#dvPreview2").empty();
                        $("#dvPreview3").empty();
                    }

                    else {
                        alert("Something Went Wrong");
                        window.location.reload();
                    }
                }
            });
        }
    }
}

function UploadDocuments() {
    var Bool = true;
    Bool = SendProfile(" ");
    if (Bool == true) {
        Bool = SendLic1(" ");
        if (Bool == true) {
            Bool = SendLic2(" ");
            if (Bool == true) {
            }
            else {
                alert("error while uploading License 2 image");
            }
        }
        else {
            alert("error while uploading License 1 image");
        }
    }
    else {
        alert("error while uploading Profile image");
    }
    return Bool;
}

function UpdateDocuments() {
    var Bool = true;

    if ($("#Profile").val() != "") {
        Bool = SendProfile($("#hdn_ProfileMapPath").val());
        //if (Bool != true) {
        //    return false;
        //}
    }
    if ($("#LicenceUpload1").val() != "") {
        Bool = SendLic1($("#hdn_License1MapPath").val());
        //if (Bool != true) {
        //    return false;
        //}
    }
    if ($("#LicenceUpload2").val() != "") {
        Bool = SendLic2($("#hdn_License2MapPath").val());
        //if (Bool != true) {
        //    return false;
        //}
    }
    return Bool;
}

function GetAllDriver() {
    $.ajax({
        type: "POST",
        url: "DriverHandler.asmx/GetAllDriver",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Drivers = obj.DriverDetails;
            //var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var ul = '';
            $("#DriverDetails").empty();
            if (obj.retCode == 1) {
                for (var i = 0; i < Drivers.length; i++) {
                    ul += '<tr >'
                    ul += '<td ><a style="cursor: pointer" data-toggle="modal" data-target="#StaffDetailModal" onclick="StaffDetailsModal(); return false" title="Click to view Staff Details">T007452</a></td>'
                    ul += '<td ><a style="cursor: pointer" data-toggle="modal" data-target="#PasswordModal" onclick="PasswordModal(); return false" title="Click to edit Password">' + Drivers[i].Name + '</a></td>'
                    ul += '<td >' + Drivers[i].Address + '</td>'
                    ul += '<td >' + Drivers[i].Email + '</td>'
                    ul += '<td align="center" style="width:35%" ><a style="cursor: pointer" onclick="UpdateDriver(\'' + Drivers[i].sid + '\',\'' + Drivers[i].Name + '\',\'' + Drivers[i].Address + '\',\'' + Drivers[i].City + '\',\'' + Drivers[i].Mobile + '\',\'' + Drivers[i].Email + '\',\'' + Drivers[i].Gender + '\',\'' + Drivers[i].Country + '\',\'' + Drivers[i].Pincode + '\',\'' + Drivers[i].ProfileMapPath + '\',\'' + Drivers[i].License1MapPath + '\',\'' + Drivers[i].License2MapPath + '\',\'' + Drivers[i].Percentage + '\',\'' + Drivers[i].Password + '\')" href="#"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a> | <a style="cursor: pointer" onclick="ShowDocument(\'' + Drivers[i].sid + '\')" href="#"><span class="glyphicon glyphicon-list" title="License" aria-hidden="true"></span></a> | <a style="cursor: pointer" onclick="DeleteDriver(' + Drivers[i].sid + ')" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true"></span></a> </td>'
                    ul += '</tr>'
                }
                $("#DriverDetails").append(ul);
            }
            else {

            }
        }
    });
}

var arrName = new Array();

function UpdateDriver(sid, Name, Address, City, Mobile, Email, Gender, Country, Pincode, ProfileMapPath, License1MapPath, License2MapPath, Percentage, Password) {
    arrName = Name.split(' ');
    $("#Fname").val(arrName[0]);
    $("#Lname").val(arrName[1]);
    $("#Address").val(Address);
    $("#Select_Percentage").val(Percentage);
    $("#City").val(City);
    $("#Mobile").val(Mobile);
    $("#Country").val(Country);
    $("#Email").val(Email);
    $("#Pin").val(Pincode);
    $("#hdn_id").val(sid);
    $("#hdn_ProfileMapPath").val(ProfileMapPath);
    $("#hdn_License1MapPath").val(License1MapPath);
    $("#hdn_License2MapPath").val(License2MapPath);
    $("#Password").val(Password)
    if (Gender == "Male") {
        $("#Male").prop("checked", true);
    }
    else {
        $("#Female").prop("checked", true);
    }
    $("#btn_RegisterDriver").val('Update');
    $("#btn_RegisterDriver").text('Update');
    $("#AddUpdateDialogBox").modal("show");
}

function Validation() {
    if ($("#Fname").val() == "") {
        alert("Please enter First Name");
        $("#Fname").focus();
        return false;
    }
    if ($("#Lname").val() == "") {
        alert("Please enter Last Name");
        $("#Lname").focus();
        return false;
    }
    if ($("#Mobile").val() == "") {
        alert("Please enter Mobile No");
        $("#Mobile").focus();
        return false;
    }
    if ($("#Address").val() == "") {
        alert("Please enter Address");
        $("#Address").focus();
        return false;
    }
    if ($("#Select_Percentage").val() == 0) {
        alert("Please select Percentage");
        $("#Select_Percentage").focus();
        return false;
    }
        //else if ($("#Country").val() == "") {
        //    alert("Please enter Country");
        //    $("#Country").focus();
        //    return false;
        //}
        //else if ($("#City").val() == "") {
        //    alert("Please enter City");
        //    $("#Select_City").focus();
        //    return false;
        //}
    if ($("#Email").val() == "") {
        alert("Please enter Email");
        $("#Email").focus();
        return false;
    }
    if ($("#Email").val() != "") {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test($("#Email").val())) {
            $("#Email").focus();
            alert("Please enter valid Email ID");
            return false;
        }
    }
    if ($("#Password").val() == "") {
        alert("Please enter Password");
        $("#Password").focus();
        return false;
    }
    //...........
    if ($("#btn_RegisterDriver").val() == "Add") {
        if ($("#Profile").val() == "") {
            alert("Please upload Profile.");
            return false;
        }
        else if ($("#LicenceUpload1").val() == "") {
            alert("Please upload Licence 1");
            return false;
        }
        else if ($("#LicenceUpload2").val() == "") {
            alert("Please upload Licence 2");
            return false;
        }
    }
    else {
        if ($("#Profile").val() == "") {
            Profile = "";
        }
        if ($("#LicenceUpload1").val() == "") {
            Lic1 = "";
        }
        if ($("#LicenceUpload2").val() == "") {
            Lic2 = "";
        }
    }
    return true;
}

var _URL = window.URL || window.webkitURL;
$("#Profile").on('change', function () {
    debugger;
    var obj = $("#Profile");
});

var arrMapPath = new Array();

function SendProfile(path) {
    var id = 1;
    var sUrl = 'DriverUploader.ashx?RandomCode=' + Random + '&id=' + id;
    if ($("#btn_RegisterDriver").val() == "Update") {
        Profile = path;
        path = path.split('.');
        Random = path[0];
        sUrl = 'DriverUploader.ashx?RandomCode=' + Random + '&id=' + $("#hdn_id").val() + '&Type=Profile';
        id = "";
    }
    var bValid = true;
    var obj = $("#Profile");
    var Files = obj[0].files
    var formData = new FormData();

    formData.append('file', Files[0]);
    formData.append('path', path);
    $.ajax({
        type: 'post',
        url: sUrl,
        data: formData,
        success: function (status) {
            debugger
            if (status != 'error') {
                var my_path = "../DriverDocument/" + status;
                Profile = my_path;
                //$("#ImgProfile").attr("src", my_path);
                //document.getElementById("ImgProfile").setAttribute("style", "width:200px;height:200px");
                //document.getElementById("hrefImgProfile").setAttribute("href", my_path);
            }
        },
        processData: false,
        contentType: false,
        error: function () {
            bValid = false;
            alert("Whoops something went wrong!");
        }
    });
    return bValid;
}

function SendLic1(path) {

    var id = 2;
    var sUrl = 'DriverUploader.ashx?RandomCode=' + Random + '&id=' + id;
    if ($("#btn_RegisterDriver").val() == "Update") {
        Lic1 = path;
        path = path.split('.');
        Random = path[0];
        sUrl = 'DriverUploader.ashx?RandomCode=' + Random + '&id=' + $("#hdn_id").val() + '&Type=Lic1';
        //id = "";
    }
    var bValid = true;
    var obj = $("#LicenceUpload1");
    var Files = obj[0].files

    var formData = new FormData();
    formData.append('file', Files[0]);
    formData.append('path', path);
    $.ajax({
        type: 'post',
        url: sUrl,
        data: formData,
        success: function (status) {
            debugger
            if (status != 'error') {
                var my_path = "../DriverDocument/" + status;
                Lic1 = my_path;
                //$("#ImgLicenceUpload1").attr("src", my_path);
                //document.getElementById("ImgLicenceUpload1").setAttribute("style", "width:200px;height:200px");
                //document.getElementById("hrefImgLicenceUpload1").setAttribute("href", my_path);
            }
        },
        processData: false,
        contentType: false,
        error: function () {
            bValid = false;
            alert("Whoops something went wrong!");
        }
    });
    return bValid;
}

function SendLic2(path) {

    var id = 3;
    var sUrl = 'DriverUploader.ashx?RandomCode=' + Random + '&id=' + id;
    if ($("#btn_RegisterDriver").val() == "Update") {
        Lic2 = path;
        path = path.split('.');
        Random = path[0];
        sUrl = 'DriverUploader.ashx?RandomCode=' + Random + '&id=' + $("#hdn_id").val() + '&Type=Lic2';
        id = "";
    }
    var bValid = true;
    var obj = $("#LicenceUpload2");
    var Files = obj[0].files

    var formData = new FormData();
    formData.append('file', Files[0]);
    formData.append('path', path);
    $.ajax({
        type: 'post',
        url: sUrl,
        data: formData,
        success: function (status) {
            debugger
            if (status != 'error') {
                var my_path = "../DriverDocument/" + status;
                Lic2 = my_path;
                //$("#ImgLicenceUpload2").attr("src", my_path);
                //document.getElementById("ImgLicenceUpload2").setAttribute("style", "width:200px;height:200px");
                //document.getElementById("hrefImgLicenceUpload2").setAttribute("href", my_path);
            }
        },
        processData: false,
        contentType: false,
        error: function () {
            bValid = false;
            alert("Whoops something went wrong!");
        }
    });
    return bValid;
}

$(function () {
    $("#Profile").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = $("#dvPreview");
            dvPreview.html("");
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function () {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = $("<img />");
                        img.attr("style", "height:200px;width: 200px");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    alert(file[0].name + " is not a valid image file.");
                    dvPreview.html("");
                    return false;
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
    $("#LicenceUpload1").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = $("#dvPreview2");
            dvPreview.html("");
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function () {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = $("<img />");
                        img.attr("style", "height:200px;width: 200px");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    alert(file[0].name + " is not a valid image file.");
                    dvPreview.html("");
                    return false;
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
    $("#LicenceUpload2").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = $("#dvPreview3");
            dvPreview.html("");
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            $($(this)[0].files).each(function () {
                var file = $(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = $("<img />");
                        img.attr("style", "height:200px;width: 200px");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    alert(file[0].name + " is not a valid image file.");
                    dvPreview.html("");
                    return false;
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
});

function RandomNo() {
    debugger
    $.ajax({
        type: "POST",
        url: "DriverHandler.asmx/GenerateRandomNumber",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            Random = obj.Code;
            $("#Password").val(Random.split('Driver-')[1]);
            //alert(Random);
        }
    });
}

function ShowDocument(sid) {
    debugger
    $("#Doc1").empty();
    $("#Doc2").empty();
    $("#Doc3").empty();
    var data = { sid: sid }
    $.ajax({
        type: "POST",
        url: "DriverHandler.asmx/DriverDocument",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);

            var ProfileNew = obj.Profile;
            var Lic1New = obj.Lic1;
            var Lic2New = obj.Lic2;
            var Url = 'thumbnil.aspx?fn=' + obj.Profile + '&w=192&h=192&rf=';
            $("#Doc1").html('<img src="' + Url + '" style="width: 500px; height: 300px" alt="">')
            Url = 'thumbnil.aspx?fn=' + Lic1New + '&w=192&h=192&rf=';
            $("#Doc2").html('<img src="' + Url + '" style="width: 500px; height: 300px" alt="">')
            Url = 'thumbnil.aspx?fn=' + Lic2New + '&w=192&h=192&rf=';
            $("#Doc3").html('<img src="' + Url + '" style="width: 500px; height: 300px" alt="">')

            $("#LicenseDilogBox").modal("show")

        }
    });


}


