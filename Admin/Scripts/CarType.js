﻿$(function () {
    LoadAll();
})


function Insert() {
    var Name = $('#txt_Name').val();
    var Description = $('#txt_Description').val();
    if (Name == '')
    {
        $('#SpnMessege').text("Please insert Vehicle Name")
        $('#ModelMessege').modal('show')
        return false;
    }
    if (Description == '') {
        $('#SpnMessege').text("Please insert Vehicle Description")
        $('#ModelMessege').modal('show')
        return false;
    }
    var data = {
        Name: Name,
        Description: Description
    }
    $.ajax({
        type: "POST",
        url: "CarHandler.asmx/InsertCar",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                $('#AddDilogBox').modal('hide')
                LoadAll();
                $('#SpnMessege').text("Vehicle Type Inserted Successfully.")
                $('#ModelMessege').modal('show')

            }
            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function OpenUpdatePopUp(sid, Name, Description) {
    $('#Upd_Name').val(Name);
    $('#Upd_Description').val(Description);

    $('#UpdateDilogBox').modal('show')
    $('#hdn_Sid').val('');
    $('#hdn_Sid').val(sid);
}

function Update() {
    debugger
    var sid = $('#hdn_Sid').val();
    var Name = $('#Upd_Name').val();
    var Description = $('#Upd_Description').val();

    if (Name == '') {
        $('#SpnMessege').text("Please insert Vehicle Name")
        $('#ModelMessege').modal('show')
        return false;
    }
    if (Description == '') {
        $('#SpnMessege').text("Please insert Vehicle Description")
        $('#ModelMessege').modal('show')
        return false;
    }
    var data = {
        sid: sid,
        Name: Name,
        Description: Description
    }
    $.ajax({
        type: "POST",
        url: "CarHandler.asmx/UpdateCar",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                $('#UpdateDilogBox').modal('hide')
                LoadAll();
                $('#SpnMessege').text("Vehicle Type Updated Successfully.")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wron.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function LoadAll() {
    $.ajax({
        type: "POST",
        url: "CarHandler.asmx/CarsLoadAll",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {


                var CarDetails = obj.CarDetails;

                var ul = '';
                $("#CarDetails").empty();
                // $('#CarDetails').append(ul);
                if (CarDetails) {
                    $("#CarDetails").empty();
                    for (var i = 0; i < CarDetails.length; i++) {
                        var status = CarDetails[i].IsActive;
                        //var ShowStatus;
                        //if (status == true) {
                        //    ShowStatus = "Active"
                        //}
                        //else {
                        //    ShowStatus = "Deactive"
                        //}
                        ul += '<tr class="odd">';
                        ul += '<td align="center" style="width: 10%">' + (i + 1) + '</td>';
                        ul += '<td align="center" style="width: 10%">' + CarDetails[i].UniqueId + '</td>';
                        ul += '<td align="center">' + CarDetails[i].Name + '</td>';
                        //ul += '<td align="center" >' + ShowStatus + '</td>';
                        if (status == true) {
                            //ul += '<td align="center" ><a style="cursor: pointer" onclick="Activate(\'' + CarDetails[i].sid + '\')" href="#"><span class="glyphicon glyphicon-eye-open" title="Edit"></span></a> | <a style="cursor: pointer" onclick="OpenUpdatePopUp(\'' + CarDetails[i].sid + '\',\'' + CarDetails[i].Name + '\',\'' + CarDetails[i].Description + '\')"><span class="glyphicon glyphicon-edit" title="Edit"></span></a>| <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="Delete(' + CarDetails[i].sid + ')"></span></a></td>';
                            ul += '<td align="center" style="width:10%"><span class="glyphicon glyphicon-eye-open" onclick="ActivateVehicleType(' + CarDetails[i].sid + ')" style="cursor:pointer" title="Activate/Deactivate"></span></td>'
                        }
                        else {
                            //ul += '<td align="center" ><a style="cursor: pointer" onclick="Activate(\'' + CarDetails[i].sid + '\')" href="#"><span class="fa fa-eye-slash" title="Edit"></span></a> | <a style="cursor: pointer" onclick="OpenUpdatePopUp(\'' + CarDetails[i].sid + '\',\'' + CarDetails[i].Name + '\',\'' + CarDetails[i].Description + '\')"><span class="glyphicon glyphicon-edit" title="Edit"></span></a>| <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="Delete(' + CarDetails[i].sid + ')"></span></a></td>';
                            ul += '<td align="center" style="width:10%"><span class="glyphicon glyphicon-eye-close" onclick="ActivateVehicleType(' + CarDetails[i].sid + ')" style="cursor:pointer" title="Activate/Deactivate"></span></td>'
                        }
                        ul += '<td align="center" style="width: 10%"><a style="cursor: pointer" onclick="OpenUpdatePopUp(\'' + CarDetails[i].sid + '\',\'' + CarDetails[i].Name + '\',\'' + CarDetails[i].Description + '\')"><span class="glyphicon glyphicon-edit" title="Edit"></span></a></td>';
                        ul += '</tr>';
                    }
                    $('#CarDetails').append(ul);


                }
                else {
                    $("#CarDetails").empty();
                    ul += '<tr>';
                    ul += '<td colspan="5">No Record Found</td>'
                    ul += '</tr>';
                    $('#CarDetails').append(ul);
                }

            }
            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function Delete(sid) {
    var data = {
        sid: sid,
    }
    $.ajax({
        type: "POST",
        url: "CarHandler.asmx/DeleteCar",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                LoadAll();
                $('#SpnMessege').text("Vehical Deleted Successfully.")
                $('#ModelMessege').modal('show')

            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not Deleted")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}

function ActivateVehicleType(sid) {
    debugger
    var data = {
        sid: sid,
    }
    $.ajax({
        type: "POST",
        url: "CarHandler.asmx/ActivateVehicleType",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                LoadAll();
                //$('#SpnMessege').text("Vehical Deactivated.")
                //$('#ModelMessege').modal('show')
            }

            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record not Deactivated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });

}


function AddDilogBox() {
    $('#txt_Name').val('');
    $('#txt_Description').val('');
    $("#AddDilogBox").modal("show")
}
function UpdateDilogBox() {

    $("#UpdateDilogBox").modal("show")
}

