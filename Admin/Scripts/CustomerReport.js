﻿
var From = '';
var To = '';
var Name = '';
var ResNo = '';
var PhoneNo = '';
var CustId = 0;
function GetAllCustomer() {
    $.ajax({
        url: "ReportHandler.asmx/GetAlCustomerName",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Customer = obj.tblCustomer;

            if (obj.retCode == 1) {
                arrService = obj.tblCustomer;
                debugger;
                var OptionMac_Name = null;
                OptionMac_Name += '<option value="0" >Select Customer Name</option>'
                if (obj.retCode == 1) {
                    for (var i = 0; i < Customer.length; i++) {
                        var fullName = Customer[i].FirstName + " " + Customer[i].LastName;
                        OptionMac_Name += '<option data-id="' + Customer[i].sid + '" >' + fullName + '</option>'
                        
                    }
                    
                    $("#sel_Customers").append(OptionMac_Name);
                }
            }
        },
      
    });
}

function Submit()
{
    From = $('#datepicker1').val();
    To = $('#datepicker2').val();
    //Name = $("#sel_Customer").val();
    var val = $('#sel_Customer').val();
    Name = $('#sel_Customers option').filter(function () { return this.value == val; }).data('id');
    ResNo = $('#ResevationNo').val();
    PhoneNo = $('#PhoneNo').val();
    var data = { From: From, To: To, uid: Name, ResNo: ResNo, PhoneNo: PhoneNo }
    $("#Details").dataTable().fnClearTable();
    $("#Details").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "ReportHandler.asmx/CustomerActivity",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                var Arr = obj.Arr
                var ul = '';
                //$("#Details").empty();
                for (var i = 0; i < Arr.length; i++) {

                    ul += '<tr>';
                    ul += '<td align="center" >' + (i + 1) + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationID + '</td>';
                    ul += '<td align="center">' + Arr[i].AssignedTo + '</td>';
                    ul += '<td align="center">' + Arr[i].FirstName + ' ' + Arr[i].LastName + '</td>';
                    ul += '<td align="center">' + Arr[i].ContactNumber + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationDate + '</td>';
                    ul += '<td align="center">' + Arr[i].Service + '</td>';
                    ul += '<td align="center">' + Arr[i].Source + '</td>';
                    ul += '<td align="center">' + Arr[i].Destination + '</td>';
                    //ul += '<td align="center">' + Arr[i].GratuityAmount + '</td>';
                    ul += '<td align="center">' + Arr[i].TotalFare + '</td>';
                    ul += '<td align="center" style="width:25%" ><a style="cursor: pointer" onclick="ReservationPopUp(' + Arr[i].uid + ')" href="#"><i class="fa fa-plus-square" aria-hidden="true" data-toggle="tooltip" Title="Add"></i></a></td>'
                    ul += '</tr>';
                }
                //$("#Details").append(ul);
                $("#Details tbody").append(ul);
                $("#Details").dataTable({
                    "bSort": false
                });
            }
            else {
                $('#SpnMessege').text("No Record Found")
                $('#ModelMessege').modal('show')
            }
        },
       
    });

}

function GetAll() {
    setTimeout(function()
    {
        Submit()
    },1000)
    
    /*$("#Details").dataTable().fnClearTable();
    $("#Details").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "ReportHandler.asmx/DriverActivityLoadAll",
        data: '',
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                var Arr = obj.Arr
                var ul = '';
                //$("#Details").empty();
                for (var i = 0; i < Arr.length; i++) {
                    var GratuityAmount = Arr[i].GratuityAmount;
                    if (GratuityAmount == null)
                    {
                        GratuityAmount = 0;
                    }
                    ul += '<tr>';
                    ul += '<td align="center" >' + (i + 1) + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationID + '</td>';
                    ul += '<td align="center">' + Arr[i].AssignedTo + '</td>';
                    ul += '<td align="center">' + Arr[i].FirstName + ' ' + Arr[i].LastName + '</td>';
                    ul += '<td align="center">' + Arr[i].ContactNumber + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationDate + '</td>';
                    ul += '<td align="center">' + Arr[i].Service + '</td>';
                    ul += '<td align="center">' + Arr[i].Source + '</td>';
                    ul += '<td align="center">' + Arr[i].Destination + '</td>';
                    //ul += '<td align="center">' + GratuityAmount + '</td>';
                    ul += '<td align="center">' + Arr[i].TotalFare + '</td>';
                    ul += '<td align="center" style="width:25%" ><a style="cursor: pointer" onclick="ReservationPopUp('+Arr[i].uid+')" href="#"><i class="fa fa-plus-square" aria-hidden="true" data-toggle="tooltip" Title="Add"></i></a></td>'
                    ul += '</tr>';
                }
                //$("#Details").append(ul);
                
            }
            else {
                $('#SpnMessege').text("No Record Found")
                $('#ModelMessege').modal('show')

            }
            $("#Details tbody").append(ul);
            $("#Details").dataTable({
                "bSort": false
            });
            document.getElementById("Details").removeAttribute("style")
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });*/

}

function ExportToExcel() {
    debugger;
    window.location.href = "ExportToExcelHandler.ashx?datatable=" + 'dtCustomer'
}

function Redirect()
{
    var Type = $("#ResType option:selected").val();
    if (Type == "Airport Reservation")
    {
        window.location.href = "AirPortReservation.aspx?ReservationType=Customer&CustId=" + CustId;
    }
    else if (Type == "PointToPoint Reservation") {
        window.location.href = "PointToPoint.aspx?ReservationType=Customer&CustId=" + CustId;
    }
    else if (Type == "Hourly Reservation") {
        window.location.href = "HourReservation.aspx?ReservationType=Customer&CustId=" + CustId;
    }
    else if(Type=="Two Stop Shuttle Reservation")
        window.location.href = "ShuttleService.aspx?ReservationType=Customer&CustId=" + CustId;
}

function ReservationPopUp(Cust)
{
    CustId = Cust;
    $("#AddReserevation").modal("show");
}

