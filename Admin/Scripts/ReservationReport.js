﻿function Submit() {

    var From = $('#datepicker1').val();
    var To = $('#datepicker2').val();
    var Type = $("#sel_Type option:selected").text();

    //if (From == "") {
    //    alert("Please Select From Date")
    //    return false
    //}
    //if (To == "") {
    //    alert("Please Select To Date")
    //    return false
    //}

    var data = { From: From, To: To, Type: Type }
    
    $.ajax({
        type: "POST",
        url: "ReportHandler.asmx/GetByType",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var ul = '';
            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                $("#Details").dataTable().fnClearTable();
                $("#Details").dataTable().fnDestroy();
                Arr = obj.Arr
                Arr = Arr.reverse();
               
                //$("#Details").empty();
                for (var i = 0; i < Arr.length; i++) {

                    ul += '<tr>';
                    ul += '<td align="center" >' + (i + 1) + '</td>';
                    ul += '<td align="center" style="cursor: pointer;" onclick="BookingDetails(' + i + ')"><a>' + Arr[i].ReservationID + '</a></td>';
                    ul += '<td align="center">' + Arr[i].AssignedTo + '</td>';
                    ul += '<td align="center">' + Arr[i].FirstName + ' ' + Arr[i].LastName + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationDate + '</td>';
                    ul += '<td align="center">' + Arr[i].Service + '</td>';
                    ul += '<td align="center">' + Arr[i].Source + '</td>';
                    ul += '<td align="center">' + Arr[i].Destination + '</td>';
                    ul += '<td align="center">' + Arr[i].TotalFare + '</td>';
                    ul += '</tr>';

                }

                $("#Details tbody").append(ul);
                //$("#Details").dataTable({
                //    "bSort": false
                //});
            }
            else {
                $("#Details tbody").empty();
                ul += '<tr>';
                ul += '<td colspan="9" align="center">No Record Found </td>';
                ul += '</tr>';
                $("#Details tbody").append(ul);
                //$("#Details").dataTable({
                //    "bSort": false
                //});
                //$('#SpnMessege').text("No Record Found")
                //$('#ModelMessege').modal('show')

            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });

}
var Arr;
function GetAll() {
    Submit();
    /*$("#Details").dataTable().fnClearTable();
    $("#Details").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "ReportHandler.asmx/DriverActivityLoadAll",
        data: '{}',
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                Arr = obj.Arr
                Arr = Arr.reverse();
                var ul = '';
                //$("#Details").empty();
                for (var i = 0; i < Arr.length; i++) {

                    ul += '<tr>';
                    ul += '<td align="center" >' + (i + 1) + '</td>';
                    ul += '<td align="center" style="cursor: pointer;" onclick="BookingDetails(' + i + ')"><a>' + Arr[i].ReservationID + '</a></td>';
                    ul += '<td align="center">' + Arr[i].AssignedTo + '</td>';
                    ul += '<td align="center">' + Arr[i].FirstName + ' ' + Arr[i].LastName + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationDate + '</td>';
                    ul += '<td align="center">' + Arr[i].Service + '</td>';
                    ul += '<td align="center">' + Arr[i].Source + '</td>';
                    ul += '<td align="center">' + Arr[i].Destination + '</td>';
                    ul += '<td align="center">' + Arr[i].TotalFare + '</td>';
                    ul += '</tr>';

                }
                $("#Details tbody").append(ul);
                $("#Details").dataTable({
                    "bSort": false
                });
            }
            else {
                $('#SpnMessege').text("No Record Found.")
                $('#ModelMessege').modal('show')

            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });*/

}

function BookingDetails(ObjNo) {
    var Name = Arr[ObjNo].FirstName + " " + Arr[ObjNo].LastName;
    $("#BookingDetails").modal("show");
    $("#BookingNo").text(Arr[ObjNo].ReservationID);
    $("#ResDate").text(Arr[ObjNo].ReservationDate);
    $("#Source").text(Arr[ObjNo].Source);
    $("#Destination").text(Arr[ObjNo].Destination);
    $("#AssignedTo").text(Arr[ObjNo].AssignedTo);
    $("#Name").text(Name);
    $("#Service").text(Arr[ObjNo].Service);
    $("#TotalFare").text("$ " + Arr[ObjNo].TotalFare); Passenger
    $("#Passenger").text(Arr[ObjNo].Persons);
    $("#PhoneNo").text(Arr[ObjNo].ContactNumber);
}

function ExportToExcel() {
    debugger;
    //var fileUpload = $("#Image_Upload2").get(0);
    //var files = fileUpload.files;
    //var test = new FormData();
    //for (var i = 0; i < files.length; i++) {
    //    test.append(files[i].name, files[i]);
    //}
    //$.ajax({
    //    url: "ExportToExcelHandler.ashx",
    //    type: "GET",
    //    contentType: false,
    //    processData: false,
    //    data: '{}',
    //    // dataType: "json",
    //    success: function (result)
    //    {
    //        alert(result);
    //    },
    //    error: function (err) {
    //        alert(err.statusText);
    //    }
    //});
    window.location.href = "ExportToExcelHandler.ashx?datatable=" + 'dtReservation'
}