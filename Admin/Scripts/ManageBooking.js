﻿var m_Resevation = [];
var m_CheckBox;
function ManageBooking() {
    $(".CheckBox").change(function () {
        m_CheckBox = document.getElementsByClassName('CheckBox');
        m_Resevation = [];
        for(var i=0;i<m_CheckBox.length;i++)
        {
            if (m_CheckBox[i].checked)
            {
                m_Resevation.push(m_CheckBox[i].value);
            }
        }
    })
}
function mDelete() {
    if (mValidate())
    {
        var data = {
            BookingSid: m_Resevation,
        }
        $.ajax({
            type: "POST",
            url: "ReservationHandler.asmx/DeleteMultipuleBooking",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {

                var obj = JSON.parse(response.d)
                m_Resevation = [];
               
                if (obj.Retcode == 1) {
                    //$('#DriverDetails').empty();
                    //LoadReservations()
                   
                    $('#SpnMessege').text("Booking Deleted")
                    $('#ModelMessege').modal('show')
                    try{
                        SearchReservation()
                    }
                    catch(ex){
                        GetAll()
                    }
                }
                else if (obj.Retcode == 4) {
                    $('#SpnMessege').text("Already Cancelled Booking.")
                    $('#ModelMessege').modal('show')
                }
                else if (obj.Retcode == -1) {
                    $('#SpnMessege').text("Something went wrong!.")
                    $('#ModelMessege').modal('show')
                }

                else {
                    $('#SpnMessege').text("Something Went Wrong!.")
                    $('#ModelMessege').modal('show')
                }
                SearchReservation()
               
            },
            error: function () {

                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            },
        });
    }
   

}

function mCancel() {

    debugger
    if (mValidate()) {
        var data = { BookingSid: m_Resevation }
        $.ajax({
            type: "POST",
            url: "ReservationHandler.asmx/CancelMultiBooking",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {
                var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                var obj = JSON.parse(response.d)
                m_Resevation = [];
                if (obj.Retcode == 1) {
                    //LoadReservations()
                    SearchReservation()
                    $('#SpnMessege').text("Booking Cancelled, an email has been sent to Customer and Driver.")
                    $('#ModelMessege').modal('show')
                }
                else if (obj.Retcode == 4) {
                    $('#SpnMessege').text("Already Cancelled Booking.")
                    $('#ModelMessege').modal('show')
                }
                else if (obj.Retcode == -1) {
                    $('#SpnMessege').text("Something went wrong!.")
                    $('#ModelMessege').modal('show')
                }

                else {
                    $('#SpnMessege').text("Something Went Wrong!.")
                    $('#ModelMessege').modal('show')
                }
            },
            error: function () {

                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            },
        });
    }

}

function mCompleteBooking() {
    debugger
    if (mValidate()) {
        var data = { BookingSid: m_Resevation }
        $.ajax({
            type: "POST",
            url: "ReservationHandler.asmx/mCompleteBooking",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {

                var obj = JSON.parse(response.d)
                m_Resevation = [];
                if (obj.Retcode == 1) {
                    SearchReservation()
                    $('#SpnMessege').text("Booking Completed")
                    $('#ModelMessege').modal('show')
                }
                else if (obj.Retcode == 2) {
                    $('#SpnMessege').text("Booking is Already Completed")
                    $('#ModelMessege').modal('show')
                }
                else if (obj.Retcode == 3) {
                    $('#SpnMessege').text("Please Assign Driver First")
                    $('#ModelMessege').modal('show')
                }
                else if (obj.Retcode == 4) {
                    $('#SpnMessege').text("Please Pay Amount First")
                    $('#ModelMessege').modal('show')
                }
                else if (obj.Retcode == 5) {
                    $('#SpnMessege').text("Booking Can Not Complete Before Service Provided")
                    $('#ModelMessege').modal('show')
                }
                else {
                    $('#SpnMessege').text("Something Went Wrong!.")
                    $('#ModelMessege').modal('show')
                }

            },
            error: function () {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            },
        });
    }
}
function mValidate() {
    if (m_Resevation.length == 0)
    {
        $('#SpnMessege').text("Please Select CheckBox")
        $('#ModelMessege').modal('show')
        //alert("Please Select CheckBox");
     return false;
    }
    else
        return true;
}

function BookingDetails(ObjNo) {
    var Name = Arr[ObjNo].FirstName + " " + Arr[ObjNo].LastName;
    var ResService = Arr[ObjNo].Service;
    $("#InVoice").val(ObjNo);
    $("#BookingDetails").modal("show");
    $("#BookingNo").text(Arr[ObjNo].ReservationID);
    $("#ResDate").text(Arr[ObjNo].ReservationDate);
    if (Arr[ObjNo].Service == "To Airport" || Arr[ObjNo].Service == "From Airport") {
        $("#Source").text(Arr[ObjNo].PickUpAddress);
        $("#Destination").text(Arr[ObjNo].DropAddress);
    }

    else {
        $("#Source").text(Arr[ObjNo].Source);
        $("#Destination").text(Arr[ObjNo].Destination);

    }
    if (Arr[ObjNo].AssignedTo == null)
        Arr[ObjNo].AssignedTo ="not assign"
    $("#AssignedTo").text(Arr[ObjNo].AssignedTo);
    $("#Name").text(Name);
    $("#Service").text(Arr[ObjNo].Service);
    $("#TotalFare").text("$ " + Arr[ObjNo].TotalFare); Passenger
    $("#Passenger").text(Arr[ObjNo].Persons);
    if (Arr[ObjNo].PhoneNumber != null)
        $("#PhoneNo").text(Arr[ObjNo].PhoneNumber);
    else
    {
        Arr[ObjNo].PhoneNumber = Arr[ObjNo].ContactNumber
        $("#PhoneNo").text(Arr[ObjNo].ContactNumber);
    }
       
    if (Arr[ObjNo].Service == "From Airport") {
        if (Arr[ObjNo].Ret_FlightTime != null)
            $("#ResTime").text(Arr[ObjNo].Ret_FlightTime);
        else 
            $("#ResTime").text(Arr[ObjNo].FlightTime);
    }
    else if (Arr[ObjNo].Service == "To Airport") {
        if (Arr[ObjNo].Ret_Time != null)
            $("#ResTime").text(Arr[ObjNo].Ret_Time);
        else 
            $("#ResTime").text(Arr[ObjNo].Pickup_Time);
    }
    else if (Arr[ObjNo].P2PTime != null)
        $("#ResTime").text(Arr[ObjNo].P2PTime);
    else if (Arr[ObjNo].Service == "From Airport Shuttle") {
        if (Arr[ObjNo].Ret_FlightTime != null)
            $("#ResTime").text(Arr[ObjNo].Ret_FlightTime);
        else 
            $("#ResTime").text(Arr[ObjNo].FlightTime);
    }
    else if (Arr[ObjNo].Service == "To Airport Shuttle") {
        if (Arr[ObjNo].Ret_FlightTime != null)
            $("#ResTime").text(Arr[ObjNo].Ret_Time);
        else
            $("#ResTime").text(Arr[ObjNo].Pickup_Time);
    }
    else
        $("#ResTime").text(Arr[ObjNo].Pickup_Time);

    //if (ResService == "From Airport" || ResService == "From Airport Shuttle") {FlightTime
    //    $("#ResTime").text(Arr[ObjNo].FlightTime);
    //}
    //else if (ResService == "To Airport") {
    //    $("#ResTime").text(Arr[ObjNo].Pickup_Time);
    //}
    //else if (Arr[ObjNo].P2PTime != null) {
    //    $("#ResTime").text(Arr[ObjNo].P2PTime);
    //}
    //else {
    //    $("#ResTime").text(Arr[ObjNo].Pickup_Time);
    //}
}

function PrintInvoice() {
    var ObjNo = $("#InVoice").val();
    var Name = Arr[ObjNo].FirstName + " " + Arr[ObjNo].LastName;
    var ResService = Arr[ObjNo].Service;
    var ResTime = "";
    if (ResService == "From Airport") {
        ResTime = Arr[ObjNo].FlightTime;
    }
    else if (ResService == "To Airport") {
        ResTime = Arr[ObjNo].Pickup_Time;
    }
    //else {
    //    ResTimeArr[ObjNo].P2PTime;
    //}
    if (Arr[ObjNo].AssignedTo == null)
        Arr[ObjNo].AssignedTo = 'Not Assigned'
    var url = "../InvoicePrint.aspx?BookingNo=" + Arr[ObjNo].ReservationID + "&ResDate=" + Arr[ObjNo].ReservationDate + "&Source=" + Arr[ObjNo].Source +
                           "&Destination=" + Arr[ObjNo].Destination + "&AssignedTo=" + Arr[ObjNo].AssignedTo + "&Name=" + Name + "&Service=" + Arr[ObjNo].Service +
                           "&TotalFare=" + "$" + Arr[ObjNo].TotalFare + "&Passenger=" + Arr[ObjNo].Persons + "&PhoneNo=" + Arr[ObjNo].PhoneNumber + "&ResTime=" + ResTime
    window.open(url, 'Print Invoice');

}