﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BWI.Admin.DataLayer;
using BWI.BL;
using System.Web.Script.Serialization;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for ServiceHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ServiceHandler : System.Web.Services.WebService
    {
        DBHandlerDataContext DB = new DBHandlerDataContext();
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";

        [WebMethod(EnableSession = true)]
        public string Load()
        {
            List<BWI.Trans_Tbl_ServiceType> Data = null;

            Data = (from obj in DB.Trans_Tbl_ServiceTypes select obj).ToList();


            if (Data.Count == 0)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
                return json;
            }

            if (Data.Count > 0)
            {
                json = jsSerializer.Serialize(Data);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }


        [WebMethod(EnableSession = true)]
        public string Delete(Int64 sid)
        {
            try
            {

                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_ServiceType SER = DB.Trans_Tbl_ServiceTypes.Single(x => x.sid == sid);
                DB.Trans_Tbl_ServiceTypes.DeleteOnSubmit(SER);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string Update(Int64 sid, string Name, string Description)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_ServiceType SER = DB.Trans_Tbl_ServiceTypes.Single(x => x.sid == sid);

                SER.Name = Name;
                SER.Description = Description;
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string Insert(string Name, string Description)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                List<BWI.Trans_Tbl_ServiceType> Data = (from obj in DB.Trans_Tbl_ServiceTypes select obj).ToList();
                Trans_Tbl_ServiceType SER = new Trans_Tbl_ServiceType();
                SER.Name = Name;
                SER.Description = Description;
                DB.Trans_Tbl_ServiceTypes.InsertOnSubmit(SER);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }



        [WebMethod(EnableSession = true)]
        public string GetSingle(Int64 sid)
        {
            List<BWI.Trans_Tbl_ServiceType> Data = null;
            Data = (from obj in DB.Trans_Tbl_ServiceTypes where obj.sid == sid select obj).ToList();


            if (Data.Count > 0)
            {
                json = jsSerializer.Serialize(Data);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            }

            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }


        [WebMethod(EnableSession = true)]
        public string Active(Int64 sid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_ServiceType SER = DB.Trans_Tbl_ServiceTypes.Single(x => x.sid == sid);

                if (SER.IsActive == true)
                {
                    SER.IsActive = false;
                }
                else
                {
                    SER.IsActive = true;
                }

                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }
    }
}
