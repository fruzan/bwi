﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BWI.Admin.DataLayer;
using BWI.BL;
using System.Web.Script.Serialization;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for AirPortHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AirPortHandler : System.Web.Services.WebService
    {
        DBHandlerDataContext DB = new DBHandlerDataContext();
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";

        //[WebMethod(EnableSession = true)]
        //public string Load()
        //{

        //    try
        //    {
        //        List<CUT.Trans_Tbl_AirPort> Data = null;
        //        Data = (from obj in DB.Trans_Tbl_AirPorts select obj).ToList();

        //        if (Data.Count == 0)
        //        {
        //            json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
        //            return json;
        //        }

        //        if (Data.Count > 0)
        //        {
        //            json = jsSerializer.Serialize(Data);
        //            json = json.TrimEnd(']');
        //            json = json.TrimStart('[');
        //            json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
        //        }
        //        else
        //        {
        //            json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //        }
        //    }

        //    catch
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return json;
        //}


        [WebMethod(EnableSession = true)]
        public string Load()
        {
            List<BWI.Trans_Tbl_AirPort> Data = null;
            try
            {

                Data = (from obj in DB.Trans_Tbl_AirPorts select obj).ToList();

                if (Data.Count == 0)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
                    return json;
                }


                if (Data.Count > 0)
                {
                    json = jsSerializer.Serialize(Data);
                    json = json.TrimEnd(']');
                    json = json.TrimStart('[');
                    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }

            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }



            return json;
        }


        [WebMethod(EnableSession = true)]
        public string Delete(Int64 sid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_AirPort SER = DB.Trans_Tbl_AirPorts.Single(x => x.sid == sid);
                DB.Trans_Tbl_AirPorts.DeleteOnSubmit(SER);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetSingle(Int64 sid)
        {
            List<BWI.Trans_Tbl_AirPort> Data = null;
            Data = (from obj in DB.Trans_Tbl_AirPorts where obj.sid == sid select obj).ToList();


            if (Data.Count > 0)
            {
                json = jsSerializer.Serialize(Data);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            }

            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string ActiveAirport(string Sid)
        {
            try
            {
                //var List = (from Obj in DB.Trans_Tbl_Locations where Obj.sid == Convert.ToInt16(Sid) select Obj).ToList();

                Trans_Tbl_Location Location = DB.Trans_Tbl_Locations.Single(x => x.sid == Convert.ToInt16(Sid));

                if (Location.Status == "Activate")
                {
                    Location.Status = "Deactivate";
                }
                else
                {
                    Location.Status = "Activate";
                }

                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        //[WebMethod(EnableSession = true)]
        //public string Update(Int64 sid, string Name, string Description)
        //{
        //    try
        //    {
        //        DBHandlerDataContext DB = new DBHandlerDataContext();
        //        Trans_Tbl_AirPort SER = DB.Trans_Tbl_AirPorts.Single(x => x.sid == sid);

        //        SER.Name = Name;
        //        SER.Description = Description;
        //        DB.SubmitChanges();
        //        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //    }
        //    catch
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
        //    }
        //    return json;
        //}


        //[WebMethod(EnableSession = true)]
        //public string Insert(string Name, string Description)
        //{
        //    try
        //    {
        //        DBHandlerDataContext DB = new DBHandlerDataContext();
        //        List<CUT.Trans_Tbl_AirPort> Data = (from obj in DB.Trans_Tbl_AirPorts select obj).ToList();
        //        Trans_Tbl_AirPort SER = new Trans_Tbl_AirPort();
        //        SER.Name = Name;

        //        DB.Trans_Tbl_AirPorts.InsertOnSubmit(SER);
        //        DB.SubmitChanges();
        //        json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
        //    }
        //    catch
        //    {
        //        json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
        //    }
        //    return json;
        //}

    }
}
