﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Net;
using System.Text;
using System.Data;
using System.Xml;
using BWI.DataLayer;
using BWI.BL;
using System.Web.Script.Serialization;
using BWI.Admin.DataLayer;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for LocationHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class LocationHandler : System.Web.Services.WebService
    {

        string json = "";
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        [WebMethod(EnableSession = true)]
        public string AddLocation(string address, string Latitude, string Longitude)
        {

            retCode = LocationManager.InsertLocation(address, Latitude, Longitude);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            return json;
        }


        [WebMethod(EnableSession = true)]
        public string UpdateLocation(int sid, string address, string Latitude, string Longitude)
        {

            retCode = LocationManager.UpdateLocation(sid, address, Latitude, Longitude);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            return json;
        }


        [WebMethod(EnableSession = true)]
        public string LocationLoadAll()
        {
            List<BWI.Proc_Trans_LocationLoadAllResult> CarsData = null;
            retCode = LocationManager.LocationLoadAll(out CarsData);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(CarsData);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"CarDetails\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }


        [WebMethod(EnableSession = true)]
        public string LoadByKey(int sid)
        {
            List<BWI.Proc_Trans_GetLocationByKeyResult> CarsData = null;
            retCode = LocationManager.LoadByKey(sid, out CarsData);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(CarsData);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"CarDetails\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteLocation(int sid)
        {

            retCode = LocationManager.DeleteLocation(sid);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            return json;
        }


        [WebMethod(EnableSession = true)]
        public string ActiveLocation(Int64 Sid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_Location Location = DB.Trans_Tbl_Locations.Single(x => x.sid == Sid);
                string Status;
                if (Location.Status == "Activate")
                {
                    Status = "Deactivate";
                }
                else
                {
                    Status = "Activate";
                }
                retCode = LocationManager.ActiveLocation(Sid, Status);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
                }
                //DB.SubmitChanges();
                //json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }
    }
}
