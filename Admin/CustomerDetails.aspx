﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="CustomerDetails.aspx.cs" Inherits="BWI.Admin.CustomerDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="Scripts/Customer.js"></script>
    <script  type="text/javascript"  src="Scripts/ManageBooking.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <br /> <br /> <br />
    <table align="right" style="margin-right: 1.8%">
        <tbody>
            <tr>
                <td colspan="6"></td>
                <td style="padding-top: 3.2%">
                    <a href="#">
                        <input type="button" class="popularbtn right" onclick="OpenPopUp()" style="margin-right: 3.8%" value="+ Add New" title="Add New Customer" /></a>
                </td>
               <%-- <td style="padding-top: 3.2%">
                    <input type="button" class="grid2btn active" style="cursor: pointer" title="Export To Excel" id="btnExport" onclick="ExportAgentDetailsToExcel()" />
                </td>--%>
            </tr>
        </tbody>
    </table>
    <br />
    <br />
    <br />
    <div class="offset-2">
        <div class="fblueline">
            Manage
           
            <span class="farrow"></span>
            <a style="color: white" href="CustomerDetails.aspx" title="Customer Details"><b>Customer Details</b></a>

        </div>
        <div class="frow1">

            <br>
            <br>

            <div class="table-responsive">


                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered dataTable" id="tbl_StaffDetails" style="width: 100%;">
                    <thead>
                        <tr>
                            <td align="center"><b>S.N</b>
                            </td>
                            <td align="center"><b>Name</b>
                            </td>
                            <td align="center"><b>Email</b>
                            </td>
                            <td align="center"><b>Mobile</b>
                            </td>
                            <td align="center"><b>Address</b>
                            </td>
                            <td align="center"><b>Edit | Delete</b>
                            </td>
                        </tr>
                    </thead>

                    <tbody id="CustomerDetails">
                  
                    </tbody>
                </table>


            </div>
            <br>
        </div>
    </div>
    <div class="modal fade" id="AddCustomer" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Add Customer</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>
                                                First Name *</label>
                                            <input id="Fname" type="text" class=" form-control" />

                                        </td>
                                        <td>
                                            <label>
                                                Last Name *</label>
                                            <input id="Lanme" type="text" class=" form-control" />

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Gender *</label><br />
                                            <select id="sel_Gender" class=" form-control">
                                                <option selected="selected" value="Male">Male
                                                </option>
                                                <option value="FeMale">Female
                                                </option>
                                            </select>

                                        </td>
                                        <td>
                                            <label>Mobile No. *</label>
                                            <input id="Mobile" type="text" class=" form-control" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <label>Address *</label>
                                            <input id="Address" type="text" class=" form-control" />
                                        </td>
                                    </tr>
<%--                                    <tr>
                                        <td>
                                            <label>Country *</label>
                                            <select id="Select_Country" class="form-control">
                                                <option value="-" selected="selected">Select Any Country</option>
                                            </select>
                                        </td>
                                        <td>
                                            <label>City *</label>
                                            <select id="Select_City" class="form-control">
                                                <option selected="selected" value="-">Select Any City</option>
                                            </select>
                                        </td>
                                    </tr>--%>
                                    <tr>
<%--                                        <td>
                                            <label>Pin Code</label>
                                            <input id="Pin" type="text" class=" form-control" />
                                        </td>--%>
                                        <td >
                                            <label>Email *</label>
                                            <input id="Email" type="text" class=" form-control" />
                                        </td>
                                         <td>
                                            <label>Password *</label>
                                            <input id="Password" type="text" class=" form-control" />
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                            <table class="table table">
                                <tr>
                                    <td>
                                        <input type="button" onclick="AddCustomer()" class="btn-search" value="ADD" title="Add Driver" id="btn_Supplier" /></td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="Update" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Add Customer</b></div>
                        <div class="frow2">
                            <table  cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>
                                                First Name *</label>
                                            <input id="AFname" type="text" class=" form-control" />

                                        </td>
                                        <td>
                                            <label>
                                                Last Name *</label>
                                            <input id="ALanme" type="text" class=" form-control" />

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Gender *</label><br />
                                            <select id="Asel_Gender" class=" form-control">
                                                <option selected="selected" value="Male">Male
                                                </option>
                                                <option value="FeMale">Female
                                                </option>
                                            </select>

                                           

                                        </td>
                                        <td>
                                            <label>Mobile No. *</label>
                                            <input id="AMobile" type="text" class=" form-control" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <label>Address *</label>
                                            <input id="AAddress" type="text" class=" form-control" />
                                        </td>
                                    </tr>
<%--                                    <tr>
                                        <td>
                                            <label>Country *</label>
                                            <select id="ASelect_Country" class="form-control">
                                                <option value="-" selected="selected">Select Any Country</option>
                                            </select>
                                        </td>
                                        <td>
                                            <label>City *</label>
                                            <select id="ASelect_City" class="form-control">
                                                <option selected="selected" value="-">Select Any City</option>
                                            </select>
                                        </td>
                                    </tr>--%>
                                    <tr>
<%--                                        <td>
                                            <label>Pin Code</label>
                                            <input id="APin" type="text" class=" form-control" />
                                        </td>--%>
                                        <td>
                                            <label>Email *</label>
                                            <input id="AEmail" type="text" class=" form-control" />
                                        </td>
                                         
                                         <td>
                                            <label>Password *</label>
                                            <input id="APassword" type="text" class=" form-control" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table">
                                <tr>
                                    <td>
                                        <input type="button" onclick="UpdateCustomer()" class="btn-search" value="Update" title="Add Driver" /></td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <input type="hidden" id="hdn"/>
</asp:Content>
