﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BWI.Admin.DataLayer;
using BWI.BL;
using System.Web.Script.Serialization;
using BWI.DataLayer;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Globalization;
using System.Text.RegularExpressions;
namespace BWI.Admin
{
    /// <summary>
    /// Summary description for ReservationHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ReservationHandler : System.Web.Services.WebService
    {
        string json = "";
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        #region AirPort Reservation

        [WebMethod(EnableSession = true)]
        public string AirPortReservation(string[] Input, string[] DropDowns, string[] CheckBoxes, string Remarks, string TravelType, string TotalDistance, string TimeTaken, string source, string destination, string AirportName, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string Airlines, string DriverID, string Email, string ContactNo, decimal GratuityAmount)
        {
            string json = "";
            DBHelper.DBReturnCode retcode;
            retcode = ReservationManager.AirPortReservation(Input, DropDowns, CheckBoxes, Remarks, TravelType, TotalDistance, TimeTaken, source, destination, AirportName, MeetGreat, SpecialAssistant, CurbSidePickUp, BaggageClaimPickup, PetInCage, Status, DriverName, Airlines, DriverID, Email, ContactNo, GratuityAmount, out json);

            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = "{\"Retcode\":\"1\"}";
            }
            else if (!json.Contains("Error"))
            {
                json = "{\"Retcode\":\"2\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateAirPortReservation(int sid, string[] Input, string[] DropDowns, string[] CheckBoxes, string Remarks, string TravelType, string TotalDistance, string TimeTaken, string source, string destination, string AirportName, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string Airlines, string DriverID, string Email, string ContactNo, decimal GratuityAmount)
        {

            DBHelper.DBReturnCode retcode;
            retcode = ReservationManager.UpdateAirPortReservation(sid, Input, DropDowns, CheckBoxes, Remarks, TravelType, TotalDistance, TimeTaken, source, destination, AirportName, MeetGreat, SpecialAssistant, CurbSidePickUp, BaggageClaimPickup, PetInCage, Status, DriverName, Airlines, DriverID, Email, ContactNo, GratuityAmount);

            string json = "";
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = "{\"Retcode\":\"1\"}";
            }
            else
            {
                json = "{\"Retcode\":\"2\"}";

            }

            return json;
        }

        #endregion

        #region Point To Point Reservation

        [WebMethod(EnableSession = true)]
        public string PointToPointReservation(string[] Input, string[] DropDowns, string[] StopLocations, string[] StopMisc, string[] StopHours, string[] StopMins, string[] CheckBoxes, string Remarks, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string TotalDistance, string TimeTaken, string Email, string ContactNo, decimal GratuityAmount, string DriverID)
        {
            DBHelper.DBReturnCode retcode;
            retcode = ReservationManager.PointToPointReservation(Input, DropDowns, StopLocations, StopMisc, StopHours, StopMins, CheckBoxes, Remarks, MeetGreat, SpecialAssistant, CurbSidePickUp, BaggageClaimPickup, PetInCage, Status, DriverName, TotalDistance, TimeTaken, Email, ContactNo, GratuityAmount, DriverID);

            string json = "";
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = "{\"Retcode\":\"1\"}";
            }
            else
            {
                json = "{\"Retcode\":\"2\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdatePointToPointReservation(int sid, string[] Input, string[] DropDowns, string[] StopLocations, string[] StopMisc, string[] StopHours, string[] StopMins, string[] CheckBoxes, string Remarks, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string TotalDistance, string TimeTaken, string Email, string ContactNo, decimal GratuityAmount, string DriverID)
        {
            DBHelper.DBReturnCode retcode;
            retcode = ReservationManager.UpdatePointToPointReservation(sid, Input, DropDowns, StopLocations, StopMisc, StopHours, StopMins, CheckBoxes, Remarks, MeetGreat, SpecialAssistant, CurbSidePickUp, BaggageClaimPickup, PetInCage, Status, DriverName, TotalDistance, TimeTaken, Email, ContactNo, GratuityAmount, DriverID);

            string json = "";
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = "{\"Retcode\":\"1\"}";
            }
            else
            {
                json = "{\"Retcode\":\"2\"}";

            }

            return json;
        }

        #endregion

        #region Hour Reservation

        [WebMethod(EnableSession = true)]
        public string HourReservation(string[] Input, string[] DropDowns, string[] CheckBoxes, string Remarks, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string Email, string ContactNo, decimal GratuityAmount, string DriverID)
        {

            DBHelper.DBReturnCode retcode;
            retcode = ReservationManager.HourReservation(Input, DropDowns, CheckBoxes, Remarks, MeetGreat, SpecialAssistant, CurbSidePickUp, BaggageClaimPickup, PetInCage, Status, DriverName, Email, ContactNo, GratuityAmount, DriverID);

            string json = "";
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = "{\"Retcode\":\"1\"}";
            }
            else
            {
                json = "{\"Retcode\":\"2\"}";

            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateHourReservation(int sid, string[] Input, string[] DropDowns, string[] CheckBoxes, string Remarks, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string Email, string ContactNo, decimal GratuityAmount, string DriverID)
        {

            DBHelper.DBReturnCode retcode;
            retcode = ReservationManager.UpdateHourReservation(sid, Input, DropDowns, CheckBoxes, Remarks, MeetGreat, SpecialAssistant, CurbSidePickUp, BaggageClaimPickup, PetInCage, Status, DriverName, Email, ContactNo, GratuityAmount, DriverID);

            string json = "";
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = "{\"Retcode\":\"1\"}";
            }
            else
            {
                json = "{\"Retcode\":\"2\"}";

            }

            return json;
        }

        #endregion

        #region Shuttle Reservation

        [WebMethod(EnableSession = true)]
        public string ShuttleReservation(string[] Input, string[] DropDowns, string[] CheckBoxes, string Remarks, string TravelType, string TotalDistance, string TimeTaken, string source, string destination, string AirportName, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string Airlines, string DriverID, string Email, string ContactNo, decimal GratuityAmount, decimal OfferCode, string Adults, string Childs)
        {
            string json = "";
            DBHelper.DBReturnCode retcode;
            retcode = ReservationManager.ShuttleReservation(Input, DropDowns, CheckBoxes, Remarks, TravelType, TotalDistance, TimeTaken, source, destination, AirportName, MeetGreat, SpecialAssistant, CurbSidePickUp, BaggageClaimPickup, PetInCage, Status, DriverName, Airlines, DriverID, Email, ContactNo, GratuityAmount, OfferCode, Adults, Childs, out json);

            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = "{\"Retcode\":\"1\"}";
            }
            else if (!json.Contains("Error"))
            {
                json = "{\"Retcode\":\"2\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateShuttleReservation(int sid, string[] Input, string[] DropDowns, string[] CheckBoxes, string Remarks, string TravelType, string TotalDistance, string TimeTaken, string source, string destination, string AirportName, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string Airlines, string DriverID, string Email, string ContactNo, decimal GratuityAmount, decimal OfferCode, string Adults, string Childs)
        {

            DBHelper.DBReturnCode retcode;
            retcode = ReservationManager.UpdateShuttleReservation(sid, Input, DropDowns, CheckBoxes, Remarks, TravelType, TotalDistance, TimeTaken, source, destination, AirportName, MeetGreat, SpecialAssistant, CurbSidePickUp, BaggageClaimPickup, PetInCage, Status, DriverName, Airlines, DriverID, Email, ContactNo, GratuityAmount, OfferCode, Adults, Childs);

            string json = "";
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = "{\"Retcode\":\"1\"}";
            }
            else
            {
                json = "{\"Retcode\":\"2\"}";

            }
            return json;
        }

        #endregion

        #region Reservation
        [WebMethod(EnableSession = true)]
        public string LoadReservations()
        {
            List<BWI.Trans_Reservation> ResvationList;
            string json = "";
            string jsonDriver = "";
            JavaScriptSerializer JS = new JavaScriptSerializer();
            DBHelper.DBReturnCode retcode;
            retcode = ReservationManager.LoadReservations(out ResvationList);
            List<BWI.Trans_Tbl_Login> List = null;
            retCode = ReservationManager.GetAllDriver(out List);

            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {

                json = JS.Serialize(ResvationList);
                json = json.TrimStart('[');
                json = json.TrimEnd(']');

                jsonDriver = JS.Serialize(List);
                jsonDriver = jsonDriver.TrimStart('[');
                jsonDriver = jsonDriver.TrimEnd(']');

                json = "{\"Retcode\":\"1\",\"Arr\":[" + json + "],\"ArrDriver\":[" + jsonDriver + "]}";

            }
            else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Retcode\":\"-1\"}";
            }

            else
            {
                json = "{\"Retcode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string SearchReservation(string From, string To, string Status)
        {
            #region My
            string jsonDriver = "";

            //DBHelper.DBReturnCode retcode;
            DBHandlerDataContext DB = new DBHandlerDataContext();
            DateTime dFrom;
            DateTime dTo;
            GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
            List<BWI.Trans_Tbl_Login> DriverList = null;
            retCode = ReservationManager.GetAllDriver(out DriverList);
            //var email = Global.sEmail;
            var List = (from Obj in DB.Trans_Reservations select Obj).ToList();

            List<Trans_Reservation> NewList = new List<Trans_Reservation>();
            bool Statas = true;
            if (Status == "Paid")
            {
                Statas = true;
            }
            else if (Status == "Unpaid")
            {
                Statas = false;
            }
            if (From != "" && To != "" && Status == "0")
            {
                dFrom = GetAppDate(From);
                dTo = GetAppDate(To);
                NewList = List.Where(s => GetAppDate(s.ReservationDate) >= dFrom && GetAppDate(s.ReservationDate) <= dTo).ToList();
            }
            else if (From == "" && To == "" && Status != "0")
            {
                if (Status == "All")
                {
                    NewList = (from Obj in DB.Trans_Reservations select Obj).ToList();
                }
                else if (Status == "Paid" || Status == "Unpaid")
                {
                    NewList = List.Where(s => s.Paid == Statas).ToList();
                }
                else if (Status == "Unassigned")
                {
                    NewList = List.Where(s => s.AssignedTo == null).ToList();
                }
                else if (Status == "Cancelled" || Status == "Confirmed" || Status == "Completed" || Status == "Deleted" || Status == "Requested" || Status == "Requested Online")
                {
                    NewList = List.Where(s => s.Status == Status).ToList();
                }

            }
            else if (From != "" && To != "" && Status != "0")
            {
                dFrom = GetAppDate(From);
                dTo = GetAppDate(To);
                if (Status == "All")
                {
                    NewList = List.Where(s => GetAppDate(s.ReservationDate) >= dFrom && GetAppDate(s.ReservationDate) <= dTo).ToList();
                }
                else if (Status == "Paid" || Status == "Unpaid")
                {
                    NewList = List.Where(s => GetAppDate(s.ReservationDate) >= dFrom && GetAppDate(s.ReservationDate) <= dTo && s.Paid == Statas).ToList();
                }
                else if (Status == "Unassigned")
                {
                    NewList = List.Where(s => GetAppDate(s.ReservationDate) >= dFrom && GetAppDate(s.ReservationDate) <= dTo && s.AssignedTo == null).ToList();
                }
                else if (Status == "Cancelled" || Status == "Confirmed" || Status == "Deleted" || Status == "Requested")
                {
                    NewList = List.Where(s => GetAppDate(s.ReservationDate) >= dFrom && GetAppDate(s.ReservationDate) <= dTo && s.Status == Status).ToList();
                }
                //NewList = List.Where(s => ConvertDateTime(s.ReservationDate) >= dFrom && ConvertDateTime(s.ReservationDate) <= dTo && s.Status == Status).ToList();
            }
            //var FirstName = Global.sFirstName;
            //var LastName = Global.sLastName;

            //var Name = FirstName+" " + LastName;
            //var Sid = Global.Sid;
            //var List = (from Obj in DB.Trans_Reservations where Obj.FirstName == FirstName && Obj.LastName == LastName select Obj).ToList();

            if (NewList.Count > 0)
            {
                jsonDriver = jsSerializer.Serialize(DriverList);
                jsonDriver = jsonDriver.TrimStart('[');
                jsonDriver = jsonDriver.TrimEnd(']');

                json = jsSerializer.Serialize(NewList);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');

                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "],\"ArrDriver\":[" + jsonDriver + "]}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            #endregion
            return json;
        }
        #endregion

        #region Get All
        [WebMethod(EnableSession = true)]
        public string GetAll()
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();
            JavaScriptSerializer objSerialize = new JavaScriptSerializer();
            try
            {
                List<BWI.Trans_Tbl_ServiceType> objServiceType = null;
                var query = from objLocation in DB.Trans_Tbl_Locations where objLocation.Status == "Activate" orderby objLocation.LocationName select objLocation;
                List<BWI.Trans_Tbl_Location> LocTable = query.ToList();
                string jsonlocation = jsSerializer.Serialize(LocTable);

                retCode = RequestBooking.GetAllDriver(out objServiceType);
                // string jsonlocation = jsSerializer.Serialize(LocTable);

                // Driver List
                List<BWI.Trans_Tbl_Login> DriverList = null;
                ReservationManager.GetAllDriver(out DriverList);

                // Airport List
                List<BWI.Trans_Tbl_Login> List = null;
                var AirPortList = (from Obj in DB.Trans_Tbl_Locations select Obj).ToList();

                // Vehicle list
                // var VehicleList = (from obj in DB.Trans_Tbl_CarTypes select obj).ToList();
                List<BWI.Trans_tbl_Vehicle_Info> VehicleList = null;
                retCode = DistanceEstimateManager.ModelNameDropDown(out VehicleList);

                //Airlines List
                var AirlinesList = (from obj in DB.Trans_Tbl_AirLines where obj.Status == "Activate" orderby obj.Callsign select obj).ToList();



                // Email list 
                List<BWI.Trans_Tbl_CustomerMaster> EmailList = null;
                EmailList = (from obj in DB.Trans_Tbl_CustomerMasters select obj).ToList();
                return objSerialize.Serialize(new { retCode = 1, ServiceType = objServiceType, Location = LocTable, tblDriver = DriverList, AirPortList = AirPortList, VehicleList = VehicleList, EmailList = EmailList, AirlinesList = AirlinesList });
            }
            catch (Exception ex)
            {
                return objSerialize.Serialize(new { retCode = 0, Session = 1, ErrorMsg = ex.Message });
            }


        }
        #endregion

        #region Booking

        [WebMethod(EnableSession = true)]
        public string AssignBooking(Int64 BookingSid, Int64 DriverSid, string DriverName)
        {
            string msg = "";
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Reservation Reservation = DB.Trans_Reservations.Single(x => x.sid == BookingSid);

                if (Reservation.Status != "Completed")
                {
                    if (DriverName == "Select Driver")
                    {
                        Reservation.AssignedTo = "not assign";
                        Reservation.DriverSid = null;
                        Reservation.Status = "Requested";
                    }
                    else
                    {
                        Reservation.AssignedTo = DriverName;
                        Reservation.DriverSid = DriverSid;
                        Reservation.Status = "Confirmed";
                    }
                }

                if (Reservation.Status == "Cancelled")
                {
                    json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                    return json;
                }
                DB.SubmitChanges();

                Trans_Tbl_Login Driver = new Trans_Tbl_Login();
                if (DriverName != "Select Driver")
                {
                    Driver = DB.Trans_Tbl_Logins.Single(x => x.Sid == DriverSid);
                }

                Trans_Reservation Booking = DB.Trans_Reservations.Single(x => x.sid == BookingSid);
                Trans_tbl_Vehicle_Info VehicleInfo = new Trans_tbl_Vehicle_Info();
                //if (Booking.Service == "Hourly Reservation")
                //{
                //    Trans_Tbl_HourlyRate HourlyRate = new Trans_Tbl_HourlyRate();
                //    HourlyRate = DB.Trans_Tbl_HourlyRates.Single(x => x.sid == Convert.ToInt64(Booking.VehicalType));
                //    VehicleInfo = DB.Trans_tbl_Vehicle_Infos.Single(x => x.sid == Convert.ToInt64(HourlyRate.CarType_Sid));
                //}
                //else
                //{
                //    VehicleInfo = DB.Trans_tbl_Vehicle_Infos.Single(x => x.sid == Convert.ToInt64(Booking.VehicalType));
                //}
                VehicleInfo = DB.Trans_tbl_Vehicle_Infos.Single(x => x.sid == Convert.ToInt64(Booking.VehicalType));
                StringBuilder sb = new StringBuilder();
                StringBuilder sbDriver = new StringBuilder();
                #region Customer Mail
                sb.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
                sb.Append("<title>www.bwishuttleservice.com- Customer Receipt</title>  ");
                sb.Append("<style>                                            ");
                sb.Append(".Norm {font-family: Verdana;                       ");
                sb.Append("font-size: 12px;                                 ");
                sb.Append("font-color: red;                               ");
                sb.Append(" }                                               ");
                sb.Append(".heading {font-family: Verdana;                   ");
                sb.Append("  font-size: 14px;                            ");
                sb.Append("  font-weight: 800;                           ");
                sb.Append("	 }                                                  ");
                sb.Append("   td {font-family: Verdana;                         ");
                sb.Append("	  font-size: 12px;                                  ");
                sb.Append("	 }                                                  ");
                sb.Append("  </style>                                           ");
                sb.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
                sb.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
                sb.Append("");
                sb.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
                sb.Append("  <tbody><tr>");
                sb.Append("    <td width='660' colspan='13' valign='top'>");
                sb.Append("      <table width='100%'>");
                sb.Append("        <tbody><tr>");
                //sb.Append("          <td colspan='2' width='100%' class='heading'><b>Bwi Shuttle Service</b></td>");
                sb.Append("        </tr>");
                //sb.Append("        <tr>");
                //sb.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com/'>http://www.bwishuttleservice.com</a></td>");
                //sb.Append("        </tr>");
                sb.Append("        <tr>");
                sb.Append("          <td colspan='3' width='100%'><a href='http://www.bwishuttleservice.com'>http://www.bwishuttleservice.com</a></td>");
                sb.Append("        </tr>");
                sb.Append("        <tr>");
                sb.Append("          <td colspan='3' width='100%'><a href='mailto:bwiairportshuttleservice@gmail.com'>bwiairportshuttleservice@gmail.com</a></td>");
                sb.Append("        </tr>");
                sb.Append("        <tr>");
                sb.Append("          <td colspan='2' width='25%'>Call Us: 1-844-904-5151, 410-904-5151</td>");
                sb.Append("          <td width='75%' align='right'><font size='3'>");
                sb.Append("		Receipt");
                sb.Append("	     </font></td>");
                sb.Append("        </tr>");
                sb.Append("      </tbody></table>");
                sb.Append("     </td>");
                sb.Append("  </tr><tr>");
                sb.Append("  </tr><tr>");
                sb.Append("    <td colspan='13' width='660'>");
                sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                sb.Append("  	<tbody><tr>");
                sb.Append("  	  <td width='70%' style='border-bottom:medium solid blue; font-size:16px'><b>Reservation # " + Booking.ReservationID + "</b></td>");
                DateTime Sdate = GetAppDate(Booking.Date.Split(' ')[0]);
                string SerDate = Sdate.ToString("MM-dd-yyyy");
                sb.Append("  	  <td width='30%' align='right' style='border-bottom:medium solid blue;'>Booking Date: " + SerDate + "</td>");
                sb.Append("  	</tr>");
                sb.Append("  	<tr>");
                sb.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br>Thank you for traveling with BWI Shuttle Service!  Below please find your confirmation. If any of the information appears to be incorrect, please contact our office immediately to correct it.<br>&nbsp;</td>");
                sb.Append("	</tr>");
                sb.Append("      </tbody></table>");
                sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                sb.Append("	<tbody><tr>");

                sb.Append("	  <td class='heading'>Pick-up Date:</td>");
                sb.Append("	  <td width='75%'>" + Booking.ReservationDate + "</td>");
                //sb.Append("	  <td width='75%'>" + EmailManager.GetDate(Booking.ReservationDate) + "</td>");
                sb.Append("	</tr>                                             ");

                if (DriverName != "Select Driver")
                {
                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Driver Name:</td>          ");
                    sb.Append("	  <td width='75%'>" + Driver.sFirstName + " " + Driver.sLastName + "</td>                      ");
                    sb.Append("	</tr>                                             ");

                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Driver Mobile:</td>          ");
                    sb.Append("	  <td width='75%'>" + Driver.sMobile + "</td>                      ");
                    sb.Append("	</tr>                                             ");

                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Driver Email:</td>          ");
                    sb.Append("	  <td width='75%'>" + Driver.sEmail + "</td>                      ");
                    sb.Append("	</tr>                                             ");
                }

                sb.Append("	<tr>                                              ");
                sb.Append("	  <td class='heading'>Pickup Time:</td>          ");
                if (Booking.Service == "Point To Point Reservation" || Booking.Service == "Hourly Reservation")
                {
                    if (Booking.P2PTime == null && Booking.Service == "Point To Point Reservation")
                    {
                        sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.Pickup_Time) + "</td>");
                    }
                    else
                        sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.P2PTime) + "</td>");
                }

                else if (Booking.Service == "To Airport" || Booking.Service == "To Airport Shuttle")
                    sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.Pickup_Time) + "</td>");
                else if (Booking.Service == "From Airport" || Booking.Service == "From Airport Shuttle")
                    sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.FlightTime) + "</td>");
                else
                {
                    if (Booking.Ret_FlightTime != null)
                        sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.Ret_FlightTime) + "</td>");
                    else
                        sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.FlightTime) + "</td>");
                }

                sb.Append("	</tr>                                             ");
                sb.Append("	<tr>                                              ");
                sb.Append("	  <td class='heading' valign='Top' style='border-bottom:thin solid green;'>Vehicle Type:</td>           ");
                sb.Append("	  <td width='75%' style='border-bottom:thin solid green;'>                                              ");
                sb.Append("	  " + VehicleInfo.Model + "<br>&nbsp;</td>                                                                                    ");
                sb.Append("	</tr>      ");


                sb.Append("	<tr>                                              ");
                sb.Append("	  <td class='heading'>Primary Contact:</td>       ");
                sb.Append("	  <td width='75%'>" + Booking.FirstName + " " + Booking.LastName + "</td>           ");
                sb.Append("	</tr>                                             ");
                sb.Append("	<tr>                                              ");
                sb.Append("	  <td class='heading'>Contact Number:</td>        ");
                if (Booking.ContactNumber == "" || Booking.ContactNumber == null)
                {
                    sb.Append("	  <td width='75%'>" + Booking.PhoneNumber + "</td>               ");
                }
                else
                {
                    sb.Append("	  <td width='75%'>" + Booking.ContactNumber + "</td>               ");
                }
                sb.Append("	</tr>                                             ");
                sb.Append("	<tr>                                              ");
                sb.Append("	  <td class='heading'>No. of Passenger:</td>         ");
                sb.Append("	  <td width='75%'>" + Booking.Persons + "</td>                          ");
                sb.Append("	</tr>                                             ");
                //sb.Append("	<tr>                                              ");
                //sb.Append("	  <td class='heading'>No. of Children:</td>       ");
                //sb.Append("	  <td width='75%'>" + Booking.Child + "</td>                          ");
                //sb.Append("	</tr>	                                          ");
                if (Booking.Service == "Hourly Reservation")
                {
                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Hours:</td>         ");
                    sb.Append("	  <td width='75%'>" + Booking.Hours + "</td>                          ");
                    sb.Append("	</tr>");
                }
                //sb.Append("	<tr>                                              ");
                //sb.Append("	  <td class='heading' valign='Top' style='border-bottom:thin solid green;'>Vehicle Type:</td>           ");
                //sb.Append("	  <td width='75%' style='border-bottom:thin solid green;'>                                              ");
                //sb.Append("	  " + Booking.VehicalType + "<br>&nbsp;</td>                                                                                    ");
                //sb.Append("	</tr>                                                                                                   ");
                sb.Append(EmailManager.BookingDetails(Booking));
                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td class='heading'>Trip Routing Information:</td>                                                    ");
                //sb.Append("	                                                                                                        ");
                //sb.Append("	  <td><b>Pick-up Location: &nbsp;</b>" + Booking.PickUpAddress + "</td>                         ");
                //sb.Append("	                                                                                                        ");
                //sb.Append("	</tr>                                                                                                   ");

                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                //sb.Append("	  <td><b>Pick-up Instructions: &nbsp;</b>your reservation is canceled by xx/xx/2016<br>&nbsp;</td>      ");
                //sb.Append("	</tr>	                                                                                                ");
                //sb.Append("                                                                                                         ");
                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                //sb.Append("	                                                                                                        ");
                //sb.Append("	  <td><b>Drop-off Location: &nbsp;</b>" + Booking.DropAddress + "</td>                                                  ");
                //sb.Append("	                                                                                                        ");
                //sb.Append("	</tr>	                                                                                                ");
                sb.Append("        <tr>                                                                                             ");
                sb.Append("          <td style='border-bottom:thin solid green;' colspan='2'>&nbsp;</td>                            ");
                sb.Append("        </tr>                                                                                            ");
                sb.Append("                                                                                                         ");
                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td class='heading' valign='top'>Payment Method:</td>                                                 ");
                //sb.Append("	  <td width='75%'>" + Booking.CCPayment + "<br>&nbsp;</td>                                                            ");
                //sb.Append("	</tr>                                                                                                   ");
                sb.Append("                                                                                                         ");
                sb.Append("                                                                                                         ");
                if (Booking.CCType != null)
                {
                    sb.Append(" 	<tr>                                                                                                ");
                    sb.Append("	  <td class='heading'>Payment Method:</td>                                                                 ");
                    sb.Append("	  <td>" + Booking.CCType + "**" + Booking.CCLast4 + "");
                    sb.Append("	</td></tr>                                                                                              ");
                }
                sb.Append(" 	<tr>                                                                                                ");
                sb.Append("	  <td class='heading'>Quoted Rate:</td>                                                                 ");
                sb.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(Booking.Fare), 2) + "                                                                                         ");
                sb.Append("	</td></tr>                                                                                              ");
                sb.Append("                                                                                                         ");
                sb.Append("	<tr>                                                                                                    ");
                sb.Append("	  <td class='heading'>Gratuity:</td>                                                                    ");
                sb.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(EmailManager.GetGratuity(Convert.ToSingle(Booking.Fare), Booking.Gratuity.ToString())), 2) + "                                                                                          ");
                sb.Append("	</td></tr>                                                                                              ");
                if (Booking.MeetGreat == true)
                {
                    sb.Append(" 	<tr>                                                                                                ");
                    sb.Append("	  <td class='heading'>Meet Great:</td>                                                                 ");
                    sb.Append("	  <td>$ " + 10 + "                                                                                         ");
                    sb.Append("	</td></tr>     ");
                }
                sb.Append("                                                                                                         ");
                sb.Append("	<tr>                                                                                                    ");
                sb.Append("	  <td class='heading'>Reservation Total:</td>                                                           ");
                sb.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(Booking.TotalFare), 2) + "                                                                                         ");
                sb.Append("	</td></tr>                                                                                              ");
                sb.Append("	<tr>                                                                                                    ");
                sb.Append("	  <td class='heading' valign='top'><font color='red'>Total Due:</font></td>");
                decimal TotalDue = 0;
                if (Convert.ToBoolean(Booking.Paid))
                {
                    TotalDue = decimal.Round(Convert.ToDecimal(TotalDue), 2);
                }
                else
                {
                    TotalDue = decimal.Round(Convert.ToDecimal(Booking.TotalFare), 2);
                }
                sb.Append("	  <td><font color='red'>$ " + TotalDue + "<br>&nbsp;</font>");
                sb.Append("	</td></tr>");
                sb.Append("	<tr>                                                                                                    ");
                sb.Append("	  <td class='heading' valign='top'><font color='red'>Note:</font></td>");
                sb.Append("	  <td><font color='red'><b>Late Night Charges and extra service charges may not show in 'Quoted Rate'. It will automatically add up in Reservation Total</b></font>");
                sb.Append("	</td></tr>");
                sb.Append("        <tr>                                                                                             ");
                sb.Append("          <td style='border-bottom:thin solid green;' colspan='2'>&nbsp;</td>                            ");
                sb.Append("        </tr>                                                                                            ");
                sb.Append("                                                                                                         ");
                //sb.Append("	  <td style='border-bottom:thin solid green;'><font color='red'>$ " + TotalDue + "<br>&nbsp;</font></td>");
                //sb.Append("	</tr>                                                                                              ");
                sb.Append("	<tr>                                                                                               ");
                sb.Append("	  <td class='heading' valign='top'>Terms &amp; Conditions / Reservation Agreement:</td>            ");
                sb.Append("	  <td>Guest agrees that there will be no smoking in our vehicles.<br>                              ");
                sb.Append("		Guest assures that no illegal drugs are brought into our vehicles.<br>                         ");
                sb.Append("		Guest agrees that no alcoholic beverages shall be brought into our vehicles.                   ");
                sb.Append("		Guest agrees that the passengers capacity of vehicle provided shall not be exceeded.<br>       ");
                sb.Append("		In case of misconduct by your party, chauffeur has the right to terminate this agreement without refunds.<br>");
                sb.Append("		Guest holds BWI Shuttle Service harmless and not liable for any personal or material damages arising from the conduct of his/her party.<br>");
                sb.Append("		Guest is responsible for the full payment of any overtime charges, beyond the original agreement.<br>");
                sb.Append("		Guest agrees to 100% payment charged to his/her credit card at the time reservation has been made.<br>");
                sb.Append("		Luxury Sedan guests can cancel 3 hours before pickup time with $10.00 processing fee; if not you will be charged the full amount of reservation. Stretch limousine and Van Guests can cancel 48 hours before pickup time with a $20.00 processing fee; if you cancel after 48 hours, you will be billed for a minimum 3 hours of service charges.<br>");
                sb.Append("		Guest acknowledges that he/she understands that BWI Shuttle Service imposes a service fee for late night service (11:00PM TO 5:00AM).<br>");
                sb.Append("		Guest ackowledges that he/she understands that BWI Shuttle Service imposes an additional service fee for Incoming International flights.<br>		");
                sb.Append("		BWI Shuttle Service cannot be held responsible for mechanical problems, inclement weather, or other uncontrollable circumstances resulting in the inability to start a job as it is schedule time or complete a job. In the event that the requested vehicle can not be provided; BWI Shuttle Service may provide a vehicle of equal quality.<br>");
                sb.Append("		BWI Shuttle Service reserves the right to change rates without notice.  If a rate change is necessary due to a destination being outside of the immediate service area, technical error or increase in fuel charges; BWI Shuttle Service will make every effort to notify the Guest of the change.<br>");
                sb.Append("	</td>                    ");
                sb.Append("	</tr>                    ");
                sb.Append("      </tbody></table>    ");
                sb.Append("  </td></tr>              ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("</tbody></table>          ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("</body></html>            ");
                string Mail = sb.ToString();
                #endregion

                #region Driver Mail
                if (DriverName != "Select Driver")
                {
                    #region Diver Mail for P2P Or Hourly
                    /*if (Booking.Service == "Point To Point Reservation" || Booking.Service == "Hourly Reservation")
                    {
                        #region Diver Mail for P2P Or Hourly
                        sbDriver.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
                    sbDriver.Append("<title>limoallaround.com - Customer Receipt</title>  ");
                    sbDriver.Append("<style>                                            ");
                    sbDriver.Append(".Norm {font-family: Verdana;                       ");
                    sbDriver.Append("font-size: 12px;                                 ");
                    sbDriver.Append("font-color: red;                               ");
                    sbDriver.Append(" }                                               ");
                    sbDriver.Append(".heading {font-family: Verdana;                   ");
                    sbDriver.Append("  font-size: 14px;                            ");
                    sbDriver.Append("  font-weight: 800;                           ");
                    sbDriver.Append("	 }                                                  ");
                    sbDriver.Append("   td {font-family: Verdana;                         ");
                    sbDriver.Append("	  font-size: 12px;                                  ");
                    sbDriver.Append("	 }                                                  ");
                    sbDriver.Append("  </style>                                           ");
                    sbDriver.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
                    sbDriver.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
                    sbDriver.Append("");
                    sbDriver.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
                    sbDriver.Append("  <tbody><tr>");
                    sbDriver.Append("    <td width='660' colspan='13' valign='top'>");
                    sbDriver.Append("      <table width='100%'>");
                    sbDriver.Append("        <tbody><tr>");
                    sbDriver.Append("          <td colspan='2' width='100%' class='heading'><b>Limo Service</b></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("        <tr>");
                    sbDriver.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com/'>http://www.bwishuttleservice.com</a></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("        <tr>");
                    sbDriver.Append("          <td colspan='2' width='100%'><a href='mailto:limoallaround.com@gmail.com'>limoallaround.com@gmail.com</a></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("        <tr>");
                    sbDriver.Append("          <td colspan='2' width='25%'>Tel: 410-235-2265</td>");
                    sbDriver.Append("          <td width='75%' align='right'><font size='3'>");
                    sbDriver.Append("		Receipt");
                    sbDriver.Append("	     </font></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("      </tbody></table>");
                    sbDriver.Append("     </td>");
                    sbDriver.Append("  </tr><tr>");
                    sbDriver.Append("  </tr><tr>");
                    sbDriver.Append("    <td colspan='13' width='660'>");
                    sbDriver.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                    sbDriver.Append("  	<tbody><tr>");
                    Sdate = GetAppDate(Booking.Date.Split(' ')[0]);
                    SerDate = Sdate.ToString("MM-dd-yyyy");
                    sbDriver.Append("  	  <td width='70%' style='border-bottom:medium solid blue; font-size:16px'><b>Reservation # " + Booking.ReservationID + "</b></td>");
                    sbDriver.Append("  	  <td width='30%' align='right' style='border-bottom:medium solid blue;'>Booking Date: " + SerDate + "</td>");
                    sbDriver.Append("  	</tr>");
                    sbDriver.Append("  	<tr>");
                    sbDriver.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br>Hi, " + Driver.sFirstName + " " + Driver.sLastName + " Your Booking with following Details has been updated!.If any of the information appears to be incorrect, please contact our office immediately to correct it. <br>&nbsp;</td>");
                    sbDriver.Append("	</tr>");
                    sbDriver.Append("      </tbody></table>");
                    sbDriver.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                    sbDriver.Append("	<tbody><tr>");
                    sbDriver.Append("	  <td class='heading'>Pick-up Date:</td>");
                    sbDriver.Append("	  <td width='75%'>" + Booking.ReservationDate + "</td>                 ");
                    sbDriver.Append("	</tr>                                             ");

                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'>Customer Name:</td>          ");
                    sbDriver.Append("	  <td width='75%'>" + Booking.FirstName + " " + Booking.LastName + "</td>                      ");
                    sbDriver.Append("	</tr>                                             ");

                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'>Customer Mobile:</td>          ");
                    sbDriver.Append("	  <td width='75%'>" + Booking.ContactNumber + "</td>                      ");
                    sbDriver.Append("	</tr>                                             ");

                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'>Customer Email:</td>          ");
                    sbDriver.Append("	  <td width='75%'>" + Booking.Email + "</td>                      ");
                    sbDriver.Append("	</tr>                                             ");

                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'>Pick-up Time:</td>          ");
                    if (Booking.Service == "From Airport")
                        sbDriver.Append("	  <td width='75%'>" + EmailManager.GetTime( Booking.FlightTime) + "</td>                      ");
                    else
                        sbDriver.Append("	  <td width='75%'>" + EmailManager.GetTime( Booking.Pickup_Time) + "</td>                      ");
                    sbDriver.Append("	</tr>                                             ");
                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'> Flight Number: </td>          ");
                    sbDriver.Append("	  <td width='75%'>" + Booking.FlightNumber + "</td>          ");
                    sbDriver.Append("	</tr>                                             ");

                sbDriver.Append("	  <td class='heading'>No. of Passenger:</td>         ");
                sbDriver.Append("	  <td width='75%'>" + Booking.Persons + "</td>                          ");
                sbDriver.Append("	</tr>                                             ");
                //sbDriver.Append("	<tr>                                              ");
                //sbDriver.Append("	  <td class='heading'>No. of Children:</td>       ");
                //sbDriver.Append("	  <td width='75%'>" + Booking.Child + "</td>                          ");
                //sbDriver.Append("	</tr>	                                          ");
                //sbDriver.Append("	<tr>                                              ");
                sbDriver.Append("	  <td class='heading' valign='Top' style='border-bottom:thin solid green;'>Vehicle Type:</td>           ");
                sbDriver.Append("	  <td width='75%' style='border-bottom:thin solid green;'>                                              ");
                sbDriver.Append("	  " + VehicleInfo.Model + "<br>&nbsp;</td>                                                                                    ");
                sbDriver.Append("	</tr>                                                                                                   ");
                sbDriver.Append("	<tr>                                                                                                    ");
                //sbDriver.Append("	  <td class='heading'>Trip Routing Information:</td>                                                    ");
                //sbDriver.Append("	                                                                                                        ");
                //sbDriver.Append("	  <td><b>Pick-up Location: &nbsp;</b>" + Booking.PickUpAddress + "</td>                         ");
                //sbDriver.Append("	                                                                                                        ");
                sbDriver.Append("	  <td class='heading' valign='Top' style='border-bottom:thin solid green;'>Pick-up Location: &nbsp;</td>           ");
                sbDriver.Append("	  <td width='75%' style='border-bottom:thin solid green;'>                                              ");
                sbDriver.Append("	  " + Booking.Source + "<br>&nbsp;</td>  ");   
                sbDriver.Append("	</tr>                                                                                                   ");
                //sbDriver.Append("	<tr>                                                                                                    ");
                //sbDriver.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                //sbDriver.Append("	  <td><b>Pick-up Instructions: &nbsp;</b>your reservation is canceled by xx/xx/2016<br>&nbsp;</td>      ");
                //sbDriver.Append("	</tr>	                                                                                                ");
                //sbDriver.Append("                                                                                                         ");
                sbDriver.Append("	<tr>                                                                                                    ");
                //sbDriver.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                //sbDriver.Append("	                                                                                                        ");
                //sbDriver.Append("	  <td><b>Drop-off Location: &nbsp;</b>" + Booking.DropAddress + "</td>                                                  ");
                sbDriver.Append("	  <td class='heading' valign='Top' style='border-bottom:thin solid green;'>Drop-off Location: &nbsp;</td>           ");
                sbDriver.Append("	  <td width='75%' style='border-bottom:thin solid green;'>                                              ");
                sbDriver.Append("	  " + Booking.Destination + "<br>&nbsp;</td>  ");   
                sbDriver.Append("	                                                                                                        ");
                sbDriver.Append("	</tr>	                                                                                                ");
                sbDriver.Append("        <tr>                                                                                             ");
                sbDriver.Append("          <td style='border-bottom:thin solid green;' colspan='2'>&nbsp;</td>                            ");
                sbDriver.Append("        </tr>                                                                                            ");
                sbDriver.Append("                                                                                                         ");
                sbDriver.Append("	<tr>                                                                                                    ");
                sbDriver.Append("	  <td class='heading' valign='top'>Payment Method:</td>                                                 ");
                sbDriver.Append("	  <td width='75%'>" + Booking.CCPayment + "<br>&nbsp;</td>                                                            ");
                sbDriver.Append("	</tr>                                                                                                   ");
                sbDriver.Append("                                                                                                         ");
                sbDriver.Append("                                                                                                         ");
                sbDriver.Append(" 	<tr>                                                                                                ");
                sbDriver.Append("	  <td class='heading'>Quoted Rate:</td>                                                                 ");
                sbDriver.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(Booking.Fare),2) + "                                                                                         ");
                sbDriver.Append("	</td></tr>                                                                                              ");
                sbDriver.Append("                                                                                                         ");
                sbDriver.Append("	<tr>                                                                                                    ");
                sbDriver.Append("	  <td class='heading'>Gratuity:</td>                                                                    ");
                sbDriver.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(Booking.Gratuity),2) + "                                                                                          ");
                sbDriver.Append("	</td></tr>                                                                                              ");
                sbDriver.Append("                                                                                                         ");
                sbDriver.Append("	<tr>                                                                                                    ");
                sbDriver.Append("	  <td class='heading'>Reservation Total:</td>                                                           ");
                sbDriver.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(Booking.TotalFare),2) + "                                                                                         ");
                sbDriver.Append("	</td></tr>                                                                                              ");
                sbDriver.Append("	<tr>                                                                                                    ");
                sbDriver.Append("	  <td style='border-bottom:thin solid green;' class='heading' valign='top'><font color='red'>Total Due:</font></td>");
                sbDriver.Append("	  <td style='border-bottom:thin solid green;'><font color='red'>$ 0.00<br>&nbsp;</font></td>     ");
                sbDriver.Append("	</tr>                                                                                              ");
                sbDriver.Append("	<tr>                                                                                               ");
                sbDriver.Append("	  <td class='heading' valign='top'>Terms &amp; Conditions / Reservation Agreement:</td>            ");
                sbDriver.Append("	  <td>Guest agrees that there will be no smoking in our vehicles.<br>                              ");
                sbDriver.Append("		Guest assures that no illegal drugs are brought into our vehicles.<br>                         ");
                sbDriver.Append("		Guest agrees that no alcoholic beverages shall be brought into our vehicles.                   ");
                sbDriver.Append("		Guest agrees that the passengers capacity of vehicle provided shall not be exceeded.<br>       ");
                sbDriver.Append("		In case of misconduct by your party, chauffeur has the right to terminate this agreement without refunds.<br>");
                sbDriver.Append("		Guest holds Limo All Around Service harmless and not liable for any personal or material damages arising from the conduct of his/her party.<br>");
                sbDriver.Append("		Guest is responsible for the full payment of any overtime charges, beyond the original agreement.<br>");
                sbDriver.Append("		Guest agrees to 100% payment charged to his/her credit card at the time reservation has been made.<br>");
                sbDriver.Append("		Luxury Sedan guests can cancel 3 hours before pickup time with $10.00 processing fee; if not you will be charged the full amount of reservation. Stretch limousine and Van Guests can cancel 48 hours before pickup time with a $20.00 processing fee; if you cancel after 48 hours, you will be billed for a minimum 3 hours of service charges.<br>");
                sbDriver.Append("		Guest acknowledges that he/she understands that Limo All Around Service imposes a service fee for late night service (11:00PM TO 5:00AM).<br>");
                sbDriver.Append("		Guest ackowledges that he/she understands that Limo All Around Service imposes an additional service fee for Incoming International flights.<br>		");
                sbDriver.Append("		Limo All Around Service cannot be held responsible for mechanical problems, inclement weather, or other uncontrollable circumstances resulting in the inability to start a job as it is schedule time or complete a job. In the event that the requested vehicle can not be provided; Limo All Around Service may provide a vehicle of equal quality.<br>");
                sbDriver.Append("		Limo All Around Service reserves the right to change rates without notice.  If a rate change is necessary due to a destination being outside of the immediate service area, technical error or increase in fuel charges; Limo All Around Service will make every effort to notify the Guest of the change.<br>");
                sbDriver.Append("	</td>                    ");
                sbDriver.Append("	</tr>                    ");
                sbDriver.Append("      </tbody></table>    ");
                sbDriver.Append("  </td></tr>              ");
                sbDriver.Append("                          ");
                sbDriver.Append("                          ");
                sbDriver.Append("                          ");
                sbDriver.Append("                          ");
                sbDriver.Append("                          ");
                sbDriver.Append("</tbody></table>          ");
                sbDriver.Append("                          ");
                sbDriver.Append("                          ");
                sbDriver.Append("</body></html>            ");

                        #endregion
                    }
                    else
                    {*/
                    #endregion
                    #region Driver Mail for Airport reservation
                    sbDriver.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
                    sbDriver.Append("<title>www.bwishuttleservice.com - Customer Receipt</title>  ");
                    sbDriver.Append("<style>                                            ");
                    sbDriver.Append(".Norm {font-family: Verdana;                       ");
                    sbDriver.Append("font-size: 12px;                                 ");
                    sbDriver.Append("font-color: red;                               ");
                    sbDriver.Append(" }                                               ");
                    sbDriver.Append(".heading {font-family: Verdana;                   ");
                    sbDriver.Append("  font-size: 14px;                            ");
                    sbDriver.Append("  font-weight: 800;                           ");
                    sbDriver.Append("	 }                                                  ");
                    sbDriver.Append("   td {font-family: Verdana;                         ");
                    sbDriver.Append("	  font-size: 12px;                                  ");
                    sbDriver.Append("	 }                                                  ");
                    sbDriver.Append("  </style>                                           ");
                    sbDriver.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
                    sbDriver.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
                    sbDriver.Append("");
                    sbDriver.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
                    sbDriver.Append("  <tbody><tr>");
                    sbDriver.Append("    <td width='660' colspan='13' valign='top'>");
                    sbDriver.Append("      <table width='100%'>");
                    sbDriver.Append("        <tbody><tr>");
                    sbDriver.Append("        <tr>");
                    sbDriver.Append("          <td colspan='3' width='100%'><a href='http://www.bwishuttleservice.com'>http://www.bwishuttleservice.com</a></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("        <tr>");
                    sbDriver.Append("          <td colspan='3' width='100%'><a href='mailto:bwiairportshuttleservice@gmail.com'>bwiairportshuttleservice@gmail.com</a></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("        <tr>");
                    sbDriver.Append("          <td colspan='2' width='25%'>Tel:  410-904-5151</td>");
                    sbDriver.Append("          <td width='75%' align='right'><font size='3'>");
                    sbDriver.Append("		Receipt");
                    sbDriver.Append("	     </font></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("      </tbody></table>");
                    sbDriver.Append("     </td>");
                    sbDriver.Append("  </tr><tr>");
                    sbDriver.Append("  </tr><tr>");
                    sbDriver.Append("    <td colspan='13' width='660'>");
                    sbDriver.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                    sbDriver.Append("  	<tbody><tr>");
                    sbDriver.Append("  	  <td width='70%' style='border-bottom:medium solid blue; font-size:16px'><b>Reservation # " + Booking.ReservationID + "</b></td>");
                    Sdate = GetAppDate(Booking.Date.Split(' ')[0]);
                    SerDate = Sdate.ToString("MM-dd-yyyy");
                    sbDriver.Append("  	  <td width='30%' align='right' style='border-bottom:medium solid blue;'>Booking Date: " + SerDate + "</td>");
                    sbDriver.Append("  	</tr>");
                    sbDriver.Append("  	<tr>");
                    sbDriver.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br>Hi, " + Driver.sFirstName + " " + Driver.sLastName + " you have Booking with following Details.If any of the information appears to be incorrect, please contact our office immediately to correct it. <br>&nbsp;</td>");
                    sbDriver.Append("	</tr>");
                    sbDriver.Append("      </tbody></table>");
                    sbDriver.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                    sbDriver.Append("	<tbody><tr>");
                    sbDriver.Append("	  <td class='heading'>SUBJ:Rez:</td>");
                    sbDriver.Append("	  <td width='75%'>" + Booking.ReservationDate + "</td>          ");
                    sbDriver.Append("	</tr>");
                    sbDriver.Append("  	<tr>");
                    sbDriver.Append("	  <td class='heading'>MSG: F/T:</td>");
                    string Airlines = Booking.Airlines;
                    if (Booking.Airlines == null)
                    {
                        Airlines = "";
                    }
                    else
                    {
                        Airlines = ", " + Airlines;
                    }
                    if (Booking.Service == "Point To Point Reservation" || Booking.Service == "Hourly Reservation")
                    {
                        if (Booking.P2PTime == null && Booking.Service == "Point To Point Reservation")
                        {
                            sbDriver.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.Pickup_Time) + "" + Airlines + "</td>");
                        }
                        else
                            sbDriver.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.P2PTime) + "" + Airlines + "</td>");
                    }

                    else if (Booking.Service == "To Airport")
                        sbDriver.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.Pickup_Time) + "" + Airlines + "</td>");
                    else
                    {
                        if (Booking.Ret_FlightTime != null)
                            sbDriver.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.Ret_FlightTime) + "" + Airlines + "</td>");
                        else
                            sbDriver.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.FlightTime) + "" + Airlines + "</td>");
                    }
                    sbDriver.Append("	</tr>                                             ");
                    if (Booking.Service != "Point To Point Reservation" && Booking.Service != "Hourly Reservation")
                    {
                        sbDriver.Append("	<tr>                                              ");
                        sbDriver.Append("	  <td class='heading'> Flight Number: </td>          ");
                        sbDriver.Append("	  <td width='75%'>" + Booking.FlightNumber + "</td>          ");
                        sbDriver.Append("	</tr>                                             ");
                    }
                    if (Booking.Service == "Hourly Reservation")
                    {
                        sbDriver.Append("	<tr>                                              ");
                        sbDriver.Append("	  <td class='heading'>Hours:</td>         ");
                        sbDriver.Append("	  <td width='75%'>" + Booking.Hours + "</td>                          ");
                        sbDriver.Append("	</tr>");
                    }
                    sbDriver.Append("	<tr>                                              ");
                    //if (Booking.Service == "To Airport")
                    //{
                    if (Booking.Service == "Point To Point Reservation" || Booking.Service == "Hourly Reservation")
                    {
                        sbDriver.Append("	  <td class='heading'>Service: </td>          ");
                        sbDriver.Append("	  <td width='75%'>" + Booking.Service + "</td>          ");
                    }
                    else
                    {
                        sbDriver.Append("	  <td class='heading'>" + Booking.Service + ": </td>          ");
                        sbDriver.Append("	  <td width='75%'>" + Booking.AirPortName + "</td>          ");
                    }
                    //}
                    //else if (Booking.Service == "From Airport")
                    //{
                    //  sbDriver.Append("	  <td class='heading'>" + Booking.Service + ": " + Booking.PickUpAddress + "</td>          ");
                    //}
                    sbDriver.Append("	</tr>                                             ");
                    sbDriver.Append("	<tr>                                              ");
                    if (Booking.Service == "To Airport" || Booking.Service == "Hourly Reservation")
                    {
                        sbDriver.Append("	  <td class='heading'>P/U: </td>          ");
                        sbDriver.Append("	  <td width='75%'>" + Booking.Source + "</td>               ");
                    }
                    else if (Booking.Service == "From Airport")
                    {
                        sbDriver.Append("	  <td class='heading'>Dest: </td>          ");
                        sbDriver.Append("	  <td width='75%'>" + Booking.Destination + "</td>               ");
                    }
                    else if (Booking.Service == "Point To Point Reservation")
                    {
                        string Location = "", HaltTime = "", Mics = "";

                        sbDriver.Append("	  <td class='heading'>Trip Routing:</td>");
                        sbDriver.Append("");
                        //sbDriver.Append("	  <td class='heading'>&nbsp;</td>");
                        sbDriver.Append("	  <td><table style=\"width:100%\"><thead><tr><th>Location</th><th>Halting Time</th><th>MISC</th></tr></thead><tbody>");
                        if (Booking.Stop1 != "^-:-^")
                        {
                            Location = ""; HaltTime = ""; Mics = "";
                            GetP2PLocation(Booking.Stop1, out Location, out HaltTime, out Mics);
                            sbDriver.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                        }
                        if (Booking.Stop2 != "^-:-^")
                        {
                            Location = ""; HaltTime = ""; Mics = "";
                            GetP2PLocation(Booking.Stop2, out Location, out HaltTime, out Mics);
                            sbDriver.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                        }
                        if (Booking.Stop3 != "^-:-^")
                        {
                            Location = ""; HaltTime = ""; Mics = "";
                            GetP2PLocation(Booking.Stop3, out Location, out HaltTime, out Mics);
                            sbDriver.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                        }
                        if (Booking.Stop4 != "^-:-^")
                        {
                            Location = ""; HaltTime = ""; Mics = "";
                            GetP2PLocation(Booking.Stop4, out Location, out HaltTime, out Mics);
                            sbDriver.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                        }
                        if (Booking.Stop5 != "^-:-^")
                        {
                            Location = ""; HaltTime = ""; Mics = "";
                            GetP2PLocation(Booking.Stop5, out Location, out HaltTime, out Mics);
                            sbDriver.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                        }
                        sbDriver.Append("	</tr>                                             ");
                        sbDriver.Append("</tbody></table></td>");
                    }
                    sbDriver.Append("	</tr>                                             ");
                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'>Name:</td>          ");
                    sbDriver.Append("	  <td width='75%'>" + Booking.FirstName + " " + Booking.LastName + "</td>                      ");
                    sbDriver.Append("	</tr>                                             ");
                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'># :</td>          ");
                    if (Booking.ContactNumber == "" || Booking.ContactNumber == null)
                    {
                        sbDriver.Append("	  <td width='75%'>" + Booking.PhoneNumber + "</td>               ");
                    }
                    else
                    {
                        sbDriver.Append("	  <td width='75%'>" + Booking.ContactNumber + "</td>               ");
                    }
                    sbDriver.Append("	</tr>                                             ");

                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'>Reservation Comments:</td>          ");
                    sbDriver.Append("	  <td width='75%'>" + Booking.Remark + "</td>                      ");
                    sbDriver.Append("	</tr>                                             ");

                    sbDriver.Append("        </tr>                                                                                            ");
                    sbDriver.Append("                          ");

                    sbDriver.Append("      </tbody></table>    ");
                    sbDriver.Append("  </td></tr>              ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("</tbody></table>          ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("</body></html>            ");
                    #endregion
                    //}
                }

                Mail = sbDriver.ToString();
                #endregion

                #region Mailing Section
                try
                {
                    MailMessage message = new MailMessage();
                    SmtpClient smtpClient = new SmtpClient();
                    try
                    {
                        MailAddress fromAddress = new MailAddress("reservation@bwishuttleservice.com");
                        message.From = fromAddress;
                        message.To.Add(Booking.Email);
                        message.CC.Add("bwiairportshuttleservice@gmail.com");
                        //if (Reservation.Status != "Completed")
                        //{
                        //    message.CC.Add("bwiairportshuttleservice@gmail.com");
                        //}
                        if (Reservation.Status == "Completed")
                        {
                            message.Subject = "Receipt Booking-" + Booking.ReservationID;
                        }
                        else
                        {
                            message.Subject = "Confirm Booking-" + Booking.ReservationID;
                        }
                        message.IsBodyHtml = true;
                        message.Body = sb.ToString();
                        //smtpClient.Host = "relay-hosting.secureserver.net";   //-- Donot change.
                        //smtpClient.Port = 25; //--- Donot change
                        //smtpClient.EnableSsl = false;//--- Donot change
                        //smtpClient.UseDefaultCredentials = true;
                        //smtpClient.Credentials = new System.Net.NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                        ////smtpClient.Credentials = new System.Net.NetworkCredential("support@limoallaround.net", "asdrc@123");
                        //smtpClient.Send(message);

                        SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                        mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                        mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        mailObj.EnableSsl = false;
                        message.Sender = new MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                        mailObj.Send(message);
                        message.Dispose();
                    }
                    catch (SmtpException ex)
                    {
                        msg = ex.Message;
                        json = "{\"Session\":\"1\",\"Retcode\":\"3\",\"Error\":\"" + msg + "\"}";
                        return json;
                    }
                    // start Customer Mail
                    /*MailMessage message = new MailMessage();
                    SmtpClient smtpClient = new SmtpClient();
                    string msg = string.Empty;
                    MailAddress fromAddress = new MailAddress("support@limoallaround.net");
                    message.From = fromAddress;
                    message.To.Add(Booking.Email);
                    message.CC.Add("limoallaround@gmail.com");
                    message.Subject = "Confirm Booking-" + Booking.ReservationID;
                    message.IsBodyHtml = true;
                    message.Body = sb.ToString();
                    smtpClient.Host = "relay-hosting.secureserver.net";   //-- Donot change.
                    smtpClient.Port = 25; //--- Donot change
                    smtpClient.EnableSsl = false;//--- Donot change
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.Credentials = new System.Net.NetworkCredential("support@limoallaround.net", "asdrc@123");
                    smtpClient.Send(message);*/
                    // End Customer Mail

                    /*System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                    mailMsg.From = new MailAddress("support@limoallaround.net", "Limo IT Team");
                    mailMsg.To.Add(Booking.Email);
                    mailMsg.Subject = "Confirm Booking-" + Booking.ReservationID;
                    mailMsg.IsBodyHtml = true;
                    mailMsg.Body = sb.ToString();
                    SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                    //SmtpClient mailObj = new SmtpClient("smtpout.secureserver.net", 25);
                    //mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                    mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    mailObj.EnableSsl = false;
                    mailMsg.Sender = new MailAddress("support@limoallaround.net");
                    mailObj.Send(mailMsg);
                    mailMsg.Dispose();*/
                }
                catch (Exception ex)
                {
                    msg = ex.Message;
                    json = "{\"Session\":\"1\",\"Retcode\":\"3\",\"Error\":\"" + msg + "\"}";
                    return json;
                }

                if (DriverName != "Select Driver")
                {
                    try
                    {
                        MailMessage message = new MailMessage();
                        SmtpClient smtpClient = new SmtpClient();
                        try
                        {
                            MailAddress fromAddress = new MailAddress("reservation@bwishuttleservice.com");
                            message.From = fromAddress;
                            message.To.Add(Driver.sEmail);
                            //message.CC.Add(Driver.sEmail);
                            message.Subject = "You have Booking -" + Booking.ReservationID;
                            message.IsBodyHtml = true;
                            message.Body = sbDriver.ToString();
                            //  smtpClient.Host = "relay-hosting.secureserver.net";   //-- Donot change.
                            // smtpClient.Port = 25; //--- Donot change
                            //  smtpClient.EnableSsl = false;//--- Donot change
                            //   smtpClient.UseDefaultCredentials = true;
                            //   smtpClient.Credentials = new System.Net.NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                            //   //smtpClient.Credentials = new System.Net.NetworkCredential("support@limoallaround.net", "asdrc@123");
                            //   smtpClient.Send(message);

                            SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                            mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                            mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                            mailObj.EnableSsl = false;
                            message.Sender = new MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                            mailObj.Send(message);
                            message.Dispose();
                        }
                        catch (Exception ex)
                        {
                            msg = ex.Message;
                            json = "{\"Session\":\"1\",\"Retcode\":\"3\",\"Error\":\"" + msg + "\"}";
                            return json;
                        }
                        /*MailMessage message = new MailMessage();
                        SmtpClient smtpClient = new SmtpClient();
                        string msg = string.Empty;
                        MailAddress fromAddress = new MailAddress("support@limoallaround.net");
                        message.From = fromAddress;
                        message.To.Add(Driver.sEmail);
                        message.Subject = "You have Booking -" + Booking.ReservationID;
                        message.IsBodyHtml = true;
                        message.Body = sbDriver.ToString();
                        smtpClient.Host = "relay-hosting.secureserver.net";   //-- Donot change.
                        smtpClient.Port = 25; //--- Donot change
                        smtpClient.EnableSsl = false;//--- Donot change
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.Credentials = new System.Net.NetworkCredential("support@limoallaround.net", "asdrc@123");
                        smtpClient.Send(message);*/
                        /*System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                        mailMsg.From = new MailAddress("support@limoallaround.net", "Limo IT Team");
                        mailMsg.To.Add(Driver.sEmail);
                        mailMsg.Subject = "You have Booking -" + Booking.ReservationID;
                        mailMsg.IsBodyHtml = true;
                        mailMsg.Body = sbDriver.ToString();
                        SmtpClient mailObj = new SmtpClient("smtpout.secureserver.net", 25);
                        mailObj.Credentials = new NetworkCredential("support@limoallaround.net", "asdrc@123");
                        //mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                        mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        mailObj.EnableSsl = false;
                        mailMsg.Sender = new MailAddress("support@limoallaround.net");
                        mailObj.Send(mailMsg);
                        mailMsg.Dispose();*/
                    }
                    catch (Exception ex)
                    {
                        msg = ex.Message;
                        json = "{\"Session\":\"1\",\"Retcode\":\"3\",\"Error\":\"" + msg + "\"}";
                        return json;
                    }
                }

                #endregion

                json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                json = "{\"Session\":\"1\",\"Retcode\":\"-1\",\"Error\":\"" + msg + "\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string CancelBooking(int BookingSid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_Login Driver = new Trans_Tbl_Login();
                Trans_Reservation Reservation = DB.Trans_Reservations.Single(x => x.sid == BookingSid);
                string DriverName = Reservation.AssignedTo;
                var DriverSid = Reservation.DriverSid;
                if (Reservation.Status == "Cancelled")
                {
                    json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
                    //json = "{\"Session\":\"1\",\"Retcode\":\"4\"}";
                    return json;
                }

                Reservation.Status = "Cancelled";
                DB.SubmitChanges();

                if (DriverName != "not assign")
                {
                    Driver = DB.Trans_Tbl_Logins.Single(x => x.Sid == DriverSid);
                }

                Trans_Reservation Booking = DB.Trans_Reservations.Single(x => x.sid == BookingSid);
                Trans_tbl_Vehicle_Info VehicleInfo = new Trans_tbl_Vehicle_Info();
                VehicleInfo = DB.Trans_tbl_Vehicle_Infos.Single(x => x.sid == Convert.ToInt64(Booking.VehicalType));
                StringBuilder sb = new StringBuilder();
                StringBuilder sbDriver = new StringBuilder();

                #region Customer Mail
                sb.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
                sb.Append("<title>www.bwishuttleservice.com- Customer Receipt</title>  ");
                sb.Append("<style>                                            ");
                sb.Append(".Norm {font-family: Verdana;                       ");
                sb.Append("font-size: 12px;                                 ");
                sb.Append("font-color: red;                               ");
                sb.Append(" }                                               ");
                sb.Append(".heading {font-family: Verdana;                   ");
                sb.Append("  font-size: 14px;                            ");
                sb.Append("  font-weight: 800;                           ");
                sb.Append("	 }                                                  ");
                sb.Append("   td {font-family: Verdana;                         ");
                sb.Append("	  font-size: 12px;                                  ");
                sb.Append("	 }                                                  ");
                sb.Append("  </style>                                           ");
                sb.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
                sb.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
                sb.Append("");
                sb.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
                sb.Append("  <tbody><tr>");
                sb.Append("    <td width='660' colspan='13' valign='top'>");
                sb.Append("      <table width='100%'>");
                sb.Append("        <tbody><tr>");
                //sb.Append("          <td colspan='2' width='100%' class='heading'><b>Bwi Shuttle Service</b></td>");
                sb.Append("        </tr>");
                //sb.Append("        <tr>");
                //sb.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com/'>http://www.bwishuttleservice.com</a></td>");
                //sb.Append("        </tr>");
                sb.Append("        <tr>");
                sb.Append("          <td colspan='3' width='100%'><a href='http://www.bwishuttleservice.com'>http://www.bwishuttleservice.com</a></td>");
                sb.Append("        </tr>");
                sb.Append("        <tr>");
                sb.Append("          <td colspan='3' width='100%'><a href='mailto:bwiairportshuttleservice@gmail.com'>bwiairportshuttleservice@gmail.com</a></td>");
                sb.Append("        </tr>");
                sb.Append("        <tr>");
                sb.Append("          <td colspan='2' width='25%'>Call Us: 1-844-904-5151, 410-904-5151</td>");
                sb.Append("          <td width='75%' align='right'><font size='3'>");
                sb.Append("		Receipt");
                sb.Append("	     </font></td>");
                sb.Append("        </tr>");
                sb.Append("      </tbody></table>");
                sb.Append("     </td>");
                sb.Append("  </tr><tr>");
                sb.Append("  </tr><tr>");
                sb.Append("    <td colspan='13' width='660'>");
                sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                sb.Append("  	<tbody><tr>");
                sb.Append("  	  <td width='70%' style='border-bottom:medium solid blue; font-size:16px'><b>Reservation # " + Booking.ReservationID + "</b></td>");
                DateTime Sdate = GetAppDate(Booking.Date.Split(' ')[0]);
                string SerDate = Sdate.ToString("MM-dd-yyyy");
                sb.Append("  	  <td width='30%' align='right' style='border-bottom:medium solid blue;'>Booking Date: " + SerDate + "</td>");
                sb.Append("  	</tr>");
                sb.Append("  	<tr>");
                sb.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br>Reservation has been cancelled! Below please find your Details. If any of the information appears to be incorrect, please contact our office immediately to correct it.<br>&nbsp;</td>");
                sb.Append("	</tr>");
                sb.Append("      </tbody></table>");
                sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                sb.Append("	<tbody><tr>");

                sb.Append("	  <td class='heading'>Pick-up Date:</td>");
                sb.Append("	  <td width='75%'>" + Booking.ReservationDate + "</td>");
                //sb.Append("	  <td width='75%'>" + EmailManager.GetDate(Booking.ReservationDate) + "</td>");
                sb.Append("	</tr>                                             ");

                sb.Append("	<tr>                                              ");
                sb.Append("	  <td class='heading'>Pickup Time:</td>          ");
                if (Booking.Service == "Point To Point Reservation" || Booking.Service == "Hourly Reservation")
                {
                    if (Booking.P2PTime == null && Booking.Service == "Point To Point Reservation")
                    {
                        sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.Pickup_Time) + "</td>");
                    }
                    else
                        sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.P2PTime) + "</td>");
                }

                else if (Booking.Service == "To Airport" || Booking.Service == "To Airport Shuttle")
                    sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.Pickup_Time) + "</td>");
                else if (Booking.Service == "From Airport" || Booking.Service == "From Airport Shuttle")
                    sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.FlightTime) + "</td>");
                else
                {
                    if (Booking.Ret_FlightTime != null)
                        sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.Ret_FlightTime) + "</td>");
                    else
                        sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.FlightTime) + "</td>");
                }

                sb.Append("	</tr>                                             ");
                sb.Append("	<tr>                                              ");
                sb.Append("	  <td class='heading' valign='Top' style='border-bottom:thin solid green;'>Vehicle Type:</td>           ");
                sb.Append("	  <td width='75%' style='border-bottom:thin solid green;'>                                              ");
                sb.Append("	  " + VehicleInfo.Model + "<br>&nbsp;</td>                                                                                    ");
                sb.Append("	</tr>      ");


                sb.Append("	<tr>                                              ");
                sb.Append("	  <td class='heading'>Primary Contact:</td>       ");
                sb.Append("	  <td width='75%'>" + Booking.FirstName + " " + Booking.LastName + "</td>           ");
                sb.Append("	</tr>                                             ");
                sb.Append("	<tr>                                              ");
                sb.Append("	  <td class='heading'>Contact Number:</td>        ");
                if (Booking.ContactNumber == "" || Booking.ContactNumber == null)
                {
                    sb.Append("	  <td width='75%'>" + Booking.PhoneNumber + "</td>               ");
                }
                else
                {
                    sb.Append("	  <td width='75%'>" + Booking.ContactNumber + "</td>               ");
                }
                sb.Append("	</tr>                                             ");
                sb.Append("	<tr>                                              ");
                sb.Append("	  <td class='heading'>No. of Passenger:</td>         ");
                sb.Append("	  <td width='75%'>" + Booking.Persons + "</td>                          ");
                sb.Append("	</tr>                                             ");
                //sb.Append("	<tr>                                              ");
                //sb.Append("	  <td class='heading'>No. of Children:</td>       ");
                //sb.Append("	  <td width='75%'>" + Booking.Child + "</td>                          ");
                //sb.Append("	</tr>	                                          ");
                if (Booking.Service == "Hourly Reservation")
                {
                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Hours:</td>         ");
                    sb.Append("	  <td width='75%'>" + Booking.Hours + "</td>                          ");
                    sb.Append("	</tr>");
                }
                //sb.Append("	<tr>                                              ");
                //sb.Append("	  <td class='heading' valign='Top' style='border-bottom:thin solid green;'>Vehicle Type:</td>           ");
                //sb.Append("	  <td width='75%' style='border-bottom:thin solid green;'>                                              ");
                //sb.Append("	  " + Booking.VehicalType + "<br>&nbsp;</td>                                                                                    ");
                //sb.Append("	</tr>                                                                                                   ");
                sb.Append(EmailManager.BookingDetails(Booking));
                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td class='heading'>Trip Routing Information:</td>                                                    ");
                //sb.Append("	                                                                                                        ");
                //sb.Append("	  <td><b>Pick-up Location: &nbsp;</b>" + Booking.PickUpAddress + "</td>                         ");
                //sb.Append("	                                                                                                        ");
                //sb.Append("	</tr>                                                                                                   ");

                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                //sb.Append("	  <td><b>Pick-up Instructions: &nbsp;</b>your reservation is canceled by xx/xx/2016<br>&nbsp;</td>      ");
                //sb.Append("	</tr>	                                                                                                ");
                //sb.Append("                                                                                                         ");
                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                //sb.Append("	                                                                                                        ");
                //sb.Append("	  <td><b>Drop-off Location: &nbsp;</b>" + Booking.DropAddress + "</td>                                                  ");
                //sb.Append("	                                                                                                        ");
                //sb.Append("	</tr>	                                                                                                ");
                sb.Append("        <tr>                                                                                             ");
                sb.Append("          <td style='border-bottom:thin solid green;' colspan='2'>&nbsp;</td>                            ");
                sb.Append("        </tr>                                                                                            ");
                sb.Append("                                                                                                         ");
                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td class='heading' valign='top'>Payment Method:</td>                                                 ");
                //sb.Append("	  <td width='75%'>" + Booking.CCPayment + "<br>&nbsp;</td>                                                            ");
                //sb.Append("	</tr>                                                                                                   ");
                sb.Append("                                                                                                         ");
                sb.Append("                                                                                                         ");
                if (Booking.CCType != null)
                {
                    sb.Append(" 	<tr>                                                                                                ");
                    sb.Append("	  <td class='heading'>Payment Method:</td>                                                                 ");
                    sb.Append("	  <td>" + Booking.CCType + "**" + Booking.CCLast4 + "");
                    sb.Append("	</td></tr>                                                                                              ");
                }
                sb.Append(" 	<tr>                                                                                                ");
                sb.Append("	  <td class='heading'>Quoted Rate:</td>                                                                 ");
                sb.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(Booking.Fare), 2) + "                                                                                         ");
                sb.Append("	</td></tr>                                                                                              ");
                sb.Append("                                                                                                         ");
                sb.Append("	<tr>                                                                                                    ");
                sb.Append("	  <td class='heading'>Gratuity:</td>                                                                    ");
                sb.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(EmailManager.GetGratuity(Convert.ToSingle(Booking.Fare), Booking.Gratuity.ToString())), 2) + "                                                                                          ");
                sb.Append("	</td></tr>                                                                                              ");
                if (Booking.MeetGreat == true)
                {
                    sb.Append(" 	<tr>                                                                                                ");
                    sb.Append("	  <td class='heading'>Meet Great:</td>                                                                 ");
                    sb.Append("	  <td>$ " + 10 + "                                                                                         ");
                    sb.Append("	</td></tr>     ");
                }
                sb.Append("                                                                                                         ");
                sb.Append("	<tr>                                                                                                    ");
                sb.Append("	  <td class='heading'>Reservation Total:</td>                                                           ");
                sb.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(Booking.TotalFare), 2) + "                                                                                         ");
                sb.Append("	</td></tr>                                                                                              ");
                sb.Append("	<tr>                                                                                                    ");
                sb.Append("	  <td class='heading' valign='top'><font color='red'>Total Due:</font></td>");
                decimal TotalDue = 0;
                if (Convert.ToBoolean(Booking.Paid))
                {
                    TotalDue = decimal.Round(Convert.ToDecimal(TotalDue), 2);
                }
                else
                {
                    TotalDue = decimal.Round(Convert.ToDecimal(Booking.TotalFare), 2);
                }
                sb.Append("	  <td><font color='red'>$ " + TotalDue + "<br>&nbsp;</font>");
                sb.Append("	</td></tr>");
                sb.Append("	<tr>                                                                                                    ");
                sb.Append("	  <td class='heading' valign='top'><font color='red'>Note:</font></td>");
                sb.Append("	  <td><font color='red'><b>Late Night Charges and extra service charges may not show in 'Quoted Rate'. It will automatically add up in Reservation Total</b></font>");
                sb.Append("	</td></tr>");
                sb.Append("        <tr>                                                                                             ");
                sb.Append("          <td style='border-bottom:thin solid green;' colspan='2'>&nbsp;</td>                            ");
                sb.Append("        </tr>                                                                                            ");
                sb.Append("                                                                                                         ");
                //sb.Append("	  <td style='border-bottom:thin solid green;'><font color='red'>$ " + TotalDue + "<br>&nbsp;</font></td>");
                //sb.Append("	</tr>                                                                                              ");
                sb.Append("	<tr>                                                                                               ");
                sb.Append("	  <td class='heading' valign='top'>Terms &amp; Conditions / Reservation Agreement:</td>            ");
                sb.Append("	  <td>Guest agrees that there will be no smoking in our vehicles.<br>                              ");
                sb.Append("		Guest assures that no illegal drugs are brought into our vehicles.<br>                         ");
                sb.Append("		Guest agrees that no alcoholic beverages shall be brought into our vehicles.                   ");
                sb.Append("		Guest agrees that the passengers capacity of vehicle provided shall not be exceeded.<br>       ");
                sb.Append("		In case of misconduct by your party, chauffeur has the right to terminate this agreement without refunds.<br>");
                sb.Append("		Guest holds BWI Shuttle Service harmless and not liable for any personal or material damages arising from the conduct of his/her party.<br>");
                sb.Append("		Guest is responsible for the full payment of any overtime charges, beyond the original agreement.<br>");
                sb.Append("		Guest agrees to 100% payment charged to his/her credit card at the time reservation has been made.<br>");
                sb.Append("		Luxury Sedan guests can cancel 3 hours before pickup time with $10.00 processing fee; if not you will be charged the full amount of reservation. Stretch limousine and Van Guests can cancel 48 hours before pickup time with a $20.00 processing fee; if you cancel after 48 hours, you will be billed for a minimum 3 hours of service charges.<br>");
                sb.Append("		Guest acknowledges that he/she understands that BWI Shuttle Service imposes a service fee for late night service (11:00PM TO 5:00AM).<br>");
                sb.Append("		Guest ackowledges that he/she understands that BWI Shuttle Service imposes an additional service fee for Incoming International flights.<br>		");
                sb.Append("		BWI Shuttle Service cannot be held responsible for mechanical problems, inclement weather, or other uncontrollable circumstances resulting in the inability to start a job as it is schedule time or complete a job. In the event that the requested vehicle can not be provided; BWI Shuttle Service may provide a vehicle of equal quality.<br>");
                sb.Append("		BWI Shuttle Service reserves the right to change rates without notice.  If a rate change is necessary due to a destination being outside of the immediate service area, technical error or increase in fuel charges; BWI Shuttle Service will make every effort to notify the Guest of the change.<br>");
                sb.Append("	</td>                    ");
                sb.Append("	</tr>                    ");
                sb.Append("      </tbody></table>    ");
                sb.Append("  </td></tr>              ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("</tbody></table>          ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("</body></html>            ");
                string Mail = sb.ToString();
                #endregion

                #region Driver Mail
                if (DriverName != "Select Driver")
                {
                    #region Driver Mail for Airport reservation
                    sbDriver.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
                    sbDriver.Append("<title>www.bwishuttleservice.com - Customer Receipt</title>  ");
                    sbDriver.Append("<style>                                            ");
                    sbDriver.Append(".Norm {font-family: Verdana;                       ");
                    sbDriver.Append("font-size: 12px;                                 ");
                    sbDriver.Append("font-color: red;                               ");
                    sbDriver.Append(" }                                               ");
                    sbDriver.Append(".heading {font-family: Verdana;                   ");
                    sbDriver.Append("  font-size: 14px;                            ");
                    sbDriver.Append("  font-weight: 800;                           ");
                    sbDriver.Append("	 }                                                  ");
                    sbDriver.Append("   td {font-family: Verdana;                         ");
                    sbDriver.Append("	  font-size: 12px;                                  ");
                    sbDriver.Append("	 }                                                  ");
                    sbDriver.Append("  </style>                                           ");
                    sbDriver.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
                    sbDriver.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
                    sbDriver.Append("");
                    sbDriver.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
                    sbDriver.Append("  <tbody><tr>");
                    sbDriver.Append("    <td width='660' colspan='13' valign='top'>");
                    sbDriver.Append("      <table width='100%'>");
                    sbDriver.Append("        <tbody><tr>");
                    sbDriver.Append("        <tr>");
                    sbDriver.Append("          <td colspan='3' width='100%'><a href='http://www.bwishuttleservice.com'>http://www.bwishuttleservice.com</a></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("        <tr>");
                    sbDriver.Append("          <td colspan='3' width='100%'><a href='mailto:bwiairportshuttleservice@gmail.com'>bwiairportshuttleservice@gmail.com</a></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("        <tr>");
                    sbDriver.Append("          <td colspan='2' width='25%'>Tel:  410-904-5151</td>");
                    sbDriver.Append("          <td width='75%' align='right'><font size='3'>");
                    sbDriver.Append("		Receipt");
                    sbDriver.Append("	     </font></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("      </tbody></table>");
                    sbDriver.Append("     </td>");
                    sbDriver.Append("  </tr><tr>");
                    sbDriver.Append("  </tr><tr>");
                    sbDriver.Append("    <td colspan='13' width='660'>");
                    sbDriver.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                    sbDriver.Append("  	<tbody><tr>");
                    sbDriver.Append("  	  <td width='70%' style='border-bottom:medium solid blue; font-size:16px'><b>Reservation # " + Booking.ReservationID + "</b></td>");
                    Sdate = GetAppDate(Booking.Date.Split(' ')[0]);
                    SerDate = Sdate.ToString("MM-dd-yyyy");
                    sbDriver.Append("  	  <td width='30%' align='right' style='border-bottom:medium solid blue;'>Booking Date: " + SerDate + "</td>");
                    sbDriver.Append("  	</tr>");
                    sbDriver.Append("  	<tr>");
                    sbDriver.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br>Hi, " + Driver.sFirstName + " " + Driver.sLastName + " your Booking has been cancelled with following Details.If any of the information appears to be incorrect, please contact our office immediately to correct it. <br>&nbsp;</td>");
                    sbDriver.Append("	</tr>");
                    sbDriver.Append("      </tbody></table>");
                    sbDriver.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                    sbDriver.Append("	<tbody><tr>");
                    sbDriver.Append("	  <td class='heading'>SUBJ:Rez:</td>");
                    sbDriver.Append("	  <td width='75%'>" + Booking.ReservationDate + "</td>          ");
                    sbDriver.Append("	</tr>");
                    sbDriver.Append("  	<tr>");
                    sbDriver.Append("	  <td class='heading'>MSG: F/T:</td>");
                    string Airlines = Booking.Airlines;
                    if (Booking.Airlines == null)
                    {
                        Airlines = "";
                    }
                    else
                    {
                        Airlines = ", " + Airlines;
                    }
                    if (Booking.Service == "Point To Point Reservation" || Booking.Service == "Hourly Reservation")
                    {
                        if (Booking.P2PTime == null && Booking.Service == "Point To Point Reservation")
                        {
                            sbDriver.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.Pickup_Time) + "" + Airlines + "</td>");
                        }
                        else
                            sbDriver.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.P2PTime) + "" + Airlines + "</td>");
                    }

                    else if (Booking.Service == "To Airport")
                        sbDriver.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.Pickup_Time) + "" + Airlines + "</td>");
                    else
                    {
                        if (Booking.Ret_FlightTime != null)
                            sbDriver.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.Ret_FlightTime) + "" + Airlines + "</td>");
                        else
                            sbDriver.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.FlightTime) + "" + Airlines + "</td>");
                    }
                    sbDriver.Append("	</tr>                                             ");
                    if (Booking.Service != "Point To Point Reservation" && Booking.Service != "Hourly Reservation")
                    {
                        sbDriver.Append("	<tr>                                              ");
                        sbDriver.Append("	  <td class='heading'> Flight Number: </td>          ");
                        sbDriver.Append("	  <td width='75%'>" + Booking.FlightNumber + "</td>          ");
                        sbDriver.Append("	</tr>                                             ");
                    }
                    if (Booking.Service == "Hourly Reservation")
                    {
                        sbDriver.Append("	<tr>                                              ");
                        sbDriver.Append("	  <td class='heading'>Hours:</td>         ");
                        sbDriver.Append("	  <td width='75%'>" + Booking.Hours + "</td>                          ");
                        sbDriver.Append("	</tr>");
                    }
                    sbDriver.Append("	<tr>                                              ");
                    //if (Booking.Service == "To Airport")
                    //{
                    if (Booking.Service == "Point To Point Reservation" || Booking.Service == "Hourly Reservation")
                    {
                        sbDriver.Append("	  <td class='heading'>Service: </td>          ");
                        sbDriver.Append("	  <td width='75%'>" + Booking.Service + "</td>          ");
                    }
                    else
                    {
                        sbDriver.Append("	  <td class='heading'>" + Booking.Service + ": </td>          ");
                        sbDriver.Append("	  <td width='75%'>" + Booking.AirPortName + "</td>          ");
                    }
                    //}
                    //else if (Booking.Service == "From Airport")
                    //{
                    //  sbDriver.Append("	  <td class='heading'>" + Booking.Service + ": " + Booking.PickUpAddress + "</td>          ");
                    //}
                    sbDriver.Append("	</tr>                                             ");
                    sbDriver.Append("	<tr>                                              ");
                    if (Booking.Service == "To Airport" || Booking.Service == "Hourly Reservation")
                    {
                        sbDriver.Append("	  <td class='heading'>P/U: </td>          ");
                        sbDriver.Append("	  <td width='75%'>" + Booking.Source + "</td>               ");
                    }
                    else if (Booking.Service == "From Airport")
                    {
                        sbDriver.Append("	  <td class='heading'>Dest: </td>          ");
                        sbDriver.Append("	  <td width='75%'>" + Booking.Destination + "</td>               ");
                    }
                    else if (Booking.Service == "Point To Point Reservation")
                    {
                        string Location = "", HaltTime = "", Mics = "";

                        sbDriver.Append("	  <td class='heading'>Trip Routing:</td>");
                        sbDriver.Append("");
                        //sbDriver.Append("	  <td class='heading'>&nbsp;</td>");
                        sbDriver.Append("	  <td><table style=\"width:100%\"><thead><tr><th>Location</th><th>Halting Time</th><th>MISC</th></tr></thead><tbody>");
                        if (Booking.Stop1 != "^-:-^")
                        {
                            Location = ""; HaltTime = ""; Mics = "";
                            GetP2PLocation(Booking.Stop1, out Location, out HaltTime, out Mics);
                            sbDriver.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                        }
                        if (Booking.Stop2 != "^-:-^")
                        {
                            Location = ""; HaltTime = ""; Mics = "";
                            GetP2PLocation(Booking.Stop2, out Location, out HaltTime, out Mics);
                            sbDriver.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                        }
                        if (Booking.Stop3 != "^-:-^")
                        {
                            Location = ""; HaltTime = ""; Mics = "";
                            GetP2PLocation(Booking.Stop3, out Location, out HaltTime, out Mics);
                            sbDriver.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                        }
                        if (Booking.Stop4 != "^-:-^")
                        {
                            Location = ""; HaltTime = ""; Mics = "";
                            GetP2PLocation(Booking.Stop4, out Location, out HaltTime, out Mics);
                            sbDriver.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                        }
                        if (Booking.Stop5 != "^-:-^")
                        {
                            Location = ""; HaltTime = ""; Mics = "";
                            GetP2PLocation(Booking.Stop5, out Location, out HaltTime, out Mics);
                            sbDriver.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                            sbDriver.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                        }
                        sbDriver.Append("	</tr>                                             ");
                        sbDriver.Append("</tbody></table></td>");
                    }
                    sbDriver.Append("	</tr>                                             ");
                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'>Name:</td>          ");
                    sbDriver.Append("	  <td width='75%'>" + Booking.FirstName + " " + Booking.LastName + "</td>                      ");
                    sbDriver.Append("	</tr>                                             ");
                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'># :</td>          ");
                    if (Booking.ContactNumber == "" || Booking.ContactNumber == null)
                    {
                        sbDriver.Append("	  <td width='75%'>" + Booking.PhoneNumber + "</td>               ");
                    }
                    else
                    {
                        sbDriver.Append("	  <td width='75%'>" + Booking.ContactNumber + "</td>               ");
                    }
                    sbDriver.Append("	</tr>                                             ");

                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'>Reservation Comments:</td>          ");
                    sbDriver.Append("	  <td width='75%'>" + Booking.Remark + "</td>                      ");
                    sbDriver.Append("	</tr>                                             ");

                    sbDriver.Append("        </tr>                                                                                            ");
                    sbDriver.Append("                          ");

                    sbDriver.Append("      </tbody></table>    ");
                    sbDriver.Append("  </td></tr>              ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("</tbody></table>          ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("                          ");
                    sbDriver.Append("</body></html>            ");
                    #endregion
                }
                Mail = sbDriver.ToString();
                #endregion

                #region Mailing Section

                try
                {
                    MailMessage message = new MailMessage();
                    SmtpClient smtpClient = new SmtpClient();
                    string msg = string.Empty;

                    MailAddress fromAddress = new MailAddress("reservation@bwishuttleservice.com");
                    message.From = fromAddress;
                    message.To.Add(Booking.Email);
                    //message.CC.Add(Driver.sEmail);
                    message.Subject = "Booking Cancelled-" + Booking.ReservationID;
                    message.IsBodyHtml = true;
                    message.Body = sb.ToString();
                    SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                    mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                    mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    mailObj.EnableSsl = false;
                    message.Sender = new MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                    mailObj.Send(message);
                    message.Dispose();
                }
                catch
                {
                    json = "{\"Session\":\"1\",\"Retcode\":\"3\"}";
                    return json;
                }
                if (Driver.sEmail != null)
                {
                    try
                    {
                        MailMessage message = new MailMessage();
                        SmtpClient smtpClient = new SmtpClient();
                        string msg = string.Empty;
                        MailAddress fromAddress = new MailAddress("reservation@bwishuttleservice.com");
                        message.From = fromAddress;
                        message.To.Add(Driver.sEmail);
                        //message.CC.Add(Driver.sEmail);
                        message.Subject = "Booking Cancelled-" + Booking.ReservationID;
                        message.IsBodyHtml = true;
                        message.Body = sb.ToString();
                        SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                        mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                        mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        mailObj.EnableSsl = false;
                        message.Sender = new MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                        mailObj.Send(message);
                        message.Dispose();
                    }
                    catch
                    {
                        json = "{\"Session\":\"1\",\"Retcode\":\"3\"}";
                        return json;
                    }
                }
                #endregion

                json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"Retcode\":\"-1\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string CancelMultiBooking(int[] BookingSid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_Login Driver = new Trans_Tbl_Login();
                for (int i = 0; i < BookingSid.Length; i++)
                {
                    Trans_Reservation Reservation = DB.Trans_Reservations.Single(x => x.sid == BookingSid[i]);
                    string DriverName = Reservation.AssignedTo;
                    var DriverSid = Reservation.DriverSid;
                    if (Reservation.Status == "Cancelled")
                    {
                        json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
                        //json = "{\"Session\":\"1\",\"Retcode\":\"4\"}";
                        return json;
                    }

                    Reservation.Status = "Cancelled";
                    DB.SubmitChanges();

                    if (DriverName != null)
                    {
                        Driver = DB.Trans_Tbl_Logins.Single(x => x.Sid == DriverSid);
                    }

                    Trans_Reservation Booking = DB.Trans_Reservations.Single(x => x.sid == BookingSid[i]);

                    StringBuilder sb = new StringBuilder();
                    StringBuilder sbDriver = new StringBuilder();

                    #region Customer Mail
                    sb.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
                    sb.Append("<title>limoallaround.com - Customer Receipt</title>  ");
                    sb.Append("<style>                                            ");
                    sb.Append(".Norm {font-family: Verdana;                       ");
                    sb.Append("font-size: 12px;                                 ");
                    sb.Append("font-color: red;                               ");
                    sb.Append(" }                                               ");
                    sb.Append(".heading {font-family: Verdana;                   ");
                    sb.Append("  font-size: 14px;                            ");
                    sb.Append("  font-weight: 800;                           ");
                    sb.Append("	 }                                                  ");
                    sb.Append("   td {font-family: Verdana;                         ");
                    sb.Append("	  font-size: 12px;                                  ");
                    sb.Append("	 }                                                  ");
                    sb.Append("  </style>                                           ");
                    sb.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
                    sb.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
                    sb.Append("");
                    sb.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
                    sb.Append("  <tbody><tr>");
                    sb.Append("    <td width='660' colspan='13' valign='top'>");
                    sb.Append("      <table width='100%'>");
                    sb.Append("        <tbody><tr>");
                    sb.Append("          <td colspan='2' width='100%' class='heading'><b>Limo All Around Service</b></td>");
                    sb.Append("        </tr>");
                    sb.Append("        <tr>");
                    sb.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com/'>http://www.bwishuttleservice.com</a></td>");
                    sb.Append("        </tr>");
                    sb.Append("        <tr>");
                    sb.Append("          <td colspan='2' width='100%'><a href='mailto:limoallaround.com@gmail.com'>limoallaround.com@gmail.com</a></td>");
                    sb.Append("        </tr>");
                    sb.Append("        <tr>");
                    sb.Append("          <td colspan='2' width='25%'>Tel: 410-235-2265</td>");
                    sb.Append("          <td width='75%' align='right'><font size='3'>");
                    sb.Append("		Receipt");
                    sb.Append("	     </font></td>");
                    sb.Append("        </tr>");
                    sb.Append("      </tbody></table>");
                    sb.Append("     </td>");
                    sb.Append("  </tr><tr>");
                    sb.Append("  </tr><tr>");
                    sb.Append("    <td colspan='13' width='660'>");
                    sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                    sb.Append("  	<tbody><tr>");
                    sb.Append("  	  <td width='70%' style='border-bottom:medium solid blue; font-size:16px'><b>Reservation # " + Booking.ReservationID + "</b></td>");
                    DateTime Sdate = GetAppDate(Booking.Date.Split(' ')[0]);
                    string SerDate = Sdate.ToString("MM-dd-yyyy");
                    sb.Append("  	  <td width='30%' align='right' style='border-bottom:medium solid blue;'>Booking Date: " + SerDate + "</td>");
                    sb.Append("  	</tr>");
                    sb.Append("  	<tr>");
                    sb.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br>Reservation has been cancelled!  Below please find your Details. If any of the information appears to be incorrect, please contact our office immediately to correct it.<br>&nbsp;</td>");
                    sb.Append("	</tr>");
                    sb.Append("      </tbody></table>");
                    sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                    sb.Append("	<tbody><tr>");
                    sb.Append("	  <td class='heading'>Pick-up Date:</td>");
                    sb.Append("	  <td width='75%'>" + Booking.Date + "</td>                 ");
                    sb.Append("	</tr>                                             ");

                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Driver Name:</td>          ");
                    sb.Append("	  <td width='75%'>" + Driver.sFirstName + " " + Driver.sLastName + "</td>                      ");
                    sb.Append("	</tr>                                             ");

                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Driver Mobile:</td>          ");
                    sb.Append("	  <td width='75%'>" + Driver.sMobile + "</td>                      ");
                    sb.Append("	</tr>                                             ");

                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Driver Email:</td>          ");
                    sb.Append("	  <td width='75%'>" + Driver.sEmail + "</td>                      ");
                    sb.Append("	</tr>                                             ");

                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Pick-up Time:</td>          ");
                    sb.Append("	  <td width='75%'>" + Booking.FlightTime + "</td>                      ");
                    sb.Append("	</tr>                                             ");

                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Primary Contact:</td>       ");
                    sb.Append("	  <td width='75%'>" + Booking.FirstName + " " + Booking.LastName + "</td>           ");
                    sb.Append("	</tr>                                             ");
                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>Contact Number:</td>        ");
                    sb.Append("	  <td width='75%'>" + Booking.ContactNumber + "</td>               ");
                    sb.Append("	</tr>                                             ");
                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>No. of Adults:</td>         ");
                    sb.Append("	  <td width='75%'>" + Booking.Adult + "</td>                          ");
                    sb.Append("	</tr>                                             ");
                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>No. of Children:</td>       ");
                    sb.Append("	  <td width='75%'>" + Booking.Child + "</td>                          ");
                    sb.Append("	</tr>	                                          ");
                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading' valign='Top' style='border-bottom:thin solid green;'>Vehicle Type:</td>           ");
                    sb.Append("	  <td width='75%' style='border-bottom:thin solid green;'>                                              ");
                    sb.Append("	  " + Booking.VehicalType + "<br>&nbsp;</td>                                                                                    ");
                    sb.Append("	</tr>                                                                                                   ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading'>Trip Routing Information:</td>                                                    ");
                    sb.Append("	                                                                                                        ");
                    sb.Append("	  <td><b>Pick-up Location: &nbsp;</b>" + Booking.PickUpAddress + "</td>                         ");
                    sb.Append("	                                                                                                        ");
                    sb.Append("	</tr>                                                                                                   ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                    sb.Append("	  <td><b>Pick-up Instructions: &nbsp;</b>your reservation is canceled by xx/xx/2016<br>&nbsp;</td>      ");
                    sb.Append("	</tr>	                                                                                                ");
                    sb.Append("                                                                                                         ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                    sb.Append("	                                                                                                        ");
                    sb.Append("	  <td><b>Drop-off Location: &nbsp;</b>" + Booking.DropAddress + "</td>                                                  ");
                    sb.Append("	                                                                                                        ");
                    sb.Append("	</tr>	                                                                                                ");
                    sb.Append("        <tr>                                                                                             ");
                    sb.Append("          <td style='border-bottom:thin solid green;' colspan='2'>&nbsp;</td>                            ");
                    sb.Append("        </tr>                                                                                            ");
                    sb.Append("                                                                                                         ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading' valign='top'>Payment Method:</td>                                                 ");
                    sb.Append("	  <td width='75%'>" + Booking.CCPayment + "<br>&nbsp;</td>                                                            ");
                    sb.Append("	</tr>                                                                                                   ");
                    sb.Append("                                                                                                         ");
                    sb.Append("                                                                                                         ");
                    sb.Append(" 	<tr>                                                                                                ");
                    sb.Append("	  <td class='heading'>Quoted Rate:</td>                                                                 ");
                    sb.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(Booking.Fare), 2) + "                                                                                         ");
                    sb.Append("	</td></tr>                                                                                              ");
                    sb.Append("                                                                                                         ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading'>Gratuity:</td>                                                                    ");
                    sb.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(Booking.Gratuity), 2) + "                                                                                          ");
                    sb.Append("	</td></tr>                                                                                              ");
                    sb.Append("                                                                                                         ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading'>Reservation Total:</td>                                                           ");
                    sb.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(Booking.TotalFare), 2) + "                                                                                         ");
                    sb.Append("	</td></tr>                                                                                              ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td style='border-bottom:thin solid green;' class='heading' valign='top'><font color='red'>Total Due:</font></td>");
                    sb.Append("	  <td style='border-bottom:thin solid green;'><font color='red'>$ 0.00<br>&nbsp;</font></td>     ");
                    sb.Append("	</tr>                                                                                              ");
                    sb.Append("	<tr>                                                                                               ");
                    sb.Append("	  <td class='heading' valign='top'>Terms &amp; Conditions / Reservation Agreement:</td>            ");
                    sb.Append("	  <td>Guest agrees that there will be no smoking in our vehicles.<br>                              ");
                    sb.Append("		Guest assures that no illegal drugs are brought into our vehicles.<br>                         ");
                    sb.Append("		Guest agrees that no alcoholic beverages shall be brought into our vehicles.                   ");
                    sb.Append("		Guest agrees that the passengers capacity of vehicle provided shall not be exceeded.<br>       ");
                    sb.Append("		In case of misconduct by your party, chauffeur has the right to terminate this agreement without refunds.<br>");
                    sb.Append("		Guest holds Limo All Around Service harmless and not liable for any personal or material damages arising from the conduct of his/her party.<br>");
                    sb.Append("		Guest is responsible for the full payment of any overtime charges, beyond the original agreement.<br>");
                    sb.Append("		Guest agrees to 100% payment charged to his/her credit card at the time reservation has been made.<br>");
                    sb.Append("		Luxury Sedan guests can cancel 3 hours before pickup time with $10.00 processing fee; if not you will be charged the full amount of reservation. Stretch limousine and Van Guests can cancel 48 hours before pickup time with a $20.00 processing fee; if you cancel after 48 hours, you will be billed for a minimum 3 hours of service charges.<br>");
                    sb.Append("		Guest acknowledges that he/she understands that Limo All Around Service imposes a service fee for late night service (11:00PM TO 5:00AM).<br>");
                    sb.Append("		Guest ackowledges that he/she understands that Limo All Around Service imposes an additional service fee for Incoming International flights.<br>		");
                    sb.Append("		Limo All Around Service cannot be held responsible for mechanical problems, inclement weather, or other uncontrollable circumstances resulting in the inability to start a job as it is schedule time or complete a job. In the event that the requested vehicle can not be provided; Limo All Around Service may provide a vehicle of equal quality.<br>");
                    sb.Append("		Limo All Around Service reserves the right to change rates without notice.  If a rate change is necessary due to a destination being outside of the immediate service area, technical error or increase in fuel charges; Limo All Around Service will make every effort to notify the Guest of the change.<br>");
                    sb.Append("	</td>                    ");
                    sb.Append("	</tr>                    ");
                    sb.Append("      </tbody></table>    ");
                    sb.Append("  </td></tr>              ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("</tbody></table>          ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("</body></html>            ");

                    #endregion

                    #region Driver Mail
                    sbDriver.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
                    sbDriver.Append("<title>limoallaround.com - Customer Receipt</title>  ");
                    sbDriver.Append("<style>                                            ");
                    sbDriver.Append(".Norm {font-family: Verdana;                       ");
                    sbDriver.Append("font-size: 12px;                                 ");
                    sbDriver.Append("font-color: red;                               ");
                    sbDriver.Append(" }                                               ");
                    sbDriver.Append(".heading {font-family: Verdana;                   ");
                    sbDriver.Append("  font-size: 14px;                            ");
                    sbDriver.Append("  font-weight: 800;                           ");
                    sbDriver.Append("	 }                                                  ");
                    sbDriver.Append("   td {font-family: Verdana;                         ");
                    sbDriver.Append("	  font-size: 12px;                                  ");
                    sbDriver.Append("	 }                                                  ");
                    sbDriver.Append("  </style>                                           ");
                    sbDriver.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
                    sbDriver.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
                    sbDriver.Append("");
                    sbDriver.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
                    sbDriver.Append("  <tbody><tr>");
                    sbDriver.Append("    <td width='660' colspan='13' valign='top'>");
                    sbDriver.Append("      <table width='100%'>");
                    sbDriver.Append("        <tbody><tr>");
                    sbDriver.Append("          <td colspan='2' width='100%' class='heading'><b>Limo All Around Service</b></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("        <tr>");
                    sbDriver.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com/'>http://www.bwishuttleservice.com</a></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("        <tr>");
                    sbDriver.Append("          <td colspan='2' width='100%'><a href='mailto:limoallaround.com@gmail.com'>limoallaround.com@gmail.com</a></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("        <tr>");
                    sbDriver.Append("          <td colspan='2' width='25%'>Tel: 410-235-2265</td>");
                    sbDriver.Append("          <td width='75%' align='right'><font size='3'>");
                    sbDriver.Append("		Receipt");
                    sbDriver.Append("	     </font></td>");
                    sbDriver.Append("        </tr>");
                    sbDriver.Append("      </tbody></table>");
                    sbDriver.Append("     </td>");
                    sbDriver.Append("  </tr><tr>");
                    sbDriver.Append("  </tr><tr>");
                    sbDriver.Append("    <td colspan='13' width='660'>");
                    sbDriver.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                    sbDriver.Append("  	<tbody><tr>");
                    sbDriver.Append("  	  <td width='70%' style='border-bottom:medium solid blue; font-size:16px'><b>Reservation # " + Booking.ReservationID + "</b></td>");
                    Sdate = GetAppDate(Booking.Date.Split(' ')[0]);
                    SerDate = Sdate.ToString("MM-dd-yyyy");
                    sbDriver.Append("  	  <td width='30%' align='right' style='border-bottom:medium solid blue;'>Booking Date: " + SerDate + "</td>");
                    sbDriver.Append("  	</tr>");
                    sbDriver.Append("  	<tr>");
                    sbDriver.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br>Hi, " + Driver.sFirstName + " " + Driver.sLastName + " your booking with following detail has been cancelled.If any of the information appears to be incorrect, please contact our office immediately to correct it. <br>&nbsp;</td>");
                    sbDriver.Append("	</tr>");
                    sbDriver.Append("      </tbody></table>");
                    sbDriver.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                    sbDriver.Append("	<tbody><tr>");
                    sbDriver.Append("	  <td class='heading'>Pick-up Date:</td>");
                    sbDriver.Append("	  <td width='75%'>" + Booking.Date + "</td>                 ");
                    sbDriver.Append("	</tr>                                             ");

                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'>Customer Name:</td>          ");
                    sbDriver.Append("	  <td width='75%'>" + Booking.FirstName + " " + Booking.LastName + "</td>                      ");
                    sbDriver.Append("	</tr>                                             ");

                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'>Customer Mobile:</td>          ");
                    sbDriver.Append("	  <td width='75%'>" + Booking.ContactNumber + "</td>                      ");
                    sbDriver.Append("	</tr>                                             ");

                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'>Customer Email:</td>          ");
                    sbDriver.Append("	  <td width='75%'>" + Booking.Email + "</td>                      ");
                    sbDriver.Append("	</tr>                                             ");

                    sbDriver.Append("	<tr>                                              ");
                    sbDriver.Append("	  <td class='heading'>Pick-up Time:</td>          ");
                    sbDriver.Append("	  <td width='75%'>" + Booking.FlightTime + "</td>                      ");
                    sbDriver.Append("	</tr>                                             ");

                    sb.Append("	  <td class='heading'>No. of Adults:</td>         ");
                    sb.Append("	  <td width='75%'>" + Booking.Adult + "</td>                          ");
                    sb.Append("	</tr>                                             ");
                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading'>No. of Children:</td>       ");
                    sb.Append("	  <td width='75%'>" + Booking.Child + "</td>                          ");
                    sb.Append("	</tr>	                                          ");
                    sb.Append("	<tr>                                              ");
                    sb.Append("	  <td class='heading' valign='Top' style='border-bottom:thin solid green;'>Vehicle Type:</td>           ");
                    sb.Append("	  <td width='75%' style='border-bottom:thin solid green;'>                                              ");
                    sb.Append("	  " + Booking.VehicalType + "<br>&nbsp;</td>                                                                                    ");
                    sb.Append("	</tr>                                                                                                   ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading'>Trip Routing Information:</td>                                                    ");
                    sb.Append("	                                                                                                        ");
                    sb.Append("	  <td><b>Pick-up Location: &nbsp;</b>" + Booking.PickUpAddress + "</td>                         ");
                    sb.Append("	                                                                                                        ");
                    sb.Append("	</tr>                                                                                                   ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                    sb.Append("	  <td><b>Pick-up Instructions: &nbsp;</b>your reservation is canceled by xx/xx/2016<br>&nbsp;</td>      ");
                    sb.Append("	</tr>	                                                                                                ");
                    sb.Append("                                                                                                         ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                    sb.Append("	                                                                                                        ");
                    sb.Append("	  <td><b>Drop-off Location: &nbsp;</b>" + Booking.DropAddress + "</td>                                                  ");
                    sb.Append("	                                                                                                        ");
                    sb.Append("	</tr>	                                                                                                ");
                    sb.Append("        <tr>                                                                                             ");
                    sb.Append("          <td style='border-bottom:thin solid green;' colspan='2'>&nbsp;</td>                            ");
                    sb.Append("        </tr>                                                                                            ");
                    sb.Append("                                                                                                         ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading' valign='top'>Payment Method:</td>                                                 ");
                    sb.Append("	  <td width='75%'>" + Booking.CCPayment + "<br>&nbsp;</td>                                                            ");
                    sb.Append("	</tr>                                                                                                   ");
                    sb.Append("                                                                                                         ");
                    sb.Append("                                                                                                         ");
                    sb.Append(" 	<tr>                                                                                                ");
                    sb.Append("	  <td class='heading'>Quoted Rate:</td>                                                                 ");
                    sb.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(Booking.Fare), 2) + "                                                                                         ");
                    sb.Append("	</td></tr>                                                                                              ");
                    sb.Append("                                                                                                         ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading'>Gratuity:</td>                                                                    ");
                    sb.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(Booking.Gratuity), 2) + "                                                                                          ");
                    sb.Append("	</td></tr>                                                                                              ");
                    sb.Append("                                                                                                         ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td class='heading'>Reservation Total:</td>                                                           ");
                    sb.Append("	  <td>$ " + decimal.Round(Convert.ToDecimal(Booking.TotalFare), 2) + "                                                                                         ");
                    sb.Append("	</td></tr>                                                                                              ");
                    sb.Append("	<tr>                                                                                                    ");
                    sb.Append("	  <td style='border-bottom:thin solid green;' class='heading' valign='top'><font color='red'>Total Due:</font></td>");
                    sb.Append("	  <td style='border-bottom:thin solid green;'><font color='red'>$ 0.00<br>&nbsp;</font></td>     ");
                    sb.Append("	</tr>                                                                                              ");
                    sb.Append("	<tr>                                                                                               ");
                    sb.Append("	  <td class='heading' valign='top'>Terms &amp; Conditions / Reservation Agreement:</td>            ");
                    sb.Append("	  <td>Guest agrees that there will be no smoking in our vehicles.<br>                              ");
                    sb.Append("		Guest assures that no illegal drugs are brought into our vehicles.<br>                         ");
                    sb.Append("		Guest agrees that no alcoholic beverages shall be brought into our vehicles.                   ");
                    sb.Append("		Guest agrees that the passengers capacity of vehicle provided shall not be exceeded.<br>       ");
                    sb.Append("		In case of misconduct by your party, chauffeur has the right to terminate this agreement without refunds.<br>");
                    sb.Append("		Guest holds Limo All Around Service harmless and not liable for any personal or material damages arising from the conduct of his/her party.<br>");
                    sb.Append("		Guest is responsible for the full payment of any overtime charges, beyond the original agreement.<br>");
                    sb.Append("		Guest agrees to 100% payment charged to his/her credit card at the time reservation has been made.<br>");
                    sb.Append("		Luxury Sedan guests can cancel 3 hours before pickup time with $10.00 processing fee; if not you will be charged the full amount of reservation. Stretch limousine and Van Guests can cancel 48 hours before pickup time with a $20.00 processing fee; if you cancel after 48 hours, you will be billed for a minimum 3 hours of service charges.<br>");
                    sb.Append("		Guest acknowledges that he/she understands that Limo All Around Service imposes a service fee for late night service (11:00PM TO 5:00AM).<br>");
                    sb.Append("		Guest ackowledges that he/she understands that Limo All Around Service imposes an additional service fee for Incoming International flights.<br>		");
                    sb.Append("		Limo All Around Service cannot be held responsible for mechanical problems, inclement weather, or other uncontrollable circumstances resulting in the inability to start a job as it is schedule time or complete a job. In the event that the requested vehicle can not be provided; Limo All Around Service may provide a vehicle of equal quality.<br>");
                    sb.Append("		Limo All Around Service reserves the right to change rates without notice.  If a rate change is necessary due to a destination being outside of the immediate service area, technical error or increase in fuel charges; Limo All Around Service will make every effort to notify the Guest of the change.<br>");
                    sb.Append("	</td>                    ");
                    sb.Append("	</tr>                    ");
                    sb.Append("      </tbody></table>    ");
                    sb.Append("  </td></tr>              ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("</tbody></table>          ");
                    sb.Append("                          ");
                    sb.Append("                          ");
                    sb.Append("</body></html>            ");

                    #endregion

                    #region Mailing Section

                    try
                    {
                        MailMessage message = new MailMessage();
                        SmtpClient smtpClient = new SmtpClient();
                        string msg = string.Empty;
                        MailAddress fromAddress = new MailAddress("support@limoallaround.net", "Limo All Around Reservation");
                        message.From = fromAddress;
                        message.To.Add(Booking.Email);
                        message.Subject = "Booking Cancelled-" + Booking.ReservationID;
                        message.IsBodyHtml = true;
                        message.Body = sb.ToString();
                        smtpClient.Host = "relay-hosting.secureserver.net";   //-- Donot change.
                        smtpClient.Port = 25; //--- Donot change
                        smtpClient.EnableSsl = false;//--- Donot change
                        smtpClient.UseDefaultCredentials = true;
                        smtpClient.Credentials = new System.Net.NetworkCredential("support@limoallaround.net", "asdrc@123");
                        smtpClient.Send(message);

                    }
                    catch
                    {
                        json = "{\"Session\":\"1\",\"Retcode\":\"3\"}";
                        return json;
                    }
                    var a = Driver.sEmail;
                    if (a != null)
                    {
                        int al = 10;
                    }
                    if (Driver.sEmail != null)
                    {
                        try
                        {
                            MailMessage message = new MailMessage();
                            SmtpClient smtpClient = new SmtpClient();
                            string msg = string.Empty;
                            MailAddress fromAddress = new MailAddress("support@limoallaround.net", "Limo All Around Reservation");
                            message.From = fromAddress;
                            message.To.Add(Driver.sEmail);
                            message.Subject = "Booking Cancelled-" + Booking.ReservationID;
                            message.IsBodyHtml = true;
                            message.Body = sbDriver.ToString();
                            smtpClient.Host = "relay-hosting.secureserver.net";   //-- Donot change.
                            smtpClient.Port = 25; //--- Donot change
                            smtpClient.EnableSsl = false;//--- Donot change
                            smtpClient.UseDefaultCredentials = true;
                            smtpClient.Credentials = new System.Net.NetworkCredential("support@limoallaround.net", "asdrc@123");
                            smtpClient.Send(message);
                        }
                        catch
                        {
                            json = "{\"Session\":\"1\",\"Retcode\":\"3\"}";
                            return json;
                        }
                    }
                    #endregion

                }


                json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"Retcode\":\"-1\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteBooking(int BookingSid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Reservation Booking = DB.Trans_Reservations.Single(x => x.sid == BookingSid);
                Booking.Status = "Deleted";
                // DB.Trans_Reservations.DeleteOnSubmit(Booking);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"Retcode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteMultipuleBooking(int[] BookingSid)
        {
            try
            {
                for (int i = 0; i < BookingSid.Length; i++)
                {
                    DBHandlerDataContext DB = new DBHandlerDataContext();
                    Trans_Reservation Booking = DB.Trans_Reservations.Single(x => x.sid == BookingSid[i]);
                    Booking.Status = "Deleted";
                    // DB.Trans_Reservations.DeleteOnSubmit(Booking);
                    DB.SubmitChanges();
                }

                json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"Retcode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string CompleteBooking(int BookingSid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Reservation Booking = DB.Trans_Reservations.Single(x => x.sid == BookingSid);
                string Paid = Booking.Paid.ToString();
                string Service = Booking.Service;

                DateTime Today = DateTime.Today;
                //string TodayDate = Today.ToString("MM-dd-yyyy");
                string TodayDate = Today.ToString("dd-MM-yyyy");
                DateTime todays = GetAppDate(TodayDate);
                DateTime ReservationDate = DateTime.ParseExact(Booking.ReservationDate, "MM-dd-yyyy", null);

                string Times = DateTime.Now.ToString("hh:mm");
                //Times = Times.ToString("HH:mm:ss");
                //string sTime = Times.ToShortTimeString();
                //string a = sTime.ToString("HH:mm:ss");
                string[] Splitter = Times.Split(':');
                Int64 Hours = Convert.ToInt64(Splitter[0]);
                Int64 Min = Convert.ToInt64(Splitter[1]);
                if (Booking.Status == "Completed")
                {
                    json = "{\"Session\":\"1\",\"Retcode\":\"2\"}";
                }
                else if (Booking.AssignedTo == "" || Booking.AssignedTo == null)
                {
                    json = "{\"Session\":\"1\",\"Retcode\":\"3\"}";
                }
                else if (Paid == "False")
                {
                    json = "{\"Session\":\"1\",\"Retcode\":\"4\"}";
                }
                else if (ReservationDate > todays)
                {
                    json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                }
                else if (ReservationDate == todays)
                {
                    if (Service == "From Airport")
                    {
                        string BookingTime = Booking.FlightTime;
                        string[] Splitter1 = BookingTime.Split(':');
                        Int64 bHours = Convert.ToInt64(Splitter1[0]);
                        Int64 bMin = Convert.ToInt64(Splitter1[1]);
                        if (bHours > Hours)
                        {
                            json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                        }
                        else if (bHours == Hours)
                        {
                            if (bMin > Min)
                            {
                                json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                            }
                        }
                        else
                        {
                            Booking.Status = "Completed";
                            DB.SubmitChanges();
                            json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
                        }
                    }
                    else if (Service == "To Airport")
                    {
                        string BookingTime = Booking.Pickup_Time;
                        string[] Splitter1 = BookingTime.Split(':');
                        Int64 bHours = Convert.ToInt64(Splitter1[0]);
                        Int64 bMin = Convert.ToInt64(Splitter1[1]);
                        if (bHours > Hours)
                        {
                            json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                        }
                        else if (bHours == Hours)
                        {
                            if (bMin > Min)
                            {
                                json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                            }
                        }
                        else
                        {
                            Booking.Status = "Completed";
                            DB.SubmitChanges();
                            json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
                        }
                    }
                    else if (Service == "Point To Point Reservation" || Service == "Hourly Reservation")
                    {
                        string BookingTime = Booking.P2PTime;
                        string[] Splitter1 = BookingTime.Split(':');
                        Int64 bHours = Convert.ToInt64(Splitter1[0]);
                        Int64 bMin = Convert.ToInt64(Splitter1[1]);
                        if (bHours > Hours)
                        {
                            json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                        }
                        else if (bHours == Hours)
                        {
                            if (bMin > Min)
                            {
                                json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                            }
                        }
                        else
                        {
                            Booking.Status = "Completed";
                            DB.SubmitChanges();
                            json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
                        }
                    }
                }

                else
                {
                    Booking.Status = "Completed";
                    DB.SubmitChanges();
                    json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"Retcode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string mCompleteBooking(int[] BookingSid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                for (int i = 0; i < BookingSid.Length; i++)
                {

                    Trans_Reservation Booking = DB.Trans_Reservations.Single(x => x.sid == BookingSid[i]);
                    string Paid = Booking.Paid.ToString();
                    string Service = Booking.Service;

                    DateTime Today = DateTime.Today;
                    //string TodayDate = Today.ToString("MM-dd-yyyy");
                    string TodayDate = Today.ToString("dd-MM-yyyy");
                    DateTime todays = GetAppDate(TodayDate);
                    DateTime ReservationDate = DateTime.ParseExact(Booking.ReservationDate, "MM-dd-yyyy", null);

                    string Times = DateTime.Now.ToString("hh:mm");
                    //Times = Times.ToString("HH:mm:ss");
                    //string sTime = Times.ToShortTimeString();
                    //string a = sTime.ToString("HH:mm:ss");
                    string[] Splitter = Times.Split(':');
                    Int64 Hours = Convert.ToInt64(Splitter[0]);
                    Int64 Min = Convert.ToInt64(Splitter[1]);
                    if (Booking.Status == "Completed")
                    {
                        json = "{\"Session\":\"1\",\"Retcode\":\"2\"}";
                    }
                    else if (Booking.AssignedTo == "" || Booking.AssignedTo == null)
                    {
                        json = "{\"Session\":\"1\",\"Retcode\":\"3\"}";
                    }
                    else if (Paid == "False")
                    {
                        json = "{\"Session\":\"1\",\"Retcode\":\"4\"}";
                    }
                    else if (ReservationDate > todays)
                    {
                        json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                    }
                    else if (ReservationDate == todays)
                    {
                        if (Service == "From Airport")
                        {
                            string BookingTime = Booking.FlightTime;
                            string[] Splitter1 = BookingTime.Split(':');
                            Int64 bHours = Convert.ToInt64(Splitter1[0]);
                            Int64 bMin = Convert.ToInt64(Splitter1[1]);
                            if (bHours > Hours)
                            {
                                json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                            }
                            else if (bHours == Hours)
                            {
                                if (bMin > Min)
                                {
                                    json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                                }
                            }
                            else
                            {
                                Booking.Status = "Completed";
                                DB.SubmitChanges();
                                json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
                            }
                        }
                        else if (Service == "To Airport")
                        {
                            string BookingTime = Booking.Pickup_Time;
                            string[] Splitter1 = BookingTime.Split(':');
                            Int64 bHours = Convert.ToInt64(Splitter1[0]);
                            Int64 bMin = Convert.ToInt64(Splitter1[1]);
                            if (bHours > Hours)
                            {
                                json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                            }
                            else if (bHours == Hours)
                            {
                                if (bMin > Min)
                                {
                                    json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                                }
                            }
                            else
                            {
                                Booking.Status = "Completed";
                                DB.SubmitChanges();
                                json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
                            }
                        }
                        else if (Service == "Point To Point Reservation" || Service == "Hourly Reservation")
                        {
                            string BookingTime = Booking.P2PTime;
                            string[] Splitter1 = BookingTime.Split(':');
                            Int64 bHours = Convert.ToInt64(Splitter1[0]);
                            Int64 bMin = Convert.ToInt64(Splitter1[1]);
                            if (bHours > Hours)
                            {
                                json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                            }
                            else if (bHours == Hours)
                            {
                                if (bMin > Min)
                                {
                                    json = "{\"Session\":\"1\",\"Retcode\":\"5\"}";
                                }
                            }
                            else
                            {
                                Booking.Status = "Completed";
                                DB.SubmitChanges();
                                json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
                            }
                        }
                    }

                    else
                    {
                        Booking.Status = "Completed";
                        DB.SubmitChanges();
                        json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
                    }
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"Retcode\":\"-1\"}";
            }
            return json;
        }

        #endregion

        [WebMethod(EnableSession = true)]
        public string GetAllServices()
        {
            //DateTime dt = DateTime.Now.Date;
            //string ds = dt.ToString("MM-dd-yyyy");
            List<BWI.Trans_Tbl_ServiceType> objST = null;

            DBHandlerDataContext DB = new DBHandlerDataContext();
            var query = from objLocation in DB.Trans_Tbl_Locations where objLocation.Status == "Activate" orderby objLocation.LocationName select objLocation;
            List<BWI.Trans_Tbl_Location> LocTable = query.ToList();
            string jsonlocation = jsSerializer.Serialize(LocTable);

            retCode = RequestBooking.GetAllDriver(out objST);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                jsonlocation = jsonlocation.TrimEnd(']');
                jsonlocation = jsonlocation.TrimStart('[');

                json = jsSerializer.Serialize(objST);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"ServiceType\":[" + json + "],\"Location\":[" + jsonlocation + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAllDriver()
        {
            List<BWI.Trans_Tbl_Login> List = null;
            retCode = ReservationManager.GetAllDriver(out List);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(List);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"tblDriver\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateData(int BookingSid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                List<BWI.Trans_Reservation> Data = null;
                Data = (from obj in DB.Trans_Reservations where obj.sid == BookingSid select obj).ToList();

                if (Data.Count > 0)
                {
                    json = jsSerializer.Serialize(Data);
                    json = json.TrimEnd(']');
                    json = json.TrimStart('[');
                    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
                }
                else
                {
                    json = "{\"Session\":\"1\",\"Retcode\":\"0\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"Retcode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAllAirPort()
        {
            List<BWI.Trans_Tbl_Login> List = null;

            DBHandlerDataContext DB = new DBHandlerDataContext();

            var Query = (from Obj in DB.Trans_Tbl_Locations select Obj).ToList();

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(Query);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"tblDriver\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UndoStatus(int BookingSid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Reservation Booking = DB.Trans_Reservations.Single(x => x.sid == BookingSid);
                Booking.Status = "Requested";
                // DB.Trans_Reservations.DeleteOnSubmit(Booking);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"Retcode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAllEmail()
        {
            List<BWI.Trans_Tbl_CustomerMaster> List = null;

            DBHandlerDataContext DB = new DBHandlerDataContext();

            List = (from obj in DB.Trans_Tbl_CustomerMasters select obj).ToList();

            string jsonlocation = jsSerializer.Serialize(List);

            if (List.Count > 0)
            {
                //jsonlocation = jsonlocation.TrimEnd(']');
                //jsonlocation = jsonlocation.TrimStart('[');

                //json = jsSerializer.Serialize(objST);
                //json = json.TrimEnd(']');
                //json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"CustomerList\":[" + jsonlocation + "]}";
            }
            else if (List.Count == 0)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string VehicleNameDropDown()
        {
            List<BWI.Trans_tbl_Vehicle_Info> List = null;
            retCode = DistanceEstimateManager.ModelNameDropDown(out List);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(List);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"VehicleName\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAllCustPhoneNo()
        {
            List<BWI.Trans_Tbl_CustomerMaster> List = null;

            DBHandlerDataContext DB = new DBHandlerDataContext();

            List = (from obj in DB.Trans_Tbl_CustomerMasters select obj).ToList();

            string jsonlocation = jsSerializer.Serialize(List);

            if (List.Count > 0)
            {
                //jsonlocation = jsonlocation.TrimEnd(']');
                //jsonlocation = jsonlocation.TrimStart('[');

                //json = jsSerializer.Serialize(objST);
                //json = json.TrimEnd(']');
                //json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"CustomerList\":[" + jsonlocation + "]}";
            }
            else if (List.Count == 0)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }
            

        [WebMethod(EnableSession = true)]
        public string GetCustById(Int64 CustId)
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();
            try
            {
                var List = (from Obj in DB.Trans_Tbl_CustomerMasters where Obj.sid == CustId select Obj).ToList();
                if (List.Count > 0)
                {
                    json = jsSerializer.Serialize(List);
                    json = json.TrimEnd(']');
                    json = json.TrimStart('[');
                    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAllOfferCode()
        {
            List<BWI.Trans_Tbl_Offer> List = null;

            DBHandlerDataContext DB = new DBHandlerDataContext();

            List = (from obj in DB.Trans_Tbl_Offers select obj).ToList();

            if (List.Count > 0)
            {
                json = jsSerializer.Serialize(new { Session = 1, retCode = 1, OfferList = List });
            }
            else if (List.Count == 0)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        #region Date Converter
        public static DateTime GetAppDate(string Date)
        {

            // string Date = Row["AppliedDate"].ToString();
            DateTime date = new DateTime();
            Date = (Date.Split(' ')[0]).Replace("/", "-");
            try
            {
                string[] formats = {"M/d/yyyy", "MM/dd/yyyy",
                                "d/M/yyyy", "dd/MM/yyyy",
                                "yyyy/M/d", "yyyy/MM/dd",
                                "M-d-yyyy", "MM-dd-yyyy",
                                "d-M-yyyy", "dd-MM-yyyy",
                                "yyyy-M-d", "yyyy-MM-dd",
                                "M.d.yyyy", "MM.dd.yyyy",
                                "d.M.yyyy", "dd.MM.yyyy",
                                "yyyy.M.d", "yyyy.MM.dd",
                                "M,d,yyyy", "MM,dd,yyyy",
                                "d,M,yyyy", "dd,MM,yyyy",
                                "yyyy,M,d", "yyyy,MM,dd",
                                "M d yyyy", "MM dd yyyy",
                                "d M yyyy", "dd MM yyyy",
                                "yyyy M d", "yyyy MM dd",
                                "m/dd/yyyy hh:mm:ss tt",
                               };
                //DateTime dateValue;

                foreach (string dateStringFormat in formats)
                {
                    if (DateTime.TryParseExact(Date, dateStringFormat, CultureInfo.CurrentCulture, DateTimeStyles.None,
                                               out date))
                    {
                        date.ToShortDateString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;
        }

        public static void GetP2PLocation(string Source, out string Locations, out string HaltTime, out string Mics)
        {
            Locations = ""; HaltTime = ""; Mics = "";
            string[] s = Regex.Split(Source, @"\^");
            if (s.Length == 3)
            {
                Locations = s[0];
                // HaltTime = GetTime(s[1]);
                HaltTime = s[1];
                Mics = s[2];
            }
            else if (s.Length == 2)
            {
                Locations = s[0];
                HaltTime = s[1];
            }
        }

        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                // string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;

                string CurrentPattern = "m-d-yy";
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;
        }
        
        #endregion

        #region Time
        public static string GetTime(string Time)
        {
            try
            {
                string[] s = null;
                if (Time.Contains("AM") || Time.Contains("PM"))
                { }
                else
                {
                    s = Regex.Split(Time, ":");

                    if (Convert.ToInt16(s[0]) > 12)
                    {
                        Int64 val = Convert.ToInt16(s[0]) - 12;
                        Time = val.ToString() + ":" + s[1] + ":PM";
                    }
                    else if (Convert.ToInt16(s[0]) == 0)
                    {
                        Time = "12:" + s[1] + ":AM";
                    }
                    else
                    {
                        Time = s[0] + ":" + s[1] + ":AM";
                    }
                }
            }
            catch
            { }
            return Time;
        }
        #endregion
    }
}
