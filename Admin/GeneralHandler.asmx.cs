﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BWI.BL;
using System.Web.Script.Serialization;
using BWI.DataLayer;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for GeneralHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class GeneralHandler : System.Web.Services.WebService
    {
        string json = "";
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        #region CountryCityCode

        [WebMethod(EnableSession = true)]
        public string GetServiceType()
        {
            List<BWI.Proc_Trans_GetServiceTypeResult> ServiceType = null;
            retCode = ServiceTypeManager.GetServiceType(out ServiceType);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(ServiceType);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }


        //[WebMethod(EnableSession = true)]
        //public string GetCountry()
        //{
        //    string jsonString = "";
        //    DataTable dtResult;
        //    DBHelper.DBReturnCode retcode = CountryCityCodeManager.GetCountry(out dtResult);
        //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        jsonString = "";
        //        foreach (DataRow dr in dtResult.Rows)
        //        {
        //            jsonString += "{";
        //            foreach (DataColumn dc in dtResult.Columns)
        //            {
        //                jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
        //            }
        //            jsonString = jsonString.Trim(',') + "},";
        //        }
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"Country\":[" + jsonString.Trim(',') + "]}";
        //        dtResult.Dispose();
        //    }
        //    else
        //    {
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}

        //[WebMethod(EnableSession = true)]
        //public string GetCity(string country)
        //{
        //    string jsonString = "";
        //    DataTable dtResult;
        //    DBHelper.DBReturnCode retcode = CountryCityCodeManager.GetCity(country, out dtResult);
        //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        jsonString = "";
        //        foreach (DataRow dr in dtResult.Rows)
        //        {
        //            jsonString += "{";
        //            foreach (DataColumn dc in dtResult.Columns)
        //            {
        //                jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
        //            }
        //            jsonString = jsonString.Trim(',') + "},";
        //        }
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"City\":[" + jsonString.Trim(',') + "]}";
        //        dtResult.Dispose();
        //    }
        //    else
        //    {
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}

        //[WebMethod(EnableSession = true)]
        //public string GetCityCode(string Description)
        //{
        //    string jsonString = "";
        //    DataTable dtResult;
        //    DBHelper.DBReturnCode retcode = CountryCityCodeManager.GetCityCode(Description, out dtResult);
        //    if (retcode == DBHelper.DBReturnCode.SUCCESS)
        //    {
        //        jsonString = "";
        //        foreach (DataRow dr in dtResult.Rows)
        //        {
        //            jsonString += "{";
        //            foreach (DataColumn dc in dtResult.Columns)
        //            {
        //                jsonString += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
        //            }
        //            jsonString = jsonString.Trim(',') + "},";
        //        }
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"1\",\"CityCode\":[" + jsonString.Trim(',') + "]}";
        //        dtResult.Dispose();
        //    }
        //    else
        //    {
        //        jsonString = "{\"Session\":\"1\",\"retCode\":\"0\"}";
        //    }
        //    return jsonString;
        //}

        #endregion
    }
}
