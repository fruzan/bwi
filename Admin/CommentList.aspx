﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="CommentList.aspx.cs" Inherits="BWI.Admin.CommentList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="Scripts/Comments.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <br />
    <div class="offset-2">
        <div class="fblueline">
            Manage
           
            <span class="farrow"></span>
            <a style="color: white" href="CommentList.aspx" title="Comment Details"><b>Comment Details</b></a>

        </div>
        <div class="frow1">

            <br>
            <br>

            <div class="table-responsive">

                <div id="tbl_StaffDetails_wrapper" class="dataTables_wrapper">
                    <table class="table table-striped table-bordered dataTable" id="tbl_StaffDetails" style="width: 100%;">
                        <thead>
                            <tr>
                                <td align="center" style="width: 10%"><b>S.N</b>
                                </td>
                                <td align="center"><b>Name</b>
                                </td>
                                <td align="center"><b>Comment</b>
                                </td>
                                <td align="center"><b>Date</b>
                                </td>
                                </td>
                                <td align="center"><b> Delete</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody id="Comments">
                            <%--   <tr class="odd">
                                <td align="center" style="width: 10%">1</td>
                                <td align="center">Salman</td>
                                <td align="center">Admin</td>
                                <td align="center">10</td>
                                <td align="center">10</td>
                                <td align="center"><a style="cursor: pointer" onclick="UpdateDilogBox()" href="#"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a>&nbsp;&nbsp;|  <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="DeleteStaffID('test@test.co','maqsood maqbool sheikh')"></span></a></td>
                            </tr>--%>
                        </tbody>
                    </table>
                </div>

            </div>
            <br>
        </div>
    </div>
    <div class="modal fade" id="AddUser" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Add System User</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>
                                                First Name *</label>
                                            <input id="Fname" type="text" class=" form-control" />

                                        </td>
                                        <td>
                                            <label>
                                                Last Name *</label>
                                            <input id="Lanme" type="text" class=" form-control" />

                                        </td>
                                    </tr>
                                    <tr>
                                         <td>
                                            <label>UserType *</label>
                                            <select id="UserType" class="form-control">
                                                <%--<option selected="selected" value="0">Select User Type</option>--%>
                                                <option value="Admin">Admin </option>
                                                <option value="Dispatcher">Dispatcher</option>
                                            </select>
                                        </td>
                                        <td>
                                            <label>Mobile No. *</label>
                                            <input id="Mobile" type="text" class=" form-control" />
                                        </td>
                                        <%--<td>
                                            <label>Text Message. *</label>
                                            <input id="TextMessage" type="text" class=" form-control" />
                                        </td>--%>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Email *</label>
                                            <input id="Email" type="text" class=" form-control" />
                                        </td>
                                        <td>
                                            <label>Password *</label>
                                            <input id="Password" type="text" class=" form-control" />
                                        </td>
                                    </tr>
                                    <tr>
                                       <td>
                                            <label>City *</label>
                                            <input id="City" type="text" class=" form-control" />
                                        </td>
                                        <td>
                                            <label>Country *</label>
                                            <input id="Country" type="text" class=" form-control" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label>Address *</label>
                                            <input id="Address" type="text" class=" form-control" />
                                        </td>
                                        <td>
                                            <label>PinCode *</label>
                                            <input id="PinCode" type="text" class=" form-control" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <%--<td>
                                            <br>
                                            <label>
                                                <input type="checkbox" id="Active" style="margin-top: 1.5px;" />
                                                Active</label>
                                        </td>--%>

                                        <td colspan="2">

                                            <div align="right" style="margin-right: 5%">
                                                <input type="button" class="btn-search" value="Save" onclick="Insert()" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#AddUser').modal('hide')" />
                                            </div>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="UpdateUser" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Update User</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>
                                                First Name *</label>
                                            <input id="AFname" type="text" class=" form-control" />

                                        </td>
                                        <td>
                                            <label>
                                                Last Name *</label>
                                            <input id="ALanme" type="text" class=" form-control" />

                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            <label>Mobile No. *</label>
                                            <input id="AMobile" type="text" class=" form-control" />
                                        </td>
                                        <td>
                                            <label>Address *</label>
                                            <input id="AAddress" type="text" class=" form-control" />
                                        </td>
                                        <td>
                                            <label>UserType *</label>
                                            <select id="AUserType" class="form-control">
                                                <option selected="selected" value="0">Select User Type</option>
                                                <option value="Admin">Admin </option>
                                                <option value="User">User</option>
                                            </select>
                                        </td>
                                        <%--<td>
                                            <label>Text Message. *</label>
                                            <input id="ATextMessage" type="text" class=" form-control" />
                                        </td>--%>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Email *</label>
                                            <input id="AEmail" type="text" class=" form-control" />
                                        </td>
                                        <td>
                                            <label>Password *</label>
                                            <input id="APassword" type="password" class=" form-control" />
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>
                                            <label>City *</label>
                                            <input id="ACity" type="text" class=" form-control" />
                                        </td>
                                        <td>
                                            <label>Country *</label>
                                            <input id="ACountry" type="text" class=" form-control" />
                                        </td>
                                    </tr>

                                    <tr>
                                        
                                        <td>
                                            <label>PinCode *</label>
                                            <input id="APinCode" type="text" class=" form-control" />
                                        </td>
                                    </tr>
                                    <tr>
                                       <%-- <td>
                                            <br>
                                            <label>
                                                <input type="checkbox" id="AActive" style="margin-top: 1.5px;" />
                                                Active</label>
                                        </td>--%>
                                        <td colspan="2">
                                            <div align="right" style="margin-right: 5%">
                                                <input type="button" class="btn-search" value="Update" onclick="UpdateUser()" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#UpdateUser').modal('hide')" />
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <input type="hidden" id="hdn" />
</asp:Content>

