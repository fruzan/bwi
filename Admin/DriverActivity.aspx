﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="DriverActivity.aspx.cs" Inherits="BWI.Admin.DriverActivity" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#datepicker1").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "mm-dd-yy"
            });

            $("#datepicker2").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "mm-dd-yy"
            });
            GetAllDriver()
            //GetAll()
        });

    </script>
    <script type="text/javascript" src="Scripts/DriverReport.js"></script>
    <style type="text/css">
        div.scrollingDiv {
            width: auto;
            height: 450px;
            overflow-y: scroll;
        }

        #TermsModal p {
            margin: 0 10px 10px;
        }

        .spacing {
            padding-bottom: 4px;
        }
    </style>
    <style type="text/css">
        .cp-btn-style1 {
    font-family: 'Exo 2', sans-serif;
    font-size: 18px;
    line-height: 18px;
    font-weight: 400;
    color: #fff;
    display: inline-block;
    text-align: center;
    padding: 13px 20px;
    max-width: 150px;
    position: relative;
    overflow: hidden;
    z-index: 11;
    border: none;
    box-shadow: none;
    text-decoration: none;
    border-bottom: 5px solid #e77d7d;
}
    </style>
    <style type="text/css">
        tr:nth-child(even) {
    background-color: rgb(224, 220, 220);
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    

  <br />
    <%--<span id="TopSpan"> <span>Total Fare:180</span>,<span>Percentage Fare: 80</span>,<span>Drivers Payout: 80</span> </span>--%>
    
    <table align="left" style="margin-left: 1.8%">
        <tbody>
            <tr>

                <td style="padding-top: 3.2%">
                    <label>From</label>
                    <input type="text" class="form-control datepicker" id="datepicker1" style="cursor: pointer; background: url('../../images/calendar.png') #fff no-repeat right;" placeholder="mm-dd-yyyy">
                </td>
                <td style="padding-top: 3.2%; padding-left: 3.2%">
                    <label>To</label>
                    <input type="text" class="form-control datepicker" id="datepicker2" style="cursor: pointer; background: url('../../images/calendar.png') #fff no-repeat right; padding-left: 25px;" placeholder="mm-dd-yyyy">
                </td>
                <td style="padding-top: 3.2%; padding-left: 3.2%">
                    <label>Driver Name</label>
                    <select id="sel_driver" class="form-control" style="padding-left: 25px;"></select>
                </td>
                <td style="padding-top: 4.4%">

                    <input type="button" class="btn-search" value="Search" id="btn_Search" onclick="Submit();">
                </td>
                <td>
                    <br /><br />
                    <img src="../images/ExportToExcel.png" title="Export To Excel" onclick="ExportToExcel()" alt="Export To Excel" style="height:40px;width:40px;cursor:pointer"/>
                </td>
                <td colspan="2"></td>
            </tr>
        </tbody>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <div class="offset-2">
        <div class="fblueline">
            Reports
           
            <span class="farrow"></span>
            <a style="color: white" href="DriverActivity.aspx" title="Driver Details"><b>Driver Details</b></a>

        </div>
        <div class="frow1">

            <br>
            <br>

            <div class="table-responsive">
                <div style="width: auto; height: 740px; overflow: scroll;">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered dataTable" id="Details" style="width: 100%;">
                        <thead>
                            <tr>
                                <td align="center"><b>S.N</b>
                                </td>
                                 <td align="center"><b>Booking No.</b>
                                </td>
                                <td align="center"><b>Assigned To</b>
                                </td>
                                <td align="center"><b>Guest Name</b>
                                </td>
                                <td align="center"><b>Reservation Date</b>
                                </td>
                                <td align="center"><b>Service</b>
                                </td>
                                <td align="center"><b>Source</b>
                                </td>
                                <td align="center"><b>Destination</b>
                                </td>
                                <td align="center"><b>Total Fare</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                    
                    
                </div>
            </div>
           
        </div><br />
         <span style="margin-left: 1.8%" id="TopSpan"></span>
    </div>

<div class="modal fade bs-example-modal-lg" id="BookingDetails" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:437px">
            <div class="modal-content" style="background-color: aliceblue;">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel2" style="text-align: center">Booking Details</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="WordSection1">
                            <label style="font-size: 18px; margin-left:100px" id="TotalAmount"></label>
                            <table border="1">
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Booking No: </p><p id="BookingNo"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Reservation Date: </p><p id="ResDate"></p></td>
                                </tr>

                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Source: </p><p id="Source"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Destination:</p><p id="Destination"></p></td>
                                </tr>
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Assigned To:</p><p id="AssignedTo"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Guest Name:</p><p id="Name"></p></td>
                                </tr>
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Passenger:</p><p id="Passenger"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Phone No:</p><p id="PhoneNo"></p></td>
                                </tr>
                                <tr>
                                        <td style="padding:8px"><p style="font-weight:bold">Service:</p><p id="Service"></p></td>
                                        <td style="padding:8px"><p style="font-weight:bold">Total Fare:</p><p id="TotalFare"></p></td>
                                </tr>

                                <%--<tr id="Ret_details"><th colspan="2" text-align: center; style="background-color: darkseagreen;font-size: 18px;padding:8px;">Return Details</th></tr>--%>
                                
                            </table><br />
                            <%--<input type="button" id="btnBook" class="cp-btn-style1" onclick="Submit();" style="background-color:red; margin-top:13px" value="Book Now" />
                            <img alt="" id="CircleImage" src="images/circleloadinganimation.gif" width="100" height="100" style="display:none"/>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
