﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for ExportToExcelHandler
    /// </summary>
    public class ExportToExcelHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
            string requestedTable = context.Request["datatable"];
            string From = context.Request["From"];
            string To = context.Request["To"];
            string DriverName = context.Request["DriverName"];
            Decimal TotalAmount = 0;
            //string dTo = context.Request["dTo"];
            //string Agent = context.Request["uid"];
            //string AgencyText = context.Request.QueryString["AgencyText"];
            //string Agency = context.Request.QueryString["Agency"];
            //string TransType = context.Request.QueryString["TransType"];
            //string OnLoad = "True";

            #region Reservation List

            if (requestedTable == "dtReservation")
            {
                DataTable dtResult = null;
                if (HttpContext.Current.Session["CustomerList"] != null)
                {
                    dtResult = (DataTable)HttpContext.Current.Session["CustomerList"];
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        TotalAmount = TotalAmount + Convert.ToDecimal(dtResult.Rows[i]["TotalFare"]);
                    }
                    //dtResult = ConvertToDatatable(List);
                    DataView myDataView = dtResult.DefaultView;
                    myDataView.Sort = "Sid DESC";
                    DataTable SorteddtResult = myDataView.ToTable();
                    DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "ReservationID", "AssignedTo", "FirstName", "ReservationDate", "Service", "Source", "Destination", "Fare", "TotalFare", "GratuityAmount");
                    ExporttoExcelReservation(dtSelectedColumns, TotalAmount);
                }
                else
                {
                    DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "ReservationID", "AssignedTo", "FirstName", "ReservationDate", "Service", "Source", "Destination", "Fare", "TotalFare", "GratuityAmount");
                    ExporttoExcelReservation(dtSelectedColumns, TotalAmount);
                }

                //////S Old
                //DBHelper.DBReturnCode retcode = ReservationManager.GetReservationList(out dtResult);

                //if (retcode == DBHelper.DBReturnCode.SUCCESS)
                //{

                //    for(int i=0; i<dtResult.Rows.Count; i++)
                //    {
                //        TotalAmount = TotalAmount + Convert.ToDecimal(dtResult.Rows[i]["TotalFare"]);
                //    }
                //    DataView myDataView = dtResult.DefaultView;
                //    myDataView.Sort = "Sid DESC";
                //    DataTable SorteddtResult = myDataView.ToTable();
                //    DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "ReservationID", "AssignedTo", "FirstName", "ReservationDate", "Service", "Source", "Destination", "TotalFare");
                //    ExporttoExcelReservation(dtSelectedColumns, TotalAmount);


                //}
                //else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
                //{

                //    DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "ReservationID", "AssignedTo", "FirstName", "ReservationDate", "Service", "Source", "Destination", "TotalFare");
                //    ExporttoExcelReservation(dtSelectedColumns, TotalAmount);
                //}
                ////E Old
            }

            #endregion Invoice

            #region Customer List

            if (requestedTable == "dtCustomer")
            {
                //List<Trans_BookingReservation> dtResult = new List<Trans_BookingReservation>();
                //DBHelper.DBReturnCode retcode = CustomerManager.GetAllCustomerActivity(out dtResult);
                DataTable dtResult = null;
                if (HttpContext.Current.Session["CustomerList"] != null)
                {
                    dtResult = (DataTable)HttpContext.Current.Session["CustomerList"];
                    //dtResult = ConvertToDatatable(List);
                    DataView myDataView = dtResult.DefaultView;
                    myDataView.Sort = "Sid DESC";
                    DataTable SorteddtResult = myDataView.ToTable();
                    DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "ReservationID", "AssignedTo", "FirstName", "ContactNumber", "ReservationDate", "Service", "Source", "Destination", "GratuityAmount", "Fare", "TotalFare");
                    ExporttoExcelCustomer(dtSelectedColumns);
                }
                else
                {
                    DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "ReservationID", "AssignedTo", "FirstName", "ContactNumber", "ReservationDate", "Service", "Source", "GratuityAmount", "Destination");
                    ExporttoExcelCustomer(dtSelectedColumns);
                }
            }

            #endregion Invoice

            #region Driver List

            if (requestedTable == "dtDriver")
            {
                DataTable dtResult = null;
                if (HttpContext.Current.Session["DriverList"] != null)
                {
                    dtResult = (DataTable)HttpContext.Current.Session["DriverList"];
                    //dtResult = ConvertToDatatable(List);
                    DataView myDataView = dtResult.DefaultView;
                    myDataView.Sort = "Sid DESC";
                    DataTable SorteddtResult = myDataView.ToTable();
                    DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "ReservationID", "AssignedTo", "FirstName", "ReservationDate", "Service", "Source", "Destination", "Fare", "TotalFare", "GratuityAmount");
                    ExporttoExcelDriver(dtSelectedColumns);
                }
                else
                {
                    DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "ReservationID", "AssignedTo", "FirstName", "ReservationDate", "Service", "Source", "Destination", "Fare", "TotalFare", "GratuityAmount");
                    ExporttoExcelDriver(dtSelectedColumns);
                }
                #region Comment
                /*DataTable dtResult;
                DBHelper.DBReturnCode retcode = DriverManager.GetAllDriverDataTable(From,To,DriverName, out dtResult);

                if (retcode == DBHelper.DBReturnCode.SUCCESS)
                {
                    DataView myDataView = dtResult.DefaultView;
                    myDataView.Sort = "Sid DESC";
                    DataTable SorteddtResult = myDataView.ToTable();
                    DataTable dtSelectedColumns = SorteddtResult.DefaultView.ToTable(false, "ReservationID", "AssignedTo", "FirstName", "ReservationDate", "Service", "Source", "Destination", "Fare", "TotalFare", "GratuityAmount");
                    ExporttoExcelDriver(dtSelectedColumns);
                }
                else if (retcode == DBHelper.DBReturnCode.SUCCESSNORESULT)
                {

                    DataTable dtSelectedColumns = dtResult.DefaultView.ToTable(false, "ReservationID", "AssignedTo", "FirstName", "ReservationDate", "Service", "Source", "Destination", "Fare", "TotalFare", "GratuityAmount");
                    ExporttoExcelDriver(dtSelectedColumns);
                }*/
                #endregion
            }


            #endregion Invoice
        }

        public static void ExporttoExcelReservation(DataTable objdatatable, Decimal Total)
        {
            string DriverAmount = HttpContext.Current.Session["CustomerAmount"].ToString();
            string test = "My 10";
            string[] Splitter = DriverAmount.Split(' ');
            string TotalFare = Splitter[0];
            string PercentageOfFare = Splitter[1];
            string DriversPayOut = Splitter[2];
            string Tip = Splitter[3];
            string Fare = Splitter[4];
            objdatatable.Columns["ReservationDate"].ColumnName = "Reservation Date";
            objdatatable.Columns["ReservationID"].ColumnName = "Reservation ID";
            objdatatable.Columns["AssignedTo"].ColumnName = "Assigned To";
            objdatatable.Columns["FirstName"].ColumnName = "First Name";
            objdatatable.Columns["Service"].ColumnName = "Service";
            objdatatable.Columns["Source"].ColumnName = "Source";
            objdatatable.Columns["Destination"].ColumnName = "Destination";
            objdatatable.Columns["Fare"].ColumnName = "Fare";
            objdatatable.Columns["TotalFare"].ColumnName = "TotalFare";
            //objdatatable.Columns["terms"].ColumnName = "Remarks";
            objdatatable.Columns["GratuityAmount"].ColumnName = "Tip Amount";
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:13.0pt; font-family:Helvetica Neue;'>");
            //HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'>");

            HttpContext.Current.Response.Write(" <TR style='font-size:40.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white; text-align:center'><td colspan=" + objdatatable.Columns.Count + ">BWI Shuttle Service </td></TR>");
            HttpContext.Current.Response.Write(" <TR>");
            //am getting my grid's column headers
            int columnscount = objdatatable.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td style='font-size:11.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white'>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");

            if (objdatatable.Rows.Count <= 0)
            {
                HttpContext.Current.Response.Write("<Td colspan=\"7\" align=\"left\">");
                HttpContext.Current.Response.Write("No Record Found");
                HttpContext.Current.Response.Write("</Td>");
            }
            else
            {
                foreach (DataRow row in objdatatable.Rows)
                {//write in new row
                    HttpContext.Current.Response.Write("<TR>");
                    for (int i = 0; i < objdatatable.Columns.Count; i++)
                    {

                        HttpContext.Current.Response.Write("<Td>");
                        HttpContext.Current.Response.Write(row[i].ToString().Replace("False", "Inactive").Replace("True", "Active"));
                        HttpContext.Current.Response.Write("</Td>");
                    }

                    HttpContext.Current.Response.Write("</TR>");
                }
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("</TR>");

                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("<table align=\"center\">");
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<b>");
                HttpContext.Current.Response.Write("Fare: ".Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write(Fare.Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write("</b>");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<b>");
                HttpContext.Current.Response.Write("Percentage Of Fare: ".Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write(PercentageOfFare.Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write("</b>");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<b>");
                HttpContext.Current.Response.Write("Tip: ".Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write(Tip.Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write("</b>");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<b>");
                HttpContext.Current.Response.Write("Total Fare: ".Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write(TotalFare.Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write("</b>");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<b>");
                HttpContext.Current.Response.Write("Drivers PayOut: ".Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write(DriversPayOut.Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write("</b>");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("</font>");
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
        }

        public static void ExporttoExcelCustomer(DataTable objdatatable)
        {
            string DriverAmount = HttpContext.Current.Session["CustomerAmount"].ToString();
            string test = "My 10";
            string[] Splitter = DriverAmount.Split(' ');
            string TotalFare = Splitter[0];
            string PercentageOfFare = Splitter[1];
            string DriversPayOut = Splitter[2];
            string Tip = Splitter[3];
            string Fare = Splitter[4];
            objdatatable.Columns["ReservationDate"].ColumnName = "Reservation Date";
            objdatatable.Columns["ReservationID"].ColumnName = "Reservation ID";
            objdatatable.Columns["AssignedTo"].ColumnName = "Assigned To";
            objdatatable.Columns["FirstName"].ColumnName = "First Name";
            objdatatable.Columns["Service"].ColumnName = "Service";
            objdatatable.Columns["Source"].ColumnName = "Source";
            objdatatable.Columns["Destination"].ColumnName = "Destination";
            objdatatable.Columns["ContactNumber"].ColumnName = "Phone Number";
            objdatatable.Columns["Fare"].ColumnName = "Fare";
            objdatatable.Columns["TotalFare"].ColumnName = "TotalFare";
            objdatatable.Columns["GratuityAmount"].ColumnName = "Tip Amount";

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:13.0pt; font-family:Helvetica Neue;'>");
            //HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'>");

            HttpContext.Current.Response.Write(" <TR style='font-size:40.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white; text-align:center'><td colspan=" + objdatatable.Columns.Count + ">BWI Shuttle Service </td></TR>");
            HttpContext.Current.Response.Write(" <TR>");
            //am getting my grid's column headers
            int columnscount = objdatatable.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td style='font-size:11.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white'>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");

            if (objdatatable.Rows.Count <= 0)
            {
                HttpContext.Current.Response.Write("<Td colspan=\"7\" align=\"left\">");
                HttpContext.Current.Response.Write("No Record Found");
                HttpContext.Current.Response.Write("</Td>");
            }
            else
            {
                foreach (DataRow row in objdatatable.Rows)
                {//write in new row
                    HttpContext.Current.Response.Write("<TR>");
                    for (int i = 0; i < objdatatable.Columns.Count; i++)
                    {

                        HttpContext.Current.Response.Write("<Td>");
                        HttpContext.Current.Response.Write(row[i].ToString().Replace("False", "Inactive").Replace("True", "Active"));
                        HttpContext.Current.Response.Write("</Td>");
                    }

                    HttpContext.Current.Response.Write("</TR>");
                }

                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("<table align=\"center\">");
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<b>");
                HttpContext.Current.Response.Write("Fare: ".Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write(Fare.Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write("</b>");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<b>");
                HttpContext.Current.Response.Write("Tip: ".Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write(Tip.Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write("</b>");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<b>");
                HttpContext.Current.Response.Write("Total Fare: ".Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write(TotalFare.Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write("</b>");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("</TR>");
                //HttpContext.Current.Response.Write("<TR>");
                //HttpContext.Current.Response.Write("<Td>");
                //HttpContext.Current.Response.Write("<b>");
                //HttpContext.Current.Response.Write("Percentage Of Fare: ".Replace("False", "Inactive").Replace("True", "Active"));
                //HttpContext.Current.Response.Write(PercentageOfFare.Replace("False", "Inactive").Replace("True", "Active"));
                //HttpContext.Current.Response.Write("</b>");
                //HttpContext.Current.Response.Write("</Td>");
                //HttpContext.Current.Response.Write("</TR>");
                //HttpContext.Current.Response.Write("<TR>");
                //HttpContext.Current.Response.Write("<Td>");
                //HttpContext.Current.Response.Write("<b>");
                //HttpContext.Current.Response.Write("Company PayOut: ".Replace("False", "Inactive").Replace("True", "Active"));
                //HttpContext.Current.Response.Write(DriversPayOut.Replace("False", "Inactive").Replace("True", "Active"));
                //HttpContext.Current.Response.Write("</b>");
                //HttpContext.Current.Response.Write("</Td>");
                //HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("</font>");
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
        }

        public static void ExporttoExcelDriver(DataTable objdatatable)
        {
            string DriverAmount = HttpContext.Current.Session["DriverAmount"].ToString();
            string test = "My 10";
            string[] Splitter = DriverAmount.Split(' ');
            string TotalFare = Splitter[0];
            string PercentageOfFare = Splitter[1];
            string DriversPayOut = Splitter[2];
            string Tip = Splitter[3];
            string Fare = Splitter[4];
            objdatatable.Columns["ReservationDate"].ColumnName = "Reservation Date";
            objdatatable.Columns["ReservationID"].ColumnName = "Booking No";
            objdatatable.Columns["AssignedTo"].ColumnName = "Assigned To";
            objdatatable.Columns["FirstName"].ColumnName = "First Name";
            objdatatable.Columns["Service"].ColumnName = "Service";
            objdatatable.Columns["Source"].ColumnName = "Source";
            objdatatable.Columns["Destination"].ColumnName = "Destination";
            objdatatable.Columns["Fare"].ColumnName = "Fare";
            objdatatable.Columns["TotalFare"].ColumnName = "TotalFare";
            objdatatable.Columns["GratuityAmount"].ColumnName = "Tip Amount";

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:13.0pt; font-family:Helvetica Neue;'>");
            //HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Helvetica Neue; background:#f2f2f2; text-align:center'>");
            HttpContext.Current.Response.Write(" <TR style='font-size:40.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white; text-align:center'><td colspan=" + objdatatable.Columns.Count + ">BWI Shuttle Service </td></TR>");
            HttpContext.Current.Response.Write(" <TR>");
            //am getting my grid's column headers
            int columnscount = objdatatable.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td style='font-size:11.0pt; font-family:Helvetica Neue; text-align:center; background:#006699;color:white'>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(objdatatable.Columns[j].ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");

            if (objdatatable.Rows.Count <= 0)
            {
                HttpContext.Current.Response.Write("<Td colspan=\"7\" align=\"left\">");
                HttpContext.Current.Response.Write("No Record Found");
                HttpContext.Current.Response.Write("</Td>");
            }
            else
            {
                foreach (DataRow row in objdatatable.Rows)
                {//write in new row
                    HttpContext.Current.Response.Write("<TR>");
                    for (int i = 0; i < objdatatable.Columns.Count; i++)
                    {

                        HttpContext.Current.Response.Write("<Td>");
                        HttpContext.Current.Response.Write(row[i].ToString().Replace("False", "Inactive").Replace("True", "Active"));
                        HttpContext.Current.Response.Write("</Td>");
                    }

                    HttpContext.Current.Response.Write("</TR>");
                }
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("</TR>");

                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("<table align=\"center\">");
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<b>");
                HttpContext.Current.Response.Write("Fare: ".Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write(Fare.Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write("</b>");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<b>");
                HttpContext.Current.Response.Write("Total Fare: ".Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write(TotalFare.Replace("False", "Inactive").Replace("True", "Active"));
                
                HttpContext.Current.Response.Write("</b>");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<b>");
                HttpContext.Current.Response.Write("Percentage Of Fare: ".Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write(PercentageOfFare.Replace("False", "Inactive").Replace("True", "Active"));
                
                HttpContext.Current.Response.Write("</b>");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<b>");
                HttpContext.Current.Response.Write("Tip: ".Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write(Tip.Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write("</b>");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("<TR>");
                HttpContext.Current.Response.Write("<Td>");
                HttpContext.Current.Response.Write("<b>");
                HttpContext.Current.Response.Write("Drivers PayOut: ".Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write(DriversPayOut.Replace("False", "Inactive").Replace("True", "Active"));
                HttpContext.Current.Response.Write("</b>");
                HttpContext.Current.Response.Write("</Td>");
                HttpContext.Current.Response.Write("</TR>");
                HttpContext.Current.Response.Write("</Table>");
                HttpContext.Current.Response.Write("</font>");
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
        }

        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}