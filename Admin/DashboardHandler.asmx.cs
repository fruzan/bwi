﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BWI.Admin.DataLayer;
using BWI.BL;
using System.Web.Script.Serialization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Data;
using System.ComponentModel;
using System.Data.Linq.SqlClient;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for DashboardHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DashboardHandler : System.Web.Services.WebService
    {
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        DBHandlerDataContext DB = new DBHandlerDataContext();
        string json = "";
        private string escapejosndata(string data)
        {
            return data.Replace("\\", "\\\\").Replace("\"", "\\\"");
        }
        [WebMethod(EnableSession = true)]
        public string LoadAllCount()
        {
            try
            {
                Trans_Reservation Reservation = new Trans_Reservation();
                var ResToday = 0;
                var Pending = 0;

                List<decimal> AmountList = new List<decimal>();
                List<string> ReservationId = new List<string>();
                DateTime Today = DateTime.Today;
                DateTime TommorowDate = DateTime.Now.AddDays(1);
                string TodayDate = Today.ToString("MM-dd-yyyy");
                string Tommorow = TommorowDate.ToString("MM-dd-yyyy");

                var SerToday = (from obj in DB.Trans_Reservations where obj.ReservationDate == TodayDate && obj.AssignedTo != null && obj.Status != "Deleted" select obj).Count();
                var SerTodayList = (from obj in DB.Trans_Reservations where obj.ReservationDate == TodayDate && obj.AssignedTo != null && obj.Status != "Deleted" select obj).ToList();
                var SerTomorrow = (from obj in DB.Trans_Reservations where obj.ReservationDate == Tommorow && obj.AssignedTo != null && obj.Status != "Deleted" select obj).Count();

                var Upcoming = (from obj in DB.Trans_Reservations where (obj.ReservationDate == TodayDate || obj.ReservationDate == Tommorow) && obj.Status != "Deleted" select obj).Count();

                var UpcomingList = (from obj in DB.Trans_Reservations where (obj.ReservationDate == TodayDate || obj.ReservationDate == Tommorow) && obj.Status != "Deleted" select obj).OrderByDescending(x=>x.sid).ToList();

                var List = (from obj in DB.Trans_Reservations select obj).OrderByDescending(x => x.sid).ToList();

                TodayDate = Today.ToString("dd-MM-yyyy");

                DateTime todays = GetAppDate(TodayDate);

                List<Trans_Reservation> ResTodayList = (from obj in DB.Trans_Reservations where SqlMethods.Like(obj.Date, TodayDate + "%") && obj.Status != "Deleted" select obj).OrderByDescending(x => x.sid).ToList();
                int Online = 0;
                var OnlineList = DB.Trans_Reservations; List<Trans_Reservation> OnlineResvation = new List<Trans_Reservation>();
                for (int i = 0; i < List.Count; i++)
                {
                    string Rdate = List[i].Date;
                    string[] Split = Rdate.Split(' ');
                    Rdate = Split[0];
                    if (Rdate == TodayDate)
                    {
                        //ResToday++;
                        decimal Amt = Convert.ToDecimal(List[i].TotalFare);
                        AmountList.Add(Amt);

                    }
                    DateTime ReservationDate = DateTime.ParseExact(List[i].ReservationDate, "MM-dd-yyyy", null);
                    if (ReservationDate >= todays && List[i].Status != "Deleted")
                    {
                        var ResId = List[i].ReservationID;
                        ReservationId.Add(List[i].ReservationID);
                        Pending++;
                    }
                    if (List[i].ReservationType == "Online" && Rdate == TodayDate && List[i].Status != "Deleted")
                    {
                        OnlineResvation.Add(List[i]);
                        Online += 1;
                    }
                }
                //DataRow dr;

                decimal TotalFare = AmountList.Sum();

                string date = Reservation.ReservationDate;

                DateTime dt = Convert.ToDateTime(date);

                var Unassign = (from obj in DB.Trans_Reservations where obj.ReservationDate == Tommorow && obj.AssignedTo == null select obj).Count();


                // var OnlineRes = (from obj in DB.Trans_Reservations where obj.ReservationType == "Online" select obj).Count();


                var OnlineRes = Online;
                // start Creating a list of result and converting to datatable

                //List<BWI.Trans_Tbl_AirLine> Data;



                var SerTomorrowList = (from obj in DB.Trans_Reservations where obj.ReservationDate == Tommorow && obj.AssignedTo != null select obj).OrderByDescending(x => x.sid).ToList();

                var UnassignList = (from obj in DB.Trans_Reservations where obj.ReservationDate == Tommorow && obj.AssignedTo == null && obj.Status != "Deleted" select obj).OrderByDescending(x => x.sid).ToList();

                ResToday = ResTodayList.Count();

                var OnlineResList = (from obj in DB.Trans_Reservations where obj.ReservationType == "Online" && obj.Status != "Deleted" select obj).OrderByDescending(x => x.sid).ToList();

                DataTable dtSerToday = ConvertToDatatable(SerTodayList);
                DataTable dtSerTomorrow = ConvertToDatatable(SerTomorrowList);
                DataTable dtResToday = ConvertToDatatable(ResTodayList);
                DataTable dtUnassign = ConvertToDatatable(UnassignList);
                DataTable dtOnlineRes = ConvertToDatatable(OnlineResvation);
                DataTable dtUpcoming = ConvertToDatatable(UpcomingList);

                DataTable dtPenging = dtSerToday.Clone();
                List<Trans_Reservation> Pend = new List<Trans_Reservation>();
                for (int i = 0; i < ReservationId.Count; i++)
                {
                    //DataTable asd;
                    Pend = (from obj in DB.Trans_Reservations where obj.ReservationID == ReservationId[i] select obj).ToList();
                    DataTable dtTemp = ConvertToDatatable(Pend);
                    foreach (DataRow dr in dtTemp.Rows)
                    {
                        //dtPenging = dtTemp.Clone();
                        dtPenging.ImportRow(dr);
                        //asd = dtTemp.Clone();
                        //asd.ImportRow(dr);
                        //dtPenging.Rows.Add(dr.ItemArray);
                    }
                }

                // End Creating a list to datatable

                // Start Driver List
                DBHelper.DBReturnCode retcode;

                List<BWI.Trans_Tbl_Login> DriverList = null;
                retCode = ReservationManager.GetAllDriver(out DriverList);
                string jsonDriver = "";
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    jsonDriver = jsSerializer.Serialize(DriverList);
                    jsonDriver = jsonDriver.TrimStart('[');
                    jsonDriver = jsonDriver.TrimEnd(']');

                    //json = "{\"Retcode\":\"1\",\"Arr\":[" + json + "],\"ArrDriver\":[" + jsonDriver + "]}";

                }
                //End Driver List

                string jSerToday = "";
                foreach (DataRow dr in dtSerToday.Rows)
                {
                    jSerToday += "{";
                    foreach (DataColumn dc in dtSerToday.Columns)
                    {
                        jSerToday += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jSerToday = jSerToday.Trim(',') + "},";
                }

                string jSerTomorrow = "";
                foreach (DataRow dr in dtSerTomorrow.Rows)
                {
                    jSerTomorrow += "{";
                    foreach (DataColumn dc in dtSerTomorrow.Columns)
                    {
                        jSerTomorrow += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jSerTomorrow = jSerTomorrow.Trim(',') + "},";
                }

                string jResToday = "";
                foreach (DataRow dr in dtResToday.Rows)
                {
                    jResToday += "{";
                    foreach (DataColumn dc in dtResToday.Columns)
                    {
                        jResToday += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jResToday = jResToday.Trim(',') + "},";
                }

                string jUnassign = "";
                foreach (DataRow dr in dtUnassign.Rows)
                {
                    jUnassign += "{";
                    foreach (DataColumn dc in dtUnassign.Columns)
                    {
                        jUnassign += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jUnassign = jUnassign.Trim(',') + "},";
                }

                string jOnlineRes = "";
                foreach (DataRow dr in dtOnlineRes.Rows)
                {
                    jOnlineRes += "{";
                    foreach (DataColumn dc in dtOnlineRes.Columns)
                    {
                        jOnlineRes += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jOnlineRes = jOnlineRes.Trim(',') + "},";
                }

                string jUpcoming = "";
                foreach (DataRow dr in dtUpcoming.Rows)
                {
                    jUpcoming += "{";
                    foreach (DataColumn dc in dtUpcoming.Columns)
                    {
                        jUpcoming += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jUpcoming = jUpcoming.Trim(',') + "},";
                }

                string jPenging = "";
                foreach (DataRow dr in dtPenging.Rows)
                {
                    jPenging += "{";
                    foreach (DataColumn dc in dtPenging.Columns)
                    {
                        jPenging += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
                    }
                    jPenging = jPenging.Trim(',') + "},";
                }
                List<Dictionary<string, object>> dtResult = new List<Dictionary<string, object>>();
                var PendingList = ConvertDataTable(dtPenging);
                dtResult = ConvertDataTable(dtUpcoming);
                var DeletedReservations = (from obj in DB.Trans_Reservations where obj.Status == "Deleted" select obj).ToList();
                return jsSerializer.Serialize(new { Session = 1, retCode = 1, SerToday = SerToday, SerTomorrow = SerTomorrow, ResToday = ResToday, Unassign = Unassign, Amount = TotalFare, OnlineRes = OnlineRes, Upcoming = Upcoming, Pending = Pending, dtSerToday = SerTodayList, dtSerTomorrow = SerTomorrowList, dtResToday = ResTodayList, dtUnassign = UnassignList, dtOnlineRes = OnlineResList, dtUpcoming = UpcomingList, dtPenging = PendingList, ArrDriver = DriverList, DeletedReservations = DeletedReservations });
                //json = "{\"Session\":\"1\",\"retCode\":\"1\",\"SerToday\":\"" + SerToday + "\",\"SerTomorrow\":\"" + SerTomorrow + "\",\"ResToday\":\"" + ResToday + "\",\"Unassign\":\"" + Unassign + "\",\"Amount\":\"" + TotalFare + "\",\"OnlineRes\":\"" + OnlineRes + "\",\"Upcoming\":\"" + Upcoming + "\",\"Pending\":\"" + Pending + "\",\"dtSerToday\":[" + jSerToday.Trim(',') + "],\"dtSerTomorrow\":[" + jSerTomorrow.Trim(',') + "],\"dtResToday\":[" + jResToday.Trim(',') + "],\"dtUnassign\":[" + jUnassign.Trim(',') + "],\"dtOnlineRes\":[" + jOnlineRes.Trim(',') + "],\"dtUpcoming\":[" + jUpcoming.Trim(',') + "],\"dtPenging\":[" + jPenging.Trim(',') + "],\"ArrDriver\":[" + jsonDriver + "]}";

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                json = "{\"Session\":\"1\",\"retCode\":\"-1\",\"Error\":\"" + msg + "\"}";
            }
            return json;
        }

        [WebMethod(true)]
        public string EmailSending(string Email, string Sub, string Message)
        {
            string sJsonString = "{\"retCode\":\"0\"}";
            if (DashboardManager.SendEmail(Email, "Information Received", Sub, Message) == true)//info@tmauto.in
            {
                sJsonString = "{\"retCode\":\"1\",}";
            }
            return sJsonString;
        }

        //[WebMethod(true)]
        //public string GetSerTodayTable()
        //{
        //    DateTime Today = DateTime.Today;
        //    string TodayDate = Today.ToString("MM-dd-yyyy");
        //    var SerTodayList = (from obj in DB.Trans_Reservations where obj.ReservationDate == TodayDate && obj.AssignedTo != null select obj).ToList();
        //    DataTable dtSerToday = ConvertToDatatable(SerTodayList);
        //    string jSerToday = "";
        //    foreach (DataRow dr in dtSerToday.Rows)
        //    {
        //        jSerToday += "{";
        //        foreach (DataColumn dc in dtSerToday.Columns)
        //        {
        //            jSerToday += "\"" + dc.ColumnName + "\":\"" + escapejosndata(dr[dc].ToString().Replace("\r\n", " ")) + "\",";
        //        }
        //        jSerToday = jSerToday.Trim(',') + "},";
        //    }
        //    return jsSerializer.Serialize(new { Session = 1, retCode = 1, dtSerToday = jSerToday });
        //}

        public static DateTime GetAppDate(string Date)
        {

            // string Date = Row["AppliedDate"].ToString();
            DateTime date = new DateTime();
            Date = (Date.Split(' ')[0]).Replace("/", "-");
            try
            {
                string[] formats = {"M/d/yyyy", "MM/dd/yyyy",
                                "d/M/yyyy", "dd/MM/yyyy",
                                "yyyy/M/d", "yyyy/MM/dd",
                                "M-d-yyyy", "MM-dd-yyyy",
                                "d-M-yyyy", "dd-MM-yyyy",
                                "yyyy-M-d", "yyyy-MM-dd",
                                "M.d.yyyy", "MM.dd.yyyy",
                                "d.M.yyyy", "dd.MM.yyyy",
                                "yyyy.M.d", "yyyy.MM.dd",
                                "M,d,yyyy", "MM,dd,yyyy",
                                "d,M,yyyy", "dd,MM,yyyy",
                                "yyyy,M,d", "yyyy,MM,dd",
                                "M d yyyy", "MM dd yyyy",
                                "d M yyyy", "dd MM yyyy",
                                "yyyy M d", "yyyy MM dd",
                                "m/dd/yyyy hh:mm:ss tt",
                               };
                //DateTime dateValue;

                foreach (string dateStringFormat in formats)
                {
                    if (DateTime.TryParseExact(Date, dateStringFormat, CultureInfo.CurrentCulture, DateTimeStyles.None,
                                               out date))
                    {
                        date.ToShortDateString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;
        }

        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                //string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
                string CurrentPattern = "dd-mm-yyyy";
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true &&

              Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, System.Globalization.CultureInfo.InvariantCulture);
                //date = Convert.ToDateTime(NewDate);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;

        }

        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static List<Dictionary<string, object>> ConvertDataTable(DataTable dtResult)
        {
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dtResult.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dtResult.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return parentRow;
        }

    }
}


