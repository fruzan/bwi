﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using BWI.BL;
using BWI.DataLayer;
using BWI.Admin.DataLayer;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for OnlineReservationHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class OnlineReservationHandler : System.Web.Services.WebService
    {
        string json = "";
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
        DBHandlerDataContext DB = new DBHandlerDataContext();

        [WebMethod(EnableSession = true)]
        public string LoadAllOnlineReservation()
        {
            var List = (from Obj in DB.Trans_Reservations select Obj).ToList();

            var NewList = List.Where(s => s.ReservationType == "Online" && s.Status != "Deleted").OrderByDescending(sid => sid.sid).ToList();

            if (NewList.Count > 0)
            {

                json = jsSerializer.Serialize(NewList);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            }

            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }


            return json;
        }

        [WebMethod(EnableSession = true)]
        public string ReqestCompleted(int BookingSid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Reservation Booking = DB.Trans_Reservations.Single(x => x.sid == BookingSid);
                Booking.Status = "Requested";
                Booking.ReservationType = "Admin";
                // DB.Trans_Reservations.DeleteOnSubmit(Booking);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"Retcode\":\"-1\"}";
            }
            return json;
        }

    }
}
