﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="VehicleInfo.aspx.cs" Inherits="BWI.Admin.VehicleInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="Scripts/VehicleInfo.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <br>

    <table align="right" style="margin-right: 1.8%">
        <tbody>
            <tr>
                <td colspan="6"></td>
                <td style="padding-top: 3.2%"><a href="#">
                    <input type="button" class="popularbtn right" onclick="AddVehicleInfo()" style="margin-right: 3.8%" value="+ Add New" title="Add New Vehicle" />
                </a></td>
                <%--<td style="padding-top: 3.2%">
                    <input type="button" class="grid2btn active" style="cursor: pointer" title="Export To Excel" id="btnExport" onclick="ExportAgentDetailsToExcel()" />
                </td>--%>
            </tr>
        </tbody>
    </table>
    <br />
    <br />
    <br />
    <div class="offset-2">
        <div class="fblueline">
            Add Master
           
            <span class="farrow"></span>
            <a style="color: white" href="VehicleInfo.aspx" title="Vehicle Details"><b>Vehicle</b></a>

        </div>
        <div class="frow1">

            <br />
            <br />

            <div class="table-responsive">
                <div id="tbl_VehicleInfo_wrapper" class="dataTables_wrapper" role="grid">
                    <table class="table table-striped table-bordered dataTable" id="Details" style="width: 100%;">
                        <thead>
                            <tr>
                                <th style="width: 152px;">Sr. No.
                                </th>
                                <th style="width: 174px;">Vehicle Make
                                </th>
                                <th style="width: 153px;">Model
                                </th>
                                <th style="width: 153px;">Reg_Year
                                </th>
                                <th style="width: 153px;">Vehicle Type
                                </th>
                                <th style="width: 153px;">Max Capacity
                                </th>
                                <th style="width: 153px;">Max Baggage
                                </th>
                                <%--<th style="width: 153px;">Image
                                </th>--%>

                                <th style="width: 153px;">Edit 
                                </th>
                            </tr>
                        </thead>
                        <tbody >
                        </tbody>
                    </table>

                </div>
            </div>
            <br>
        </div>
    </div>

    <div class="modal fade" id="AddVehicleInfo" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxes()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel1" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Add Vehicle</b></div>
                        <div class="frow2 scrollmodal">
                            <table id="tblForms2" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                         <td colspan="2">

                                        <center>
                                            <a id="hrefImgVehicle" target="_blank">
                                                <img id="ImgVehicle" /></a></center>

                                    </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Vehicle Manufacturer :</label>
                                            <select id="ddl_VehicleMake" class="form-control">
                                                <option value="0">--Select Vehicle Manufacturer--</option>
                                            </select>

                                        </td>
                                        <td>
                                            <label>Vehicle Type :</label>
                                            <select id="ddl_VehicleType" class="form-control">
                                                <option value="0">--Select Vehicle Type--</option>
                                            </select>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Model :</label>
                                            <input id="txt_ModelName" type="text" class=" form-control" />
                                        </td>
                                        <td>
                                            <label>Registration Year :</label><br />
                                            <input id="txt_RegistrationYear" class="form-control" type="text" />
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Maximum Capacity :</label>
                                            <input id="txt_MaxCapacity" type="text" class="form-control" />
                                        </td>
                                        <td>
                                            <label>Maximum Baggage :</label>
                                            <input id="txt_MaxBaggage" type="text" class="form-control" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Image :</label>
                                            <input id="Vehicle_Image" type="file" class="form-control" onchange="VehicleImage()" />
                                        </td>
                                        <td>
                                            <label>Remarks :</label>
                                            <%--<textarea rows="4" cols="50" id="txt_Remarks" class="form-control">
                                                </textarea>--%>
                                             <textarea rows="4" cols="50" id="txt_Remarks" class="form-control">
                                                </textarea>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                     <input type="button" onclick="AddVehicle()" class="btn-search" value="ADD" title="Add Vehicle Info" id="btn_AddVehicleInfo" /></td>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="Update" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxes()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel1" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Add Vehicle Info</b></div>
                        <div class="frow2 scrollmodal">
                            <table id="tblForms2" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>
                                                Vehicle Manufacturer :</label>
                                            <select id="Addl_VehicleMake" class="form-control">
                                                <option value="0">--Select Vehicle Manufacturer--</option>
                                            </select>

                                        </td>
                                         <td>
                                            <label>Vehicle Type :</label>
                                            <select id="Addl_VehicleType" class="form-control">
                                                <option value="0">--Select Vehicle Type--</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>
                                                Model :</label>
                                            <input id="Atxt_ModelName" type="text" class=" form-control" />
                                        </td>
                                        <td>
                                            <label>Registration Year :</label><br />
                                            <input id="Atxt_RegistrationYear" class="form-control" type="text" />
                                        </td>
                                       
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Maximum Capacity :</label>
                                            <input id="Atxt_MaxCapacity" type="text" class="form-control" />
                                        </td>
                                        <td>
                                            <label>Maximum Baggage :</label>
                                            <input id="Atxt_MaxBaggage" type="text" class="form-control" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Image :</label>
                                            <input id="Af_img" type="file" class="form-control" onchange="VehicleImageUpdate()"/>
                                        </td>
                                         <td>
                                            <label>Remarks :</label>
                                            <%--<textarea rows="4" cols="50" id="txt_Remarks" class="form-control">
                                                </textarea>--%>
                                             <textarea rows="4" cols="50" id="Atxt_Remarks" class="form-control">
                                                </textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">

                                        <center>
                                            <a id="hrefImgVehicleUpdate" target="_blank">
                                                <img id="ImgVehicleUpdate" /></a></center>

                                    </td>
                                    </tr>
                                </tbody>
                            </table>
                             
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="button" onclick="UpdateVehicle()" class="btn-search" value="Update" title="Add Vehicle Info" />
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <input type="hidden" id="hdn" />
</asp:Content>
