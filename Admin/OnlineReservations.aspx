﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="OnlineReservations.aspx.cs" Inherits="BWI.Admin.OnlineReservations" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="Scripts/ManageBooking.js?v=1.2"></script>
        <script type="text/javascript">

        $(function () {
            //$("#datepicker1").datepicker({
            //    changeMonth: true,
            //    changeYear: true,
            //    dateFormat: "d-m-yy"
            //});

            //$("#datepicker2").datepicker({
            //    changeMonth: true,
            //    changeYear: true,
            //    dateFormat: "d-m-yy"
            //});
            //GetAllCustomer()
            GetAll()
        });

    </script>
    
    <script type="text/javascript" src="Scripts/OnlineReservation.js?v=1.2"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <br />
    <%--<table align="left" style="margin-left: 1.8%">
        <tbody>
            <tr>

                <td style="padding-top: 3.2%">
                    <label>From</label>
                    <input type="text" class="form-control datepicker" id="datepicker1" style="cursor: pointer; background: url('../../images/calendar.png') #fff no-repeat right;" placeholder="mm-dd-yyyy">
                </td>
                <td style="padding-top: 3.2%; padding-left: 3.2%">
                    <label>To</label>
                    <input type="text" class="form-control datepicker" id="datepicker2" style="cursor: pointer; background: url('../../images/calendar.png') #fff no-repeat right; padding-left: 25px;" placeholder="mm-dd-yyyy">
                </td>
                <td style="padding-top: 3.2%; padding-left: 3.2%">
                    <label>Customer Name</label>
                    <select id="sel_driver" class="form-control" style="padding-left: 25px;"></select>
                </td>
                <td style="padding-top: 4.4%">

                    <input type="button" class="btn-search" value="Search" id="btn_Search" onclick="Submit();">
                </td>
                <td colspan="2"></td>
            </tr>
        </tbody>
    </table>--%>
    <%--<br />
    <br />
    <br />--%>
    <br />
    <br />
    <div class="offset-2">
        <div class="fblueline">
            Reservation
           
            <span class="farrow"></span>
            <a style="color: white" href="OnlineReservations.aspx" title="Online Reservations Details"><b>Online Reservations</b></a>
            <span class=" yellow  size12" style="float:right">  <a style="cursor: pointer" href="#" onclick="mDelete()"><span class="yellow size12  glyphicon glyphicon-trash" title="Delete Selected Booking" aria-hidden="true" style="cursor: pointer"></span></a></span>
        </div>
        <div class="frow1">

            <br>
            <br>

            <div class="table-responsive">
                <div style="width: auto; height: 740px; overflow: scroll;">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered dataTable" id="Details" style="width: 100%;">
                        <thead>
                            <tr>
                                <td align="center"><b>S.N</b>
                                </td>
                                <td style="width:5px">
                                  <b>  Select </b>
                                </td>
                                <td align="center"><b>Booking No</b>
                                </td>
                                <td align="center"><b>Assigned To</b>
                                </td>
                                <td align="center"><b>Guest Name</b>
                                </td>
                                <td align="center"><b>Reservation Date</b>
                                </td>
                                <td align="center"><b>Service</b>
                                </td>
                                <td align="center"><b>Source</b>
                                </td>
                                <td align="center"><b>Destination</b>
                                </td>
                                <td align="center"><b>Total Fare</b>
                                </td>
                                <td align="center"><b>Edit</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>

                </div>
            </div>
            <br>
        </div>
    </div>
</asp:Content>
