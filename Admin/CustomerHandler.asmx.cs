﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using BWI.BL;
using BWI.DataLayer;
using BWI.Admin.DataLayer;


namespace BWI.Admin
{
    /// <summary>
    /// Summary description for CustomerHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class CustomerHandler : System.Web.Services.WebService
    {

        string json = "";
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
        DBHandlerDataContext DB = new DBHandlerDataContext();
        Trans_Tbl_CustomerMaster Customer = new Trans_Tbl_CustomerMaster();

        [WebMethod(EnableSession = true)]
        public string AddCustomer(string fName, string LastName, string Email, string Mobile, string Gender, string Address, string City, string Country, string Pan, string Pin, string Password)
        {
            var List = (from obj in DB.Trans_Tbl_CustomerMasters where obj.Email == Email select obj).ToList();
            if (List.Count <= 0)
            {
                retCode = CustomerManager.AddCustomer(fName, LastName, Email, Mobile, Gender, Address, City, Country, Pan, Pin, Password);

                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {

                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            else if (List.Count >= 1)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"2\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAllCustomer()
        {
            List<BWI.Trans_Tbl_CustomerMaster> CustomerList = null;

            retCode = CustomerManager.GetAllCustomer(out  CustomerList);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(CustomerList);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');

                json = "{\"Session\":\"1\",\"retcode\":\"1\",\"CustomerTable\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetSingle(Int64 sid)
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();
            List<BWI.Trans_Tbl_CustomerMaster> Data = null;
            Data = (from obj in DB.Trans_Tbl_CustomerMasters where obj.sid == sid select obj).ToList();


            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(Data);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateCustomer(Int64 sid, string fName, string LastName, string Email, string Mobile, string Gender, string Address, string City, string Country, string Pan, string Pin, string Password)
        {

            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_CustomerMaster Customer = DB.Trans_Tbl_CustomerMasters.Single(x => x.sid == sid);
                Customer.FirstName = fName;
                Customer.LastName = LastName;
                Customer.Email = Email;
                Customer.Mobile = Mobile;
                Customer.Gender = Gender;
                Customer.Address = Address;
                Customer.City = City;
                Customer.Country = Country;
                Customer.Pin = Pin;
                Customer.Pan = Pan;
                Customer.Password = Password;
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Delete(Int64 sid)
        {
            try
            {

                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_CustomerMaster Customer = DB.Trans_Tbl_CustomerMasters.Single(x => x.sid == sid);
                DB.Trans_Tbl_CustomerMasters.DeleteOnSubmit(Customer);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }

    }
}
