﻿using BWI.Admin.DataLayer;
using BWI.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for AirLinesHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AirLinesHandler : System.Web.Services.WebService
    {
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";

        [WebMethod(EnableSession = true)]
        public string LoadAirLines()
        {
            List<BWI.Trans_Tbl_AirLine> Data = null;
            retCode = AirLinesManager.LoadAirLines(out Data);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(Data);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteAirLines(Int64 Sid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_AirLine AirLines = DB.Trans_Tbl_AirLines.Single(x => x.Sid == Sid);
                DB.Trans_Tbl_AirLines.DeleteOnSubmit(AirLines);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string UpdateAirLines(Int64 Sid, string Name, string IATA, string ICAO)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_AirLine AirLines = DB.Trans_Tbl_AirLines.Single(x => x.Sid == Sid);
                AirLines.ICAO = ICAO;
                AirLines.IATA = IATA;
                AirLines.Callsign = Name;
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string InsertAirLines(string Name, string IATA, string ICAO)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                List<BWI.Trans_Tbl_AirLine> Data = (from obj in DB.Trans_Tbl_AirLines select obj).ToList();
                bool Exist = Data.Any(x => x.Callsign == Name);
                if (Exist == true)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"3\"}";
                    return json;
                }

                Exist = Data.Any(x => x.IATA == IATA);
                if (Exist == true)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"4\"}";
                    return json;
                }

                Exist = Data.Any(x => x.ICAO == ICAO);
                if (Exist == true)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"5\"}";
                    return json;
                }
                Trans_Tbl_AirLine AirLines = new Trans_Tbl_AirLine();
                AirLines.ICAO = ICAO;
                AirLines.IATA = IATA;
                AirLines.Callsign = Name;
                AirLines.Status = "Activate";
                DB.Trans_Tbl_AirLines.InsertOnSubmit(AirLines);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetSingle(Int64 sid)
        {
            List<BWI.Trans_Tbl_AirLine> Data = null;
            retCode = AirLinesManager.GetSingle(sid, out Data);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(Data);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string ActiveAirLines(Int64 Sid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_AirLine AirLines = DB.Trans_Tbl_AirLines.Single(x => x.Sid == Sid);
                if (AirLines.Status == "Activate")
                {
                    AirLines.Status = "Deactivate";
                }
                else
                {
                    AirLines.Status = "Activate";
                }
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }
    }
}
