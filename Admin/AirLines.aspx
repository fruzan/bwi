﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AirLines.aspx.cs" Inherits="BWI.Admin.AirLines" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="Scripts/AirLines.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />

    <table align="right" style="margin-right: 1.8%">
        <tbody>
            <tr>
                <td colspan="6"></td>

                <td style="padding-top: 3.2%"><a href="#">
                    <input type="button" class="popularbtn right" onclick="Open();" style="margin-right: 3.8%" value="+ Add New" title="Add New AirLines">
                </a></td>
                <%--<td style="padding-top: 3.2%">
                    <input type="button" class="grid2btn active" style="cursor: pointer" title="Export To Excel" id="btnExport" onclick="ExportAgentDetailsToExcel()">
                </td>--%>
            </tr>
        </tbody>
    </table>
    <br>
    <br>
    <br>
    <div class="offset-2">
        <div class="fblueline">
            Add Master
           
            <span class="farrow"></span>
            <a style="color: white" href="AirLines.aspx" title="AirLines Details"><b>AirLines</b></a>

        </div>
        <div class="frow1">

            <br />
            <br />

            <div class="table-responsive" style="width: auto; height: 500px; overflow: scroll;">
                <div class="dataTables_wrapper">

                    <table class="table table-striped table-bordered" id="tbl_StaffDetails" style="width: 100%;">
                        <thead>
                            <tr>
                                <td align="center" style="width: 10%"><b>S.N</b>
                                </td>
                                <td align="center"><b>Name</b>
                                </td>
                                <td align="center"><b>ICAO</b>
                                </td>
                                <td align="center"><b>IATA</b>
                                </td>
                                 <td align="center"><b>Change Status</b>
                                </td>
                                <td align="center"><b>Edit</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody id="Details">
                        </tbody>
                    </table>

                </div>
            </div>
            <br>
        </div>
    </div>

    <div class="modal fade" id="Add" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Add AirLines</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>

                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <label>Name *</label>
                                                        <div>
                                                            <input id="AName" type="text" class="form-control" />
                                                        </div>
                                                    </td>
                                                    <td>

                                                        <label>IATA *</label>
                                                        <div>
                                                            <input id="AIATA" type="text" class="form-control" />
                                                        </div>
                                                    </td>
                                                    <td>

                                                        <label>ICAO *</label>
                                                        <div>
                                                            <input id="AICAO" type="text" class="form-control" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            <div align="right">
                                                <input type="button" class="btn-search" value="Save" id="SubmitAdd" onclick="InsertAirLines()" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#Add').modal('hide')" />
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="Update" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Add AirPort</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>

                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <label>Name *</label>
                                                        <div>
                                                            <input id="Name" type="text" class="form-control" />
                                                        </div>
                                                    </td>
                                                    <td>

                                                        <label>IATA *</label>
                                                        <div>
                                                            <input id="IATA" type="text" class="form-control" />
                                                        </div>
                                                    </td>
                                                    <td>

                                                        <label>ICAO *</label>
                                                        <div>
                                                            <input id="ICAO" type="text" class="form-control" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td>
                                            <div align="right">
                                                <input type="button" class="btn-search" value="Update" id="Submit" onclick="UpdateAirLines();" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#Update').modal('hide')" />
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <input type="hidden" id="hdn" />
</asp:Content>
