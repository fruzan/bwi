﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="OldReservation.aspx.cs" Inherits="BWI.Admin.OldReservation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="Scripts/OldReservation.js"></script>
        <style type="text/css">
        div.scrollingDiv {
            width: auto;
            height: 450px;
            overflow-y: scroll;
        }

        #TermsModal p {
            margin: 0 10px 10px;
        }

        .spacing {
            padding-bottom: 4px;
        }
    </style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    

    <table align="left" style="margin-left: 1.8%;">
        <tbody>
        </tbody>
        
    </table>
   
    <br />
    
    <br />
    <div class="offset-2">
        <div class="fblueline">
            Reports
           
            <span class="farrow"></span>
            <a style="color: white" href="OldReservation.aspx" title="Old Reservation Details"><b>Old Reservations</b></a>

        </div>
        <div class="frow1">

            <br>
           

            <div class="table-responsive">
                <div style="width: auto; height: 740px; overflow: scroll;">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered dataTable" id="Details" style="width: 100%;">
                        <thead>
                            <tr>
                                <td align="center"><b>S.N</b>
                                </td>
                                <td align="center"><b>Res Number</b>
                                </td>
                                <td align="center"><b>Assigned To</b>
                                </td>
                                <td align="center"><b>Guest Name</b>
                                </td>
                                <td align="center"><b>Reservation Date</b>
                                </td>
                                 <td align="center"><b>Flight Time</b>
                                </td>
                                <td align="center"><b>To / From Airport</b>
                                </td>
                                <td align="center"><b>Airport</b>
                                </td>
                                <td align="center"><b>Completed</b>
                                </td>
                                <td align="center"><b>View</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>

                </div>
            </div>
            <br>
        </div>
    </div>
<div class="modal fade bs-example-modal-lg" id="BookingDetails" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:437px">
            <div class="modal-content" style="background-color: aliceblue;">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel2" style="text-align: center">Booking Details</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="WordSection1">
                            <label style="font-size: 18px; margin-left:100px" id="TotalAmount"></label>
                            <table border="1">
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Booking No: </p><p id="BookingNo"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Reservation Date: </p><p id="ResDate"></p></td>
                                </tr>

                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Source: </p><p id="Source"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Destination:</p><p id="Destination"></p></td>
                                </tr>
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Assigned To:</p><p id="AssignedTo"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Guest Name:</p><p id="Name"></p></td>
                                </tr>
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Passenger:</p><p id="Passenger"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Phone No:</p><p id="PhoneNo"></p></td>
                                </tr>
                                <tr>
                                        <td style="padding:8px"><p style="font-weight:bold">Service:</p><p id="Service"></p></td>
                                        <td style="padding:8px"><p style="font-weight:bold">Total Fare:</p><p id="TotalFare"></p></td>
                                </tr>

                                <%--<tr id="Ret_details"><th colspan="2" text-align: center; style="background-color: darkseagreen;font-size: 18px;padding:8px;">Return Details</th></tr>--%>
                                
                            </table><br />
                            <%--<input type="button" id="btnBook" class="cp-btn-style1" onclick="Submit();" style="background-color:red; margin-top:13px" value="Book Now" />
                            <img alt="" id="CircleImage" src="images/circleloadinganimation.gif" width="100" height="100" style="display:none"/>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     
</asp:Content>
