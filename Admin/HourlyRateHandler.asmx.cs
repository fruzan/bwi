﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using BWI.BL;
using BWI.DataLayer;
using BWI.Admin.DataLayer;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for HourlyRateHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class HourlyRateHandler : System.Web.Services.WebService
    {
        string json = "";
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        [WebMethod(EnableSession = true)]
        public string VehicleNameDropDown()
        {
            List<BWI.Trans_tbl_Vehicle_Info> List = null;
            retCode = HourlyRateManager.VehicleNameDropDown(out List);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(List);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"VehicleName\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string AddHourlyRate(string Group, string VehName, string HourlyMinimum, string Service, string UptoHours, string HourlyRate)
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();

            var List = (from obj in DB.Trans_Tbl_HourlyRates where obj.Name == VehName select obj).ToList();

            if (List.Count > 0)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"2\"}";
            }
            else
            {
                retCode = HourlyRateManager.AddHourlyRate(Group, VehName, HourlyMinimum, Service, UptoHours, HourlyRate);
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {

                    json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
                }
                else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAllRates()
        {
            List<BWI.Trans_Tbl_HourlyRate> List = null;
            retCode = HourlyRateManager.GetAllRates(out List);

            if (List.Count > 0)
            {
                if (retCode == DBHelper.DBReturnCode.SUCCESS)
                {
                    json = jsSerializer.Serialize(List);
                    json = json.TrimEnd(']');
                    json = json.TrimStart('[');
                    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"RateDetails\":[" + json + "]}";
                }
                else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                }
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"2\"}";
            }


            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetRate(Int64 sid)
        {
            List<BWI.Trans_Tbl_HourlyRate> List = null;
            retCode = HourlyRateManager.GetRate(sid, out List);


            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(List);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"RateDetails\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateRate(Int64 sid, string Group, string VehName, string HourlyMinimum, string Service, string UptoHours, string HourlyRate)
        {
            retCode = HourlyRateManager.UpdateRate(sid, Group, VehName, HourlyMinimum, Service, UptoHours, HourlyRate);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteRate(Int64 sid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_HourlyRate HourlyRate = DB.Trans_Tbl_HourlyRates.Single(x => x.sid == sid);
                DB.Trans_Tbl_HourlyRates.DeleteOnSubmit(HourlyRate);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }
    }
}
