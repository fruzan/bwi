﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using BWI.BL;
using BWI.DataLayer;
using BWI.Admin.DataLayer;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for VehicleInfoHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class VehicleInfoHandler : System.Web.Services.WebService
    {
        string json = "";
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();


        [WebMethod(EnableSession = true)]
        public string LoadAll()
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                var List = (DB.Proc_VehicalInfo()).ToList();
                if (List.Count > 0)
                {
                    json = jsSerializer.Serialize(List);
                    json = json.TrimEnd(']');
                    json = json.TrimStart('[');
                    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"VehicleMake\":[" + json + "]}";
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }



        [WebMethod(EnableSession = true)]
        public string VehicleMakeDropDown()
        {
            List<BWI.Trans_Tbl_Manufacture> List = null;
            retCode = VehicleInfoManager.VehicleMakeDropDown(out List);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(List);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"VehicleMake\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string VehicleTypeDropDown()
        {
            List<BWI.Trans_Tbl_CarType> List = null;
            retCode = VehicleInfoManager.VehicleTypeDropDown(out List);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(List);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"VehicleType\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string AddVehicleInfo(string ManufacturerName, string ModelName, string RegistrationYear, string VehicleType, int Capacity, int Baggage, string Remarks, string Image_Path)
        {
            //GetRandomNo();
            string UniqueId = "VI-" + GenerateRandomNumber();
            //string UniqueId = HttpContext.Current.Session["RandomNo"].ToString();

            retCode = VehicleInfoManager.AddVehicleInfo(UniqueId, ManufacturerName, ModelName, RegistrationYear, VehicleType, Capacity, Baggage, Remarks, Image_Path);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }


        [WebMethod(EnableSession = true)]
        public string UpdateVehicleInfo(Int64 sid, string ManufacturerName, string ModelName, string RegistrationYear, string VehicleType, int Capacity, int Baggage, string Remarks, string Image_Path)
        {


            retCode = VehicleInfoManager.UpdateVehicleInfo(sid, ManufacturerName, ModelName, RegistrationYear, VehicleType, Capacity, Baggage, Remarks, Image_Path);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }


        [WebMethod(EnableSession = true)]
        public string Delete(Int64 sid)
        {
            try
            {

                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_tbl_Vehicle_Info Vehicle = DB.Trans_tbl_Vehicle_Infos.Single(x => x.sid == sid);
                DB.Trans_tbl_Vehicle_Infos.DeleteOnSubmit(Vehicle);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }



        //[WebMethod(EnableSession = true)]
        //public string GetRandomNo()
        //{
        //    string VisaCode = "VI-" + GenerateRandomNumber();
        //    string logoFileName = VisaCode;
        //    Session["RandomNo"] = logoFileName;

        //    return logoFileName;

        //}
        [WebMethod(EnableSession = true)]
        public static int GenerateRandomNumber()
        {
            int rndnumber = 0;
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();
            return rndnumber;
        }
    }
}
