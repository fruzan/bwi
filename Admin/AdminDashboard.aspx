﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AdminDashboard.aspx.cs" Inherits="BWI.Admin.AdminDashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="Scripts/Dashboard.js"></script>
    <script type="text/javascript" src="Scripts/ManageBooking.js"></script>
    
     <style type="text/css">
        tr:nth-child(even) {
    background-color: rgb(224, 220, 220);
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="clearfix">
                <%--<ul class="d-status">
                  <li class="popwidth2"><a href="#" id="notifications" data-content="No new messages...">Notifications<span class="d-not">0</span></a></li>
                  <li><a href="#" id="messages" data-content=">
                                <div class='msgbox offset-0'>
                                    <img src='../images/dash/avatar3.jpg' alt='' width='30' class='left margright10 roundav'/>
                                    <span class='opensans size13 dark'>Hello, concgratulations.. </span><br/>
                                    <span class='opensans size12'>Thank's for this awesome theme</span>
                                </div>
                                <div class='msgbox2 offset-0'>
                                    <img src='../images/dash/no-avatar.jpg' alt='' width='30' class='left margright10 roundav'/>
                                    <span class='opensans size13 dark'>Hello Sir, need customisation </span><br/>
                                    <span class='opensans size12'>Are you available for freelance work?</span>
                                </div>					
                                <div class='msgbox offset-0'>
                                    <img src='../images/dash/avatar2.jpg' alt='' width='30' class='left margright10 roundav'/>
                                    <span class='opensans size13 dark'>Extramaziordinary</span><br/>
                                    <span class='opensans size12'>Bought it 5 min ago and i'm loving it</span>
                                </div>
                                " data-original-title="<span class='dark bold'>Notifications</span>">Messages<span class="d-mes active">3</span></a></li>
                  <li class="popwidth"><a href="#" id="tasks" data-content="
                                    <ul class='checklist3'>
                                        <li>10:00 AM - Update to v2.0 <button type='button' class='close poscls' data-dismiss='alert' aria-hidden='true'>&times;</button></li>
                                        <li>28.12.13 - Withdrawal - <b>$500.00</b> <button type='button' class='close poscls' data-dismiss='alert' aria-hidden='true'>&times;</button></li>
                                        <li>Sent Email to <b>dave@gmail.com</b> <button type='button' class='close poscls' data-dismiss='alert' aria-hidden='true'>&times;</button></li>
                                        <li>Recieved Email from <b>ana@yahoo.com</b><button type='button' class='close poscls' data-dismiss='alert' aria-hidden='true'>&times;</button></li>
                                        <li>25.12.13 - Sent Christas Newsletter <button type='button' class='close poscls' data-dismiss='alert' aria-hidden='true'>&times;</button></li>
                                        <li><b>Antony</b> booked 1 room's - New York <button type='button' class='close poscls' data-dismiss='alert' aria-hidden='true'>&times;</button></li>
                                    </ul>
                                    <div class='clearfix'></div>
                                " >Tasks<span class="d-tas active">6</span></a></li>
                </ul>--%></div>
                <%--<div class="line2"></div>--%>
               <%-- <div class="alert alert-warning fade in margtop20">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <strong>Notification!</strong> Best check yo self, you're not looking too good. 
                </div>--%>
                <div class="alert alert-warning fade in margtop20">
                 <%-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>--%>
                  <strong><span onclick="GetGridData('DeletedReservation')" style="cursor:pointer" title="Deleted Reservation's List">Deleted Reservations</span></strong>
                </div>
                <div class="row">
                  <div class="col-md-3" onclick="GetGridData('SerToday')">
                    <div class="dbox-visitors">
                      <div class="orangebox"> <a><img src="../images/dash/ServiceToday.png" alt=""/></a> </div>
                      <span class="size11 mt-5">Reservations for Service Today</span><br/>
                      <span class="countvisitors lato size24"></span>
                      <div class="clearfix"></div>
                      <div class="dboxarrow"></div>
                    </div>
                  </div>
                  <div class="col-md-3" onclick="GetGridData('SerTomorrow')">
                    <div class="dbox-visitors">
                      <div class="cyanbox"> <a><img src="../images/dash/ServiceTomorrow.png" alt=""/></a> </div>
                      <span class="size11 mt-5">Reservations for Service Tommorow</span><br/>
                      <span class="lato size24"><span class="countrevenue"></span></span>
                      <div class="clearfix"></div>
                      <div class="dboxarrow"></div>
                    </div>
                  </div>
                  <div class="col-md-3" onclick="GetGridData('ResToday')">
                    <div class="dbox-visitors">
                      <div> <a href="#"><img src="../images/dash/booking.png" style="margin-bottom: 10px;" alt=""/></a> </div>
                      <span class="size11 mt-5">Reservations Made Today</span><br/>
                      <span class="countemail lato size24"></span>
                      <div class="clearfix"></div>
                      <div class="dboxarrow"></div>
                    </div>  
                  </div>
                  <div class="col-md-3" onclick="GetGridData('Unassign')">
                    <div class="dbox-visitors">
                      <div class="darkbox"> <a href="#"><img src="../images/dash/Unassigned.png" alt=""/></a> </div>
                      <span class="size11 mt-5">Unassigned For Tommorow</span><br/>
                      <span class="countbookings lato size24"></span>
                      <div class="clearfix"></div>
                      <div class="dboxarrow"></div>
                    </div>
                  </div>
                </div>
            <br />
            <div class="row">
                <div class="col-md-3"  onclick="GetGridData('Amount')">
                    <div class="dbox-visitors">
                      <div class="darkbox"> <a><img src="../images/dash/Amount.jpg" alt=""/></a> </div>
                      <span class="size11 mt-5">Today's Reservation Amount</span><br/>
                      <span class="lato size24">$ <span class="TodayAmount"></span></span>
                      <div class="clearfix"></div>
                      <div class="dboxarrow"></div>
                    </div>
                  </div>
               <div class="col-md-3" onclick="GetGridData('Online')">
                    <div class="dbox-visitors">
                      <div class="darkbox"> <a><img src="../images/dash/online.png" alt=""/></a> </div>
                      <span class="size11 mt-5">Online Reservation</span><br/>
                      <span class="lato size24"><span class="OnlineReservation"></span></span>
                      <div class="clearfix"></div>
                      <div class="dboxarrow"></div>
                    </div>
                  </div>
                    <div class="col-md-3" onclick="GetGridData('Upcoming')">
                    <div class="dbox-visitors">
                      <div class="darkbox"> <a><img src="../images/dash/upcoming.png" alt=""/></a> </div>
                      <span class="size11 mt-5">Upcoming Reservation</span><br/>
                      <span class="lato size24"><span class="UpcomingReservation"></span></span>
                      <div class="clearfix"></div>
                      <div class="dboxarrow"></div>
                    </div>
                  </div>
                    <div class="col-md-3" onclick="GetGridData('Pending')">
                    <div class="dbox-visitors">
                      <div class="darkbox"> <a><img src="../images/dash/pending.png" alt=""/></a> </div>
                      <span class="size11 mt-5">Pending Reservation</span><br/>
                      <span class="lato size24"><span class="PendingReservation"></span></span>
                      <div class="clearfix"></div>
                      <div class="dboxarrow"></div>
                    </div>
                  </div>
    </div>
                <div class="clearfix"></div>
                <br/>
    <div class="line2"></div>
    <div class="offset-2">
        <br /><br />
        <div class="fblueline" id="GridTitle">Reservations for Service Today</div>
        <div class="frow1">

            <br />
            <br />
            <div class="table-responsive">
                <div style="width: auto; height:400px; overflow: scroll;">

                    <table class="table table-striped table-bordered dataTable" id="ReservationDetails" style="width: 100%;">
                        <thead>
                            <tr>
                                <td align="center" style="width: 10%"><b>S.N</b>
                                </td>
                                <td align="center"><b>Booking No</b>
                                </td>
                                <td align="center"><b>Service Type</b>
                                </td>
                                <td align="center"><b>Resevation Date</b>
                                </td>
                                 <td align="center"><b>Resevation Time</b>
                                </td>
                                <td align="center"><b>Total Fare</b>
                                </td>
                                <td align="center"><b>Status</b>
                                </td>
                                <td align="center"><b>Authorized Res</b>
                                </td>
                                <td align="center"><b>Update | Confirm</b>
                                </td>
                                <td align="center"><b>Cancel | Delete</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody >
                        </tbody>
                    </table>

                </div>
            </div>
            <br>
        </div>
    </div>

    <div>
        <h2>Send Mail</h2>
        <input class="form-control" id="email" placeholder="Email To" type="text" /><br/>
        <input class="form-control" id="Sub" placeholder="Subject" type="text" /><br/>
        <textarea placeholder="Message" id="text" class="form-control"></textarea><br/>
        <input type="button" onclick="SendMail()" value="Send" />
    </div>

    <div class="modal fade" id="AssignDriver" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Confirm Booking</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>Assigned Driver *</label>
                                            <select id="Sel_Driver" class="form-control">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">

                                            <div align="right">
                                                <input type="button" class="btn-search" value="Confirm" onclick="Assign()" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#AssignDriver').modal('hide')" />
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
       <div class="modal fade bs-example-modal-lg" id="BookingDetails" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:50%">
            <div class="modal-content" style="background-color: aliceblue;">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel2" style="text-align: center">Booking Details</h4>
                </div>
                <div class="modal-body">
                    <a style="float: right;cursor:pointer" target="_blank" onclick="PrintInvoice()">Print</a>
                    <input type="hidden" id="InVoice" />
                    <br />
                    <div class="scrollmodal">
                        <div class="WordSection1">
                            <label style="font-size: 18px; margin-left:100px" id="TotalAmount"></label>
                            <table border="1">
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Booking No: </p><p id="BookingNo"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Reservation Date: </p><p id="ResDate"></p></td>
                                </tr>
                                 <tr>
                                    <td style="width: 50%;padding:8px;"><p style="font-weight:bold">Time *:   <span style="font-family: monospace;" id="ResTime"></span></p>  <label style="font-size:smaller;font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif">(Note :Here 24-hour time formatis to  represent time without a.m. or p.m. For example, 1:00 p.m. becomes 13:00 in 24-hour format.)</label>  
                                      
                                        
                                    </td>
                                    <td style="padding:8px"><p style="font-weight:bold">Guest Name:</p><p id="Name"></p>
                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Source: </p><p id="Source"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Destination:</p><p id="Destination"></p></td>
                                </tr>
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Assigned To:</p><p id="AssignedTo"></p></td>
                                   <td style="padding:8px"><p style="font-weight:bold">Passenger:</p><p id="Passenger"></p></td>
                                </tr>
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Phone No:</p><p id="PhoneNo"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Service:</p><p id="Service"></p></td>
                                </tr>
                                <tr>
                                        <td colspan="2" style="padding:8px"><p style="font-weight:bold">Total Fare:</p><p id="TotalFare"></p></td>
                                </tr>

                                <%--<tr id="Ret_details"><th colspan="2" text-align: center; style="background-color: darkseagreen;font-size: 18px;padding:8px;">Return Details</th></tr>--%>
                                
                            </table><br />
                            <%--<input type="button" id="btnBook" class="cp-btn-style1" onclick="Submit();" style="background-color:red; margin-top:13px" value="Book Now" />
                            <img alt="" id="CircleImage" src="images/circleloadinganimation.gif" width="100" height="100" style="display:none"/>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="hdn" />
                <%--<span class="lato size18">Traffic</span> <br/>
                <br/>
                <ul class="visitsboolets mt10 left margleft15">
                  <li class="size14 mt-3 dark">Visits</li>
                  <li><span class="blue"></span><a href="#">Search Traffic</a></li>
                  <li><span class="red"></span><a href="#">Referral Traffic</a></li>
                  <li><span class="green"></span><a href="#">Direct Traffic</a></li>
                  <li><span class="yellow"></span><a href="#">Campaigns</a></li>
                  <li><span class="grey"></span><a href="#">Today’s traffic</a></li>
                </ul>
                <ul class="pagination right">
                  <li class="active"><a href="#">Day</a></li>
                  <li><a href="#">Week</a></li>
                  <li><a href="#">Month</a></li>
                </ul>
                <div class="clearfix"></div>
                <br/>
                <div class="bgstats">
                  <div class="visitsnr"> 4,000<br/>
                    3,000<br/>
                    2,000<br/>
                    1,000 </div>
                  <ul class="mdates">
                    <li>Aug 15</li>
                    <li>Aug 22</li>
                    <li>Aug 29</li>
                    <li>Sep 5</li>
                  </ul>
                  <span class="stats">1,4,4,7,5,9,10</span> </div>
                <br/>
                <div class="clearfix"></div>
                <br/>
                <span class="lato size18">Today’s visits</span>
                <div class="clearfix"></div>
                <div class="col-md-6 stats2container ohidden  offset-0"> <span class="stats2"></span> </div>
                <div class="col-md-6"> 
                  <script type="text/javascript" src="../assets/js/jquery.easy-pie-chart.js"></script>
                  <div class="chartcontainer">
                    <div class="wh33percent left">
                      <div class="chart" data-percent="69"><span class="countbouncerate">69</span>%</div>
                      <div class="chartext">Bounce Rate</div>
                    </div>
                    <div class="wh33percent left textcenter">
                      <div class="chart2 center" data-percent="81"><span class="countnewvisits">81</span>%</div>
                      <div class="chartext center">New Visits</div>
                    </div>
                    <div class="wh33percent left textright">
                      <div class="chart3 right" data-percent="33"><span class="countsearchtrafic">33</span>%</div>
                      <div class="clearfix"></div>
                      <div class="chartext right">Search Traffic</div>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                <br/>
                <div class="line2"></div>
                <br/>--%>
               <%-- <div class="col-md-6 offset-0"> <span class="lato size18">8,173 people visited this site</span><br/>
                  <br/>
                  <div class="col-md-12 peoplevisits offset-0">
                    <div class="col-md-4">Visits<br/>
                      <span class="size18 dark">10,090</span><br/>
                      <span class="cvisits"></span></div>
                    <div class="col-md-4">Unique Visitors<br/>
                      <span class="size18 dark">8,173</span><br/>
                      <span class="cvisits"></span></div>
                    <div class="col-md-4">Previews<br/>
                      <span class="size18 dark">16,462</span><br/>
                      <span class="cpreview"></span></div>
                    <div class="clearfix"></div>
                    <br/>
                    <div class="col-md-4">Pages / Visit<br/>
                      <span class="size18 dark">1.63</span><br/>
                      <span class="cvisits2"></span></div>
                    <div class="col-md-4">Avg. Visit Duration<br/>
                      <span class="size18 dark">00:01:39</span><br/>
                      <span class="cvisits2"></span></div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-md-6 offset-0">
                  <div class="col-md-12 pdfix">
                    <div class="col-md-6 offset-0">
                      <div class="pdfix"> <span class="lato size18">Visits by Browser</span><br/>
                        <br/>
                        <div class="table-responsive">
                          <table class="table">
                            <tr class="active">
                              <td>Chrome</td>
                              <td class="textright lblue">5,633</td>
                            </tr>
                            <tr class="">
                              <td>Firefox</td>
                              <td class="textright lblue">3,274</td>
                            </tr>
                            <tr class="active">
                              <td>Safari</td>
                              <td class="textright lblue">756</td>
                            </tr>
                            <tr class="">
                              <td>Internet Explorer</td>
                              <td class="textright lblue">354</td>
                            </tr>
                            <tr class="active">
                              <td>Opera</td>
                              <td class="textright lblue">105</td>
                            </tr>
                          </table>
                        </div>
                        <a href="#" class="lato size12 lblue right">More info</a> </div>
                    </div>
                    <div class="col-md-6 offset-0">
                      <div class="pdfix"> <span class="lato size18">Mobile overview</span><br/>
                        <br/>
                        <div class="table-responsive">
                          <table class="table">
                            <tr class="active">
                              <td>Desktop</td>
                              <td class="textright green">10,047</td>
                            </tr>
                            <tr class="">
                              <td>Mobile</td>
                              <td class="textright green">110</td>
                            </tr>
                            <tr class="active">
                              <td>Tablet</td>
                              <td class="textright green">86</td>
                            </tr>
                          </table>
                        </div>
                        <a href="#" class="lato size12 lblue right">More info</a> </div>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                <br/>
                <div class="line2"></div>
                <br/>
                <div class="col-md-12 offset-0">
                  <div class="col-md-6 offset-0"> 
                    <!-- Nav tabs -->
                    <ul class="nav navigation-tabs2">
                      <li class="active"><a href="#tab-newtopic" data-toggle="tab">Topics <span class="badge cbhide">6</span></a></li>
                      <li><a href="#tab-comments" data-toggle="tab">Comments <span class="badge cbhide">1</span></a></li>
                      <li><a href="#tab-blogcomments" data-toggle="tab">Blog <span class="badge cbhide">2</span></a></li>
                    </ul>
                    <div class="tab-content4"> 
                      <!-- Tab 1 -->
                      <div class="tab-pane active" id="tab-newtopic">
                        <div class="table-responsive">
                          <div class="fixedtopic">
                            <table class="tablehover">
                              <tr>
                                <td><img src="../images/dash/no-avatar.jpg" alt=""/></td>
                                <td><span class="dark">Load Container Content</span><br/>
                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et dictum urna. Donec et tortor ullamcorper, tempor ante eu, rhoncus ligula. </td>
                                <td class="cwth"><input type="checkbox" class=""></td>
                              </tr>
                            </table>
                            <div class="line3"></div>
                            <table class="tablehover">
                              <tr>
                                <td><img src="../images/dash/no-avatar.jpg" alt=""/></td>
                                <td><span class="dark">Customize Twitter api & add sign up/login</span><br/>
                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et dictum urna. Donec et tortor ullamcorper, tempor ante eu, rhoncus ligula. </td>
                                <td class="cwth"><input type="checkbox" class=""></td>
                              </tr>
                            </table>
                            <div class="line3"></div>
                            <table class="tablehover">
                              <tr>
                                <td><img src="../images/dash/no-avatar.jpg" alt=""/></td>
                                <td><span class="dark">Changing top Nav text colors</span><br/>
                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et dictum urna. Donec et tortor ullamcorper, tempor ante eu, rhoncus ligula. </td>
                                <td class="cwth"><input type="checkbox" class=""></td>
                              </tr>
                            </table>
                            <div class="line3"></div>
                            <table class="tablehover">
                              <tr>
                                <td><img src="../images/dash/no-avatar.jpg" alt=""/></td>
                                <td><span class="dark">Load Container Content</span><br/>
                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et dictum urna. Donec et tortor ullamcorper, tempor ante eu, rhoncus ligula. </td>
                                <td class="cwth"><input type="checkbox" class=""></td>
                              </tr>
                            </table>
                            <div class="line3"></div>
                            <table class="tablehover">
                              <tr>
                                <td><img src="../images/dash/no-avatar.jpg" alt=""/></td>
                                <td><span class="dark">Customize Twitter api & add sign up/login</span><br/>
                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et dictum urna. Donec et tortor ullamcorper, tempor ante eu, rhoncus ligula. </td>
                                <td class="cwth"><input type="checkbox" class=""></td>
                              </tr>
                            </table>
                            <div class="line3"></div>
                            <table class="tablehover">
                              <tr>
                                <td><img src="../images/dash/no-avatar.jpg" alt=""/></td>
                                <td><span class="dark">Changing top Nav text colors</span><br/>
                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et dictum urna. Donec et tortor ullamcorper, tempor ante eu, rhoncus ligula. </td>
                                <td class="cwth"><input type="checkbox" class=""></td>
                              </tr>
                            </table>
                          </div>
                          <br/>
                          <div class="input-group">
                            <input type="text" class="form-control cfctrl">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Send</button>
                            </span> </div>
                          <!-- /input-group --> 
                        </div>
                      </div>
                      <!-- End of Tab 1 --> 
                      
                      <!-- Tab 2 -->
                      <div class="tab-pane" id="tab-comments">
                        <div class="table-responsive">
                          <table class="tablehover">
                            <tr>
                              <td><img src="../images/dash/no-avatar.jpg" alt=""/></td>
                              <td><span class="dark">Customize Twitter api & add sign up/login</span><br/>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et dictum urna. Donec et tortor ullamcorper, tempor ante eu, rhoncus ligula. </td>
                              <td class="cwth"><input type="checkbox" class=""></td>
                            </tr>
                          </table>
                          <br/>
                          <div class="input-group">
                            <input type="text" class="form-control cfctrl">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Send</button>
                            </span> </div>
                        </div>
                      </div>
                      <!-- End of Tab 2 --> 
                      
                      <!-- Tab 3 -->
                      <div class="tab-pane" id="tab-blogcomments">
                        <div class="table-responsive">
                          <table class="tablehover">
                            <tr>
                              <td><img src="../images/dash/no-avatar.jpg" alt=""/></td>
                              <td><span class="dark">Changing top Nav text colors</span><br/>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et dictum urna. Donec et tortor ullamcorper, tempor ante eu, rhoncus ligula. </td>
                              <td class="cwth"><input type="checkbox" class=""></td>
                            </tr>
                          </table>
                          <div class="line3"></div>
                          <table class="tablehover">
                            <tr>
                              <td><img src="../images/dash/no-avatar.jpg" alt=""/></td>
                              <td><span class="dark">Customize Twitter api & add sign up/login</span><br/>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum et dictum urna. Donec et tortor ullamcorper, tempor ante eu, rhoncus ligula. </td>
                              <td class="cwth"><input type="checkbox" class=""></td>
                            </tr>
                          </table>
                          <br/>
                          <div class="input-group">
                            <input type="text" class="form-control cfctrl">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Send</button>
                            </span> </div>
                        </div>
                      </div>
                      <!-- End of Tab 3 --> 
                    </div>
                  </div>
                  <div class="col-md-6 cpadding01"> <span class="lato size18">Server stats</span><br/>
                    <br/>
                    <div class="progress2 progress-striped">
                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> </div>
                    </div>
                    <span class="lato size13 grey">Disk space usage: 1,746.5 / 50,000 MB</span> <br/>
                    <br/>
                    <br/>
                    <div class="progress2 progress-striped">
                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" style="width: 55%"></div>
                    </div>
                    <span class="lato size13 grey">Monthly Bandwidth Transfer: 14,706.1 / 30.000</span> <br/>
                    <br/>
                    <br/>
                    <div class="progress2 progress-striped">
                      <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%"> </div>
                    </div>
                    <span class="lato size13 grey">Subdomains: 7/15</span> <br/>
                    <br/>
                    <br/>
                    <div class="progress2 progress-striped active">
                      <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%"> </div>
                    </div>
                    <span class="lato size13 grey">All SQL Databases : 6/8</span> <br/>
                    <br/>
                    <br/>
                    <div class="progress2 progress-striped active">
                      <div class="progress-bar progress-bar-black" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%"> </div>
                    </div>
                    <span class="lato size13 grey">Email Accounts: 8 / 10</span> </div>
                </div>--%>



</asp:Content>
