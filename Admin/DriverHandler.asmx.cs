﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using BWI.BL;
using BWI.DataLayer;
using BWI.Admin.DataLayer;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for DriverHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]




    public class DriverHandler : System.Web.Services.WebService, IHttpHandler
    {
        string json = "";
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        [WebMethod(EnableSession = true)]
        public string GetAllDriver()
        {
            List<BWI.Proc_GetAllDriverResult> Driver = null;
            retCode = DriverManager.GetAllDriver(out Driver);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(Driver);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"DriverDetails\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string AddDriver(string sid, string Fname, string Lname, string Gender, string Mobile, string Address, string Country, string City, string Pincode, string Email, string Password, string Profile, string Lic1, string Lic2, string Percentage)
        {
            retCode = DriverManager.AddUpdateDriver(sid, Fname, Lname, Gender, Mobile, Address, Country, City, Pincode, Email, Password, Profile, Lic1, Lic2, Percentage);


            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateDriver(string sid, string Fname, string Lname, string Gender, string Mobile, string Address, string Percentage, string Country, string City, string Pincode, string Email, string Password, string Profile, string Lic1, string Lic2)
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();

            var Data = (from obj in DB.Trans_Tbl_Logins where obj.Sid == Convert.ToInt64(sid) select obj).ToList();


            if (Profile == "")
            {
                Profile = Data[0].sProfileMapPath;
            }

            if (Lic1 == "")
            {
                Lic1 = Data[0].sLicense1MapPath;
            }

            if (Lic2 == "")
            {
                Lic2 = Data[0].sLicense2MapPath;
            }



            retCode = DriverManager.AddUpdateDriver(sid, Fname, Lname, Gender, Mobile, Address, Country, City, Pincode, Email, Password, Profile, Lic1, Lic2, Percentage);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }


        [WebMethod(EnableSession = true)]
        public string DriverDocument(string sid)
        {
            string Profile = "";
            string Lic1 = "";
            string Lic2 = "";

            DBHandlerDataContext DB = new DBHandlerDataContext();

            var Data = (from obj in DB.Trans_Tbl_Logins where obj.Sid == Convert.ToInt64(sid) select obj).ToList();


            if (Profile == "")
            {
                Profile = Data[0].sProfileMapPath;
            }

            if (Lic1 == "")
            {
                Lic1 = Data[0].sLicense1MapPath;
            }

            if (Lic2 == "")
            {
                Lic2 = Data[0].sLicense2MapPath;
            }

            json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Profile\":\"" + Profile + "\",\"Lic1\":\"" + Lic1 + "\",\"Lic2\":\"" + Lic2 + "\"}";

            return json;
        }




        [WebMethod(EnableSession = true)]
        public string DeleteDriver(Int64 sid)
        {
            retCode = DriverManager.DeleteDriver(sid);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GenerateRandomNumber()
        {
            int rndnumber = 0;
            string json = "";
            Random rnd = new Random((int)DateTime.Now.Ticks);
            rndnumber = rnd.Next();
            string Code = "Driver-" + rndnumber;
            return json = "{\"Session\":\"1\",\"Code\":\"" + Code + "\"}";
        }

        public bool IsReusable
        {
            get { throw new NotImplementedException(); }
        }

        public void ProcessRequest(HttpContext context)
        {
            throw new NotImplementedException();
        }


    }

}
