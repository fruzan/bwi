﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BWI.Admin.DataLayer;
using BWI.BL;
using System.Web.Script.Serialization;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for Coupons
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Coupons : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        string json = "";

        [WebMethod(EnableSession = true)]
        public string LoadCoupons()
        {
            List<BWI.Trans_Tbl_Offer> Data = null;
            retCode = CouponManager.LoadCoupons(out Data);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(Data);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string DeleteCoupons(Int64 Sid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_Offer Offer = DB.Trans_Tbl_Offers.Single(x => x.Sid == Sid);
                DB.Trans_Tbl_Offers.DeleteOnSubmit(Offer);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string UpdateCoupons(Int64 Sid, string Name, string Code, string Percents)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_Offer Offer = DB.Trans_Tbl_Offers.Single(x => x.Sid == Sid);
                Offer.Name = Name;
                Offer.Code = Code;
                Offer.Percents = Percents;
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string InsertCoupon(string Name, string Code, string Percents)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                List<BWI.Trans_Tbl_Offer> Data = (from obj in DB.Trans_Tbl_Offers select obj).ToList();
                //bool Exist = Data.Any(x => x.Callsign == Name);
                //if (Exist == true)
                //{
                //    json = "{\"Session\":\"1\",\"retCode\":\"3\"}";
                //    return json;
                //}

                //Exist = Data.Any(x => x.IATA == IATA);
                //if (Exist == true)
                //{
                //    json = "{\"Session\":\"1\",\"retCode\":\"4\"}";
                //    return json;
                //}

                //Exist = Data.Any(x => x.ICAO == ICAO);
                //if (Exist == true)
                //{
                //    json = "{\"Session\":\"1\",\"retCode\":\"5\"}";
                //    return json;
                //}
                Trans_Tbl_Offer Offer = new Trans_Tbl_Offer();
                Offer.Name = Name;
                Offer.Code = Code;
                Offer.Percents = Percents;
                DB.Trans_Tbl_Offers.InsertOnSubmit(Offer);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetSingle(Int64 sid)
        {
            List<BWI.Trans_Tbl_Offer> Data = null;
            retCode = CouponManager.GetSingle(sid, out Data);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(Data);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Arr\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string ActiveAirLines(Int64 Sid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_AirLine AirLines = DB.Trans_Tbl_AirLines.Single(x => x.Sid == Sid);
                if (AirLines.Status == "Activate")
                {
                    AirLines.Status = "Deactivate";
                }
                else
                {
                    AirLines.Status = "Activate";
                }
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            return json;
        }
    }
}
