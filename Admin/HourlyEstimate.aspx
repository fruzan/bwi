﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="HourlyEstimate.aspx.cs" Inherits="BWI.Admin.HourlyEstimate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script src="Scripts/HourlyRate.js"></script>
    <br />

    <table align="right" style="margin-right: 1.8%">
        <tbody>
            <tr>
                <td colspan="6"></td>

                <td style="padding-top: 3.2%"><a href="#">
                    <input type="button" class="popularbtn right" onclick="AddDialog()" style="margin-right: 3.8%" value="+ Add New" title="Add New Hourly Rate">
                </a></td>
                <%--<td style="padding-top: 3.2%">
                    <input type="button" class="grid2btn active" style="cursor: pointer" title="Export To Excel" id="btnExport" onclick="ExportAgentDetailsToExcel()">
                </td>--%>
            </tr>
        </tbody>
    </table>
    <br>
    <br>
    <br>
    <div class="offset-2">
        <div class="fblueline">
            Rate Master
           
            <span class="farrow"></span>
            <a style="color: white" href="HourlyEstimate.aspx" title="Hourly Rate Details"><b>Hourly Rate Set Up</b></a>

        </div>
        <div class="frow1">

            <br />
            <br />

            <div class="table-responsive">
                <div id="tbl_StaffDetails_wrapper" class="dataTables_wrapper">

                    <table class="table table-striped table-bordered" id="tbl_StaffDetails" style="width: 100%;">
                        <thead>
                            <tr>
                                <td align="center" style="width: 10%"><b>S.N</b>
                                </td>
                                <td align="center" style="width:10%"><b>Name</b>
                                </td>
                                 <%--<td align="center" style="width:10%"><b>Rate Group</b>
                                </td>--%>
                                 <td align="center" ><b>Minimum Hours</b>
                                </td>
                                <%-- <td align="center" style="width:10%"><b>Service</b>
                                </td>
                                <td align="center" style="width:10%"><b>Upto (Hours)</b>
                                </td>--%>
                                 <td align="center" style="width:10%"><b>Hourly Rate</b>
                                </td>
                                 
                                <td align="center" style="width:25%"><b>Edit | Delete</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody id="RateDetails">
                            
                        </tbody>
                    </table>

                </div>
            </div>
            <br>
        </div>
    </div>

    <div class="modal fade" id="AddDilogBox" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Rate Set Up</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                       <%-- <td>
                                            <label>Group *</label>

                                              <select id="ddl_SelectGroup" class="form-control">
                                                <option value="-" selected="selected">Regular</option>
                                                  <option value="-">Holiday</option>
                                            </select>
                                        </td>--%>
                                        <td>
                                            <label>Name :</label>
                                            <select id="ddl_SelectName" class="form-control"/>
                                        </td>
                                        <td>
                                            <label>Minimum Hours :</label>
                                            <input id="txt_HourlyMinimum" type="text" class="form-control" />
                                        </td>
                                    </tr>
                                       <tr>
                                             <%--<td>
                                            <label>Service:</label>
                                            <input id="txt_Service" type="text" class="form-control" />
                                        </td>
                                        <td>
                                            <label>Upto (Hours):</label>
                                            <input id="txt_UptoHours" type="text" class="form-control" />
                                        </td>--%>
                                        <td>
                                            <label>Hourly Rate :</label>
                                            <input id="txt_HourlyRate" type="text" class="form-control" />
                                        </td>
                                  
                                    </tr>
                                    <tr>
                                        <td colspan="3">

                                            <div align="right">
                                                <input type="button" class="btn-search" value="Save" onclick="AddHourlyRate()" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#AddDilogBox').modal('hide')" />
                                            </div>
                                        </td>
                                    </tr>



                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

<div class="modal fade" id="UpdateDilogBox" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Rate Set Up</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                 <tbody>
                                    <tr>
                                        <%--<td>
                                            <label>Group *</label>

                                              <select id="Select_Group" class="form-control">
                                                <option value="Regular" selected="selected">Regular</option>
                                                  <option value="Holiday">Holiday</option>
                                            </select>
                                        </td>--%>
                                        <td>
                                            <label>Name :</label>
                                            <select id="ddl_SelectVehName" class="form-control"/>
                                        </td>
                                        <td>
                                            <label>Hourly Minimum :</label>
                                            <input id="txtHourlyMinimum" type="text" class="form-control" />
                                        </td>
                                    </tr>
                                       <tr>
                                           <%--<td>
                                            <label>Service:</label>
                                            <input id="txtService" type="text" class="form-control" />
                                        </td>
                                        <td>
                                            <label>Upto (Hours):</label>
                                            <input id="txtUptoHours" type="text" class="form-control" />
                                        </td>--%>
                                        <td>
                                            <label>Hourly Rate :</label>
                                            <input id="txtHourlyRate" type="text" class="form-control" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <div align="right">
                                                <input type="button" class="btn-search" onclick="UpdateRate()" value="Update" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#UpdateDilogBox').modal('hide')" />
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</asp:Content>
