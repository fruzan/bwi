﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="BookingList.aspx.cs" Inherits="BWI.Admin.BookingList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
         $(function () {
             $("#datepicker1").datepicker({
                 changeMonth: true,
                 changeYear: true,
                 dateFormat: "mm-dd-yy"
             });

             $("#datepicker2").datepicker({
                 changeMonth: true,
                 changeYear: true,
                 dateFormat: "mm-dd-yy"
             });
         });

    </script>
    <style type="text/css">
        div.scrollingDiv {
            width: auto;
            height: 450px;
            overflow-y: scroll;
        }

        #TermsModal p {
            margin: 0 10px 10px;
        }

        .spacing {
            padding-bottom: 4px;
        }
    </style>
    <style type="text/css">
        .cp-btn-style1 {
    font-family: 'Exo 2', sans-serif;
    font-size: 18px;
    line-height: 18px;
    font-weight: 400;
    color: #fff;
    display: inline-block;
    text-align: center;
    padding: 13px 20px;
    max-width: 150px;
    position: relative;
    overflow: hidden;
    z-index: 11;
    border: none;
    box-shadow: none;
    text-decoration: none;
    border-bottom: 5px solid #e77d7d;
}
    </style>
    <style type="text/css">
        tr:nth-child(even) {
    background-color: rgb(224, 220, 220);
}
    </style>
    <script type="text/javascript" src="Scripts/Reservation.js"></script>
    <script type="text/javascript" src="Scripts/ManageBooking.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    


<br />
    <span style="margin-left: 1.8%" id="TopSpan"></span>
    <br/>
    <table align="left" style="margin-left: 1.8%">
        <tbody>
            <tr>

                <td style="padding-top: 3.2%">
                    <label>From</label>
                    <input type="text" class="form-control datepicker" id="datepicker1" style="cursor: pointer; background: url('../../images/calendar.png') #fff no-repeat right;" placeholder="mm-dd-yyyy"/>
                </td>
                <td style="padding-top: 3.2%; padding-left: 3.2%">
                    <label>To</label>
                    <input type="text" class="form-control datepicker" id="datepicker2" style="cursor: pointer; background: url('../../images/calendar.png') #fff no-repeat right; padding-left: 25px;" placeholder="mm-dd-yyyy"/>
                </td>
                <td style="padding-top: 3.2%;padding-left:15px">
                     <label>Status</label>
                   <select class="form-control" id="Sel_Status">
                       <option value="0">Select Status</option>
                       <option value="All">All</option>
                       <option value="Cancelled">Cancelled</option>
                        <option value="Confirmed">Confirmed</option>
                       <option value="Completed">Completed</option>
                        <option value="Deleted">Deleted</option>
                       <option value="Requested" selected="selected">Requested</option>
                        <option value="Requested Online">Requested Online</option>
                       <option value="Paid">Paid</option>
                       <option value="Unpaid">Unpaid</option>
                       <option value="Unassigned">Unassigned</option>
                   </select>
                </td>
                <td style="padding-top: 4.4%">

                    <input type="button" class="btn-search" value="Search" id="btn_Search" onclick="SearchReservation();"/>
                </td>
                <td style="padding-top: 4.4%">

                    <input type="button" class="btn-search" value="All" id="btn_All" onclick=" LoadReservations()"/>
                </td>
                <td colspan="2"></td>
            </tr>
        </tbody>
    </table>
    <br /><br /><br />
    <table align="right" style="margin-right: 1.8%">
        <tbody>
            <tr>
                <%--<td colspan="4"></td>--%>
                 
                
                <td style="padding-top: 3.2%"><a href="AirPortReservation.aspx">

                    <input type="button" class="popularbtn right" style="margin-right: 3.8%" value="+ Add AirPort Reservation" title="Add AirPort Reservation">
                </a></td>
                <td style="padding-top: 3.2%"><a href="PointToPoint.aspx">
                    <input type="button" class="popularbtn right" style="margin-right: 3.8%" value="+ Add Point To Point Reservation" title="Add Point To Point Reservation">
                </a></td>
                <td style="padding-top: 3.2%"><a href="HourReservation.aspx">
                    <input type="button" class="popularbtn right" style="margin-right: 3.8%" value="+ Add Hourly Reservation" title="Add Hourly Reservation">
                </a></td>
                 <td style="padding-top: 3.2%"><a href="ShuttleService.aspx">
                    <input type="button" class="popularbtn right" style="margin-right: 3.8%" value="+ Two Stop Shuttle Reservation" title="Two Stop Shuttle Reservation">
                </a></td>
            </tr>
        </tbody>
    </table>
    <br /><br /><br />
    <div class="offset-2">
        <br /><br />
        <div class="fblueline">
            Reservation
           
            <span class="farrow"></span>
            <a style="color: white" href="DriverDetails.aspx" title="Reservation Details"><b>View Reservation</b></a>

            <span class="yellow size12" style="float:right"> <a style="cursor: pointer" onclick="mCompleteBooking()" href="#"><span class="yellow size12  fa fa-check" title="Completed Selected Booking"></span></a> | <a style="cursor: pointer"  onclick="mCancel()" href="#"><span class="yellow size12 fa fa-times" title="Cancel Selected Booking"></span></a> | <a style="cursor: pointer" href="#" onclick="mDelete()"><span class="yellow size12  glyphicon glyphicon-trash" title="Delete Selected Booking" aria-hidden="true" style="cursor: pointer"></span></a></span>
        </div>
        <div class="frow1">

            <br />
            <br />
            <div class="table-responsive">
                <div >

                    <table class="table table-striped table-bordered dataTable" id="DriverDetails" style="width: 100%;">
                        <thead>
                            <tr>
                                <td align="center" style="width: 10%"><b>S.N</b>
                                </td>
                                <td align="center" style="width: 10%"><b>Select</b>
                                </td>
                               
                                <td align="center"><b>Booking No</b>
                                </td>
                                 <td align="center"><b>Guest Name</b>
                                </td>
                                <td align="center"><b>Service Type</b>
                                </td>
                                <td align="center"><b>Resevation Date</b>
                                </td>

                                <td align="center"><b>Total Fare</b>
                                </td>
                                <td align="center"><b>Status</b>
                                </td>
                                <td align="center"><b>Authorized Res</b>
                                </td>

                                <td align="center"><b>Update | Confirm | Cancel | Delete | Completed</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody >
                        </tbody>
                    </table>

                </div>
            </div>
            <br>
        </div>
    </div>

    <div class="modal fade" id="AssignDriver" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Confirm Booking</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>Assigned Driver *</label>
                                            <select id="Sel_Driver" class="form-control">
                                            </select>
                                        </td>

                                    </tr>


                                    <tr>
                                        <td colspan="2">

                                            <div align="right">
                                                <input type="button" class="btn-search" value="Confirm" onclick="Assign()" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#AssignDriver').modal('hide')" />
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <input type="hidden" id="hdn" />
    <div class="modal fade bs-example-modal-lg" id="BookingDetails" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width:50%">
            <div class="modal-content" style="background-color: aliceblue;">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel2" style="text-align: center">Booking Details</h4>
                </div>
                <div class="modal-body">
                    <a style="float: right;cursor:pointer" target="_blank" onclick="PrintInvoice()">Print</a>
                    <input type="hidden" id="InVoice" />
                    <br />
                    <div>
                        <div class="WordSection1">
                            <label style="font-size: 18px; margin-left:100px" id="TotalAmount"></label>
                            <table border="1">
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Booking No: </p><p id="BookingNo"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Reservation Date: </p><p id="ResDate"></p></td>
                                </tr>
                                 <tr>
                                    <td style="width: 50%;padding:8px;"><p style="font-weight:bold">Time *:   <span style="font-family: monospace;" id="ResTime"></span></p>  <label style="font-size:smaller;font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif">(Note :Here 24-hour time formatis to  represent time without a.m. or p.m. For example, 1:00 p.m. becomes 13:00 in 24-hour format.)</label>  
                                      
                                        
                                    </td>
                                    <td style="padding:8px"><p style="font-weight:bold">Guest Name:</p><p id="Name"></p>
                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Source: </p><p id="Source"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Destination:</p><p id="Destination"></p></td>
                                </tr>
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Assigned To:</p><p id="AssignedTo"></p></td>
                                   <td style="padding:8px"><p style="font-weight:bold">Passenger:</p><p id="Passenger"></p></td>
                                </tr>
                                <tr>
                                    <td style="padding:8px"><p style="font-weight:bold">Phone No:</p><p id="PhoneNo"></p></td>
                                    <td style="padding:8px"><p style="font-weight:bold">Service:</p><p id="Service"></p></td>
                                </tr>
                                <tr>
                                        <td colspan="2" style="padding:8px"><p style="font-weight:bold">Total Fare:</p><p id="TotalFare"></p></td>
                                </tr>

                                <%--<tr id="Ret_details"><th colspan="2" text-align: center; style="background-color: darkseagreen;font-size: 18px;padding:8px;">Return Details</th></tr>--%>
                                
                            </table><br />
                            <%--<input type="button" id="btnBook" class="cp-btn-style1" onclick="Submit();" style="background-color:red; margin-top:13px" value="Book Now" />
                            <img alt="" id="CircleImage" src="images/circleloadinganimation.gif" width="100" height="100" style="display:none"/>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
