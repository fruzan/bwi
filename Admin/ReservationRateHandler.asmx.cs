﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BWI.Admin.DataLayer;
using BWI.BL;
using System.Web.Script.Serialization;
using BWI.DataLayer;
using System.Text;
using System.Net;
using System.Net.Mail;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for ReservationRateHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ReservationRateHandler : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public string GetRate(string Tab, int sid)
        {
            JavaScriptSerializer Js = new JavaScriptSerializer();
            DBHandlerDataContext DB = new DBHandlerDataContext();
            string json = "";
            string ArrCar = "";
            try
            {

                if (Tab == "1" || Tab == "2")
                {

                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                    var List = (from Obj in DB.Trans_Tbl_DistanceRates where Obj.CarType_Sid == sid select Obj).ToList();
                    if (List.Count > 0)
                    {
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"Tab\":\"1\",\"Arr\":[" + ArrCar + "]}";
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                }

                else
                {
                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                    var List = (from Obj in DB.Trans_Tbl_HourlyRates where Obj.CarType_Sid == sid select Obj).ToList();
                    if (List.Count > 0)
                    {
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"Tab\":\"3\",\"Arr\":[" + ArrCar + "]}";
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                }
            }
            catch
            {
                json = "{\"Retcode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetAllVehicle(string Tab, int sid)
        {
            JavaScriptSerializer Js = new JavaScriptSerializer();
            DBHandlerDataContext DB = new DBHandlerDataContext();
            string json = "";
            string ArrCar = "";
            try
            {
                var List = (from obj in DB.Trans_Tbl_CarTypes select obj).ToList();

                if (List.Count > 0)
                {
                    ArrCar = Js.Serialize(List);
                    ArrCar = ArrCar.TrimStart('[');
                    ArrCar = ArrCar.TrimEnd(']');
                    json = "{\"Retcode\":\"1\",\"Tab\":\"1\",\"Arr\":[" + ArrCar + "]}";
                }
                else
                {
                    json = "{\"Retcode\":\"2\"}";
                }

            }
            catch
            {
                json = "{\"Retcode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string GetAirLines()
        {
            JavaScriptSerializer Js = new JavaScriptSerializer();
            DBHandlerDataContext DB = new DBHandlerDataContext();
            string json = "";
            string ArrCar = "";
            try
            {
                var List = (from obj in DB.Trans_Tbl_AirLines where obj.Status == "Activate" orderby obj.Callsign select obj).ToList();

                if (List.Count > 0)
                {
                    ArrCar = Js.Serialize(List);
                    ArrCar = ArrCar.TrimStart('[');
                    ArrCar = ArrCar.TrimEnd(']');
                    json = "{\"Retcode\":\"1\",\"Tab\":\"1\",\"Arr\":[" + ArrCar + "]}";
                }
                else
                {
                    json = "{\"Retcode\":\"2\"}";
                }

            }
            catch
            {
                json = "{\"Retcode\":\"0\"}";
            }
            return json;
        }

    }
}
