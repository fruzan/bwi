﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for DriverUploader
    /// </summary>
    public class DriverUploader : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            try
            {
                string dirFullPath = HttpContext.Current.Server.MapPath("~/DriverDocument/");
                string RandomCode = System.Convert.ToString(context.Request.QueryString["RandomCode"]);
                string Id = System.Convert.ToString(context.Request.QueryString["id"]);

                string[] files;
                int numFiles;
                files = System.IO.Directory.GetFiles(dirFullPath);
                numFiles = files.Length;
                numFiles = numFiles + 1;
                Guid DocumentName = Guid.NewGuid();
                string str_image = "";
                string path = context.Request.Form[0];
                foreach (string s in context.Request.Files)
                {
                    HttpPostedFile file = context.Request.Files[s];
                    string fileName = file.FileName;
                    string fileExtension = file.ContentType;

                    if (!string.IsNullOrEmpty(fileName))
                    {
                        fileExtension = Path.GetExtension(fileName);
                        //str_image = "Driver_" + DocumentName.ToString() + fileExtension;
                        string name = RandomCode.ToString() + Id.ToString();
                        str_image = name + fileExtension;
                        string pathToSave_100 = HttpContext.Current.Server.MapPath("~/DriverDocument/") + str_image;
                        if (path == " ")
                        {
                            file.SaveAs(pathToSave_100);
                        }
                        else
                        {
                            string Type = System.Convert.ToString(context.Request.QueryString["Type"]);
                            DBHandlerDataContext DB = new DBHandlerDataContext();
                            Trans_Tbl_Login DriverDetails = DB.Trans_Tbl_Logins.Single(x => x.Sid == Convert.ToInt64(Id));
                            if (Type == "Profile")
                                DriverDetails.sProfileMapPath = str_image;
                            else if (Type == "Lic1")
                                DriverDetails.sLicense1MapPath = str_image;
                            else
                                DriverDetails.sLicense2MapPath = str_image;



                            DB.SubmitChanges();
                            FileInfo f = new FileInfo(pathToSave_100);
                            file.SaveAs(pathToSave_100);
                            //if (f.Exists)
                            //{
                            //    f.Delete();

                            //}
                        }
                    }
                }
                //  database record update logic here  ()

                context.Response.Write(str_image);
            }
            catch (Exception ac)
            {

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}