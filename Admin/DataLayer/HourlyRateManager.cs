﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.BL;
using System.Configuration;

namespace BWI.Admin.DataLayer
{
    public class HourlyRateManager
    {
        public static DBHelper.DBReturnCode VehicleNameDropDown(out List<BWI.Trans_tbl_Vehicle_Info> List)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            List = null;
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_CarType Manufacture = new Trans_Tbl_CarType();

                var Query = from Obj in DB.Trans_tbl_Vehicle_Infos select Obj;

                List = Query.ToList();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }

        public static DBHelper.DBReturnCode AddHourlyRate(string Group, string VehName, string HourlyMinimum, string Service, string UptoHours, string HourlyRate)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                //var CarType_Sid = (from obj in DB.Trans_Tbl_CarTypes where obj.Name == VehName select obj.sid).ToList();
                var CarType_Sid = (from obj in DB.Trans_tbl_Vehicle_Infos where obj.Model == VehName select obj.sid).ToList();
                Trans_Tbl_HourlyRate HourlyRates = new Trans_Tbl_HourlyRate();

                HourlyRates.Name = VehName;
                HourlyRates.RateGroup = Group;
                HourlyRates.HourlyMinimum = HourlyMinimum;
                HourlyRates.ServiceUpto = UptoHours;
                HourlyRates.HourlyRate = HourlyRate;
                HourlyRates.Service = Service;
                HourlyRates.CarType_Sid = CarType_Sid[0];

                DB.Trans_Tbl_HourlyRates.InsertOnSubmit(HourlyRates);

                DB.SubmitChanges();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }

        public static DBHelper.DBReturnCode GetAllRates(out List<BWI.Trans_Tbl_HourlyRate> List)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            List = null;
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_HourlyRate HourlyRate = new Trans_Tbl_HourlyRate();

                var Query = from Obj in DB.Trans_Tbl_HourlyRates select Obj;

                List = Query.ToList();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }

        public static DBHelper.DBReturnCode GetRate(Int64 sid, out List<BWI.Trans_Tbl_HourlyRate> List)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            List = null;
            try
            {
                string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

                DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
                Trans_Tbl_HourlyRate HourlyRate = new Trans_Tbl_HourlyRate();


                var Query = from Obj in DB.Trans_Tbl_HourlyRates where Obj.sid == sid select Obj;

                List = Query.ToList();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }

        public static DBHelper.DBReturnCode UpdateRate(Int64 sid, string Group, string VehName, string HourlyMinimum, string Service, string UptoHours, string HourlyRate)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();

                Trans_Tbl_HourlyRate HourlyRates = DB.Trans_Tbl_HourlyRates.Single(x => x.sid == sid);

                //Trans_Tbl_HourlyRate HourlyRates = DB.Trans_Tbl_AirLines.Single(x => x.sid == sid);

                HourlyRates.Name = VehName;
                HourlyRates.RateGroup = Group;
                HourlyRates.HourlyMinimum = HourlyMinimum;
                HourlyRates.ServiceUpto = UptoHours;
                HourlyRates.HourlyRate = HourlyRate;
                HourlyRates.Service = Service;

                //DB.Trans_Tbl_HourlyRates.InsertOnSubmit(HourlyRates);

                DB.SubmitChanges();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }
    }
}