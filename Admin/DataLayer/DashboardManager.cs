﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Net;
using System.Collections;

namespace BWI.Admin.DataLayer
{
    public class DashboardManager
    {
        const string SERVER = "EvidentNet";
        const string URL = "http://www.evidentnet.com";
        const string EmailTemplate = "<html><body>" +
                                    "<div align=\"center\">" +
                                    "<table style=\"width:602px;border:silver 1px solid\" cellspacing=\"0\" cellpadding=\"0\">" +
                                    "<tr>" +

                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;color:White; FONT-SIZE: 14px;background-color:Black\"><span>#title#</span></td>" +
                                    "</tr>" +

                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\"><a href='http://www.bwishuttleservice.com/'>http://www.bwishuttleservice.com</a></td>" +
                                    "</tr>" +

                                     "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\"><a href='mailto:reservation@bwishuttleservice.com'>reservation@bwishuttleservice.com</a></td>" +
                                    "</tr>" +

                                    "<tr>" +
                                    "<td width=\"100%\" style=\"vertical-align:top;text-align:left;padding:6px 5px 6px 5px;height:50px;FONT-FAMILY: Verdana;FONT-SIZE: 14px\">Message: #text#</td>" +
                                    "</tr>" +

                                    "</table>" +
                                    "</div>" +
                                    "</html></body>";

        public static bool SendEmail(string To, string sMessageTitle, string Sub, string sMessage)
        {
            string msg = "";
            try
            {
                MailAddress fromAddress = new MailAddress("reservation@limoallaround.com");
                string sMailMessage = EmailTemplate.Replace("#title#", sMessageTitle);
                sMailMessage = sMailMessage.Replace("#text#", sMessage);
                MailMessage message = new MailMessage();
                SmtpClient smtpClient = new SmtpClient();
                message.From = fromAddress;
                message.To.Add(To);
                //message.CC.Add("limoallaround@gmail.com");
                message.Subject = Sub;
                message.IsBodyHtml = true;
                message.Body = sMailMessage;
                smtpClient.Host = "relay-hosting.secureserver.net";   //-- Donot change.
                smtpClient.Port = 25; //--- Donot change
                smtpClient.EnableSsl = false;//--- Donot change
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new System.Net.NetworkCredential("reservation@limoallaround.com", "Limo@Test123");
                //smtpClient.Credentials = new System.Net.NetworkCredential("support@limoallaround.net", "asdrc@123");
                smtpClient.Send(message);
                return true;
            }
            catch (SmtpException ex)
            {
                msg = ex.Message;
                return false;
            }

            /*try
            {
                string sMailMessage = EmailTemplate.Replace("#title#", sMessageTitle);
                //sMailMessage = sMailMessage.Replace("#name#", sName);
                //sMailMessage = sMailMessage.Replace("#email#", sEmail);
                sMailMessage = sMailMessage.Replace("#text#", sMessage);
                System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                mailMsg.From = new MailAddress("support@limoallaround.com", "LimoAllAround");
                mailMsg.To.Add(To);
                mailMsg.Subject = Sub;
                mailMsg.IsBodyHtml = true;
                mailMsg.Body = sMailMessage;
                //SmtpClient mailObj = new SmtpClient("smtp.tmauto.in", 587);
                SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                //SmtpClient mailObj = new SmtpClient("smtp.tmauto.in", 587);
                mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                //mailObj.Credentials = new NetworkCredential("support@tmauto.in", "admin123");
                mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                mailObj.EnableSsl = false;
                mailObj.Send(mailMsg);
                mailMsg.Dispose();
                return true;

            }
            catch
            {
                return false;
            }*/

        }
    }
}