﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.BL;
using System.Configuration;

namespace BWI.Admin.DataLayer
{
    public class VehicleManager
    {
        public static DBHelper.DBReturnCode AddManufacturer(string Mac_Name)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            try
            {

                BWI.DataLayer.GlobalDefaultTransfers Global = new BWI.DataLayer.GlobalDefaultTransfers();
                Global = (BWI.DataLayer.GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];

            DBHandlerDataContext DB = new DBHandlerDataContext();
            
            Trans_Tbl_Manufacture Manufacture = new Trans_Tbl_Manufacture();

            string UpdatedBy = Global.sEmail;
            string Date = DateTime.Now.ToString("dd/MM/yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);

            Manufacture.Name = Mac_Name;
            Manufacture.Date = Date;
            Manufacture.UpdatedBy = UpdatedBy;

            DB.Trans_Tbl_Manufactures.InsertOnSubmit(Manufacture);
            DB.SubmitChanges();

            return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }
           
        }

        public static DBHelper.DBReturnCode GetAllManufacturer(out List<BWI.Trans_Tbl_Manufacture> List)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            List = null;
            try
            {

                //CUT.DataLayer.GlobalDefaultTransfers Global = new CUT.DataLayer.GlobalDefaultTransfers();
                //Global = (CUT.DataLayer.GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];

                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_Manufacture Manufacture = new Trans_Tbl_Manufacture();

                var Query = from Obj in DB.Trans_Tbl_Manufactures select Obj;

               List = Query.ToList();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }
    }
}