﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.BL;
using System.Configuration;

namespace BWI.Admin.DataLayer
{
    public class DistanceEstimateManager
    {
        public static DBHelper.DBReturnCode VehicleNameDropDown(out List<BWI.Trans_Tbl_CarType> List)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            List = null;
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_CarType Manufacture = new Trans_Tbl_CarType();

                var Query = from Obj in DB.Trans_Tbl_CarTypes select Obj;

                List = Query.ToList();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }

        public static DBHelper.DBReturnCode AddDistanceRate(string Group, string VehName, string BaseDistance, string BaseCharge, string CostDistance, string MilesDistance, string UptoMiles)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();

                //var CarType_Sid = (from obj in DB.Trans_Tbl_CarTypes where obj.Name == VehName select obj.sid).ToList();

                var CarType_Sid = (from obj in DB.Trans_tbl_Vehicle_Infos where obj.Model == VehName select obj.sid).ToList();

                Trans_Tbl_DistanceRate DistanceRate = new Trans_Tbl_DistanceRate();
                DistanceRate.CarType_Sid = CarType_Sid[0];
                DistanceRate.RateGroup = Group;
                DistanceRate.Name = VehName;
                DistanceRate.BaseDistance = BaseDistance;
                DistanceRate.BaseCharge = BaseCharge;
                DistanceRate.CostPerDistance = CostDistance;
                DistanceRate.MilesPerDistance = MilesDistance;
                DistanceRate.UptoMiles = UptoMiles;

                DB.Trans_Tbl_DistanceRates.InsertOnSubmit(DistanceRate);

                DB.SubmitChanges();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }

        public static DBHelper.DBReturnCode GetAllRates(out List<BWI.Trans_Tbl_DistanceRate> List)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            List = null;
            try
            {

                //CUT.DataLayer.GlobalDefaultTransfers Global = new CUT.DataLayer.GlobalDefaultTransfers();
                //Global = (CUT.DataLayer.GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];

                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_DistanceRate DistanceRate = new Trans_Tbl_DistanceRate();

                var Query = from Obj in DB.Trans_Tbl_DistanceRates select Obj;

                List = Query.ToList();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }

        public static DBHelper.DBReturnCode GetRate(Int64 sid, out List<BWI.Trans_Tbl_DistanceRate> List)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            List = null;
            try
            {
                string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

                DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
                Trans_Tbl_DistanceRate DistanceRate = new Trans_Tbl_DistanceRate();


                var Query = from Obj in DB.Trans_Tbl_DistanceRates where Obj.sid == sid select Obj;

                List = Query.ToList();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }

        public static DBHelper.DBReturnCode UpdateRate(Int64 sid, string Group, string VehName, string BaseDistance, string BaseCharge, string CostDistance, string MilesDistance, string UptoMiles)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();

                Trans_Tbl_DistanceRate DistanceRate = DB.Trans_Tbl_DistanceRates.Single(x => x.sid == sid);

                //Trans_Tbl_HourlyRate HourlyRates = DB.Trans_Tbl_AirLines.Single(x => x.sid == sid);

                DistanceRate.Name = VehName;
                DistanceRate.RateGroup = Group;
                DistanceRate.BaseDistance = BaseDistance;
                DistanceRate.BaseCharge = BaseCharge;
                DistanceRate.CostPerDistance = CostDistance;
                DistanceRate.MilesPerDistance = MilesDistance;
                DistanceRate.UptoMiles = UptoMiles;

                //DB.Trans_Tbl_HourlyRates.InsertOnSubmit(HourlyRates);

                DB.SubmitChanges();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }

        public static DBHelper.DBReturnCode ModelNameDropDown(out List<BWI.Trans_tbl_Vehicle_Info> List)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            List = null;
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_tbl_Vehicle_Info Manufacture = new Trans_tbl_Vehicle_Info();

                var Query = from Obj in DB.Trans_tbl_Vehicle_Infos select Obj;

                List = Query.ToList();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }
    }
}