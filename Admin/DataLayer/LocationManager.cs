﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.BL;
using System.Configuration;
using System.Globalization;
using BWI.DataLayer;
using System.Data;
using System.Data.SqlClient;

namespace BWI.Admin.DataLayer
{
    public class LocationManager
    {

        public static DBHelper.DBReturnCode InsertLocation(string address, string Latitude, string Longitude)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            try
            {
                int Insert = DB.Proc_Trans_AddLocation(address, Latitude, Longitude);

                if (Insert == 1)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else if (Insert == 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNOAFFECT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

        public static DBHelper.DBReturnCode LocationLoadAll(out List<BWI.Proc_Trans_LocationLoadAllResult> CarsData)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            CarsData = null;

            try
            {

                var query = DB.Proc_Trans_LocationLoadAll();
                CarsData = query.ToList();
                if (CarsData.Count > 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retCode;
        }

        public static DBHelper.DBReturnCode LoadByKey(int sid, out List<BWI.Proc_Trans_GetLocationByKeyResult> Data)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            Data = null;

            try
            {

                var query = DB.Proc_Trans_GetLocationByKey(sid);
                Data = query.ToList();
                if (Data.Count > 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retCode;
        }

        public static DBHelper.DBReturnCode DeleteLocation(int sid)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            try
            {
                int Insert = DB.Proc_Trans_DelteLocation(sid);

                if (Insert == 1)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else if (Insert == 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNOAFFECT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

        public static DBHelper.DBReturnCode UpdateLocation(int sid, string address, string Latitude, string Longitude)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            try
            {
                int Insert = DB.Proc_Trans_UpdLocation(sid,address, Latitude, Longitude);

                if (Insert == 1)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else if (Insert == 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNOAFFECT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

        public static DBHelper.DBReturnCode ActiveLocation(Int64 Sid, string Status)
        {
            int RowWffected;
            DBHelper.DBReturnCode retCode;
            SqlParameter[] sqlParams = new SqlParameter[2];
            sqlParams[0] = new SqlParameter("@sid", Sid);
            sqlParams[1] = new SqlParameter("@Status", Status);
            retCode = DBHelper.ExecuteNonQuery("Proc_Trans_Tbl_LocationUpdateByStatus", out RowWffected, sqlParams);
            return retCode;
        }
    }
}