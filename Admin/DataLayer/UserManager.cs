﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.BL;
using System.Configuration;

namespace BWI.Admin.DataLayer
{
    public class UserManager
    {
        public static DBHelper.DBReturnCode Add(string FN, string Lanme, string Mobile, string Email, string Password, string UserType, string Active, string Address, string Country, string City, string PinCode)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            string RoleId = "";
            if(UserType=="Dispatcher")
            {
                RoleId = "4";
            }
            else if (UserType == "Admin")
            {
                RoleId = "1";
            }
            Int64 sid = 0;

            try
            {
                int Insert = DB.Proc_TransRegistration(sid, FN, Lanme, "", "", "UnMaried", "", Address, "", "", Country, Address, City, "", City, PinCode, "", Mobile, "", Email, Password, "", "", UserType, RoleId, "", "", Email, true, "", "", "", "", "", "");

                if (Insert == 1)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else if (Insert == 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNOAFFECT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }


        public static DBHelper.DBReturnCode Update(Int64 sid, string FN, string Lanme, string Mobile, string Email, string Password, string UserType, string Active, string Address, string Country, string City, string PinCode)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();

            try
            {
                int Insert = DB.Proc_TransRegistration(sid, FN, Lanme, "", "", "UnMaried", "", Address, "", "", Country, Address, City, "", City, PinCode, "", Mobile, "", Email, Password, "", "", "Admin", "1", "", "", Email, true, "", "", "", "", "","");

                if (Insert == 1)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else if (Insert == 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNOAFFECT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

    }
}