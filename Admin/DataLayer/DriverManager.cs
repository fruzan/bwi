﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.BL;
using System.Configuration;
using System.Data;
using System.ComponentModel;
using System.Threading;

namespace BWI.Admin.DataLayer
{
    public class DriverManager
    {
        public static DBHelper.DBReturnCode GetAllDriver(out List<BWI.Proc_GetAllDriverResult> ServiceType)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();


            ServiceType = null;
            try
            {
                var Service = DB.Proc_GetAllDriver();
                ServiceType = Service.ToList();
                if (ServiceType.Count > 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retCode;
        }

        public static DBHelper.DBReturnCode AddUpdateDriver(string sid, string Fname, string Lname, string Gender, string Mobile, string Address, string Country, string City, string Pincode, string Email,string Password, string Profile, string Lic1, string Lic2, string Percentage)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();


            Int64 Sid = Convert.ToInt64(sid);

            try
            {
                int Insert = DB.Proc_TransRegistration(Sid, Fname, Lname, Gender, "", "Maried", "", Address, "", Pincode, Country, Address, City, "", City, Pincode, "", Mobile, "", Email, Password, "", "", "Driver", "3", "", "", Email, true, "", "", Profile, Lic1, Lic2, Percentage);

            

                if (Insert == 1)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else if (Insert == 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNOAFFECT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

        public static DBHelper.DBReturnCode DeleteDriver(Int64 sid)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();

                Trans_Tbl_Login Driver = DB.Trans_Tbl_Logins.Single(x => x.Sid == sid);

                //Trans_Tbl_HourlyRate HourlyRates = DB.Trans_Tbl_AirLines.Single(x => x.sid == sid);

                Driver.sIsActive = false;

                //DB.Trans_Tbl_HourlyRates.InsertOnSubmit(HourlyRates);

                DB.SubmitChanges();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }

        public static DBHelper.DBReturnCode GetAllDriverDataTable(string From, string To, string DriverName, out DataTable dtDriverList)
        {
            dtDriverList = null;
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            DBHandlerDataContext DB = new DBHandlerDataContext();
            List<Trans_Reservation> NewList = new List<Trans_Reservation>();

            var List = (from Obj in DB.Trans_Reservations where Obj.DriverSid == Convert.ToInt64(DriverName) && Obj.Status == "Completed" select Obj).ToList();
            if (From != "")
            {
                DateTime dFrom = ConvertDateTime(From);
                DateTime dTo = ConvertDateTime(To);
                NewList = List.Where(s => ConvertDateTime(s.ReservationDate) >= dFrom && ConvertDateTime(s.ReservationDate) <= dTo).ToList();
            }
            else
            {
                NewList = List;
            }

            if (NewList.Count > 0)
            {
                string DriverSid = (NewList[0].DriverSid).ToString();
                Trans_Tbl_Login Login = DB.Trans_Tbl_Logins.Single(x => x.Sid == Convert.ToInt64(DriverSid));
                Decimal Percentage = Convert.ToDecimal(Login.Percentage);

                Decimal TotalFare = 0;
                Decimal PercentageOfFare = 0;
                Decimal DriversPayOut = 0;
                Decimal Tip = 0;
                Decimal Fare = 0;
                foreach (var item in NewList)
                {
                    TotalFare = TotalFare + Convert.ToDecimal(item.TotalFare);
                    Fare = Fare + Convert.ToDecimal(item.Fare);
                    Tip = Tip + Convert.ToDecimal(item.GratuityAmount);
                }
                Percentage = Percentage / 100;
                PercentageOfFare = (Percentage * Fare);
                DriversPayOut = TotalFare - PercentageOfFare;
                //DriversPayOut = DriversPayOut + Tip;

                decimal Totalf = Math.Round(TotalFare, 2);

                HttpContext.Current.Session["DriverAmount"] = Math.Round(TotalFare, 2) + " " + Math.Round(PercentageOfFare, 2) + " " + Math.Round(DriversPayOut, 2) + " " + Math.Round(Tip, 2) + " " + Math.Round(Fare, 2);
                dtDriverList = ConvertToDatatable(NewList);
                retCode = DBHelper.DBReturnCode.SUCCESS;
            }
            else
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =   TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                // string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;

                string CurrentPattern = "mm-dd-yyyy";
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;
        }
    }
}