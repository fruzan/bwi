﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.BL;
using System.Configuration;
using System.Globalization;
using BWI.DataLayer;

namespace BWI.Admin.DataLayer
{
    public static class CarManager
    {
        public static DBHelper.DBReturnCode InsertCar(string Name, string Description)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
          
            Random generator = new Random();
            int r = generator.Next(100000, 1000000);
            String InsertDate = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
            string UniqueCode = "CA-" + r;
            GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
            string UpdateBy = Global.sEmail;
            bool IsActive=true;
            bool Isdeleted=false;
            try
            {
                int Insert = DB.Proc_Trans_InsertCar(Name, Description, UniqueCode, UpdateBy, InsertDate, IsActive, Isdeleted);

                if (Insert == 1)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else if (Insert == 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNOAFFECT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

        public static DBHelper.DBReturnCode CarsLoadAll(out List<BWI.Proc_Trans_CarLoadAllResult> CarsData)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            CarsData = null;
           
            try
            {

                var query = DB.Proc_Trans_CarLoadAll();
                CarsData = query.ToList();
                if (CarsData.Count > 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retCode;
        }

        public static DBHelper.DBReturnCode UpdateCar(string sid, string Name, string Description)
        {
            Int64 SID = Convert.ToInt64(sid);
            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();

            string UpdateDate = DateTime.Now.ToString("dd/MM/yyy HH:mm", CultureInfo.CurrentCulture);
          
            GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
            string UpdateBy = Global.sEmail;
       
            try
            {
                int Insert = DB.Proc_Trans_CarUpdate(SID, Name, Description, UpdateDate, UpdateBy);

                if (Insert == 1)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else if (Insert == 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNOAFFECT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }


        public static DBHelper.DBReturnCode DeleteCar(string sid)
        {
            Int64 SID = Convert.ToInt64(sid);
            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();

            try
            {
                int Insert = DB.Proc_Trans_CarDelete(SID);

                if (Insert == 1)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else if (Insert == 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNOAFFECT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

        public static DBHelper.DBReturnCode ActivateVehicleType(string sid)
        {
            Int64 SID = Convert.ToInt64(sid);
            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();

            try
            {
                int Insert = DB.Proc_Trans_CarDeactivate(SID);

                if (Insert == 1)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else if (Insert == 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNOAFFECT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

    }
}