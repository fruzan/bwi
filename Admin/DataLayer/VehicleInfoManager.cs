﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.BL;
using System.Configuration;

namespace BWI.Admin.DataLayer
{
    public class VehicleInfoManager
    {
        public static DBHelper.DBReturnCode VehicleMakeDropDown(out List<BWI.Trans_Tbl_Manufacture> List)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            List = null;
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_Manufacture Manufacture = new Trans_Tbl_Manufacture();

                var Query = from Obj in DB.Trans_Tbl_Manufactures where Obj.IsActive==true select Obj;

                List = Query.ToList();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }

        public static DBHelper.DBReturnCode VehicleTypeDropDown(out List<BWI.Trans_Tbl_CarType> List)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            List = null;
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_CarType Manufacture = new Trans_Tbl_CarType();

                var Query = from Obj in DB.Trans_Tbl_CarTypes where Obj.IsActive == true select Obj;

                List = Query.ToList();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }
        }

        public static DBHelper.DBReturnCode AddVehicleInfo(string UniqueId, string ManufacturerName, string ModelName, string RegistrationYear, string VehicleType, int Capacity, int Baggage, string Remarks, string Image_Path)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            try
            {
                BWI.DataLayer.GlobalDefaultTransfers Global = new BWI.DataLayer.GlobalDefaultTransfers();
                Global = (BWI.DataLayer.GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];

                DBHandlerDataContext DB = new DBHandlerDataContext();

                Trans_tbl_Vehicle_Info Vehicle_Info = new Trans_tbl_Vehicle_Info();

                string UpdatedBy = Global.sEmail;
                string Date = DateTime.Now.ToString("dd/MM/yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);

                Vehicle_Info.UniqueId = UniqueId;
                Vehicle_Info.Vehicle_Make = ManufacturerName;
                Vehicle_Info.Model = ModelName;
                Vehicle_Info.Year_Registration = RegistrationYear;
                Vehicle_Info.Vechile_Type = VehicleType;
                Vehicle_Info.Max_Capcity = Capacity;
                Vehicle_Info.Max_Baggage = Baggage;
                Vehicle_Info.Remark = Remarks;
                Vehicle_Info.AddedBy = UpdatedBy;
                Vehicle_Info.AddedDate = Date;
                Vehicle_Info.Img_Url = Image_Path;

                DB.Trans_tbl_Vehicle_Infos.InsertOnSubmit(Vehicle_Info);
                //DB.Trans_tbl_Vehicle_Info.InsertOnSubmit(Vehicle_Info);
                DB.SubmitChanges();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }

        public static DBHelper.DBReturnCode UpdateVehicleInfo(Int64 sid, string ManufacturerName, string ModelName, string RegistrationYear, string VehicleType, int Capacity, int Baggage, string Remarks, string Image_Path)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            try
            {

                BWI.DataLayer.GlobalDefaultTransfers Global = new BWI.DataLayer.GlobalDefaultTransfers();
                Global = (BWI.DataLayer.GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];

                DBHandlerDataContext DB = new DBHandlerDataContext();

                Trans_tbl_Vehicle_Info Vehicle_Info = DB.Trans_tbl_Vehicle_Infos.Single(x => x.sid == sid);

                string UpdatedBy = Global.sEmail;
            
                Vehicle_Info.Vehicle_Make = ManufacturerName;
                Vehicle_Info.Model = ModelName;
                Vehicle_Info.Year_Registration = RegistrationYear;
                Vehicle_Info.Vechile_Type = VehicleType;
                Vehicle_Info.Max_Capcity = Capacity;
                Vehicle_Info.Max_Baggage = Baggage;
                Vehicle_Info.Remark = Remarks;
                Vehicle_Info.AddedBy = UpdatedBy;
                Vehicle_Info.Img_Url = Image_Path;
                DB.SubmitChanges();

                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }
    }
}