﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.BL;
using System.Configuration;
using System.Data;
using System.Threading;
using System.ComponentModel;


namespace BWI.Admin.DataLayer
{
    public class CustomerManager
    {


        public static DBHelper.DBReturnCode AddCustomer(string fName, string LastName, string Email, string Mobile, string Gender, string Address, string City, string Country, string Pan, string Pin,string Password)
        {

            DBHandlerDataContext DB = new DBHandlerDataContext();
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            Trans_Tbl_CustomerMaster Customer = new Trans_Tbl_CustomerMaster();

            
            try
            {
                Customer.FirstName = fName;
                Customer.LastName = LastName;
                Customer.Email = Email;
                Customer.Mobile = Mobile;
                Customer.Gender = Gender;
                Customer.Address = Address;
                Customer.City = City;
                Customer.Country = Country;
                Customer.Pin = Pin;
                Customer.Pan = Pan;
                Customer.IsActive = true;
                Customer.IsDeleted = false;
                Customer.CreatedBy = "Admin ";
                Customer.CreateDate = DateTime.Now.ToString("dd/MM/yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                Customer.Password = Password;
                DB.Trans_Tbl_CustomerMasters.InsertOnSubmit(Customer);
                DB.SubmitChanges();
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }


        public static DBHelper.DBReturnCode GetAllCustomer(out List<BWI.Trans_Tbl_CustomerMaster> CustomerList)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();

            Int64 sid = 0;
            CustomerList = null;
            try
            {


                CustomerList = (from obj in DB.Trans_Tbl_CustomerMasters select obj).ToList();
                if (CustomerList.Count > 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

        public static DBHelper.DBReturnCode GetAllCustomerActivity(out DataTable dtCustomerList)
        {
            dtCustomerList = null;
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            DBHandlerDataContext DB = new DBHandlerDataContext();
            var List = (from Obj in DB.Trans_Reservations select Obj).ToList();
            // var NewList = List.Where(x => ConvertDateTime(x.Date) < DateTime.Now).ToList();
            var NewList = List.Where(x => ConvertDateTime(x.ReservationDate) < DateTime.Now).ToList();

            if (NewList.Count > 0)
            {
                dtCustomerList = ConvertToDatatable(NewList);
                retCode = DBHelper.DBReturnCode.SUCCESS;
               
            }
            else
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                // string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;

                string CurrentPattern = "mm-dd-yyyy";
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;
        }
    }
}