﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace BWI.Admin.DataLayer
{
    public class EmailManager
    {
        #region Time
        public static string GetTime(string Time)
        {
            try
            {
                string[] s = null;
                if (Time.Contains("AM") || Time.Contains("PM"))
                {

                }
                else
                {
                    s = Regex.Split(Time, ":");

                    if (Convert.ToInt16(s[0]) > 12)
                    {
                        Int64 val = Convert.ToInt16(s[0]) - 12;
                        Time = val.ToString() + ":" + s[1] + ":PM";
                    }
                    else if (Convert.ToInt16(s[0]) == 0)
                    {
                        Time = "12:" + s[1] + ":AM";
                    }
                    else
                    {
                        Time = s[0] + ":" + s[1] + ":AM";
                    }
                }
            }
            catch
            {

            }
            return Time;
        }
        #endregion

        #region Gratuity
        public static float GetGratuity(float TotalFare, string Gratuity)
        {
            float GratuityAmt = 0;
            if (Gratuity != "" || Gratuity != null)
            {
                GratuityAmt = TotalFare * Convert.ToSingle(Gratuity) / 100;
            }
            return Convert.ToSingle(GratuityAmt);
        }
        #endregion

        #region Reservation Date
        public static string GetDate(string Date)
        {
            try
            {
                string[] s = Regex.Split(Date, " ");
                s = Regex.Split(s[0], "-");
                return Date = s[1] + "-" + s[0] + "-" + s[2];

            }
            catch
            {
                return Date;
            }

        }
        #endregion

        public static bool CustomerMail(Trans_Reservation Booking)
        {
            #region Customer Mail
            DBHandlerDataContext DB = new DBHandlerDataContext();
            string DriverName = Booking.AssignedTo;
            Trans_Tbl_Login Driver = new Trans_Tbl_Login();
            if (DriverName != "Select Driver")
            {
                Driver = DB.Trans_Tbl_Logins.Single(x => x.Sid == Booking.DriverSid);
            }
            StringBuilder sb = new StringBuilder();
            sb.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
            sb.Append("<title>limoallaround.com - Customer Receipt</title>  ");
            sb.Append("<style>                                            ");
            sb.Append(".Norm {font-family: Verdana;                       ");
            sb.Append("font-size: 12px;                                 ");
            sb.Append("font-color: red;                               ");
            sb.Append(" }                                               ");
            sb.Append(".heading {font-family: Verdana;                   ");
            sb.Append("  font-size: 14px;                            ");
            sb.Append("  font-weight: 800;                           ");
            sb.Append("	 }                                                  ");
            sb.Append("   td {font-family: Verdana;                         ");
            sb.Append("	  font-size: 12px;                                  ");
            sb.Append("	 }                                                  ");
            sb.Append("  </style>                                           ");
            sb.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
            sb.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
            sb.Append("");
            sb.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
            sb.Append("  <tbody><tr>");
            sb.Append("    <td width='660' colspan='13' valign='top'>");
            sb.Append("      <table width='100%'>");
            sb.Append("        <tbody><tr>");
            //sb.Append("          <td colspan='2' width='100%' class='heading'><b>Bwi Shuttle Service</b></td>");
            sb.Append("        </tr>");
            //sb.Append("        <tr>");
            //sb.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com/'>http://www.bwishuttleservice.com</a></td>");
            //sb.Append("        </tr>");
            sb.Append("        <tr>");
            sb.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com'>http://www.bwishuttleservice.com</a></td>");
            sb.Append("        </tr>");
            sb.Append("        <tr>");
            sb.Append("          <td colspan='2' width='100%'><a href='mailto:reservation@bwishuttleservice.com'>reservation@bwishuttleservice.com</a></td>");
            sb.Append("        </tr>");
            sb.Append("        <tr>");
            sb.Append("          <td colspan='2' width='25%'>Call Us: 1-844-904-5151, 410-904-5151</td>");
            sb.Append("          <td width='75%' align='right'><font size='3'>");
            sb.Append("		Receipt");
            sb.Append("	     </font></td>");
            sb.Append("        </tr>");
            sb.Append("      </tbody></table>");
            sb.Append("     </td>");
            sb.Append("  </tr><tr>");
            sb.Append("  </tr><tr>");
            sb.Append("    <td colspan='13' width='660'>");
            sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
            sb.Append("  	<tbody><tr>");
            sb.Append("  	  <td width='70%' style='border-bottom:medium solid blue; font-size:16px'><b>Reservation # " + Booking.ReservationID + "</b></td>");
            sb.Append("  	  <td width='30%' align='right' style='border-bottom:medium solid blue;'>Service Date: " + EmailManager.GetDate(Booking.Date) + "</td>");
            sb.Append("  	</tr>");
            sb.Append("  	<tr>");
            sb.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br>Thank you for traveling with Limo All Around Service!  Below please find your confirmation. If any of the information appears to be incorrect, please contact our office immediately to correct it.<br>&nbsp;</td>");
            sb.Append("	</tr>");
            sb.Append("      </tbody></table>");
            sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
            sb.Append("	<tbody><tr>");

            sb.Append("	  <td class='heading'>Pick-up Date:</td>");
            sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.ReservationDate) + "</td>");
            sb.Append("	</tr>                                             ");

            if (DriverName != "Select Driver" || DriverName != "not assign")
            {
                sb.Append("	<tr>                                              ");
                sb.Append("	  <td class='heading'>Driver Name:</td>          ");
                sb.Append("	  <td width='75%'>" + Driver.sFirstName + " " + Driver.sLastName + "</td>                      ");
                sb.Append("	</tr>                                             ");

                sb.Append("	<tr>                                              ");
                sb.Append("	  <td class='heading'>Driver Mobile:</td>          ");
                sb.Append("	  <td width='75%'>" + Driver.sMobile + "</td>                      ");
                sb.Append("	</tr>                                             ");


                sb.Append("	<tr>                                              ");
                sb.Append("	  <td class='heading'>Driver Email:</td>          ");
                sb.Append("	  <td width='75%'>" + Driver.sEmail + "</td>                      ");
                sb.Append("	</tr>                                             ");
            }

            sb.Append("	<tr>                                              ");
            sb.Append("	  <td class='heading'>Pickup Time:</td>          ");
            if (Booking.Service == "Point To Point Reservation")
                sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.P2PTime) + "</td>");
            else
                sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.FlightTime) + "</td>");
            sb.Append("	</tr>                                             ");


            sb.Append("	<tr>                                              ");
            sb.Append("	  <td class='heading'>Primary Contact:</td>       ");
            sb.Append("	  <td width='75%'>" + Booking.FirstName + " " + Booking.LastName + "</td>           ");
            sb.Append("	</tr>                                             ");
            sb.Append("	<tr>                                              ");
            sb.Append("	  <td class='heading'>Contact Number:</td>        ");
            sb.Append("	  <td width='75%'>" + Booking.ContactNumber + "</td>               ");
            sb.Append("	</tr>                                             ");
            sb.Append("	<tr>                                              ");
            sb.Append("	  <td class='heading'>No. of Passenger:</td>         ");
            sb.Append("	  <td width='75%'>" + Booking.Persons + "</td>                          ");
            sb.Append("	</tr>                                             ");

            sb.Append("	</tr>	                                          ");
            sb.Append("	<tr>                                                                                                    ");
            sb.Append("	  <td class='heading'>Trip Routing Information:</td>                                                    ");
            sb.Append("	                                                                                                        ");
            sb.Append("	  <td><b>Pick-up Location: &nbsp;</b>" + Booking.PickUpAddress + "</td>                         ");
            sb.Append("	                                                                                                        ");
            sb.Append("	</tr>                                                                                                   ");
            sb.Append("        <tr>                                                                                             ");
            sb.Append("          <td style='border-bottom:thin solid green;' colspan='2'>&nbsp;</td>                            ");
            sb.Append("        </tr>                                                                                            ");
            sb.Append("                                                                                                         ");
            sb.Append("                                                                                                         ");
            sb.Append("                                                                                                         ");
            sb.Append(" 	<tr>                                                                                                ");
            sb.Append("	  <td class='heading'>Quoted Rate:</td>                                                                 ");
            sb.Append("	  <td>$ " + Booking.Fare + "                                                                                         ");
            sb.Append("	</td></tr>                                                                                              ");
            sb.Append("                                                                                                         ");
            sb.Append("	<tr>                                                                                                    ");
            sb.Append("	  <td class='heading'>Gratuity:</td>                                                                    ");
            sb.Append("	  <td>$ " + EmailManager.GetGratuity(Convert.ToSingle(Booking.Fare), Booking.Gratuity.ToString()) + "                                                                                          ");
            sb.Append("	</td></tr>                                                                                              ");
            sb.Append("                                                                                                         ");
            sb.Append("	<tr>                                                                                                    ");
            sb.Append("	  <td class='heading'>Reservation Total:</td>                                                           ");
            sb.Append("	  <td>$ " + Booking.TotalFare + "                                                                                         ");
            sb.Append("	</td></tr>                                                                                              ");
            sb.Append("	<tr>                                                                                                    ");
            sb.Append("	  <td style='border-bottom:thin solid green;' class='heading' valign='top'><font color='red'>Total Due:</font></td>");
            sb.Append("	  <td style='border-bottom:thin solid green;'><font color='red'>$ 0.00<br>&nbsp;</font></td>     ");
            sb.Append("</tr>                                                                                              ");
            sb.Append("<tr>                                                                                               ");
            sb.Append("<td class='heading' valign='top'>Terms &amp; Conditions / Reservation Agreement:</td>            ");
            sb.Append("<td>Guest agrees that there will be no smoking in our vehicles.<br>                              ");
            sb.Append("Guest assures that no illegal drugs are brought into our vehicles.<br>                         ");
            sb.Append("Guest agrees that no alcoholic beverages shall be brought into our vehicles.                   ");
            sb.Append("Guest agrees that the passengers capacity of vehicle provided shall not be exceeded.<br>       ");
            sb.Append("In case of misconduct by your party, chauffeur has the right to terminate this agreement without refunds.<br>");
            sb.Append("Guest holds Limo All Around Service harmless and not liable for any personal or material damages arising from the conduct of his/her party.<br>");
            sb.Append("Guest is responsible for the full payment of any overtime charges, beyond the original agreement.<br>");
            sb.Append("Guest agrees to 100% payment charged to his/her credit card at the time reservation has been made.<br>");
            sb.Append("Luxury Sedan guests can cancel 3 hours before pickup time with $10.00 processing fee; if not you will be charged the full amount of reservation. Stretch limousine and Van Guests can cancel 48 hours before pickup time with a $20.00 processing fee; if you cancel after 48 hours, you will be billed for a minimum 3 hours of service charges.<br>");
            sb.Append("Guest acknowledges that he/she understands that Limo All Around Service imposes a service fee for late night service (11:00PM TO 5:00AM).<br>");
            sb.Append("Guest ackowledges that he/she understands that Limo All Around Service imposes an additional service fee for Incoming International flights.<br>		");
            sb.Append("Limo All Around Service cannot be held responsible for mechanical problems, inclement weather, or other uncontrollable circumstances resulting in the inability to start a job as it is schedule time or complete a job. In the event that the requested vehicle can not be provided; Limo All Around Service may provide a vehicle of equal quality.<br>");
            sb.Append("Limo All Around Service reserves the right to change rates without notice.  If a rate change is necessary due to a destination being outside of the immediate service area, technical error or increase in fuel charges; Limo All Around Service will make every effort to notify the Guest of the change.<br>");
            sb.Append("	</td>");
            sb.Append("	</tr>");
            sb.Append("</tbody></table>    ");
            sb.Append("</td></tr>");
            sb.Append("</tbody></table>");
            sb.Append("</body></html>");
            string Mail = sb.ToString();
            #endregion
            return true;
        }

        public static bool SendMail(string ReservationID, string To, string Cc, string BCc, string MailDetails, string MailSubject)
        {
            try
            {
                System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                mailMsg.From = new MailAddress("support@memorEbook.in", "Transfers ");
                mailMsg.To.Add(To);
                //mailMsg.To.Add("shahidanwar888@gmail.com");
                mailMsg.Subject = MailSubject + " Booking-" + ReservationID;
                mailMsg.IsBodyHtml = true;
                mailMsg.Body = MailDetails;
                SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                mailObj.EnableSsl = false;
                mailMsg.Sender = new MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                mailObj.Send(mailMsg);
                mailMsg.Dispose();
                return true;
            }
            catch
            {
                return false;
            }

        }


        public static string BookingDetails(Trans_Reservation Booking)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr>");
            sb.Append("<td class='heading'>Service:</td>");
            sb.Append("<td width='75%'>" + Booking.Service + "</td>");
            sb.Append("</tr>");
            sb.Append("	<tr>                                                                                                    ");
            sb.Append("	  <td class='heading'>Trip Routing Information:</td>                                                    ");
            if (Booking.Service == "From Airport" || Booking.Service == "To Airport" || Booking.Service == "From Airport Shuttle" || Booking.Service == "To Airport Shuttle")
            {
                sb.Append("<td><b>Pick-up Location: &nbsp;</b>" + Booking.PickUpAddress + "</td>");
                sb.Append("	</tr>");
                sb.Append("	<tr>");
                sb.Append("	  <td class='heading'>&nbsp;</td>");
                sb.Append("	  <td><b>Drop-off Location: &nbsp;</b>" + Booking.DropAddress + "</td>");

            }
            else if (Booking.Service == "Point To Point Reservation")
            {
                string Location = "", HaltTime = "", Mics = "";

                sb.Append("	</tr>");
                sb.Append("	<tr>");
                sb.Append("	  <td class='heading'>&nbsp;</td>");
                sb.Append("	  <td><table style=\"width:100%\"><thead><tr><th>Location</th><th>Halting Time</th><th>MISC</th></tr></thead><tbody>");
                if (Booking.Stop1 != "^-:-^")
                {
                    Location = ""; HaltTime = ""; Mics = "";
                    GetP2PLocation(Booking.Stop1, out Location, out HaltTime, out Mics);
                    sb.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                    sb.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                    sb.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                }
                if (Booking.Stop2 != "^-:-^")
                {
                    Location = ""; HaltTime = ""; Mics = "";
                    GetP2PLocation(Booking.Stop2, out Location, out HaltTime, out Mics);
                    sb.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                    sb.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                    sb.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                }
                if (Booking.Stop3 != "^-:-^")
                {
                    Location = ""; HaltTime = ""; Mics = "";
                    GetP2PLocation(Booking.Stop3, out Location, out HaltTime, out Mics);
                    sb.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                    sb.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                    sb.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                }
                if (Booking.Stop4 != "^-:-^")
                {
                    Location = ""; HaltTime = ""; Mics = "";
                    GetP2PLocation(Booking.Stop4, out Location, out HaltTime, out Mics);
                    sb.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                    sb.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                    sb.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                }
                if (Booking.Stop5 != "^-:-^")
                {
                    Location = ""; HaltTime = ""; Mics = "";
                    GetP2PLocation(Booking.Stop5, out Location, out HaltTime, out Mics);
                    sb.Append("<tr><td style=\"text-align: center;\">" + Location + "</td>");
                    sb.Append("<td style=\"text-align: center;\">" + HaltTime + "</td>");
                    sb.Append("<td style=\"text-align: center;\">" + Mics + "</td></tr>");
                }
                sb.Append("</tbody></table></td>");
            }
            else if (Booking.Service == "Hourly Reservation")
            {
                sb.Append("<td><b>Pick-up Location: &nbsp;</b>" + Booking.Source + "</td>");
            }
            sb.Append("</tr>");
            return sb.ToString();
        }

        public static void GetP2PLocation(string Source, out string Locations, out string HaltTime, out string Mics)
        {
            Locations = ""; HaltTime = ""; Mics = "";
            string[] s = Regex.Split(Source, @"\^");
            if (s.Length == 3)
            {
                Locations = s[0];
                HaltTime = GetTime(s[1]);
                Mics = s[2];
            }
            else if (s.Length == 2)
            {
                Locations = s[0];
                HaltTime = s[1];
            }
        }
    }
}
