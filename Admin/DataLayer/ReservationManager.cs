﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.Admin;
using BWI.BL;
using System.Text;
using System.Net;
using System.Net.Mail;
using BWI.DataLayer;
using System.Data;
using System.ComponentModel;
using System.Threading;
namespace BWI.Admin.DataLayer
{
    public class ReservationManager
    {
        public static DBHelper.DBReturnCode AirPortReservation(string[] Input, string[] DropDowns, string[] CheckBoxes, string Remarks, string TravelType, string TotalDistance, string TimeTaken, string source, string destination, string AirportName, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string Airlines, string DriverID, string Email, string ContactNo, decimal GratuityAmount, out string Response)
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();
            Trans_Reservation ResTable = new Trans_Reservation();
            DBHelper.DBReturnCode retcode; Response = "";
            GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
            //string a = Global.sEmail;
            try
            {
                var CustomerSid = DB.Trans_Tbl_CustomerMasters.Single(x => x.Email == Email);
                //var CustomerSid = (from User in DB.Trans_Tbl_CustomerMasters where User.Email== Email select User.sid);
                //var CustomerSid = DB.Proc_AddSelectCustomer(Input[0], Input[1], Email, ContactNo, "");
                ResTable.uid = CustomerSid.sid;
                if (DriverName == "Select Driver")
                {
                    //ResTable.uid = null;
                    ResTable.AssignedTo = "not assign";
                    ResTable.Status = "Requested";
                }
                else
                {
                    ResTable.DriverSid = Convert.ToInt32(DriverID);
                    ResTable.Status = "Confirmed";
                    //ResTable.AssignedTo = DropDowns[4];
                    //ResTable.DriverSid = Convert.ToInt32(DriverID);
                }
                Random generator = new Random();
                int Reservation = generator.Next(1000000, 10000000);

                ResTable.ReservationID = "RES-" + Reservation;
                ResTable.InvoiceNum = "INV-" + Reservation;
                ResTable.VoucherNum = "VCH-" + Reservation;

                //ResTable.AccountNumber = Input[0];
                ResTable.FirstName = Input[0];
                ResTable.LastName = Input[1];
                ResTable.ContactNumber = ContactNo;
                ResTable.Email = Email;
                ResTable.ReservationDate = Input[2];
                //ResTable.OrganisationCode = Input[6];
                ResTable.Persons = Input[4];
                //ResTable.Child = Input[8];
                ResTable.FlightNumber = Input[5];
                ResTable.FlightDate = Input[2];
                //ResTable.AssignedTo = Input[12];
                ResTable.CCLast4 = Input[6];
                ResTable.Fare = Convert.ToDecimal(Input[8]);
                if (DropDowns[4] == "-")
                {
                    ResTable.Gratuity = 0;
                }
                else
                {
                    ResTable.Gratuity = Convert.ToDecimal(DropDowns[4]);
                }
                ResTable.GratuityAmount = GratuityAmount;

                if (Input[9] != "")
                {
                    ResTable.Parking = Convert.ToDecimal(Input[9]);
                }
                else
                {
                    ResTable.Parking = 0;
                }
                if (Input[12] != "")
                {
                    ResTable.Toll = Convert.ToDecimal(Input[12]);
                }
                else
                {
                    ResTable.Toll = 0;
                }
                ResTable.late_nite = Input[10];
                ResTable.TotalFare = Convert.ToDecimal(Input[11]);

                ResTable.Remark = Remarks;
                ResTable.Airlines = Airlines;
                ResTable.Service = DropDowns[0];
                ResTable.AirPortName = AirportName;
                ResTable.VehicalType = DropDowns[2];

                ResTable.TravelType = DropDowns[1];
                ResTable.OfferCode = DropDowns[5];
                ResTable.CCType = DropDowns[6];
                ResTable.ApproxDistance = TotalDistance;
                ResTable.ApproxTime = TimeTaken;
                string[] Source = source.Split(',');
                if (ResTable.Service == "From Airport")
                {
                    ResTable.Service = "From Airport";
                    ResTable.Source = Source[0];
                    ResTable.Destination = destination;
                    ResTable.FlightTime = EmailManager.GetTime(Input[3]);
                    ResTable.PickUpAddress = Source[0];
                    ResTable.DropAddress = destination;
                }
                else
                {
                    ResTable.Service = "To Airport";
                    ResTable.Source = Input[7];
                    ResTable.PickUpAddress = Input[7];
                    ResTable.DropAddress = destination;
                    ResTable.Destination = destination;
                    ResTable.Pickup_Time = EmailManager.GetTime(Input[3]);
                }
                //ResTable.Status = "Pending";

                //ResTable.Status = Status;
                ResTable.Paid = Convert.ToBoolean(CheckBoxes[0]);
                //ResTable.Completed = Convert.ToBoolean(CheckBoxes[1]);
                ResTable.Collect = Convert.ToBoolean(CheckBoxes[2]);
                ResTable.CCPayment = Convert.ToBoolean(CheckBoxes[3]);
                ResTable.ChildCarSheet = Convert.ToBoolean(CheckBoxes[4]);
                ResTable.Date = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.ModifiedDate = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.UpdatedBy = Global.sEmail;
                ResTable.MeetGreat = MeetGreat;
                ResTable.SpecialAssistant = SpecialAssistant;
                ResTable.CurbSidePickUp = CurbSidePickUp;
                //ResTable.BaggageClaimPickup = BaggageClaimPickup;
                ResTable.PetInCage = PetInCage;
                ResTable.ReservationType = "Admin";

                DB.Trans_Reservations.InsertOnSubmit(ResTable);
                DB.SubmitChanges();

                var BookingSid = (from Obj in DB.Trans_Reservations where Obj.ReservationID == "RES-" + Reservation select Obj.sid).ToList();
                ReservationHandler r = new ReservationHandler();
                Response = r.AssignBooking(Convert.ToInt64(BookingSid[0]), Convert.ToInt64(DriverID), DriverName);
                if (Response.Contains("Error"))
                {
                    retcode = DBHelper.DBReturnCode.EXCEPTION;
                }
                else
                    retcode = DBHelper.DBReturnCode.SUCCESS;
            }
            catch
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retcode;
        }

        public static DBHelper.DBReturnCode UpdateAirPortReservation(int sid, string[] Input, string[] DropDowns, string[] CheckBoxes, string Remarks, string TravelType, string TotalDistance, string TimeTaken, string source, string destination, string AirportName, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string Airlines, string DriverID, string Email, string ContactNo, decimal GratuityAmount)
        {

            DBHandlerDataContext DB = new DBHandlerDataContext();
            Trans_Reservation ResTable = DB.Trans_Reservations.Single(x => x.sid == sid);
            DBHelper.DBReturnCode retcode;
            GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];

            try
            {
                var CustomerSid = DB.Trans_Tbl_CustomerMasters.Single(x => x.Email == Email);
                ResTable.uid = CustomerSid.sid;
                if (DriverName == "Select Driver")
                {
                    ResTable.AssignedTo = null;
                    ResTable.Status = "Requested";
                }
                else
                {
                    ResTable.DriverSid = Convert.ToInt32(DriverID);
                    ResTable.AssignedTo = DriverName;
                    ResTable.Status = "Confirmed";
                }

                ResTable.FirstName = Input[0];
                ResTable.LastName = Input[1];
                ResTable.ContactNumber = ContactNo;
                ResTable.Email = Email;
                ResTable.ReservationDate = Input[2];
                //ResTable.OrganisationCode = Input[6];
                ResTable.Persons = Input[4];
                //ResTable.Child = Input[8];
                ResTable.FlightNumber = Input[5];
                ResTable.FlightDate = Input[2];
                //ResTable.AssignedTo = Input[12];
                ResTable.CCLast4 = Input[6];
                ResTable.Fare = Convert.ToDecimal(Input[8]);
                ResTable.Gratuity = Convert.ToDecimal(DropDowns[4]);
                ResTable.GratuityAmount = GratuityAmount;
                //if(Input[15]!="")
                //{
                //    ResTable.Gratuity = Convert.ToDecimal(Input[15]);
                //}
                //else
                //{
                //    ResTable.Gratuity = 0;
                //}
                if (Input[9] != "")
                {
                    ResTable.Parking = Convert.ToDecimal(Input[9]);
                }
                else
                {
                    ResTable.Parking = 0;
                }
                if (Input[12] != "")
                {
                    ResTable.Toll = Convert.ToDecimal(Input[12]);
                }
                else
                {
                    ResTable.Toll = 0;
                }
                ResTable.TotalFare = Convert.ToDecimal(Input[11]);

                ResTable.Remark = Remarks;
                ResTable.Airlines = Airlines;
                ResTable.Service = DropDowns[0];
                ResTable.AirPortName = AirportName;
                ResTable.VehicalType = DropDowns[2];
                ResTable.OfferCode = DropDowns[5];
                ResTable.CCType = DropDowns[6];
                ResTable.TravelType = DropDowns[1];
                ResTable.ApproxDistance = TotalDistance;
                ResTable.ApproxTime = TimeTaken;

                //var Splitter = Input[3].Split(':');
                //if()
                if (ResTable.Service == "From Airport")
                {
                    ResTable.Service = "From Airport";
                    ResTable.Source = source;
                    ResTable.PickUpAddress = source;
                    ResTable.DropAddress = destination;
                    ResTable.Destination = destination;
                    ResTable.FlightTime = EmailManager.GetTime(Input[3]);
                }
                else
                {
                    ResTable.Service = "To Airport";
                    ResTable.Source = source;
                    ResTable.Destination = destination;
                    ResTable.Pickup_Time = EmailManager.GetTime(Input[3]);
                    ResTable.PickUpAddress = source;
                    ResTable.DropAddress = destination;
                }
                //ResTable.Status = "Pending";
                //ResTable.Status = Status;
                ResTable.Paid = Convert.ToBoolean(CheckBoxes[0]);
                ResTable.Completed = Convert.ToBoolean(CheckBoxes[1]);
                ResTable.Collect = Convert.ToBoolean(CheckBoxes[2]);
                ResTable.CCPayment = Convert.ToBoolean(CheckBoxes[3]);
                ResTable.ChildCarSheet = Convert.ToBoolean(CheckBoxes[4]);
                // ResTable.Date = DateTime.Now.ToString("dd/MM/yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.ModifiedDate = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.UpdatedBy = Global.sEmail;
                ResTable.MeetGreat = MeetGreat;
                ResTable.SpecialAssistant = SpecialAssistant;
                ResTable.CurbSidePickUp = CurbSidePickUp;
                ResTable.BaggageClaimPickup = BaggageClaimPickup;
                ResTable.PetInCage = PetInCage;

                DB.SubmitChanges();

                //var BookingSid = (from Obj in DB.Trans_Reservations where Obj.ReservationID == "RES-" + Reservation select Obj.sid).ToList();
                ReservationHandler r = new ReservationHandler();
                r.AssignBooking(Convert.ToInt64(ResTable.sid), Convert.ToInt64(DriverID), DriverName);
                retcode = DBHelper.DBReturnCode.SUCCESS;

                retcode = DBHelper.DBReturnCode.SUCCESS;
            }
            catch
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retcode;
        }

        public static DBHelper.DBReturnCode PointToPointReservation(string[] Input, string[] DropDowns, string[] StopLocations, string[] StopMisc, string[] StopHours, string[] StopMins, string[] CheckBoxes, string Remarks, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string TotalDistance, string TimeTaken, string Email, string ContactNo, decimal GratuityAmount, string DriverID)
        {

            DBHandlerDataContext DB = new DBHandlerDataContext();
            Trans_Reservation ResTable = new Trans_Reservation();
            DBHelper.DBReturnCode retcode;
           GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];

            try
            {
                var CustomerSid = DB.Trans_Tbl_CustomerMasters.Single(x => x.Email == Email);
                ResTable.uid = CustomerSid.sid;
                if (DriverName == "Select Driver")
                {
                    ResTable.AssignedTo = null;
                    ResTable.Status = "Requested";
                }
                else
                {
                    ResTable.DriverSid = Convert.ToInt32(DriverID);
                    ResTable.AssignedTo = DriverName;
                    ResTable.Status = "Confirmed";
                }

                Random generator = new Random();
                int Reservation = generator.Next(1000000, 10000000);

                ResTable.ReservationID = "RES-" + Reservation;
                ResTable.InvoiceNum = "INV-" + Reservation;
                ResTable.VoucherNum = "VCH-" + Reservation;

                //ResTable.AccountNumber = Input[0];
                ResTable.FirstName = Input[0];
                ResTable.LastName = Input[1];
                ResTable.ContactNumber = ContactNo;
                ResTable.Email = Email;
                ResTable.ReservationDate = Input[2];
                //ResTable.OrganisationCode = Input[8];
                //ResTable.Adult = Input[9];
                //ResTable.Child = Input[10];
                ResTable.Persons = Input[4];
                ResTable.FlightNumber = "";
                ResTable.FlightTime = "";
                ResTable.FlightDate = "";

                ResTable.CCLast4 = Input[5];

                ResTable.Fare = Convert.ToDecimal(Input[7]);

                if (Input[8] != "")
                {
                    ResTable.Parking = Convert.ToDecimal(Input[8]);
                }
                else
                {
                    ResTable.Parking = 0;
                }
                if (Input[11] != "")
                {
                    ResTable.Toll = Convert.ToDecimal(Input[11]);
                }
                else
                {
                    ResTable.Toll = 0;
                }
                ResTable.late_nite = Input[9];
                ResTable.TotalFare = Convert.ToDecimal(Input[10]);

                ResTable.PickUpAddress = Input[6];
                ResTable.P2PTime = EmailManager.GetTime(Input[3]);
                //ResTable.PickUpCity = Input[18];
                //ResTable.PickUpState = Input[19];
                //ResTable.PickUpZipCode = Input[20];

                ResTable.Source = Input[6];

                for (int i = 0; i < StopLocations.Length; i++)
                {
                    if (StopLocations[i] != "")
                    {

                        ResTable.Destination = StopLocations[i];
                    }
                }

                ResTable.Remark = Remarks;
                ResTable.Airlines = "";
                ResTable.Service = "Point To Point Reservation";
                ResTable.AirPortName = "";
                //ResTable.CCType = DropDowns[0];
                ResTable.TravelType = DropDowns[0];
                ResTable.VehicalType = DropDowns[1];
                ResTable.Gratuity = Convert.ToDecimal(DropDowns[3]);
                ResTable.OfferCode = DropDowns[4];
                ResTable.CCType = DropDowns[5];
                ResTable.GratuityAmount = GratuityAmount;

                //ResTable.Status = Status;
                ResTable.Paid = Convert.ToBoolean(CheckBoxes[0]);
                //ResTable.Completed = Convert.ToBoolean(CheckBoxes[1]);
                ResTable.Collect = Convert.ToBoolean(CheckBoxes[2]);
                ResTable.CCPayment = Convert.ToBoolean(CheckBoxes[3]);
                ResTable.ChildCarSheet = Convert.ToBoolean(CheckBoxes[4]);

                ResTable.Stop1 = StopLocations[0] + "^" + StopHours[0] + ":" + StopMins[0] + "^" + StopMisc[0];
                ResTable.Stop2 = StopLocations[1] + "^" + StopHours[1] + ":" + StopMins[1] + "^" + StopMisc[1];
                ResTable.Stop3 = StopLocations[2] + "^" + StopHours[2] + ":" + StopMins[2] + "^" + StopMisc[2];
                ResTable.Stop4 = StopLocations[3] + "^" + StopHours[3] + ":" + StopMins[3] + "^" + StopMisc[3];
                ResTable.Stop5 = StopLocations[4] + "^" + StopHours[4] + ":" + StopMins[4] + "^" + StopMisc[4];

                ResTable.Date = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.ModifiedDate = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.UpdatedBy = Global.sEmail;
                ResTable.MeetGreat = MeetGreat;
                ResTable.SpecialAssistant = SpecialAssistant;
                ResTable.CurbSidePickUp = CurbSidePickUp;
                //ResTable.BaggageClaimPickup = BaggageClaimPickup;
                ResTable.PetInCage = PetInCage;
                ResTable.ApproxDistance = TotalDistance;
                ResTable.ApproxTime = TimeTaken;
                ResTable.ReservationType = "Admin";
                DB.Trans_Reservations.InsertOnSubmit(ResTable);
                DB.SubmitChanges();
                var BookingSid = (from Obj in DB.Trans_Reservations where Obj.ReservationID == "RES-" + Reservation select Obj.sid).ToList();
                ReservationHandler r = new ReservationHandler();
                r.AssignBooking(Convert.ToInt64(BookingSid[0]), Convert.ToInt64(DropDowns[2]), DriverName);
                retcode = DBHelper.DBReturnCode.SUCCESS;

            }
            catch
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retcode;
        }

        public static DBHelper.DBReturnCode UpdatePointToPointReservation(int sid, string[] Input, string[] DropDowns, string[] StopLocations, string[] StopMisc, string[] StopHours, string[] StopMins, string[] CheckBoxes, string Remarks, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string TotalDistance, string TimeTaken, string Email, string ContactNo, decimal GratuityAmount, string DriverID)
        {

            DBHandlerDataContext DB = new DBHandlerDataContext();
            Trans_Reservation ResTable = DB.Trans_Reservations.Single(x => x.sid == sid);
            DBHelper.DBReturnCode retcode;
            GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];

            try
            {
                var CustomerSid = DB.Trans_Tbl_CustomerMasters.Single(x => x.Email == Email);
                ResTable.uid = CustomerSid.sid;
                if (DriverName == "Select Driver")
                {
                    ResTable.AssignedTo = null;
                    ResTable.Status = "Requested";
                }
                else
                {
                    ResTable.DriverSid = Convert.ToInt32(DriverID);
                    ResTable.AssignedTo = DriverName;
                    ResTable.Status = "Confirmed";
                }
                //Random generator = new Random();
                //int Reservation = generator.Next(1000000, 10000000);
                //ResTable.ReservationID = "RES-" + Reservation;
                //ResTable.InvoiceNum = "INV-" + Reservation;
                //ResTable.VoucherNum = "VCH-" + Reservation;

                ResTable.FirstName = Input[0];
                ResTable.LastName = Input[1];
                ResTable.ContactNumber = ContactNo;
                ResTable.Email = Email;
                ResTable.ReservationDate = Input[2];
                //ResTable.OrganisationCode = Input[8];
                //ResTable.Adult = Input[9];
                //ResTable.Child = Input[10];
                ResTable.Persons = Input[4];
                ResTable.FlightNumber = "";
                ResTable.FlightTime = "";
                ResTable.FlightDate = "";

                ResTable.CCLast4 = Input[5];

                ResTable.Fare = Convert.ToDecimal(Input[7]);

                if (Input[8] != "")
                {
                    ResTable.Parking = Convert.ToDecimal(Input[8]);
                }
                else
                {
                    ResTable.Parking = 0;
                }
                if (Input[11] != "")
                {
                    ResTable.Toll = Convert.ToDecimal(Input[11]);
                }
                else
                {
                    ResTable.Toll = 0;
                }
                ResTable.late_nite = Input[9];
                ResTable.TotalFare = Convert.ToDecimal(Input[10]);

                ResTable.PickUpAddress = Input[6];
                ResTable.P2PTime = EmailManager.GetTime(Input[3]);
                //ResTable.PickUpCity = Input[18];
                //ResTable.PickUpState = Input[19];
                //ResTable.PickUpZipCode = Input[20];

                ResTable.Source = Input[6];

                for (int i = 0; i < StopLocations.Length; i++)
                {
                    if (StopLocations[i] != "")
                    {

                        ResTable.Destination = StopLocations[i];
                    }
                }

                ResTable.Remark = Remarks;
                ResTable.Airlines = "";
                ResTable.Service = "Point To Point Reservation";
                ResTable.AirPortName = "";
                ResTable.TravelType = DropDowns[0];
                ResTable.VehicalType = DropDowns[1];
                ResTable.Gratuity = Convert.ToDecimal(DropDowns[3]);
                ResTable.OfferCode = DropDowns[4];
                ResTable.CCType = DropDowns[5];
                ResTable.GratuityAmount = GratuityAmount;

                //ResTable.Status = Status;
                ResTable.Paid = Convert.ToBoolean(CheckBoxes[0]);
                //ResTable.Completed = Convert.ToBoolean(CheckBoxes[1]);
                ResTable.Collect = Convert.ToBoolean(CheckBoxes[2]);
                ResTable.CCPayment = Convert.ToBoolean(CheckBoxes[3]);
                ResTable.ChildCarSheet = Convert.ToBoolean(CheckBoxes[4]);

                ResTable.Stop1 = StopLocations[0] + "^" + StopHours[0] + ":" + StopMins[0] + "^" + StopMisc[0];
                ResTable.Stop2 = StopLocations[1] + "^" + StopHours[1] + ":" + StopMins[1] + "^" + StopMisc[1];
                ResTable.Stop3 = StopLocations[2] + "^" + StopHours[2] + ":" + StopMins[2] + "^" + StopMisc[2];
                ResTable.Stop4 = StopLocations[3] + "^" + StopHours[3] + ":" + StopMins[3] + "^" + StopMisc[3];

                ResTable.Date = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.ModifiedDate = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.UpdatedBy = Global.sEmail;
                ResTable.MeetGreat = MeetGreat;
                ResTable.SpecialAssistant = SpecialAssistant;
                ResTable.CurbSidePickUp = CurbSidePickUp;
                ResTable.BaggageClaimPickup = BaggageClaimPickup;
                ResTable.PetInCage = PetInCage;
                ResTable.ApproxDistance = TotalDistance;
                ResTable.ApproxTime = TimeTaken;


                DB.SubmitChanges();
                #region Update Mail

                Trans_Tbl_Login Driver = new Trans_Tbl_Login();
                if (DriverName != "Select Driver")
                {
                    Driver = DB.Trans_Tbl_Logins.Single(x => x.Sid == Convert.ToInt64(DropDowns[2]));
                }
                Trans_Reservation Booking = DB.Trans_Reservations.Single(x => x.sid == ResTable.sid);
                Trans_tbl_Vehicle_Info VehicleInfo = new Trans_tbl_Vehicle_Info();
                VehicleInfo = DB.Trans_tbl_Vehicle_Infos.Single(x => x.sid == Convert.ToInt64(Booking.VehicalType));

                ReservationHandler r = new ReservationHandler();
                r.AssignBooking(Convert.ToInt64(ResTable.sid), Convert.ToInt64(DropDowns[2]), DriverName);
                //StringBuilder sb = new StringBuilder();
                //StringBuilder sbDriver = new StringBuilder();
                //#region Customer Mail
                //sb.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
                //sb.Append("<title>limoallaround.com - Customer Receipt</title>  ");
                //sb.Append("<style>                                            ");
                //sb.Append(".Norm {font-family: Verdana;                       ");
                //sb.Append("font-size: 12px;                                 ");
                //sb.Append("font-color: red;                               ");
                //sb.Append(" }                                               ");
                //sb.Append(".heading {font-family: Verdana;                   ");
                //sb.Append("  font-size: 14px;                            ");
                //sb.Append("  font-weight: 800;                           ");
                //sb.Append("	 }                                                  ");
                //sb.Append("   td {font-family: Verdana;                         ");
                //sb.Append("	  font-size: 12px;                                  ");
                //sb.Append("	 }                                                  ");
                //sb.Append("  </style>                                           ");
                //sb.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
                //sb.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
                //sb.Append("");
                //sb.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
                //sb.Append("  <tbody><tr>");
                //sb.Append("    <td width='660' colspan='13' valign='top'>");
                //sb.Append("      <table width='100%'>");
                //sb.Append("        <tbody><tr>");
                //sb.Append("          <td colspan='2' width='100%' class='heading'><b>Bwi Shuttle Service</b></td>");
                //sb.Append("        </tr>");
                //sb.Append("        <tr>");
                //sb.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com/'>http://www.bwishuttleservice.com</a></td>");
                //sb.Append("        </tr>");
                //sb.Append("        <tr>");
                //sb.Append("          <td colspan='2' width='100%'><a href='mailto:limoallaround.com@gmail.com'>limoallaround.com@gmail.com</a></td>");
                //sb.Append("        </tr>");
                //sb.Append("        <tr>");
                //sb.Append("          <td colspan='2' width='25%'>Tel: 410-235-2265</td>");
                //sb.Append("          <td width='75%' align='right'><font size='3'>");
                //sb.Append("		Receipt");
                //sb.Append("	     </font></td>");
                //sb.Append("        </tr>");
                //sb.Append("      </tbody></table>");
                //sb.Append("     </td>");
                //sb.Append("  </tr><tr>");
                //sb.Append("  </tr><tr>");
                //sb.Append("    <td colspan='13' width='660'>");
                //sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                //sb.Append("  	<tbody><tr>");
                //sb.Append("  	  <td width='70%' style='border-bottom:medium solid blue; font-size:16px'><b>Reservation # " + Booking.ReservationID + "</b></td>");
                //sb.Append("  	  <td width='30%' align='right' style='border-bottom:medium solid blue;'>Service Date: " + EmailManager.GetDate(Booking.Date) + "</td>");
                //sb.Append("  	</tr>");
                //sb.Append("  	<tr>");
                //sb.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br>Your Booking with following Details has been updated!  Below please find your confirmation. If any of the information appears to be incorrect, please contact our office immediately to correct it.<br>&nbsp;</td>");
                //sb.Append("	</tr>");
                //sb.Append("      </tbody></table>");
                //sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                //sb.Append("	<tbody><tr>");
                //sb.Append("	  <td class='heading'>Pick-up Date:</td>");
                //sb.Append("	  <td width='75%'>" + EmailManager.GetDate(Booking.Date) + "</td>                 ");
                //sb.Append("	</tr>                                             ");

                //if (DriverName != "Select Driver")
                //{
                //    sb.Append("	<tr>                                              ");
                //    sb.Append("	  <td class='heading'>Driver Name:</td>          ");
                //    sb.Append("	  <td width='75%'>" + Driver.sFirstName + " " + Driver.sLastName + "</td>                      ");
                //    sb.Append("	</tr>                                             ");


                //    sb.Append("	<tr>                                              ");
                //    sb.Append("	  <td class='heading'>Driver Mobile:</td>          ");
                //    sb.Append("	  <td width='75%'>" + Driver.sMobile + "</td>                      ");
                //    sb.Append("	</tr>                                             ");


                //    sb.Append("	<tr>                                              ");
                //    sb.Append("	  <td class='heading'>Driver Email:</td>          ");
                //    sb.Append("	  <td width='75%'>" + Driver.sEmail + "</td>                      ");
                //    sb.Append("	</tr>                                             ");
                //}


                //sb.Append("	<tr>                                              ");
                //sb.Append("	  <td class='heading'>Pick-up Time:</td>          ");
                //if (Booking.Service == "Point To Point Reservation")
                //    sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.P2PTime) + "</td>");
                //else
                //    sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.FlightTime) + "</td>");
                //sb.Append("	</tr>                                             ");
                //sb.Append("	<tr>                                              ");
                //sb.Append("	  <td class='heading'>Primary Contact:</td>       ");
                //sb.Append("	  <td width='75%'>" + Booking.FirstName + " " + Booking.LastName + "</td>           ");
                //sb.Append("	</tr>                                             ");
                //sb.Append("	<tr>                                              ");
                //sb.Append("	  <td class='heading'>Contact Number:</td>        ");
                //sb.Append("	  <td width='75%'>" + Booking.ContactNumber + "</td>               ");
                //sb.Append("	</tr>                                             ");
                //sb.Append("	<tr>                                              ");
                //sb.Append("	  <td class='heading'>No. of Passenger:</td>         ");
                //sb.Append("	  <td width='75%'>" + Booking.Persons + "</td>                          ");
                //sb.Append("	</tr>                                             ");
                ////sb.Append("	<tr>                                              ");
                ////sb.Append("	  <td class='heading'>No. of Children:</td>       ");
                ////sb.Append("	  <td width='75%'>" + Booking.Child + "</td>                          ");
                ////sb.Append("	</tr>	                                          ");
                //sb.Append("	<tr>                                              ");
                //sb.Append("	  <td class='heading' valign='Top' style='border-bottom:thin solid green;'>Vehicle Type:</td>           ");
                //sb.Append("	  <td width='75%' style='border-bottom:thin solid green;'>                                              ");
                //sb.Append("	  " + VehicleInfo.Model + "<br>&nbsp;</td>                                                                                    ");
                //sb.Append("	</tr>                                                                                                   ");
                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td class='heading'>Trip Routing Information:</td>                                                    ");
                //sb.Append("	                                                                                                        ");
                //sb.Append("	  <td><b>Pick-up Location: &nbsp;</b>" + Booking.PickUpAddress + "</td>                         ");
                //sb.Append("	                                                                                                        ");
                //sb.Append("	</tr>                                                                                                   ");
                ////sb.Append("	<tr>                                                                                                    ");
                ////sb.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                ////sb.Append("	  <td><b>Pick-up Instructions: &nbsp;</b>your reservation is canceled by xx/xx/2016<br>&nbsp;</td>      ");
                ////sb.Append("	</tr>	                                                                                                ");
                //sb.Append("                                                                                                         ");
                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                //sb.Append("	                                                                                                        ");
                //sb.Append("	  <td><b>Drop-off Location: &nbsp;</b>" + Booking.Destination + "</td>                                                  ");
                //sb.Append("	                                                                                                        ");
                //sb.Append("	</tr>	                                                                                                ");
                //sb.Append("        <tr>                                                                                             ");
                //sb.Append("          <td style='border-bottom:thin solid green;' colspan='2'>&nbsp;</td>                            ");
                //sb.Append("        </tr>                                                                                            ");
                //sb.Append("                                                                                                         ");
                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td class='heading' valign='top'>Payment Method:</td>                                                 ");
                //sb.Append("	  <td width='75%'>" + Booking.CCPayment + "<br>&nbsp;</td>                                                            ");
                //sb.Append("	</tr>                                                                                                   ");
                //sb.Append("                                                                                                         ");
                //sb.Append("                                                                                                         ");
                //sb.Append(" 	<tr>                                                                                                ");
                //sb.Append("	  <td class='heading'>Quoted Rate:</td>                                                                 ");
                //sb.Append("	  <td>Rs. " + Booking.Fare + "                                                                                         ");
                //sb.Append("	</td></tr>                                                                                              ");
                //sb.Append("                                                                                                         ");
                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td class='heading'>Gratuity:</td>                                                                    ");
                //sb.Append("	  <td>Rs. " + EmailManager.GetGratuity(Convert.ToSingle(Booking.Fare), Booking.Gratuity.ToString()) + "                                                                                          ");
                //sb.Append("	</td></tr>                                                                                              ");
                //sb.Append("                                                                                                         ");
                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td class='heading'>Reservation Total:</td>                                                           ");
                //sb.Append("	  <td>Rs. " + Booking.TotalFare + "                                                                                         ");
                //sb.Append("	</td></tr>                                                                                              ");
                //sb.Append("	<tr>                                                                                                    ");
                //sb.Append("	  <td style='border-bottom:thin solid green;' class='heading' valign='top'><font color='red'>Total Due:</font></td>");
                //sb.Append("	  <td style='border-bottom:thin solid green;'><font color='red'>Rs. 0.00<br>&nbsp;</font></td>     ");
                //sb.Append("	</tr>                                                                                              ");
                //sb.Append("	<tr>                                                                                               ");
                //sb.Append("	  <td class='heading' valign='top'>Terms &amp; Conditions / Reservation Agreement:</td>            ");
                //sb.Append("	  <td>Guest agrees that there will be no smoking in our vehicles.<br>                              ");
                //sb.Append("		Guest assures that no illegal drugs are brought into our vehicles.<br>                         ");
                //sb.Append("		Guest agrees that no alcoholic beverages shall be brought into our vehicles.                   ");
                //sb.Append("		Guest agrees that the passengers capacity of vehicle provided shall not be exceeded.<br>       ");
                //sb.Append("		In case of misconduct by your party, chauffeur has the right to terminate this agreement without refunds.<br>");
                //sb.Append("		Guest holds Bwi Shuttle Service harmless and not liable for any personal or material damages arising from the conduct of his/her party.<br>");
                //sb.Append("		Guest is responsible for the full payment of any overtime charges, beyond the original agreement.<br>");
                //sb.Append("		Guest agrees to 100% payment charged to his/her credit card at the time reservation has been made.<br>");
                //sb.Append("		Luxury Sedan guests can cancel 3 hours before pickup time with $10.00 processing fee; if not you will be charged the full amount of reservation. Stretch limousine and Van Guests can cancel 48 hours before pickup time with a $20.00 processing fee; if you cancel after 48 hours, you will be billed for a minimum 3 hours of service charges.<br>");
                //sb.Append("		Guest acknowledges that he/she understands that Bwi Shuttle Service imposes a service fee for late night service (11:00PM TO 5:00AM).<br>");
                //sb.Append("		Guest ackowledges that he/she understands that Bwi Shuttle Service imposes an additional service fee for Incoming International flights.<br>		");
                //sb.Append("		Bwi Shuttle Service cannot be held responsible for mechanical problems, inclement weather, or other uncontrollable circumstances resulting in the inability to start a job as it is schedule time or complete a job. In the event that the requested vehicle can not be provided; Bwi Shuttle Service may provide a vehicle of equal quality.<br>");
                //sb.Append("		Bwi Shuttle Service reserves the right to change rates without notice.  If a rate change is necessary due to a destination being outside of the immediate service area, technical error or increase in fuel charges; Bwi Shuttle Service will make every effort to notify the Guest of the change.<br>");
                //sb.Append("	</td>");
                //sb.Append("	</tr>");
                //sb.Append("      </tbody></table>    ");
                //sb.Append("  </td></tr>");
                //sb.Append("");
                //sb.Append("");
                //sb.Append("                          ");
                //sb.Append("");
                //sb.Append("                          ");
                //sb.Append("</tbody></table>          ");
                //sb.Append("                          ");
                //sb.Append("                          ");
                //sb.Append("</body></html>            ");
                //string Mail = sb.ToString();
                //#endregion

                //#region Driver Mail
                //if (DriverName != "Select Driver")
                //{
                //    sbDriver.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
                //    sbDriver.Append("<title>limoallaround.com - Customer Receipt</title>  ");
                //    sbDriver.Append("<style>                                            ");
                //    sbDriver.Append(".Norm {font-family: Verdana;                       ");
                //    sbDriver.Append("font-size: 12px;                                 ");
                //    sbDriver.Append("font-color: red;                               ");
                //    sbDriver.Append(" }                                               ");
                //    sbDriver.Append(".heading {font-family: Verdana;                   ");
                //    sbDriver.Append("  font-size: 14px;                            ");
                //    sbDriver.Append("  font-weight: 800;                           ");
                //    sbDriver.Append("	 }                                                  ");
                //    sbDriver.Append("   td {font-family: Verdana;                         ");
                //    sbDriver.Append("	  font-size: 12px;                                  ");
                //    sbDriver.Append("	 }                                                  ");
                //    sbDriver.Append("  </style>                                           ");
                //    sbDriver.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
                //    sbDriver.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
                //    sbDriver.Append("");
                //    sbDriver.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
                //    sbDriver.Append("  <tbody><tr>");
                //    sbDriver.Append("    <td width='660' colspan='13' valign='top'>");
                //    sbDriver.Append("      <table width='100%'>");
                //    sbDriver.Append("        <tbody><tr>");
                //    sbDriver.Append("          <td colspan='2' width='100%' class='heading'><b>Bwi Shuttle Service</b></td>");
                //    sbDriver.Append("        </tr>");
                //    sbDriver.Append("        <tr>");
                //    sbDriver.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com/'>http://www.bwishuttleservice.com</a></td>");
                //    sbDriver.Append("        </tr>");
                //    sbDriver.Append("        <tr>");
                //    sbDriver.Append("          <td colspan='2' width='100%'><a href='mailto:limoallaround.com@gmail.com'>limoallaround.com@gmail.com</a></td>");
                //    sbDriver.Append("        </tr>");
                //    sbDriver.Append("        <tr>");
                //    sbDriver.Append("          <td colspan='2' width='25%'>Tel: 410-235-2265</td>");
                //    sbDriver.Append("          <td width='75%' align='right'><font size='3'>");
                //    sbDriver.Append("		Receipt");
                //    sbDriver.Append("	     </font></td>");
                //    sbDriver.Append("        </tr>");
                //    sbDriver.Append("      </tbody></table>");
                //    sbDriver.Append("     </td>");
                //    sbDriver.Append("  </tr><tr>");
                //    sbDriver.Append("  </tr><tr>");
                //    sbDriver.Append("    <td colspan='13' width='660'>");
                //    sbDriver.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                //    sbDriver.Append("  	<tbody><tr>");
                //    sbDriver.Append("  	  <td width='70%' style='border-bottom:medium solid blue; font-size:16px'><b>Reservation # " + Booking.ReservationID + "</b></td>");
                //    sbDriver.Append("  	  <td width='30%' align='right' style='border-bottom:medium solid blue;'>Service Date: " + EmailManager.GetDate(Booking.Date) + "</td>");
                //    sbDriver.Append("  	</tr>");
                //    sbDriver.Append("  	<tr>");
                //    sbDriver.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br>Hi, " + Driver.sFirstName + " " + Driver.sLastName + " Your Booking with following Details has been updated!.If any of the information appears to be incorrect, please contact our office immediately to correct it. <br>&nbsp;</td>");
                //    sbDriver.Append("	</tr>");
                //    sbDriver.Append("      </tbody></table>");
                //    sbDriver.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                //    sbDriver.Append("	<tbody><tr>");
                //    sbDriver.Append("	  <td class='heading'>Pick-up Date:</td>");
                //    sbDriver.Append("	  <td width='75%'>" + EmailManager.GetDate(Booking.Date) + "</td>                 ");
                //    sbDriver.Append("	</tr>                                             ");

                //    sbDriver.Append("	<tr>                                              ");
                //    sbDriver.Append("	  <td class='heading'>Customer Name:</td>          ");
                //    sbDriver.Append("	  <td width='75%'>" + Booking.FirstName + " " + Booking.LastName + "</td>                      ");
                //    sbDriver.Append("	</tr>                                             ");

                //    sbDriver.Append("	<tr>                                              ");
                //    sbDriver.Append("	  <td class='heading'>Customer Mobile:</td>          ");
                //    sbDriver.Append("	  <td width='75%'>" + Booking.ContactNumber + "</td>                      ");
                //    sbDriver.Append("	</tr>                                             ");

                //    sbDriver.Append("	<tr>                                              ");
                //    sbDriver.Append("	  <td class='heading'>Customer Email:</td>          ");
                //    sbDriver.Append("	  <td width='75%'>" + Booking.Email + "</td>                      ");
                //    sbDriver.Append("	</tr>                                             ");

                //    sbDriver.Append("	<tr>                                              ");
                //    sbDriver.Append("	  <td class='heading'>Pick-up Time:</td>          ");
                //    if (Booking.Service == "Point To Point Reservation")
                //        sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.P2PTime) + "</td>");
                //    else
                //        sb.Append("	  <td width='75%'>" + EmailManager.GetTime(Booking.FlightTime) + "</td>");
                //    sbDriver.Append("	</tr>                                             ");
                //}
                //sbDriver.Append("	  <td class='heading'>No. of Passenger:</td>         ");
                //sbDriver.Append("	  <td width='75%'>" + Booking.Persons + "</td>                          ");
                //sbDriver.Append("	</tr>                                             ");
                ////sbDriver.Append("	<tr>                                              ");
                ////sbDriver.Append("	  <td class='heading'>No. of Children:</td>       ");
                ////sbDriver.Append("	  <td width='75%'>" + Booking.Child + "</td>                          ");
                ////sbDriver.Append("	</tr>	                                          ");
                //sbDriver.Append("	<tr>                                              ");
                //sbDriver.Append("	  <td class='heading' valign='Top' style='border-bottom:thin solid green;'>Vehicle Type:</td>           ");
                //sbDriver.Append("	  <td width='75%' style='border-bottom:thin solid green;'>                                              ");
                //sbDriver.Append("	  " + VehicleInfo.Model + "<br>&nbsp;</td>                                                                                    ");
                //sbDriver.Append("	</tr>                                                                                                   ");
                //sbDriver.Append("	<tr>                                                                                                    ");
                //sbDriver.Append("	  <td class='heading'>Trip Routing Information:</td>                                                    ");
                //sbDriver.Append("	                                                                                                        ");
                //sbDriver.Append("	  <td><b>Pick-up Location: &nbsp;</b>" + Booking.PickUpAddress + "</td>                         ");
                //sbDriver.Append("	                                                                                                        ");
                //sbDriver.Append("	</tr>                                                                                                   ");
                ////sbDriver.Append("	<tr>                                                                                                    ");
                ////sbDriver.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                ////sbDriver.Append("	  <td><b>Pick-up Instructions: &nbsp;</b>your reservation is canceled by xx/xx/2016<br>&nbsp;</td>      ");
                ////sbDriver.Append("	</tr>	                                                                                                ");
                //sbDriver.Append("                                                                                                         ");
                //sbDriver.Append("	<tr>                                                                                                    ");
                //sbDriver.Append("	  <td class='heading'>&nbsp;</td>                                                                       ");
                //sbDriver.Append("	                                                                                                        ");
                //sbDriver.Append("	  <td><b>Drop-off Location: &nbsp;</b>" + Booking.Destination + "</td>                                                  ");
                //sbDriver.Append("	                                                                                                        ");
                //sbDriver.Append("	</tr>	                                                                                                ");
                //sbDriver.Append("        <tr>                                                                                             ");
                //sbDriver.Append("          <td style='border-bottom:thin solid green;' colspan='2'>&nbsp;</td>                            ");
                //sbDriver.Append("        </tr>                                                                                            ");
                //sbDriver.Append("                                                                                                         ");
                //sbDriver.Append("	<tr>                                                                                                    ");
                //sbDriver.Append("	  <td class='heading' valign='top'>Payment Method:</td>                                                 ");

                //sbDriver.Append("	  <td width='75%'>" + Booking.CCType + "<br>&nbsp;</td>                                                            ");
                //sbDriver.Append("	</tr>                                                                                                   ");
                //sbDriver.Append("                                                                                                         ");
                //sbDriver.Append("                                                                                                         ");
                //sbDriver.Append(" 	<tr>                                                                                                ");
                //sbDriver.Append("	  <td class='heading'>Quoted Rate:</td>                                                                 ");
                //sbDriver.Append("	  <td>Rs. " + Booking.Fare + "                                                                                         ");
                //sbDriver.Append("	</td></tr>                                                                                              ");
                //sbDriver.Append("                                                                                                         ");
                //sbDriver.Append("	<tr>                                                                                                    ");
                //sbDriver.Append("	  <td class='heading'>Gratuity:</td>                                                                    ");
                //sbDriver.Append("	  <td>Rs. " + EmailManager.GetGratuity(Convert.ToSingle(Booking.Fare), Booking.Gratuity.ToString()) + "                                                                                          ");
                //sbDriver.Append("	</td></tr>                                                                                              ");
                //sbDriver.Append("                                                                                                         ");
                //sbDriver.Append("	<tr>                                                                                                    ");
                //sbDriver.Append("	  <td class='heading'>Reservation Total:</td>                                                           ");
                //sbDriver.Append("	  <td>Rs. " + Booking.TotalFare + "                                                                                         ");
                //sbDriver.Append("	</td></tr>                                                                                              ");
                //sbDriver.Append("	<tr>                                                                                                    ");
                //sbDriver.Append("	  <td style='border-bottom:thin solid green;' class='heading' valign='top'><font color='red'>Total Due:</font></td>");
                //sbDriver.Append("	  <td style='border-bottom:thin solid green;'><font color='red'>Rs. 0.00<br>&nbsp;</font></td>     ");
                //sbDriver.Append("	</tr>                                                                                              ");
                //sbDriver.Append("	<tr>                                                                                               ");
                //sbDriver.Append("	  <td class='heading' valign='top'>Terms &amp; Conditions / Reservation Agreement:</td>            ");
                //sbDriver.Append("	  <td>Guest agrees that there will be no smoking in our vehicles.<br>                              ");
                //sbDriver.Append("		Guest assures that no illegal drugs are brought into our vehicles.<br>                         ");
                //sbDriver.Append("		Guest agrees that no alcoholic beverages shall be brought into our vehicles.                   ");
                //sbDriver.Append("		Guest agrees that the passengers capacity of vehicle provided shall not be exceeded.<br>       ");
                //sbDriver.Append("		In case of misconduct by your party, chauffeur has the right to terminate this agreement without refunds.<br>");
                //sbDriver.Append("		Guest holds Bwi Shuttle Service harmless and not liable for any personal or material damages arising from the conduct of his/her party.<br>");
                //sbDriver.Append("		Guest is responsible for the full payment of any overtime charges, beyond the original agreement.<br>");
                //sbDriver.Append("		Guest agrees to 100% payment charged to his/her credit card at the time reservation has been made.<br>");
                //sbDriver.Append("		Luxury Sedan guests can cancel 3 hours before pickup time with $10.00 processing fee; if not you will be charged the full amount of reservation. Stretch limousine and Van Guests can cancel 48 hours before pickup time with a $20.00 processing fee; if you cancel after 48 hours, you will be billed for a minimum 3 hours of service charges.<br>");
                //sbDriver.Append("		Guest acknowledges that he/she understands that Bwi Shuttle Service imposes a service fee for late night service (11:00PM TO 5:00AM).<br>");
                //sbDriver.Append("		Guest ackowledges that he/she understands that Bwi Shuttle Service imposes an additional service fee for Incoming International flights.<br>		");
                //sbDriver.Append("		Bwi Shuttle Service cannot be held responsible for mechanical problems, inclement weather, or other uncontrollable circumstances resulting in the inability to start a job as it is schedule time or complete a job. In the event that the requested vehicle can not be provided; Bwi Shuttle Service may provide a vehicle of equal quality.<br>");
                //sbDriver.Append("		Bwi Shuttle Service reserves the right to change rates without notice.  If a rate change is necessary due to a destination being outside of the immediate service area, technical error or increase in fuel charges; Bwi Shuttle Service will make every effort to notify the Guest of the change.<br>");
                //sbDriver.Append("	</td>                    ");
                //sbDriver.Append("	</tr>                    ");
                //sbDriver.Append("      </tbody></table>    ");
                //sbDriver.Append("  </td></tr>              ");
                //sbDriver.Append("                          ");
                //sbDriver.Append("                          ");
                //sbDriver.Append("                          ");
                //sbDriver.Append("                          ");
                //sbDriver.Append("                          ");
                //sbDriver.Append("</tbody></table>          ");
                //sbDriver.Append("                          ");
                //sbDriver.Append("                          ");
                //sbDriver.Append("</body></html>            ");
                //Mail = sbDriver.ToString();
                //#endregion

                //#region Mailing Section
                //try
                //{

                //    System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                //    mailMsg.From = new MailAddress("support@memorEbook.in", "Transfers ");
                //    mailMsg.To.Add(Booking.Email);
                //    mailMsg.Subject = "Updated Booking-" + Booking.ReservationID;
                //    mailMsg.IsBodyHtml = true;
                //    mailMsg.Body = sb.ToString();
                //    SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                //    mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                //    mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //    mailObj.EnableSsl = false;
                //    mailMsg.Sender = new MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                //    mailObj.Send(mailMsg);
                //    mailMsg.Dispose();
                //}
                //catch
                //{

                //    return DBHelper.DBReturnCode.EXCEPTION;
                //}

                //if (DriverName != "Select Driver")
                //{
                //    try
                //    {

                //        System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                //        mailMsg.From = new MailAddress("support@memorEbook.in", "Transfers ");
                //        mailMsg.To.Add(Driver.sEmail);
                //        mailMsg.Subject = "Updated Booking  -" + Booking.ReservationID;
                //        mailMsg.IsBodyHtml = true;
                //        mailMsg.Body = sbDriver.ToString();
                //        SmtpClient mailObj = new SmtpClient("relay-hosting.secureserver.net", 25);
                //        mailObj.Credentials = new NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                //        mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //        mailObj.EnableSsl = false;
                //        mailMsg.Sender = new MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                //        mailObj.Send(mailMsg);
                //        mailMsg.Dispose();


                //    }
                //    catch
                //    {

                //        return DBHelper.DBReturnCode.EXCEPTION;
                //    }
                //}
                //#endregion


                #endregion
                retcode = DBHelper.DBReturnCode.SUCCESS;
            }
            catch
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retcode;
        }

        public static DBHelper.DBReturnCode HourReservation(string[] Input, string[] DropDowns, string[] CheckBoxes, string Remarks, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string Email, string ContactNo, decimal GratuityAmount, string DriverID)
        {

            DBHandlerDataContext DB = new DBHandlerDataContext();
            Trans_Reservation ResTable = new Trans_Reservation();
            DBHelper.DBReturnCode retcode;
            GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];

            try
            {
                Int64 DriverUid;
                var CustomerSid = DB.Trans_Tbl_CustomerMasters.Single(x => x.Email == Email);
                ResTable.uid = CustomerSid.sid;
                if (DriverName == "Select Driver")
                {
                    ResTable.AssignedTo = null;
                    ResTable.Status = "Requested";
                }
                else
                {
                    ResTable.DriverSid = Convert.ToInt32(DriverID);
                    ResTable.AssignedTo = DriverName;
                    ResTable.Status = "Confirmed";
                }
                Random generator = new Random();
                int Reservation = generator.Next(1000000, 10000000);

                ResTable.ReservationID = "RES-" + Reservation;
                ResTable.InvoiceNum = "INV-" + Reservation;
                ResTable.VoucherNum = "VCH-" + Reservation;

                //ResTable.AccountNumber = Input[0];
                ResTable.FirstName = Input[0];
                ResTable.LastName = Input[1];
                ResTable.ContactNumber = ContactNo;
                ResTable.Email = Email;
                ResTable.ReservationDate = Input[2];
                //ResTable.OrganisationCode = Input[6];
                ResTable.Persons = Input[3];
                ResTable.CCLast4 = Input[4];
                //ResTable.Child = Input[8];
                ResTable.FlightNumber = "";
                ResTable.FlightTime = "";
                ResTable.FlightDate = "";
                //ResTable.CCLast4 = Input[9];


                ResTable.Fare = Convert.ToDecimal(Input[7]);
                //if (Input[11] != "")
                //{
                //    ResTable.Gratuity = Convert.ToDecimal(Input[11]);
                //}
                //else
                //{
                //    ResTable.Gratuity = 0;
                //}
                if (Input[8] != "")
                {
                    ResTable.Parking = Convert.ToDecimal(Input[8]);
                }
                else
                {
                    ResTable.Parking = 0;
                }
                if (Input[10] != "")
                {
                    ResTable.Toll = Convert.ToDecimal(Input[10]);
                }
                else
                {
                    ResTable.Toll = 0;
                }
                ResTable.TotalFare = Convert.ToDecimal(Input[9]);


                ResTable.PickUpAddress = Input[5];
                ResTable.P2PTime = EmailManager.GetTime(Input[6]);
                ResTable.Source = Input[5];
                ResTable.Destination = "-";
                //ResTable.PickUpCity = Input[18];
                //ResTable.PickUpState = Input[19];
                //ResTable.PickUpZipCode = Input[20];

                ResTable.Remark = Remarks;
                ResTable.Airlines = "";
                ResTable.Service = "Hourly Reservation";
                ResTable.AirPortName = "";
                ResTable.VehicalType = DropDowns[0];
                ResTable.Hours = DropDowns[2];
                ResTable.TravelType = DropDowns[3];
                ResTable.Gratuity = Convert.ToDecimal(DropDowns[4]);
                ResTable.OfferCode = DropDowns[6];
                ResTable.CCType = DropDowns[7];
                ResTable.GratuityAmount = GratuityAmount;
                ResTable.TravelType = "";
                //ResTable.Status = "Pending";
                //ResTable.Status = Status;
                ResTable.Paid = Convert.ToBoolean(CheckBoxes[0]);
                //ResTable.Completed = Convert.ToBoolean(CheckBoxes[1]);
                ResTable.Collect = Convert.ToBoolean(CheckBoxes[2]);
                ResTable.CCPayment = Convert.ToBoolean(CheckBoxes[3]);
                ResTable.ChildCarSheet = Convert.ToBoolean(CheckBoxes[4]);
                ResTable.Date = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.ModifiedDate = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.UpdatedBy = Global.sEmail;
                ResTable.MeetGreat = MeetGreat;
                ResTable.SpecialAssistant = SpecialAssistant;
                ResTable.CurbSidePickUp = CurbSidePickUp;


                ResTable.PetInCage = PetInCage;
                ResTable.ReservationType = "Admin";


                DB.Trans_Reservations.InsertOnSubmit(ResTable);
                DB.SubmitChanges();
                var BookingSid = (from Obj in DB.Trans_Reservations where Obj.ReservationID == "RES-" + Reservation select Obj.sid).ToList();
                ReservationHandler r = new ReservationHandler();
                r.AssignBooking(Convert.ToInt64(BookingSid[0]), Convert.ToInt64(DropDowns[1]), DriverName);
                retcode = DBHelper.DBReturnCode.SUCCESS;
            }
            catch
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retcode;
        }

        public static DBHelper.DBReturnCode UpdateHourReservation(int sid, string[] Input, string[] DropDowns, string[] CheckBoxes, string Remarks, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string Email, string ContactNo, decimal GratuityAmount, string DriverID)
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();
            Trans_Reservation ResTable = DB.Trans_Reservations.Single(x => x.sid == sid);
            DBHelper.DBReturnCode retcode;
            GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];

            try
            {
                var CustomerSid = DB.Trans_Tbl_CustomerMasters.Single(x => x.Email == Email);
                ResTable.uid = CustomerSid.sid;
                if (DriverName == "Select Driver")
                {
                    ResTable.AssignedTo = null;
                    ResTable.Status = "Requested";
                }
                else
                {
                    ResTable.DriverSid = Convert.ToInt32(DriverID);
                    ResTable.AssignedTo = DriverName;
                    ResTable.Status = "Confirmed";
                }
                //Random generator = new Random();
                //int Reservation = generator.Next(1000000, 10000000);
                //
                //ResTable.ReservationID = "RES-" + Reservation;
                //ResTable.InvoiceNum = "INV-" + Reservation;
                //ResTable.VoucherNum = "VCH-" + Reservation;

                ResTable.FirstName = Input[0];
                ResTable.LastName = Input[1];
                ResTable.ContactNumber = ContactNo;
                ResTable.Email = Email;
                ResTable.ReservationDate = Input[2];
                //ResTable.OrganisationCode = Input[6];
                ResTable.Persons = Input[3];
                ResTable.CCLast4 = Input[4];
                //ResTable.Child = Input[8];
                ResTable.FlightNumber = "";
                ResTable.FlightTime = "";
                ResTable.FlightDate = "";
                //ResTable.CCLast4 = Input[9];


                ResTable.Fare = Convert.ToDecimal(Input[7]);
                //if (Input[11] != "")
                //{
                //    ResTable.Gratuity = Convert.ToDecimal(Input[11]);
                //}
                //else
                //{
                //    ResTable.Gratuity = 0;
                //}
                if (Input[8] != "")
                {
                    ResTable.Parking = Convert.ToDecimal(Input[8]);
                }
                else
                {
                    ResTable.Parking = 0;
                }
                if (Input[10] != "")
                {
                    ResTable.Toll = Convert.ToDecimal(Input[10]);
                }
                else
                {
                    ResTable.Toll = 0;
                }
                ResTable.TotalFare = Convert.ToDecimal(Input[9]);
                ResTable.PickUpAddress = Input[5];
                ResTable.P2PTime = EmailManager.GetTime(Input[6]);
                ResTable.Source = Input[5];
                ResTable.Destination = "-";
                //ResTable.PickUpCity = Input[18];
                //ResTable.PickUpState = Input[19];
                //ResTable.PickUpZipCode = Input[20];

                ResTable.Remark = Remarks;
                ResTable.Airlines = "";
                ResTable.Service = "Hourly Reservation";
                ResTable.AirPortName = "";
                //ResTable.CCType = DropDowns[0];
                ResTable.VehicalType = DropDowns[0];
                ResTable.Hours = DropDowns[2];
                ResTable.TravelType = DropDowns[3];
                ResTable.Gratuity = Convert.ToDecimal(DropDowns[5]);
                ResTable.OfferCode = DropDowns[6];
                ResTable.CCType = DropDowns[7];
                ResTable.GratuityAmount = GratuityAmount;
                ResTable.TravelType = "";
                //ResTable.Status = "Pending";
                //ResTable.Status = Status;
                ResTable.Paid = Convert.ToBoolean(CheckBoxes[0]);
                //ResTable.Completed = Convert.ToBoolean(CheckBoxes[1]);
                ResTable.Collect = Convert.ToBoolean(CheckBoxes[2]);
                ResTable.CCPayment = Convert.ToBoolean(CheckBoxes[3]);
                ResTable.ChildCarSheet = Convert.ToBoolean(CheckBoxes[4]);

                ResTable.ModifiedDate = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.UpdatedBy = Global.sEmail;
                ResTable.MeetGreat = MeetGreat;
                ResTable.SpecialAssistant = SpecialAssistant;
                ResTable.CurbSidePickUp = CurbSidePickUp;
                ResTable.BaggageClaimPickup = BaggageClaimPickup;
                ResTable.PetInCage = PetInCage;

                DB.SubmitChanges();
                ReservationHandler ar = new ReservationHandler();
                ar.AssignBooking(Convert.ToInt64(ResTable.sid), Convert.ToInt64(DriverID), DriverName);
               

                retcode = DBHelper.DBReturnCode.SUCCESS;
            }
            catch
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retcode;
        }

        public static DBHelper.DBReturnCode ShuttleReservation(string[] Input, string[] DropDowns, string[] CheckBoxes, string Remarks, string TravelType, string TotalDistance, string TimeTaken, string source, string destination, string AirportName, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string Airlines, string DriverID, string Email, string ContactNo, decimal GratuityAmount, decimal OfferCode, string Adults, string Childs, out string Response)
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();
            Trans_Reservation ResTable = new Trans_Reservation();
            DBHelper.DBReturnCode retcode; Response = "";
            GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
            //string a = Global.sEmail;
            try
            {
                var CustomerSid = DB.Trans_Tbl_CustomerMasters.Single(x => x.Email == Email);
                //var CustomerSid = DB.Proc_AddSelectCustomer(Input[0], Input[1], Email, ContactNo, "");
                ResTable.uid = CustomerSid.sid;
                if (DriverName == "Select Driver")
                {
                    //ResTable.uid = null;
                    ResTable.AssignedTo = "not assign";
                    ResTable.Status = "Requested";
                }
                else
                {
                    ResTable.DriverSid = Convert.ToInt32(DriverID);
                    ResTable.Status = "Confirmed";
                    //ResTable.AssignedTo = DropDowns[4];
                    //ResTable.DriverSid = Convert.ToInt32(DriverID);
                }
                Random generator = new Random();
                int Reservation = generator.Next(1000000, 10000000);

                ResTable.ReservationID = "RES-" + Reservation;
                ResTable.InvoiceNum = "INV-" + Reservation;
                ResTable.VoucherNum = "VCH-" + Reservation;

                //ResTable.AccountNumber = Input[0];
                ResTable.FirstName = Input[0];
                ResTable.LastName = Input[1];
                ResTable.ContactNumber = ContactNo;
                ResTable.Email = Email;
                ResTable.ReservationDate = Input[2];
                //ResTable.OrganisationCode = Input[6];
                ResTable.Persons = Input[6];
                ResTable.Adult = Adults;
                ResTable.Child = Childs;
                ResTable.FlightNumber = Input[4];
                ResTable.FlightDate = Input[2];
                //ResTable.AssignedTo = Input[12];
                ResTable.CCLast4 = Input[5];
                ResTable.Fare = Convert.ToDecimal(Input[8]);
                if (DropDowns[4] == "-")
                {
                    ResTable.Gratuity = 0;
                }
                else
                {
                    ResTable.Gratuity = Convert.ToDecimal(DropDowns[4]);
                }
                ResTable.GratuityAmount = GratuityAmount;

                if (Input[9] != "")
                {
                    ResTable.Parking = Convert.ToDecimal(Input[9]);
                }
                else
                {
                    ResTable.Parking = 0;
                }
                if (Input[12] != "")
                {
                    ResTable.Toll = Convert.ToDecimal(Input[12]);
                }
                else
                {
                    ResTable.Toll = 0;
                }
                ResTable.late_nite = Input[10];
                ResTable.TotalFare = Convert.ToDecimal(Input[11]);

                ResTable.Remark = Remarks;
                ResTable.Airlines = Airlines;
                ResTable.AirPortName = AirportName;
                ResTable.Service = DropDowns[0];
                ResTable.TravelType = DropDowns[1];
                ResTable.VehicalType = DropDowns[2];
                ResTable.OfferCode = DropDowns[5];
                ResTable.CCType = DropDowns[6];
                
                ResTable.ApproxDistance = TotalDistance;
                ResTable.ApproxTime = TimeTaken;
                string[] Source = source.Split(',');
                if (ResTable.Service == "From Airport")
                {
                    ResTable.Service = "From Airport Shuttle";
                    ResTable.Source = Source[0];
                    ResTable.Destination = destination;
                    ResTable.FlightTime = EmailManager.GetTime(Input[3]);
                    ResTable.PickUpAddress = Source[0];
                    ResTable.DropAddress = destination;
                }
                else
                {
                    ResTable.Service = "To Airport Shuttle";
                    ResTable.Source = Input[7];
                    ResTable.PickUpAddress = Input[7];
                    ResTable.DropAddress = destination;
                    ResTable.Destination = destination;
                    ResTable.Pickup_Time = EmailManager.GetTime(Input[3]);
                }
                //ResTable.Status = "Pending";

                //ResTable.Status = Status;
                ResTable.Paid = Convert.ToBoolean(CheckBoxes[0]);
                //ResTable.Completed = Convert.ToBoolean(CheckBoxes[1]);
                ResTable.Collect = Convert.ToBoolean(CheckBoxes[2]);
                ResTable.CCPayment = Convert.ToBoolean(CheckBoxes[3]);
                ResTable.ChildCarSheet = Convert.ToBoolean(CheckBoxes[4]);
                ResTable.Date = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.ModifiedDate = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.UpdatedBy = Global.sEmail;
                ResTable.MeetGreat = MeetGreat;
                ResTable.SpecialAssistant = SpecialAssistant;
                ResTable.CurbSidePickUp = CurbSidePickUp;
                //ResTable.BaggageClaimPickup = BaggageClaimPickup;
                ResTable.PetInCage = PetInCage;
                ResTable.ReservationType = "Admin";
                ResTable.OfferCode = OfferCode.ToString();

                DB.Trans_Reservations.InsertOnSubmit(ResTable);
                DB.SubmitChanges();

                var BookingSid = (from Obj in DB.Trans_Reservations where Obj.ReservationID == "RES-" + Reservation select Obj.sid).ToList();
                ReservationHandler r = new ReservationHandler();
                Response = r.AssignBooking(Convert.ToInt64(BookingSid[0]), Convert.ToInt64(DriverID), DriverName);
                if (Response.Contains("Error"))
                {
                    retcode = DBHelper.DBReturnCode.EXCEPTION;
                }
                else
                    retcode = DBHelper.DBReturnCode.SUCCESS;
            }
            catch
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retcode;
        }

        public static DBHelper.DBReturnCode UpdateShuttleReservation(int sid, string[] Input, string[] DropDowns, string[] CheckBoxes, string Remarks, string TravelType, string TotalDistance, string TimeTaken, string source, string destination, string AirportName, bool MeetGreat, bool SpecialAssistant, bool CurbSidePickUp, bool BaggageClaimPickup, bool PetInCage, string Status, string DriverName, string Airlines, string DriverID, string Email, string ContactNo, decimal GratuityAmount, decimal OfferCode, string Adults, string Childs)
        {

            DBHandlerDataContext DB = new DBHandlerDataContext();
            Trans_Reservation ResTable = DB.Trans_Reservations.Single(x => x.sid == sid);
            DBHelper.DBReturnCode retcode;
            GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
            Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];

            try
            {
                var CustomerSid = DB.Proc_AddSelectCustomer(Input[0], Input[1], Email, ContactNo, "");
                ResTable.uid = CustomerSid;
                if (DriverName == "Select Driver")
                {
                    ResTable.AssignedTo = null;
                    ResTable.Status = "Requested";
                }
                else
                {
                    ResTable.DriverSid = Convert.ToInt32(DriverID);
                    ResTable.AssignedTo = DriverName;
                    ResTable.Status = "Confirmed";
                }

                ResTable.FirstName = Input[0];
                ResTable.LastName = Input[1];
                ResTable.ContactNumber = ContactNo;
                ResTable.Email = Email;
                ResTable.ReservationDate = Input[2];
                //ResTable.OrganisationCode = Input[6];
                ResTable.Persons = Input[6];
                ResTable.Adult = Adults;
                ResTable.Child = Childs;
                ResTable.FlightNumber = Input[4];
                ResTable.FlightDate = Input[2];
                //ResTable.AssignedTo = Input[12];
                ResTable.CCLast4 = Input[5];
                ResTable.Fare = Convert.ToDecimal(Input[8]);
                ResTable.Gratuity = Convert.ToDecimal(DropDowns[4]);
                ResTable.GratuityAmount = GratuityAmount;
                //if(Input[15]!="")
                //{
                //    ResTable.Gratuity = Convert.ToDecimal(Input[15]);
                //}
                //else
                //{
                //    ResTable.Gratuity = 0;
                //}
                if (Input[9] != "")
                {
                    ResTable.Parking = Convert.ToDecimal(Input[9]);
                }
                else
                {
                    ResTable.Parking = 0;
                }
                if (Input[12] != "")
                {
                    ResTable.Toll = Convert.ToDecimal(Input[12]);
                }
                else
                {
                    ResTable.Toll = 0;
                }
                ResTable.TotalFare = Convert.ToDecimal(Input[11]);

                ResTable.Remark = Remarks;
                ResTable.Airlines = Airlines;
                ResTable.AirPortName = AirportName;
                ResTable.Service = DropDowns[0];
                ResTable.TravelType = DropDowns[1];
                ResTable.VehicalType = DropDowns[2];
                ResTable.OfferCode = DropDowns[5];
                ResTable.CCType = DropDowns[6];
                ResTable.ApproxDistance = TotalDistance;
                ResTable.ApproxTime = TimeTaken;

                //var Splitter = Input[3].Split(':');
                //if()
                if (ResTable.Service == "From Airport")
                {
                    ResTable.Service = "From Airport Shuttle";
                    ResTable.Source = source;
                    ResTable.PickUpAddress = source;
                    ResTable.DropAddress = destination;
                    ResTable.Destination = destination;
                    ResTable.FlightTime = EmailManager.GetTime(Input[3]);
                }
                else
                {
                    ResTable.Service = "To Airport Shuttle";
                    ResTable.Source = source;
                    ResTable.Destination = destination;
                    ResTable.Pickup_Time = EmailManager.GetTime(Input[3]);
                    ResTable.PickUpAddress = source;
                    ResTable.DropAddress = destination;
                }
                //ResTable.Status = "Pending";
                //ResTable.Status = Status;
                ResTable.Paid = Convert.ToBoolean(CheckBoxes[0]);
                ResTable.Completed = Convert.ToBoolean(CheckBoxes[1]);
                ResTable.Collect = Convert.ToBoolean(CheckBoxes[2]);
                ResTable.CCPayment = Convert.ToBoolean(CheckBoxes[3]);
                ResTable.ChildCarSheet = Convert.ToBoolean(CheckBoxes[4]);
                // ResTable.Date = DateTime.Now.ToString("dd/MM/yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.ModifiedDate = DateTime.Now.ToString("dd-MM-yyy HH:mm", System.Globalization.CultureInfo.CurrentCulture);
                ResTable.UpdatedBy = Global.sEmail;
                ResTable.MeetGreat = MeetGreat;
                ResTable.SpecialAssistant = SpecialAssistant;
                ResTable.CurbSidePickUp = CurbSidePickUp;
                ResTable.BaggageClaimPickup = BaggageClaimPickup;
                ResTable.PetInCage = PetInCage;
                ResTable.OfferCode = OfferCode.ToString();

                DB.SubmitChanges();

                //var BookingSid = (from Obj in DB.Trans_Reservations where Obj.ReservationID == "RES-" + Reservation select Obj.sid).ToList();
                ReservationHandler r = new ReservationHandler();
                r.AssignBooking(Convert.ToInt64(ResTable.sid), Convert.ToInt64(DriverID), DriverName);
                retcode = DBHelper.DBReturnCode.SUCCESS;

                retcode = DBHelper.DBReturnCode.SUCCESS;
            }
            catch
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retcode;
        }

        public static DBHelper.DBReturnCode LoadReservations(out List<BWI.Trans_Reservation> ResvationList)
        {

            DBHandlerDataContext DB = new DBHandlerDataContext();
            DBHelper.DBReturnCode retcode;
            ResvationList = null;
            try
            {
                var query = from objReservation in DB.Trans_Reservations select objReservation;
                ResvationList = query.ToList();
                if (ResvationList.Count > 0)
                {
                    retcode = DBHelper.DBReturnCode.SUCCESS;
                }
                else
                {
                    retcode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                }
            }
            catch
            {
                retcode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retcode;
        }

        public static DBHelper.DBReturnCode GetAllDriver(out List<BWI.Trans_Tbl_Login> List)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            List = null;
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_Login Manufacture = new Trans_Tbl_Login();

                var Query = from Obj in DB.Trans_Tbl_Logins where Obj.sUserType == "Driver" && Obj.sIsActive == true select Obj;

                List = Query.ToList();
                var test = (from Obj in DB.Trans_Tbl_Logins where Obj.sUserType == "Driver" && Obj.sIsActive == true select Obj).ToList();
                return retCode;
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
                return retCode;
            }

        }

        public static DBHelper.DBReturnCode GetReservationList(out DataTable dtResult)
        {
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            DBHandlerDataContext DB = new DBHandlerDataContext();
            dtResult = null;
            var List = (from Obj in DB.Trans_Reservations select Obj).ToList();
            // var NewList = List.Where(x => ConvertDateTime(x.Date) < DateTime.Now).ToList();
            var NewList = List.Where(x => ConvertDateTime(x.ReservationDate) < DateTime.Now).ToList();
            DataTable dtReservationList = ConvertToDatatable(NewList);
            HttpContext.Current.Session["ReservationList"] = dtReservationList;
            if (NewList.Count > 0)
            {
                dtResult = ConvertToDatatable(NewList);
                retCode = DBHelper.DBReturnCode.SUCCESS;
            }
            else
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }
            return retCode;
        }

        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static DateTime ConvertDateTime(string Date)
        {
            DateTime date = new DateTime();
            try
            {
                // string CurrentPattern = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;

                string CurrentPattern = "mm-dd-yyyy";
                string[] Split = new string[] { "-", "/", @"\", "." };
                string[] Patternvalue = CurrentPattern.Split(Split, StringSplitOptions.None);
                string[] DateSplit = Date.Split(Split, StringSplitOptions.None);
                string NewDate = "";
                if (Patternvalue[0].ToLower().Contains("d") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[0] + "/" + DateSplit[1] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("m") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("y") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[0] + "/" + DateSplit[2];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("m") == true && Patternvalue[2].ToLower().Contains("d") == true)
                {
                    NewDate = DateSplit[2] + "/" + DateSplit[1] + "/" + DateSplit[0];
                }
                else if (Patternvalue[0].ToLower().Contains("y") == true && Patternvalue[1].ToLower().Contains("d") == true && Patternvalue[2].ToLower().Contains("m") == true)
                {
                    NewDate = DateSplit[1] + "/" + DateSplit[2] + "/" + DateSplit[0];
                }
                date = DateTime.Parse(NewDate, Thread.CurrentThread.CurrentCulture);
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            return date;
        }

    }
}