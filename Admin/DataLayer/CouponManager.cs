﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BWI.DataLayer;
using BWI.BL;
using System.Configuration;

namespace BWI.Admin.DataLayer
{
    public class CouponManager
    {
        public static DBHelper.DBReturnCode LoadCoupons(out List<BWI.Trans_Tbl_Offer> Data)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            Data = null;

            try
            {
                Data = (from obj in DB.Trans_Tbl_Offers select obj).ToList();
                if (Data.Count > 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retCode;
        }

        public static DBHelper.DBReturnCode GetSingle(Int64 sid, out List<BWI.Trans_Tbl_Offer> Data)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString;

            DBHandlerDataContext DB = new DBHandlerDataContext(ConnectionString);
            DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
            Data = null;

            try
            {
                Data = (from obj in DB.Trans_Tbl_Offers where obj.Sid == sid select obj).ToList();
                if (Data.Count > 0)
                {
                    retCode = DBHelper.DBReturnCode.SUCCESS;
                }
                else
                {
                    retCode = DBHelper.DBReturnCode.SUCCESSNORESULT;
                }
            }
            catch
            {
                retCode = DBHelper.DBReturnCode.EXCEPTION;
            }

            return retCode;
        }
    }
}