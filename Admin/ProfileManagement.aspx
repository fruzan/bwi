﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="ProfileManagement.aspx.cs" Inherits="BWI.Admin.ProfileManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="Scripts/ProfileManagement.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <br />

    <div class="container">
        <div class="row">
            <div class="col-md-12">


                <div class="fblueline">
                    Setting           
                    <span class="farrow"></span>
                    <a style="color: white" href="ProfileManagement.aspx"><b>Profile Management</b></a><br />
                </div>
                <div class="tab-content55">


                    <!-- COL 1 -->
                    <div class="col-md-12 offset-0">
                        <form id="formAdmin" action="#" style="padding-left: 2%; padding-right: 2%">
                            <div class="row">
                                <div class="col-md-4">
                                    <br />
                                    First Name*:
                                    <input type="text" class="form-control"  id="txtFirstName" />
                          
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    Last Name*:
						
                                    <input type="text" class="form-control"  id="txtLastName" />
                     
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    Email*:
                                    <input type="text" id="txtEmail" class="form-control" />
                                </div>

                            </div>
                         
                           
                       
                          
                      

                            <div class="row">
                           
                                <div class="col-md-4">
                                    <br />
                                    Mobile*:
					
                                    <input type="text" id="txtMobileNumber" class="form-control" />
                       
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    Phone:
					
                                    <input type="text" id="txtPhone" class="form-control" />
                             
                                </div>
                                    <div class="col-md-4">
                                    <br />
                                    PAN Number:
					
                                    <input type="text" id="txtPanNumber" class="form-control"/>
                          
                                </div>
                            </div>

                            <br />
                            Address*:
					
                            <input type="text" id="txtAddress" class="form-control" />
             
                            <div class="row">
                                <div class="col-md-4">
                                    <br />
                                    Country*:
						
                                    <select id="selCountry" class="form-control logpadding margtop5" onchange="GetCity(this.value)">
                                        <option value="-" selected="selected">Select Any Country</option>
                                    </select>
                       
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    City*:
						
                                    <select id="selCity" class="form-control logpadding margtop5">
                                        <option selected="selected" value="-">Select Any City</option>
                                    </select>
                               
                                </div>
                                <div class="col-md-4">
                                    <br />

                                    Pin Code:
					
                                    <input type="text" id="txtPinCode" class="form-control" />
                                
                                </div>
                            </div>


                            <br />
                            <table align="right">
                                <tr>
                                    <td>
                                        <input type="button" class="btn-search margtop-2" value="Update" onclick="Update_Click()" />
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <!-- END OF COL 1 -->

                    <div class="clearfix"></div>
                    <br />
                </div>
            </div>
        </div>
    </div>

</asp:Content>
