﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using BWI.BL;
using BWI.DataLayer;
using BWI.Admin.DataLayer;

namespace BWI.Admin
{
    /// <summary>
    /// Summary description for UserHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class UserHandler : System.Web.Services.WebService
    {
        string json = "";
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        [WebMethod(EnableSession = true)]
        public string GetAll()
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                List<BWI.Trans_Tbl_Login> List = new List<BWI.Trans_Tbl_Login>();
                List = (from obj in DB.Trans_Tbl_Logins where obj.sUserType == "Admin" || obj.sUserType == "Dispatcher" select obj).ToList();
                if (List.Count > 0)
                {

                    json = jsSerializer.Serialize(List);
                    json = json.TrimEnd(']');
                    json = json.TrimStart('[');
                    json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Details\":[" + json + "]}";
                    return json;
                }
                else
                {
                    json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
                    return json;
                }
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
                return json;
            }
        }

        [WebMethod(EnableSession = true)]
        public string Add(string FN, string Lanme, string Mobile, string Email, string Password, string UserType, string Active, string Address, string Country, string City, string PinCode)
        {
            retCode = UserManager.Add(FN, Lanme, Mobile, Email, Password, UserType, Active, Address, Country, City, PinCode);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateDetails(Int64 sid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                List<BWI.Trans_Tbl_Login> List = new List<BWI.Trans_Tbl_Login>();
                List = (from obj in DB.Trans_Tbl_Logins where obj.Sid == sid select obj).ToList();
                json = jsSerializer.Serialize(List);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"Details\":[" + json + "]}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }


        [WebMethod(EnableSession = true)]
        public string Update(Int64 sid, string FN, string Lanme, string Mobile, string Email, string Password, string UserType, string Active, string Address, string Country, string City, string PinCode)
        {

            retCode = UserManager.Update(sid, FN, Lanme, Mobile, Email, Password, UserType, Active, Address, Country, City, PinCode);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Delete(Int64 sid)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_Login Delete = DB.Trans_Tbl_Logins.Single(x => x.Sid == sid);
                DB.Trans_Tbl_Logins.DeleteOnSubmit(Delete);
                DB.SubmitChanges();
                json = "{\"Session\":\"1\",\"retCode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }
    }
}
