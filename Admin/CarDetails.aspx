﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="CarDetails.aspx.cs" Inherits="BWI.Admin.CarDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        tbody.scrollContent {
            display: block;
            height: 262px;
            overflow: auto;
            width: 100%;
        }

        div.scrollingDiv {
            width: auto;
            height: auto;
            overflow: scroll;
        }

        div.scrollingDiv2 {
            width: 100%;
            height: 600px;
            overflow-x: scroll;
            padding-right: 20px;
        }

        div.scrollingIndividual {
            width: auto;
            height: 330px;
            overflow-x: scroll;
            padding-right: 20px;
        }

        @media screen and (max-width: 992px) {

            span {
                font-size: 100%;
            }
        }

        @media screen and (max-width: 768px) {
            span {
                font-size: 12px;
            }
        }
    </style>
    <style type="text/css">
        input[type="text"] {
            width: 80%;
            margin-bottom: 5px;
        }
    </style>

    <script type="text/javascript" src="Scripts/CarType.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    

 <br>
    <div class="offset-2">
        <div id="alSuccess" class="alert alert-success alert-dismissible" role="alert" style="display: none">
            <span id="spnMsg">Staff status has been changed successfully!</span>
            <button type="button" class="close" onclick="hidealert()"><span aria-hidden="true">×</span></button>
        </div>
        <div id="alError" class="alert alert-danger alert-dismissible" role="alert" style="display: none">
            Something went wrong while processing your request! Please try again.
                   
            <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="hidealert()"><span aria-hidden="true">×</span></button>
        </div>
    </div>
    <table align="right" style="margin-right: 1.8%">
        <tbody>
            <tr>
                <td colspan="6"></td>

                <td style="padding-top: 3.2%"><a href="#">
                    <input type="button" class="popularbtn right" onclick="AddDilogBox()" style="margin-right: 3.8%" value="+ Add New" title="Add New Vehicle">
                </a></td>
               <%-- <td style="padding-top: 3.2%">
                    <input type="button" class="grid2btn active" style="cursor: pointer" title="Export To Excel" id="btnExport" onclick="ExportAgentDetailsToExcel()">
                </td>--%>
            </tr>
        </tbody>
    </table>
    <br>
    <br>
    <br>
    <div class="offset-2">
        <div class="fblueline">
            Add Master
           
            <span class="farrow"></span>
            <a style="color: white" href="CarDetails.aspx" title="Vehicle Details"><b>Vehicle Type</b></a>

        </div>
        <div class="frow1">

            <br />
            <br />

            <div class="table-responsive">
                <div id="tbl_StaffDetails_wrapper" class="dataTables_wrapper">

                    <table  class="table table-striped table-bordered " id="tbl_StaffDetails" style="width: 100%;">
                        <thead>
                            <tr >
                                <td align="center" style="width: 10%"><b>S.N</b>
                                </td>
                                <td align="center"><b>Unique Code</b>
                                </td>
                                <td align="center"><b>Name</b>
                                </td>
                                <td align="center"><b>Change Status</b>
                                </td>

                                <td align="center" "><b>Edit</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody id="CarDetails">

                        </tbody>
                    </table>

                </div>
            </div>
            <br>
    

        </div>
    </div>
   
    <div class="modal fade" id="AddDilogBox" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Add Vehicle Type</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>Enter Type *</label>
                                            <div style="width:50%">
                                            <input id="txt_Name" type="text" class="form-control" />
                                                </div>
                                            <br/>
                                            <label> Description *</label>
                                            <br/>
                                            <textarea id="txt_Description" class=" form-control" rows="4" cols="60" name="comment"></textarea>
                                            <br/>
                                          <div align="right" >
                                        <input type="button" class="btn-search" value="Save" onclick="Insert();" />
                                        <input type="button" class="btn-search" value="Cancel" onclick="$('#AddDilogBox').modal('hide')"  />
                                        </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <input type="hidden" id="hdn_Sid"/>
    <div class="modal fade" id="UpdateDilogBox" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Add Vehicle</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>Name *</label>
                                            <div style="width:50%">
                                            <input id="Upd_Name" type="text" class="form-control" />
                                                </div>
                                            <br/>
                                            <label> Description *</label>
                                            <br/>
                                            <textarea  id="Upd_Description" class=" form-control" rows="4" cols="60" name="comment"></textarea>
                                            <br/>
                                          <div align="right" >
                                        <input type="button" class="btn-search" value="Update" onclick="Update();"  />
                                        <input type="button" class="btn-search" value="Cancel" onclick="$('#UpdateDilogBox').modal('hide')" />
                                        </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</asp:Content>
