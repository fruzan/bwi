﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.Master" AutoEventWireup="true" CodeBehind="HourReservation.aspx.cs" Inherits="BWI.Admin.HourReservation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyAcadP8HV_jRAWLvwkKxzMT9wHjzYUnxBI"></script>
    <style type="text/css">
        label {
            float: left;
            width: auto;
            padding: 10px 10px 10px 10px;
            text-align: center;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 14px;
            line-height: 1.428571429;
            color: #333333;
            background-color: #ffffff;
            font-weight: 400;
        }
    </style>
    <script type="text/javascript" src="Scripts/HourReservationJS.js?v=1.2"></script>
    <script type="text/javascript">

        $(function () {
            GetAllEmail();
            VehicleNameDropDown();
            var dateToday = new Date();
            //VehicleTypeDropDown()
            GetAllDriver();
            $("#txt_Date").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "mm-dd-yy",
                minDate: dateToday
            });
        })

        var directionsDisplay;
        var directionsService = new google.maps.DirectionsService();

        google.maps.event.addDomListener(window, 'load', function () {
            debugger;

            new google.maps.places.SearchBox(document.getElementById('txt_Address'));

            directionsDisplay = new google.maps.DirectionsRenderer({ 'draggable': true });
            var places = new google.maps.places.Autocomplete(document.getElementById('txt_Address'));
            google.maps.event.addListener(places, 'place_changed', function () {
                debugger;
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();

                dLat = latitude;
                dLong = longitude;
                //GetRoute();
            });
        });

        function GetRoute() {
            debugger;
            var mumbai = new google.maps.LatLng(18.9750, 72.8258);
            var mapOptions = {
                zoom: 7,
                center: mumbai
            };
            map = new google.maps.Map(document.getElementById('dvMap'), mapOptions);
            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById('dvPanel'));

            //*********DIRECTIONS AND ROUTE**********************//
            if (document.getElementById("Select_Service").value == 'FromAirPort') {
                source = $("#Select_Location option:selected").text();
                destination = document.getElementById("txt_Address").value;
            }
            else {
                source = document.getElementById("txt_Address").value;
                destination = $("#Select_Location option:selected").text();
            }

            var request = {
                origin: source,
                destination: destination,
                travelMode: google.maps.TravelMode.DRIVING
            };
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                }
            });

            // *********DISTANCE AND DURATION**********************//
            var service = new google.maps.DistanceMatrixService();
            service.getDistanceMatrix({
                origins: [source],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function (response, status) {
                if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
                    var distance = response.rows[0].elements[0].distance.text;
                    var duration = response.rows[0].elements[0].duration.text;
                    TimeTaken = duration;
                    TotalDistance = distance;
                    TotalDistance = (parseFloat(TotalDistance) * 0.621371).toFixed(2)
                    //alert(distance)
                } else {
                    alert("Unable to find the distance via road.");
                }
            });
        }

    </script>
    <script type="text/javascript" src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
    <script type="text/javascript" src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
    <script type="text/javascript">
        webshims.setOptions('waitReady', false);
        webshims.setOptions('forms-ext', { types: 'date' });
        webshims.polyfill('forms forms-ext');
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="fblueline">
                    Reservation
           
                    <span class="farrow"></span>
                    <a style="color: white" href="HourReservation.aspx" title="Per Hour Reservation">Per Hour Reservation</a>

                    <br />
                </div>

                <div class="tab-content55">
                    <div class="col-md-12">

                       

                        <div class="tab-pane padding40 active" id="profile">

                            <div class="row">
                                   
                                    <div class="col-md-4">
                                    <br />
                                    Email :
                                      <a style="cursor:pointer" data-toggle="modal" data-target="#MasterCustomer"> <span class="glyphicon glyphicon-edit right" title="Add New Customer" aria-hidden="true" style="padding-top: 25px;margin-right: 20px;"></span></a> 
                                   <input id="txt_Email" class="form-control"  style="width:80%" list="Select_Email"/>
                                    <datalist id="Select_Email"></datalist>      
                                   <%-- <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Email" placeholder="Email" class="form-control" />--%>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Email">
                                        <b>* This field is required</b></label>
                                </div>
                                 <div class="col-md-4">
                                    <br />
                                    Contact Number :
                                        <input id="txt_ContactNumber" class="form-control"  list="Select_ContactNumber"/>
                                    <datalist id="Select_ContactNumber"></datalist>
                                    <%--<input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_ContactNumber" placeholder="Contact Number" class="form-control" />--%>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_ContactNumber">
                                        <b>* This field is required</b></label>
                                </div>      
                                    
                                
                                <div class="col-md-4">
                                    <br />
                                    First Name :
                                       
                                    <input type="text" name="Text[]" id="txt_FirstName" placeholder="First Name" class="form-control"/>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_FirstName">
                                        <b>* This field is required</b></label>
                                </div>
                                
                            </div>

                           
                            <div class="row">
                                <div class="col-md-4">
                                    <br />
                                    Last Name :
                                       
                                    <input type="text" name="Text[]" id="txt_LastName" placeholder="Last Name" class="form-control"/>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_LastName">
                                        <b>* This field is required</b></label>
                                </div>
                               <%-- <div class="col-md-4">
                                    <br />
                                    Contact Number :
                                       
                                    <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_ContactNumber" placeholder="Contact Number" class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_ContactNumber">
                                        <b>* This field is required</b></label>
                                </div>--%>
                                <%--<div class="col-md-4">
                                    <br />
                                    Email :
                                       
                                    <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Email" placeholder="Email" class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Email">
                                        <b>* This field is required</b></label>
                                </div>--%>
                                <div class="col-md-4">
                                    <br />
                                    Reservation Date :
                                       <input type="text" name="Text[]" class="form-control datepicker" id="txt_Date" style="cursor: pointer; background: url('../../images/calendar.png') #fff no-repeat right;" placeholder="mm-dd-yyyy">
                                    <%--<input type="date" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Date" placeholder="Reservation Date" class="form-control" />--%>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Date">
                                        <b>* This field is required</b></label>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    Passenger :
                                      
                                    <input type="number" name="Text[]" id="txt_Adult" min="1" max="50" class="form-control"/>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Adult">
                                        <b>* This field is required</b></label>
                                </div>
                            </div>

                            <div class="row">

                               <%-- <div class="col-md-4">
                                    <br />
                                    Organisation Code :
                                       
                                    <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Code" placeholder="Organisation Code" class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Code">
                                        <b>* This field is required</b></label>
                                </div>
                                <div class="col-md-2">
                                    <br />
                                    Adult :
                                       
                                 <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Adult" placeholder="Adult" class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Adult">
                                        <b>* This field is required</b></label>
                                </div>

                                <div class="col-md-2">
                                    <br />
                                    Child :
                                       
                              <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Child" placeholder="Child" class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Country">
                                        <b>* This field is required</b></label>
                                </div>--%>

                               <%-- <div class="col-md-4">
                                    <br />
                                    CC/Type :
                                       
                              <select name="Drop[]" size="1" id="Sel_CC" class="form-control">
                                  <option selected="" value="-">Select CC Type</option>
                                  <option value="American Express">American Express</option>
                                  <option value="Discover Club">Discover Club</option>
                                  <option value="Master Card">Master Card</option>
                                  <option value="Visa">Visa</option>
                              </select>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_CC">
                                        <b>* This field is required</b></label>
                                </div>
                            </div>
                            <br />

                            <div class="row">
                                <div class="col-md-4">
                                    <br />
                                    CC Last 4 :
                                       
                                    <input type="text" id="txt_Last4" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder=" CC Last 4 " class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Last4">
                                        <b>* This field is required</b></label>
                                </div>--%>
                                

                                <div class="col-md-4">
                                    <br />
                                    Vehicle Type :
                                      
                                  <select name="Drop[]" id="ddl_VehicleType" class="form-control">
                                  </select>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Vehical">
                                        <b>* This field is required</b></label>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    Assigned To :
                                       
                                    <select id="txt_Assigned" name="Drop[]" class="form-control">
                                        <option value="0">Select Driver</option>
                                    </select>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Assigned">
                                        <b>* This field is required</b></label>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    Hours :
                                      
                                  <select name="Drop[]" id="Hours" class="form-control">
                                      <option selected="selected" value="1">1</option>
                                      <option value="2">2</option>
                                      <option value="3">3</option>
                                      <option value="4">4</option>
                                      <option value="5">5</option>
                                      <option value="6">6</option>
                                      <option value="7">7</option>
                                      <option value="8">8</option>
                                      <option value="9">9</option>
                                      <option value="10">10</option>
                                      <option value="11">11</option>
                                      <option value="12">12</option>
                                      <option value="13">13</option>
                                      <option value="14">14</option>
                                      <option value="15">15</option>
                                      <option value="16">16</option>
                                      <option value="17">17</option>
                                      <option value="18">18</option>
                                      <option value="19">19</option>
                                      <option value="20">20</option>
                                      <option value="21">21</option>
                                      <option value="22">22</option>
                                      <option value="23">23</option>
                                      <option value="24">24</option>

                                  </select>

                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-4">
                                    <br />
                                    CC Last 4 :
                                       
                                    <input type="text" id="txt_Last4" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder=" CC Last 4 " class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Last4">
                                        <b>* This field is required</b></label>
                                </div>
                                 <div class="col-md-4">
                                    <br />
                                    Flight Type :
                                      
                                    <select id="Sel_TravelType" name="Drop[]" class="form-control">
                                        <option selected="selected" value="Domestic">Domestic</option>
                                        <option value="International">International</option>

                                    </select>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_PinCode">
                                        <b>* This field is required</b></label>
                                </div>
                                 </div>
                            
                            <br />

                            <div class="row">
                                <br />
                                <div align="center">
                                    <center><span id="Spn_Location"><b>PickUp Location</b></span></center>
                                </div>

                                <div class="col-md-9">
                                    <br />
                                    Address:
                                       
                             <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Address" placeholder="Address" class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Address">
                                        <b>* This field is required</b></label>
                                </div>

                                <div class="col-md-3">
                                    <br />
                                    PickUp Time:
                                       
                             <input type="time" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_PickUpTime" placeholder="Pickup Time" class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_PickUpTime">
                                        <b>* This field is required</b></label>
                                </div>
                            </div>

                            <div class="row" style="display:none">



                                
                                <div class="col-md-4">
                                    <br />
                                    Minutes :
                                      
                                  <select name="Drop[]" id="Minutes" class="form-control">

                                      <option selected="selected" value="0">10</option>
                                      <option value="20">20</option>
                                      <option value="30">30</option>
                                      <option value="40">40</option>
                                      <option value="50">50</option>
                                      <option value="60">60</option>
                                  </select>

                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <br /><br />
                                    <center><span><b>Note: We charge $10 extra for late night and early morning pick up Time 11 pm to 5 am</b></span></center>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-4">
                                    <br />

                                    <div style="width: 100%">
                                        <div>
                                            <label>
                                                <input id="Chk_Paid" type="radio" name="money"/><span>&nbsp Paid</span></label>
                                        </div>
                                        <div style="display:none">
                                            <label>
                                                <input id="Chk_Completed" type="checkbox"/><span>&nbsp Completed</span></label>
                                        </div>

                                        <div>
                                            <label>
                                                <input id="Chk_Collect" type="radio" name="money"/><span>&nbsp Collect</span></label>
                                        </div>
                                        <div>
                                            <label>
                                                <input id="Chk_CCPayment" type="checkbox" /><span>&nbsp CC Payment</span></label>
                                        </div>
                                        <br />
                                        <div>
                                            <label>
                                                <input id="Chk_ChildCarSheet" type="checkbox" /><span>&nbsp Child Car Seat</span></label>
                                        </div>
                                        <div>
                                            <label>
                                                <input id="Chk_Meet" type="checkbox" /><span>&nbsp Meet & Greet</span></label>
                                        </div>
                                        <div>
                                            <label>
                                                <input id="Chk_Special" type="checkbox" /><span>&nbsp Special assistant </span>
                                            </label>
                                        </div>
                                        <div>
                                            <label>
                                                <input id="Chk_rub" type="checkbox" /><span>&nbsp Curb side pick up  </span>
                                            </label>
                                        </div>

                                        <div>
                                            <label>
                                                <input id="Chk_PetInCage" type="checkbox" /><span>&nbsp Pet in cage  </span>
                                            </label>
                                        </div>


                                        <br />

                                    </div>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Reuired">
                                        <b>* This field is required</b></label>
                                </div>

                                <div class="col-md-4">
                                    <br />
                                    <table>
                                        <tr>
                                            <td>Fare</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <input type="text" id="txt_Fare" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder="Fare" class="form-control" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Fare">
                                                    <b>* This field is required</b></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gratuity</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <%--<input id="txt_Gratuity" type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder="Gratuity" class="form-control" />--%>
                                                <select class="form-control" name="Drop[]"  id="txt_Gratuity">
                                                    <option value="-" selected="selected">Select Gratuity</option>
                                                    <option value="0">0 %</option>
                                                    <option value="10">10 %</option>
                                                    <option value="15">15 %</option>
                                                    <option value="20">20 %</option>
                                                    <option value="25">25 %</option>
                                                    <option value="30">30 %</option>
                                                </select>
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Gratuity">
                                                    <b>* This field is required</b></label>
                                            </td>

                                        </tr>

                                        <tr>
                                            <td>Parking</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <input type="text" id="txt_Parking" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder="Parking" class="form-control" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Parking">
                                                    <b>* This field is required</b></label>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>Total Fare</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <input id="txt_Total" type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder="Total Fare" class="form-control" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Total">
                                                    <b>* This field is required</b></label>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    <table style="width: 100%">
                                        <tr>
                                            <td>Toll</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <input id="txt_Toll" type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder="Toll" class="form-control" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Toll">
                                                    <b>* This field is required</b></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Offer Code</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <select class="form-control" name="Drop[]" id="ddlOfferCode"></select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>CC Type</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <select class="form-control" name="Drop[]" id="ddlCCType">
                                                    <option value="-">Select CC Type</option>
                                                    <option value="Amex">Amex</option>
                                                    <option value="Discover">Discover</option>
                                                    <option value="Master">Master</option>
                                                    <option value="Visa">Visa</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </div>


                            </div>

                            <%--<div class="row">
                                <div class="col-md-4">
                                    <br />
                                    City :
                                       
                                      <input type="text" id="txt_City" name="Text[]" placeholder=" City " class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_City">
                                        <b>* This field is required</b></label>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    State :
                                       
                                    <input type="text" name="Text[]" id="txt_State" placeholder="State" class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_State">
                                        <b>* This field is required</b></label>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    ZipCode :
                                       
                                    <input type="text" name="Text[]" id="txt_ZipCode" placeholder="ZipCode" class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_ZipCode">
                                        <b>* This field is required</b></label>
                                </div>
                            </div>--%>
                            <br />


                            <div align="center">
                                <center><span><b>If Address Changes, enter information in comment section</b></span></center>
                            </div>
                            <br />

                            <div>Remarks</div>
                            <div align="center">

                                <center>
                                    <textarea id="txt_AreaRemarks" name="Text[]" rows="4" cols="50" name="comment" class="form-control" placeholder="Remarks"></textarea></center>
                            </div>



                        </div>
                        <br />
                        <table align="right">
                            <tr>
                                <td>
                                    <input type="button" value="Add" class="btn-search margtop-2"
                                        style="width: 100%;" onclick="Submit();" title="Submit Details" id="btn_Submit" />
                                </td>
                                <td>
                                    <input type="button" value="Cancel" class="btn-search margtop-2" onclick="window.location.href = 'AdminDashBoard.aspx'"
                                        style="width: 100%;" />
                                </td>
                            </tr>
                        </table>

                        <br />
                    </div>
                    <input type="hidden" id="hdn_Status" value="Pending" />
                    <div id="dvMap" style="display: none"></div>
                    <input type="text" id="txt_Account" name="Text[]" placeholder=" Account Number " class="form-control" style="display:none"/>
                </div>

            </div>
        </div>

    </div>
</asp:Content>
