﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using BWI.BL;
using BWI.DataLayer;
using BWI.Admin.DataLayer;
namespace BWI
{
    /// <summary>
    /// Summary description for GeneralHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class GeneralHandler : System.Web.Services.WebService
    {
        string json = "";
        DBHandlerDataContext DB = new DBHandlerDataContext();

        [WebMethod(EnableSession = true)]
        public string UserLogin(string Username, string Password)
        {
            List<BWI.Trans_Tbl_CustomerMaster> List = new List<Trans_Tbl_CustomerMaster>();

            try
            {
                List = (from Obj in DB.Trans_Tbl_CustomerMasters where Obj.Email == Username && Obj.Password == Password select Obj).ToList();

                Session["OuterUser"] = List;


                if (List.Count > 0)
                {
                    json = "{\"Retcode\":\"1\"}";
                }
                else
                {
                    json = "{\"Retcode\":\"2\"}";
                }
            }
            catch
            {
                json = "{\"Retcode\":\"0\"}";
            }
            return json;
        }



        [WebMethod(EnableSession = true)]
        public string CreateAccount(string[] Input, string AccountType, string Prefix)
        {
            Trans_Tbl_CustomerMaster Customer = new Trans_Tbl_CustomerMaster();

            try
            {
                Customer.FirstName = Input[0];
                Customer.LastName = Input[1];
                Customer.Address = Input[2];
                Customer.City = Input[3];
                Customer.State = Input[4];
                Customer.Pin = Input[5];
                Customer.CompanyName = Input[6];
                Customer.Email = Input[7];
                Customer.Password = Input[8];
                Customer.Mobile = Input[9];
                Customer.Fax = Input[10];
                Customer.AccountType = AccountType;
                Customer.Prefix = Prefix;

                DB.Trans_Tbl_CustomerMasters.InsertOnSubmit(Customer);
                DB.SubmitChanges();
                json = "{\"Retcode\":\"1\"}";


                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                #region Customer Mail
                sb.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
                sb.Append("<title>limoallaround.com - Customer Receipt</title>  ");
                sb.Append("<style>                                            ");
                sb.Append(".Norm {font-family: Verdana;                       ");
                sb.Append("font-size: 12px;                                 ");
                sb.Append("font-color: red;                               ");
                sb.Append(" }                                               ");
                sb.Append(".heading {font-family: Verdana;                   ");
                sb.Append("  font-size: 14px;                            ");
                sb.Append("  font-weight: 800;                           ");
                sb.Append("	 }                                                  ");
                sb.Append("   td {font-family: Verdana;                         ");
                sb.Append("	  font-size: 12px;                                  ");
                sb.Append("	 }                                                  ");
                sb.Append("  </style>                                           ");
                sb.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
                sb.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
                sb.Append("");
                sb.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
                sb.Append("  <tbody><tr>");
                sb.Append("    <td width='660' colspan='13' valign='top'>");
                sb.Append("      <table width='100%'>");
                sb.Append("        <tbody><tr>");
                //sb.Append("          <td colspan='2' width='100%' class='heading'><b>Bwi Shuttle Service</b></td>");
                sb.Append("        </tr>");
                sb.Append("        <tr>");
                sb.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com'>http://www.bwishuttleservice.com</a></td>");
                sb.Append("        </tr>");
                sb.Append("        <tr>");
                sb.Append("          <td colspan='2' width='100%'><a href='mailto:reservation@bwishuttleservice.com'>reservation@bwishuttleservice.com</a></td>");
                sb.Append("        </tr>");
                sb.Append("        <tr>");
                sb.Append("          <td colspan='2' width='25%'>Tel: 410-235-2265</td>");
                sb.Append("          <td width='75%' align='right'><font size='3'>");

                sb.Append("	     </font></td>");
                sb.Append("        </tr>");
                sb.Append("      </tbody></table>");
                sb.Append("     </td>");
                sb.Append("  </tr><tr>");
                sb.Append("  </tr><tr>");
                sb.Append("    <td colspan='13' width='660'>");
                sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
                sb.Append("  	<tbody><tr>");

                sb.Append("  	</tr>");
                sb.Append("  	<tr>");
                sb.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br>Thank you for Registration with BWI Shuttle Service Service.s If any of the information appears to be incorrect, please contact our office immediately to correct it.<br>&nbsp;</td>");
                sb.Append("	</tr>");
                sb.Append("      </tbody></table>");
                sb.Append("	<tr>                                                                                               ");
                sb.Append("	  <td class='heading' valign='top'>Terms &amp; Conditions / Reservation Agreement:</td>            ");
                sb.Append("	  <td>Guest agrees that there will be no smoking in our vehicles.<br>                              ");
                sb.Append("		Guest assures that no illegal drugs are brought into our vehicles.<br>                         ");
                sb.Append("		Guest agrees that no alcoholic beverages shall be brought into our vehicles.                   ");
                sb.Append("		Guest agrees that the passengers capacity of vehicle provided shall not be exceeded.<br>       ");
                sb.Append("		In case of misconduct by your party, chauffeur has the right to terminate this agreement without refunds.<br>");
                sb.Append("		Guest holds BWI Shuttle Service Service harmless and not liable for any personal or material damages arising from the conduct of his/her party.<br>");
                sb.Append("		Guest is responsible for the full payment of any overtime charges, beyond the original agreement.<br>");
                sb.Append("		Guest agrees to 100% payment charged to his/her credit card at the time reservation has been made.<br>");
                sb.Append("		Luxury Sedan guests can cancel 3 hours before pickup time with $10.00 processing fee; if not you will be charged the full amount of reservation. Stretch limousine and Van Guests can cancel 48 hours before pickup time with a $20.00 processing fee; if you cancel after 48 hours, you will be billed for a minimum 3 hours of service charges.<br>");
                sb.Append("		Guest acknowledges that he/she understands that BWI Shuttle Service Service imposes a service fee for late night service (11:00PM TO 5:00AM).<br>");
                sb.Append("		Guest ackowledges that he/she understands that BWI Shuttle Service Service imposes an additional service fee for Incoming International flights.<br>		");
                sb.Append("		BWI Shuttle Service Service cannot be held responsible for mechanical problems, inclement weather, or other uncontrollable circumstances resulting in the inability to start a job as it is schedule time or complete a job. In the event that the requested vehicle can not be provided; BWI Shuttle Service Service may provide a vehicle of equal quality.<br>");
                sb.Append("		BWI Shuttle Service Service reserves the right to change rates without notice.  If a rate change is necessary due to a destination being outside of the immediate service area, technical error or increase in fuel charges; BWI Shuttle Service Service will make every effort to notify the Guest of the change.<br>");
                sb.Append("	</td>                    ");
                sb.Append("	</tr>                    ");
                sb.Append("      </tbody></table>    ");
                sb.Append("  </td></tr>              ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("</tbody></table>          ");
                sb.Append("                          ");
                sb.Append("                          ");
                sb.Append("</body></html>            ");

                #endregion
                try
                {

                    System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                    mailMsg.From = new System.Net.Mail.MailAddress("support@memorEbook.in", "Transfers ");
                    mailMsg.To.Add(Input[7]);
                    mailMsg.Subject = "Registration Completed";
                    mailMsg.IsBodyHtml = true;
                    mailMsg.Body = sb.ToString();
                    System.Net.Mail.SmtpClient mailObj = new System.Net.Mail.SmtpClient("relay-hosting.secureserver.net", 25);
                    mailObj.Credentials = new System.Net.NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                    mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    mailObj.EnableSsl = false;
                    mailMsg.Sender = new System.Net.Mail.MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                    mailObj.Send(mailMsg);
                    mailMsg.Dispose();
                }
                catch
                {
                    json = "{\"Session\":\"1\",\"Retcode\":\"3\"}";
                    return json;
                }


            }
            catch
            {
                json = "{\"Retcode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string ResetPassowrd(string Username)
        {

            List<BWI.Trans_Tbl_CustomerMaster> Data = (from obj in DB.Trans_Tbl_CustomerMasters select obj).ToList();
            bool Exist = Data.Any(x => x.Email == Username);
            if (Exist == false)
            {
                json = "{\"Session\":\"1\",\"Retcode\":\"2\"}";
                return json;
            }

         
            List<string> PassWord = (from obj in DB.Trans_Tbl_CustomerMasters where obj.Email == Username select obj.Password).ToList();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            #region Customer Mail
            sb.Append("<html><head><meta http-equiv='Content-Type' content='text/html; charset=windows-1252'>");
            sb.Append("<title>limoallaround.com - Customer Receipt</title>  ");
            sb.Append("<style>                                            ");
            sb.Append(".Norm {font-family: Verdana;                       ");
            sb.Append("font-size: 12px;                                 ");
            sb.Append("font-color: red;                               ");
            sb.Append(" }                                               ");
            sb.Append(".heading {font-family: Verdana;                   ");
            sb.Append("  font-size: 14px;                            ");
            sb.Append("  font-weight: 800;                           ");
            sb.Append("	 }                                                  ");
            sb.Append("   td {font-family: Verdana;                         ");
            sb.Append("	  font-size: 12px;                                  ");
            sb.Append("	 }                                                  ");
            sb.Append("  </style>                                           ");
            sb.Append("<style>				.askemmy {					background: #fff url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/logo_housefly_new.png) no-repeat right 5px bottom 5px;					background-size: 45px;				}				.askemmy {				    z-index: 10000;				    position: fixed;				    display: block;				    width: 350px;				    height: 145px;				    background-color: white;				    border-radius: 2px;				    box-shadow: rgb(133, 133, 133) 0px 0px 25px 1px;				    margin: 0 auto;				    text-align: center;				    margin-left: 35%;				    margin-top: 10%;				}				.askemmy p#msg {				    font-size: 1.1em;				    font-weight: 600;				    margin-top: 31px;				    margin-bottom: 20px;				}				.askemmy .error-msg {					color: #FF5600;					padding-top: 10px;				}				.askemmy .success-msg {					color: green;					padding-top: 10px;				}				.askemmy input {				    padding: .5em .6em;				    display: inline-block;				    border: 1px solid #ccc;				    box-shadow: inset 0 1px 3px #ddd;				    border-radius: 4px;				    vertical-align: middle;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				    line-height: normal;				    -webkit-appearance: textfield;				    cursor: auto;				 }				 .askemmy input[type='submit'] {				    font-family: inherit;				    font-size: 100%;				    padding: .5em 1em;				    color: white;				    font-weight: 600;				    border: 1px solid #999;				    border: 0 rgba(0,0,0,0);				    background-color: rgba(31, 196, 255, .8);				    text-decoration: none;				    border-radius: 2px;				    display: inline-block;				    zoom: 1;				    line-height: normal;				    white-space: nowrap;				    vertical-align: middle;				    text-align: center;				    cursor: pointer;				    -webkit-user-drag: none;				    -webkit-user-select: none;				    user-select: none;				    -webkit-box-sizing: border-box;				    box-sizing: border-box;				 }				.askemmy .closebox {				    display: inline-block;				    height: 16px;				    width: 16px;				    position: absolute;				    right: 4px;				    top: 4px;				    cursor: pointer;				    background: url(chrome-extension://gllmlkidgbagkcikijiljllpdloelocn/close_box.png)				}				</style></head>");
            sb.Append("<body marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' rightmargin='0' bottommargin='0'>");
            sb.Append("");
            sb.Append("<br><table border='0' width='700' cellspacing='0' cellpadding='0' align='Center'>");
            sb.Append("  <tbody><tr>");
            sb.Append("    <td width='660' colspan='13' valign='top'>");
            sb.Append("      <table width='100%'>");
            sb.Append("        <tbody><tr>");
            //sb.Append("          <td colspan='2' width='100%' class='heading'><b>Bwi Shuttle Service</b></td>");
            sb.Append("        </tr>");
            sb.Append("        <tr>");
            sb.Append("          <td colspan='2' width='100%'><a href='http://www.bwishuttleservice.com'>http://www.bwishuttleservice.com</a></td>");
            sb.Append("        </tr>");
            sb.Append("        <tr>");
            sb.Append("          <td colspan='2' width='100%'><a href='mailto:reservation@bwishuttleservice.com'>reservation@bwishuttleservice.com</a></td>");
            sb.Append("        </tr>");
            sb.Append("        <tr>");
            sb.Append("          <td colspan='2' width='25%'>Tel: 410-235-2265</td>");
            sb.Append("          <td width='75%' align='right'><font size='3'>");

            sb.Append("	     </font></td>");
            sb.Append("        </tr>");
            sb.Append("      </tbody></table>");
            sb.Append("     </td>");
            sb.Append("  </tr><tr>");
            sb.Append("  </tr><tr>");
            sb.Append("    <td colspan='13' width='660'>");
            sb.Append("      <table width='100%' cellspacing='0' cellpadding='0'>");
            sb.Append("  	<tbody><tr>");

            sb.Append("  	</tr>");
            sb.Append("  	<tr>");
            sb.Append("    	  <td colspan='13' style='border-bottom:thin solid green;'><br>Your Password is " + PassWord [0]+ ". If any of the information appears to be incorrect, please contact our office immediately to correct it.<br>&nbsp;</td>");
            sb.Append("	</tr>");
            sb.Append("      </tbody></table>");
            sb.Append("	<tr>                                                                                               ");
            sb.Append("	  <td class='heading' valign='top'>Terms &amp; Conditions / Reservation Agreement:</td>            ");
            sb.Append("	  <td>Guest agrees that there will be no smoking in our vehicles.<br>                              ");
            sb.Append("		Guest assures that no illegal drugs are brought into our vehicles.<br>                         ");
            sb.Append("		Guest agrees that no alcoholic beverages shall be brought into our vehicles.                   ");
            sb.Append("		Guest agrees that the passengers capacity of vehicle provided shall not be exceeded.<br>       ");
            sb.Append("		In case of misconduct by your party, chauffeur has the right to terminate this agreement without refunds.<br>");
            sb.Append("		Guest holds BWI Shuttle Service Service harmless and not liable for any personal or material damages arising from the conduct of his/her party.<br>");
            sb.Append("		Guest is responsible for the full payment of any overtime charges, beyond the original agreement.<br>");
            sb.Append("		Guest agrees to 100% payment charged to his/her credit card at the time reservation has been made.<br>");
            sb.Append("		Luxury Sedan guests can cancel 3 hours before pickup time with $10.00 processing fee; if not you will be charged the full amount of reservation. Stretch limousine and Van Guests can cancel 48 hours before pickup time with a $20.00 processing fee; if you cancel after 48 hours, you will be billed for a minimum 3 hours of service charges.<br>");
            sb.Append("		Guest acknowledges that he/she understands that BWI Shuttle Service Service imposes a service fee for late night service (11:00PM TO 5:00AM).<br>");
            sb.Append("		Guest ackowledges that he/she understands that BWI Shuttle Service Service imposes an additional service fee for Incoming International flights.<br>		");
            sb.Append("		BWI Shuttle Service Service cannot be held responsible for mechanical problems, inclement weather, or other uncontrollable circumstances resulting in the inability to start a job as it is schedule time or complete a job. In the event that the requested vehicle can not be provided; BWI Shuttle Service Service may provide a vehicle of equal quality.<br>");
            sb.Append("		BWI Shuttle Service Service reserves the right to change rates without notice.  If a rate change is necessary due to a destination being outside of the immediate service area, technical error or increase in fuel charges; BWI Shuttle Service Service will make every effort to notify the Guest of the change.<br>");
            sb.Append("	</td>                    ");
            sb.Append("	</tr>                    ");
            sb.Append("      </tbody></table>    ");
            sb.Append("  </td></tr>              ");
            sb.Append("                          ");
            sb.Append("                          ");
            sb.Append("                          ");
            sb.Append("                          ");
            sb.Append("                          ");
            sb.Append("</tbody></table>          ");
            sb.Append("                          ");
            sb.Append("                          ");
            sb.Append("</body></html>            ");

            #endregion

            try
            {
                System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage();
                mailMsg.From = new System.Net.Mail.MailAddress("support@memorEbook.in", "Transfers");
                mailMsg.To.Add(Username);
                mailMsg.Subject = "Reset Password";
                mailMsg.IsBodyHtml = true;
                mailMsg.Body = sb.ToString();
                System.Net.Mail.SmtpClient mailObj = new System.Net.Mail.SmtpClient("relay-hosting.secureserver.net", 25);
                mailObj.Credentials = new System.Net.NetworkCredential("reservation@bwishuttleservice.com", "aBC$32#2SDI_&%12");
                mailObj.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                mailObj.EnableSsl = false;
                mailMsg.Sender = new System.Net.Mail.MailAddress("reservation@bwishuttleservice.com", "www.bwishuttleservice.com");
                mailObj.Send(mailMsg);
                mailMsg.Dispose();
                json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"1\",\"Retcode\":\"0\"}";
                return json;
            }
            return json;
        }

    }
}
