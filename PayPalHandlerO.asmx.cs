﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections.Specialized;
using System.Configuration;
using PayPal.Payments.Common.Utility;
using PayPal.Payments.Communication;
using PayPal.Payments.DataObjects;

namespace BWI
{
    /// <summary>
    /// Summary description for PayPalHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PayPalHandler : System.Web.Services.WebService
    {
        string json = "";
        [WebMethod(EnableSession = true)]
        public string Paypal(string CardNumber, string Month, string Year, string Security_Code, string Amount)
        {


            string PayPalRequest = "TRXTYPE=S" //S - sale transaction
          + "&TENDER=C" //C - Credit card
          + "&ACCT=" + CardNumber //card number
          + "&EXPDATE=" + Month + Year
          + "&CVV2=" + Security_Code   //card validation value (card security code)               
          //+ "&AMT=" + Amount
            + "&AMT=" + 1
          + "&COMMENT1=My Product Sale"
          + "&USER=" + ConfigurationManager.AppSettings["USER"]
          + "&VENDOR=" + ConfigurationManager.AppSettings["VENDOR"]
          + "&PARTNER=" + ConfigurationManager.AppSettings["PARTNER"]
          + "&PWD=" + ConfigurationManager.AppSettings["PWD"];


            // Create an instantce of PayflowNETAPI.
            PayflowNETAPI PayflowNETAPI = new PayflowNETAPI();

            // RequestId is a unique string that is required for each & every transaction. 
            // The merchant can use her/his own algorithm to generate this unique request id or 
            // use the SDK provided API to generate this as shown below (PayflowUtility.RequestId).
            string PayPalResponse = PayflowNETAPI.SubmitTransaction(PayPalRequest, PayflowUtility.RequestId);

            //place data from PayPal into a namevaluecollection
            NameValueCollection RequestCollection = GetPayPalCollection(PayflowNETAPI.TransactionRequest);
            NameValueCollection ResponseCollection = GetPayPalCollection(PayPalResponse);
           PayPalResponse= PayflowUtility.GetStatus(PayPalResponse);
            return PayPalResponse;
        }


        private NameValueCollection GetPayPalCollection(string payPalInfo)
        {
            //place the responses into collection
            NameValueCollection PayPalCollection = new System.Collections.Specialized.NameValueCollection();
            string[] ArrayReponses = payPalInfo.Split('&');

            for (int i = 0; i < ArrayReponses.Length; i++)
            {
                string[] Temp = ArrayReponses[i].Split('=');
                PayPalCollection.Add(Temp[0], Temp[1]);
            }
            return PayPalCollection;
        }
        private string ShowPayPalInfo(NameValueCollection collection)
        {
            string PayPalInfo = "";
            foreach (string key in collection.AllKeys)
            {
                PayPalInfo += "<br /><span class=\"bold-text\">" + key + ":</span> " + collection[key];
            }
            return PayPalInfo;
        }
    }
}
