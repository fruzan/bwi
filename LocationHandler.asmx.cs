﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Net;
using System.Text;
using System.Data;
using System.Xml;
using BWI.DataLayer;
using BWI.BL;
using System.Web.Script.Serialization;



namespace BWI
{
    /// <summary>
    /// Summary description for LocationHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class LocationHandler : System.Web.Services.WebService
    {


        [WebMethod(EnableSession = true)]
        public string Post(string address)
        {
            string Request = "";
            string response = "";
            int status = 0;

            Request = "http://maps.google.com/maps/api/geocode/xml?address=" + address + "&sensor=false";
            response = "";
            status = 0;
            try
            {

                var oRequest = WebRequest.Create(Request);

                oRequest.Method = "GET";
                using (var oResponse = oRequest.GetResponse())
                {
                    using (var oResponseStream = oResponse.GetResponseStream())
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(oResponseStream);
                        response = sr.ReadToEnd();

                        XmlDocument objDocument = new XmlDocument();

                        byte[] buffer = System.Text.Encoding.UTF8.GetBytes(response);

                        MemoryStream ms = new MemoryStream(buffer);
                        using (XmlTextReader reader = new XmlTextReader(ms))
                        {
                            objDocument.Load(reader);
                        }

                        XmlNode OK = objDocument.SelectSingleNode("/GeocodeResponse/status");

                        XmlNodeList parentNode1 = objDocument.SelectNodes("/GeocodeResponse/result/geometry/location");

                        string OkTest = "";
                        string obj = "";

                        OkTest = OK.InnerXml;

                        if (OkTest == "OK")
                        {
                            foreach (XmlNode node in parentNode1)
                            {
                                obj = node.InnerXml;
                                break;
                            }
                            obj = obj.Replace("</lat>", ",");
                            obj = obj.Replace("<lat>", "");
                            obj = obj.Replace("<lng>", "");
                            obj = obj.Replace("</lng>", "");
                            return "{\"Session\":\"1\",\"retCode\":\"1\",\"OutPut\":\"" + obj + "\"}";
                        }
                        else
                        {
                            return "{\"Session\":\"1\",\"retCode\":\"-1\",\"OutPut\":\"" + OkTest + "\"}";
                        }

                        
                    }
                }

            }
            catch (WebException oWebException)
            {
                if (oWebException.Response != null)
                {
                    using (var oResponseStream = oWebException.Response.GetResponseStream())
                    {
                        System.IO.StreamReader sr = new System.IO.StreamReader(oResponseStream);
                        response = sr.ReadToEnd();
                        status = 0;
                    }
                }
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
            catch (Exception oException)
            {
                response = oException.Message;
                status = 0;
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }
        }


    }
}

