﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class ControlPanel : System.Web.UI.Page
{
    controlPanel objControlPanel = new controlPanel();
    protected void Page_Load(object sender, EventArgs e)
    {
 
if (Request.Cookies["sessionInfo"] != null)
        {

            var value = Request.Cookies["sessionInfo"].Value;
      
        }
        else
        {
            Response.Redirect("/admin/siteadmin/default.asp");
        }

        if (!Page.IsPostBack)
        {
          
  
		if (DateTime.Now > new DateTime(2018, 03, 20))
        {
            Response.Redirect("/admin/siteadmin/default.asp");
            return;
        }

            string DailyReservation = objControlPanel.getDailyReservationCounts();
            string DailyIncome = objControlPanel.getDailyIncomeFromReservation();
            string NextDayReservationCount = objControlPanel.getNextDayReservationCounts();
            string NextDayIncome = objControlPanel.getNextDatIncomeFromReservation();


             boxvalue.InnerText = DailyReservation;
             box2value.InnerText=DailyIncome;

             box3vaue.InnerText = NextDayReservationCount;
             box4vaue.InnerText=NextDayIncome;
             getlast10DaysReservations();
             getlast10DaysService();


        }

    }

    public string getTwoLineChartData()
    {
        return objControlPanel.getMonthlySalesgraph();
    }

    public string getDailySalesgraph()
    {
        return objControlPanel.getDailySalesgraph();
    }

    public string getpiechart()
    {
       
        return objControlPanel.getpiechart();
 
    }

    public void getlast10DaysReservations()
    {
        string html = "";
        html = objControlPanel.getlast10DaysReservations();
        lasttenreservation.InnerHtml = html;
    }


    public void getlast10DaysService()
    {
        string html = "";
        html = objControlPanel.getlast10DaysService();
        lasttendaysservice.InnerHtml = html;
    }




    protected void link_signup_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            Response.Redirect("sign_up.aspx?id=" + Request.QueryString["id"].ToString());
        }
        else
        {
            Response.Redirect("sign_up.aspx");
        }
    }

 [WebMethod()]
    public static string Loadcities()
    {
        controlPanel objControlPanel = new controlPanel();
        return objControlPanel.getCities();
    }
    
}