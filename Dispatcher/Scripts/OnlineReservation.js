﻿function GetAll() {
    $("#Details").dataTable().fnClearTable();
    $("#Details").dataTable().fnDestroy();
    $.ajax({
        type: "POST",
        url: "../Admin/OnlineReservationHandler.asmx/LoadAllOnlineReservation",
        data: '{}',
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                var Arr = obj.Arr
                var ul = '';
                //$("#Details").empty();
                for (var i = 0; i < Arr.length; i++) {

                    ul += '<tr>';
                    ul += '<td align="center" >' + (i + 1) + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationID + '</td>';
                    ul += '<td align="center">' + Arr[i].AssignedTo + '</td>';
                    ul += '<td align="center">' + Arr[i].FirstName + ' ' + Arr[i].LastName + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationDate + '</td>';
                    ul += '<td align="center">' + Arr[i].Service + '</td>';
                    ul += '<td align="center">' + Arr[i].Source + '</td>';
                    ul += '<td align="center">' + Arr[i].Destination + '</td>';
                    ul += '<td align="center">' + Arr[i].TotalFare + '</td>';
                    ul += '<td align="center"><a style="cursor: pointer" onclick="Update(' + Arr[i].sid + ',\'' + Arr[i].Service + '\')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a></td>';
                    ul += '</tr>';
                    ul += '</tr>';

                }
                $("#Details tbody").append(ul);
                $("#Details").dataTable({
                    "bSort": false
                });
            }
            else {
                $('#SpnMessege').text("No Record Found.")
                $('#ModelMessege').modal('show')

            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });
}

function Update(sid, Service) {
    debugger
    if (Service == "FromAirPort" || Service == "ToAirPort") {

        window.location.href = "AirPortReservation.aspx?sid=" + sid
    }
    else if (Service == "Point To Point Reservation") {

        window.location.href = "PointToPoint.aspx?sid=" + sid
    }
    else {
        window.location.href = "HourReservation.aspx?sid=" + sid
    }
}