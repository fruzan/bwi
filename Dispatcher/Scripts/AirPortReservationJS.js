﻿var dLat;
var dLong;
var TimeTaken;
var TotalDistance;
var source;
var destination;
var AirportName;
var Paid = false;
var Completed = false;
var Collect = false;
var CCPayment = false;
var ChildCarSheet = false;

var MeetGreat = false;
var SpecialAssistant = false;
var CurbSidePickUp = false;
var BaggageClaimPickup = false;
var PetInCage = false;

var Chk_PetInCage = false;

var Chk_rub = false;

var Chk_Baggageclaim = false;
var Email;
var bValid = true;
var arrService = new Array();
var sum = 0;
var ChkSum = 0;
var sid;
var Arr
var TotalDistance
var TimeTaken
var CustomerList;
var SpecialService = 0;
var Gratuity = 0;
var Parking = 0;
var Toll = 0;
var Remark = '';
var ContactNo;
var LateNight = 0;
var GratuityAmount = 0;
$(function () {
    //debugger;

    if (location.href.indexOf('?') != -1) {
        sid = GetQueryStringParams('sid');
        $('#btn_RegiterAgent').val('Update')
        var data = { BookingSid: sid }
        $.ajax({
            type: "POST",
            url: "../Admin/ReservationHandler.asmx/UpdateData",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d)
                if (obj.retCode == 1) {
                    Arr = obj.Arr;
                    TotalDistance = Arr[0].ApproxDistance
                    TimeTaken = Arr[0].ApproxTime
                    sum = Arr[0].TotalFare

                    if (Arr[0].Paid) {
                        $("#Chk_Paid").prop("checked", true);
                        Paid = true;
                        Collect = false;
                    }
                    else {
                        $("#Chk_Collect").prop("checked", true);
                        Collect = true;
                        Paid = false;
                    }
                    if (Arr[0].Completed) {
                        $("#Chk_Completed").prop("checked", true);
                        Completed = true;
                    }

                    //if (Arr[0].Collect) {
                    //    $("#Chk_Collect").prop("checked", true);
                    //    Collect = true;
                    //}
                    if (Arr[0].CCPayment) {
                        $("#Chk_CCPayment").prop("checked", true);
                        CCPayment = true;
                    }
                    if (Arr[0].ChildCarSheet) {
                        $("#Chk_ChildCarSheet").prop("checked", true);
                        ChildCarSheet = true;
                    }

                    if (Arr[0].MeetGreat) {
                        $("#Chk_Meet").prop("checked", true);
                        MeetGreat = true;
                    }

                    if (Arr[0].SpecialAssistant) {
                        $("#Chk_Special").prop("checked", true);
                        SpecialAssistant = true;
                    }
                    if (Arr[0].CurbSidePickUp) {
                        $("#Chk_rub").prop("checked", true);
                        CurbSidePickUp = true;
                        Chk_rub = true
                    }
                    if (Arr[0].BaggageClaimPickup) {
                        $("#Chk_Baggageclaim").prop("checked", true);
                        BaggageClaimPickup = true;
                        Chk_Baggageclaim = true
                    }
                    if (Arr[0].PetInCage) {
                        $("#Chk_PetInCage").prop("checked", true);
                        PetInCage = true;
                        Chk_PetInCage = true;
                    }

                    $('#txt_Account').val(Arr[0].AccountNumber)
                    $('#txt_Account').attr('readonly', true);
                    $('#txt_Account').addClass('input-disabled');
                    //$('#ReservationDate').val(Arr[0].ReservationDate)
                    $('#txt_Date').val(Arr[0].ReservationDate);
                    $('#txt_Code').val(Arr[0].OrganisationCode);
                    $('#Sel_TravelType').val(Arr[0].TravelType);
                    $('#Select_Airline').val(Arr[0].Airlines);
                    //$('#txt_Time').val(Arr[0].FlightTime);
                    //$('#txt_FlightDate').val(Arr[0].FlightDate);
                    $('#Select_Service').val(Arr[0].Service);
                    //$('#Select_Location').val(Arr[0].AirPortName);

                    //$('#txt_SpecialService').val(Arr[0].Tax);
                    //SpecialService = Arr[0].Tax;
                    $('#ddl_VehicleType').val(Arr[0].VehicalType);
                    //$('#txt_Assigned').val(Arr[0].AssignedTo);

                    setTimeout(function () {
                        $("#txt_Assigned option").filter(function () {
                            return $(this).val() == Arr[0].DriverSid;
                        }).prop("selected", true);
                    }, 1500);

                    //$('#Sel_CC').val(Arr[0].CCType);
                    //setTimeout(function () {
                    //    $("#Sel_CC option").filter(function () {
                    //        return $(this).val() == Arr[0].CCType;
                    //    }).prop("selected", true);
                    //}, 1000);
                    setTimeout(function () {
                        $("#Select_Location option").filter(function () {
                            return $(this).text() == Arr[0].AirPortName;
                        }).prop("selected", true);
                    }, 1000);

                    setTimeout(function () {
                        $("#ddl_VehicleType option").filter(function () {
                            return $(this).val() == Arr[0].VehicalType;
                        }).prop("selected", true);
                    }, 2000);

                    //if (Type == "FromAirPort") {
                    //    $('#Spn_Location').html('<b>Drop Location</b>')
                    //}
                    //else {
                    //    $('#Spn_Location').html('<b>PickUp Location</b>')
                    //}

                    if (Arr[0].Service == 'From Airport') {
                        $('#Spn_Location').html('<b>Drop Location</b>')
                        $('#txt_Address').val(Arr[0].Destination);
                        $("#Time").text("Flight Arrival Time :");
                        //var asda = Arr[0].FlightTime;
                        //alert(asda);
                        $('#txt_FlightTime').val(Arr[0].FlightTime);
                    }
                    else {
                        $('#Spn_Location').html('<b>PickUp Location</b>')
                        $('#txt_Address').val(Arr[0].Source);
                        $("#Time").text("Pickup Time :");
                        var a = Arr[0].Pickup_Time;
                        //alert(a);
                        $('#txt_FlightTime').val(a);
                    }

                    if (Arr[0].GratuityAmount != null) {
                        GratuityAmount = Arr[0].GratuityAmount;
                    }
                    //alert(GratuityAmount);
                    $('#txt_AreaRemarks').val(Arr[0].Remark);

                    $('#txt_Email').val(Arr[0].Email)
                    $('#txt_FirstName').val(Arr[0].FirstName)
                    $('#txt_LastName').val(Arr[0].LastName)
                    $('#txt_ContactNumber').val(Arr[0].ContactNumber)
                    //$('#txt_Child').val(Arr[0].Child)
                    $('#txt_Adult').val(Arr[0].Persons)
                    $('#txt_FlightNumber').val(Arr[0].FlightNumber)
                    $('#txt_Last4').val(Arr[0].CCLast4)
                    $('#txt_Fare').val(Arr[0].Fare)
                    $('#txt_Gratuity').val(Arr[0].Gratuity)
                    Gratuity = Arr[0].Gratuity;
                    $('#txt_Parking').val(Arr[0].Parking)
                    Parking = Arr[0].Parking;
                    $('#txt_Total').val((Arr[0].TotalFare).toFixed(2))
                    $('#txt_Toll').val(Arr[0].Toll)
                    Toll = Arr[0].Toll;
                    $('#txt_LateNight').val(Arr[0].late_nite)
                    LateNight = Arr[0].late_nite;
                    //$('#txt_FlightTime').val(Arr[0].FlightTime)
                    $('#hdn_Status').val(Arr[0].Status)
                    // $('#txt_Address').val(Arr[0].AccountNumber)

                    TimeTaken = Arr[0].ApproxTime
                    TotalDistance = Arr[0].ApproxDistance
                    source = Arr[0].Source
                    destination = Arr[0].Destination

                }
                else {
                    $('#SpnMessege').text("Reservation unsuccessfull.")
                    $('#ModelMessege').modal('show')

                }
            },
            error: function () {

                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            },
        });
    }

    else {
        $("#Chk_Paid").prop("checked", true);
        random = Math.floor((Math.random() * (99999999 - 10000000 + 1)) + 10000000);
        $('#txt_Account').val("ARC-" + random);
        $('#txt_Account').attr('readonly', true);
        $('#txt_Account').addClass('input-disabled');
    }

    $("#Chk_Paid").change(function () {
        debugger;

        Paid = ($(this).is(":checked"));
        if (Paid) {
            Paid = true;
            Collect = false;
            CheckRemark("Collect Fare")
        }
        else {
            Paid = false;
            Collect = true;
        }
    });

    //$("#Chk_Completed").change(function () {
    //    debugger;
    //    Completed = ($(this).is(":checked"));
    //});

    $("#Chk_Collect").change(function () {
        debugger;
        Collect = ($(this).is(":checked"));

        if (Collect) {
            //Remark=
            SetRemark("Collect Fare");
            Paid = false;
            Collect = true;
        }
        else {
            CheckRemark("Collect Fare");
            Paid = true;
            Collect = false;
        }

    });

    $("#Chk_CCPayment").change(function () {
        //debugger;
        CCPayment = ($(this).is(":checked"));
        if (CCPayment) {
            CCPayment = true;
            //$('#txt_AreaRemarks').text('CC Payment');
        }
        else {
            CCPayment = false;
            //$('#txt_AreaRemarks').text('');
        }
    });

    $("#Chk_Meet").change(function () {
        //debugger;
        MeetGreat = ($(this).is(":checked"));

        if (MeetGreat) {
            SetRemark("Meet & Greet");
            sum = parseFloat(sum) + 10;
            sum = sum.toFixed(2)
            $("#txt_Total").val(sum);
        }
        else {
            CheckRemark("Meet & Greet");
            sum = parseFloat(sum) - 10;
            sum = sum.toFixed(2)
            $("#txt_Total").val(sum);
        }
    });

    $("#Chk_Special").change(function () {
        //debugger;
        SpecialAssistant = ($(this).is(":checked"));
        if (SpecialAssistant) {
            SetRemark("Special Assistant");
        }
        else {
            CheckRemark("Special Assistant");
        }
    });

    $("#ServicePopUp").change(function () {
        //debugger;
        var a;
        var Type = $("#ServicePopUp").val();
        if (Type == "FromAirPort") {
            $('#New_Pop_Up_Location').html('Drop Location')
            a = "From Airport";
            $("#Select_Service option").each(function () {

                if ($(this).html() == a) {
                    $(this).attr("selected", "selected");
                    return;
                }
            });
            //alert($('#Select_Service option:selected').val())

        }
        else {
            a = "To Airport";
            $("#Select_Service").focus();
            $('#New_Pop_Up_Location').html('PickUp Location');
            $("#Select_Service option").each(function () {
                //alert($(this).html())
                if ($(this).html() == a) {
                    $(this).attr("selected", "selected");
                    return;
                }
            });

        }
    });

    $("#Select_Service").change(function () {
        //debugger;
        $("#Time").text('');
        var Type = $("#Select_Service").val();
        if (Type == "From Airport") {
            $('#Spn_Location').html('<b>Drop Location</b>')
            $("#Time").text("Flight Arrival Time :");
        }
        else {
            $('#Spn_Location').html('<b>PickUp Location</b>')
            $("#Time").text("Pickup Time :");
        }
    });

    $("#txt_Fare").blur(function () {
        //debugger
        sum = 0
        var Num = /^[0-9]+$/;
        var FareVal = $("#txt_Fare").val();
        Gratuity = $("#txt_Gratuity").val();
        Parking = $("#txt_Parking").val();
        Toll = $("#txt_Toll").val();

        if (FareVal != "") {
            sum = parseFloat(sum) + parseFloat(FareVal)
        }

        if (Parking != "") {
            sum = parseFloat(sum) + parseFloat(Parking)
        }
        if (Toll != "") {
            sum = parseFloat(sum) + parseFloat(Toll)
        }
        if (ChildCarSheet == true) {
            sum = parseFloat(sum) + 10
        }

        if (Chk_rub == true) {
            sum = parseFloat(sum) + 10
        }
        //if (Chk_Baggageclaim == true) {
        //    sum = parseFloat(sum) + 10
        //}
        if (Chk_PetInCage == true) {
            sum = parseFloat(sum) + 10
        }
        if (Gratuity != "") {
            var Gra = parseFloat(sum) * parseFloat(Gratuity) / 100;
            GratuityAmount = Gra;
            sum = parseFloat(sum) + parseFloat(Gra);
        }
        if (LateNight == 10) {
            sum = parseFloat(sum) + 10;
        }
        $("#txt_Total").val(sum.toFixed(2));
    });

    $("#txt_Gratuity").change(function () {
        debugger
        if ($("#txt_Total").val() != "") {
            sum = 0
            var Num = /^[0-9]+$/;
            var FareVal = $("#txt_Fare").val();
            Gratuity = $("#txt_Gratuity option:selected").val();
            Parking = $("#txt_Parking").val();
            Toll = $("#txt_Toll").val();

            if (FareVal != "") {
                sum = parseFloat(sum) + parseFloat(FareVal)
            }


            if (Parking != "") {
                sum = parseFloat(sum) + parseFloat(Parking)
            }
            if (Toll != "") {
                sum = parseFloat(sum) + parseFloat(Toll)
            }


            if (ChildCarSheet == true) {
                sum = parseFloat(sum) + 10
            }

            if (Chk_rub == true) {
                sum = parseFloat(sum) + 10
            }
            if (MeetGreat) {
                sum = parseFloat(sum) + 10
            }
            //if (Chk_Baggageclaim == true) {
            //    sum = parseFloat(sum) + 10
            //}
            if (Chk_PetInCage == true) {
                sum = parseFloat(sum) + 10
            }
            if (Gratuity != "") {
                var Gra = parseFloat(sum) * parseFloat(Gratuity) / 100;
                GratuityAmount = Gra;
                sum = parseFloat(sum) + parseFloat(Gra);
                //sum = sum.toFixed(2);
                //sum = parseFloat(sum) * parseFloat(Gratuity) / 100;
            }
            if (LateNight == 10) {
                sum = parseFloat(sum) + 10;
            }
            $("#txt_Total").val(sum.toFixed(2));
        }
        else {
            $("#txt_Gratuity").val('-');
            alert("Gratuity will be set on Total Amount");
            return false;
        }

    });

    $("#txt_Parking").blur(function () {
        debugger
        sum = 0
        var Num = /^[0-9]+$/;
        var FareVal = $("#txt_Fare").val();
        Gratuity = $("#txt_Gratuity").val();
        Parking = $("#txt_Parking").val();
        Toll = $("#txt_Toll").val();

        if (FareVal != "") {
            sum = parseFloat(sum) + parseFloat(FareVal)
        }
        if (Parking != "") {
            sum = parseFloat(sum) + parseFloat(Parking)
        }
        if (Toll != "") {
            sum = parseFloat(sum) + parseFloat(Toll)
        }
        if (ChildCarSheet == true) {
            sum = parseFloat(sum) + 10
        }

        if (Chk_rub == true) {
            sum = parseFloat(sum) + 10
        }
        if (MeetGreat) {
            sum = parseFloat(sum) + 10
        }
        //if (Chk_Baggageclaim == true) {
        //    sum = parseFloat(sum) + 10
        //}
        if (Chk_PetInCage == true) {
            sum = parseFloat(sum) + 10
        }
        if (Gratuity != "-") {
            var GraAmount = parseFloat(sum) * parseFloat(Gratuity) / 100
            GratuityAmount = GraAmount;
            sum = parseFloat(sum) + parseFloat(GraAmount)
        }
        if (LateNight == 10) {
            sum = parseFloat(sum) + 10;
        }
        $("#txt_Total").val(sum.toFixed(2));
    });

    $("#txt_Toll").blur(function () {
        debugger
        sum = 0
        var Num = /^[0-9]+$/;
        var FareVal = $("#txt_Fare").val();
        Gratuity = $("#txt_Gratuity").val();
        Parking = $("#txt_Parking").val();
        Toll = $("#txt_Toll").val();

        if (FareVal != "") {
            sum = parseFloat(sum) + parseFloat(FareVal)
        }
        if (Parking != "") {
            sum = parseFloat(sum) + parseFloat(Parking)
        }
        if (Toll != "") {
            sum = parseFloat(sum) + parseFloat(Toll)
        }

        if (ChildCarSheet == true) {
            sum = parseFloat(sum) + 10
        }
        if (Chk_rub == true) {
            sum = parseFloat(sum) + 10
        }
        if (MeetGreat) {
            sum = parseFloat(sum) + 10
        }
        //if (Chk_Baggageclaim == true) {
        //    sum = parseFloat(sum) + 10
        //}
        if (Chk_PetInCage == true) {
            sum = parseFloat(sum) + 10
        }
        if (Gratuity != "-") {
            var Gra = parseFloat(sum) * parseFloat(Gratuity) / 100;
            GratuityAmount = Gra;
            sum = parseFloat(sum) + parseFloat(Gra);
        }
        if (LateNight == 10) {
            sum = parseFloat(sum) + 10;
        }

        $("#txt_Total").val(sum.toFixed(2));
    });

    $("#Chk_ChildCarSheet").change(function () {
        debugger;

        ChildCarSheet = ($(this).is(":checked"));

        if (ChildCarSheet) {
            SetRemark("Child Car Sheet");
            //SpecialService = parseFloat(SpecialService) + 10;
            //$("#txt_SpecialService").val(SpecialService);
            sum = parseFloat(sum) + 10
            sum = sum.toFixed(2)
            $("#txt_Total").val(sum);
        }
        else {
            CheckRemark("Child Car Sheet");
            //SpecialService = parseFloat(SpecialService) - 10;
            //$("#txt_SpecialService").val(SpecialService);
            sum = parseFloat(sum) - 10;
            sum = sum.toFixed(2)
            $("#txt_Total").val(sum);
        }

    });

    $("#Chk_rub").change(function () {
        debugger;
        CurbSidePickUp = ($(this).is(":checked"));
        Chk_rub = ($(this).is(":checked"));

        if (Chk_rub) {
            SetRemark("Curb Side PickUp");
            sum = parseFloat(sum) + 10
            sum = sum.toFixed(2)
            $("#txt_Total").val(sum);
        }
        else {
            CheckRemark("Curb Side PickUp");
            sum = parseFloat(sum) - 10;
            sum = sum.toFixed(2)
            $("#txt_Total").val(sum);
        }

    });

    $("#Chk_PetInCage").change(function () {
        debugger;
        PetInCage = ($(this).is(":checked"));
        Chk_PetInCage = ($(this).is(":checked"));

        if (Chk_PetInCage) {
            SetRemark("Pet In Cage");
            sum = parseFloat(sum) + 10
            sum = sum.toFixed(2)
            $("#txt_Total").val(sum);
        }
        else {
            CheckRemark("Pet In Cage");
            sum = parseFloat(sum) - 10;
            sum = sum.toFixed(2)
            $("#txt_Total").val(sum);
        }

    });

    $("#ddl_VehicleType").change(function () {
        debugger;
        var Destination = $('#txt_Address').val();
        if (Destination == "") {

            alert("Please Complete Address Feild")
            return false;
        }

        var Tab = "1";
        var VehicleSid = $('#ddl_VehicleType').val();
        var data = { Tab: Tab, sid: VehicleSid }
        $.ajax({
            type: "POST",
            url: "../Admin/ReservationRateHandler.asmx/GetRate",
            data: JSON.stringify(data),
            contentType: "application/json",
            datatype: "json",
            success: function (response) {

                var obj = JSON.parse(response.d)
                if (obj.Retcode == 1) {
                    var Arr = obj.Arr
                    //var Splitter = Distance.split('km');
                    //var TravelDistance = Splitter[0];
                    var Baserate = (parseFloat(Arr[0].BaseCharge)).toFixed(2);
                    var CostPerMiles = (parseFloat(Arr[0].MilesPerDistance)).toFixed(2);
                    var SubTotal = parseFloat(CostPerMiles) * parseFloat(TotalDistance) + parseFloat(Baserate);
                    SubTotal = SubTotal;
                    sum = SubTotal;

                    $('#txt_Fare').val(sum.toFixed(2))
                    if (Chk_PetInCage) {
                        sum = parseFloat(sum) + 10
                    }
                    if (ChildCarSheet) {
                        sum = parseFloat(sum) + 10
                    }
                    //if (Chk_Baggageclaim) {
                    //    sum = parseFloat(sum) + 10
                    //}
                    if (MeetGreat) {
                        sum = parseFloat(sum) + 10
                    }

                    if (Parking != 0) {

                        sum = parseFloat(sum) + parseFloat(Parking)
                    }
                    if (Toll != 0) {

                        sum = parseFloat(sum) + parseFloat(Toll)
                    }
                    if (Chk_rub) {
                        sum = parseFloat(sum) + 10
                    }
                    //if()
                    if (Gratuity > 0) {
                        var GraAmount = (parseFloat(Gratuity) * parseFloat(sum)) / 100;
                        GratuityAmount = GraAmount;
                        sum = parseFloat(sum) + parseFloat(GraAmount)
                    }
                    if (LateNight == 10) {
                        sum = parseFloat(sum) + 10;
                    }
                    sum = sum.toFixed(2);
                    $('#txt_Total').val(sum)

                    //Start Old Calculation
                    //var BaseDistance = (parseFloat(Arr[0].BaseDistance)).toFixed(2);
                    //var CPD = (parseFloat(Arr[0].MilesPerDistance)).toFixed(2);
                    //var base = Arr[0].BaseCharge

                    //var TD = TotalDistance.split(' ')

                    //if (parseFloat(BaseDistance) > parseFloat(TD[0])) {
                    //    var BaseAmount = base;

                    //    sum = BaseAmount
                    //}
                    //else {
                    //    var BaseAmount = base;
                    //    var Diff = parseFloat(TD[0]) - parseFloat(BaseDistance);
                    //    CPDTotal = parseFloat(Diff) * CPD
                    //    CPDTotal = CPDTotal.toFixed(2)
                    //    sum = parseFloat(BaseAmount) + parseFloat(CPDTotal)
                    //    BaseAmount = parseFloat(BaseAmount) + parseFloat(CPDTotal)
                    //}
                    //End Old Calculation

                }
                else {
                    $('#SpnMessege').text("No rate Found.")
                    $('#ModelMessege').modal('show')

                }
            },
            error: function () {

                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            },
        });

    });

    $("#txt_Email").blur(function () {
        debugger;

        Email = $("#txt_Email").val();

        for (var i = 0; i < CustomerList.length; i++) {
            var as = CustomerList[i].Email;
            if (CustomerList[i].Email == Email) {
                if (CustomerList[i].FirstName != null) {
                    $("#txt_FirstName").val(CustomerList[i].FirstName);
                }
                if (CustomerList[i].LastName != null) {
                    $("#txt_LastName").val(CustomerList[i].LastName);
                }
                if (CustomerList[i].Mobile != null) {
                    $("#txt_ContactNumber").val(CustomerList[i].Mobile);
                }
                //$("#txt_LastName").val(CustomerList[i].LastName);
                $("#txt_ContactNumber").val(CustomerList[i].Mobile);
                //$("#txt_FirstName").val(CustomerList[i].FirstName);
            }
        }
        //$("#txt_Total").val(sum);
    });

    $("#txt_ContactNumber").blur(function () {
        debugger;

        ContactNo = $("#txt_ContactNumber").val();

        for (var i = 0; i < CustomerList.length; i++) {
            var as = CustomerList[i].Email;
            if (CustomerList[i].Mobile == ContactNo) {
                if (CustomerList[i].FirstName != null) {
                    $("#txt_FirstName").val(CustomerList[i].FirstName);
                }
                if (CustomerList[i].LastName != null) {
                    $("#txt_LastName").val(CustomerList[i].LastName);
                }
                if (CustomerList[i].Mobile != null) {
                    $("#txt_ContactNumber").val(CustomerList[i].Mobile);
                }
                //$("#txt_LastName").val(CustomerList[i].LastName);
                $("#txt_Email").val(CustomerList[i].Email);
                //$("#txt_FirstName").val(CustomerList[i].FirstName);
            }
        }
        //$("#txt_Total").val(sum);
    });

    $("#txt_FlightTime").blur(function () {
        debugger;

        sum = 0
        var Num = /^[0-9]+$/;
        var FareVal = $("#txt_Fare").val();
        Gratuity = $("#txt_Gratuity option:selected").val();
        Parking = $("#txt_Parking").val();
        Toll = $("#txt_Toll").val();

        if (FareVal != "") {
            sum = parseFloat(sum) + parseFloat(FareVal)
        }


        if (Parking != "") {
            sum = parseFloat(sum) + parseFloat(Parking)
        }
        if (Toll != "") {
            sum = parseFloat(sum) + parseFloat(Toll)
        }


        if (ChildCarSheet == true) {
            sum = parseFloat(sum) + 10
        }

        if (Chk_rub == true) {
            sum = parseFloat(sum) + 10
        }
        if (MeetGreat) {
            sum = parseFloat(sum) + 10
        }
        //if (Chk_Baggageclaim == true) {
        //    sum = parseFloat(sum) + 10
        //}
        if (Chk_PetInCage == true) {
            sum = parseFloat(sum) + 10
        }
        if (Gratuity != "") {
            var Gra = parseFloat(sum) * parseFloat(Gratuity) / 100;
            GratuityAmount = Gra;
            sum = parseFloat(sum) + parseFloat(Gra);
            //sum = sum.toFixed(2);
            //sum = parseFloat(sum) * parseFloat(Gratuity) / 100;
        }

        var timing = $("#txt_FlightTime").val();
        var Splitter = timing.split(':');
        var Hours = Splitter[0];
        var Min = Splitter[1];
        if (Hours == "23" && Min != "00") {
            $("#txt_LateNight").val('10');
            LateNight = 10;
            sum = parseFloat(sum) + 10
        }
        else if (Hours == "00" || Hours == "01" || Hours == "02" || Hours == "03" || Hours == "04") {
            $("#txt_LateNight").val('10');
            LateNight = 10;
            sum = parseFloat(sum) + 10
        }
        else {
            $("#txt_LateNight").val('');
            LateNight = 0;
        }
        $("#txt_Total").val(sum.toFixed(2));
    });

    //$("#Chk_Baggageclaim").change(function () {
    //    debugger;
    //    BaggageClaimPickup = ($(this).is(":checked"));
    //    Chk_Baggageclaim = ($(this).is(":checked"));

    //    if (Chk_Baggageclaim) {

    //        sum = parseFloat(sum) + 10
    //        $("#txt_Total").val(sum);
    //    }
    //    else {

    //        sum = parseFloat(sum) - 10;
    //        $("#txt_Total").val(sum);
    //    }

    //});
})

function Submit() {
    debugger;

    var text = $('#btn_RegiterAgent').val()

    if (text == "Add") {

        bValid = Validation();

        if (bValid) {

            var Input = $('input[name="Text[]"]').map(function () {
                return this.value
            }).get()


            var DropDowns = $('select[name="Drop[]"]').map(function () {
                return this.value;
            }).get();

            var CheckBoxes = [Paid, Completed, Collect, CCPayment, ChildCarSheet]

            //var Remarks = $('#txt_AreaRemarks').val();
            var Remarks = "";
            Remarks = $('#txt_AreaRemarks').val();
            var TravelType = '';

            var Status = $('#hdn_Status').val();

            AirportName = $("#Select_Location option:selected").text();
            var Airlines = $('#Select_Airline').val();
            //alert(Airlines);
            var DriverName = $("#txt_Assigned option:selected").text();
            var DriverID = $("#txt_Assigned option:selected").val();

            var data = { Input: Input, DropDowns: DropDowns, CheckBoxes: CheckBoxes, Remarks: Remarks, TravelType: TravelType, TotalDistance: TotalDistance, TimeTaken: TimeTaken, source: source, destination: destination, AirportName: AirportName, MeetGreat: MeetGreat, SpecialAssistant: SpecialAssistant, CurbSidePickUp: CurbSidePickUp, BaggageClaimPickup: BaggageClaimPickup, PetInCage: PetInCage, Status: Status, DriverName: DriverName, Airlines: Airlines, DriverID: DriverID, Email: Email, ContactNo: ContactNo, GratuityAmount: GratuityAmount }

            var Time = $('#txt_Time').val();

            $.ajax({
                type: "POST",
                url: "../Admin/ReservationHandler.asmx/AirPortReservation",
                data: JSON.stringify(data),
                contentType: "application/json",
                datatype: "json",
                success: function (response) {

                    var obj = JSON.parse(response.d)
                    if (obj.Retcode == 1) {
                        $('#SpnMessege').text("AirPort Reservation Done Successfully.")
                        $('#ModelMessege').modal('show')
                        ClearAll();
                        //windows.location.href="BookingList.aspx"
                    }
                    else {
                        $('#SpnMessege').text("Reservation unsuccessfull.")
                        $('#ModelMessege').modal('show')
                    }
                },
                error: function () {

                    $('#SpnMessege').text("Something Went Wrong.")
                    $('#ModelMessege').modal('show')
                },
            });
        }
    }

    else {
        bValid = Validation();

        if (bValid) {

            var Input = $('input[name="Text[]"]').map(function () {
                return this.value
            }).get()

            var DropDowns = $('select[name="Drop[]"]').map(function () {
                return this.value;
            }).get();

            var CheckBoxes = [Paid, Completed, Collect, CCPayment, ChildCarSheet]

            var Remarks = $('#txt_AreaRemarks').val();

            var TravelType = "";

            var Status = $('#hdn_Status').val();

            AirportName = $("#Select_Location option:selected").text();
            var DriverName = $("#txt_Assigned option:selected").text();
            var DriverID = $("#txt_Assigned option:selected").val();
            var Airlines = $('#Select_Airline').val();
            var data = { sid: sid, Input: Input, DropDowns: DropDowns, CheckBoxes: CheckBoxes, Remarks: Remarks, TravelType: TravelType, TotalDistance: TotalDistance, TimeTaken: TimeTaken, source: source, destination: destination, AirportName: AirportName, MeetGreat: MeetGreat, SpecialAssistant: SpecialAssistant, CurbSidePickUp: CurbSidePickUp, BaggageClaimPickup: BaggageClaimPickup, PetInCage: PetInCage, Status: Status, DriverName: DriverName, Airlines: Airlines, DriverID: DriverID, Email: Email, ContactNo: ContactNo, GratuityAmount: GratuityAmount }


            var Time = $('#txt_Time').val();

            $.ajax({
                type: "POST",
                url: "../Admin/ReservationHandler.asmx/UpdateAirPortReservation",
                data: JSON.stringify(data),
                contentType: "application/json",
                datatype: "json",
                success: function (response) {

                    var obj = JSON.parse(response.d)
                    if (obj.Retcode == 1) {
                        $('#SpnMessege').text("AirPort Reservation Updated Successfully.")
                        $('#ModelMessege').modal('show')
                        ClearAll();
                    }
                    else {
                        $('#SpnMessege').text("Reservation unsuccessfull.")
                        $('#ModelMessege').modal('show')
                    }
                },
                error: function () {
                    $('#SpnMessege').text("Something Went Wrong.")
                    $('#ModelMessege').modal('show')
                },
            });
        }
    }
}

function Validation() {

    debugger;
    //alert(dLat);
    //alert(dLong);
    //alert(TimeTaken);
    //alert(TotalDistance);
    var regex = new RegExp("^[a-zA-Z]+$"); // for alphabets to allow
    var FN = $('#txt_FirstName').val();
    //if (FN != "") {
    //    if (regex.test(FN)) {
    //        $('#lbl_FirstName').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_FirstName').text("Only alphbets Are Allowed");
    //        $('#lbl_FirstName').css("display", "");
    //        return false;
    //    }
    //}
    if (FN == "") {
        $('#lbl_FirstName').text("* This field is required");
        $('#lbl_FirstName').css("display", "");
        return false;
    }
    else {
        $('#lbl_FirstName').css("display", "none");
    }
    var LN = $('#txt_LastName').val();
    //if (LN != "") {
    //    if (regex.test(LN)) {
    //        $('#lbl_LastName').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_LastName').text("Only alphbets Are Allowed");
    //        $('#lbl_LastName').css("display", "");
    //        return false;}
    //}
    if (LN == "") {
        $('#lbl_LastName').text("* This field is required");
        $('#lbl_LastName').css("display", "");
        return false;
    }
    else {
        $('#lbl_LastName').css("display", "none");
    }

    // Mobile Validation 

    ContactNo = document.getElementById("txt_ContactNumber").value;
    //var pattern = /^\d{10}$/;
    //if (LN != "") {
    //    if (pattern.test(MN)) {
    //        $('#lbl_ContactNumber').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_ContactNumber').text("It is not valid mobile number.input 10 digits number!");
    //        $('#lbl_ContactNumber').css("display", "");
    //        return false;
    //    }
    //}
    if (ContactNo == "") {
        $('#lbl_ContactNumber').text("* This field is required");
        $('#lbl_ContactNumber').css("display", "");
        return false;
    }
    else {
        $('#lbl_ContactNumber').css("display", "none");
    }

    // Email validation 
    Email = document.getElementById("txt_Email").value;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (Email != "") {
        if (emailReg.test(Email)) {
            $('#lbl_Email').css("display", "none");
        }
        else {
            $('#lbl_Email').text("Please Enter proper Email Format!");
            $('#lbl_Email').css("display", "");
            return false;
        }
    }
    else {
        $('#lbl_Email').text("* This field is required");
        $('#lbl_Email').css("display", "");
        return false;
    }


    var ReservationDate = document.getElementById("txt_Date").value;
    if (ReservationDate == "") {
        $('#lbl_Date').text("* This field is required");
        $('#lbl_Date').css("display", "");
        return false;
    }
    else {
        $('#lbl_Date').css("display", "none");
    }


    var Code = document.getElementById("txt_Code").value;
    if (Code == "") {
        Code = 0;
        //$('#lbl_Code').text("* This field is required");
        //$('#lbl_Code').css("display", "");
        //return false;
    }
    else {
        $('#lbl_Code').css("display", "none");
    }

    var FlightTime = document.getElementById("txt_FlightTime").value;
    //var test = $('#txt_FlightTime').val();
    //alert(test);
    //test = $('#txt_FlightTime').text();
    //alert(test);
    if (FlightTime == "") {
        $('#lbl_FlightTime').text("* This field is required");
        $('#lbl_FlightTime').css("display", "");
        return false;
    }
    else {
        $('#lbl_FlightTime').css("display", "none");
    }

    var Adult = document.getElementById("txt_Adult").value;
    var Num = /^[0-9]+$/;
    if (Adult != "") {
        if (Num.test(Adult)) {
            $('#lbl_Adult').css("display", "none");
        }
        else {
            $('#lbl_Adult').text("Only Numbers Allowed!!");
            $('#lbl_Adult').css("display", "");
            return false;
        }
    }
    else {

        $('#lbl_Adult').css("display", "");
        return false;
    }

    //var FlightNumber = document.getElementById("txt_FlightNumber").value;
    //if (FlightNumber == "") {
    //    $('#lbl_FlightNumber').text("* This field is required");
    //    $('#lbl_FlightNumber').css("display", "");
    //    return false;
    //}
    //else {
    //    $('#lbl_FlightNumber').css("display", "none");
    //}


    //var Time = document.getElementById("txt_Time").value;
    //if (Time == "") {
    //    $('#lbl_Time').text("* This field is required");
    //    $('#lbl_Time').css("display", "");
    //    return false;
    //}
    //else {
    //    $('#lbl_Time').css("display", "none");
    //}


    //var FlightDate = document.getElementById("txt_FlightDate").value;
    //if (FlightDate == "") {
    //    $('#lbl_FlightDate').text("* This field is required");
    //    $('#lbl_FlightDate').css("display", "");
    //    return false;
    //}
    //else {
    //    $('#lbl_FlightDate').css("display", "none");
    //}


    //var AS = $("#txt_Assigned option:selected").text();

    //if (AS != "")
    //{
    //    if (regex.test(AS)) {
    //        $('#lbl_Assigned').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_Assigned').text("Only alphbets Are Allowed");
    //        $('#lbl_Assigned').css("display", "");
    //        return false;
    //    }
    //}
    //else {
    //    $('#lbl_Assigned').text("* This field is required");
    //    $('#lbl_Assigned').css("display", "");
    //    return false;
    //}

    var CCFour = document.getElementById("txt_Last4").value;
    var patternFour = /^\d{4}$/;
    if (CCFour != "") {
        if (patternFour.test(CCFour)) {
            $('#lbl_Last4').css("display", "none");
        }
        else {
            $('#lbl_Last4').text("Only 4 Digits are allowed!");
            $('#lbl_Last4').css("display", "");
            return false;
        }
    }
    else {
        $('#lbl_Last4').text("* This field is required");
        $('#lbl_Last4').css("display", "");
        return false;
    }

    var Address = document.getElementById("txt_Address").value;
    if (Address == "") {
        $('#lbl_Address').text("* This field is required");
        $('#lbl_Address').css("display", "");
        return false;
    }
    else {
        $('#lbl_Address').css("display", "none");
    }

    var VehicleType = $("#ddl_VehicleType option:selected").val();
    if (VehicleType == "0") {
        $('#lbl_Vehical').text("* This field is required");
        $('#lbl_Vehical').css("display", "");
        return false;
    }
    else {
        $('#lbl_Vehical').css("display", "none");
    }
    var payment;

    if (Paid) {
        payment = true
    }
    else if (Completed) {
        payment = true
    }
    else if (Collect) {
        payment = true
    }
    else if (CCPayment) {
        payment = true
    }
    else if (ChildCarSheet) {
        payment = true
    }
    //if (payment != true) {

    //    alert("Select Any payment option");
    //    return false;
    //}



    //var Fare = document.getElementById("txt_Fare").value;
    //Num = /^[0-9]+$/;
    //if (Fare != "") {
    //    if (Num.test(Fare)) {
    //        $('#lbl_Fare').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_Fare').text("Only Numbers Allowed!!");
    //        $('#lbl_Fare').css("display", "");
    //        return false;
    //    }
    //}
    //else {

    //    $('#lbl_Fare').css("display", "");
    //    return false;
    //}

    //var Gratuity = document.getElementById("txt_Gratuity").value;
    //Num = /^[0-9]+$/;
    //if (Gratuity != "") {
    //    if (Num.test(Gratuity)) {
    //        $('#lbl_Gratuity').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_Gratuity').text("Only Numbers Allowed!!");
    //        $('#lbl_Gratuity').css("display", "");
    //        return false;
    //    }
    //}
    //else {

    //    $('#lbl_Gratuity').css("display", "");
    //    return false;
    //}


    //var Parking = document.getElementById("txt_Parking").value;
    //Num = /^[0-9]+$/;
    //if (Parking != "") {
    //    if (Num.test(Parking)) {
    //        $('#lbl_Parking').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_Parking').text("Only Numbers Allowed!!");
    //        $('#lbl_Parking').css("display", "");
    //        return false;
    //    }
    //}
    //else {

    //    $('#lbl_Parking').css("display", "");
    //    return false;
    //}


    //var TotalFare = document.getElementById("txt_Total").value;
    //Num = /^[0-9]+$/;
    //if (TotalFare != "") {
    //    if (Num.test(TotalFare)) {
    //        $('#lbl_Total').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_Total').text("Only Numbers Allowed!!");
    //        $('#lbl_Total').css("display", "");
    //        return false;
    //    }
    //}
    //else {

    //    $('#lbl_Total').css("display", "");
    //    return false;
    //}

    //var Toll = document.getElementById("txt_Toll").value;
    //Num = /^[0-9]+$/;
    //if (Toll != "") {
    //    if (Num.test(Toll)) {
    //        $('#lbl_Toll').css("display", "none");
    //    }
    //    else {
    //        $('#lbl_Toll').text("Only Numbers Allowed!!");
    //        $('#lbl_Toll').css("display", "");
    //        return false;
    //    }
    //}
    //else {

    //    $('#lbl_Toll').css("display", "");
    //    return false;
    //}


    //var City = document.getElementById("txt_City").value;
    //if (City == "") {
    //    $('#lbl_City').text("* This field is required");
    //    $('#lbl_City').css("display", "");
    //    return false;
    //}
    //else {
    //    $('#lbl_City').css("display", "none");
    //}

    //var State = document.getElementById("txt_State").value;
    //if (State == "") {
    //    $('#lbl_State').text("* This field is required");
    //    $('#lbl_State').css("display", "");
    //    return false;
    //}
    //else {
    //    $('#lbl_State').css("display", "none");
    //}

    //var ZipCode = document.getElementById("txt_ZipCode").value;
    //if (ZipCode == "") {
    //    $('#lbl_ZipCode').text("* This field is required");
    //    $('#lbl_ZipCode').css("display", "");
    //    return false;
    //}
    //else {
    //    $('#lbl_ZipCode').css("display", "none");
    //}

    return true;

}

function GetServices() {
    $.ajax({
        url: "../Admin/ReservationHandler.asmx/GetAllServices",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var ServiceType = obj.ServiceType;
            var arrLocation = obj.Location;
            if (obj.retCode == 1) {
                arrService = obj.ServiceType;
                //debugger;

                if (arrLocation.length > 0) {
                    var ddlRequest = '';
                    $("#Select_Location").empty();
                    //var ddlRequest = '<option value="-" selected="selected">Select</option>';
                    //ddlRequest = null;
                    for (i = 0; i < arrLocation.length; i++) {
                        ddlRequest += '<option value="' + arrLocation[i].sid + ',' + arrLocation[i].Latitude + ',' + arrLocation[i].Longitude + ',' + arrLocation[i].LocationName + '">' + arrLocation[i].LocationName + '</option>';
                    }
                    $("#Select_Location").append(ddlRequest);
                }
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}

function GetAllDriver() {
    $.ajax({
        url: "../Admin/ReservationHandler.asmx/GetAllDriver",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Driver = obj.tblDriver;
            //var arrLocation = obj.Location;
            if (obj.retCode == 1) {
                arrService = obj.tblDriver;
                //debugger;
                var OptionMac_Name = '';

                if (obj.retCode == 1) {
                    for (var i = 0; i < Driver.length; i++) {
                        var fullName = Driver[i].sFirstName + " " + Driver[i].sLastName;
                        OptionMac_Name += '<option value="' + Driver[i].Sid + '" >' + fullName + '</option>'
                    }
                    //var s = OptionMac_Name.split('undefined')
                    //Mac_Name = s[1];
                    $("#txt_Assigned").append(OptionMac_Name);
                }
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}

function GetAllAirPort() {
    $.ajax({
        url: "../Admin/ReservationHandler.asmx/GetAllAirPort",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Location = obj.tblDriver;
            if (obj.retCode == 1) {
                arrService = obj.tblDriver;
                //debugger;
                var OptionMac_Name = '';

                if (obj.retCode == 1) {
                    for (var i = 0; i < Location.length; i++) {

                        OptionMac_Name += '<option value="' + Location[i].Latitude + ',' + Location[i].Longitude + '" >' + Location[i].LocationName + '</option>'
                    }

                    $("#Select_Location").append(OptionMac_Name);

                }
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}

function VehicleNameDropDown() {
    //debugger;
    $.ajax({
        type: "POST",
        url: "../Admin/ReservationHandler.asmx/VehicleNameDropDown",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Manufacturer = obj.VehicleName;
            $("#ddl_VehicleType").empty();
            //$("#Addl_VehicleType").empty();
            var OptionVehicleType;
            OptionVehicleType += '<option value="0">Select Vehicle Type</option>'

            if (obj.retCode == 1) {
                for (var i = 0; i < Manufacturer.length; i++) {

                    //OptionVehicleType += '<option value="' + Manufacturer[i].sid + '" >' + Manufacturer[i].Name + '</option>'
                    OptionVehicleType += '<option value="' + Manufacturer[i].sid + '" >' + Manufacturer[i].Model + '</option>'
                }
                var s = OptionVehicleType.split('undefined')
                VehicleType = s[1];
                $("#ddl_VehicleType").append(VehicleType);
                //$("#Addl_VehicleType").append(VehicleType);
            }
            else {

            }
        }
    });
}

function GetAllVehicle() {

    $.ajax({
        url: "../Admin/ReservationRateHandler.asmx/GetAllVehicle",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Driver = obj.ArrCar;

            if (obj.retCode == 1) {
                arrService = obj.ArrCar;
                //debugger;
                var OptionMac_Name = null;

                if (obj.retCode == 1) {
                    for (var i = 0; i < Driver.length; i++) {
                        var fullName = Driver[i].sFirstName + " " + Driver[i].sLastName;
                        OptionMac_Name += '<option value="' + Driver[i].Sid + '" >' + fullName + '</option>'
                    }

                    $("#Select_Vehical").append(OptionMac_Name);
                }
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}

function GetAirLines() {

    $.ajax({
        url: "../Admin/ReservationRateHandler.asmx/GetAirLines",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);


            if (obj.Retcode == 1) {
                var Driver = obj.Arr;
                //debugger;
                var OptionMac_Name = null;

                if (obj.Retcode == 1) {
                    var Driver = obj.Arr;
                    for (var i = 0; i < Driver.length; i++) {

                        OptionMac_Name += '<option value="' + Driver[i].Callsign + '" >' + Driver[i].Callsign + '</option>'
                    }

                    $("#Select_Airlines").append(OptionMac_Name);
                }
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}

function ShowModel() {
    $(".pac-container").css("z-index", $("#ModelRate").css("z-index"));
    $.ajax({
        url: "../Admin/ReservationHandler.asmx/GetAllAirPort",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Location = obj.tblDriver;
            if (obj.retCode == 1) {
                arrService = obj.tblDriver;
                //debugger;
                var OptionMac_Name = null;

                if (obj.retCode == 1) {
                    for (var i = 0; i < Location.length; i++) {

                        OptionMac_Name += '<option value="' + Location[i].Latitude + ',' + Location[i].Longitude + '" >' + Location[i].LocationName + '</option>'
                    }
                    $("#Sel_AirPortNamePopUp").append(OptionMac_Name);
                }
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });

    $('#ModelRate').modal('show')
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

function ClearAll() {
    $('input[name="Text[]"]').val('');
    //$("#txt_FirstName").val('');
    //$("#txt_LastName").val('');
    //$("#txt_ContactNumber").val('');
    //$("#txt_Email").val('');
    //$("#txt_Date").val('');
    //$("#txt_Code").val('');
    //$("#txt_Adult").val('');
    //$("#txt_Child").val('');
    $("#Sel_TravelType").val('Domestic');
    //$("#Select_Airline").val('-');
    //$("#txt_FlightNumber").val('');
    //$("#txt_Time").val('');
    //$("#txt_FlightDate").val('');
    $("#Select_Service").val('From Airport');
    $("#Sel_CC").val('-');
    $("#ddl_VehicleType").val(0);
    $("#txt_Assigned").val(0);
    $('#Chk_Paid').attr('checked', true);
    $('#Chk_Completed').attr('checked', false);
    //$('#Chk_Collect').attr('checked', false);
    $('#Chk_CCPayment').attr('checked', false);
    $('#Chk_ChildCarSheet').attr('checked', false);
    $('#Chk_Meet').attr('checked', false);
    $("#Chk_Special").attr('checked', false);
    $("#Chk_rub").attr('checked', false);
    $("#Chk_Baggageclaim").attr('checked', false);
    $("#Chk_PetInCage").attr('checked', false);
    $("#txt_AreaRemarks").val('');
    $("#Select_Airline").val('');
    $("#txt_Email").val('');
    $("#txt_ContactNumber").val('');
    $("#txt_Gratuity").val("-");

}

function HideMessage() {
    $('#SpnMessege').text("We get Source & Destination")
    $('#ModelMessege').modal('show')
    //alert("We get Source & Destination");
    $('#ModelRate').modal('hide');
}

function MapAirPortName_Change() {
    debugger;
    $("#txt_New_Location").val('');
    $("#PopUpDistance").text('');
    $("#PopUpTime").text('');
}

function GetAllEmail() {

    $.ajax({
        url: "../Admin/ReservationHandler.asmx/GetAllEmail",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);

            if (obj.retCode == 1) {
                CustomerList = obj.CustomerList;
                CustomerList = CustomerList[0];
                //debugger;
                var Div = '';

                for (var i = 0; i < CustomerList.length; i++) {
                    Div += '<option value="' + CustomerList[i].Email + '" >' + CustomerList[i].Email + '</option>'
                }
                $("#Select_Email").append(Div);
            }
            else if (obj.retCode == -1) {
                //$('#SpnMessege').text("Somthing went wrong. Please try again.")
                //$("#ModelMessege").modal("show")
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}

function CheckRemark(Text) {
    debugger;
    var FinalRemark = '';

    //alert(asdf);
    Remark = $('#txt_AreaRemarks').val();
    var Splitter = Remark.split(',');
    for (var i = 0; i < Splitter.length; i++) {
        if (Text == Splitter[i] && i == 0) {
            Splitter[i] = '';
            for (var j = 0; j < Splitter.length; j++) {
                if (j == 0 || j == 1) {
                    FinalRemark = Splitter[j];
                }

                else {
                    if (Splitter[j] != '')
                        FinalRemark += "," + Splitter[j];
                }
            }
        }
        else if (Text == Splitter[i]) {
            Splitter[i] = '';
            for (var j = 0; j < Splitter.length; j++) {
                if (j == 0) {
                    FinalRemark = Splitter[j];
                }
                else {
                    if (Splitter[j] != '')
                        FinalRemark += "," + Splitter[j];
                }
            }
        }
    }
    $('#txt_AreaRemarks').val(FinalRemark);
}

function SetRemark(text) {
    debugger;
    Remark = $('#txt_AreaRemarks').val();
    if (Remark != '') {
        Remark += "," + text;
        $('#txt_AreaRemarks').val(Remark);
    }
    else {
        Remark = text;
        $('#txt_AreaRemarks').val(Remark);
    }
}

function GetAllPhoneNo() {

    $.ajax({
        url: "../Admin/ReservationHandler.asmx/GetAllEmail",
        type: "POST",
        data: {},
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);

            if (obj.retCode == 1) {
                CustomerList = obj.CustomerList;
                CustomerList = CustomerList[0];
                //debugger;
                var Div = '';

                for (var i = 0; i < CustomerList.length; i++) {
                    Div += '<option value="' + CustomerList[i].Mobile + '" >' + CustomerList[i].Mobile + '</option>'
                }
                $("#Select_ContactNumber").append(Div);
            }
            else if (obj.retCode == -1) {
                //$('#SpnMessege').text("Somthing went wrong. Please try again.")
                //$("#ModelMessege").modal("show")
            }
        },
        error: function () {
            $('#SpnMessege').text("Somthing went wrong. Please try again.")
            $("#ModelMessege").modal("show")
        }
    });
}