﻿$(document).ready(function () {
    //setTimeout( LoadAllCount(),1000);
    //setTimeout(GetSerTodayTable(), 1000);
    LoadAllCount();
    //GetSerTodayTable();
});
var dtSerToday;
var dtSerTomorrow;
var dtResToday;
var dtUnassign;
var dtOnlineRes;
var dtUpcoming;
var dtPenging;
var Arr;
var ul = "";
var Driver;

function LoadAllCount() {
    $.ajax({
        type: "POST",
        url: "../Admin/DashboardHandler.asmx/LoadAllCount",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1) {
                var SerToday = result.SerToday;
                $('.countvisitors').countTo({
                    from: 0,
                    to: SerToday,
                    speed: 1000,
                });

                var SerTomorrow = result.SerTomorrow;
                $('.countrevenue').countTo({
                    from: 0,
                    to: SerTomorrow,
                    speed: 1000,
                });

                var ResToday = result.ResToday;
                $('.countemail').countTo({
                    from: 0,
                    to: ResToday,
                    speed: 1000,
                });

                var Unassign = result.Unassign;
                $('.countbookings').countTo({
                    from: 0,
                    to: Unassign,
                    speed: 1000,
                });

                var Amount = result.Amount;
                $('.TodayAmount').countTo({
                    from: 0,
                    to: Amount,
                    speed: 1000,
                });

                var Online = result.OnlineRes;
                $('.OnlineReservation').countTo({
                    from: 0,
                    to: Online,
                    speed: 1000,
                });

                var Upcoming = result.Upcoming;
                $('.UpcomingReservation').countTo({
                    from: 0,
                    to: Upcoming,
                    speed: 1000,
                });

                var Pending = result.Pending;
                $('.PendingReservation').countTo({
                    from: 0,
                    to: Pending,
                    speed: 1000,
                });

                dtSerToday = result.dtSerToday;
                dtSerTomorrow = result.dtSerTomorrow;
                dtResToday = result.dtResToday;
                dtUnassign = result.dtUnassign;
                dtOnlineRes = result.dtOnlineRes;
                dtUpcoming = result.dtUpcoming;
                dtPenging = result.dtPenging;
                Driver = result.ArrDriver;
                GetGridData("SerToday");
            }
        },
    });
}

function SendMail() {
    var validate = true;

    if ($("#email").val() == "") {
        $("#email").focus();
        alert("Please enter your Email");
        validate = false;
        return false;
    }
    if ($("#email").val() != "") {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test($("#email").val())) {
            $("#email").focus();
            alert("Please enter valid Email ID");
            return false;
        }
        else if ($("#text").val() == "") {
            $("#text").focus();
            alert("Please enter your Message");
            validate = false;
            return false;
        }
    }
    if ($("#Sub").val() == "") {
        $("#Sub").focus();
        alert("Please enter Subject");
        return false;
    }
    if (validate) {
        var Email = $("#email").val();
        var Sub = $("#Sub").val();
        var Message = $("#text").val();
        $.ajax({
            type: "POST",
            url: "../Admin/DashboardHandler.asmx/EmailSending",
            beforeSend: function () {
                $('#wait').css('display', 'inline');
            },
            complete: function () {
                $('#wait').css('display', 'none');
            },
            data: '{"Email":"' + Email + '","Sub":"' + Sub + '","Message":"' + Message + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                try {
                    var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
                    if (result.retCode == 1) {
                        $("#Sub").val(' ');
                        $("#email").val(' ');
                        $("#text").val('');
                        alert("Your request sent.We will get in touch with you soon.");
                    }
                    else {
                        alert("Your request sent.We will get in touch with you soon");
                    }

                } catch (e) { }
            },
            error: function () {
                alert("Your request sent.We will get in touch with you soon");
            }
        });
        return validate;
    }
}

function GetGridData(Title) {
    ul = '';
    Arr = '';
    $("#GridTitle").empty();
    if (Title == "SerToday") {
        Arr = dtSerToday;
        $("#GridTitle").append("Reservations for Service Today");
    }
    else if (Title == "SerTomorrow") {
        Arr = dtSerTomorrow;
        $("#GridTitle").append("Reservations for Service Tomorrow");
    }
    else if (Title == "ResToday" || Title == "Amount") {
        Arr = dtResToday;
        $("#GridTitle").append("Reservations Made Today");
    }
    else if (Title == "Unassign") {
        Arr = dtUnassign;
        $("#GridTitle").append("Unassigned For Tommorow");
    }
    else if (Title == "Online") {
        Arr = dtOnlineRes;
        $("#GridTitle").append("Online Reservations");
    }
    else if (Title == "Upcoming") {
        Arr = dtUpcoming;
        $("#GridTitle").append("Upcoming Reservations");
    }
    else if (Title == "Pending") {
        Arr = dtPenging;
        $("#GridTitle").append("Pending Reservations");
    }
    LoadGrid();
}

function Update(sid, Service) {
    debugger
    if (Service == "From Airport" || Service == "To Airport") {

        window.location.href = "AirPortReservation.aspx?sid=" + sid
    }
    else if (Service == "Point To Point Reservation") {

        window.location.href = "PointToPoint.aspx?sid=" + sid
    }
    else {
        window.location.href = "HourReservation.aspx?sid=" + sid
    }
}

function ConfirmBooking(sid) {
    debugger
    $('#AssignDriver').modal('show')
    $('#hdn').val(sid)
}

function Assign() {
    debugger
    var BookingSid = $('#hdn').val()
    var DriverSid = $('#Sel_Driver').val()
    var DriverName = $('#Sel_Driver option:selected').text()

    var data = { BookingSid: BookingSid, DriverSid: DriverSid, DriverName: DriverName }

    $.ajax({
        type: "POST",
        url: "ReservationHandler.asmx/AssignBooking",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {
                location.reload();
                $('#SpnMessege').text("Booking Confirm, an email has been sent to Customer and Driver.")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == 4) {
                $('#SpnMessege').text("Already Confirmed Booking.")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == 5) {
                $('#SpnMessege').text("This booking is cancelled, can not Confirmed Booking. ")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == 6) {
                $('#SpnMessege').text("This booking is Unpaid, Please paid amount. ")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == -1) {
                $('#SpnMessege').text("Driver Not Assigned, Something went wrong!.")
                $('#ModelMessege').modal('show')
            }

            else {
                $('#SpnMessege').text("Something Went Wrong!.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },


    });

}

function Cancel(sid) {
    debugger
    var data = { BookingSid: sid }
    $.ajax({
        type: "POST",
        url: "ReservationHandler.asmx/CancelBooking",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {
                location.reload();
                $('#SpnMessege').text("Booking Cancelled, an email has been sent to Customer and Driver.")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == 4) {
                $('#SpnMessege').text("Already Cancelled Booking.")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == -1) {
                $('#SpnMessege').text("Something went wrong!.")
                $('#ModelMessege').modal('show')
            }

            else {
                $('#SpnMessege').text("Something Went Wrong!.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });
}

function Delete(sid) {

    debugger
    var data = { BookingSid: sid }
    $.ajax({
        type: "POST",
        url: "ReservationHandler.asmx/DeleteBooking",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {
                location.reload();
                $('#SpnMessege').text("Booking Deleted")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == 4) {
                $('#SpnMessege').text("Already Cancelled Booking.")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == -1) {
                $('#SpnMessege').text("Something went wrong!.")
                $('#ModelMessege').modal('show')
            }

            else {
                $('#SpnMessege').text("Something Went Wrong!.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });

}

function SearchReservation() {
    $("#DriverDetails").dataTable().fnClearTable();
    $("#DriverDetails").dataTable().fnDestroy();
    var From = $('#datepicker1').val();
    var To = $('#datepicker2').val();
    var Status = $('#Sel_Status option:selected').val();
    //var DriverName = $("#sel_driver option:selected").text();

    if (From == '' && To != '') {
        alert("Please enter from date");
        return false;
    }
    else if (To == '' && From != '') {
        alert("Please enter to date");
    }
    if (From == '' && To == '' && Status == '0') {
        alert("Please Enter Date or Status to search")
        return false;
    }
    var data = { From: From, To: To, Status: Status }

    $.ajax({
        type: "POST",
        url: "ReservationHandler.asmx/SearchReservation",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            debugger
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                $('#datepicker1').val('');
                $('#datepicker2').val('');
                $('#Sel_Status').val(0);
                Arr = obj.Arr;
                var ul = '';
                //$('#DriverDetails tbody').empty();
                if (Arr && Arr.length > 0) {
                    for (var i = 0; i < Arr.length; i++) {
                        //alert(Arr[i].ReservationDate);
                        ul += '<tr class="odd">';
                        ul += '<td align="center" style="width: 10%">' + (i + 1) + '</td>';
                        ul += '<td align="center" style="cursor: pointer;width: 15%;" onclick="BookingDetails(' + i + ')"><a>' + Arr[i].ReservationID + '</a></td>';
                        ul += '<td align="center" style="width: 20%">' + Arr[i].Service + '</td>';
                        ul += '<td align="center" style="width: 10%">' + Arr[i].ReservationDate + '</td>';
                        ul += '<td align="center"style="width: 10%">' + Arr[i].TotalFare + '</td>';
                        var status = Arr[i].Status
                        if (status == "") {
                            ul += '<td align="center" style="width: 10%">Pending</td>';
                        }
                        else if (status == "Cancelled") {
                            ul += '<td align="center" style="width: 10%">Cancelled <p style="cursor: pointer; color:blue;" onclick="UndoStatus(' + Arr[i].sid + ')">Undo</p></td>';
                        }
                        else if (status == "Deleted") {
                            ul += '<td align="center" style="width: 10%">Deleted <p style="cursor: pointer; color:blue;" onclick="UndoStatus(' + Arr[i].sid + ')">Undo</p></td>';
                        }
                        else {
                            ul += '<td align="center" style="width: 10%">' + status + '</td>';
                        }

                        var Paid = Arr[i].Paid
                        if (Paid) {
                            ul += '<td align="center" style="width: 10%">Paid</td>';
                        }
                        else {
                            ul += '<td align="center" style="width: 10%"><span style="color:red"> Not paid</span></td>';
                        }

                        ul += '<td align="center"><a style="cursor: pointer" onclick="Update(' + Arr[i].sid + ',\'' + Arr[i].Service + '\')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a> | <a style="cursor: pointer" onclick="ConfirmBooking(' + Arr[i].sid + ')" href="#"><span class="fa fa-check" title="Confirm"></span></a> | <a style="cursor: pointer" onclick="Cancel(' + Arr[i].sid + ')" href="#"><span class="fa fa-times" title="Cancel"></span></a> | <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="Delete(' + Arr[i].sid + ')"></span></a></td>';
                        ul += '</tr>';
                    }
                    $('#DriverDetails tbody').append(ul);
                    $("#DriverDetails").dataTable({
                        "bSort": false
                    });
                    var OptionMac_Name
                    Driver = obj.ArrDriver;
                    for (var i = 0; i < Driver.length; i++) {
                        var fullName = Driver[i].sFirstName + " " + Driver[i].sLastName;
                        OptionMac_Name += '<option value="' + Driver[i].Sid + '" >' + fullName + '</option>'
                    }
                    $("#Sel_Driver").append(OptionMac_Name);
                }
                else {
                    $('#SpnMessege').text("No Record Found")
                    $('#ModelMessege').modal('show')
                    //l += '<tr>';
                    //l += '<td colspan="8" align="center">No Record Found </td>';
                    //l += '</tr>';
                    //('#DriverDetails').append(ul);
                }
            }
            else if (obj.retCode == -1) {
                $('#SpnMessege').text("No Record Found")
                $('#ModelMessege').modal('show')
            }

            else {
                $('#SpnMessege').text("Something Went Wrong!.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });
}

function UndoStatus(sid) {
    debugger
    var data = { BookingSid: sid }
    $.ajax({
        type: "POST",
        url: "ReservationHandler.asmx/UndoStatus",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {
                location.reload();
                $('#SpnMessege').text("Status of Booking is changed to Requested")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == -1) {
                $('#SpnMessege').text("Something went wrong!.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });
}

function LoadGrid() {
    $("#ReservationDetails").dataTable().fnClearTable();
    $("#ReservationDetails").dataTable().fnDestroy();

    if (Arr.length > 0) {
        for (var i = 0; i < Arr.length; i++) {
            ul += '<tr class="odd">';
            ul += '<td align="center" style="width: 10%">' + (i + 1) + '</td>';
            //ul += '<td align="center" style="width: 15%">' + Arr[i].ReservationID + '</td>';
            ul += '<td align="center" style="cursor: pointer;width: 15%;" onclick="BookingDetails(' + i + ')"><a>' + Arr[i].ReservationID + '</a></td>';
            ul += '<td align="center" style="width: 20%">' + Arr[i].Service + '</td>';
            ul += '<td align="center" style="width: 10%">' + Arr[i].ReservationDate + '</td>';
            if (Arr[i].Service == "From Airport") {
                ul += '<td align="center" style="width: 10%">' + Arr[i].FlightTime + '</td>';
                //$("#ResTime").text(Arr[ObjNo].FlightTime);
            }
            else if (Arr[i].Service == "To Airport") {
                ul += '<td align="center" style="width: 10%">' + Arr[i].Pickup_Time + '</td>';
                //$("#ResTime").text(Arr[ObjNo].Pickup_Time);
            }
            else {
                ul += '<td align="center" style="width: 10%">' + Arr[i].P2PTime + '</td>';
                // $("#ResTime").text(Arr[ObjNo].P2pTime);
            }
            ul += '<td align="center"style="width: 10%">' + Arr[i].TotalFare + '</td>';
            var status = Arr[i].Status
            if (status == "") {
                ul += '<td align="center" style="width: 10%">Pending</td>';
            }
            else if (status == "Cancelled") {
                ul += '<td align="center" style="width: 10%">Cancelled <p style="cursor: pointer; color:blue;" onclick="UndoStatus(' + Arr[i].sid + ')">Undo</p></td>';
            }
            else if (status == "Deleted") {
                ul += '<td align="center" style="width: 10%">Deleted <p style="cursor: pointer; color:blue;" onclick="UndoStatus(' + Arr[i].sid + ')">Undo</p></td>';
            }
            else {
                ul += '<td align="center" style="width: 10%">' + status + '</td>';
            }

            var Paid = Arr[i].Paid
            if (Paid) {
                ul += '<td align="center" style="width: 10%">Paid</td>';
            }
            else {
                ul += '<td align="center" style="width: 10%"><span style="color:red"> Not paid</span></td>';
            }

            ul += '<td align="center" style="width: 10%"><a style="cursor: pointer" onclick="Update(' + Arr[i].sid + ',\'' + Arr[i].Service + '\')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a> | <a style="cursor: pointer" onclick="ConfirmBooking(' + Arr[i].sid + ')" href="#"><span class="fa fa-check" title="Confirm"></span></a></td>';
            ul += '<td align="center" style="width: 10%"><a style="cursor: pointer" onclick="Cancel(' + Arr[i].sid + ')" href="#"><span class="fa fa-times" title="Cancel"></span></a> | <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="Delete(' + Arr[i].sid + ')"></span></a></td>';

            ul += '</tr>';
        }

        var OptionMac_Name

        for (var i = 0; i < Driver.length; i++) {
            var fullName = Driver[i].sFirstName + " " + Driver[i].sLastName;
            OptionMac_Name += '<option value="' + Driver[i].Sid + '" >' + fullName + '</option>'
        }
        $("#Sel_Driver").append(OptionMac_Name);
    }
    else {
        ul += '<tr>';
        ul += '<td align="center" >No Record Found</td>';
        ul += '<td align="center" >No Record Found</td>';
        ul += '<td align="center" >No Record Found</td>';
        ul += '<td align="center" >No Record Found</td>';
        ul += '<td align="center" >No Record Found</td>';
        ul += '<td align="center" >No Record Found</td>';
        ul += '<td align="center" >No Record Found</td>';
        ul += '<td align="center" >No Record Found</td>';
        ul += '<td align="center" >No Record Found</td>';
        ul += '<td align="center" >No Record Found</td>';
        ul += '</tr>';
        //$('#SpnMessege').text("No Reservation Found")
        //$('#ModelMessege').modal('show')
    }
    $('#ReservationDetails tbody').append(ul);
    $("#ReservationDetails").dataTable({
        "bSort": false
    });
    document.getElementById("ReservationDetails").removeAttribute("style")
}

function ser() {
    $("#ReservationDetails").dataTable().fnClearTable();
    $("#ReservationDetails").dataTable().fnDestroy();

    if (Arr.length > 0) {
        for (var i = 0; i < Arr.length; i++) {
            ul += '<tr class="odd">';
            ul += '<td align="center" style="width: 10%">' + (i + 1) + '</td>';
            //ul += '<td align="center" style="width: 15%">' + Arr[i].ReservationID + '</td>';
            ul += '<td align="center" style="cursor: pointer;width: 15%;" onclick="BookingDetails(' + i + ')"><a>' + Arr[i].ReservationID + '</a></td>';
            ul += '<td align="center" style="width: 20%">' + Arr[i].Service + '</td>';
            ul += '<td align="center" style="width: 10%">' + Arr[i].ReservationDate + '</td>';
            ul += '<td align="center"style="width: 10%">' + Arr[i].TotalFare + '</td>';
            var status = Arr[i].Status
            if (status == "") {
                ul += '<td align="center" style="width: 10%">Pending</td>';
            }
            else if (status == "Cancelled") {
                ul += '<td align="center" style="width: 10%">Cancelled <p style="cursor: pointer; color:blue;" onclick="UndoStatus(' + Arr[i].sid + ')">Undo</p></td>';
            }
            else if (status == "Deleted") {
                ul += '<td align="center" style="width: 10%">Deleted <p style="cursor: pointer; color:blue;" onclick="UndoStatus(' + Arr[i].sid + ')">Undo</p></td>';
            }
            else {
                ul += '<td align="center" style="width: 10%">' + status + '</td>';
            }

            var Paid = Arr[i].Paid
            if (Paid) {
                ul += '<td align="center" style="width: 10%">Paid</td>';
            }
            else {
                ul += '<td align="center" style="width: 10%"><span style="color:red"> Not paid</span></td>';
            }

            ul += '<td align="center"><a style="cursor: pointer" onclick="Update(' + Arr[i].sid + ',\'' + Arr[i].Service + '\')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a> | <a style="cursor: pointer" onclick="ConfirmBooking(' + Arr[i].sid + ')" href="#"><span class="fa fa-check" title="Confirm"></span></a> | <a style="cursor: pointer" onclick="Cancel(' + Arr[i].sid + ')" href="#"><span class="fa fa-times" title="Cancel"></span></a> | <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="Delete(' + Arr[i].sid + ')"></span></a></td>';
            ul += '</tr>';
        }
        $('#ReservationDetails tbody').append(ul);
        $("#ReservationDetails").dataTable({
            "bSort": false
        });
        var OptionMac_Name

        for (var i = 0; i < Driver.length; i++) {
            var fullName = Driver[i].sFirstName + " " + Driver[i].sLastName;
            OptionMac_Name += '<option value="' + Driver[i].Sid + '" >' + fullName + '</option>'
        }
        $("#Sel_Driver").append(OptionMac_Name);
    }
    else {
        $('#SpnMessege').text("No Reservation Found")
        $('#ModelMessege').modal('show')
    }
}

function GetSerTodayTable() {
    $.ajax({
        type: "POST",
        url: "../Admin/DashboardHandler.asmx/GetSerTodayTable",
        data: {},
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            if (result.retCode == 1)
            { }
        },
    });
}