﻿$(document).ready(function () {
    GetAllCustomer();
});
var Cus;

function AddCustomer() {
    debugger;
    var Bvalid = Validation();
    if (Bvalid == true) {

        var fName = $("#Fname").val()
        var LastName = $("#Lanme").val()
        var Mobile = $("#Mobile").val()
        var Address = $("#Address").val()
        var City = ""
        //var City = $("#Select_City").val()
        var Email = $("#Email").val()
        var Gender = $("#sel_Gender").val()
        var Country = ""
        var Pin = ""
        var Pan = "";

        var param = {
            fName: fName,
            LastName: LastName,
            Email: Email,
            Mobile: Mobile,
            Gender: Gender,
            Address: Address,
            City: City,
            Country: Country,
            Pan: Pan,
            Pin: Pin
        }
        $.ajax({
            type: "POST",
            url: "../Admin/CustomerHandler.asmx/AddCustomer",
            data: JSON.stringify(param),
            contentType: "application/json",
            datatype: "json",
            success: function (data) {
                var obj = JSON.parse(data.d);
                if (obj.retCode == 1) {
                    GetAllCustomer();
                    $('#SpnMessege').text("Customer Added Successfully");
                    $('#ModelMessege').modal('show');
                    $("#Fname").val('');
                    $("#Lanme").val('');
                    $("#Mobile").val('');
                    $("#Address").val('');
                    $("#Email").val('');
                    $("#sel_Gender").val('');
                    $("#AddCustomer").modal("hide");

                    //alert("Customer Added");
                    //window.location.reload();
                }
                else if (obj.retCode == 2) {
                    $('#SpnMessege').text("This Email Id is already exist");
                    $('#ModelMessege').modal('show');
                }
                else {
                    alert("Something Went Wrog");
                    window.location.reload();
                }
            }
        });
    }
}


function GetAllCustomer() {

    $.ajax({
        type: "POST",
        url: "../Admin/CustomerHandler.asmx/GetAllCustomer",
        data: '{}',
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            var Customer = obj.CustomerTable;
            var ul;
            $("#CustomerDetails").empty();
            if (obj.retcode == 1) {

                for (var i = 0; i < Customer.length; i++) {
                    ul += '<tr class="odd">'
                    ul += '<td align="center" style="width:8%">' + (i + 1) + '</td>'
                    ul += '<td align="center">' + Customer[i].FirstName + ' ' + Customer[i].LastName + '</td>'
                    ul += '<td align="center">' + Customer[i].Email + '</td>'
                    ul += '<td align="center">' + Customer[i].Mobile + '</td>'
                    ul += '<td align="center">' + Customer[i].Address + '</td>'
                    ul += '<td align="center" style="width:25%" ><a style="cursor: pointer" onclick="UpdateDilogBox(' + Customer[i].sid + ')" href="#"><span class="glyphicon glyphicon-edit" title="Edit" aria-hidden="true"></span></a> | <a style="cursor: pointer" onclick="Delete(' + Customer[i].sid + ')" href="#"><span class="glyphicon glyphicon-trash" title="Edit" aria-hidden="true"></span></a> </td>'
                    ul += '</tr>'

                }
                var s = ul.split('undefined')
                Cus = s[1];
                $("#CustomerDetails").append(Cus);
            }

            else {

            }
        }

    });

}

function OpenPopUp() {
    $("#AddCustomer").modal("show")
}

function Validation() {
    if ($("#Fname").val() == "") {
        alert("Please enter First Name");
        $("#Fname").focus();
        return false;
    }
    else if ($("#Lanme").val() == "") {
        alert("Please enter Last Name");
        $("#Lanme").focus();
        return false;
    }
    else if ($("#Mobile").val() == "") {
        alert("Please enter Mobile No");
        $("#Mobile").focus();
        return false;
    }
    else if ($("#Address").val() == "") {
        alert("Please enter Address");
        $("#Address").focus();
        return false;
    }
    else if ($("#Mobile").val() == "") {
        alert("Please enter Mobile No");
        $("#Mobile").focus();
        return false;
    }
    //else if ($("#Select_City").val() == "-") {
    //    alert("Please enter select City");
    //    $("#Select_City").focus();
    //    return false;
    //}


    // Email validation 
    var Email = document.getElementById("Email").value;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (Email != "") {
        if (emailReg.test(Email)) {

        }
        else {
            alert("Please enter valid Email ID");
            return false;
        }
    }
    else {

        alert("Please enter Email");
        $("#Email").focus();
        return false;
    }

    //else if ($("#Email").val() == "") {
    //    alert("Please enter Email");
    //    $("#Email").focus();
    //    return false;
    //}
    //else if ($("#Email").val() != "") {
    //    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    //    if (!emailReg.test($("#user_email").val())) {
    //        $("#Email").focus();
    //        alert("Please enter valid Email ID");
    //        return false;
    //    }
    //}
    return true;
}



function UpdateDilogBox(sid) {
    debugger;
    var data = { sid: sid }
    debugger;
    $.ajax({
        type: "POST",
        url: "../Admin/CustomerHandler.asmx/GetSingle",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            var obj = JSON.parse(response.d);
            if (obj.retCode == 1) {
                var Arr = obj.Arr;

                $('#AFname').val(Arr[0].FirstName)
                $('#ALanme').val(Arr[0].LastName)
                $('#AEmail').val(Arr[0].Email)
                $('#AMobile').val(Arr[0].Mobile)
                $('#Asel_Gender').val(Arr[0].Gender)
                $('#AAddress').val(Arr[0].Address)
                $('#ASelect_City').val(Arr[0].City)
                $('#ASelect_Country').val(Arr[0].Country)
                $('#APin').val(Arr[0].Pin)
                $('#hdn').val(sid);
                $('#Update').modal('show')
            }
            else if (obj.retCode == -1) {
                $('#SpnMessege').text("Record is not updated")
                $('#ModelMessege').modal('show')
            }

            else if (obj.retCode == 0) {
                $('#SpnMessege').text("Something Went Wrong.")
                $('#ModelMessege').modal('show')
            }
        }
    });
}


function UpdateCustomer() {
    debugger;


    var fName = $("#AFname").val()
    var LastName = $("#ALanme").val()
    var Mobile = $("#AMobile").val()
    var Address = $("#AAddress").val()
    var City = ""
    var Email = $("#AEmail").val()
    var Gender = $("#Asel_Gender").val()
    var Country = ""
    var Pin = ""
    var Pan = "";
    var sid = $("#hdn").val()


    if ($("#AFname").val() == "") {
        alert("Please enter First Name");
        $("#Fname").focus();
        return false;
    }
    else if ($("#ALanme").val() == "") {
        alert("Please enter Last Name");
        $("#Lanme").focus();
        return false;
    }
    else if ($("#AMobile").val() == "") {
        alert("Please enter Mobile No");
        $("#Mobile").focus();
        return false;
    }
    else if ($("#AAddress").val() == "") {
        alert("Please enter Address");
        $("#Address").focus();
        return false;
    }
    else if ($("#AMobile").val() == "") {
        alert("Please enter Mobile No");
        $("#Mobile").focus();
        return false;
    }



    // Email validation 
    var Email = document.getElementById("AEmail").value;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (Email != "") {
        if (emailReg.test(Email)) {

        }
        else {
            alert("Please enter valid Email ID");
            return false;
        }
    }
    else {

        alert("Please enter Email");
        $("#Email").focus();
        return false;
    }


    var param = {
        sid: sid,
        fName: fName,
        LastName: LastName,
        Email: Email,
        Mobile: Mobile,
        Gender: Gender,
        Address: Address,
        City: City,
        Country: Country,
        Pan: Pan,
        Pin: Pin
    }
    $.ajax({
        type: "POST",
        url: "../Admin/CustomerHandler.asmx/UpdateCustomer",
        data: JSON.stringify(param),
        contentType: "application/json",
        datatype: "json",
        success: function (data) {
            var obj = JSON.parse(data.d);
            if (obj.retCode == 1) {
                $('#Update').modal('hide')
                GetAllCustomer();
                $('#SpnMessege').text("Customer Updated Successfully")
                $('#ModelMessege').modal('show')

            }

            else {
                $('#SpnMessege').text("Customer not Updated")
                $('#ModelMessege').modal('show')
            }
        }
    });
}


function Delete(sid) {
    debugger;
    if (confirm("Are you sure, you want to delete this record ?") == true) {
        var data = { sid: sid }
        debugger;
        $.ajax({
            type: "POST",
            url: "../Admin/CustomerHandler.asmx/Delete",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                var obj = JSON.parse(response.d);
                if (obj.retCode == 1) {
                    GetAllCustomer();
                    $('#SpnMessege').text("Customer Deleted Successfully")
                    $('#ModelMessege').modal('show')

                }
                else if (obj.retCode == -1) {
                    $('#SpnMessege').text("Record is not Deleted")
                    $('#ModelMessege').modal('show')
                }

                else if (obj.retCode == 0) {
                    $('#SpnMessege').text("Something Went Wrong.")
                    $('#ModelMessege').modal('show')
                }
            }
        });
    }
}