﻿
$(function () {
    debugger;
    LoadReservations()
})
var Arr;
function LoadReservations() {
    $("#DriverDetails").dataTable().fnClearTable();
    $("#DriverDetails").dataTable().fnDestroy();
    debugger;
    $.ajax({
        type: "POST",
        url: "../Admin/ReservationHandler.asmx/LoadReservations",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            debugger
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {
                Arr = obj.Arr;
                Arr = Arr.reverse();
                var ul = '';

                if (Arr && Arr.length > 0) {
                    for (var i = 0; i < Arr.length; i++) {
                        ul += '<tr class="odd">';
                        ul += '<td align="center" style="width: 10%">' + (i + 1) + '</td>';
                        //ul += '<td align="center" style="width: 15%">' + Arr[i].ReservationID + '</td>';
                        ul += '<td align="center" style="cursor: pointer;width: 15%;" onclick="BookingDetails(' + i + ')"><a>' + Arr[i].ReservationID + '</a></td>';
                        ul += '<td align="center" style="width: 20%">' + Arr[i].Service + '</td>';
                        ul += '<td align="center" style="width: 10%">' + Arr[i].ReservationDate + '</td>';
                        ul += '<td align="center"style="width: 10%">' + Arr[i].TotalFare + '</td>';
                        var status = Arr[i].Status
                        if (status == "") {
                            ul += '<td align="center" style="width: 10%">Pending</td>';
                        }
                        else if (status == "Cancelled") {
                            ul += '<td align="center" style="width: 10%">Cancelled <p style="cursor: pointer; color:blue;" onclick="UndoStatus(' + Arr[i].sid + ')">Undo</p></td>';
                        }
                        else if (status == "Deleted") {
                            ul += '<td align="center" style="width: 10%">Deleted <p style="cursor: pointer; color:blue;" onclick="UndoStatus(' + Arr[i].sid + ')">Undo</p></td>';
                        }
                        else {
                            ul += '<td align="center" style="width: 10%">' + status + '</td>';
                        }

                        var Paid = Arr[i].Paid
                        if (Paid) {
                            ul += '<td align="center" style="width: 10%">Paid</td>';
                        }
                        else {
                            ul += '<td align="center" style="width: 10%"><span style="color:red"> Not paid</span></td>';
                        }

                        if (status == "Cancelled") {
                            ul += '<td align="center"><a style="cursor: pointer" onclick="Update(' + Arr[i].sid + ',\'' + Arr[i].Service + '\')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a> | <a style="cursor: pointer;color:orange" onclick="ConfirmBookingAlert()" href="#"><span class="fa fa-bars" title="Confirm"></span></a> | <a style="cursor: pointer" onclick="Cancel(' + Arr[i].sid + ')" href="#"><span class="fa fa-times" title="Cancel"></span></a> | <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="Delete(' + Arr[i].sid + ')"></span></a> | </span></a><a style="cursor: pointer" onclick="CompleteBooking(' + Arr[i].sid + ')" href="#"><span class="fa fa-check" title="Completed"></span></a></td>';
                        }
                        else {
                            ul += '<td align="center"><a style="cursor: pointer" onclick="Update(' + Arr[i].sid + ',\'' + Arr[i].Service + '\')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a> | <a style="cursor: pointer" onclick="ConfirmBooking(' + Arr[i].sid + ')" href="#"><span class="fa fa-bars" title="Confirm"></span></a> | <a style="cursor: pointer" onclick="Cancel(' + Arr[i].sid + ')" href="#"><span class="fa fa-times" title="Cancel"></span></a> | <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="Delete(' + Arr[i].sid + ')"></span></a> | <a style="cursor: pointer" onclick="CompleteBooking(' + Arr[i].sid + ')" href="#"><span class="fa fa-check" title="Completed"></span></a></td>';
                        }
                        //ul += ' ';
                        ul += '</tr>';
                    }
                    $('#DriverDetails tbody').append(ul);
                    $("#DriverDetails").dataTable({
                        "bSort": false
                    });
                    var OptionMac_Name
                    var Driver = obj.ArrDriver;
                    for (var i = 0; i < Driver.length; i++) {
                        var fullName = Driver[i].sFirstName + " " + Driver[i].sLastName;
                        OptionMac_Name += '<option value="' + Driver[i].Sid + '" >' + fullName + '</option>'
                    }
                    $("#Sel_Driver").append(OptionMac_Name);
                }
                else {
                    ul += '<tr>';
                    ul += '<td colspan="5" align="center">No Record Found </td>';
                    ul += '</tr>';
                    $('#DriverDetails').append(ul);
                }
            }
            else if (obj.Retcode == -1) {
                ul += '<tr>';
                ul += '<td colspan="8" align="center">No Record Found </td>';
                ul += '</tr>';
                $('#DriverDetails').append(ul);
            }

            else {
                $('#SpnMessege').text("Something Went Wrong!.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert(jqXHR.responseText);
    })
}

function ConfirmBooking(sid) {
    debugger
    $('#AssignDriver').modal('show')
    $('#hdn').val(sid)
}

function Assign() {
    debugger
    var BookingSid = $('#hdn').val()
    var DriverSid = $('#Sel_Driver').val()
    var DriverName = $('#Sel_Driver option:selected').text()

    var data = { BookingSid: BookingSid, DriverSid: DriverSid, DriverName: DriverName }

    $.ajax({
        type: "POST",
        url: "../Admin/ReservationHandler.asmx/AssignBooking",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {
                //$('#DriverDetails').empty();
                LoadReservations()
                $('#SpnMessege').text("Booking Confirm, an email has been sent to Customer and Driver.")
                $('#ModelMessege').modal('show')
                $('#AssignDriver').modal('hide')
            }
            else if (obj.Retcode == 4) {
                $('#SpnMessege').text("Already Confirmed Booking.")
                $('#ModelMessege').modal('show')
                $('#AssignDriver').modal('hide')
            }
            else if (obj.Retcode == 5) {
                $('#SpnMessege').text("This booking is cancelled, can not Confirmed Booking. ")
                $('#ModelMessege').modal('show')
                $('#AssignDriver').modal('hide')
            }
                //else if (obj.Retcode == 6) {
                //    $('#SpnMessege').text("This booking is Unpaid, Please paid amount. ")
                //    $('#ModelMessege').modal('show')
                //}
            else if (obj.Retcode == -1) {
                $('#SpnMessege').text("Driver Not Assigned, Something went wrong!.")
                $('#ModelMessege').modal('show')
                $('#AssignDriver').modal('hide')
            }

            else {
                $('#SpnMessege').text("Something Went Wrong!.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },


    });

}

function Cancel(sid) {
    debugger
    var data = { BookingSid: sid }
    $.ajax({
        type: "POST",
        url: "../Admin/ReservationHandler.asmx/CancelBooking",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {
                LoadReservations()
                $('#SpnMessege').text("Booking Cancelled, an email has been sent to Customer and Driver.")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == 4) {
                $('#SpnMessege').text("Already Cancelled Booking.")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == -1) {
                $('#SpnMessege').text("Something went wrong!.")
                $('#ModelMessege').modal('show')
            }

            else {
                $('#SpnMessege').text("Something Went Wrong!.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });
}

function Delete(sid) {

    debugger
    var data = { BookingSid: sid }
    $.ajax({
        type: "POST",
        url: "../Admin/ReservationHandler.asmx/DeleteBooking",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {
                //$('#DriverDetails').empty();
                LoadReservations()
                $('#SpnMessege').text("Booking Deleted")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == 4) {
                $('#SpnMessege').text("Already Cancelled Booking.")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == -1) {
                $('#SpnMessege').text("Something went wrong!.")
                $('#ModelMessege').modal('show')
            }

            else {
                $('#SpnMessege').text("Something Went Wrong!.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });

}

function Update(sid, Service) {
    debugger
    if (Service == "From Airport" || Service == "To Airport") {

        window.location.href = "AirPortReservation.aspx?sid=" + sid
    }
    else if (Service == "Point To Point Reservation") {

        window.location.href = "PointToPoint.aspx?sid=" + sid
    }
    else {
        window.location.href = "HourReservation.aspx?sid=" + sid
    }


}

function SearchReservation() {
    $("#DriverDetails").dataTable().fnClearTable();
    $("#DriverDetails").dataTable().fnDestroy();
    var From = $('#datepicker1').val();
    var To = $('#datepicker2').val();
    var Status = $('#Sel_Status option:selected').val();
    //var DriverName = $("#sel_driver option:selected").text();

    if (From == '' && To != '') {
        alert("Please enter from date");
        return false;
    }
    else if (To == '' && From != '') {
        alert("Please enter to date");
    }
    if (From == '' && To == '' && Status == '0') {
        alert("Please Enter Date or Status to search")
        return false;
    }
    var data = { From: From, To: To, Status: Status }

    $.ajax({
        type: "POST",
        url: "../Admin/ReservationHandler.asmx/SearchReservation",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            debugger
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                $('#datepicker1').val('');
                $('#datepicker2').val('');
                $('#Sel_Status').val(0);
                Arr = obj.Arr;
                var ul = '';
                //$('#DriverDetails tbody').empty();
                if (Arr && Arr.length > 0) {
                    for (var i = 0; i < Arr.length; i++) {
                        //alert(Arr[i].ReservationDate);
                        ul += '<tr class="odd">';
                        ul += '<td align="center" style="width: 10%">' + (i + 1) + '</td>';
                        ul += '<td align="center" style="cursor: pointer;width: 15%;" onclick="BookingDetails(' + i + ')"><a>' + Arr[i].ReservationID + '</a></td>';
                        ul += '<td align="center" style="width: 20%">' + Arr[i].Service + '</td>';
                        ul += '<td align="center" style="width: 10%">' + Arr[i].ReservationDate + '</td>';
                        ul += '<td align="center"style="width: 10%">' + Arr[i].TotalFare + '</td>';
                        var status = Arr[i].Status
                        if (status == "") {
                            ul += '<td align="center" style="width: 10%">Pending</td>';
                        }
                        else if (status == "Cancelled") {
                            ul += '<td align="center" style="width: 10%">Cancelled <p style="cursor: pointer; color:blue;" onclick="UndoStatus(' + Arr[i].sid + ')">Undo</p></td>';
                        }
                        else if (status == "Deleted") {
                            ul += '<td align="center" style="width: 10%">Deleted <p style="cursor: pointer; color:blue;" onclick="UndoStatus(' + Arr[i].sid + ')">Undo</p></td>';
                        }
                        else {
                            ul += '<td align="center" style="width: 10%">' + status + '</td>';
                        }

                        var Paid = Arr[i].Paid
                        if (Paid) {
                            ul += '<td align="center" style="width: 10%">Paid</td>';
                        }
                        else {
                            ul += '<td align="center" style="width: 10%"><span style="color:red"> Not paid</span></td>';
                        }

                        if (status == "Cancelled") {
                            ul += '<td align="center"><a style="cursor: pointer" onclick="Update(' + Arr[i].sid + ',\'' + Arr[i].Service + '\')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a> | <a style="cursor: pointer;color:orange" onclick="ConfirmBookingAlert()" href="#"><span class="fa fa-check" title="Confirm"></span></a> | <a style="cursor: pointer" onclick="Cancel(' + Arr[i].sid + ')" href="#"><span class="fa fa-times" title="Cancel"></span></a> | <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="Delete(' + Arr[i].sid + ')"></span></a></td>';
                        }
                        else {
                            ul += '<td align="center"><a style="cursor: pointer" onclick="Update(' + Arr[i].sid + ',\'' + Arr[i].Service + '\')" href="#"><span class="glyphicon glyphicon-edit" title="Edit"></span></a> | <a style="cursor: pointer" onclick="ConfirmBooking(' + Arr[i].sid + ')" href="#"><span class="fa fa-check" title="Confirm"></span></a> | <a style="cursor: pointer" onclick="Cancel(' + Arr[i].sid + ')" href="#"><span class="fa fa-times" title="Cancel"></span></a> | <a style="cursor: pointer" href="#"><span class="glyphicon glyphicon-trash" title="Delete" aria-hidden="true" style="cursor: pointer" onclick="Delete(' + Arr[i].sid + ')"></span></a></td>';
                        }
                        ul += '</tr>';
                    }
                    $('#DriverDetails tbody').append(ul);
                    $("#DriverDetails").dataTable({
                        "bSort": false
                    });
                    var OptionMac_Name
                    var Driver = obj.ArrDriver;
                    for (var i = 0; i < Driver.length; i++) {
                        var fullName = Driver[i].sFirstName + " " + Driver[i].sLastName;
                        OptionMac_Name += '<option value="' + Driver[i].Sid + '" >' + fullName + '</option>'
                    }
                    $("#Sel_Driver").append(OptionMac_Name);
                }
                else {
                    $('#SpnMessege').text("No Record Found")
                    $('#ModelMessege').modal('show')
                    //l += '<tr>';
                    //l += '<td colspan="8" align="center">No Record Found </td>';
                    //l += '</tr>';
                    //('#DriverDetails').append(ul);
                }
            }
            else if (obj.retCode == -1) {
                $('#SpnMessege').text("No Record Found")
                $('#ModelMessege').modal('show')
            }

            else {
                $('#SpnMessege').text("Something Went Wrong!.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });
}

function UndoStatus(sid) {
    debugger
    var data = { BookingSid: sid }
    $.ajax({
        type: "POST",
        url: "../Admin/ReservationHandler.asmx/UndoStatus",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {
            var result = (typeof response.d) == 'string' ? eval('(' + response.d + ')') : response.d;
            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {
                LoadReservations()
                $('#SpnMessege').text("Status of Booking is changed to Requested")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == -1) {
                $('#SpnMessege').text("Something went wrong!.")
                $('#ModelMessege').modal('show')
            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });
}

function BookingDetails(ObjNo) {
    var Name = Arr[ObjNo].FirstName + " " + Arr[ObjNo].LastName;
    var ResService = Arr[ObjNo].Service;

    $("#BookingDetails").modal("show");
    $("#BookingNo").text(Arr[ObjNo].ReservationID);
    $("#ResDate").text(Arr[ObjNo].ReservationDate);
    $("#Source").text(Arr[ObjNo].Source);
    $("#Destination").text(Arr[ObjNo].Destination);
    $("#AssignedTo").text(Arr[ObjNo].AssignedTo);
    $("#Name").text(Name);
    $("#Service").text(Arr[ObjNo].Service);
    $("#TotalFare").text("$ " + Arr[ObjNo].TotalFare); Passenger
    $("#Passenger").text(Arr[ObjNo].Persons);
    $("#PhoneNo").text(Arr[ObjNo].ContactNumber);
    if (ResService == "From Airport") {
        $("#ResTime").text(Arr[ObjNo].FlightTime);
    }
    else if (ResService == "To Airport") {
        $("#ResTime").text(Arr[ObjNo].Pickup_Time);
    }
    else {
        $("#ResTime").text(Arr[ObjNo].P2PTime);
    }
}

function ConfirmBookingAlert() {
    $('#SpnMessege').text("Cancel booking can not confirmed");
    $('#ModelMessege').modal('show');
}

function CompleteBooking(Sid) {
    debugger
    var data = { BookingSid: Sid }
    $.ajax({
        type: "POST",
        url: "../Admin/ReservationHandler.asmx/CompleteBooking",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.Retcode == 1) {
                LoadReservations()
                $('#SpnMessege').text("Booking Completed")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == 2) {
                $('#SpnMessege').text("Booking is Already Completed")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == 3) {
                $('#SpnMessege').text("Please Assign Driver First")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == 4) {
                $('#SpnMessege').text("Please Pay Amount First")
                $('#ModelMessege').modal('show')
            }
            else if (obj.Retcode == 5) {
                $('#SpnMessege').text("Booking Can Not Complete Before Service Provided")
                $('#ModelMessege').modal('show')
            }
            else {
                $('#SpnMessege').text("Something Went Wrong!.")
                $('#ModelMessege').modal('show')
            }

        },
        error: function () {
            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });
}
