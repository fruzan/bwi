﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dispatcher/DispatchMaster.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="CUT.Dispatcher.ChangePassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript" src="../Admin/Scripts/ChangePassword.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <br />
    <br />
    <%--<br />--%>
    <div class="container">
        <div class="row">

            <div class="col-md-12">


       
                <br />
       
                <div class="fblueline">
                    Setting
           
                    <span class="farrow"></span>
                    <a style="color: white" href="ChangePassword.aspx"><b>Change password</b></a><br />
                </div>
                <div class="tab-content55">
               
                    <div class="tab-pane" id="password">
                        <br />
                        <br />
                
                        <div class="row">
                            <div class="col-md-4">
                                Current Password<br />
                                <input type="password" id="txtOldPassword" class="form-control " placeholder="" data-placement="top" data-trigger="focus" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_OldPassword">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="col-md-4">
                                New Password<br />
                                <input type="password" id="txtNewPassword" class="form-control " placeholder="" data-placement="top" data-trigger="focus" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_NewPassword">
                                    <b>* This field is required</b></label>
                            </div>
                            <div class="col-md-4">
                                Confirm New Password<br />
                                <input type="password" id="txtConfirmNewPassword" class="form-control " placeholder="" data-placement="top" data-trigger="focus" />
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_ConfirmNewPassword">
                                    <b>* This field is required</b></label>
                            </div>
                        </div>
                        <input type="hidden" id="hdnpassword" />
                        <table align="right">
                            <tr>
                                <td>
                                    <button type="submit" id="btnSubmit" class="btn-search right" onclick="ChangePassWord()">Change</button></td>
                            </tr>
                        </table>
                        <br />
                        <br />
     
                    </div>
                </div>
         
            </div>
        </div>
    </div>
</asp:Content>
