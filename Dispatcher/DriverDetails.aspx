﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dispatcher/DispatchMaster.Master" AutoEventWireup="true" CodeBehind="DriverDetails.aspx.cs" Inherits="CUT.Dispatcher.DriverDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script type="text/javascript" src="Scripts/Driver.js"></script>
    <script type="text/javascript" src="../Admin/Scripts/DialogBox.js"></script>

    <style type="text/css">
        .ScrollDiv {
            overflow-y: auto;
            height: 400px;
        }

        .ScrollDiv2 {
        }

        /*For image preview code*/
        .drop-area {
            width: 100px;
            height: 25px;
            border: 1px solid #999;
            text-align: center;
            padding: 10px;
            cursor: pointer;
        }

        #thumbnail img {
            width: 200px;
            height: 200px;
            margin: 5px;
        }

        canvas {
            border: 1px solid red;
        }
        /*end image preview code*/
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <br />
    <div class="offset-2">
        <div id="alSuccess" class="alert alert-success alert-dismissible" role="alert" style="display: none">
            <span id="spnMsg">Staff status has been changed successfully!</span>
            <button type="button" class="close" onclick="hidealert()"><span aria-hidden="true">×</span></button>
        </div>
        <div id="alError" class="alert alert-danger alert-dismissible" role="alert" style="display: none">
            Something went wrong while processing your request! Please try again.
                   
            <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="hidealert()"><span aria-hidden="true">×</span></button>
        </div>
    </div>
    <table align="right" style="margin-right: 1.8%">
        <tbody>
            <tr>
                <td colspan="6"></td>

                <td style="padding-top: 3.2%"><a href="#">
                    <input type="button" class="popularbtn right" onclick="AddDialogBox()" style="margin-right: 3.8%" value="+ Add New" title="Add New Driver">
                </a></td>   
            </tr>
        </tbody>
    </table>
    <br />
    <br />
    <br />
    <div class="offset-2">
        <div class="fblueline">
            Manage
           
            <span class="farrow"></span>
            <a style="color: white" href="DriverDetails.aspx" title="Driver Details"><b>Driver Details</b></a>

        </div>
        <div class="frow1">

            <br />
            <br />

            <div class="table-responsive">
                <div id="tbl_StaffDetails_wrapper" class="dataTables_wrapper" role="grid">

                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered dataTable" id="tbl_StaffDetails" style="width: 100%;">
                        <thead>
                            <tr>
                                <td align="center"><b>Unique Code</b>
                                </td>
                                <td align="center"><b>Name</b>
                                </td>
                                <td align="center"><b>Address</b>
                                </td>
                                <td align="center"><b>Email</b>
                                </td>

                                <td align="center"><b>Edit |  License |  Delete</b>
                                </td>
                            </tr>
                        </thead>

                        <tbody role="alert" aria-live="polite" aria-relevant="all" id="DriverDetails">
                        </tbody>
                    </table>

                </div>
            </div>
            <br />
        </div>
    </div>

    <div class="modal fade" id="AddUpdateDialogBox" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel2" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Add Driver</b></div>
                        <div class="frow2">
                            <input type="hidden" id="hdn_id" value="0" />
                            <input type="hidden" id="hdn_ProfileMapPath" value="0" />
                            <input type="hidden" id="hdn_License1MapPath" value="0" />
                            <input type="hidden" id="hdn_License2MapPath" value="0" />
                            <div id="scrollDiv" class="ScrollDiv">
                                <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <label>
                                                    First Name *</label>
                                                <input id="Fname" type="text" class=" form-control" />

                                            </td>
                                            <td>
                                                <label>
                                                    Last Name *</label>
                                                <input id="Lname" type="text" class=" form-control" />

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Gender *</label><br />
                                                <input id="Male" name="gender" type="radio" checked="checked" /><label>&nbsp;Male</label>
                                                <input id="Female" name="gender" type="radio" /><label>&nbsp;Female</label>

                                            </td>
                                            <td>
                                                <label>Mobile No. *</label>
                                                <input id="Mobile" type="text" class=" form-control" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Address *</label>
                                                <input id="Address" type="text" class=" form-control" />
                                            </td>
                                            <td>
                                                <label>Percentage *</label>
                                                <select id="Select_Percentage" class="form-control">
                                                    <option value="0" selected="selected">Select </option>
                                                    <option value="10">10 </option>
                                                    <option value="15">15 </option>
                                                    <option value="20">20 </option>
                                                    <option value="25">25 </option>
                                                    <option value="30">30 </option>
                                                    <option value="40">40 </option>
                                                    <option value="50">50 </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <%--                                        <tr>
                                            <td>
                                                <label>Country *</label>
                                                <input id="Country" type="text" class=" form-control" />
                                            </td>
                                            <td>
                                                <label>City *</label>
                                                <input id="City" type="text" class=" form-control" />
                                            
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <%--     <td>
                                                <label>Pin Code</label>
                                                <input id="Pin" type="text" class=" form-control" />
                                            </td>--%>
                                            <td colspan="2">
                                                <label>Email *</label>
                                                <input id="Email" type="text" class=" form-control" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table style="width: 100%">
                                    <tbody>
                                        <tr>
                                            <td style="width: 33%">

                                                <center>
                                                    <%--                                                    <input type="file" onselect="test()" id="upload-image1" value="Upload Profile" /></center>
                                                <div id="upload1" style="display: none">
                                                    Upload File
                                                </div>
                                                <div id="thumbnail1"></div>--%>
                                                    <label>Upload Profile</label>
                                                    <input id="Profile" type="file" />
                                                    <hr />
                                                    <b>Live Preview</b>
                                                    <br />
                                                    <br />
                                                    <div id="dvPreview">
                                                    </div>
                                                    <%--<input type="file" id="Profile" onchange="SendProfile()" style="display: none;" />--%>
                                                    <%--                                                <center>
                                                    <input type="button" onclick="$('#Profile').trigger('click');" class="btn-search" value="Upload Profile" /></center>
                                                <input type="file" id="Profile" onchange="SendProfile()" style="display: none;" />--%>
                                            </td>
                                            <td style="width: 33%">
                                                <label>Upload License</label>
                                                <input id="LicenceUpload1" type="file" />
                                                <hr />
                                                <b>Live Preview</b>
                                                <br />
                                                <br />
                                                <div id="dvPreview2">
                                                </div>
                                                <%--                                                <center>
                                                    <input type="button" onclick="$('#LicenceUpload1').trigger('click');" class="btn-search" value="Upload License" /></center>
                                                <input type="file" id="LicenceUpload1" onchange="SendLic1()" style="display: none;" />--%></td>
                                            <td style="width: 33%">
                                                <label>Upload Document</label>
                                                <input id="LicenceUpload2" type="file" />
                                                <hr />
                                                <b>Live Preview</b>
                                                <br />
                                                <br />
                                                <div id="dvPreview3">
                                                </div>
                                                <%--                                                <center>
                                                    <input type="button" onclick="$('#LicenceUpload2').trigger('click');" class="btn-search" value="Upload Document" /></center>
                                                <input type="file" id="LicenceUpload2" onchange="SendLic2()" style="display: none;" />--%></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br />

                                                <center>
                                                    <%--<input type="file" style="display: none" id="upload-image" />--%>
                                                    <%--                                                    <div id="upload" class="drop-area">
                                                        Upload File
                                                    </div>--%>
                                                    <%--<div id="thumbnail"></div>--%>

                                                    <a id="hrefImgProfile" target="_blank">
                                                        <img id="ImgProfile" /></a></center>
                                            </td>
                                            <td>
                                                <br />

                                                <center>
                                                    <a id="hrefImgLicenceUpload1" target="_blank">
                                                        <img id="ImgLicenceUpload1" /></a></center>

                                            </td>
                                            <td>
                                                <br />

                                                <center>
                                                    <a id="hrefImgLicenceUpload2" target="_blank">
                                                        <img id="ImgLicenceUpload2" /></a></center>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>



                                <table class="table table">
                                    <tr>
                                        <td>
                                            <input type="button" onclick="AddUpdateDriver()" class="btn-search" value="Add" title="Add/Update Driver" id="btn_RegisterDriver" /></td>
                                    </tr>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade" id="LicenseDilogBox" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">

        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">License</b></div>
                        <div class="frow2">
                            <div id="scrollDivDoc" class="ScrollDiv">

                                <div id="Doc1" style="width: 300px; height: 300px">
                                </div>
                                <br/>
                                <div id="Doc2" style="width: 300px; height: 300px">
                                </div>
                                <br/>
                                <div id="Doc3" style="width: 300px; height: 300px">
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</asp:Content>
