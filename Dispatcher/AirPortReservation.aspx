﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Dispatcher/DispatchMaster.Master" AutoEventWireup="true" CodeBehind="AirPortReservation.aspx.cs" Inherits="CUT.Dispatcher.AirPortReservation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyBnIPCXTY_ul30N9GMmcSmJPLjPEYzGI7c"></script>
    <script type="text/javascript">

        $(function () {
            var dateToday = new Date();
            $("#txt_Date").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "mm-dd-yy",
                minDate: dateToday
            });

            $("#txt_FlightDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "mm-dd-yy",
                minDate: dateToday
            });
            
            VehicleNameDropDown()
            GetAllDriver();
            GetAllEmail();
            GetAllPhoneNo();
            GetAirLines();
            GetServices();
        })

        var directionsDisplay;
        var directionsService = new google.maps.DirectionsService();

        google.maps.event.addDomListener(window, 'load', function () {
            debugger;


            new google.maps.places.SearchBox(document.getElementById('txt_New_Location'));
            directionsDisplay = new google.maps.DirectionsRenderer({ 'draggable': true });

            var places1 = new google.maps.places.Autocomplete(document.getElementById('txt_New_Location'));
            google.maps.event.addListener(places1, 'place_changed', function () {

                debugger;
                var place = places1.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();

                dLat = latitude;
                dLong = longitude;
                GetRoute1();
            });
        });

        google.maps.event.addDomListener(window, 'load', function () {
            debugger;

            new google.maps.places.SearchBox(document.getElementById('txt_Address'));
            directionsDisplay = new google.maps.DirectionsRenderer({ 'draggable': true });

            var places = new google.maps.places.Autocomplete(document.getElementById('txt_Address'));
            google.maps.event.addListener(places, 'place_changed', function () {

                debugger;
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();

                dLat = latitude;
                dLong = longitude;
                GetRoute();
            });
        });

        function GetRoute() {

            debugger;
            var mumbai = new google.maps.LatLng(18.9750, 72.8258);
            var mapOptions = {
                zoom: 7,
                center: mumbai
            };
            map = new google.maps.Map(document.getElementById('dvMap'), mapOptions);
            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById('dvPanel'));

            //*********DIRECTIONS AND ROUTE**********************//
            var asfd = document.getElementById("Select_Service").value;
            if (document.getElementById("Select_Service").value == 'From Airport') {

                source = document.getElementById("Select_Location").value;
                destination = document.getElementById("txt_Address").value;

                //source = $("#Select_Location option:selected").value;
                //destination = document.getElementById("txt_Address").value;
            }
            else {
                source = document.getElementById("txt_Address").value;
                destination = document.getElementById("Select_Location").value;
                //source = document.getElementById("txt_Address").value;
                //destination = $("#Select_Location option:selected").value;
            }

            var request = {
                origin: source,
                destination: destination,
                travelMode: google.maps.TravelMode.DRIVING
            };
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                }
            });

            //*********DISTANCE AND DURATION**********************//
            var service = new google.maps.DistanceMatrixService();
            service.getDistanceMatrix({
                origins: [source],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function (response, status) {
                if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
                    var distance = response.rows[0].elements[0].distance.text;
                    var duration = response.rows[0].elements[0].duration.text;
                    TimeTaken = duration;
                    TotalDistance = distance;
                    TotalDistance = (parseFloat(TotalDistance) * 0.621371).toFixed(2)

                    $('#PopUpDistance').text(TotalDistance)
                    $('#PopUpTime').text(TimeTaken)
                    var Cat = document.getElementById("txt_Address").value;
                    var BigCat = $("#Select_Location option:selected").text();
                    setTimeout(function () {
                        $("#Sel_AirPortNamePopUp option").filter(function () {
                            return $(this).text() == BigCat;
                        }).prop("selected", true);
                    }, 1000);
                    $('#txt_New_Location').val(Cat)
                    //alert(distance)
                } else {
                    alert("Unable to find the distance via road.");
                }
            });
        }

        function GetRoute1() {

            debugger;
            var mumbai = new google.maps.LatLng(18.9750, 72.8258);
            var mapOptions = {
                zoom: 7,
                center: mumbai
            };
            map = new google.maps.Map(document.getElementById('dvMap'), mapOptions);
            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById('dvPanel'));

            //*********DIRECTIONS AND ROUTE**********************//
            if (document.getElementById("ServicePopUp").value == 'FromAirPort') {
                source = $("#Sel_AirPortNamePopUp option:selected").text();
                destination = document.getElementById("txt_New_Location").value;
            }
            else {
                source = document.getElementById("txt_New_Location").value;
                destination = $("#Sel_AirPortNamePopUp option:selected").text();
            }

            var request = {
                origin: source,
                destination: destination,
                travelMode: google.maps.TravelMode.DRIVING
            };
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                }
            });

            //*********DISTANCE AND DURATION**********************//
            var service = new google.maps.DistanceMatrixService();
            service.getDistanceMatrix({
                origins: [source],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function (response, status) {
                if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
                    var distance = response.rows[0].elements[0].distance.text;
                    var duration = response.rows[0].elements[0].duration.text;
                    TimeTaken = duration;
                    TotalDistance = distance;
                    TotalDistance = (parseFloat(TotalDistance) * 0.621371).toFixed(2)
                    $('#PopUpDistance').text(TotalDistance)
                    $('#PopUpTime').text(TimeTaken)
                    debugger;
                    var Cat = document.getElementById("txt_New_Location").value;
                    var BigCat = $("#Sel_AirPortNamePopUp option:selected").text();
                    setTimeout(function () {
                        $("#Select_Location option").filter(function () {
                            return $(this).text() == BigCat;
                        }).prop("selected", true);
                    }, 1000);
                    $('#txt_Address').val(Cat)
                } else {
                    alert("Unable to find the distance via road.");
                }
            });
        }

    </script>

    <style type="text/css">
        label {
            float: left;
            width: auto;
            padding: 10px 10px 10px 10px;
            text-align: center;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 14px;
            line-height: 1.428571429;
            color: #333333;
            background-color: #ffffff;
            font-weight: 400;
        }
    </style>
    <script src="Scripts/AirPortReservationJS.js"></script>
    <!-- cdn for modernizr, if you haven't included it already -->
    <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
    <!-- polyfiller file to detect and load polyfills -->
    <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
    <script>
        webshims.setOptions('waitReady', false);
        webshims.setOptions('forms-ext', { types: 'date' });
        webshims.polyfill('forms forms-ext');
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <br />
    <br />
    <br />
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <input type="button" value="Get Map" class="btn-search margtop-2"
                    style="width: 50%; float: right" onclick="ShowModel();" title="Submit Details" />
            </div>
        </div>
        <br />
        <div class="row">

            <div class="col-md-12">
                <div class="fblueline">
                    Reservation
           
                    <span class="farrow"></span>
                    <a style="color: white" href="AirPortReservation.aspx" title="AirPort Reservation">AirPort Reservation</a>

                    <br />
                </div>

                <div class="tab-content55">
                    <div class="col-md-12">

                        <div class="tab-pane padding40 active" id="profile">

                            <div class="row">
                               <div class="col-md-4">
                                    <br />
                                    Email :
                                    <input id="txt_Email" class="form-control"  list="Select_Email"/>
                                    <datalist id="Select_Email"></datalist>      
                                   <%-- <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Email" placeholder="Email" class="form-control" />--%>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Email">
                                        <b>* This field is required</b></label>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    Contact Number :
                                        <input id="txt_ContactNumber" class="form-control"  list="Select_ContactNumber"/>
                                    <datalist id="Select_ContactNumber"></datalist>
                                    <%--<input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_ContactNumber" placeholder="Contact Number" class="form-control" />--%>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_ContactNumber">
                                        <b>* This field is required</b></label>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    First Name :
                                       
                                    <input type="text" name="Text[]" id="txt_FirstName" placeholder="First Name" class="form-control" readonly="readonly"/>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_FirstName">
                                        <b>* This field is required</b></label>
                                </div>
                                
                            </div>

                            <br />
                            <div class="row">
                                
                                <div class="col-md-4">
                                    <br />
                                    Last Name :
                                       
                                    <input type="text" name="Text[]" id="txt_LastName" placeholder="Last Name" class="form-control"  readonly="readonly"/>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_LastName">
                                        <b>* This field is required</b></label>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    Reservation Date :
                                       <input type="text" name="Text[]" class="form-control datepicker" id="txt_Date" style="cursor: pointer; background: url('../../images/calendar.png') #fff no-repeat right;" placeholder="mm-dd-yyyy">
                                    <%--<input type="date" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Date" placeholder="Reservation Date" class="form-control" />--%>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Date">
                                        <b>* This field is required</b></label>
                                </div>
                               <%-- <div class="col-md-4">
                                    <br />
                                    Reservation Time :
                                 <input type="time" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Time" placeholder="Address" class="form-control" />   
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Time">
                                        <b>* This field is required</b></label>
                                </div>--%>
                                <div class="col-md-4">
                                    <br />
                                    Service  :
                                       
                             <select id="Select_Service" name="Drop[]" class="form-control">

                                 <option value="From Airport">From Airport</option>
                                 <option value="To Airport">To Airport</option>
                             </select>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Service">
                                        <b>* This field is required</b></label>
                                </div>
                                
                            </div>

                            <div class="row">

                                <div class="col-md-4">
                                    
                                    <label id="Time">Flight Arrival Time:</label>
                                 <input type="time" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_FlightTime" placeholder="Address" class="form-control" />   
                                <label style="color: red; margin-top: 3px; display: none" id="lbl_FlightTime">
                                        <b>* This field is required</b></label>
                                </div>

                                 <div class="col-md-4">
                                    <br />
                                    
                                    Airport Name :
                                    <select id="Select_Location" class="form-control">
                                        <option selected="" value="-">Select Location</option>
                                    </select>

                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Location">
                                        <b>* This field is required</b></label>
                                </div>

                                <div class="col-md-4">
                                    <br />
                                    Passenger :
                                      
                                    <input type="number" name="Text[]" id="txt_Adult" min="1" max="50" class="form-control"/>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Adult">
                                        <b>* This field is required</b></label>
                                </div>
                                
                               <%-- <div class="col-md-2">
                                    <br />
                                    Adult :
                                       
                                 <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Adult" placeholder="Adult" class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Adult">
                                        <b>* This field is required</b></label>
                                </div>

                                <div class="col-md-2">
                                    <br />
                                    Child :
                                       
                              <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Child" placeholder="Child" class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Country">
                                        <b>* This field is required</b></label>
                                </div>--%>

                                
                            </div>
                            <br />

                            <div class="row">

                                 <div class="col-md-4">
                                    <br />
                                    Airlines / Flight :
                                   <input id="Select_Airline" class="form-control"  list="Select_Airlines"/>
                                    <datalist id="Select_Airlines"></datalist>   

                                 <%-- <select id="Select_Airlines" name="Drop[]" class="form-control">
                                      <option selected="selected" value="-">Select</option>

                                  </select>--%>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Airlines">
                                        <b>* This field is required</b></label>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    Flight Number :
                                    
                               <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_FlightNumber" class="form-control" />

                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_FlightNumber">
                                        <b>* This field is required</b></label>
                                </div>
                                
                                <%--<div class="col-md-4">
                                    <br />
                                    Date :
                                    <input type="text" name="Text[]" class="form-control datepicker" id="txt_FlightDate" style="cursor: pointer; background: url('../../images/calendar.png') #fff no-repeat right;" placeholder="mm-dd-yyyy">
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_FlightDate">
                                        <b>* This field is required</b></label>
                                </div>--%>
                                 <div class="col-md-4">
                                    <br />
                                    CC Last 4 :
                                       
                                    <input type="text" id="txt_Last4" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder=" CC Last 4 " class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Last4">
                                        <b>* This field is required</b></label>
                                </div>

                                <%--<div class="col-md-4">
                                    <br />
                                    CC/Type :
                                       
                              <select name="Drop[]" size="1" id="Sel_CC" name="CCType" class="form-control">
                                  <option selected="" value="-">Select CC Type</option>
                                  <option value="American Express">Amer Express</option>
                                  <option value="Discover Club">Discover Club</option>
                                  <option value="Master Card">Master Card</option>
                                  <option value="Visa">Visa</option>
                              </select>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_CC">
                                        <b>* This field is required</b></label>
                                </div>--%>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <br />
                                    Flight Type :
                                      
                                    <select id="Sel_TravelType" name="Drop[]" class="form-control">
                                        <option selected="selected" value="Domestic">Domestic</option>
                                        <option value="International">International</option>

                                    </select>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_PinCode">
                                        <b>* This field is required</b></label>
                                </div>
                            </div>
                            <br />

                            <div class="row">
                                <br />
                                <div align="center">
                                    <center><span id="Spn_Location"><b>Drop Location</b></span></center>
                                </div>

                                <div class="col-md-12">
                                    <br />
                                    Address:
                                       
                             <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Address" placeholder="Address" class="form-control" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Address">
                                        <b>* This field is required</b></label>
                                </div>
                            </div>
                            <br />

                            <div class="row">

                                <div class="col-md-4">
                                    <br />
                                    Vehicle Type :
                                      
                                  <select name="Drop[]" id="ddl_VehicleType" class="form-control">
                                  </select>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Vehical">
                                        <b>* This field is required</b></label>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    Assigned To :
                                   
                                    <select id="txt_Assigned" name="Drop[]" class="form-control">
                                        <option value="0">Select Driver</option>
                                    </select>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Assigned">
                                        <b>* This field is required</b></label>
                                </div>

                            </div>
                            <br />

                            <div class="row">
                                <div class="col-md-12">
                                    <center><span><b>Note: We charge $10 extra for late night and early morning pick up Time 11 pm to 5 am</b></span></center>
                                </div>
                            </div>


                            <div class="row">

                                <div class="col-md-4">
                                    <br />


                                    <div style="width: 100%">
                                        <div>

                                            <label onclick="qwerty()">
                                                <input id="Chk_Paid" type="radio" name="rButton" /><span>&nbsp Paid</span></label>
                                        </div>
                                        <div style="display:none">
                                            <label>
                                                <input id="Chk_Completed" type="checkbox" /><span>&nbsp Completed</span></label>
                                        </div>

                                        <div>
                                            <label>
                                                <input id="Chk_Collect" type="radio" name="rButton" /><span>&nbsp Collect</span></label>
                                        </div>
                                        <div>
                                            <label>
                                                <input id="Chk_CCPayment" type="checkbox" /><span>&nbsp CC Payment</span></label>
                                        </div>
                                        <br />
                                        <div>
                                            <label>
                                                <input id="Chk_ChildCarSheet" type="checkbox" /><span>&nbsp Child Car Seat</span></label>
                                        </div>

                                        <div>
                                            <label>
                                                <input id="Chk_Meet" type="checkbox" /><span>&nbsp Meet & Greet</span></label>
                                        </div>
                                        <div>
                                            <label>
                                                <input id="Chk_Special" type="checkbox" /><span>&nbsp Special assistant </span>
                                            </label>
                                        </div>
                                        <div>
                                            <label>
                                                <input id="Chk_rub" type="checkbox" /><span>&nbsp Curb side pick up  </span>
                                            </label>
                                        </div>

                                       <%-- <div>
                                            <label>
                                                <input id="Chk_Baggageclaim" type="checkbox" /><span>&nbsp Baggage claim pickup  </span>
                                            </label>
                                        </div>--%>

                                        <div>
                                            <label>
                                                <input id="Chk_PetInCage" type="checkbox" /><span>&nbsp Pet in cage  </span>
                                            </label>
                                        </div>

                                    </div>
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_Reuired">
                                        <b>* This field is required</b></label>
                                </div>
                                <div class="col-md-5">
                                    <br />
                                    <table>
                                        <tr>
                                            <td>Fare</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <input type="text" id="txt_Fare" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder="Fare" class="form-control" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Fare">
                                                    <b>* This field is required</b></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gratuity</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <%--<input id="txt_Gratuity" type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder="Gratuity" class="form-control" />--%>
                                                <select class="form-control" name="Drop[]" id="txt_Gratuity">
                                                    <option value="-" selected="selected">Select Gratuity</option>
                                                    <option value="0">0 %</option>
                                                    <option value="10">10 %</option>
                                                    <option value="15">15 %</option>
                                                    <option value="20">20 %</option>
                                                    <option value="25">25 %</option>
                                                    <option value="30">30 %</option>
                                                </select>
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Gratuity">
                                                    <b>* This field is required</b></label>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Parking</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <input type="text" id="txt_Parking" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder="Parking" class="form-control" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Parking">
                                                    <b>* This field is required</b></label>
                                            </td>
                                        </tr>
                                       <%-- <tr>
                                            <td>Special Services</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <input type="text" id="txt_SpecialService" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder="Special Service" class="form-control" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_SpecialService">
                                                    <b>* This field is required</b></label>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td>Late Night</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <input id="txt_LateNight" type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder="Late Night" class="form-control" readonly="readonly"/>
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_LateNight">
                                                    <b>* This field is required</b></label</td>
                                        </tr>
                                        <tr>
                                            <td>Total Fare</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <input id="txt_Total" type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder="Total Fare" class="form-control" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Total">
                                                    <b>* This field is required</b></label</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <br />
                                    <table style="width: 100%">
                                        <tr>
                                            <td>Toll</td>
                                            <td>&nbsp:&nbsp</td>
                                            <td>
                                                <input id="txt_Toll" type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" placeholder="Toll" class="form-control" />
                                                <label style="color: red; margin-top: 3px; display: none" id="lbl_Toll">
                                                    <b>* This field is required</b></label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <br />

                            <div align="center">
                                <center><span><b>If Address Changes, enter information in comment section</b></span></center>
                            </div>
                            <br />

                            <div>Remarks</div>
                            <div align="center">

                                <center>
                                    <textarea id="txt_AreaRemarks" name="Text[]" rows="4" cols="50" name="comment" class="form-control" placeholder="Remarks"></textarea></center>
                            </div>

                        </div>
                        <br />
                        <table align="right">
                            <tr>
                                <td>
                                    <input id="btn_RegiterAgent" type="button" value="Add" class="btn-search margtop-2"
                                        style="width: 100%;" onclick="Submit();" title="Submit Details" />
                                </td>
                                <td>
                                    <input type="button" value="Cancel" class="btn-search margtop-2" onclick="window.location.href = 'DashBoard.aspx'"
                                        style="width: 100%;" />
                                </td>
                            </tr>
                        </table>

                        <br />
                    </div>
                    <input type="hidden" id="hdn_Status" value="Pending" />

                </div>

            </div>
        </div>

    </div>

    <div class="modal fade" id="ModelRate" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%;">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true" onclick="cleartextboxesHotelBeds()">&times;</span></button>
                    <h4 class="modal-title" id="gridSystemModalLabel" style="text-align: left"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div style="background: #ff9900; color: #fff; padding: 3px 10px 3px 10px;"><b id="Supplier">Rate Set Up</b></div>
                        <div class="frow2">
                            <table id="tblForms1" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>Service :</label>
                                            <select id="ServicePopUp" class="form-control">
                                                <option value="From Airport">From Airport</option>
                                                <option value="To Airport">To Airport</option>
                                            </select>
                                        </td>

                                        <td>
                                            <label>AirPortName</label>
                                            <select id="Sel_AirPortNamePopUp" onchange="MapAirPortName_Change()" class="form-control">
                                                <option selected="" value="-">Select Location</option>
                                            </select>
                                        </td>
                                        <td>
                                            <label id="New_Pop_Up_Location">Drop Location :</label>
                                            <input id="txt_New_Location" type="text" class="form-control" />

                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <div id="dateTimeDiv">
                                                <div align="center"><b>Distance :</b><span id="PopUpDistance"></span>&nbsp <b>Time :<b /><span id="PopUpTime"></span></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <div id="dvMap" style="width: auto; height: 300px"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">

                                            <div align="right">
                                                <input type="button" class="btn-search" value="Save" onclick="HideMessage()" />
                                                <input type="button" class="btn-search" value="Cancel" onclick="$('#ModelRate').modal('hide')" />
                                            </div>
                                        </td>
                                    </tr>

                                    <input type="text" id="txt_Account" name="Text[]" placeholder=" Account Number " class="form-control" style="display:none" />
                                    <label style="color: red; margin-top: 3px; display: none" id="lbl_AccountNumber">
                                        <input type="text" name="Text[]" data-toggle="popover" data-placement="top" data-trigger="focus" id="txt_Code" placeholder="Organisation Code" class="form-control" style="display:none" />

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</asp:Content>
