﻿//$(document).ready(function()
//{
//    GetAll();
//});

function Submit() {
    var From = $('#datepicker1').val();
    var To = $('#datepicker2').val();
    //var DriverName = $("#sel_driver option:selected").text();
    var data = { From: From, To: To}
   
    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/DriverActivity",
        data: JSON.stringify(data),
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                var Arr = obj.Arr
                var ul = '';
                $("#Details").empty();
                for (var i = 0; i < Arr.length; i++) {

                    ul += '<tr>';
                    ul += '<td align="center" >'+(i+1)+'</td>';
                    ul += '<td align="center">'+Arr[i].AssignedTo+'</td>';
                    ul += '<td align="center">' + Arr[i].FirstName + ' ' + Arr[i].LastName + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationDate+ '</td>';
                    ul += '<td align="center">' + Arr[i].Service+ '</td>';
                    ul += '<td align="center">' + Arr[i].Source+ '</td>';
                    ul += '<td align="center">' + Arr[i].Destination+ '</td>';
                    ul += '<td align="center">' + Arr[i].TotalFare + '</td>';
                    ul += '</tr>';

                }
                //$("#TopSpan").html('<span>Total Fare : $' + parseFloat( obj.TotalFare).toFixed(2) + '</span>, <span>Percentage Fare :  $' + parseFloat(obj.PercentageOfFare).toFixed(2) + '</span>, <span>Drivers Payout :  $' + parseFloat(obj.DriversPayOut).toFixed(2) + '</span>');
                $("#Details").append(ul);
            }
            else {
                $("#TopSpan").empty();
                $("#Details").empty();
                $('#SpnMessege').text("No Record Found.")
                $('#ModelMessege').modal('show')

            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });

}

function GetAll() {
    $.ajax({
        type: "POST",
        url: "DefaultHandler.asmx/LoadAll",
        data: '',
        contentType: "application/json",
        datatype: "json",
        success: function (response) {

            var obj = JSON.parse(response.d)
            if (obj.retCode == 1) {
                var Arr = obj.Arr
                var ul = '';
                $("#Details").empty();
                for (var i = 0; i < Arr.length; i++) {

                    ul += '<tr>';
                    ul += '<td align="center" >' + (i + 1) + '</td>';
                    ul += '<td align="center">' + Arr[i].AssignedTo + '</td>';
                    ul += '<td align="center">' + Arr[i].FirstName + ' ' + Arr[i].LastName + '</td>';
                    ul += '<td align="center">' + Arr[i].ReservationDate + '</td>';
                    ul += '<td align="center">' + Arr[i].Service + '</td>';
                    ul += '<td align="center">' + Arr[i].Source + '</td>';
                    ul += '<td align="center">' + Arr[i].Destination + '</td>';
                    ul += '<td align="center">' + Arr[i].TotalFare + '</td>';
                    ul += '</tr>';

                }

                $("#Details").append(ul);

                //$("#TopSpan").html('<span>Total Fare:' + obj.TotalFare + '</span>,<span>Percentage Fare: ' + obj.PercentageOfFare + '</span>,<span>Drivers Payout: ' + obj.DriversPayOut + '</span>');
            }
            else {
                $('#SpnMessege').text("No Record Found.")
                $('#ModelMessege').modal('show')

            }
        },
        error: function () {

            $('#SpnMessege').text("Something Went Wrong.")
            $('#ModelMessege').modal('show')
        },
    });

}