﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using BWI.BL;
using BWI.DataLayer;
//using BWI.Admin.DataLayer;
using System.Data;
using System.Globalization;

namespace BWI
{
    /// <summary>
    /// Summary description for DefaultHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DefaultHandler : System.Web.Services.WebService
    {

        string json = "";
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        GlobalDefaultTransfers Global = new GlobalDefaultTransfers();

        [WebMethod(EnableSession = true)]
        public string UserLogin(string Username, string Password, string UserType)
        {
            retCode = DefaultManager.UserLogin(Username, Password, UserType);

            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
                string RoleId = Global.sRoleId;
                string Redirect = "";
                if (RoleId == "1")
                {
                    Redirect = "Admin/AdminDashboard.aspx";
                }
                else if (RoleId == "2")
                {
                    Redirect = "Customer/CustomerDashBoard.aspx";
                }
                else if (RoleId == "3")
                {
                    Redirect = "Driver/DriverDashBoard.aspx";
                }
                else if (RoleId == "4")
                {
                    Redirect = "Dispatcher/DashBoard.aspx";
                }

                json = "{\"Session\":\"1\",\"Retcode\":\"1\",\"Redirect\":\"" + Redirect + "\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"Retcode\":\"-1\"}";
            }
            else if (retCode == DBHelper.DBReturnCode.EXCEPTION)
            {
                json = "{\"Session\":\"1\",\"Retcode\":\"0\"}";
            }
            else
            {
                json = "{\"Session\":\"0\",\"Retcode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string LogOut()
        {
            HttpContext.Current.Session["UserLogin"] = null;
            json = "{\"Session\":\"1\",\"Retcode\":\"1\"}";
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string CheckSession()
        {
            if (Session["UserLogin"] != null)
                return "{\"Session\":\"1\",\"retCode\":\"1\"}";
            else
                return "{\"Session\":\"1\",\"retCode\":\"0\"}";
        }

        [WebMethod(EnableSession = true)]
        public string GetServiceType()
        {
            List<BWI.Proc_Trans_GetServiceTypeResult> ServiceType = null;
            retCode = ServiceTypeManager.GetServiceType(out ServiceType);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {
                json = jsSerializer.Serialize(ServiceType);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"ReservationDetails\":[" + json + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string ChangePassword(string Old, string NewPassword)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
                Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
                string Match = Global.sPassword;

                if (Old != Match)
                {
                    json = "{\"Session\":\"0\",\"Retcode\":\"2\"}";
                    return json;
                }
                else if (Global.sRoleId != "2")
                {
                    string Email = Global.sEmail;
                    Trans_Tbl_Login NewLogin = DB.Trans_Tbl_Logins.Single(x => x.sEmail == Email);
                    NewLogin.sPassword = NewPassword;
                    DB.SubmitChanges();
                    json = "{\"Session\":\"0\",\"Retcode\":\"1\"}";
                }
                else
                {
                    string Email = Global.sEmail;
                    Trans_Tbl_CustomerMaster NewLogin = DB.Trans_Tbl_CustomerMasters.Single(x => x.Email == Email);
                    NewLogin.Password = NewPassword;
                    Global.sPassword = NewPassword;
                    Session["UserLogin"] = Global;
                    DB.SubmitChanges();
                    json = "{\"Session\":\"0\",\"Retcode\":\"1\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"0\",\"Retcode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetAdminProfile()
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
                Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
                string Match = Global.sPassword;
                string Email = Global.sEmail;
                if (Global.sRoleId != "2")
                {
                    List<BWI.Trans_Tbl_Login> List = (from obj in DB.Trans_Tbl_Logins where obj.sEmail == Email select obj).ToList();
                    json = jsSerializer.Serialize(List);
                    json = "{\"Session\":\"0\",\"Retcode\":\"1\",\"Arr\":" + json + "}";
                }
                else
                {
                    List<BWI.Trans_Tbl_CustomerMaster> List = (from obj in DB.Trans_Tbl_CustomerMasters where obj.Email == Email select obj).ToList();
                    json = jsSerializer.Serialize(List);
                    json = "{\"Session\":\"0\",\"Retcode\":\"1\",\"Arr\":" + json + "}";
                }


            }
            catch
            {
                json = "{\"Session\":\"0\",\"Retcode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string UpdateProfile(string sFirstName, string sLastName, string sMobile, string sEmail, string sPhone, string sAddress, string sCountry, string sCode, string sPinCode, string sPanNumber)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                GlobalDefaultTransfers Global = new GlobalDefaultTransfers();
                Global = (GlobalDefaultTransfers)HttpContext.Current.Session["UserLogin"];
                string Match = Global.sPassword;
                string Email = Global.sEmail;
                if (Global.sRoleId != "2")
                {
                    Trans_Tbl_Login NewLogin = DB.Trans_Tbl_Logins.Single(x => x.sEmail == Email);

                    NewLogin.sFirstName = sFirstName;
                    NewLogin.sLastName = sLastName;
                    NewLogin.sMobile = sMobile;
                    NewLogin.sPhone = sPhone;
                    NewLogin.sPAddress = sAddress;
                    NewLogin.sPCountry = sCountry;
                    NewLogin.sPCity = sCode;
                    NewLogin.sPPincode = sPinCode;
                }
                else
                {
                    BWI.Trans_Tbl_CustomerMaster NewLogin = DB.Trans_Tbl_CustomerMasters.Single(x => x.Email == Email);
                    NewLogin.FirstName = sFirstName;
                    NewLogin.LastName = sLastName;
                    NewLogin.Mobile = sMobile;
                    NewLogin.Phone = sPhone;
                    NewLogin.Address = sAddress;
                    NewLogin.Country = sCountry;
                    NewLogin.City = sCode;
                    NewLogin.Pin = sPinCode;
                    NewLogin.Pan = sPanNumber;
                }


                DB.SubmitChanges();
                json = "{\"Session\":\"0\",\"Retcode\":\"1\"}";
            }
            catch
            {
                json = "{\"Session\":\"0\",\"Retcode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string sendPassword(string sEmail)
        {
            string sJsonString = "{\"retCode\":\"0\"}";

            Trans_Tbl_Login Login = new Trans_Tbl_Login();

            DataTable dtResult = null;
            DBHandlerDataContext DB = new DBHandlerDataContext();
            List<BWI.Trans_Tbl_Login> List;


            var Query = from Obj in DB.Trans_Tbl_Logins where Obj.sEmail == sEmail select Obj;
            //dtResult=   (from Obj in DB.Trans_Tbl_Logins where Obj.sEmail == sEmail select Obj);
            List = Query.ToList();
            if(List.Count > 0)
            {
                var Password = List[0].sPassword;
                if (retCode == DBHelper.DBReturnCode.SUCCESS && List.Count > 0)
                {
                    //string sDecryptedPassword = dtResult.Rows[0]["password"].ToString();
                    if (DefaultManager.SendPassword(sEmail, "Your Password Detail", "Information Received", Password.ToString()) == true)//info@redhillindia.com
                    {
                        json = "{\"retCode\":\"1\"}";
                    }
                }
                else
                {
                    json = "{\"retCode\":\"0\"}";
                }
            }
            else
            {
                json = "{\"retCode\":\"-1\"}";
            }

            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Register(string Email, string Password, string FirstName, string LastName, string Gender, string Mobile)
        {
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                Trans_Tbl_CustomerMaster Customer = new Trans_Tbl_CustomerMaster();

                var List = (from Obj in DB.Trans_Tbl_CustomerMasters where (Obj.Email == Email) select Obj).ToList();

                if (List.Count > 0)
                {
                    json = "{\"Session\":\"0\",\"Retcode\":\"2\"}";
                }
                else
                {
                    Customer.FirstName = FirstName;
                    Customer.LastName = LastName;
                    Customer.Gender = Gender;
                    Customer.Mobile = Mobile;
                    Customer.Email = Email;
                    Customer.Password = Password;
                    DB.Trans_Tbl_CustomerMasters.InsertOnSubmit(Customer);
                    DB.SubmitChanges();
                    json = "{\"Session\":\"0\",\"Retcode\":\"1\"}";
                }
            }
            catch
            {
                json = "{\"Session\":\"0\",\"Retcode\":\"-1\"}";
            }
            return json;
        }

        [WebMethod(true)]
        public string EmailSending(string Name, string Email, string Subject, string Website, string Message)
        {
            string sJsonString = "{\"retCode\":\"0\"}";
            //if (EmailManager.SendEmail("info@Universalimpex.asia", sEmail, "Enquiry", "Information Received", sName, sEmail, sPhone, sMessage) == true)//info@redhillindia.com
            if (DefaultManager.SendEmail("khazhar007@gmail.com", Email, "Enquiry", "Information Received", Name, Email, Subject, Website, Message) == true)//info@redhillindia.com
            {
                sJsonString = "{\"retCode\":\"1\"}";
            }
            return sJsonString;
        }

        #region Comment
        [WebMethod(true)]
        public string LoadAllComment()
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();
            string sJsonString = "";
            try
            {
                var List = (from Comm in DB.tbl_Comments select Comm).ToList();
                sJsonString = jsSerializer.Serialize(new { retCode = 1, Arr = List });
            }
            catch (Exception ex)
            {
                sJsonString = "{\"retCode\":\"0\",}";
            }
            return sJsonString;
        }

        [WebMethod(true)]
        public string SaveComment(string Name, string Message)
        {
            string sJsonString = "";
            try
            {
                DBHandlerDataContext DB = new DBHandlerDataContext();
                tbl_Comment Comment = new tbl_Comment();
                Comment.Name = Name;
                Comment.Text = Message;
                Comment.Date = DateTime.Now.Date.ToString("mm-DD-yy");
                DB.tbl_Comments.InsertOnSubmit(Comment);
                DB.SubmitChanges();
                sJsonString = "{\"retCode\":\"1\"}";
            }
            catch(Exception ex)
            {
                sJsonString = "{\"retCode\":\"0\",}";
            }
            return sJsonString;
        }

        [WebMethod(true)]
        public string DeleteComment(Int64 Sid)
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();
            string sJsonString = "";
            try
            {
                tbl_Comment Comm = DB.tbl_Comments.Single(x => x.Sid == Sid);
                DB.tbl_Comments.DeleteOnSubmit(Comm);
                DB.SubmitChanges();
                sJsonString = jsSerializer.Serialize(new { retCode = 1 });
            }
            catch (Exception ex)
            {
                sJsonString = jsSerializer.Serialize(new { retCode = 0 });
            }
            return sJsonString;
        }

        #endregion

        [WebMethod(EnableSession = true)]
        public string CheckResTime(string Pickuptime, string Date)
        {
            DateTime Today = DateTime.Now;
            try
            {
                string Pick = Date + " " + Pickuptime;
                DateTime App_Date = DateTime.ParseExact(Pick, "MM-dd-yyyy hh:mm:tt", CultureInfo.InvariantCulture);
                DateTime CDate = DateTime.Now.AddHours(24);
               
                if (App_Date > CDate)
                {
                    json = "{\"Session\":\"0\",\"Retcode\":\"1\",\"App_Date\":\"" + App_Date + "\",\"CDate\":\"" + CDate + "\"}";
                    return json;
                }
                else
                {
                    json = "{\"Session\":\"0\",\"Retcode\":\"-1\"}";
                }
                //json = "{\"Session\":\"0\",\"Retcode\":\"1\",\"Dates\":" + Today + "}";
                return json;
            }
            catch (Exception ex)
            {
                json = "{\"Session\":\"0\",\"Retcode\":\"0\"}";
            }
            //json = "{\"Session\":\"0\",\"Retcode\":\"1\",\"Dates\":" + Today + "}";
            return json;
        }

        public static DateTime GetAppDate(string Date)
        {
            // string Date = Row["AppliedDate"].ToString();
            DateTime date = new DateTime();
            Date = (Date.Split(' ')[0]).Replace("/", "-");
            try
            {
                string[] formats = {"M/d/yyyy", "MM/dd/yyyy",
                                "d/M/yyyy", "dd/MM/yyyy",
                                "yyyy/M/d", "yyyy/MM/dd",
                                "M-d-yyyy", "MM-dd-yyyy",
                                "d-M-yyyy", "dd-MM-yyyy",
                                "yyyy-M-d", "yyyy-MM-dd",
                                "M.d.yyyy", "MM.dd.yyyy",
                                "d.M.yyyy", "dd.MM.yyyy",
                                "yyyy.M.d", "yyyy.MM.dd",
                                "M,d,yyyy", "MM,dd,yyyy",
                                "d,M,yyyy", "dd,MM,yyyy",
                                "yyyy,M,d", "yyyy,MM,dd",
                                "M d yyyy", "MM dd yyyy",
                                "d M yyyy", "dd MM yyyy",
                                "yyyy M d", "yyyy MM dd",
                                "m/dd/yyyy hh:mm:ss tt",
                               };
                //DateTime dateValue;
                foreach (string dateStringFormat in formats)
                {
                    if (DateTime.TryParseExact(Date, dateStringFormat, CultureInfo.CurrentCulture, DateTimeStyles.None,
                                               out date))
                    {
                        date.ToShortDateString();
                        break;
                    }
                }
            }
            catch (Exception ex)
            { }
            finally
            { }
            return date;
        }
    }
}
