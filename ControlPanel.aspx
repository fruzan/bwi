﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ControlPanel.aspx.cs" Inherits="ControlPanel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <meta charset="UTF-8">
        <title>BWI Shuttle Service Administration Section | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    
 <body class="skin-blue">

        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                BWI Shuttle Service 
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                
                </div>
            </nav>
        </header>
     <form id="form1" runat="server">
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                  
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="index.html">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="/admin/siteadmin/adminrates.aspx">
                                <i class="fa fa-th"></i> <span>Rates Table Admin</span> <small class="badge pull-right bg-green">new</small>
                            </a>
                        </li>

                        <li>
                            <a href="/admin/siteadmin/tblAdmin.asp">
                                <i class="fa fa-th"></i> <span>Administer Users</span> <small class="badge pull-right bg-green">new</small>
                            </a>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Reservations</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/admin/siteadmin/AddResStops.asp"><i class="fa fa-angle-double-right"></i>Point to point Reservations</a></li>
                                <li><a href="/admin/siteadmin/AddRes.asp"><i class="fa fa-angle-double-right"></i>Add Reservations</a></li>
                                <li><a href="/admin/siteadmin/reztoday.asp"><i class="fa fa-angle-double-right"></i> View Reservations</a></li>
                                <li><a href="/admin/siteadmin/vwresdonebycus.asp"><i class="fa fa-angle-double-right"></i>View Reservations Done By Customer</a></li>
                                <li><a href="/admin/siteadmin/cancRes.asp"><i class="fa fa-angle-double-right"></i>Cancle Reservations </a></li>
                                <li><a href="/admin/siteadmin/DeleteRes.asp"><i class="fa fa-angle-double-right"></i>DeleteReservations </a></li>
                                <li><a href="/admin/siteadmin/pendres.asp"><i class="fa fa-angle-double-right"></i>Pending Reservations </a></li>
                                <li><a href="/admin/siteadmin/compresadmin.asp"><i class="fa fa-angle-double-right"></i>Completed Reservation </a></li>
                                <li><a href="/admin/siteadmin/updres.asp"><i class="fa fa-angle-double-right"></i>Update Reservation</a></li>
                            </ul>
                        </li>
                         <li>
                            <a href="/admin/siteadmin/Activity.asp">
                                <i class="fa fa-th"></i> <span>View Driver Activity Info </span>
                            </a>
                        </li>
                         <li>
                            <a href="/admin/siteadmin/corpActivity.asp">
                                <i class="fa fa-th"></i> <span>View Accounts Info </span> 
                            </a>
                        </li>

                          <li>
                            <a href="/admin/siteadmin/qrycustomer.asp">
                                <i class="fa fa-th"></i> <span>Query Customer Information</span> 
                            </a>
                        </li>
                        
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <!-- Small boxes (Stat box) -->
                    <div class="row">

                 <div class="col-lg-2 col-xs-4">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3 id="boxvaluexx" runat="Server">
                                        New Res
                                    </h3>
                                    <p id="box1xx">
                                         ADD Reservations 
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="/admin/siteadmin/addRes.asp" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-2 col-xs-4">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3 id="boxvalue" runat="Server">
                                        150
                                    </h3>
                                    <p id="box1">
                                         Reservations Made Today
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="/admin/siteadmin/reztoday.asp" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-2 col-xs-4">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3 id="box2value" runat="server">
                                        53<sup style="font-size: 20px">%</sup>
                                    </h3>
                                    <p id="box2">
                                         Reservations For Ser Today
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="/admin/siteadmin/corpActivity.asp" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-2 col-xs-4">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3 id="box3vaue" runat="server">
                                        44
                                    </h3>
                                    <p id="box3">
                                        Unassigned For Tommorow
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="/admin/siteadmin/unassigned.asp" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-2 col-xs-4">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3 id="box4vaue" runat="server">
                                        65
                                    </h3>
                                    <p id="box4">
                                        Rez For Service Today
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="/admin/siteadmin/servicetoday.asp" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                    </div><!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <section class="col-lg-7 connectedSortable">                            


                            <!-- Custom tabs (Charts with tabs)-->
                            <div class="nav-tabs-custom">
                                <!-- Tabs within a box -->
                                <ul class="nav nav-tabs pull-right">
                                    <li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>
                                    <li><a href="#sales-chart" data-toggle="tab">Donut</a></li>
                                    <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>
                                </ul>
                                <div class="tab-content no-padding">
                                    <!-- Morris chart - Sales -->
                                    <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
                                    <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
                                </div>
                            </div><!-- /.nav-tabs-custom -->

                            <!-- Chat box -->
                            <div class="box box-success">
                                <div class="box-header">
                                    <i class="fa fa-comments-o"></i>
                                    <h3 class="box-title">Today Reservations</h3>
                                    <div class="box-tools pull-right" data-toggle="tooltip" title="Status" >
                                      
                                    </div>
                                </div>
                                <div class="box-body chat" id="lasttenreservation"   runat="server">
                                    
                                   
                                </div><!-- /.chat -->
                                
                            </div><!-- /.box (chat box) -->                                                        

                            <!-- TO DO List -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <i class="ion ion-clipboard"></i>
                                    <h3 class="box-title">Today Service</h3>
                                    <div class="box-tools pull-right" id="lasttenServicedetails" >
                                        
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <ul class="todo-list" id="lasttendaysservice" runat="server">
                                        
                                        
                                        
                                        
                                       
                                       
                                    </ul>
                                </div><!-- /.box-body -->
                              
                            </div><!-- /.box -->

                            <!-- quick email widget -->
                            <div class="box box-info">
                                <div class="box-header">
                                    <i class="fa fa-envelope"></i>
                                    <h3 class="box-title">Quick Email</h3>
                                    <!-- tools box -->
                                    <div class="pull-right box-tools">
                                        <button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                                    </div><!-- /. tools -->
                                </div>
                                <div class="box-body">
                                   
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="emailto" placeholder="Email to:" id="txtmailto" />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="subject" placeholder="Subject" id="txtsubject" />
                                        </div>
                                        <div>
                                            <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px; "id="txtmailbody"></textarea>
                                        </div>
                                    
                                </div>
                                <div class="box-footer clearfix">
                                    <button class="pull-right btn btn-default" id="btnsendEmail" onclick="SendEmail()">Send <i class="fa fa-arrow-circle-right"></i></button>
                                    
                                </div>
     
                            </div>

                        </section><!-- /.Left col -->
                        <!-- right col (We are only adding the ID to make the widgets sortable)-->
                        <section class="col-lg-5 connectedSortable"> 

                            <!-- Map box -->
                            <div class="box box-solid bg-light-blue-gradient">
                                <div class="box-header">
                                    <!-- tools box -->
                                    <div class="pull-right box-tools">
                                        <button class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="Date range"><i class="fa fa-calendar"></i></button>
                                        <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
                                    </div><!-- /. tools -->

                                    <i class="fa fa-map-marker"></i>
                                    <h3 class="box-title">
                                        Visitors
                                    </h3>
                                </div>
                                <div class="box-body">
                                    <div id="world-map" style="height: 250px;"></div>
                                </div><!-- /.box-body-->
                                <div class="box-footer no-border">
                                    <div class="row">
                                        <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                            <div id="sparkline-1"></div>
                                            <div class="knob-label">Visitors</div>
                                        </div><!-- ./col -->
                                        <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                           <div id="sparkline-2"></div>
                                            <div class="knob-label">Online</div>
                                        </div><!-- ./col -->
                                        <div class="col-xs-4 text-center">
                                            <div id="sparkline-3"></div>
                                            <div class="knob-label">Exists</div>
                                        </div><!-- ./col -->
                                    </div><!-- /.row -->
                                </div>
                            </div>
                            <!-- /.box -->

                            <!-- solid sales graph -->
                            <div class="box box-solid bg-teal-gradient">
                                <div class="box-header">
                                    <i class="fa fa-th"></i>
                                    <h3 class="box-title">Sales Graph</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <div class="box-body border-radius-none">
                                    <div class="chart" id="line-chart" style="height: 250px;"></div>                                    
                                </div><!-- /.box-body -->
                               
                            </div><!-- /.box -->                            

                            <!-- Calendar -->
                            <div class="box box-solid bg-green-gradient">
                                <div class="box-header">
                                    <i class="fa fa-calendar"></i>
                                    <h3 class="box-title">Calendar</h3>
                                    <!-- tools box -->
                                    <div class="pull-right box-tools">
                                        <!-- button with a dropdown -->
                                        <div class="btn-group">
                                            <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></button>
                                           
                                        </div>
                                        <button class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>                                        
                                    </div><!-- /. tools -->
                                </div><!-- /.box-header -->
                                <div class="box-body no-padding">
                                    <!--The calendar -->
                                    <div id="calendar" style="width: 100%"></div>
                                </div><!-- /.box-body -->  
                               
                            </div><!-- /.box -->                            

                        </section><!-- right col -->
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="js/AdminLTE/bootstrap.min.js"></script>
        <script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="js/plugins/morris/morris.min.js" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- datepicker -->
        <script src="js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="js/AdminLTE/dashboard.js" type="text/javascript"></script>

        <!-- AdminLTE for demo purposes -->
        <script src="js/AdminLTE/demo.js" type="text/javascript"></script>
         </form>
    </body>
    <script>

        $("document").ready
            (function () {
                "use strict";

                //Make the dashboard widgets sortable Using jquery UI
                $(".connectedSortable").sortable({
                    placeholder: "sort-highlight",
                    connectWith: ".connectedSortable",
                    handle: ".box-header, .nav-tabs",
                    forcePlaceholderSize: true,
                    zIndex: 999999
                }).disableSelection();
                $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");
                //jQuery UI sortable for the todo list
                $(".todo-list").sortable({
                    placeholder: "sort-highlight",
                    handle: ".handle",
                    forcePlaceholderSize: true,
                    zIndex: 999999
                }).disableSelection();
                ;

                //bootstrap WYSIHTML5 - text editor
                $(".textarea").wysihtml5();

                $('.daterange').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                'Last 7 Days': [moment().subtract('days', 6), moment()],
                                'Last 30 Days': [moment().subtract('days', 29), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                            },
                            startDate: moment().subtract('days', 29),
                            endDate: moment()
                        },
                function (start, end) {
                    alert("You chose: " + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                });

                /* jQueryKnob */
                $(".knob").knob();

                //jvectormap data
                var visitorsData = {
                    "US": 398, //USA
                    "SA": 400, //Saudi Arabia
                    "CA": 1000, //Canada
                    "DE": 500, //Germany
                    "FR": 760, //France
                    "CN": 300, //China
                    "AU": 700, //Australia
                    "BR": 600, //Brazil
                    "IN": 800, //India
                    "GB": 320, //Great Britain
                    "RU": 3000 //Russia
                };
                //World map by jvectormap
                $('#world-map').vectorMap({
                    map: 'world_mill_en',
                    backgroundColor: "transparent",
                    regionStyle: {
                        initial: {
                            fill: '#e4e4e4',
                            "fill-opacity": 1,
                            stroke: 'none',
                            "stroke-width": 0,
                            "stroke-opacity": 1
                        }
                    },
                    series: {
                        regions: [{
                            values: visitorsData,
                            scale: ["#92c1dc", "#ebf4f9"],
                            normalizeFunction: 'polynomial'
                        }]
                    },
                    onRegionLabelShow: function (e, el, code) {
                        if (typeof visitorsData[code] != "undefined")
                            el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
                    }
                });

                //Sparkline charts
                var myvalues = [1000, 1200, 920, 927, 931, 1027, 819, 930, 1021];
                $('#sparkline-1').sparkline(myvalues, {
                    type: 'line',
                    lineColor: '#92c1dc',
                    fillColor: "#ebf4f9",
                    height: '50',
                    width: '80'
                });
                myvalues = [515, 519, 520, 522, 652, 810, 370, 627, 319, 630, 921];
                $('#sparkline-2').sparkline(myvalues, {
                    type: 'line',
                    lineColor: '#92c1dc',
                    fillColor: "#ebf4f9",
                    height: '50',
                    width: '80'
                });
                myvalues = [15, 19, 20, 22, 33, 27, 31, 27, 19, 30, 21];
                $('#sparkline-3').sparkline(myvalues, {
                    type: 'line',
                    lineColor: '#92c1dc',
                    fillColor: "#ebf4f9",
                    height: '50',
                    width: '80'
                });

                //The Calender
                $("#calendar").datepicker();

                //SLIMSCROLL FOR CHAT WIDGET
                $('#chat-box').slimScroll({
                    height: '250px'
                });

                /* Morris.js Charts */
                // Sales chart
                var area = new Morris.Area({
                    element: 'revenue-chart',
                    resize: true,
                    data: [
                         <%=getTwoLineChartData()%>],
                    xkey: 'y',
                    ykeys: ['item1', 'item2'],
                    labels: ['Item 1', 'Item 2'],
                    lineColors: ['#a0d0e0', '#3c8dbc'],
                    hideHover: 'auto',
                    xLabels: "month"
                });
                var line = new Morris.Line({
                    element: 'line-chart',
                    resize: true,
                    data: [
                         <%=getDailySalesgraph()%>],
                    xkey: 'y',
                    ykeys: ['item1'],
                    labels: ['Item 1'],
                    lineColors: ['#efefef'],
                    lineWidth: 2,
                    hideHover: 'auto',
                    gridTextColor: "#fff",
                    gridStrokeWidth: 0.4,
                    pointSize: 4,
                    pointStrokeColors: ["#efefef"],
                    gridLineColor: "#efefef",
                    gridTextFamily: "Open Sans",
                    gridTextSize: 10,
                    xLabels: 'day'
                    
                   
                });

                //Donut Chart
                var donut = new Morris.Donut({
                    element: 'sales-chart',
                    resize: true,
                    colors: ["#3c8dbc", "#f56954", "#00a65a"],
                    data: [
                         <%=getpiechart()%>],
                    hideHover: 'auto'
                });
                /*Bar chart
                var bar = new Morris.Bar({
                    element: 'bar-chart',
                    resize: true,
                    data: [
                        {y: '2006', a: 100, b: 90},
                        {y: '2007', a: 75, b: 65},
                        {y: '2008', a: 50, b: 40},
                        {y: '2009', a: 75, b: 65},
                        {y: '2010', a: 50, b: 40},
                        {y: '2011', a: 75, b: 65},
                        {y: '2012', a: 100, b: 90}
                    ],
                    barColors: ['#00a65a', '#f56954'],
                    xkey: 'y',
                    ykeys: ['a', 'b'],
                    labels: ['CPU', 'DISK'],
                    hideHover: 'auto'
                });*/
                //Fix for charts under tabs
                $('.box ul.nav a').on('shown.bs.tab', function (e) {
                    area.redraw();
                    donut.redraw();
                });


                /* BOX REFRESH PLUGIN EXAMPLE (usage with morris charts) */
                $("#loading-example").boxRefresh({
                    source: "ajax/dashboard-boxrefresh-demo.php",
                    onLoadDone: function (box) {
                        bar = new Morris.Bar({
                            element: 'bar-chart',
                            resize: true,
                            data: [
                                { y: '2006', a: 100, b: 90 },
                                { y: '2007', a: 75, b: 65 },
                                { y: '2008', a: 50, b: 40 },
                                { y: '2009', a: 75, b: 65 },
                                { y: '2010', a: 50, b: 40 },
                                { y: '2011', a: 75, b: 65 },
                                { y: '2012', a: 100, b: 90 }
                            ],
                            barColors: ['#00a65a', '#f56954'],
                            xkey: 'y',
                            ykeys: ['a', 'b'],
                            labels: ['CPU', 'DISK'],
                            hideHover: 'auto'
                        });
                    }
                });

                /* The todo list plugin */
                $(".todo-list").todolist({
                    onCheck: function (ele) {
                        //console.log("The element has been checked")
                    },
                    onUncheck: function (ele) {
                        //console.log("The element has been unchecked")
                    }
                });

            });


        function SendEmail()
        {
            var mailto = $("#txtmailto").text();
            var subject = $("#txtsubject").text();
            var mailbody = $("#txtmailbody").text();

            alert(mailto);


        }

    </script>
</html>
