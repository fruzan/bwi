﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using BWI.BL;
using BWI.DataLayer;
using System.Configuration;
using System.Data;


namespace BWI
{
    /// <summary>
    /// Summary description for RequestBookingHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RequestBookingHandler : System.Web.Services.WebService
    {

        string json = "";
        DBHelper.DBReturnCode retCode = new DBHelper.DBReturnCode();
        JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

        [WebMethod(EnableSession = true)]
        public string GetAllServices()
        {


            List<BWI.Trans_Tbl_ServiceType> objST = null;
       
            DBHandlerDataContext DB = new DBHandlerDataContext();
            var query = from objLocation in DB.Trans_Tbl_Locations select objLocation;
            List<BWI.Trans_Tbl_Location> LocTable = query.ToList();
            string jsonlocation = jsSerializer.Serialize(LocTable);

            retCode = RequestBooking.GetAllDriver(out objST);
            if (retCode == DBHelper.DBReturnCode.SUCCESS)
            {

                jsonlocation = jsonlocation.TrimEnd(']');
                jsonlocation = jsonlocation.TrimStart('[');

                json = jsSerializer.Serialize(objST);
                json = json.TrimEnd(']');
                json = json.TrimStart('[');
                json = "{\"Session\":\"1\",\"retCode\":\"1\",\"ServiceType\":[" + json + "],\"Location\":[" + jsonlocation + "]}";
            }
            else if (retCode == DBHelper.DBReturnCode.SUCCESSNORESULT)
            {
                json = "{\"Session\":\"1\",\"retCode\":\"-1\"}";
            }
            else
            {
                json = "{\"Session\":\"1\",\"retCode\":\"0\"}";
            }

            return json;
        }

        #region Destination


        [WebMethod(EnableSession = true)]
        public string GetLocation(string name)
        {
            string jsonString = "";
            //DataTable dtResult;
            List<BWI.Trans_Tbl_Location> LocTable;
            DBHelper.DBReturnCode retcode = RequestBooking.Get(name, out LocTable);
            JavaScriptSerializer objSerlizer = new JavaScriptSerializer();
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                List<Models.AutoComplete> list_autocomplete = new List<Models.AutoComplete>();
                list_autocomplete = LocTable.AsEnumerable()
                .Select(data => new Models.AutoComplete
                {
                    sid = data.sid,
                    LocationName = data.LocationName,
                    Latitude = data.Latitude,
                    Longitude = data.Longitude
                }).ToList();

                jsonString = objSerlizer.Serialize(list_autocomplete);
                LocTable.Clear();
            }
            else
            {
                jsonString = objSerlizer.Serialize(new { id = "", value = "No Data found" });
            }
            return jsonString;
        }

        #endregion Destination
    }
}
