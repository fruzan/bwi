﻿using BWI.BL;
using BWI.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace BWI
{
    /// <summary>
    /// Summary description for BookingHandler
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class BookingHandler : System.Web.Services.WebService
    {


        [WebMethod(EnableSession = true)]
        public string LoadCars(string Tab, int Capacity)
        {
            Session["Tab"] = Tab;
            Session["Capacity"] = Capacity;
            JavaScriptSerializer Js = new JavaScriptSerializer();
            DBHandlerDataContext DB = new DBHandlerDataContext();
            string json = "";
            string ArrCar = "";

            try
            {

                if (Tab == "1" || Tab == "2")
                {
                    #region Distance
                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                    //var List = (from ObjVehicle in DB.Trans_tbl_Vehicle_Infos
                    //            join ObjRate in DB.Trans_Tbl_DistanceRates
                    //            on Convert.ToInt16(ObjVehicle.Vechile_Type) equals ObjRate.CarType_Sid
                    //            where ObjVehicle.Max_Capcity >= Capacity
                    //            orderby Convert.ToDecimal(ObjRate.BaseCharge) ascending
                    //            select new
                    var List = (from ObjVehicle in DB.Trans_tbl_Vehicle_Infos
                                join ObjRate in DB.Trans_Tbl_DistanceRates
                                on Convert.ToInt16(ObjVehicle.sid) equals ObjRate.CarType_Sid
                                where ObjVehicle.Max_Capcity >= Capacity && ObjVehicle.Model != "Shuttle"
                                orderby Convert.ToDecimal(ObjRate.BaseCharge) ascending
                                select new
                                {
                                    //ObjVehicle.sid,
                                    ObjVehicle.UniqueId,
                                    ObjVehicle.Vehicle_Make,
                                    ObjVehicle.Model,
                                    ObjVehicle.Vechile_Type,
                                    ObjVehicle.Max_Capcity,
                                    ObjVehicle.Max_Baggage,
                                    ObjVehicle.Img_Url,
                                    ObjVehicle.Remark,
                                    ObjRate.sid,
                                    ObjRate.CarType_Sid,
                                    ObjRate.Name,
                                    ObjRate.RateGroup,
                                    ObjRate.BaseDistance,
                                    ObjRate.BaseCharge,
                                    ObjRate.CostPerDistance,
                                    ObjRate.MilesPerDistance
                                }).ToList();


                    //List<CUT.Trans_tbl_Vehicle_Info> List = Query.ToList();
                    if (List.Count > 0)
                    {
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"ArrCar\":[" + ArrCar + "]}";
                        Session["List"] = List;
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                    #endregion
                }
                else if(Tab=="4")
                {
                    #region Distance
                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                    //var List = (from ObjVehicle in DB.Trans_tbl_Vehicle_Infos
                    //            join ObjRate in DB.Trans_Tbl_DistanceRates
                    //            on Convert.ToInt16(ObjVehicle.Vechile_Type) equals ObjRate.CarType_Sid
                    //            where ObjVehicle.Max_Capcity >= Capacity
                    //            orderby Convert.ToDecimal(ObjRate.BaseCharge) ascending
                    //            select new
                    var List = (from ObjVehicle in DB.Trans_tbl_Vehicle_Infos
                                join ObjRate in DB.Trans_Tbl_DistanceRates on Convert.ToInt16(ObjVehicle.sid) equals ObjRate.CarType_Sid
                                where ObjVehicle.sid == 4052
                               // orderby Convert.ToDecimal(ObjRate.BaseCharge) ascending
                                select new
                                {
                                    //ObjVehicle.sid,
                                    ObjVehicle.UniqueId,
                                    ObjVehicle.Vehicle_Make,
                                    ObjVehicle.Model,
                                    ObjVehicle.Vechile_Type,
                                    ObjVehicle.Max_Capcity,
                                    ObjVehicle.Max_Baggage,
                                    ObjVehicle.Img_Url,
                                    ObjVehicle.Remark,
                                    ObjRate.sid,
                                    ObjRate.CarType_Sid,
                                    ObjRate.Name,
                                    ObjRate.RateGroup,
                                    ObjRate.BaseDistance,
                                    ObjRate.BaseCharge,
                                    ObjRate.CostPerDistance,
                                    ObjRate.MilesPerDistance
                                }).ToList();


                    //List<CUT.Trans_tbl_Vehicle_Info> List = Query.ToList();
                    if (List.Count > 0)
                    {
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"ArrCar\":[" + ArrCar + "]}";
                        Session["List"] = List;
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                    #endregion
                }

                else
                {
                    #region Hourly
                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                    var List = (from ObjVehicle in DB.Trans_tbl_Vehicle_Infos
                                join ObjRate in DB.Trans_Tbl_HourlyRates
                                on Convert.ToInt16(ObjVehicle.sid) equals ObjRate.CarType_Sid
                                where ObjVehicle.Max_Capcity >= Capacity
                                orderby Convert.ToDecimal(ObjRate.HourlyRate) ascending
                                select new
                                {
                                    //ObjVehicle.sid,
                                    ObjVehicle.UniqueId,
                                    ObjVehicle.Vehicle_Make,
                                    ObjVehicle.Model,
                                    ObjVehicle.Vechile_Type,
                                    ObjVehicle.Max_Capcity,
                                    ObjVehicle.Max_Baggage,
                                    ObjVehicle.Img_Url,
                                    ObjVehicle.Remark,

                                    ObjRate.sid,
                                    ObjRate.Name,
                                    ObjRate.RateGroup,
                                    //ObjRate.BaseCharge,
                                    ObjRate.HourlyMinimum,
                                    ObjRate.HourlyRate,
                                    ObjRate.ServiceUpto,
                                    ObjRate.Service
                                }).ToList();


                    //List<CUT.Trans_tbl_Vehicle_Info> List = Query.ToList();
                    if (List.Count > 0)
                    {
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"ArrCar\":[" + ArrCar + "]}";
                        Session["List"] = List;
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                    #endregion
                }
            }
            catch
            {
                json = "{\"Retcode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string Assending()
        {
            string Tab = (Session["Tab"]).ToString();
            int Capacity = Convert.ToInt16(Session["Capacity"]);
            JavaScriptSerializer Js = new JavaScriptSerializer();
            DBHandlerDataContext DB = new DBHandlerDataContext();
            string json = "";
            string ArrCar = "";

            try
            {

                if (Tab == "1" || Tab == "2")
                {
                    #region Distance
                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                    var List = (from ObjVehicle in DB.Trans_tbl_Vehicle_Infos
                                join ObjRate in DB.Trans_Tbl_DistanceRates
                                on Convert.ToInt16(ObjVehicle.Vechile_Type) equals ObjRate.CarType_Sid
                                where ObjVehicle.Max_Capcity >= Capacity
                                orderby (ObjVehicle.Model) ascending
                                select new
                                {
                                    //ObjVehicle.sid,
                                    ObjVehicle.UniqueId,
                                    ObjVehicle.Vehicle_Make,
                                    ObjVehicle.Model,
                                    ObjVehicle.Vechile_Type,
                                    ObjVehicle.Max_Capcity,
                                    ObjVehicle.Max_Baggage,
                                    ObjVehicle.Img_Url,
                                    ObjVehicle.Remark,
                                    ObjRate.sid,
                                    ObjRate.Name,
                                    ObjRate.RateGroup,
                                    ObjRate.BaseDistance,
                                    ObjRate.BaseCharge,
                                    ObjRate.CostPerDistance,
                                    ObjRate.MilesPerDistance
                                }).ToList();


                    //List<CUT.Trans_tbl_Vehicle_Info> List = Query.ToList();
                    if (List.Count > 0)
                    {
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"ArrCar\":[" + ArrCar + "]}";
                        Session["List"] = List;
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                    #endregion
                }

                else
                {
                    #region Hourly
                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                    var List = (from ObjVehicle in DB.Trans_tbl_Vehicle_Infos
                                join ObjRate in DB.Trans_Tbl_HourlyRates
                                on Convert.ToInt16(ObjVehicle.Vechile_Type) equals ObjRate.CarType_Sid
                                where ObjVehicle.Max_Capcity >= Capacity
                                orderby (ObjVehicle.Model) ascending
                                select new
                                {
                                    //ObjVehicle.sid,
                                    ObjVehicle.UniqueId,
                                    ObjVehicle.Vehicle_Make,
                                    ObjVehicle.Model,
                                    ObjVehicle.Vechile_Type,
                                    ObjVehicle.Max_Capcity,
                                    ObjVehicle.Max_Baggage,
                                    ObjVehicle.Img_Url,
                                    ObjVehicle.Remark,
                                    ObjRate.sid,
                                    ObjRate.Name,
                                    ObjRate.RateGroup,
                                    ObjRate.HourlyMinimum,
                                    ObjRate.HourlyRate,
                                    ObjRate.ServiceUpto,
                                    ObjRate.Service
                                }).ToList();


                    //List<CUT.Trans_tbl_Vehicle_Info> List = Query.ToList();
                    if (List.Count > 0)
                    {
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"ArrCar\":[" + ArrCar + "]}";
                        Session["List"] = List;
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                    #endregion
                }
            }
            catch
            {
                json = "{\"Retcode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string Descending()
        {
            string Tab = (Session["Tab"]).ToString();
            int Capacity = Convert.ToInt16(Session["Capacity"]);
            JavaScriptSerializer Js = new JavaScriptSerializer();
            DBHandlerDataContext DB = new DBHandlerDataContext();
            string json = "";
            string ArrCar = "";

            try
            {

                if (Tab == "1" || Tab == "2")
                {
                    #region Distance
                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                    var List = (from ObjVehicle in DB.Trans_tbl_Vehicle_Infos
                                join ObjRate in DB.Trans_Tbl_DistanceRates
                                on Convert.ToInt16(ObjVehicle.Vechile_Type) equals ObjRate.CarType_Sid
                                where ObjVehicle.Max_Capcity >= Capacity
                                orderby (ObjVehicle.Model) descending
                                select new
                                {
                                    //ObjVehicle.sid,
                                    ObjVehicle.UniqueId,
                                    ObjVehicle.Vehicle_Make,
                                    ObjVehicle.Model,
                                    ObjVehicle.Vechile_Type,
                                    ObjVehicle.Max_Capcity,
                                    ObjVehicle.Max_Baggage,
                                    ObjVehicle.Img_Url,
                                    ObjVehicle.Remark,
                                    ObjRate.sid,
                                    ObjRate.Name,
                                    ObjRate.RateGroup,
                                    ObjRate.BaseDistance,
                                    ObjRate.BaseCharge,
                                    ObjRate.CostPerDistance,
                                    ObjRate.MilesPerDistance
                                }).ToList();


                    //List<CUT.Trans_tbl_Vehicle_Info> List = Query.ToList();
                    if (List.Count > 0)
                    {
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"ArrCar\":[" + ArrCar + "]}";
                        Session["List"] = List;
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                    #endregion
                }

                else
                {
                    #region Hourly
                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                    var List = (from ObjVehicle in DB.Trans_tbl_Vehicle_Infos
                                join ObjRate in DB.Trans_Tbl_HourlyRates
                                on Convert.ToInt16(ObjVehicle.Vechile_Type) equals ObjRate.CarType_Sid
                                where ObjVehicle.Max_Capcity >= Capacity
                                orderby (ObjVehicle.Model) descending
                                select new
                                {
                                    //ObjVehicle.sid,
                                    ObjVehicle.UniqueId,
                                    ObjVehicle.Vehicle_Make,
                                    ObjVehicle.Model,
                                    ObjVehicle.Vechile_Type,
                                    ObjVehicle.Max_Capcity,
                                    ObjVehicle.Max_Baggage,
                                    ObjVehicle.Img_Url,
                                    ObjVehicle.Remark,
                                    ObjRate.sid,
                                    ObjRate.Name,
                                    ObjRate.RateGroup,
                                    ObjRate.HourlyMinimum,
                                    ObjRate.HourlyRate,
                                    ObjRate.ServiceUpto,
                                    ObjRate.Service
                                }).ToList();


                    //List<CUT.Trans_tbl_Vehicle_Info> List = Query.ToList();
                    if (List.Count > 0)
                    {
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"ArrCar\":[" + ArrCar + "]}";
                        Session["List"] = List;
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                    #endregion
                }
            }
            catch
            {
                json = "{\"Retcode\":\"0\"}";
            }
            return json;
        }


        [WebMethod(EnableSession = true)]
        public string PriceFilter(Decimal Min, Decimal Max)
        {
            string Tab = (Session["Tab"]).ToString();
            int Capacity = Convert.ToInt16(Session["Capacity"]);
            JavaScriptSerializer Js = new JavaScriptSerializer();
            DBHandlerDataContext DB = new DBHandlerDataContext();
            string json = "";
            string ArrCar = "";

            try
            {

                if (Tab == "1" || Tab == "2")
                {
                    #region Distance
                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();



                    var List = (from ObjVehicle in DB.Trans_tbl_Vehicle_Infos
                                join ObjRate in DB.Trans_Tbl_DistanceRates
                                on Convert.ToInt16(ObjVehicle.Vechile_Type) equals ObjRate.CarType_Sid
                                where ObjVehicle.Max_Capcity >= Capacity && Convert.ToDecimal(ObjRate.BaseCharge) >= Min && Convert.ToDecimal(ObjRate.BaseCharge) <= Max
                                orderby Convert.ToDecimal(ObjRate.BaseCharge) ascending
                                select new
                                {
                                    //ObjVehicle.sid,
                                    ObjVehicle.UniqueId,
                                    ObjVehicle.Vehicle_Make,
                                    ObjVehicle.Model,
                                    ObjVehicle.Vechile_Type,
                                    ObjVehicle.Max_Capcity,
                                    ObjVehicle.Max_Baggage,
                                    ObjVehicle.Img_Url,
                                    ObjVehicle.Remark,
                                    ObjRate.sid,
                                    ObjRate.Name,
                                    ObjRate.RateGroup,
                                    ObjRate.BaseDistance,
                                    ObjRate.BaseCharge,
                                    ObjRate.CostPerDistance,
                                    ObjRate.MilesPerDistance
                                }).ToList();


                    //List<CUT.Trans_tbl_Vehicle_Info> List = Query.ToList();
                    if (List.Count > 0)
                    {
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"ArrCar\":[" + ArrCar + "]}";
                        Session["List"] = List;
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                    #endregion
                }

                else
                {
                    #region Hourly
                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                    var List = (from ObjVehicle in DB.Trans_tbl_Vehicle_Infos
                                join ObjRate in DB.Trans_Tbl_HourlyRates
                                on Convert.ToInt16(ObjVehicle.Vechile_Type) equals ObjRate.CarType_Sid
                                where ObjVehicle.Max_Capcity >= Capacity && Convert.ToDecimal(ObjRate.HourlyRate) >= Min && Convert.ToDecimal(ObjRate.HourlyRate) <= Max
                                orderby Convert.ToDecimal(ObjRate.HourlyRate) ascending
                                select new
                                {
                                    //ObjVehicle.sid,
                                    ObjVehicle.UniqueId,
                                    ObjVehicle.Vehicle_Make,
                                    ObjVehicle.Model,
                                    ObjVehicle.Vechile_Type,
                                    ObjVehicle.Max_Capcity,
                                    ObjVehicle.Max_Baggage,
                                    ObjVehicle.Img_Url,
                                    ObjVehicle.Remark,
                                    ObjRate.sid,
                                    ObjRate.Name,
                                    ObjRate.RateGroup,
                                    ObjRate.HourlyMinimum,
                                    ObjRate.HourlyRate,
                                    ObjRate.ServiceUpto,
                                    ObjRate.Service
                                }).ToList();


                    //List<CUT.Trans_tbl_Vehicle_Info> List = Query.ToList();
                    if (List.Count > 0)
                    {
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"ArrCar\":[" + ArrCar + "]}";
                        Session["List"] = List;
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                    #endregion
                }
            }
            catch
            {
                json = "{\"Retcode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string BookingSuccess(string ResDate, string Pickup_Location, string Drop_Location, string PickupTime, string txt_Person, string AirLines, string FlightNumber, string FlightTime, string FName, string LName, string PhoneNumber, string AltPhoneNumber, string Email, string TotalFare, string From, string To, string ApproxDistance, string ApproxTime, string Service, string Stop1, string Stop2, string Stop3, string Stop4, string Stop5, string TotalAmount, string MeetGreet, string GratuityPercent, string GratuityAmount, string CcType, string CvNumber, string OfferCode, decimal DiscountPrice, decimal OfferPercent, string Remark, string Adult, string Child, string AccNo)
        {
            DBHelper.DBReturnCode retcode;
            string Acc4Last = (AccNo.Length > 3) ? AccNo.Substring(AccNo.Length - 4, 4) : AccNo;
            retcode = BookingManager.BookingSuccess(ResDate, Pickup_Location, Drop_Location, PickupTime, txt_Person, AirLines, FlightNumber, FlightTime, FName, LName, PhoneNumber, AltPhoneNumber, Email, TotalFare, From, To, ApproxDistance, ApproxTime, Service, Stop1, Stop2, Stop3, Stop4, Stop5, TotalAmount, MeetGreet, GratuityPercent, GratuityAmount, CcType, CvNumber, OfferCode, DiscountPrice, OfferPercent, Remark, Adult, Child, Acc4Last);

            DBHandlerDataContext DB = new DBHandlerDataContext();

            string json = "";
            var List = (from Obj in DB.Trans_Tbl_CustomerMasters where (Obj.Email == Email) select Obj).ToList();
            if (List.Count > 0)
            {
                if (retcode == DBHelper.DBReturnCode.SUCCESS)
                {
                    string SessionReservation = "";
                    SessionReservation = (HttpContext.Current.Session["ReservationNo"]).ToString();
                    json = "{\"Retcode\":\"1\",\"ID\":\"" + SessionReservation + "\"}";
                }
                else
                {
                    json = "{\"Retcode\":\"2\"}";

                }
            }
            else
            {
                json = "{\"Retcode\":\"3\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string BookingSuccessReturn(string ResDate, string Pickup_Location, string Drop_Location, string PickupTime, string txt_Person, string AirLines, string FlightNumber, string FlightTime, string FName, string LName, string PhoneNumber, string AltPhoneNumber, string Email, string TotalFare, string From, string To, string ApproxDistance, string ApproxTime, string Service, string Stop1, string Stop2, string Stop3, string Stop4, string Stop5, string Ret_Date, string Ret_Time, string Ret_FlightNumber, string Ret_Airlines, string Ret_FlightTime, string MeetGreet, string GratuityPercent, string GratuityAmount, string CcType, string CvNumber, string OfferCode, decimal DiscountPrice, decimal OfferPercent, string Remark, string Adult, string Child, string AccNo)
        {
            DBHelper.DBReturnCode retcode;
            string Acc4Last = (AccNo.Length > 3) ? AccNo.Substring(AccNo.Length - 4, 4) : AccNo;
            retcode = BookingManager.BookingSuccessReturn(ResDate, Pickup_Location, Drop_Location, PickupTime, txt_Person, AirLines, FlightNumber, FlightTime, FName, LName, PhoneNumber, AltPhoneNumber, Email, TotalFare, From, To, ApproxDistance, ApproxTime, Service, Stop1, Stop2, Stop3, Stop4, Stop5, Ret_Date, Ret_Time, Ret_FlightNumber, Ret_Airlines, Ret_FlightTime, MeetGreet, GratuityPercent, GratuityAmount, CcType, CvNumber, OfferCode, DiscountPrice, OfferPercent, Remark, Adult, Child, Acc4Last);

            string json = "";
            if (retcode == DBHelper.DBReturnCode.SUCCESS)
            {
                string SessionReservation = "";
                SessionReservation = (HttpContext.Current.Session["ReservationNo"]).ToString();
                json = "{\"Retcode\":\"1\",\"ID\":\"" + SessionReservation + "\"}";
            }
            else
            {
                json = "{\"Retcode\":\"2\"}";

            }



            return json;
        }

        [WebMethod(EnableSession = true)]
        public string CustomerLogin(string txt_Username, string txt_Password)
        {
            string FirstName = "";
            string LastName = "";
            string Email = "";
            string Mobile = "";

            DBHandlerDataContext DB = new DBHandlerDataContext();
            var List = (from Obj in DB.Trans_Tbl_CustomerMasters where (Obj.Email == txt_Username) && (Obj.Password == txt_Password) select Obj).ToList();

            if (List.Count == 1)
            {
                FirstName = (List[0].FirstName).ToString();
                LastName = (List[0].LastName).ToString();
                Email = (List[0].Email).ToString();
                Mobile = (List[0].Mobile).ToString();
                return "{\"Retcode\":\"1\",\"FirstName\":\"" + FirstName + "\",\"LastName\":\"" + LastName + "\",\"Email\":\"" + Email + "\",\"Mobile\":\"" + Mobile + "\"}";
            }
            else
            {
                return "{\"Retcode\":\"2\",\"FirstName\":\"" + FirstName + "\",\"LastName\":\"" + LastName + "\",\"Email\":\"" + Email + "\",\"Mobile\":\"" + Mobile + "\"}";
            }


        }

        [WebMethod(EnableSession = true)]
        public string Emailchechking(string Email)
        {
            DBHandlerDataContext DB = new DBHandlerDataContext();

            string json = "";
            var List = (from Obj in DB.Trans_Tbl_CustomerMasters where (Obj.Email == Email) select Obj).ToList();
            if (List.Count > 0)
            {
                json = "{\"Retcode\":\"1\"}";
            }
            else
            {
                json = "{\"Retcode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string GetRate(Int64 sid)
        {

            string Tab = (Session["Tab"]).ToString();
            Session["CarType"] = sid.ToString();

            JavaScriptSerializer Js = new JavaScriptSerializer();
            DBHandlerDataContext DB = new DBHandlerDataContext();
            string json = "";
            string ArrCar = "";
            try
            {

                if (Tab == "1" || Tab == "2")
                {

                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                    var List = (from Obj in DB.Trans_Tbl_DistanceRates where Obj.CarType_Sid == sid select Obj).ToList();
                    if (List.Count > 0)
                    {
                        string CarSid = (List[0].CarType_Sid).ToString();
                        var Image = (from Obj in DB.Trans_tbl_Vehicle_Infos where Obj.sid == Convert.ToInt16(CarSid) select Obj.Img_Url).ToList();
                        string Img = Image[0];
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"Tab\":\"" + Tab + "\",\"Arr\":[" + ArrCar + "],\"Img\":\"" + Img + "\"}";
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                }

                else if (Tab == "4")
                {

                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                    var List = (from Obj in DB.Trans_Tbl_DistanceRates where Obj.CarType_Sid == sid select Obj).ToList();
                    if (List.Count > 0)
                    {
                        string CarSid = (List[0].CarType_Sid).ToString();
                        var Image = (from Obj in DB.Trans_tbl_Vehicle_Infos where Obj.sid == Convert.ToInt16(CarSid) select Obj.Img_Url).ToList();
                        string Img = Image[0];
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"Tab\":\"" + Tab + "\",\"Arr\":[" + ArrCar + "],\"Img\":\"" + Img + "\"}";
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                }

                else
                {
                    Trans_tbl_Vehicle_Info VehicalInfo = new Trans_tbl_Vehicle_Info();
                    //var List = (from Obj in DB.Trans_Tbl_HourlyRates where Obj.CarType_Sid == sid select Obj).ToList();
                    var List = (from ObjVehicle in DB.Trans_tbl_Vehicle_Infos
                                join ObjRate in DB.Trans_Tbl_HourlyRates
                                on Convert.ToInt16(ObjVehicle.sid) equals ObjRate.CarType_Sid
                                where ObjRate.sid == sid
                                orderby Convert.ToDecimal(ObjRate.HourlyRate) ascending
                                select new
                                {
                                    //ObjVehicle.sid,
                                    ObjVehicle.UniqueId,
                                    ObjVehicle.Vehicle_Make,
                                    ObjVehicle.Model,
                                    ObjVehicle.Vechile_Type,
                                    ObjVehicle.Max_Capcity,
                                    ObjVehicle.Max_Baggage,
                                    ObjVehicle.Img_Url,
                                    ObjVehicle.Remark,

                                    ObjRate.sid,
                                    ObjRate.Name,
                                    ObjRate.RateGroup,
                                    ObjRate.CarType_Sid,
                                    ObjRate.HourlyMinimum,
                                    ObjRate.HourlyRate,
                                    ObjRate.ServiceUpto,
                                    ObjRate.Service
                                }).ToList();
                    if (List.Count > 0)
                    {
                        Int64 CarSid = Convert.ToInt64(List[0].CarType_Sid);
                        var Image = (from Obj in DB.Trans_tbl_Vehicle_Infos where Obj.sid == CarSid select Obj.Img_Url).ToList();
                        string Img = Image[0];
                        ArrCar = Js.Serialize(List);
                        ArrCar = ArrCar.TrimStart('[');
                        ArrCar = ArrCar.TrimEnd(']');
                        json = "{\"Retcode\":\"1\",\"Tab\":\"3\",\"Arr\":[" + ArrCar + "],\"Img\":\"" + Img + "\"}";
                    }
                    else
                    {
                        json = "{\"Retcode\":\"2\"}";
                    }
                }
            }
            catch
            {
                json = "{\"Retcode\":\"0\"}";
            }
            return json;
        }

        [WebMethod(EnableSession = true)]
        public string CheckOffer(string OfferCode)
        {
            string json = "";
            //decimal DiscountPrice;
            DBHandlerDataContext DB = new DBHandlerDataContext();
            JavaScriptSerializer Js = new JavaScriptSerializer();
            try
            {
                var List = (from Offers in DB.Trans_Tbl_Offers where Offers.Code == OfferCode select Offers).ToList();
                if (List.Count > 0)
                {
                    //decimal PercentAmount = (Cost / 100) * Convert.ToDecimal(List[0].Percents);
                    //DiscountPrice = Cost - PercentAmount;
                    json = Js.Serialize(new { retCode = 1, Offerstbl = List });
                }
                else
                {
                    json = Js.Serialize(new { retCode = 0 });
                }
            }
            catch (Exception ex)
            {
                json = Js.Serialize(new { retCode = 0 });
            }
            return json;
        }
    }
}
