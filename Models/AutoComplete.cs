﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BWI.Models
{
    public class AutoComplete
    {
        public long sid { get; set; }
        public string LocationName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}