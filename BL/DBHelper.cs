﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BWI.BL
{
    public class DBHelper
    {
        public enum DBReturnCode
        {
            SUCCESS = 0,
            CONNECTIONFAILURE = -1,
            SUCCESSNORESULT = -2,
            SUCCESSNOAFFECT = -3,
            DUPLICATEENTRY = -4,
            EXCEPTION = -5,
            INPUTPARAMETEREMPTY = -6
        }

        protected static SqlConnection OpenConnection()
        {
            SqlConnection conn = null;
            try
            {
                //string pwd = ConfigurationManager.AppSettings["DBPassword"].ToString();
                string conStr = String.Format(ConfigurationManager.ConnectionStrings["Bwi_ConnectionString"].ConnectionString);
                conn = new SqlConnection(conStr);
                conn.Open();
            }
            catch
            {
                conn = null;
            }
            return conn;
        }
        protected static void CloseConnection(SqlConnection conn)
        {
            try
            {
                conn.Close();
                conn.Dispose();
            }
            catch { }
        }

        public static DBReturnCode ExecuteNonQuery(string cmdTxt, out int rows, params SqlParameter[] commandParameters)
        {
            SqlConnection connection = OpenConnection();
            DBReturnCode retcode;
            rows = 0;
            if (connection == null)
            { retcode = DBReturnCode.CONNECTIONFAILURE; }
            else
            {
                try
                {
                    SqlCommand command = GetCommand(cmdTxt, connection, commandParameters);
                    rows = command.ExecuteNonQuery();
                    if (rows < 1)
                    { retcode = DBReturnCode.SUCCESSNOAFFECT; }
                    else
                    { retcode = DBReturnCode.SUCCESS; }
                }
                catch
                { retcode = DBReturnCode.EXCEPTION; }
                finally
                { CloseConnection(connection); }
            }
            return retcode;
        }
        
        private static SqlCommand GetCommand(string cmdTxt, SqlConnection connection, params SqlParameter[] commandParameters)
        {
            SqlCommand command = new SqlCommand(cmdTxt, connection);
            if (cmdTxt.ToLower().StartsWith("usp_") || cmdTxt.ToLower().StartsWith("pr"))
                command.CommandType = CommandType.StoredProcedure;
            if (commandParameters != null)
                AttachParameters(command, commandParameters);
            return command;
        }

        private static void AttachParameters(SqlCommand command, SqlParameter[] commandParameters)
        {
            if (command == null) throw new ArgumentNullException("command");
            if (commandParameters != null)
            {
                foreach (SqlParameter p in commandParameters)
                {
                    if (p != null)
                    {
                        // Check for derived output value with no value assigned
                        if ((p.Direction == ParameterDirection.InputOutput ||
                            p.Direction == ParameterDirection.Input) &&
                            (p.Value == null))
                        {
                            p.Value = DBNull.Value;
                        }
                        command.Parameters.Add(p);
                    }
                }
            }
        }
    }
}